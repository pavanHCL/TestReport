﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.CacheManagement
{
    public  class CountryInformation
    {
        public string COUNTRYCODE { get; set; }

        public int Value { get; set; }

        public string Text { get; set; }

        public string OceanPort { get; set; }

        public string AirPort { get; set; }

        public string Doortodoor { get; set; }
    }

    public class ProductClassification
    {
        public string text { get; set; }

        public int Value { get; set; }

        public string Exception { get; set; }

        public string CountryId { get; set; }

        public string CountryName { get; set; }

        public string CountryException { get; set; }

    }
}
