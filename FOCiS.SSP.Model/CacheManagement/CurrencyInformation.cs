﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.CacheManagement
{
    public class CurrencyInformation
    {
        public string ID { get; set; }
        public string TEXT { get; set; }
        public int DECIMALPOINT { get; set; }      
    }
}
