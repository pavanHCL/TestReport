﻿namespace FOCiS.SSP.Models.JP
{
    using System.ComponentModel.DataAnnotations;

    public class StripeSuccessModel
    {
        [Required]
        public string ID { get; set; }

        [Required]
        public int AmountValue { get; set; }
        // These fields are optional and are not 
        // required for the creation of the token
        public string CurrencyV { get; set; }
        public string StatusV { get; set; }
        public bool LivemodeV { get; set; }
        public string EmailV { get; set; }
        public string ObjectV { get; set; }
        public string DescriptionV { get; set; }
    }
}