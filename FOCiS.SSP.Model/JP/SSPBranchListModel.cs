﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.JP
{
    public class SSPBranchListModel
    {
        public decimal BranchKey { get; set; }
        public string CountryCode { get; set; }
        public decimal FinancialEntityCode { get; set; }
        public string FinancialEntityName { get; set; }
        public string NWCCode { get; set; }
        public string NWCFunction { get; set; }
        public string SiteAddress { get; set; }
        public string SiteCity { get; set; }
        public string SiteName { get; set; }
        public string USC { get; set; }        
    }
}
