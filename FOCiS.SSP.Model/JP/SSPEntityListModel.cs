﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.JP
{
    public class SSPEntityListModel
    {        
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BeneficiaryName { get; set; }
        public string Branch { get; set; }
        public string CountryCode { get; set; }
        public Int64 EntityKey { get; set; }
        public decimal FinancialEntityCode { get; set; }
        public string FinancialEntityName { get; set; }
        public string IFSCCode { get; set; }
        public string SwiftCode { get; set; }
        public Int64 CountryID { get; set; }
    }
}
