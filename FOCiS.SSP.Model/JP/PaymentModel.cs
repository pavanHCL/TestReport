﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.JP
{
    public class PaymentModel
    {
        [Required]
        public string Token { get; set; }

        [Required]
        public double Amount { get; set; }
        // These fields are optional and are not 
        // required for the creation of the token
        public string CardHolderName { get; set; } 
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressPostcode { get; set; }
        public string AddressCountry { get; set; }
        public long RECEIPTHDRID { get; set; }
        public long JobNumber { get; set; }
        public string OPjobnumber { get; set; }
        public long QuotationId { get; set; }
        public string QuotationNumber { get; set; }
        public decimal ReceiptNetAmount { get; set; }
        public string Currency { get; set; }

        public string ShipperName { get; set; }
        public string ConsigneeName { get; set; }
        public string ModeOfTransport { get; set; }
        public string MovementType { get; set; }

        public string CardHolderFirstName { get; set; }
        public string CardHolderLastName { get; set; }

        public string CardNumber { get; set; }        
        public string CVV { get; set; }        
        public string CardType { get; set; }       
        public string ExpMonth { get; set; }        
        public string ExpYear { get; set; }

        public string PaymentOption { get; set; }
        public string PaymentType { get; set; }
        public string TransactionMode { get; set; }
        public string TransAmount { get; set; }
        public string TransCurrency { get; set; }
        public string TransDescription { get; set; }
        public string TransBankName { get; set; }
        public string TransBranchName { get; set; }       
        public string TransChequeNo { get; set; }
        public DateTime? TransChequeDate { get; set; }
        public string TransCDDescription { get; set; }
        public string TransCDAmount { get; set; }
        public string TransCDCurrency { get; set; }

        public string WTTransBankName { get; set; }
        public string WTTransBranchName { get; set; }
        public string WTTransChequeNo { get; set; }
        public DateTime? WTTransChequeDate { get; set; }
        public string WTTransAmount { get; set; }
        public string WTTransCurrency { get; set; }
        public string WTTransDescription { get; set; }

        public double CreditAmount { get; set; }
        public string CreditCurrency { get; set; }

        public string OfficeSiteCity { get; set; }

        public IList<SSPBranchListModel> BranchList { get; set; }
        public SSPEntityListModel EntityList { get; set; }
        public DateTime CargoAvailableFrom { get; set; }
        public SSPPaymentOptionListModel PaymentOptionList { get; set; }
        public string DiscountCode { get; set; }
        public decimal DiscountAmount { get; set; }
        public DateTime DiscountExpiryDate { get; set; }
        public decimal NOTSUPPAMOUNTUSD { get; set; }
        public string UserId { get; set; }
        public decimal ADDITIONALCHARGEAMOUNT { get; set; }
        public string ADDITIONALCHARGESTATUS { get; set; }
        public decimal ADDITIONALCHARGEAMOUNTINUSD { get; set; }
        public Int64 CHARGESETID { get; set; }

        public string CURRENCYCODE { get; set; }
        public string QUOTESOURCE { get; set; }
        public string stripePKKey { get; set; }
        public string REFERREREMAILID { get; set; }
        public string REFERRERMODEL { get; set; }
        public decimal REFERRERBALAMOUNT { get; set; }
        public decimal REFERRERBALCOUPONCOUNT { get; set; }
        public string REFERRERCOUPONCODE { get; set; }
        public string USERCOUNTRY { get; set; }

        public int LIVEUPLOADS { get; set; }
        public int LOADINGDOCKAVAILABLE { get; set; }
        public int CARGOPALLETIZED { get; set; }
        public int ORIGINALDOCUMENTSREQUIRED { get; set; }
        public int TEMPERATURECONTROLREQUIRED { get; set; }
        public int COMMERCIALPICKUPLOCATION { get; set; }
        public string SPECIALINSTRUCTIONS { get; set; }
    }

    public class MPaymentResult
    {       
        public string MODE { get; set; }
        public string STATUS { get; set; }
        public string TOKEN { get; set; }
        public string DATA { get; set; }
        public string USERID { get; set; }
        public long JOBNUMBER { get; set; }
        public long QUOTATIONID { get; set; }
        public string CURRENCY { get; set; }
        public double RECEIPTNETAMOUNT { get; set; }
    }

    public class StripeChargeResponseModel
    {
        public StripeBalanceTransactionModel balance_transaction { get; set; }
        public StripeSourceModel Source { get; set; }
    }

    public class StripeBalanceTransactionModel
    {
        public int amount { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public int fee { get; set; }
        public int net { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string exchange_rate { get; set; }
    }

    public class StripeSourceModel
    {
        public StripeCardModel three_d_secure { get; set; }
    }

    public class StripeCardModel
    {
        public string Brand { get; set; }
        public string Last4 { get; set; }
    }

    public class OnlinePaymentResponseDto
    {
        public string redirectUrl { get; set; }
        public bool isRedirect { get; set; }
    }
}
