﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.JP
{
    public class SSPDocumentsModel
    {
        public virtual long DOCID { get; set; }
        public virtual long JOBNUMBER { get; set; }
        public virtual long QUOTATIONID { get; set; }
        public virtual string SOURCE { get; set; }
        public virtual string SOURCEREFNO { get; set; }
        public virtual long SOURCEREFID { get; set; }
        public virtual string DOCUMNETTYPE { get; set; }
        public virtual string FILENAME { get; set; }
        public virtual string FILEEXTENSION { get; set; }
        public virtual long FILESIZE { get; set; }
        public virtual byte[] FILECONTENT { get; set; }
        public virtual string CREATEDBY { get; set; }
        public virtual DateTime DATECREATED { get; set; }
        public virtual DateTime DATEMODIFIED { get; set; }
        public virtual string MODIFIEDBY { get; set; }
        public virtual string OWNERORGID { get; set; }
        public virtual string OWNERLOCID { get; set; }
        public virtual string STATEID { get; set; }
        public virtual string DocumentName { get; set; }
        public virtual string DOCUMENTTEMPLATE { get; set; }
        public virtual string CONDMAND { get; set; }
        public virtual string DIRECTION { get; set; }
        public virtual string DOCDELETE { get; set; }
        public virtual string PROVIDEDDBY { get; set; }
        public virtual DateTime DOCUPLOADDATE { get; set; }
        public virtual string PROVIDERMAILID { get; set; }
        public virtual string PROVIDER { get; set; }
        public virtual long? COMMENTID { get; set; }
    }
}
