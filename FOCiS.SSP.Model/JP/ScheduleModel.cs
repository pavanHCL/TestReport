﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.JP
{
    public class ScheduleModel
    {
        public long SCHEDULEID { get; set; }
        public string CARRIERCODE { get; set; }
        public string VESSELNAME { get; set; }
        public string VOYAGE { get; set; }
        public long TRANSITTIME { get; set; }
        public DateTime ESTIMATEDARRIVALDATE { get; set; }
        public DateTime ESTIMATEDDEPARTUREDATE { get; set; }
        public string ORIGINNAME { get; set; }
        public string ORGINCODE { get; set; }
        public string DESTNAME { get; set; }
        public string DESTCODE { get; set; }
        public string ISINTTRASCH { get; set; }
        public long PORTOFDISCHARGE { get; set; }
        public long PORTOFLOADING { get; set; }
    }
}
