﻿using System.ComponentModel.DataAnnotations;
namespace FOCiS.SSP.Models.JP
{
    public class JobBookingShipmentModel
    {
        public long ContainerId { get; set; }

        public string ItemDescription { get; set; }

        [Required]
        public decimal ItemHeight { get; set; }

        public decimal ItemLength { get; set; }

        public decimal ItemWidth { get; set; }

        public long LengthUOM { get; set; }

        public string LengthUOMCode { get; set; }

        public string LengthUOMName { get; set; }

        public string PackageTypeCode { get; set; }

        public long PackageTypeId { get; set; }

        public string PackageTypeName { get; set; }

        public long Quantity { get; set; }

        public long QuotationId { get; set; }

        public long ShipmentItemId { get; set; }

        public decimal TotalCBM { get; set; }

        public decimal WeightPerPiece { get; set; }

        public decimal WeightTotal { get; set; }

        public long WeightUOM { get; set; }

        public string WeightUOMCode { get; set; }

        public string WeightUOMName { get; set; }

        public string ContainerName { get; set; }

        public string ContainerCode { get; set; }

        public long JOBSHIPMENTITEMID { get; set; }

        public double TotalWgtKG { get; set; }
        public double TotalWgtCBM { get; set; }
        public double TotalWgtTON { get; set; }
    }
}