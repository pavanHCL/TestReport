﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.JP
{
    public class PaymentTransactionDetailsModel
    {
        public long RECEIPTPAYMENTDTLSID { get; set; }
        public long RECEIPTHDRID { get; set; }
        public long JOBNUMBER { get; set; }
        public string OPJOBNUMBER { get; set; }
        public string OPERATIONALJOBNUMBER { get; set; }
        public long QUOTATIONID { get; set; }
        public string PAYMENTOPTION { get; set; }
        public long ISPAYMENTSUCESS { get; set; }
        public string PAYMENTREFNUMBER { get; set; }
        public string PAYMENTREFMESSAGE { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public string CREATEDBY { get; set; }
        public string MODIFIEDBY { get; set; }

        public string PAYMENTTYPE { get; set; }
        public string TRANSMODE { get; set; }
        public double TRANSAMOUNT { get; set; }
        public string TRANSCURRENCY { get; set; }
        public string TRANSDESCRIPTION { get; set; }
        public string TRANSBANKNAME { get; set; }
        public string TRANSBRANCHNAME { get; set; }
        public string TRANSCHEQUENO { get; set; }
        public DateTime? TRANSCHEQUEDATE { get; set; }

        public decimal BRANCHENTITYKEY { get; set; }
        public decimal ADDITIONALCHARGEAMOUNT { get; set; }
        public string ADDITIONALCHARGESTATUS { get; set; }
        public decimal ADDITIONALCHARGEAMOUNTINUSD { get; set; }

        public string PAIDCURRENCY { get; set; }
        public decimal PREFFEREDCURRENCYAMOUNT { get; set; }
        public string SITEADDRESS { get; set; }
        public string SITECITY { get; set; }
        public string DiscountCode { get; set; }
        public decimal DiscountAmount { get; set; }
    }
}
