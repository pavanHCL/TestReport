﻿using FOCiS.SSP.Models.Payment;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.JP
{   
    public  class JobBookingModel
    {
        public decimal CARGOVALUE { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public long CUSTOMERID { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public string MODIFIEDBY { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public long ORIGINPLACEID { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        [Display(Name = "CBM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TOTALCBM { get; set; }
        [Display(Name = "WEIGHT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public decimal GRANDTOTAL { get; set; }
        public int DecimalCount { get; set; }

        public IList<QuotationShipmentModel> ShipmentItems { get; set; }
        public IList<JobBookingShipmentModel> JobShipmentItems { get; set; }
        public IList<SSPDocumentsModel> DocumentDetails { get; set; }
        public IList<SSPJobDetailsModel> JobItems { get; set; }
        public IList<SSPPartyDetails>  PartyItems { get; set; }
        public IList<PaymentCharges> PaymentCharges { get; set; }
        public IList<ReceiptHeader> ReceiptHeaderDetails { get; set; }
        public IList<UserPartyMasterModel> PartiesModel { get; set; }
        public IList<ScheduleModel> ScheduleDetails { get; set; }
        public IList<IncotermsDetailsModel> IncotermDetails { get; set; }
        public IList<Compliance_Fields_Model> CopmlnceFlds { get; set; }
        public IList<HsCodeInstructionModel> HSCodeInsmodel { get; set; }
        public IList<HsCodeDutyModel> HSCodeDutymodel { get; set; }
        public CargoAvabilityModel CargoAvabilityModel { get; set; }
        //shipper  details
        public string SHADDRESSTYPE { get; set; }
        [StringLength(100, ErrorMessage = "House/Building Name cannot Exceed 100 Characters")]
        public string SHBUILDINGNAME { get; set; }
        public string SHCITYCODE { get; set; }
        public string SHCITYNAME { get; set; }      
        public string SHCLIENTID { get; set; }
        [StringLength(100, ErrorMessage = "Company name cannot Exceed 100 Characters")]
        public string SHCLIENTNAME { get; set; }
        public string SHCOUNTRYCODE { get; set; }
        public string SHCOUNTRYNAME { get; set; }
        public string SHCOUNTRYID { get; set; }
        public DateTime DATECREATED { get; set; }

        public string SHFULLADDRESS { get; set; }
        [StringLength(50, ErrorMessage = "House/Building No cannot Exceed 50 Characters")]
        public string SHHOUSENO { get; set; }
        public long SHISDEFAULTADDRESS { get; set; }
        public long SHJOBNUMBER { get; set; }
        public string SHLATITUDE { get; set; }
        public string SHLONGITUDE { get; set; }

        public long SHPARTYDETAILSID { get; set; }
        public string SHPARTYTYPE { get; set; }
        [StringLength(10, ErrorMessage = "Postal code cannot Exceed 10 Characters")]
        public string SHPINCODE { get; set; }
        public string SHSTATECODE { get; set; }

        public string SHSTATENAME { get; set; }
        [StringLength(100, ErrorMessage = "Address Line1 cannot Exceed 100 Characters")]
        public string SHSTREET1 { get; set; }
        [StringLength(100, ErrorMessage = "Address Line2 cannot Exceed 100 Characters")]  
        public string SHSTREET2 { get; set; }
        [StringLength(100, ErrorMessage = "First Name cannot Exceed 100 Characters")]  
        public string SHFIRSTNAME { get; set; }
        [StringLength(100, ErrorMessage = "Last Name cannot Exceed 100 Characters")]
        public string SHLASTNAME { get; set; }
        [StringLength(12, ErrorMessage = "Enter Valid Contact No")]
        public string SHPHONE { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string SHEMAILID { get; set; }
        public string SHSALUTATION { get; set; }
        public int SHISSAVEDORNEWADDRESS { get; set; }      

        //Consignee Details
        public string CONADDRESSTYPE { get; set; }
        [StringLength(100, ErrorMessage = "House/Building Name cannot Exceed 100 Characters")]
        public string CONBUILDINGNAME { get; set; }
        public string CONCITYCODE { get; set; }           
        public string CONCITYNAME { get; set; }
        public string CONCLIENTID { get; set; }
        public string CONCLIENTNAME { get; set; }
        public string CONCOUNTRYCODE { get; set; }
        public string CONCOUNTRYNAME { get; set; }
        public string CONCOUNTRYID { get; set; }
        //public DateTime DATECREATED { get; set; }

        public string CONFULLADDRESS { get; set; }
        [StringLength(50, ErrorMessage = "House/Building No cannot Exceed 50 Characters")]        
        public string CONHOUSENO { get; set; }
        public long CONISDEFAULTADDRESS { get; set; }
        public long CONJOBNUMBER { get; set; }
        public string CONLATITUDE { get; set; }
        public string CONLONGITUDE { get; set; }

        public long CONPARTYDETAILSID { get; set; }
        public string CONPARTYTYPE { get; set; }
        [StringLength(10, ErrorMessage = "Postal code cannot Exceed 10 Characters")]
        public string CONPINCODE { get; set; }
        public string CONSTATECODE { get; set; }

        public string CONSTATENAME { get; set; }
        [StringLength(100, ErrorMessage = "Address Line1 cannot Exceed 100 Characters")]            
        public string CONSTREET1 { get; set; }
        [StringLength(100, ErrorMessage = "Address Line2 cannot Exceed 100 Characters")]
        public string CONSTREET2 { get; set; }
        [StringLength(100, ErrorMessage = "First Name cannot Exceed 100 Characters")]
        public string CONFIRSTNAME { get; set; }
        [StringLength(100, ErrorMessage = "Last Name cannot Exceed 100 Characters")]
        public string CONLASTNAME { get; set; }
        [StringLength(12, ErrorMessage = "Enter Valid Contact No")]
        public string CONPHONE { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string CONEMAILID { get; set; }
        public string CONSALUTATION { get; set; }
        public int CONISSAVEDORNEWADDRESS { get; set; }            

        //JobDetails
        public virtual long JJOBNUMBER { get; set; }
        public virtual string JOPERATIONALJOBNUMBER { get; set; }
        public virtual long JQUOTATIONID { get; set; }
        public virtual string JQUOTATIONNUMBER { get; set; }
        public virtual string JINCOTERMCODE { get; set; }
        public virtual string JINCOTERMDESCRIPTION { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
   
        public virtual DateTime JCARGOAVAILABLEFROM { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public virtual DateTime JCARGODELIVERYBY { get; set; }
        //public virtual string JORIGINCUSTOMSCLEARANCEBY { get; set; }
        //public virtual string JDESTINATIONCUSTOMSCLEARANCEBY { get; set; }
        public virtual int JDOCUMENTSREADY { get; set; }
        public virtual string JSLINUMBER { get; set; }
        public virtual string JCONSIGNMENTID { get; set; }
        public virtual DateTime JDATECREATED { get; set; }
        public virtual DateTime JDATEMODIFIED { get; set; }
        public virtual string JMODIFIEDBY { get; set; }
        public virtual string JSTATEID { get; set; }
        public string SPECIALINSTRUCTION { get; set; }
        public bool LIVEUPLOADS { get; set; }
        public bool LOADINGDOCKAVAILABLE { get; set; }
        public bool CARGOPALLETIZED { get; set; }
        public bool ORIGINALDOCUMENTSREQUIRED { get; set; }
        public bool TEMPERATURECONTROLREQUIRED { get; set; }
        public bool COMMERCIALPICKUPLOCATION { get; set; }
        public int PartyCount { get; set; }

        public decimal OCOUNTRYID { get; set; }
        public decimal DCOUNTRYID { get; set; }

        public string ORIGINZIPCODE { get; set; }
        public string DESTINATIONZIPCODE { get; set; }
        public string ORIGINSUBDIVISIONCODE { get; set; }
        public string DESTINATIONSUBDIVISIONCODE { get; set; }

        public long SCHEDULEID { get; set; }
        public string UPERSONALID { get; set; }
        public string UPERSONALCOUNTRYID { get; set; }
        public string CONUPERSONALID { get; set; }
        public string CONUPERSONALCOUNTRYID { get; set; }
        public string CREATEDBY { get; set; }
        public string COLLABRATEDCONSIGNEE { get; set; }
        public string COLLABRATEDSHIPPER { get; set; }
        public int CHARGESETID { get; set; }
        public string HAZARDOUSGOODSTYPE { get; set; }
        public string TRANSITTIME { get; set; }
        public string QUOTETYPE { get; set; }
        public string AMAZONFCCODE { get; set; }
    }
    public class JobBookingHidden
    {
        public string JINCOTERMCODE { get; set; }
        public string JINCOTERMDESCRIPTION { get; set; }
        public DateTime JCARGODELIVERYBY { get; set; }
        public DateTime JCARGOAVAILABLEFROM { get; set; }
        public string HDNCONTINETOPAYMENT { get; set; }
        public string TXTFIELDSVALUE { get; set; }
        public string TXTFIELDSNAME { get; set; }
        public string SHCLIENTID { get; set; }
        public string CONCLIENTID { get; set; }
        public string HSCode { get; set; }
        public string DesHSCode { get; set; }
        public string SUBMITTOCUSTOMER { get; set; }
        public string CREATEDBY { get; set; }
        public string PARTY { get; set; }
        public string MandatoryDocs { get; set; }
    }
    public class CargoAvabilityModel
    {
        public string AIRAVAILABILITY { get; set; }
        public string AIRDELIVERY { get; set; }
        public string OCEANAVAILABILITY { get; set; }
        public string OCEANDELIVERY { get; set; }
    }

    public class QuotationShipmentEntity
    {
        public long CONTAINERID { get; set; }
        public string ITEMDESCRIPTION { get; set; }
        public decimal ITEMHEIGHT { get; set; }
        public decimal ITEMLENGTH { get; set; }
        public decimal ITEMWIDTH { get; set; }
        public long LENGTHUOM { get; set; }
        public string LENGTHUOMCODE { get; set; }
        public string LENGTHUOMNAME { get; set; }
        public string PACKAGETYPECODE { get; set; }
        public long PACKAGETYPEID { get; set; }
        public string PACKAGETYPENAME { get; set; }
        public long QUANTITY { get; set; }
        public long QUOTATIONID { get; set; }
        public long SHIPMENTITEMID { get; set; }
        public decimal WEIGHTPERPIECE { get; set; }
        public decimal WEIGHTTOTAL { get; set; }
        public long WEIGHTUOM { get; set; }
        public string WEIGHTUOMCODE { get; set; }
        public string WEIGHTUOMNAME { get; set; }
    }

    public class QuotationTermsConditionEntity
    {
        public object DESCRIPTION { get; set; }
        public long QMTERMSANDCONDITIONID { get; set; }
        public long QUOTATIONID { get; set; }
        public long TERMSANDCONDITIONID { get; set; }
    }

    public class SSPPartyDetails
    {
        public string ADDRESSTYPE { get; set; }
        public string BUILDINGNAME { get; set; }
        public string CITYCODE { get; set; }
        public string CITYNAME { get; set; }
        public string CLIENTID { get; set; }
        public string CLIENTNAME { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public string FULLADDRESS { get; set; }
        public string HOUSENO { get; set; }
        public long ISDEFAULTADDRESS { get; set; }
        public long JOBNUMBER { get; set; }
        public string LATITUDE { get; set; }
        public string LONGITUDE { get; set; }
        public string MODIFIEDBY { get; set; }
        public long PARTYDETAILSID { get; set; }
        public string PARTYTYPE { get; set; }
        public string PINCODE { get; set; }
        public string STATECODE { get; set; }
        public string STATEID { get; set; }
        public string STATENAME { get; set; }
        public string STREET1 { get; set; }
        public string STREET2 { get; set; }
        public string CREATEDBY { get; set; }
        public long COUNTRYID { get; set; }
    }

    public class SSPJobDetailsModel
    {
        public virtual long JOBNUMBER { get; set; }
        public virtual string OPERATIONALJOBNUMBER { get; set; }
        public virtual long QUOTATIONID { get; set; }
        public virtual string QUOTATIONNUMBER { get; set; }
        public virtual string INCOTERMCODE { get; set; }
        public virtual string INCOTERMDESCRIPTION { get; set; }
        public virtual DateTime CARGOAVAILABLEFROM { get; set; }
        public virtual DateTime CARGODELIVERYBY { get; set; }
        //public virtual string ORIGINCUSTOMSCLEARANCEBY { get; set; }
        //public virtual string DESTINATIONCUSTOMSCLEARANCEBY { get; set; }
        public virtual int DOCUMENTSREADY { get; set; }
        public virtual string SLINUMBER { get; set; }
        public virtual string CONSIGNMENTID { get; set; }
        public virtual DateTime DATECREATED { get; set; }
        public virtual DateTime DATEMODIFIED { get; set; }
        public virtual string MODIFIEDBY { get; set; }
        public virtual string STATEID { get; set; }
        public virtual decimal JOBGRANDTOTAL { get; set; }
        public virtual string COLLABRATEDCONSIGNEE { get; set; }
        public virtual string COLLABRATEDSHIPPER { get; set; }
        public virtual string SHIPPERPRIVILEGE { get; set; }
        public virtual string CONSIGNEEPRIVILEGE { get; set; }
        public virtual decimal ADDITIONALCHARGEAMOUNT { get; set; }
        public virtual string ADDITIONALCHARGESTATUS { get; set; }
        public bool LIVEUPLOADS { get; set; }
        public bool LOADINGDOCKAVAILABLE { get; set; }
        public bool CARGOPALLETIZED { get; set; }
        public bool ORIGINALDOCUMENTSREQUIRED { get; set; }
        public bool TEMPERATURECONTROLREQUIRED { get; set; }
        public bool COMMERCIALPICKUPLOCATION { get; set; }


    }

    public class SSPClientMasterModel
    {
        public string ADDRESSTYPE { get; set; }
        public string BUILDINGNAME { get; set; }
        public string CITYCODE { get; set; }
        public string CITYNAME { get; set; }
        public string CLIENTID { get; set; }
        public string CLIENTNAME { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public string FULLADDRESS { get; set; }
        public string HOUSENO { get; set; }
        public long ISDEFAULTADDRESS { get; set; }
        public long JOBNUMBER { get; set; }
        public string LATITUDE { get; set; }
        public string LONGITUDE { get; set; }
        public string MODIFIEDBY { get; set; }
        public long PARTYDETAILSID { get; set; }
        public string PARTYTYPE { get; set; }
        public string PINCODE { get; set; }
        public string STATECODE { get; set; }
        public string STATEID { get; set; }
        public string STATENAME { get; set; }
        public string STREET1 { get; set; }
        public string STREET2 { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PHONE { get; set; }
        public string EMAILID { get; set; }
        public string SALUTATION { get; set; }
        public long SSPCLIENTID { get; set; }
        public string CONTROLCLIENTID { get; set; }
        public string CREATEDBY { get; set; }
    }

    public class JobBookingListModel{

        public JobBookingListModel()
        {
            ShipmentItems = new List<QuotationShipmentPreviewModel>();
            QuotationCharges = new List<QuotationChargePreviewModel>();
            JobItems = new List<SSPJobDetailsModel>();
            ReceiptHeaderDetails = new List<ReceiptHeader>();
        }
        [Display(Name = "CARGOVALUE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal CargoValue { get; set; }

        [Display(Name = "CARGOVALUECURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string CargoValueCurrencyId { get; set; }

        [Display(Name = "CHARGEABLEVOLUME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal ChargeableVolume { get; set; }

       [Display(Name = "ChargeableWeight", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal ChargeableWeight { get; set; }

        [Display(Name = "CUSTOMERID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string CustomerId { get; set; }

        [Display(Name = "DATEMODIFIED", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateModified { get; set; }

        [Display(Name = "DATEOFENQUIRY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateofEnquiry { get; set; }

        [Display(Name = "DATEOFSHIPMENT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateofShipment { get; set; }

        [Display(Name = "DATEOFVALIDITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateOfValidity { get; set; }

        [Display(Name = "DENSITYRATIO", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public double DensityRatio { get; set; }

        [Display(Name = "DESTINATIONPLACECODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPlaceCode { get; set; }

        [Display(Name = "DESTINATIONPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPlaceName { get; set; }

        [Display(Name = "DESTINATIONPORTCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPortCode { get; set; }

        [Display(Name = "DESTINATIONPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPortName { get; set; }

        [Display(Name = "GRANDTOTAL", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal GrandTotal { get; set; }

        [Display(Name = "INSUREDVALUE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long InsuredValue { get; set; }

        [Display(Name = "INSUREDVALUECURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string InsuredValueCurrencyId { get; set; }

        [Display(Name = "ISHAZARDOUSCARGO", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public long IsHazardousCargo { get; set; }

        [Display(Name = "MODIFIEDBY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ModifiedBy { get; set; }

        [Display(Name = "MOVEMENTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string MovementTypeName { get; set; }

        [Display(Name = "ORIGINPLACECODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPlaceCode { get; set; }

        [Display(Name = "ORIGINPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPlaceName { get; set; }

        [Display(Name = "ORIGINPORTCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPortCode { get; set; }

        [Display(Name = "ORIGINPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPortName { get; set; }

        [Display(Name = "PREFERREDCURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string PreferredCurrencyId { get; set; }

        [Display(Name = "PRODUCTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ProductName { get; set; }

        [Display(Name = "PRODUCTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ProductTypeName { get; set; }

        [Display(Name = "QUOTATIONID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long QuotationId { get; set; }

        [Display(Name = "QUOTATIONNUMBER", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string QuotationNumber { get; set; }

        [Display(Name = "SHIPMENTDESCRIPTION", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public string ShipmentDescription { get; set; }

        [Display(Name = "STATEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string StateId { get; set; }

        [Display(Name = "TOTALCBM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TotalCBM { get; set; }

        [Display(Name = "TOTALGROSSWEIGHT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TotalGrossWeight { get; set; }

        [Display(Name = "TOTALQUANTITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long TotalQuantity { get; set; }

       [Display(Name = "VolumetricWeight", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal VolumetricWeight { get; set; }
        public long JOBNUMBER { get; set; }
        public string OPERATIONALJOBNUMBER { get; set; }
        public string JSTATEID { get; set; }
        
        //public IList<QuotationShipmentPreviewModel> ShipmentItems { get; set; }
        public IEnumerable<QuotationShipmentPreviewModel> ShipmentItems { get; set; }
        public IEnumerable<QuotationChargePreviewModel> QuotationCharges { get; set; }
        public IEnumerable<SSPJobDetailsModel> JobItems { get; set; }
        public IEnumerable<ReceiptHeader> ReceiptHeaderDetails { get; set; }    
    }

    public class JobBookingDemoModel
    {
        public decimal CARGOVALUE { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public long CUSTOMERID { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public string MODIFIEDBY { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public long ORIGINPLACEID { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public decimal GRANDTOTAL { get; set; }

        public IList<QuotationShipmentModel> ShipmentItems { get; set; }
        public IList<SSPDocumentsModel> DocumentDetails { get; set; }
        public IList<SSPJobDetailsModel> JobItems { get; set; }
        public IList<SSPPartyDetails> PartyItems { get; set; }
        public IList<PaymentCharges> PaymentCharges { get; set; }

        //shipper  details
        public string SHADDRESSTYPE { get; set; }
        public string SHBUILDINGNAME { get; set; }
        public string SHCITYCODE { get; set; }
        public string SHCITYNAME { get; set; }
        public string SHCLIENTID { get; set; }

        public string SHCLIENTNAME { get; set; }
        public string SHCOUNTRYCODE { get; set; }
        public string SHCOUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }

        public string SHFULLADDRESS { get; set; }
        public string SHHOUSENO { get; set; }
        public long SHISDEFAULTADDRESS { get; set; }
        public long SHJOBNUMBER { get; set; }
        public string SHLATITUDE { get; set; }
        public string SHLONGITUDE { get; set; }

        public long SHPARTYDETAILSID { get; set; }
        public string SHPARTYTYPE { get; set; }
        public string SHPINCODE { get; set; }
        public string SHSTATECODE { get; set; }

        public string SHSTATENAME { get; set; }
        public string SHSTREET1 { get; set; }
        public string SHSTREET2 { get; set; }

        public string SHFIRSTNAME { get; set; }
        public string SHLASTNAME { get; set; }
        public string SHPHONE { get; set; }
        public string SHEMAILID { get; set; }
        public string SHSALUTATION { get; set; }
        public int SHISSAVEDORNEWADDRESS { get; set; }

        //Consignee Details
        public string CONADDRESSTYPE { get; set; }
        public string CONBUILDINGNAME { get; set; }
        public string CONCITYCODE { get; set; }
        public string CONCITYNAME { get; set; }
        public string CONCLIENTID { get; set; }
        public string CONCLIENTNAME { get; set; }
        public string CONCOUNTRYCODE { get; set; }
        public string CONCOUNTRYNAME { get; set; }
        //public DateTime DATECREATED { get; set; }

        public string CONFULLADDRESS { get; set; }
        public string CONHOUSENO { get; set; }
        public long CONISDEFAULTADDRESS { get; set; }
        public long CONJOBNUMBER { get; set; }
        public string CONLATITUDE { get; set; }
        public string CONLONGITUDE { get; set; }
        public long CONPARTYDETAILSID { get; set; }
        public string CONPARTYTYPE { get; set; }
        public string CONPINCODE { get; set; }
        public string CONSTATECODE { get; set; }
        public string CONSTATENAME { get; set; }
        public string CONSTREET1 { get; set; }
        public string CONSTREET2 { get; set; }

        public string CONFIRSTNAME { get; set; }
        public string CONLASTNAME { get; set; }
        public string CONPHONE { get; set; }
        public string CONEMAILID { get; set; }
        public string CONSALUTATION { get; set; }
        public int CONISSAVEDORNEWADDRESS { get; set; }

        //JobDetails
        public virtual long JJOBNUMBER { get; set; }
        public virtual string JOPERATIONALJOBNUMBER { get; set; }
        public virtual long JQUOTATIONID { get; set; }
        public virtual string JQUOTATIONNUMBER { get; set; }
        public virtual string JINCOTERMCODE { get; set; }
        public virtual string JINCOTERMDESCRIPTION { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]

        public virtual DateTime JCARGOAVAILABLEFROM { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public virtual DateTime JCARGODELIVERYBY { get; set; }
        //public virtual string JORIGINCUSTOMSCLEARANCEBY { get; set; }
        //public virtual string JDESTINATIONCUSTOMSCLEARANCEBY { get; set; }
        public virtual int JDOCUMENTSREADY { get; set; }
        public virtual string JSLINUMBER { get; set; }
        public virtual string JCONSIGNMENTID { get; set; }
        public virtual DateTime JDATECREATED { get; set; }
        public virtual DateTime JDATEMODIFIED { get; set; }
        public virtual string JMODIFIEDBY { get; set; }
        public virtual string JSTATEID { get; set; }
    }

    public class ReceiptHeader
    {
        public long JOBNUMBER { get; set; }
        public decimal RECEIPTAMOUNT { get; set; }
        public decimal RECEIPTTAX { get; set; }
        public decimal RECEIPTNETAMOUNT { get; set; }
        public decimal DISCOUNTAMOUNT { get; set; }
    }

    public class IncotermsDetailsModel
    {
        public long TERMSOFTRADEID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
    }
    public class Compliance_Fields_Model
    {
        public Int64 FIELDID { get; set; }
        public string FIELDINSTRUCTIONID { get; set; }
        public string FIELDDESCRIPTION { get; set; }
        public string FIELDTYPE { get; set; }
        public string FIELDLENGTH { get; set; }
        public string JOBNUMBER { get; set; }
        public string VALUE { get; set; }
        public string DIRECTION { get; set; }
    }
}
