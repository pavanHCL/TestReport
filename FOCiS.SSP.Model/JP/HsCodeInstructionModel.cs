﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.JP
{
    public class HsCodeInstructionModel
    {
        

        //public string InstrucationType { get; set; }
        //public string INSTRUCATION { get; set; }
        public string HSCODE { get; set; }
        //public string DUTYTYPE { get; set; }
        //public string DUTYRATEPERCENTAGE { get; set; }
    }

    public class HsCodeDutyModel
    {
        public string HSCODE { get; set; }
        public string DUTYTYPE { get; set; }
        public string DUTYRATEPERCENTAGE { get; set; }
    }

}
