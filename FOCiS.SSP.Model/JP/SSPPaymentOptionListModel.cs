﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.JP
{
    public class SSPPaymentOptionListModel
    {   
        public Int64 COUNTRYID { get; set; }
        public string ISCREDITCARD { get; set; }
        public string ISBITCOIN { get; set; }
        public string ISBUSINESSCREDIT { get; set; }
        public string ISPAYATBRANCH { get; set; }
        public string ISWIRETRANSFER { get; set; }
        public string ISCASH { get; set; }
        public string ISCHEQUE { get; set; }
        public string ISDD { get; set; }
        public string ISALIPAY { get; set; } 
    }
}
