﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.Tracking
{
    public class TrackingServiceModel
    {
        public string ConsignmentId { get; set; }
        public DateTime JobConfirmed { get; set; }
        public string JobConfirmedTime { get; set; }
        public string JobConfirmedStatus { get; set; }
        public DateTime ReceiptOfGoods { get; set; }
        public string ReceiptOfGoodsTime { get; set; }
        public string ReceiptOfGoodsStatus { get; set; }
        public string ReceiptOfGoodsLocation { get; set; }
        public DateTime Departed { get; set; }
        public string DepartedTime { get; set; }
        public string DepartedStatus { get; set; }
        public string DepartedLocation { get; set; }
        public DateTime Arrival { get; set; }
        public string ArrivalTime { get; set; }
        public string ArrivalStatus { get; set; }
        public string ArrivalLocation { get; set; }
        public DateTime Delivered { get; set; }
        public string DeliveredTime { get; set; }
        public string DeliveredStatus { get; set; }
        public string DeliveredLocation { get; set; }

        public string Consignee { get; set; }
        public string Shipper { get; set; }
        public string ShipperReferences { get; set; }
        public string ConsigneeReferences { get; set; }
        public string ServiceLevel { get; set; }

        ////Status Of running track
        public string DepartedTrackStatus { get; set; }
        public string ArrivalTrackStatus { get; set; }
        public string DeliveredTrackStatus { get; set; }
        public int CurrentStatusIcon { get; set; }

        ////Summary Details Objects
        //public string Origin { get; set; }
        //public string Destination { get; set; }
        //public string JobMode { get; set; }
        //public string CargoDescription { get; set; }
        //public string ShipperId { get; set; }
       
        
       
        //public string POD { get; set; }
       
        //public DateTime JobBookingDate { get; set; }
        //public string JobStatus { get; set; }
        //public string OtherShipmentDetails { get; set; }
        //public float CubicVolume { get; set; }
        //public int Pieces { get; set; }
        //public float ActualWeight { get; set; }
        //public float TotalCWgt { get; set; }

        
    }

    
}
