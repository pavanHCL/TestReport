﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.Tracking
{
    public class TrackingPartyDetailsModel
    {
        public string ClientName { get; set; }
        public string EMAILID { get; set; }
        public string PHONE { get; set; }
        public string PARTYTYPE { get; set; }
        public DateTime JobInitiated { get; set; }
        public DateTime PaymentCompleted { get; set; }
    }
}
