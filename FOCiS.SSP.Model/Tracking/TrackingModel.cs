﻿using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Models.QM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.Tracking
{
    public class TrackingModel
    {
        public string ORIGINPLACECODE { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public long ORIGINPLACEID { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string SALUTATION { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PHONE { get; set; }
        public string EMAILID { get; set; }
        public string DCOUNTRYCODE { get; set; }

        public long JOBNUMBER { get; set; }
        public string OPERATIONALJOBNUMBER { get; set; }
        public long QUOTATIONID { get; set; }
        public string CLIENTID { get; set; }
        public string CLIENTNAME { get; set; }
        public string PARTYTYPE { get; set; }
        public string ADDRESSTYPE { get; set; }
        public string HOUSENO { get; set; }
        public string BUILDINGNAME { get; set; }
        public string STREET1 { get; set; }
        public string STREET2 { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public string CITYCODE { get; set; }
        public string CITYNAME { get; set; }
        public string STATECODE { get; set; }
        public string STATENAME { get; set; }
        public string PINCODE { get; set; }
        public string FULLADDRESS { get; set; }
        public long ISDEFAULTADDRESS { get; set; }
        public string LATITUDE { get; set; }
        public string LONGITUDE { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public string PARTYSTATUS { get; set; }


        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public string INCOTERMCODE { get; set; }
        public string INCOTERMDESCRIPTION { get; set; }
        public DateTime CARGOAVAILABLEFROM { get; set; }
        public string ORIGINCUSTOMSCLEARANCEBY { get; set; }
        public string DESTINATIONCUSTOMSCLEARANCEBY { get; set; }
        public string SLINUMBER { get; set; }
        public string CONSIGNMENTID { get; set; }
        public string JOBSTATUS { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public Int64 TOTALQUANTITY { get; set; }
        public decimal GRANDTOTAL { get; set; }
        public string CREATEDBY { get; set; }
        public string QUOTESTATUS { get; set; }
        public long CUSTOMERID { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public decimal OCOUNTRYID { get; set; }
        public string OCOUNTRYCODE { get; set; }
        public string OCOUNTRYNAME { get; set; }
        public decimal DCOUNTRYID { get; set; }
        public DateTime DATECREATED { get; set; }
        public string PACKAGETYPENAME { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public string ORIGINSUBDIVISIONCODE { get; set; }
        public string DESTINATIONSUBDIVISIONCODE { get; set; }

        public IEnumerable<PackageDetailsModel> PackageDetailsItems { get; set; }
        public IEnumerable<TrackModel> TrackItems { get; set; }
        public IEnumerable<TrackModel> TrackOrderByItems { get; set; }

        public DateTime JobInitiated { get; set; }
        public DateTime PaymentCompleted { get; set; }

        public string ADDITIONALCHARGESTATUS { get; set; }
        
    }
    public class PackageDetailsModel
    {
        public Int64 QUOTATIONID { get; set; }
        public Int64 JobNumber { get; set; }
        public string PACKAGETYPENAME { get; set; }
        public Int64 QUANTITY { get; set; }
        public decimal WEIGHTPERPIECE { get; set; }
        public decimal ITEMLENGTH { get; set; }
        public decimal ITEMHEIGHT { get; set; }
        public decimal ITEMWIDTH { get; set; }
        public string CONTAINERNAME { get; set; }
        public string LENGTHUOMNAME { get; set; }
    }

    public class ListTrackingModel
    {
        public IEnumerable<TrackingModel> TrackingItems { get; set; }
       // public IEnumerable<TrackingServiceModel> TrackingServiceItems { get; set; }
        public IEnumerable<TrackingPartyDetailsModel> TrackingPartyItems { get; set; }
        public IEnumerable<PackageDetailsModel> PackageDetailsItems { get; set; }
        public IEnumerable<ShpimentEventCommentsModel> shipmentCommentsItems { get; set; }
        public IEnumerable<TrackModel> TrackItems { get; set; }
        public IEnumerable<SSPDocumentsModel> DocumentDetails { get; set; }
        public IEnumerable<TrackStatusModelCount> TrackStatusModelCount { get; set; } 
        
    }

    public class ListTrackingModelForMB
    {
        public IEnumerable<TrackingModel> TrackingItems { get; set; }
        public IEnumerable<ShpimentEventCommentsModel> shipmentCommentsItems { get; set; }
        public IEnumerable<TrackingPartyDetailsModel> TrackingPartyItems { get; set; }
        public IEnumerable<TrackStatusModelCountMB> TrackStatusModelCount { get; set; }
        //public IEnumerable<SSPDocumentsModel> DocumentDetails { get; set; }
    }

    public class ShpimentEventCommentsModel
    {
        public decimal Jobnumber { get; set; }
        public string EVENTNAME { get; set; }
        public string COMMENTS { get; set; }
        public DateTime COMMENTEDDATE { get; set; }
        public string COMMENTEDBY { get; set; }
    }

    public class TrackModel
    {

        public string ConsignmentId { get; set; }
        public string EVENTCODE { get; set; }
        public string EVENTDATELOCAL { get; set; }
        public string ESTDATELOCAL { get; set; }
        public string LOCATIONINFO { get; set; }
        public string EVENTSEGMENTCOLOR { get; set; }
        public string CURRENTEVENTORDER { get; set; }
    }

    public class TrackStatusModelCount
    {
        public decimal TrackCount { get; set; }
        public string Trackstatus { get; set; }

    }

    public class TrackStatusModelCountMB
    {
        public decimal ALLTrack { get; set; }
        public decimal ACTIVETRACK { get; set; }
        public decimal COMPLETEDTRACK { get; set; }

    }
}
