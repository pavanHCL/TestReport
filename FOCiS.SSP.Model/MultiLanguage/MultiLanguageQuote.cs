﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.MultiLanguage
{

    public class Movementtype
    {
        public string movementid { get; set; }
        public string movementtype { get; set; }
        public string type { get; set; }
    }

    public class Packagetype
    {
        public string packagetypeid { get; set; }
        public string packagetype { get; set; }
        public string code { get; set; }
    }

    public class Containertype
    {
        public string containertypeid { get; set; }
        public string containertype { get; set; }
        public string code { get; set; }
    }

    public class Quotation
    {
        public List<Movementtype> movementtypeocean { get; set; }
        public List<Movementtype> movementtypeair { get; set; }
        public List<Packagetype> packagetype { get; set; }
        public List<Containertype> containertype { get; set; }
    }

    public class RootObject
    {
        public Quotation Quotation { get; set; }
    }
}
