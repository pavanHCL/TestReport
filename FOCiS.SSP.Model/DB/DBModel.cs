﻿using FOCiS.SSP.Models.QM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.DB
{
    public class DBModel
    {
        public decimal BOOKINGSDONE { get; set; }
        public decimal TOTALVOLUME { get; set; }
        public decimal CREDITLIMIT { get; set; }
        public decimal TotalSpent { get; set; }
        public string CURRENCYID { get; set; }
    }  
}
