﻿using FOCiS.Dictionary.App_GlobalResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.QM
{
    public class QuotationModel
    {
        public QuotationModel()
        {
            this.MovementTypeId = MovementTypeEnum.DoortoDoor;
            this.ProductId = ProductEnum.OceanFreight;
            this.PackageTypes = PackageTypeEnum.LCL;
            this.DensityRatio = this.ProductId == ProductEnum.AirFreight ? 166.667m : 1000m;
            this.PalletId = PalletEnum.Myself;
            this.LabellingId = PalletEnum.Myself;
            this.PartyType = PartyTypeEnum.EXPORTING;
            //this.PreferredCurrencyId = "USD";
        }

        public decimal? CargoValue { get; set; }

        [Display(Name = "Cargovalue", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "Required")]
        //[Range(1, 9999999999)]
        public string CurrencyMode { get; set; }

        [Display(Name = "Cargovaluecurrency", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string CargoValueCurrencyId { get; set; }

        [Display(Name = "WeightinTON", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal? ChargeableVolume { get; set; }

        [Display(Name = "ChargeableWeight", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal? ChargeableWeight { get; set; }

        [Display(Name = "CUSTOMERID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long CustomerId { get; set; }

        [Display(Name = "DATEMODIFIED", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateModified { get; set; }

        [Display(Name = "DATEOFENQUIRY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateofEnquiry { get; set; }

        [Display(Name = "DATEOFSHIPMENT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateOfshipment { get; set; }

        [Display(Name = "DATEOFVALIDITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateOfValidity { get; set; }

        [Display(Name = "DENSITYRATIO", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal DensityRatio { get; set; }

        public string DestinationPlaceCode { get; set; }

        [Display(Name = "DESTINATIONPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long? DestinationPlaceId { get; set; }

        [Display(Name = "DESTINATIONPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPlaceName { get; set; }

        [Display(Name = "DESTINATIONPORTCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPortCode { get; set; }

        [Display(Name = "DESTINATIONPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long? DestinationPortid { get; set; }

        [Display(Name = "DESTINATIONPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPortName { get; set; }

        public decimal GrandTotal { get; set; }

        [Display(Name = "INSUREDVALUE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long InsuredValue { get; set; }

        [Display(Name = "INSUREDVALUECURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string InsuredvalueCurrencyId { get; set; }

        [Display(Name = "Hazardousmaterial", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool IsHazardousCargo { get; set; }

        [Display(Name = "ISHOUSEHOLDGOODS", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool IsHouseholdGoods { get; set; }

        [Display(Name = "ISEVENTCARGO", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool IsEventCargo { get; set; }

        [Display(Name = "GUESTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string GuestName { get; set; }

        [Display(Name = "GUESTLASTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string GuestLastName { get; set; }

        [Display(Name = "GUESTCOMPANY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string GuestCompany { get; set; }

        [Display(Name = "GUESTEMAIL", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string GuestEmail { get; set; }

        public string GuestCountryName { get; set; }

        public int NOTIFICATIONSUBSCRIPTION { get; set; }
        public string GUESTCOUNTRYCODE { get; set; }
        public string SHIPMENTPROCESS { get; set; }
        public string USEOFQUOTE { get; set; }

        [Display(Name = "ISINSURANCEREQUIRED", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool IsInsuranceRequired { get; set; }

        [Display(Name = "MODIFIEDBY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ModifiedBy { get; set; }

        [Display(Name = "MOVEMENTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public Nullable<MovementTypeEnum> MovementTypeId { get; set; }

        [Display(Name = "MOVEMENTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string MovementTypeName { get; set; }

        public string LanguageMovementTypeName { get; set; }

        [Display(Name = "ORIGINPLACECODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPlaceCode { get; set; }

        [Display(Name = "ORIGINPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long OriginPlaceId { get; set; }

        public string OriginPlaceName { get; set; }

        [Display(Name = "ORIGINPORTCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPortCode { get; set; }

        [Display(Name = "ORIGINPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long? OriginPortId { get; set; }

        public string OriginPortName { get; set; }

        [Display(Name = "PREFERREDCURRENCYID", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public string PreferredCurrencyId { get; set; }
        [Display(Name = "INCOTERMID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string INCOTERMID { get; set; }
        public string INCOTERMDESC { get; set; }

        public string GoogleTaxonamyCategory { get; set; }

        public int PreferredDecimal { get; set; }

        public int CargoDecimal { get; set; }

        public ProductEnum ProductId { get; set; }

        public PalletEnum PalletId { get; set; }
        public PalletEnum LabellingId { get; set; }

        public string PALLETIZINGBY { get; set; }
        public string LABELLINGBY { get; set; }

        [Display(Name = "Amazon Supplier ID")]
        [StringLength(25)]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string AmazonSupplierID { get; set; }


        public string QUOTETYPE { get; set; }

        [Display(Name = "PRODUCTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ProductName { get; set; }

        public long ProductTypeId { get; set; }

        [Display(Name = "PRODUCTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ProductTypeName { get; set; }

        public long QuotationId { get; set; }

        [Display(Name = "QUOTATIONNUMBER", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string QuotationNumber { get; set; }


        public string ShipmentDescription { get; set; }

        [Display(Name = "ShipmentDescriptionId", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [StringLength(1500)]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ShipmentDescriptionId { get; set; }

        [Display(Name = "STATEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string StateId { get; set; }

        [Display(Name = "CBM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal? TotalCbm { get; set; }

        [Display(Name = "WeightinKgs", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal? TotalGrossWeight { get; set; }

        public long TotalQuantity { get; set; }

        [Display(Name = "VolumetricWeight", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal? VolumetricWeight { get; set; }

        public IList<QuotationShipmentModel> ShipmentItems { get; set; }
        public IList<QuotationTemplateModel> QuotationTemplates { get; set; }

        [Display(Name = "ShipmentDescriptionId", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public int? NoOfItems { get; set; }

        public long HasMissingCharges { get; set; }

        public string OriginsubdivisionCode { get; set; }

        public string DestinationsubdivisionCode { get; set; }

        public decimal OCountryId { get; set; }

        public decimal OriginCountryId { get; set; }

        public decimal DestinationCountryId { get; set; }

        [Display(Name = "ORIGINCOUNTRYNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string OriginCountryName { get; set; }

        [Display(Name = "DESTINATIONCOUNTRYNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string DestinationCountryName { get; set; }

        [Display(Name = "DESTINATIONSTATENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string DestinationStateName { get; set; }
        public decimal DestinationStateId { get; set; }

        public string AMAZONFCCODE { get; set; }

        [Display(Name = "AMAZONPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string AmazonPlaceId { get; set; }

        [Display(Name = "AMAZONPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string AmazonPlaceName { get; set; }

        public decimal DCountryId { get; set; }

        public long OPortCountryId { get; set; }

        public long DPortCountryId { get; set; }
        [Display(Name = "OriginZipCode", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Required")]
        public string OriginZipCode { get; set; }
        [Display(Name = "DestinationZipCode", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Required")]
        public string DestinationZipCode { get; set; }

        public string UserName { get; set; }

        public string OriginZipCodeEnable { get; set; }

        public string DestinationZipCodeEnable { get; set; }

        public string OCountryCode { get; set; }

        public string DCountryCode { get; set; }

        public string OCountryName { get; set; }

        public string DCountryName { get; set; }

        public string HazardousGoodsType { get; set; }

        public long IsCustomRequired { get; set; }

        public string CustomsDisclaimr { get; set; }

        public int IsOversizeCargo { get; set; }

        [Display(Name = "CREATEDBY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string CREATEDBY { get; set; }

        public string QuoteSource { get; set; }

        public int IsProhibitedCargo { get; set; }

        public int DESCRIPTIONISVALID { get; set; }

        public bool IsOfferApplicable { get; set; }

        public string OfferCode { get; set; }

        public string RequestType { get; set; }

        public string alternateSearchOrigin { get; set; }

        public string alternateSearchDestination { get; set; }

        public string GSSCReason { get; set; }

        public PackageTypeEnum PackageTypes { get; set; }

        public string FirstName { get; set; }
        public long MSDSCERTIFICATEDOCID { get; set; }
        public string CollaboratedShipperMail { get; set; }
        public string QuoteSharedBy { get; set; }
        public bool IsDimsModified { get; set; }
        public PartyTypeEnum PartyType { get; set; }
    }


    public enum PackageTypeEnum : long
    {
        [Display(Name = "LCL")]
        LCL = 1,

        [Display(Name = "FCL")]
        FCL = 2
    }

    public enum PartyTypeEnum : long
    {
        [Display(Name = "EXPORTING")]
        EXPORTING = 1,

        [Display(Name = "IMPORTING")]
        IMPORTING = 2
    }

    public enum ProductEnum : long
    {
        [Display(Name = "Air Freight")]
        AirFreight = 3L,

        [Display(Name = "Ocean Freight")]
        OceanFreight = 2L
    }

    public enum MovementTypeEnum : long
    {
        [Display(Name = "Door to door")]
        DoortoDoor = 1,

        [Display(Name = "Port to port")]
        PorttoPort = 2,

        [Display(Name = "Door to port")]
        DoortoPort = 3,

        [Display(Name = "Port to door")]
        PorttoDoor = 4,
    }
    public enum Country : long
    {
        INDIA = 105
    }

    public class QuotationTemplateModel
    {
        public long QUOTATIONID { get; set; }
        public string TEMPLATENAME { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string PRODUCTNAME { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public decimal TOTALCBM { get; set; }
    }

    public enum PalletEnum : long
    {
        [Display(Name = "Myself")]
        Myself = 01,

        [Display(Name = "Shipafreight")]
        Shipafreight = 02
    }
}