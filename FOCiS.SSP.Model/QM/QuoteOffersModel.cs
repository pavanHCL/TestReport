﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FOCiS.SSP.Models.JP;

namespace FOCiS.SSP.Models.QM
{
   public class QuoteOffersModel
    {
        //public QuoteJobListModel()
        //{
        //    ShipmentItems = new List<QuotationShipmentPreviewModel>();           
        //    JobItems = new List<SSPJobDetailsModel>();
        //    ReceiptHeaderDetails = new List<ReceiptHeader>();
        //}
        public decimal CargoValue { get; set; }

        public string CargoValueCurrencyId { get; set; }

        public decimal ChargeableVolume { get; set; }

        public decimal ChargeableWeight { get; set; }

        public string CustomerId { get; set; }

        public DateTime DateModified { get; set; }

        public DateTime DateofEnquiry { get; set; }

        public DateTime DateofShipment { get; set; }

        public DateTime DateOfValidity { get; set; }

        public double DensityRatio { get; set; }

        public string DestinationPlaceCode { get; set; }

        public string DestinationPlaceName { get; set; }

        public string DestinationPortCode { get; set; }

        public string DestinationPortName { get; set; }

        public decimal DestinationPortId { get; set; }

        public decimal GrandTotal { get; set; }

        public long InsuredValue { get; set; }

        public string InsuredValueCurrencyId { get; set; }

        public long IsHazardousCargo { get; set; }

        public string ModifiedBy { get; set; }

        public string MovementTypeName { get; set; }

        public string OriginPlaceCode { get; set; }

        public string OriginPlaceName { get; set; }

        public string OriginPortCode { get; set; }

        public string OriginPortName { get; set; }

        public decimal OriginPortId { get; set; }

        public string PreferredCurrencyId { get; set; }

        public string ProductName { get; set; }

        public string ProductTypeName { get; set; }

        public decimal QuotationId { get; set; }

        public string QuotationNumber { get; set; }

        public string BannerDescription { get; set; }

        public string StateId { get; set; }

        public decimal TotalCBM { get; set; }

        public decimal TotalGrossWeight { get; set; }

        public long TotalQuantity { get; set; }

        public decimal VolumetricWeight { get; set; }

        public long JOBNUMBER { get; set; }

        public string OPERATIONALJOBNUMBER { get; set; }

        public string JSTATEID { get; set; }

        public long HasMissingCharges { get; set; }

        public bool IAgree { get; set; }

        public string OriginSubDivisionCode { get; set; }

        public string DestinationSubDivisionCode { get; set; }

        public int ProductId { get; set; }

        public int ProductTypeId { get; set; }

        public string CreatedBy { get; set; }

        public string GuestEmail { get; set; }

        public string GuestName { get; set; }

        public string GuestCompany { get; set; }

        public int ThresholdQty { get; set; }

        public string OriginZipCode { get; set; }

        public string DestinationZipCode { get; set; }

        public DateTime JDDATEMODIFIED { get; set; }

        public decimal JOBGRANDTOTAL { get; set; }

        public string OCountryName { get; set; }

        public decimal OCountryId { get; set; }

        public string OCountryCode { get; set; }

        public string OriginsubdivisionCode { get; set; }

        public string DCountryName{ get; set; }

        public decimal DCountryId { get; set; }

        public string DCountryCode { get; set; }

        public string DestinationsubdivisionCode { get; set; }

        public string TEMPLATENAME { get; set; } 
        //public IEnumerable<QuotationShipmentPreviewModel> ShipmentItems { get; set; }       
        //public IEnumerable<SSPJobDetailsModel> JobItems { get; set; }
        //public IEnumerable<ReceiptHeader> ReceiptHeaderDetails { get; set; }
        //public List<QuotationTermsAndConditionsModel> TermAndConditions { get; set; }
    }
}
