﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.QM
{
    public class QuotationPreviewModel
    {
        public QuotationPreviewModel()
        {
            ShipmentItems = new List<QuotationShipmentPreviewModel>();
            QuotationCharges = new List<QuotationChargePreviewModel>();
            TermAndConditions = new List<QuotationTermsAndConditionsModel>();
            IAgree = false;
        }
        [Display(Name = "CARGOVALUE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal CargoValue { get; set; }

        [Display(Name = "CARGOVALUECURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string CargoValueCurrencyId { get; set; }

        [Display(Name = "CHARGEABLEVOLUME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal ChargeableVolume { get; set; }

        [Display(Name = "ChargeableWeight", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal ChargeableWeight { get; set; }

        [Display(Name = "CUSTOMERID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string CustomerId { get; set; }

        [Display(Name = "DATEMODIFIED", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateModified { get; set; }

        [Display(Name = "DATEOFENQUIRY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateofEnquiry { get; set; }

        [Display(Name = "DATEOFSHIPMENT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateofShipment { get; set; }

        [Display(Name = "DATEOFVALIDITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public DateTime DateOfValidity { get; set; }

        [Display(Name = "DENSITYRATIO", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public double DensityRatio { get; set; }

        [Display(Name = "DESTINATIONPLACECODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPlaceCode { get; set; }

        [Display(Name = "DESTINATIONPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPlaceName { get; set; }

        [Display(Name = "DESTINATIONPORTCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPortCode { get; set; }

        [Display(Name = "DESTINATIONPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string DestinationPortName { get; set; }

        [Display(Name = "GRANDTOTAL", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal GrandTotal { get; set; }

        [Display(Name = "GRANDTOTAL", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string RoundedGrandTotalPrice { get; set; }

        [Display(Name = "INSUREDVALUE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long InsuredValue { get; set; }

        [Display(Name = "INSUREDVALUECURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string InsuredValueCurrencyId { get; set; }

        [Display(Name = "ISHAZARDOUSCARGO", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long IsHazardousCargo { get; set; }

        [Display(Name = "ISEVENTCARGO", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long IsEventCargo { get; set; }

        [Display(Name = "ISINSURANCEREQUIRED", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public bool IsInsuranceRequired { get; set; }

        [Display(Name = "MODIFIEDBY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ModifiedBy { get; set; }

        public string LanguageMovementTypeName { get; set; }

        [Display(Name = "MOVEMENTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string MovementTypeName { get; set; }

        [Display(Name = "ORIGINPLACECODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPlaceCode { get; set; }

        [Display(Name = "ORIGINPLACENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPlaceName { get; set; }

        [Display(Name = "ORIGINPORTCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPortCode { get; set; }

        [Display(Name = "ORIGINPORTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string OriginPortName { get; set; }

        [Display(Name = "Preferredcurrency",ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public string PreferredCurrencyId { get; set; }

        [Display(Name = "PRODUCTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ProductName { get; set; }

        [Display(Name = "PRODUCTTYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ProductTypeName { get; set; }

        [Display(Name = "QUOTATIONID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long QuotationId { get; set; }

        [Display(Name = "QUOTATIONNUMBER", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string QuotationNumber { get; set; }

        [Display(Name = "SHIPMENTDESCRIPTION", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public string ShipmentDescription { get; set; }

        [Display(Name = "STATEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string StateId { get; set; }

        [Display(Name = "TOTALCBM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TotalCBM { get; set; }

        [Display(Name = "TOTALGROSSWEIGHT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TotalGrossWeight { get; set; }

        [Display(Name = "TOTALQUANTITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long TotalQuantity { get; set; }

       [Display(Name = "VolumetricWeight", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public decimal VolumetricWeight { get; set; }

        public IEnumerable<QuotationShipmentPreviewModel> ShipmentItems { get; set; }

        public IEnumerable<QuotationChargePreviewModel> QuotationCharges { get; set; }

        public IEnumerable<QuotationTermsAndConditionsModel> TermAndConditions { get; set; }

        public long HasMissingCharges { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "You gotta tick the box!")]
        public bool IAgree { get; set; }

        public string OriginSubDivisionCode { get; set; }

        public string DestinationSubDivisionCode { get; set; }

        public int ProductId { get; set; }

        public int ProductTypeId { get; set; }

        public string CreatedBy { get; set; }

        public string GuestEmail { get; set; }

        public string GuestName { get; set; }

        public string GuestCompany { get; set; }

        public int ThresholdQty { get; set; }

        public int IsOversizeCargo { get; set; }

        public string HazardousGoodsType { get; set; }

        public long IsCustomRequired { get; set; }

        public string CustomsDisclaimr { get; set; }

        public string OriginZipCode { get; set; }

        public string DestinationZipCode { get; set; }

        public string SubmissionComments { get; set; }

        public double co2emission { get; set; }

        public string QuoteSource { get; set; }

        public string Transittime { get; set; }

        public string GSSCReason { get; set; }

        public long Originportid { get; set; }

        public long Destinationportid { get; set; }

        public string CE { get; set; }

        public string INCOTERMID { get; set; }

        public string INCOTERMDESC { get; set; }

        public string QUOTETYPE { get; set; }

        public long MultiQuotationId { get; set; }

        public string MTransittime { get; set; }

        public decimal MGrandTotal { get; set; }

        public string MProductTypeName { get; set; }

        public string OCOUNTRYNAME { get; set; }

        public string DCOUNTRYNAME { get; set; }
    }
}

