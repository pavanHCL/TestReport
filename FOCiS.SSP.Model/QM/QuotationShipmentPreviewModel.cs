﻿using DataAnnotationsExtensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.QM
{
    public class QuotationShipmentPreviewModel
    {
        [Display(Name = "ITEMDESCRIPTION", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ItemDescription { get; set; }

        [Display(Name = "ITEMHEIGHT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal ItemHeight { get; set; }

        [Display(Name = "ITEMLENGTH", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal ItemLength { get; set; }

        [Display(Name = "ITEMWIDTH", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal ItemWidth { get; set; }

        [Display(Name = "LENGTHUOM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long LengthUOM { get; set; }

        [Display(Name = "LENGTHUOMCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string LengthUOMCode { get; set; }

        [Display(Name = "LENGTHUOMNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string LengthUOMName { get; set; }

        [Display(Name = "PACKAGETYPECODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string PackageTypeCode { get; set; }
       
        [Display(Name = "PACKAGETYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string PackageTypeName { get; set; }

        [Display(Name = "QUANTITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long Quantity { get; set; }

        [Display(Name = "QUOTATIONID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long QuotationId { get; set; }

        [Display(Name = "SHIPMENTITEMID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long SHIPMENTITEMID { get; set; }

        [Display(Name = "TOTALCBM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TotalCBM { get; set; }

        [Display(Name = "WEIGHTPERPIECE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal WEIGHTPERPIECE { get; set; }

        [Display(Name = "WEIGHTTOTAL", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal WEIGHTTOTAL { get; set; }

        [Display(Name = "WEIGHTUOM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long WEIGHTUOM { get; set; }

        [Display(Name = "WEIGHTUOMCODE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string WEIGHTUOMCODE { get; set; }

        [Display(Name = "WEIGHTUOMNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string WEIGHTUOMNAME { get; set; }

        public long ContainerId { get; set; }

        public string ContainerName { get; set; }

        public string ContainerCode { get; set; }
    }
}
