﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.Models.QM
{
    public class QuotationChargePreviewModel
    {
        [Display(Name = "BASECHARGEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long BaseChargeId { get; set; }

        [Display(Name = "BASECHARGELOCALNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string BaseChargeLocalName { get; set; }

        [Display(Name = "BASECHARGENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string BaseChargeName { get; set; }

        [Display(Name = "CHARGEAPPLICABILITY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ChargeApplicability { get; set; }

        [Display(Name = "CHARGECONDITIONS", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ChargeConditions { get; set; }

        [Display(Name = "CHARGEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long ChargeId { get; set; }

        [Display(Name = "CHARGELOCALNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ChargeLocalName { get; set; }

        [Display(Name = "CHARGENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string ChargeName { get; set; }

        [Display(Name = "CURRENCYID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string CurrencyId { get; set; }

        [Display(Name = "QUOTATIONID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long QuotationId { get; set; }

        [Display(Name = "RATEBASISID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long RateBasisId { get; set; }

        [Display(Name = "RATEBASISNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string RateBasisName { get; set; }

        [Display(Name = "RATEREFERENCEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long RatereferenceId { get; set; }

        [Display(Name = "ROUTETYPEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long RouteTypeId { get; set; }

        [Display(Name = "ROUTETYPENAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string RouteTypeName { get; set; }

        [Display(Name = "SERVICECHARGEID", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long ServiceChargeId { get; set; }

        [Display(Name = "TOTALPRICE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal TotalPrice { get; set; }

        [Display(Name = "UNITRATE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public decimal UnitRate { get; set; }

        [Display(Name = "UOM", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string UOM { get; set; }

        [Display(Name = "WEIGHTAPP", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public long WeightApp { get; set; }

        public bool ISCHARGEINCLUDED { get; set; }

        [Display(Name = "ROUNDEDTOTALPRICE", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        public string RoundedTotalPrice { get; set; }       
        
    }
}
