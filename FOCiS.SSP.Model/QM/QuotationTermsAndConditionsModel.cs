﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.QM
{
    public class QuotationTermsAndConditionsModel
    {
        public long QUOTETERMSANDCONDITIONID { get; set; }

        public string DESCRIPTION { get; set; }      
    }
}
