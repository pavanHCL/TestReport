﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.UserManagement
{
    

    public class SupportRequestsEnv
    {
        public string Email { get; set; }
        public string TabStatus { get; set; }
        public string SearchString { get; set; }
        public int PageNo { get; set; }
        public string USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string MOBILENUMBER { get; set; }        
        public decimal PREVIEW { get; set; }
        public decimal Submitted { get; set; }
        public decimal Closed { get; set; }
        public decimal All { get; set; }
        public string REQID { get; set; }
        public IList<RequestsModel> Requests { get; set; }
        public IList<ChatModel> Chat { get; set; }
    }

    public class ExceptionEmailDto
    {
        public string Error { get; set; }
        public string StackTrace { get; set; }
        public string UTC { get; set; }
        public string AppUrl { get; set; }
        public string Url { get; set; }
        public string ActionMethod { get; set; }
        public string Headers { get; set; }
        public string Content { get; set; }
        public string ServerName { get; set; }
        public string UserAgent { get; set; }
        public string UserHostName { get; set; }
        public string UserHostAddress { get; set; }
        public string UserEmail { get; set; }
        public string ServerVariables { get; set; }
    }
}
