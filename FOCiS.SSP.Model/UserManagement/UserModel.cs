﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOCiS.Dictionary;
using FOCiS.SSP.Models.DB;

namespace FOCiS.SSP.Models.UserManagement
{
    public class UserModel
    {
        public string UserId { get; set; }

        public string UserType { get; set; }

        [Required(ErrorMessage = "Enter EmailId")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Retype Password required")]
        [CompareAttribute("Password", ErrorMessage = "Password doesn't match.")]
        [DataType(DataType.Password)]
        public string RetypePassword { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string CompanyName { get; set; }
        [StringLength(10)]
        public string ISDCode { get; set; }
        [StringLength(10)]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "Please select Terms and conditions")]
        public int IsTermsAgreed { get; set; }

        public long CountryId { get; set; }

        public int AccountStatus { get; set; }

        public DateTime LastlogonTime { get; set; }

        public long TimeZoneId { get; set; }

        public long UserCultureId { get; set; }

        public string Token { get; set; }

        public DateTime Tokenexpiry { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public bool RememberMe { get; set; }

        public int IsVATRegistered { get; set; }
        [StringLength(50)]
        public string VATRegNumber { get; set; }

        public string Salutation { get; set; }

        public long JobTitleId { get; set; }

        public string JobTitle { get; set; }

        public string CompanyLink { get; set; }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public long DeptId { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        [StringLength(10)]
        public string WorkPhone { get; set; }
        public long FailedPasswordAttempt { get; set; }

        public IList<UserAddressModel> UserAddress { get; set; }

        public string UAAddressType { get; set; }
        [StringLength(100)]
        public string UAHouseNo { get; set; }
        [StringLength(100)]
        public string UABuildingName { get; set; }
        [StringLength(100)]
        public string UAAddressline1 { get; set; }
        [StringLength(100)]
        public string UAAddressline2 { get; set; }
        public long UACountryId { get; set; }
        public string UACountryCode { get; set; }
        public string UACountryName { get; set; }
        public long UAStateId { get; set; }
        public string UAStateCode { get; set; }
        public string UAStateName { get; set; }
        public long UACityId { get; set; }
        public string UACityCode { get; set; }
        public string UACityName { get; set; }
        [StringLength(10)]
        public string UAPostCode { get; set; }
        public string UAFullAddress { get; set; }
        public int UAISDefault { get; set; }
        public DateTime UADateCreated { get; set; }
        public DateTime UADateModified { get; set; }

        public string ImgFileName { get; set; }
        public byte[] ImgContent { get; set; }

        [Display(Name = "GUESTNAME", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string GuestName { get; set; }

        [Display(Name = "GUESTCOMPANY", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string GuestCompany { get; set; }

        [Display(Name = "GUESTEMAIL", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string GuestEmail { get; set; }

        [Display(Name = "EmailmeaboutShipaFreightspecialoffersproductnewsandshippingindustryinsights", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool NOTIFICATIONSUBSCRIPTION { get; set; }

        public string  SHIPPINGEXPERIENCE { get; set; }
        public string SHIPMENTPROCESS { get; set; }
        public string PERSONALASSISTANCE { get; set; }
        public string EXISTINGFREIGHTFORWARDER { get; set; }
    }

    public class ForgotPasswordModel
    {
        [Required(ErrorMessage = "We need your email to send you a reset link!")]
        [Display(Name = "Your account email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Not a valid email--what are you trying to do here?")]
        public string EmailId { get; set; }
    }

    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "We need your email to send you a reset link!")]
        [Display(Name = "Your account email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Not a valid email")]
        public string EmailId { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "New password and confirmation does not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string ReturnToken { get; set; }
    }

    public class UserAddressModel
    {
        public long UserAdtlsId { get; set; }
        public string UserId { get; set; }
        public string AddressType { get; set; }
        public string HouseNo { get; set; }
        public string BuildingName { get; set; }
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public long CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public long StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public long CityId { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string PostCode { get; set; }
        public string FullAddress { get; set; }
        public int ISDefault { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string UAAddressType { get; set; }
        [StringLength(100)]
        public string UAHouseNo { get; set; }
        [StringLength(100)]
        public string UABuildingName { get; set; }
        [StringLength(100)]
        public string UAAddressline1 { get; set; }
        [StringLength(100)]
        public string UAAddressline2 { get; set; }
        public long UACountryId { get; set; }
        public string UACountryCode { get; set; }
        public string UACountryName { get; set; }
        public long UAStateId { get; set; }
        public string UAStateCode { get; set; }
        public string UAStateName { get; set; }
        public long UACityId { get; set; }
        public string UACityCode { get; set; }
        public string UACityName { get; set; }
        [StringLength(10)]
        public string UAPostCode { get; set; }
        public string UAFullAddress { get; set; }

    }

    public class UserProfileModel
    {
        public string UserId { get; set; }

        public string UserType { get; set; }

        public string EmailId { get; set; }

        public string Password { get; set; }

        public string RetypePassword { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [StringLength(100, ErrorMessage = "First Name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "Required")]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [StringLength(100, ErrorMessage = "Last Name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "Required")]
        public string LastName { get; set; }

        [Display(Name = "CompanyName", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [StringLength(100, ErrorMessage = "Company Name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "Required")]
        public string CompanyName { get; set; }

        [StringLength(5, ErrorMessage = "Enter valid ISD")]
        public string ISDCode { get; set; }

        [Display(Name = "MOBILENUMBER", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(12, ErrorMessage = "Enter valid Mobile Number")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string MobileNumber { get; set; }

        public int IsTermsAgreed { get; set; }

        [Display(Name = "COUNTRYID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long CountryId { get; set; }

        [Display(Name = "EmailmeaboutShipaFreightspecialoffersproductnewsandshippingindustryinsights", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool NOTIFICATIONSUBSCRIPTION { get; set; }

        public string SHIPPINGEXPERIENCE { get; set; }
        public string SHIPMENTPROCESS { get; set; }
        public string PERSONALASSISTANCE { get; set; }
        public string EXISTINGFREIGHTFORWARDER { get; set; }

        public int AccountStatus { get; set; }

        public DateTime LastlogonTime { get; set; }

        public long TimeZoneId { get; set; }

        public long UserCultureId { get; set; }

        public string Token { get; set; }

        public DateTime Tokenexpiry { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public bool RememberMe { get; set; }

        public int IsVATRegistered { get; set; }

        [Display(Name = "VATREGNUMBER", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [StringLength(50, ErrorMessage = "VAT Number cannot Exceed 50 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "Required")]
        public string VATRegNumber { get; set; }

        public string Salutation { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public Nullable<SalutationEnum> SalutationEnum { get; set; }

        [Display(Name = "JOBTITLEID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long JobTitleId { get; set; }

        [Display(Name = "JOBTITLEID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(40, ErrorMessage = "Job Title cannot Exceed 40 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string OtherJobTitle { get; set; }

        public string JobTitle { get; set; }

        [StringLength(100, ErrorMessage = "Comapny link cannot Exceed 100 Characters")]
        public string CompanyLink { get; set; }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public string ImgFileName { get; set; }
        public byte[] ImgContent { get; set; }

        [Display(Name = "DEPTID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long DeptId { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        [StringLength(10, ErrorMessage = "Enter valid work phone")]
        public string WorkPhone { get; set; }
        public IList<UserPartyMasterModel> PartiesModel { get; set; }
        public IList<RequestsModel> RequestsModel { get; set; }
        public IList<ChatModel> ChatModel { get; set; }
        public IList<AlternateEmails> AlternateEmails { get; set; }  
        //public IList<CreditModel> CreditItems { get; set; }
        public long UAUserAdtlsId { get; set; }
        public string UAAddressType { get; set; }
        [Display(Name = "UAHOUSENO", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "House Number cannot Exceed 100 Characters")]
        //[Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string UAHouseNo { get; set; }
        [Display(Name = "UABUILDINGNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Building Number cannot Exceed 100 Characters")]
        //[Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string UABuildingName { get; set; }
        [Display(Name = "UAADDRESSLINE1", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        [StringLength(100, ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "AddressLine1cannotExceed100Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "Required")]
        public string UAAddressline1 { get; set; }
        [Display(Name = "UAADDRESSLINE2", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessageResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource), ErrorMessageResourceName = "AddressLine2cannotExceed100Characters")]
        //[Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string UAAddressline2 { get; set; }
        [Display(Name = "UACOUNTRYID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long UACountryId { get; set; }
        public long UACountryDummyId { get; set; }
        public string UACountryCode { get; set; }
        public string UACountryName { get; set; }
        [Display(Name = "UASTATEID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long UAStateId { get; set; }
        public string UAStateCode { get; set; }
        public string UAStateName { get; set; }
        [Display(Name = "UACITYID", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public long UACityId { get; set; }
        public string UACityCode { get; set; }
        public string UACityName { get; set; }
        [Display(Name = "UAPOSTCODE", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        //[StringLength(10, ErrorMessage = "Enter Valid ZIP code")]
        //[Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string UAPostCode { get; set; }
        public string UAFullAddress { get; set; }
        public int UAISDefault { get; set; }
        public DateTime UADateCreated { get; set; }
        public DateTime UADateModified { get; set; }
        public long CREDITAPPID { get; set; }
        public string INDUSTRYID { get; set; }
        [Display(Name = "NOOFEMP", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Range(0, 999999, ErrorMessage = "Number of Employees must be in the range of 0 and 999999")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public int NOOFEMP { get; set; }
        public string SUBVERTICAL { get; set; }
        [StringLength(50, ErrorMessage = "Parent company cannot Exceed 50 Characters")]
        public string PARENTCOMPANY { get; set; }
        //[Range(0, 9999999999999999.99, ErrorMessage = "Invalid Annual Turnover; Max 18 digits")]
        [Required(ErrorMessage = "Annual Turnover is required")]
        [Display(Name = "ANNUALTURNOVER", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        //[Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string ANNUALTURNOVERCURRENCY { get; set; }
        public string PARTY { get; set; }
        public decimal ANNUALTURNOVER { get; set; }
        public int AnnualTurnOverDecimal { get; set; }
        public int CreditLimitDecimal { get; set; }

        [StringLength(50, ErrorMessage = "D&B Number cannot Exceed 50 Characters")]
        public string DBNUMBER { get; set; }
        public string PRODUCTPROFILE { get; set; }
        public string SALESCHANNEL { get; set; }
        public decimal PREVIOUSYEARREVENUE { get; set; }
        public decimal PREVIOUSYEARNR { get; set; }
        public decimal PREVIOUSYEARNRMARGIN { get; set; }
        public decimal PREVIOUSYEARBILLING { get; set; }
        public decimal NEXTYEARREVENUE { get; set; }
        public decimal NEXTYEARNR { get; set; }
        public decimal NEXTYEARNRMARGIN { get; set; }
        public decimal NEXTYEARBILLING { get; set; }
        public decimal CURRENTYEARREVENUE { get; set; }
        public decimal CURRENTYEARNR { get; set; }
        public decimal CURRENTYEARNRMARGIN { get; set; }
        public decimal CURRENTYEARBILLING { get; set; }
        public string HFMENTRYCODE { get; set; }
        public string HFMENTRYCURRENCY { get; set; }
        //[Range(0, 9999999999999999.99, ErrorMessage = "Invalid Credit Limit; Max 18 digits")]
        [Display(Name = "REQUESTEDCREDITLIMIT", ResourceType = typeof(FOCiS.Dictionary.QMStrings))]
        [Required(ErrorMessage = "Credit limit is required")]
        //[Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string REQUESTEDCREDITLIMITCURRENCY { get; set; }
        public decimal REQUESTEDCREDITLIMIT { get; set; }
        public string CURRENTPAYTERMSNAME { get; set; }
        [StringLength(500, ErrorMessage = "Comments cannot Exceed 500 Characters")]
        public string COMMENTS { get; set; }
        public DateTime CREDITLIMITEXPDATE { get; set; }
        public string CURRENCYID { get; set; }
        public string CURRENCYDESC { get; set; }
        public string DESCRIPTIONOFKS { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime CreditDATECREATED { get; set; }
        public DateTime CreditDATEMODIFIED { get; set; }
        public string MODIFIEDBY { get; set; }
        public string OWNERORGID { get; set; }
        public string OWNERLOCID { get; set; }
        public long STATEID { get; set; }
        public string CREDITREFNO { get; set; }
        public decimal APPROVEDCREDITLIMIT { get; set; }
        public decimal CURRENTOSBALANCE { get; set; }
        [Display(Name = "IamanexistingAgilitycustomer", ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool EXISTINGCUSTOMER { get; set; }
        [Display(Name = "AuthoriseShipaFreighttousemyexistingAgilitycreditaccount",ResourceType = typeof(FOCiS.Dictionary.App_GlobalResources.Resource))]
        public bool USEMYCREIDT { get; set; }
        public decimal PREVIEW { get; set; }
        public decimal Submitted { get; set; }
        public decimal Closed { get; set; }
        public string PREFERREDCURRENCY { get; set; }
        public string Industry { get; set; }
        public string OtherIndustry { get; set; }
        public bool ISFAPIAO { get; set; }
        public string PREFERREDCURRENCYCODE { get; set; }
        public string LENGTHUNIT { get; set; }
        public string WEIGHTUNIT { get; set; }
        //Added for FAPIAO
        public long FUSERFAPIAOID { get; set; }
        [Display(Name = "FCOMPANYNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Company name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FCOMPANYNAMECHINESE { get; set; }
        [Display(Name = "FCOMPANYNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Company name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
         public string FCOMPANYNAME { get; set; }       
        public string FPHONENUMBER { get; set; }
         [Display(Name = "FCOMPANYADDRESS", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Company Address cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FCOMPANYADDRESSCHINESE { get; set; }
         [Display(Name = "FCOMPANYADDRESS", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
         [StringLength(100, ErrorMessage = "Company Address cannot Exceed 100 Characters")]
         [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FCOMPANYADDRESS { get; set; }
        [Display(Name = "FTAXIDENTIFICATIONNO", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(50, ErrorMessage = "Tax Identification Number cannot Exceed 50 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FTAXIDENTIFICATIONNO { get; set; }
        public string FEMAILID { get; set; }
        public string FBANKACCOUNTNO { get; set; }
        public string FBANKNAME { get; set; }
        public DateTime FDATECREATED { get; set; }
        public DateTime FDATEMODIFIED { get; set; }
        public bool ISINDIVIDUAL { get; set; }
        [Display(Name = "FINDIVIDUALNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Individual Name  cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FINDIVIDUALNAME { get; set; }
        [Display(Name = "FINDTAXIDENTIFICATION", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(50, ErrorMessage = "Tax Identification Number cannot Exceed 50 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FINDTAXIDENTIFICATION { get; set; }
        public string FINDPHONENUMBER { get; set; }
        public string FINDEMAILID { get; set; }
        // public decimal EXTUSERFLAG { get; set; }
        //added for Dashboard tab
        public DBModel Dashboard { get; set; }
        //added for Credit
        public string CreditStatus { get; set; }
        public IList<CreditDoc> CreditDoc { get; set; }
        public string REJECTIONCOMMENTS { get; set; }
        public List<CrediComment> CreditComments { get; set; }
        public string ISNPC { get; set; }
        public string CRM { get; set; }
        public string BCOUNTRYSETUP { get; set; }
        //For Referral points
        public string REFERRERMODEL { get; set; }
        public double REFERRERBALAMOUNT { get; set; }
        public double REFERRERBALCOUPONCOUNT { get; set; }
    }
    public class CreditDoc
    {
        public string FILENAME { get; set; }
        public Int64 DOCID { get; set; }
        public Int64 COMMENTID { get; set; }

    }
    public enum SalutationEnum : long
    {
        [Display(Name = "Mr.")]
        SalutationMr = 1,

        [Display(Name = "Ms")]
        SalutationMs = 2,
    }
    public class AddCreditDoc
    {
        public string FILENAME { get; set; }
        public Int64 DOCID { get; set; }
        public string DocDownloadLink { get; set; }
    }
    public class AdditionalCredit
    {
        public long CREDITAPPID { get; set; }
        public long ADDITIONALCREDITID { get; set; }
        public string INDUSTRYID { get; set; }
        public Int64 NOOFEMP { get; set; }
        public string PARENTCOMPANY { get; set; }
        public decimal ANNUALTURNOVER { get; set; }
        public string DBNUMBER { get; set; }
        public string HFMENTRYCODE { get; set; }
        public string CURRENCYID { get; set; }
        public decimal REQUESTEDCREDITLIMIT { get; set; }
        public string CURRENTPAYTERMSNAME { get; set; }
        public string COMMENTS { get; set; }
        public string CreditStatus { get; set; }
        public bool EXISTINGCUSTOMER { get; set; }
        public bool USEMYCREIDT { get; set; }
        public string ADDCREDITREFNO { get; set; }
        public IList<AddCreditDoc> CreditDoc { get; set; }
        public List<AddCrediComment> CreditComments { get; set; }
        public string UserId { get; set; }
    }
    public class NotificationModel
    {
        public Int64 NotificationId { get; set; }
        public Int64 NotificationTypeId { get; set; }
        public string UserId { get; set; }
        public Int64 QuotationId { get; set; }
        public string QuotationNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public string NotificationCode { get; set; }
        public Int64 JobNumber { get; set; }
        public DateTime DateRequested { get; set; }
        public string ConsignmentId { get; set; }
        public string BookingId { get; set; }
        public string NotificationStatus { get; set; }
        public string PartyName { get; set; }
        public string PartyStatus { get; set; }
        public string REQID { get; set; }
        public Int64 CHARGESETID { get; set; }
    }
    public class PartiesModel
    {
        [Display(Name = "FIRSTNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "First Name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string FirstName { get; set; }

        [Display(Name = "LASTNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Last Name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string LastName { get; set; }

        [Display(Name = "COMPANYNAME", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(100, ErrorMessage = "Company Name cannot Exceed 100 Characters")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string CompanyName { get; set; }

        [Display(Name = "MOBILENUMBER", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(12, ErrorMessage = "Enter valid Mobile Number")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string MobileNumber { get; set; }

        [Display(Name = "UAPOSTCODE", ResourceType = typeof(FOCiS.Dictionary.UserManagementStrings))]
        [StringLength(10, ErrorMessage = "Enter Valid ZIP code")]
        [Required(ErrorMessageResourceType = typeof(FOCiS.Dictionary.ValidationStrings), ErrorMessageResourceName = "Required")]
        public string UAPostCode { get; set; }

        public string Address { get; set; }
    }

    public class UserPartyMasterModel
    {
        public Int64 SSPCLIENTID { get; set; }
        public string Clientname { get; set; }
        public string HOUSENO { get; set; }
        public string BUILDINGNAME { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public string ADDRESSLINE2 { get; set; }
        public string COUNTRYNAME { get; set; }
        public string CITYNAME { get; set; }
        public string STATENAME { get; set; }
        public string PHONE { get; set; }
        public string EMAILID { get; set; }
        public string Pincode { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string SALUTATION { get; set; }
        public decimal UPERSONAL { get; set; }
        public decimal COUNTRYID { get; set; }
        public decimal STATEID { get; set; }
        public decimal CITYID { get; set; }
        public string STATUS { get; set; }
        public string PARTYSTATUS { get; set; }
        public string NICKNAME { get; set; }
        public string TAXID { get; set; }
        public string FULLADDRESSINCHINESE { get; set; }
        public string ISDCODE { get; set; }
    }

    public class MultipleModel
    {
        public UserProfileModel UserProfileModel { get; set; }
        public IList<UserPartyMasterModel> PartiesModel { get; set; }
    }
    public class RequestsModel
    {
        public Decimal REQID { get; set; }
        public string CATEGORY { get; set; }
        public string MODULE { get; set; }
        public string QUERY { get; set; }
        public string SEQNO { get; set; }
        public string MODULEID { get; set; }
        public string DESCRIPTION { get; set; }
        public string REQSTATUS { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime DATECREATED { get; set; }
        public decimal ChatCount { get; set; }
    }

    public class ChatModel
    {
        public Decimal REQID { get; set; }
        public string CATEGORY { get; set; }
        public string MODULE { get; set; }
        public string QUERY { get; set; }
        public string SEQNO { get; set; }
        public string MODULEID { get; set; }
        public string DESCRIPTION { get; set; }
        public string REQSTATUS { get; set; }
        public DateTime DATECREATED { get; set; }
        public string CREATEDBY { get; set; }
        public string MESSAGE { get; set; }
        public byte[] FILE { get; set; }
        public string FILENAME { get; set; }
        public Decimal DOCREQID { get; set; }
        public string UPLOADEDFROM { get; set; }
    }

    public class AlternateEmails
    {
        public Decimal ID { get; set; }
        public string ALTERNATEEMAIL { get; set; }
        public string USERID { get; set; }
        public string TYPE { get; set; }
        public string PARTYNAME { get; set; }
        public Decimal PARTYID { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }       
    }
    public class UserFapiaoModel
    {
        public long USERFAPIAOID { get; set; }
        public string USERID { get; set; }
        public string COMPANYNAME { get; set; }
        public string PHONENUMBER { get; set; }
        public string COMPANYADDRESS { get; set; }
        public string TAXIDENTIFICATIONNO { get; set; }
        public string BANKNAME { get; set; }
        public string BANKACCOUNTNO { get; set; }
    }
}
