﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.UserManagement
{
    public class CrediComment
    {
        public decimal COMMENTID { get; set; }
        public long CREDITAPPID { get; set; }
        public string COMMENTS { get; set; }
        public string CREATEDBY { get; set; }
        public string COMMENTSFROM { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public decimal? INFOTYPE { get; set; }
        public List<CreditDoc> CreditDoc { get; set; }
    }
    public class AddCrediComment
    {
        public decimal COMMENTID { get; set; }
        public long CREDITAPPID { get; set; }
        public string COMMENTS { get; set; }
        public string CREATEDBY { get; set; }
        public string COMMENTSFROM { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public decimal? INFOTYPE { get; set; }
        public List<AddCreditDoc> CreditDoc { get; set; }
    }
}
