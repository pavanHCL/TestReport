﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.Payment
{
    public class StripeData
    {
        public string publicKey { get; set; }
        public string secretKey { get; set; }
        public string account { get; set; }
    }
}
