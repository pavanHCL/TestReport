﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.Models.Payment
{
    public class HsbcOrder
    {
        private string sOrderNo;
        private string sOrderTime;
        private string sTransType;
        private string sPayType;
        private string sOrderCurr;
        private string sOrderAmt;
        private string sMerId;
        private string sMerIp;
        private string sUserId;
        private string sNotifyUrl;
        private string sFrontUrl;
        private string sLimitPay;
        private string sGoodsDes;
        private string sGoodsInfo;
        private string sRemarks;
        private string sExpireTime;

        public string expireTime
        {
            get { return sExpireTime; }
            set { sExpireTime = value; }
        }

        public string orderNo
        {
            get { return sOrderNo; }
            set { sOrderNo = value; }
        }

        public string orderTime
        {
            get { return sOrderTime; }
            set { sOrderTime = value; }
        }

        public string transType
        {
            get { return sTransType; }
            set { sTransType = value; }
        }

        public string payType
        {
            get { return sPayType; }
            set { sPayType = value; }
        }

        public string orderCurr
        {
            get { return sOrderCurr; }
            set { sOrderCurr = value; }
        }

        public string orderAmt
        {
            get { return sOrderAmt; }
            set { sOrderAmt = value; }
        }

        public string merId
        {
            get { return sMerId; }
            set { sMerId = value; }
        }

        public string merIp
        {
            get { return sMerIp; }
            set { sMerIp = value; }
        }

        public string userId
        {
            get { return sUserId; }
            set { sUserId = value; }
        }

        public string notifyUrl
        {
            get { return sNotifyUrl; }
            set { sNotifyUrl = value; }
        }

        public string frontUrl
        {
            get { return sFrontUrl; }
            set { sFrontUrl = value; }
        }

        public string limitPay
        {
            get { return sLimitPay; }
            set { sLimitPay = value; }
        }

        public string goodsDes
        {
            get { return sGoodsDes; }
            set { sGoodsDes = value; }
        }

        public string goodsInfo
        {
            get { return sGoodsInfo; }
            set { sGoodsInfo = value; }
        }

        public string remarks
        {
            get { return sRemarks; }
            set { sRemarks = value; }
        }

    }

    public class HSBCResponseDto
    {
        public string StatusCode { get; set; }
        public string HtmlCode { get; set; }
        public string proCode { get; set; }
        public string proMsg { get; set; }
        public string orderid { get; set; }
    }

    public class HSBCStatusDto
    {
        public string orderNo { get; set; }
        public string transType { get; set; }
        public string merId { get; set; }
        public string merIp { get; set; }
        public string orgOrderNo { get; set; }
    }


    public class HSBCEnquiryStatus
    {
        public string orgOrderNo { get; set; }
        public string orderAmt { get; set; }
        public string txnTime { get; set; }
        public string merId { get; set; }
        public string proCode { get; set; }
        public string proMsg { get; set; }
        public string orderNo { get; set; }
    }

    public class HsbcRefundDto
    {
        public string orderCurr { get; set; }
        public string orderNo { get; set; }
        public string orderAmt { get; set; }
        public string merIp { get; set; }
        public string orderTime { get; set; }
        public string transType { get; set; }
        public string notifyUrl { get; set; }
        public string merId { get; set; }
        public string orgOrderNo { get; set; }
        public string orgOrderDate { get; set; }
        public string orgAmt { get; set; }
    }

    public class HsbcRefundResponse
    {
        public HsbcCommonResponse common { get; set; }
        public HSBCEnquiryStatus response { get; set; }
    }
    
    public class HsbcCommonResponse
    {
        public string messageId { get; set; }
        public string returnCode { get; set; }
        public string returnReason { get; set; }
        public string sentTime { get; set; }
        public string responseTime { get; set; }
    }
}
