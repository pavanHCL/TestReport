﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using FOCiS.SSP.DBFactory.Factory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FOCiS.SSP.Web.UI.Controllers.UserManagement;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Web.UI;
using System.Web.Mvc;
using FOCiS.SSP.DBFactory.Entities;
using System.Data;
using FOCiS.SSP.Web.UI.Helpers;

namespace ShipaUnitTest
{
    [TestClass]
    public class UserManagement
    {
        Mock<IUserDBFactory> _udb;
        [TestInitialize]
        public void UserManagement_Controller_Initialize()
        {
            _udb = new Mock<IUserDBFactory>(MockBehavior.Default);
            AutoMapperConfig.RegisterMappings();
            var con = new UserManagementController();
        }

        [TestMethod]
        public void DeleteNotification_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup(m => m.DeleteNotification(It.IsAny<int>())).Returns("");
            // Act
            var res = _uc.Object.DeleteNotification(It.IsAny<int>()) as ViewResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DeleteNotification_Should_Throw_Exception()
        {
            // Arrange
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _udb.Setup(m => m.DeleteNotification(It.IsAny<int>())).Throws(new ArgumentException());
            // Act
            var res = _uc.Object.DeleteNotification(It.IsAny<int>()) as ViewResult;
        }
        [TestMethod]
        public void NotificationsMark_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup(m => m.NotificationsMark(It.IsAny<int>())).Returns("");
            // Act
            var res = _uc.Object.NotificationsMark(It.IsAny<int>()) as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NotificationsMark_Should_Throw_Exception()
        {
            // Arrange
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _udb.Setup(m => m.NotificationsMark(It.IsAny<int>())).Throws(new ArgumentException());
            // Act
            var res = _uc.Object.NotificationsMark(It.IsAny<int>()) as JsonResult;
        }
        [TestMethod]
        [ExpectedException(typeof(ShipaExceptions))]
        public void DownloadCreditDoc_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup<IList<SSPDocumentsEntity>>(m => m.GetCreditDocument(It.IsAny<string>(),It.IsAny<string>())).Returns(MockGetCreditDocument);
            // Act
            _uc.Object.DownloadCreditDoc(It.IsAny<string>());
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ShipaExceptions))]
        public void DownloadAddCreditDoc_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup<IList<SSPDocumentsEntity>>(m => m.GetAddCreditDocument(It.IsAny<string>(), It.IsAny<string>())).Returns(MockGetCreditDocument);
            // Act
            _uc.Object.DownloadAddCreditDoc(It.IsAny<string>());
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void Deletecreditdoc_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup(m => m.DeleteCreditDocument(It.IsAny<string>(),It.IsAny<int>()));
            // Act
            var res = _uc.Object.Deletecreditdoc(It.IsAny<int>()) as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
       
        [TestMethod]
        public void DeleteAddcreditdoc_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup(m => m.DeleteAddCreditDocument(It.IsAny<string>(), It.IsAny<int>()));
            // Act
            var res = _uc.Object.DeleteAddcreditdoc(It.IsAny<int>()) as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void ChecGuestkUserExists_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup<ProfileInfo>(m => m.GetProfileInfo(It.IsAny<string>()));
            // Act
            var res = _uc.Object.ChecGuestkUserExists(It.IsAny<string>()) as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChecGuestkUserExists_Should_Throw_Exception()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _udb.Setup<ProfileInfo>(m => m.GetProfileInfo(It.IsAny<string>())).Throws(new ArgumentException());
            // Act
            var res = _uc.Object.ChecGuestkUserExists(It.IsAny<string>()) as JsonResult;
        }
        [TestMethod]
        public void IsProfileUpdated_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup<DataSet>(m => m.IsProfileUpdated(It.IsAny<string>()));
            // Act
            var res = _uc.Object.IsProfileUpdated() as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IsProfileUpdated_Should_Throw_Exception()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _udb.Setup<DataSet>(m => m.IsProfileUpdated(It.IsAny<string>())).Throws(new ArgumentException());
            // Act
            var res = _uc.Object.IsProfileUpdated() as JsonResult;
        }
        [TestMethod]
        public void QuoteforSignUp_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();     
            // Act
            var res = _uc.Object.QuoteforSignUp() as ViewResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void EncryptId_Should_Work()
        {
            var _qc = new Mock<UserManagementController>(_udb.Object);
            _qc.Object.EncryptId("1234");
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void UnlocationMapping_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup(m => m.UnlocationMapping(It.IsAny<int>())).Returns("");
            // Act
            var res = _uc.Object.UnlocationMapping(It.IsAny<int>()) as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UnlocationMapping_Should_Throw_Exception()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _udb.Setup(m => m.UnlocationMapping(It.IsAny<int>())).Throws(new ArgumentException());
            // Act
            var res = _uc.Object.UnlocationMapping(It.IsAny<int>()) as JsonResult;

        }
        [TestMethod]
        public void DeleteNotificationsScript_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            _udb.Setup(m => m.DeleteNotification(It.IsAny<int>())).Returns("");
            // Act
            var res = _uc.Object.DeleteNotificationsScript(It.IsAny<int>()) as JsonResult;
            // Assert
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DeleteNotificationsScript_Should_Throw_Exception()
        {
            // Arrange
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _udb.Setup(m => m.DeleteNotification(It.IsAny<int>())).Throws(new ArgumentException());
            // Act
            var res = _uc.Object.DeleteNotificationsScript(It.IsAny<int>()) as JsonResult;
        }
        [TestMethod]
        public void ResetPassword_Should_Work()
        {
            var _uc = new Mock<UserManagementController>(_udb.Object);
            _uc.Object.SetFakeControllerContextAuthorized();
            // Act
            var res = _uc.Object.ResetPassword(It.IsAny<string>(),It.IsAny<string>()) as ViewResult;
            // Assert
            Assert.IsTrue(true);
        }
        
        #region Mock Data
        private IList<SSPDocumentsEntity> MockGetCreditDocument()
        {
            //var file = new Mock<byte[]>();
            var itemMock = new Mock<SSPDocumentsEntity>();
            itemMock.Object.FILECONTENT = CreateSpecialByteArray(7000);
            var items = new List<SSPDocumentsEntity> { itemMock.Object };
            //var ss = CreateSpecialByteArray(7000);
            //items.First().FILECONTENT = ss;

            var mock = new Mock<IList<SSPDocumentsEntity>>();
            mock.Setup(m => m.GetEnumerator()).Returns(() => items.GetEnumerator());
            //mock.Setup(m => m.First().FILECONTENT).Returns(new byte[] { 1, 2, 3 });
            return mock.Object;
        }
        static byte[] CreateSpecialByteArray(int length)
        {
            var arr = new byte[length];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = 0x20;
            }
            return arr;
        }
        #endregion Mock Data
    }
}
