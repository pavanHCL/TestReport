﻿using FOCiS.SSP.Web.UI.Controllers.Videos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShipaUnitTest
{
    [TestClass]
    public  class Videos
    {
        [TestMethod]
        public void ShipaVideos()
        {
            VideosController vs = new VideosController();
            ViewResult result = vs.Videos() as ViewResult;
            Assert.AreEqual("ShipaVideos", result.ViewName);
        }
        //[TestMethod]
        //public void IntroShipaFreight()
        //{
        //    VideosController vs = new VideosController();
        //    ViewResult result = vs.IntroShipaFreight() as ViewResult;
        //    Assert.AreEqual("videoplayer", result.ViewName);
        //}
        //[TestMethod]
        //public void RegisterVideo()
        //{
        //    VideosController vs = new VideosController();
        //    ViewResult result = vs.RegisterVideo() as ViewResult;
        //    Assert.AreEqual("videoplayer", result.ViewName);
        //}
        //[TestMethod]
        //public void QuoteVideo()
        //{
        //    VideosController vs = new VideosController();
        //    ViewResult result = vs.QuoteVideo() as ViewResult;
        //    Assert.AreEqual("videoplayer", result.ViewName);
        //}
        //[TestMethod]
        //public void BookaShipment()
        //{
        //    VideosController vs = new VideosController();
        //    ViewResult result = vs.BookaShipment() as ViewResult;
        //    Assert.AreEqual("videoplayer", result.ViewName);
        //}
        //[TestMethod]
        //public void ArrangePayment()
        //{
        //    VideosController vs = new VideosController();
        //    ViewResult result = vs.ArrangePayment() as ViewResult;
        //    Assert.AreEqual("videoplayer", result.ViewName);
        //}
        //[TestMethod]
        //public void TrackaShipment()
        //{
        //    VideosController vs = new VideosController();
        //    ViewResult result = vs.TrackaShipment() as ViewResult;
        //    Assert.AreEqual("videoplayer", result.ViewName);
        //}
    }
}
