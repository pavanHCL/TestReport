﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FOCiS.SSP.Web.UI.Controllers.Quotation;
using FOCiS.SSP.Models.QM;
using System.Web.Mvc;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using System.Data;
using System.Collections.Generic;
using System.Web;
using Moq;
using FOCiS.SSP.DBFactory.Factory;
using System.Threading.Tasks;


namespace ShipaUnitTest
{
    [TestClass]
    public class Quotation
    {
        Mock<IQuotationDBFactory> _qdb;

        [TestInitialize]
        public void Quotation_Controller_Initialize()
        {
            _qdb = new Mock<IQuotationDBFactory>(MockBehavior.Default);
        }

        private readonly QuotationController _quote;
        private readonly QuotationModel _quotemodel;
        private readonly QuotationDBFactory _quoteDBfactory;
        public Quotation()
        {
            _quote = new QuotationController();
            _quotemodel = new QuotationModel();
            _quoteDBfactory = new QuotationDBFactory();
        }

        public Quotation(
            QuotationController quote,
            QuotationModel quotemodel,
            QuotationDBFactory quoteDBfactory
            )
        {
            _quote = quote;
            _quotemodel = quotemodel;
            _quoteDBfactory = quoteDBfactory;
        }

        //[TestMethod]
        //public void DistanceTo()
        //{
        //    double lat1 = 1.0;
        //    double lat2 = 2.0;
        //    double lon1 = 3.0;
        //    double lon2 = 4.0;
        //    double expectedvalue = 157.21786778587091;
        //    double result = _quote.DistanceTo(lat1, lon1, lat2, lon2, 'K');
        //    Assert.AreEqual(result, expectedvalue);
        //}

        [TestMethod]
        public void DimsNoChange()
        {
            ViewResult result = _quote.DimsNoChange() as ViewResult;
            Assert.AreEqual("DimsNoChange", result.ViewName);
        }

        [TestMethod]
        public void SwissLanding()
        {
            ViewResult result = _quote.SwissLanding() as ViewResult;
            Assert.AreEqual("SwissLanding", result.ViewName);
        }

        [TestMethod]
        public void Contactus()
        {
            ViewResult result = _quote.Contactus() as ViewResult;
            Assert.AreEqual("Contactus", result.ViewName);
        }

        //[TestMethod]
        //public void ShareLink()
        //{
        //    string email = "vchikoti@agility.com";
        //    string link = "";
        //    string name = "Chikoti";
        //    string notes = "Unit Test";
        //    string AddresBook = "";
        //    int id = 120883;
        //    try
        //    {
        //        _quote.ShareLink(email, link, id, name, notes, AddresBook);
        //        Assert.IsTrue(true);
        //    }
        //    catch
        //    {
        //        Assert.IsTrue(false);
        //    } 
        //}


        [TestMethod]
        public void GetRegisterVideo()
        {
            var _qcVideo = new Mock<QuotationController>(_qdb.Object);
            _qdb.Setup(m => m.GetRegisterVideo("","")).Returns("");
            _qcVideo.Object.GetRegisterVideo("");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TemplateNameCheck()
        {
            QuotationEntity objquote = new QuotationEntity();
            var _qc = new Mock<QuotationController>(_qdb.Object);
            _qc.Object.SetFakeControllerContextAuthorized();
            _qdb.Setup(m => m.TemplateNameCheck("", "")).Returns(0);
            _qdb.Setup(m => m.GetQuotationIdCount(0)).Returns(0);
            _qdb.Setup(m => m.GetById(23)).Returns(objquote);
            _qdb.Setup(m => m.SaveTemplate(objquote, "")).Returns("");
            _qc.Object.TemplateNameCheck(It.IsAny<string>(), It.IsAny<long>());
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void NoofQuotesForGuest()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            _qdb.Setup(m => m.NoofQuotesForGuest("")).Returns(0);
            _qc.Object.NoofQuotesForGuest("");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetEncodedId()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            _qc.Object.GetEncodedId("1234");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Descriptionvalidation()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();
            dt.Columns.Add("test");
            dt.Rows.Add("0");
            _qdb.Setup(m => m.Descriptionvalidation(0, 0, "")).Returns(dt);
            _qc.Object.Descriptionvalidation(0, 0, "");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void ValidateMinWeight()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();
            dt.Columns.Add("test");
            dt.Rows.Add("0");
            ds.Tables.Add(dt);
            _qdb.Setup(m => m.ValidateMinWeight("")).Returns(ds);
            _qc.Object.ValidateMinWeight("");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void NoofQuotes()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            _qc.Object.SetFakeControllerContextAuthorized();
            _qdb.Setup(m => m.NoofQuotesForGuest("")).Returns(0);
            _qc.Object.NoofQuotes();
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void QuoteLimit()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            _qc.Object.SetFakeControllerContextAuthorized();
            _qdb.Setup(m => m.QuoteLimit("")).Returns(0);
            _qc.Object.QuoteLimit();
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void SubmitReasonforUnbook()
        {
            var _qc = new Mock<QuotationController>(_qdb.Object);
            _qdb.Setup(m => m.SubmitReasonforUnbook(0,0,""));
            _qc.Object.SubmitReasonforUnbook(0, 0, "");
            Assert.IsTrue(true);
        }
    }
}
