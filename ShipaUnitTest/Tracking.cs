﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.Tracking;
using FOCiS.SSP.Web.UI;
using FOCiS.SSP.Web.UI.Controllers.Tracking;
using FOCiS.SSP.Web.UI.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShipaUnitTest
{
    [TestClass]
    public class Tracking
    {
        Mock<ITrackingDBFactory> _tdb;
        Mock<IJobBookingDbFactory> _jdb;

        [TestInitialize]
        public void Tracking_Controller_Initialize()
        {
            _tdb = new Mock<ITrackingDBFactory>(MockBehavior.Default);
            _jdb = new Mock<IJobBookingDbFactory>(MockBehavior.Default);
            AutoMapperConfig.RegisterMappings();
            var con = new TrackingController();
        }

        [TestMethod]
        public void TrackIndex_Should_return_View()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);

            _con.Object.SetFakeControllerContextAuthorized();
            _tdb.Setup<ListTrackingEntity>(o =>
               o.ReturnTrackEntityList(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>()
                        ))
               .Returns(ListMockData());

            _tdb.Setup<List<TrackStatusCount>>(o =>
              o.GetTrackStatusCount(
                       It.IsAny<string>(),
                       It.IsAny<string>()
                       ))
              .Returns(new List<TrackStatusCount>());

            // Act
            var res = _con.Object.Index(It.IsAny<Int32>(), It.IsAny<string>()) as ViewResult;

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TrackIndex_Will_Throw_Exception()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);

            _con.Object.SetFakeControllerContextAuthorized();
            _tdb.Setup<ListTrackingEntity>(o =>
               o.ReturnTrackEntityList(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>()
                        ))
               .Throws(new ArgumentException());

            // Act
            var res = _con.Object.Index(It.IsAny<Int32>(), It.IsAny<string>()) as ViewResult;

        }

        [TestMethod]
        public void TrackSummary_Should_Work()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();
            _con.Object.Session["TrackingList"] = new ListTrackingModel();
            _tdb.Setup<ListTrackingEntity>(o =>
               o.ReturnTrackEntityList(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>()
                        ))
               .Returns(ListMockData());

            _tdb.Setup<List<PartyDetails>>(o => o.GetPartyDetails(It.IsAny<string>(), It.IsAny<Int64>()))
                    .Returns(new List<PartyDetails>());

            _tdb.Setup<List<ShpimentEventComments>>(o => o.GetShipmentEventComments(It.IsAny<string>(), It.IsAny<Int32>(), It.IsAny<string>()))
                    .Returns(new List<ShpimentEventComments>());

            // Act
            var res = _con.Object.TrackSummary(It.IsAny<Int32>(), It.IsAny<string>()) as ViewResult;

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TrackSummary_Will_Throw_Exception()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();
            _con.Object.Session["TrackingList"] = new ListTrackingModel();
            //_con.Object.TempData = new TempDataDictionary();
            _con.Object.TempData.Add("ConsignmentId", It.IsAny<string>());
            _con.Object.TempData.Add("jobnumber", It.IsAny<string>());

            _tdb.Setup<ListTrackingEntity>(o =>
               o.ReturnTrackEntityList(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>()
                        ))
               .Throws(new ArgumentException());


            // Act
            var res = _con.Object.TrackSummary(null, It.IsAny<string>()) as ViewResult;

        }

        [TestMethod]
        [ExpectedException(typeof(ShipaExceptions))]
        public void TrackDownLoad_Should_Work()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();

            _jdb.Setup<IList<SSPDocumentsEntity>>(o =>
                o.GetDocumentDetailsbyDocumetnId(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(MockDocumentData());
            ;

            // Act           
            _con.Object.TrackDownLoad(It.IsAny<string>());

            // Assert
            Assert.IsTrue(true);

        }

        [TestMethod]
        public void InsertComments_Should_Work()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();

            _tdb.Setup(o =>
                o.InsertShareComments(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<Int64>(),
                    It.IsAny<Int32>()))
            ;

            // Act
            _con.Object.InsertComments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InsertComments_Should_Throw_Exception()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);

            _tdb.Setup(o =>
                o.InsertShareComments(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<Int64>(),
                    It.IsAny<Int32>()))
            ;

            // Act
            _con.Object.InsertComments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
        }

        [TestMethod]
        public void UpdateComments_Should_Work()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();

            _tdb.Setup(o =>
                o.InsertShareComments(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<Int64>(),
                    It.IsAny<Int32>()))
            ;

            // Act
            _con.Object.UpdateComments("test_test", It.IsAny<string>(), It.IsAny<string>());

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UpdateComments_Should_Throw_Exception()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);

            _tdb.Setup(o =>
                o.InsertShareComments(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<Int64>(),
                    It.IsAny<Int32>()))
            ;

            // Act
            _con.Object.UpdateComments("test_test", It.IsAny<string>(), It.IsAny<string>());
        }

        [TestMethod]
        public void GetEnteredComments_Should_Work()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();
            _con.Object.Session["TrackingList"] = MockListTrackingModel();

            // Act
            _con.Object.GetEnteredComments("test_test");

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GetEnteredComments_Should_Throw_Exception()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            
            // Act
            _con.Object.GetEnteredComments("test_test");

        }

        [TestMethod]
        public void ShipaTracking_Should_Work()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();

            _tdb.Setup<ListTrackingEntity>(o =>
               o.ShipmentTracking(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<string>()
                        ))
               .Returns(ListMockData());

            _tdb.Setup<List<PartyDetails>>(o => o.GetPartyDetails(It.IsAny<string>(), It.IsAny<Int64>()))
                    .Returns(new List<PartyDetails>());

            _tdb.Setup<List<ShpimentEventComments>>(o => o.GetShipmentEventComments(It.IsAny<string>(), It.IsAny<Int32>(), It.IsAny<string>()))
                    .Returns(new List<ShpimentEventComments>());

            _tdb.Setup<List<SSPDocumentsEntity>>(o => o.GetUploadedDocuments(
                    It.IsAny<string>(),
                    It.IsAny<Int32>(),
                    It.IsAny<string>()
                    ))
                .Returns(new List<SSPDocumentsEntity>());
            ;

            // Act
            var res = _con.Object.ShipaTracking(It.IsAny<string>(), It.IsAny<string>()) as ViewResult;

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShipaTracking_Should_Throw_Exception()
        {
            // Arrange
            var _con = new Mock<TrackingController>(_tdb.Object, _jdb.Object);
            _con.Object.SetFakeControllerContextAuthorized();

            _tdb.Setup<ListTrackingEntity>(o =>
               o.ShipmentTracking(
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<string>()
                        ))
               .Throws(new ArgumentException());

            // Act
            var res = _con.Object.ShipaTracking(It.IsAny<string>(), It.IsAny<string>()) as ViewResult;

        }

        #region Mock Data
        private ListTrackingEntity ListMockData()
        {
            // Mock PackageDetailsItems
            var itemMockpde = new Mock<PackageDetailsEntity>();
            var itemspde = new List<PackageDetailsEntity> { itemMockpde.Object };
            var mockpde = new Mock<IEnumerable<PackageDetailsEntity>>();
            mockpde.Setup(m => m.GetEnumerator()).Returns(() => itemspde.GetEnumerator());

            // Mock TrackingItems
            var itemMockte = new Mock<TrackingEntity>();
            var itemspte = new List<TrackingEntity> { itemMockte.Object };
            var mockpte = new Mock<IEnumerable<TrackingEntity>>();
            mockpte.Setup(m => m.GetEnumerator()).Returns(() => itemspte.GetEnumerator());

            // Mock TrackItems
            var itemMockti = new Mock<Track>();
            var itemspti = new List<Track> { itemMockti.Object };
            var mockpti = new Mock<IEnumerable<Track>>();
            mockpti.Setup(m => m.GetEnumerator()).Returns(() => itemspti.GetEnumerator());

            var data = new ListTrackingEntity()
            {
                PackageDetailsItems = mockpde.Object,
                TrackingItems = mockpte.Object,
                TrackItems = mockpti.Object
            };
            return data;
        }

        private IList<SSPDocumentsEntity> MockDocumentData()
        {
            //var file = new Mock<byte[]>();
            var itemMock = new Mock<SSPDocumentsEntity>();
            itemMock.Object.FILECONTENT = CreateSpecialByteArray(7000);
            var items = new List<SSPDocumentsEntity> { itemMock.Object };
            //var ss = CreateSpecialByteArray(7000);
            //items.First().FILECONTENT = ss;

            var mock = new Mock<IList<SSPDocumentsEntity>>();
            mock.Setup(m => m.GetEnumerator()).Returns(() => items.GetEnumerator());
            //mock.Setup(m => m.First().FILECONTENT).Returns(new byte[] { 1, 2, 3 });
            return mock.Object;
        }

        private ListTrackingModel MockListTrackingModel()
        {
            // Mock TrackingItems
            var itemMockte = new Mock<TrackingModel>();
            var itemspte = new List<TrackingModel> { itemMockte.Object };
            var mockpte = new Mock<IEnumerable<TrackingModel>>();
            mockpte.Setup(m => m.GetEnumerator()).Returns(() => itemspte.GetEnumerator());

            //IEnumerable<ShpimentEventCommentsModel>
            var itemMockec = new Mock<ShpimentEventCommentsModel>();
            itemMockec.Object.EVENTNAME = "test";
            itemMockec.Object.COMMENTS = "test";
            var itemsec = new List<ShpimentEventCommentsModel> { itemMockec.Object };
            var mockec = new Mock<IEnumerable<ShpimentEventCommentsModel>>();
            mockec.Setup(m => m.GetEnumerator()).Returns(() => itemsec.GetEnumerator());


            var data = new ListTrackingModel()
            {
                TrackingItems = mockpte.Object,
                shipmentCommentsItems = mockec.Object
            };
            return data;
        }

        static byte[] CreateSpecialByteArray(int length)
        {
            var arr = new byte[length];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = 0x20;
            }
            return arr;
        }

        #endregion Mock Data
    }
}
