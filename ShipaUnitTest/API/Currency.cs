﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Web.UI;
using FOCiS.SSP.WebApi.Controllers;
using FOCiS.SSP.DBFactory.Entities;
using System.Web.Http;
using System.Web.Http.Results;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net;

namespace ShipaUnitTest.API
{
    [TestClass]
    public class Currency
    {

        Mock<IMDMDataFactory> _mdb;
        Mock<IJobBookingDbFactory> _jdb;

        [TestInitialize]
        public void Currency_ApiController_Initialize()
        {
            _mdb = new Mock<IMDMDataFactory>(MockBehavior.Default);
            _jdb = new Mock<IJobBookingDbFactory>(MockBehavior.Default);
            AutoMapperConfig.RegisterMappings();
        }

        [TestMethod]
        public void GetCurrency_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object,_mdb.Object);

            _mdb.Setup<dynamic>(o =>
               o.FindCurrency(
                        It.IsAny<Int32>(),
                        It.IsAny<Int32>(),
                        It.IsAny<string>()
                        ))
               .Returns(new PagedResultsEntity<Select2Entity<string>>());

            // Act
            IHttpActionResult res = _con.GetCurrency(
                It.IsAny<string>(), It.IsAny<Int32>(), It.IsAny<Int32>()) as IHttpActionResult;

            var contentResult = res as OkNegotiatedContentResult<PagedResultsEntity<Select2Entity<string>>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
        }

        [TestMethod]
        public void GetCountry_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object, _mdb.Object);

            _mdb.Setup<PagedResultsEntity<Select3Entity<Int64>>>(o =>
               o.FindCountry(
                        It.IsAny<string>(),
                        It.IsAny<string>()
                        ))
               .Returns(new PagedResultsEntity<Select3Entity<Int64>>());

            // Act
            IHttpActionResult res = _con.GetCountry(
                It.IsAny<string>(), It.IsAny<string>()) as IHttpActionResult;

            var contentResult = res as OkNegotiatedContentResult<PagedResultsEntity<Select3Entity<Int64>>>;
            
            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
        }

        [TestMethod]
        public void ValidateZipcode_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object, _mdb.Object);
            var data = new ResultsEntity<zipcodeEntity>();

            _mdb.Setup<Task<ResultsEntity<zipcodeEntity>>>(o =>
               o.GetZipcodesByCity(
                        It.IsAny<string>(),
                        It.IsAny<string>()
                        ))
               .Returns(Task.FromResult(data));

            // Act
            var d = _con.ValidateZipcode(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());

            // Act

            var d2 = _con.ValidateZipcode("test", It.IsAny<string>(), It.IsAny<string>());

            //Assert
            Assert.IsTrue(true);

        }

        [TestMethod]
        public void IsZipcodesAvailable_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object, _mdb.Object);

            _mdb.Setup<ResultsEntity<zipcodeEntity>>(o =>
               o.IsZipcodesAvailable(
                        It.IsAny<string>()
                        ))
               .Returns(new ResultsEntity<zipcodeEntity>());

            // Act
            IHttpActionResult res = _con.IsZipcodesAvailable(
                It.IsAny<string>()) as IHttpActionResult;

            var contentResult = res as OkNegotiatedContentResult<ResultsEntity<zipcodeEntity>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
        }

        [TestMethod]
        public void GetStateIsapplicable_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object, _mdb.Object);

            _jdb.Setup<DataTable>(o =>
               o.GetStateConfigurationFlag(
                        It.IsAny<string>()
                        ))
               .Returns(new DataTable());

            // Act
            IHttpActionResult res = _con.GetStateIsapplicable(
                new GetStateIsapplicableDto()) as IHttpActionResult;

            var contentResult = res as OkNegotiatedContentResult<DataTable>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
        }

        [TestMethod]
        public void FillCityByCountryID_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object, _mdb.Object);

            _mdb.Setup<List<CityEntity>>(o =>
               o.GetCitiesByCountry(
                        It.IsAny<Int32>()
                        ))
               .Returns(
                   new List<CityEntity>() { 
                    new CityEntity(){
                        CODE = "",
                        NAME = "",
                        UNLOCATIONID = ""
                    }
                });

            // Act
            IHttpActionResult res = _con.FillCityByCountryID(
                new GetStateIsapplicableDto()) as IHttpActionResult;
            
            // Arrange
            _mdb.Setup<List<CityEntity>>(o =>
              o.GetCitiesByCountry(
                       It.IsAny<Int32>()
                       ));
            // Act
            HttpResponseMessage res2 = _con.FillCityByCountryID(
                new GetStateIsapplicableDto()) as HttpResponseMessage;
            
            // Assert
            Assert.AreEqual(HttpStatusCode.OK,res2.StatusCode);

        }

        [TestMethod]
        public void GetCurrencyRounded_Should_Work()
        {
            // Arrange
            var _con = new CurrencyController(_jdb.Object, _mdb.Object);

            _jdb.Setup<dynamic>(o =>
               o.GetCurrencyRounded(
                        It.IsAny<string>(),
                        It.IsAny<string>()
                        ))
              .Returns(GetCurrencyRounded_Data_Mock());

            // Act
            IHttpActionResult res = _con.GetCurrencyRounded(
                new GetCurrencyRoundedDto() { 
                    AMOUNT = "200", 
                    CURRENCY = "USD"
                }) as IHttpActionResult;

        }




        #region DataMocks
        public DataTable GetCurrencyRounded_Data_Mock()
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("currency", typeof(String));
            DataColumn dc2 = new DataColumn("DecimalCount", typeof(String));
            dt.Columns.Add(dc);
            dt.Columns.Add(dc2);
            DataRow dr = dt.NewRow();
            dr[0] = "";
            dt.Rows.Add(dr);
            return dt;
        }
        #endregion
    }
}
