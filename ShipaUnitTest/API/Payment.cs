﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.WebApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using FOCiS.SSP.DBFactory.Factory;
using System.Data;
using System.Net.Http;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Web.UI;

namespace ShipaUnitTest.API
{
    [TestClass]
    public class Payment
    {
        Mock<QuotationDBFactory> _qdb;
        Mock<IJobBookingDbFactory> _jdb;
        Mock<UserDBFactory> _udb;
        Mock<FOCiS.SSP.WebApi.OneSignal> _onesignal;

        [TestInitialize]
        public void Payment_ApiController_Initialize()
        {
            _qdb = new Mock<QuotationDBFactory>(MockBehavior.Default);
            _jdb = new Mock<IJobBookingDbFactory>(MockBehavior.Default);
            _udb = new Mock<UserDBFactory>(MockBehavior.Strict);
            _onesignal = new Mock<FOCiS.SSP.WebApi.OneSignal>(MockBehavior.Strict);
            AutoMapperConfig.RegisterMappings();
        }


        [TestMethod]
        public void StripePayment_Should_Return_Success()
        {
            // Arrange
            var _con = new Mock<PaymentController>(_jdb.Object, _udb.Object, _qdb.Object, _onesignal.Object);
            _con.CallBase = false;
            _jdb.Setup<DataSet>(o =>
                o.CheckPaymentStatus(It.IsAny<Int32>()))
                .Returns(Get_CheckPaymentStatus("NOTEXIST"));

            _jdb.Setup<PaymentEntity>(o =>
                o.GetPaymentDetailsByJobNumber(It.IsAny<Int32>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<double>()))
                .Returns(new PaymentEntity() { RECEIPTNETAMOUNT = 100 });

            _jdb.Setup(o =>
            o.SaveStripeTransactionDetails(
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>()
                ));

            // Act
            try
            {
                _con.Object.StripePayment(MockOnline_Request_Data());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            // Assert
            Assert.IsTrue(true);
        }

        #region Test Data
        private DataSet Get_CheckPaymentStatus(string data)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("col1", typeof(String));
            dt.Columns.Add(dc);
            DataRow dr = dt.NewRow();
            dr[0] = data;
            dt.Rows.Add(dr);
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

        private StripeModel MockOnline_Request_Data()
        {
            return new StripeModel()
            {
                JobNumber = It.IsAny<Int32>(),
                Currency = "USD"
            };
        }

        #endregion
    }
}
