﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FOCiS.SSP.Web.UI.Controllers.JobBooking;
using FOCiS.SSP.Models.QM;
using System.Web.Mvc;
using FOCiS.SSP.Web.UI.Controllers.Payment;
using FOCiS.SSP.Models.JP;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using FOCiS.SSP.DBFactory.Entities;
using Newtonsoft.Json;
using FOCiS.SSP.DBFactory.Factory;

namespace ShipaUnitTest
{
    [TestClass]
    public class JobBooking
    {
        private readonly JobBookingController _JobBooking;

        public JobBooking()
        {
            _JobBooking = new JobBookingController();

        }
        public JobBooking(
            JobBookingController jobBooking
            )
        {
            _JobBooking = jobBooking;
        }

        [TestMethod]
        public void getCountryWeekends()
        {
            JsonResult countryWeekends = _JobBooking.getCountryWeekends(105, 77);
            var countryWeekendsDe = (JArray)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(countryWeekends.Data));
            var res = (JObject)countryWeekendsDe[0];
            Assert.AreEqual(105, countryWeekendsDe[1]["Text"]);
            Assert.AreEqual(10000001, countryWeekendsDe[1]["Value"]);
            Assert.AreEqual(77, countryWeekendsDe[0]["Text"]);
            Assert.AreEqual(10000011, countryWeekendsDe[0]["Value"]);
        }

        [TestMethod]
        public void CheckPaymentStatus()
        {
            var ds = _JobBooking.CheckPaymentStatus(100);
            Assert.AreEqual("NOTEXIST", ds);
        }
    }
}

