﻿using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Web.UI.Controllers.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShipaUnitTest
{
    [TestClass]
    public class Payment
    {
        [TestMethod]
        public void OnlineCCSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.OnlineCCSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("OnlineCCSuccess", result.ViewName);
        }

        [TestMethod]
        public void OnlineCCFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.OnlineCCFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("OnlineCCFailure", result.ViewName);
        }

        [TestMethod]
        public void ServerError_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.ServerError(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("ServerError", result.ViewName);
        }

        [TestMethod]
        public void PaymentDone_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.PaymentDone(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("PaymentDone", result.ViewName);
        }

        [TestMethod]
        public void OfflinePBSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.OfflinePBSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("OfflinePBSuccess", result.ViewName);
        }

        [TestMethod]
        public void OfflineWTSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.OfflineWTSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("OfflineWTSuccess", result.ViewName);
        }

        [TestMethod]
        public void OfflinePBFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.OfflinePBFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("OfflinePBFailure", result.ViewName);
        }

        [TestMethod]
        public void OfflineWTFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.OfflineWTFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("OfflineWTFailure", result.ViewName);
        }

        [TestMethod]
        public void BusinessCreditSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.BusinessCreditSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("BusinessCreditSuccess", result.ViewName);
        }

        [TestMethod]
        public void BusinessCreditFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.BusinessCreditFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("BusinessCreditFailure", result.ViewName);
        }

        [TestMethod]
        public void MOnlineCCSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MOnlineCCSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MOnlineCCSuccess", result.ViewName);
        }

        [TestMethod]
        public void MOnlineCCFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MOnlineCCFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MOnlineCCFailure", result.ViewName);
        }

        [TestMethod]
        public void MServerError_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MServerError(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MServerError", result.ViewName);
        }

        [TestMethod]
        public void MPaymentDone_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MPaymentDone(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MPaymentDone", result.ViewName);
        }

        [TestMethod]
        public void MOfflinePBSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MOfflinePBSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MOfflinePBSuccess", result.ViewName);
        }

        [TestMethod]
        public void MOfflineWTSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MOfflineWTSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MOfflineWTSuccess", result.ViewName);
        }

        [TestMethod]
        public void MOfflinePBFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MOfflinePBFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MOfflinePBFailure", result.ViewName);
        }

        [TestMethod]
        public void MOfflineWTFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MOfflineWTFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MOfflineWTFailure", result.ViewName);
        }

        [TestMethod]
        public void MBusinessCreditSuccess_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MBusinessCreditSuccess(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MBusinessCreditSuccess", result.ViewName);
        }

        [TestMethod]
        public void MBusinessCreditFailure_Should_Return_View()
        {
            var con = new PaymentController();
            ViewResult result = con.MBusinessCreditFailure(new MPaymentResult()) as ViewResult;
            Assert.AreEqual("MBusinessCreditFailure", result.ViewName);
        }
    }
}
