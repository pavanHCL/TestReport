﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FOCiS.SSP.Web.UI.Controllers.KnowledgSeries;
using System.Web.Mvc;
using System.Reflection;
using System.IO;
using System.Web;

namespace ShipaUnitTest
{
    public interface IPathProvider
    {
        string MapPath(string path);
    }

    public class TestPathProvider : IPathProvider
    {
        public string MapPath(string path)
        {
            return "~/UploadedFiles/ShipForSuccess-Report-FINAL.pdf";
        }
    }  

    [TestClass]
    public class Knowledgeseries
    {
        //[TestMethod]
        //public void DownloadPressDoc()
        //{
        //    KnowledgeSeriesController ks = new KnowledgeSeriesController();
        //    ks.DownloadPressDoc();
        //    Assert.Inconclusive("A method that does not return a value cannot be verified.");
        //}

        [TestMethod]
        public void KnowledgeSeries()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.KnowledgeSeries() as ViewResult;
            Assert.AreEqual("KnowledgeSeries", result.ViewName);
        }

        [TestMethod]
        public void TheSMEKnowledgeGap()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.TheSMEKnowledgeGap() as ViewResult;
            Assert.AreEqual("TheSMEKnowledgeGap", result.ViewName);
        }

        [TestMethod]
        public void Introduction()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.Introduction() as ViewResult;
            Assert.AreEqual("Introduction", result.ViewName);
        }        

        [TestMethod]
        public void FirstShipment()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.FirstShipment() as ViewResult;
            Assert.AreEqual("FirstShipment", result.ViewName);
        }

        [TestMethod]
        public void FindingPartner()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.FindingPartner() as ViewResult;
            Assert.AreEqual("FindingPartner", result.ViewName);
        }

        [TestMethod]
        public void ExportFinance()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ExportFinance() as ViewResult;
            Assert.AreEqual("ExportFinance", result.ViewName);
        }

        [TestMethod]
        public void KeepingCompliant()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.KeepingCompliant() as ViewResult;
            Assert.AreEqual("KeepingCompliant", result.ViewName);
        }

        [TestMethod]
        public void MarketingIntel()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.MarketingIntel() as ViewResult;
            Assert.AreEqual("MarketingIntel", result.ViewName);
        }

        [TestMethod]
        public void ProtectingIP()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ProtectingIP() as ViewResult;
            Assert.AreEqual("ProtectingIP", result.ViewName);
        }

        [TestMethod]
        public void InternationalEcommerce()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.InternationalEcommerce() as ViewResult;
            Assert.AreEqual("InternationalEcommerce", result.ViewName);
        }

        [TestMethod]
        public void ProductStandards()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ProductStandards() as ViewResult;
            Assert.AreEqual("ProductStandards", result.ViewName);
        }

        [TestMethod]
        public void CustomsProcessing()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.CustomsProcessing() as ViewResult;
            Assert.AreEqual("CustomsProcessing", result.ViewName);
        }

        [TestMethod]
        public void ExportingEurope()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ExportingEurope() as ViewResult;
            Assert.AreEqual("ExportingEurope", result.ViewName);
        }

        [TestMethod]
        public void ExportClassification()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ExportClassification() as ViewResult;
            Assert.AreEqual("ExportClassification", result.ViewName);
        }

        [TestMethod]
        public void EmergingMarkets()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.EmergingMarkets() as ViewResult;
            Assert.AreEqual("EmergingMarkets", result.ViewName);
        }

        [TestMethod]
        public void ChineseExporters()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ChineseExporters() as ViewResult;
            Assert.AreEqual("ChineseExporters", result.ViewName);
        }

        [TestMethod]
        public void InsightsforAmazonFBA()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.InsightsforAmazonFBA() as ViewResult;
            Assert.AreEqual("InsightsforAmazonFBA", result.ViewName);
        }

        [TestMethod]
        public void LCLShipping()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.LCLShipping() as ViewResult;
            Assert.AreEqual("LCLShipping", result.ViewName);
        }

        [TestMethod]
        public void FCLShipping()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.FCLShipping() as ViewResult;
            Assert.AreEqual("FCLShipping", result.ViewName);
        }

        [TestMethod]
        public void ShipForSuccess()
        {
            KnowledgeSeriesController ks = new KnowledgeSeriesController();
            ViewResult result = ks.ShipForSuccess() as ViewResult;
            Assert.AreEqual("ShipForSuccess", result.ViewName);
        }
    }
}
