﻿using FindUserCountryName;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Controllers.KnowledgSeries
{

    //public interface IPathProvider
    //{
    //    string MapPath(string path);
    //}

    //public class ServerPathProvider : IPathProvider
    //{
    //    public string MapPath(string path)
    //    {
    //        return HttpContext.Current.Server.MapPath("~/UploadedFiles/ShipForSuccess-Report-FINAL.pdf");
    //    }
    //}

    /// <summary>
    /// KnowledgeSeries class for shipa knowledge series
    /// </summary>
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class KnowledgeSeriesController : BaseController
    {
        public KnowledgeSeriesController()
        {
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
        }
        /// <summary>
        /// About Shipa knowledge-series
        /// </summary>
        /// <returns></returns>
        [ActionName("knowledge-series")]
        public ActionResult KnowledgeSeries()
        {
            //for canonical tag     
            ViewBag.MetaTitle = "Knowledge Series | Shipafreight.com";
           ViewBag.MetaDescription = "ShipAfreight.com brings to you an intricate Knowledge Series.This is curated exclusively by our team of Logistics experts in which we will regularly report about news,rumors and insights about the industry & our product.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series";
            return View("KnowledgeSeries");
        }
         [ActionName("opt-in")]
        public ActionResult showopt()
        {
            //for canonical tag     
            ViewBag.MetaTitle = "Knowledge Series-show-opt | Shipafreight.com";
            ViewBag.MetaDescription = "ShipAfreight.com brings to you an intricate Knowledge Series.This is curated exclusively by our team of Logistics experts in which we will regularly report about news,rumors and insights about the industry & our product.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series";
            return View("KnowledgeSeries-showopt");
        }

        /// <summary>
        /// About Shipa sme-knowledge-gap
        /// </summary>
        /// <returns></returns>
        [ActionName("sme-knowledge-gap")]
        public ActionResult TheSMEKnowledgeGap()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "The SME Knowledge Gap | Shipafreight.com";
            ViewBag.MetaDescription = "SMEs employ most workers, account for 95% of all firms and are the lifeblood of the world’s economy, and yet remain understudied, underappreciated and underserved. Read more on the SME Knowledge Gap at shipafreight.com.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/sme-knowledge-gap";
            return View("TheSMEKnowledgeGap");
        }
        /// <summary>
        /// About ShipaFreightKnowledgeSeries
        /// </summary>
        /// <returns></returns>
         [ActionName("introduction")]
        public ActionResult Introduction()
        {
            //for canonical tag  
            ViewBag.MetaTitle = "Introduction to Knowledge Series | Shipafreight.com";
            ViewBag.MetaDescription = "The Shipa Freight Knowledge Series is a free collection of insights to help you start and grow your cross-border trade. Read our articles written by industry experts at Shipafreight.com. Visit Now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/introduction";
            return View("Introduction");
        }
        /// <summary>
        /// About Shipa FirstShipment
        /// </summary>
        /// <returns></returns>
        [ActionName("first-shipment")]
        public ActionResult FirstShipment()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Your First Overseas Shipment | Shipafreight.com";
            ViewBag.MetaDescription = "Make sure that the process of international shipping goes flawlessly for your first freight. Read our SME FAQ for your first overseas shipment & the elements to keep in mind for it, only at Shipafreight.com.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/first-shipment";
            return View("FirstShipment");
        }
        /// <summary>
        /// About Shipa FindingPartner
        /// </summary>
        /// <returns></returns>
        [ActionName("finding-partner")]
        public ActionResult FindingPartner()
        {
            //for canonical tag       
            ViewBag.MetaTitle = "Finding the Right Partner In New Market | Shipafreight.com";
            ViewBag.MetaDescription = "The success or failure of market entry attempts can be greatly affected by any international business partner you choose. Read our article on how can you find the best one, only at ShipAfreight.com.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/finding-partner";
            return View("FindingPartner");
        }
        /// <summary>
        /// About Shipa ExportFinance
        /// </summary>
        /// <returns></returns>
        [ActionName("export-finance")]
        public ActionResult ExportFinance()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "Export Finance FAQ | Knowledge Series | Shipafreight.com";
            ViewBag.MetaDescription = "Export finance helps organizations release working capital from cross border trade transactions, that could otherwise be tied up in invoices or purchase orders for up to 6 months. Read our Export Finance FAQ for small businesses.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/export-finance";
            return View("ExportFinance");
        }
        /// <summary>
        /// About Shipa Complaints
        /// </summary>
        /// <returns></returns>
        [ActionName("keeping-compliant")]
        public ActionResult KeepingCompliant()
        {
            //for canonical tag
            ViewBag.MetaTitle = "Keeping Compliant When Exporting | Shipafreight.com";
            ViewBag.MetaDescription = "What should you do to make sure you're making it happen to stay Compliant to Export Control Laws? If you are entrusted with creating and implementing an Export Control Compliance Program in your company, read this article on Shipafreight.com!"; 
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/keeping-compliant";
            return View("KeepingCompliant");
        }
        /// <summary>
        /// About Shipa MarketingIntel
        /// </summary>
        /// <returns></returns>
        [ActionName("marketing-intel")]
        public ActionResult MarketingIntel()
        {
            //for canonical tag   
            ViewBag.MetaTitle = "Market Intelligence Guide for SMEs | Shipafreight.com";
            ViewBag.MetaDescription = "Have you thought about exporting your goods or services abroad? With huge potential for growth, many businesses still find it a daunting experience. Read our guide at Shipafreight.com.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/marketing-intel";
            return View("MarketingIntel");
        }
        /// <summary>
        /// About Shipa ProtectingIP
        /// </summary>
        /// <returns></returns>
        [ActionName("protecting-ip")]
        public ActionResult ProtectingIP()
        {
            //for canonical tag  
            ViewBag.MetaTitle = "Protecting your intellectual property | Shipafreight.com";
            ViewBag.MetaDescription = "Lots of businesses have lost opportunities because they failed to realize the implications of their IP actions (or inactions). Consider taking steps to protect your intellectual property before you export abroad. Read more only at shipafreight.com.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/protecting-ip";
            return View("ProtectingIP");
        }
        /// <summary>
        /// About Shipa InternationalEcommerce
        /// </summary>
        /// <returns></returns>
        [ActionName("international-eCommerce")]
        public ActionResult InternationalEcommerce()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Guide to International e-Commerce | Shipafreight.com";
            ViewBag.MetaDescription = "Prepare your business for Global E-Commerce with our Guide to international e-commerce only at Shipafreight.com. Selling globally from your e-commerce site is more complex than domestic sales, but with huge rewards. Read more!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/international-ecommerce";
            return View("InternationalEcommerce");
        }
        /// <summary>
        /// About Shipa ProductStandards
        /// </summary>
        /// <returns></returns>
        [ActionName("product-standards")]
        public ActionResult ProductStandards()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Understanding Product Standards | Shipafreight.com";
            ViewBag.MetaDescription = "Learn about important points to consider when preparing a product for a new international market. The market considerations for packing and labelling your product you will need to comply with, only at our guide for understanding product standards, at shipafreight.com.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/product-standards";
            return View("ProductStandards");
        }
        /// <summary>
        /// About Shipa CustomsProcessing
        /// </summary>
        /// <returns></returns>
        [ActionName("customs-processing")]
        public ActionResult CustomsProcessing()
        {
            //for canonical tag      
            ViewBag.MetaTitle = "Pre-Arrival Customs Processing | Shipafreight.com";
            ViewBag.MetaDescription = "Voluntary pre-arrival declaration and processing arrangements are the solution to expedite release. According to many countries' Standards, Customs shall allow the lodging and registering of goods declarations and supporting documents prior to arrival of goods. Read more at shipafreight.com!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/customs-processing";
            return View("CustomsProcessing");
        }
        /// <summary>
        /// About Shipa ExportingEurope
        /// </summary>
        /// <returns></returns>
        [ActionName("exporting-europe")]
        public ActionResult ExportingEurope()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "Exporting to Europe | Shipafreight.com";
            ViewBag.MetaDescription = "Europe is the world's largest market. The continent offers lots of business opportunities, but competition is fierce. If you want to have a chance, read our guide only at Shipafreight.com!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/exporting-europe";
            return View("ExportingEurope");
        }
        /// <summary>
        /// About Shipa ExportClassification
        /// </summary>
        /// <returns></returns>
        [ActionName("export-classification")]
        public ActionResult ExportClassification()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "Understanding Export Classification | Shipafreight.com";
            ViewBag.MetaDescription = "An Export Control Classification Number (ECCN) is an alphanumeric code assigned to items by the United States Department of Commerce, Bureau of Industry and Security. Read more on Shipafreight.com!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/export-classification";
            return View("ExportClassification");
        }
        /// <summary>
        /// About Shipa EmergingMarkets
        /// </summary>
        /// <returns></returns>
        [ActionName("emerging-markets")]
        public ActionResult EmergingMarkets()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Exporting to Emerging Markets | Shipafreight.com";
            ViewBag.MetaDescription = "If your company is exporting to emerging markets, what are the things you should watch out for? Read our guide for Exporting to Emerging Markets only at shipafreight.com!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/emerging-markets";
            return View("EmergingMarkets");
        }
        /// <summary>
        /// About Shipa ChineseExporters
        /// </summary>
        /// <returns></returns>
        [ActionName("chinese-exporters")]
        public ActionResult ChineseExporters()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Agility - 3 Challenges Faced by Chinese Exporters When Going Global";
            ViewBag.MetaDescription = "For Chinese exporters, going global can present a number of challenges. Three of those challenges, along with some tips to overcome them, are discussed in this article from Shipa Freight—the platform that simplifies international freight shipping. For an instant freight quote, try our website now.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/chinese-exporters";
            return View("ChineseExporters");
        }
        /// <summary>
        /// About Shipa ChineseExporters
        /// </summary>
        /// <returns></returns>
        [ActionName("insights-for-amazon-fba")]
        public ActionResult InsightsforAmazonFBA()
        {
            //for canonical tag  
            ViewBag.MetaTitle = "Agility - 6 Facts about International Freight Shipping to Fulfillment by Amazon";
            ViewBag.MetaDescription = "Fulfillment by Amazon simplifies etail order fulfillment, but is not entirely without complexity, especially for international shippers. This article reveals six top things merchants should know about international freight shipping to FBA. For more help with FBA shipments, contact Shipa Freight now.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/insights-for-amazon-fba";
            return View("InsightsforAmazonFBA");
        }

        [ActionName("lcl-shipping")]
        public ActionResult LCLShipping()
        {
            //for canonical tag  
            ViewBag.MetaTitle = "An Essential LCL Shipping Guide for International Traders";
            ViewBag.MetaDescription = "If you’re unfamiliar with LCL shipping, this guide will help you understand what it is, when to use it, how to get your goods shipped, how LCL pricing works, and the important characteristics of this particular shipping model. If you decide it’s right for you, get a Shipa Freight LCL quote today.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/lcl-shipping";
            return View("LCLShipping");
        }

        [ActionName("fcl-shipping")]
        public ActionResult FCLShipping()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "A Brief Guide to the Fundamentals of FCL Shipping";
            ViewBag.MetaDescription = "Are you confused by container shipping terms and jargon? It’s actually not as complex as it might seem. FCL shipping for example, is really quite straightforward. This guide contains the basic information you need to get started. Once you’re ready to go, get your FCL quote online from Shipa Freight.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/fcl-shipping";
            return View("FCLShipping");
        }
        [ActionName("sme-internationalization")]
        public ActionResult smeInternationalizationSml()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "6 SME Internationalization Barriers Your Business Should Prepare For";
            ViewBag.MetaDescription = "SME internationalization has never been easier than it is today, but success still depends on overcoming some challenges. In this article, we outline 6 internationalization barriers to have on your radar when planning to enter markets abroad. To learn how Shipa Freight can help, contact us today.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/sme-internationalizationsml";
            return View("smeInternationalizationSml");
        }
        [ActionName("find-freight-forwarder")]
        public ActionResult findFreightForwarderSml()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "A Guide to Finding the Right International Freight Forwarder";
            ViewBag.MetaDescription = "Finding a freight forwarder is not difficult, but it can be harder to find one that’s a good fit for your business. This guide will help you plan and execute the first steps in finding the right forwarder. Meanwhile, it’s easy to try Shipa Freight. Just visit our platform to get a freight quote now.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/find-freightforwardersml";
            return View("findFreightForwarderSml");
        }
        /// <summary>
        /// Redirect method for KnowledgeSeries.
        /// </summary>
        /// <param name="actionname">action name</param>
        /// <returns></returns>
        public ActionResult Redirect(string actionname)
        {
            return RedirectToRoutePermanent(new
            {
                controller = "KnowledgeSeries",
                action = actionname
            });
        }

        [ActionName("ship-success")]
        public ActionResult ShipForSuccess()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Agility - Ship for Success: SMEs and International Tradess";
            ViewBag.MetaDescription = "Have a look at Shipa Freight’s global study of 800 SMEs from developed and emerging markets. Know how the smaller companies are remarkably upbeat about their ability to expand through trade. Visit Shipafreight.com now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/ship-success";
            return View("ShipForSuccess");
        }
        /// <summary>
        /// About shipa co2facts
        /// </summary>
        /// <returns></returns>
        [ActionName("co2-facts")]
        public ActionResult Co2Facts()
        {
            //for canonical tag  
            ViewBag.MetaTitle = "CO2 Emissions | Sustainable Freight Shipping | Shipafreight.com";
            ViewBag.MetaDescription = "Find out more about Shipa Freight's sustainable shipping methods and how we measure the CO2 emissions. To know if you can use the reports for payment of carbon taxes, visit us today  ShipAfreight.com";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "knowledge-series/co2-facts";
            return View("Co2Facts");
        }
        
        [AllowAnonymous]
        [ActionName("DownloadReportPDF")]
        public void DownloadPressDoc()
        {
            try
            {
                //ServerPathProvider path = new ServerPathProvider();
                //string file = path.MapPath(string.Empty);
                string file = Path.Combine(Server.MapPath("~/UploadedFiles/ShipForSuccess-Report-FINAL.pdf"));

                byte[] bytes = System.IO.File.ReadAllBytes(file);

                using (MemoryStream memoryStream = new MemoryStream())
                {

                    memoryStream.Close();
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.AddHeader("Content-Disposition", "inline; filename=" + "SME Research Release for Website" + ".pdf");
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
                //return  File(file, "application/docx", Path.GetFileName(file));
            }
            catch (Exception ex)
            {
                //
            }
        }
       

    }
}