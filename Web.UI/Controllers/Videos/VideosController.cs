﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Controllers.Videos
{
    /// <summary>
    /// Videos Class for Shipa Videos
    /// </summary>
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    public class VideosController : BaseController
    {
        public VideosController()
        {
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
        }
        /// <summary>
        /// Videos method for ShipaVideos
        /// </summary>
        /// <returns>shipa videos</returns>
        [ActionName("videos")]
        public ActionResult Videos()
        {
            //for canonical tag
            //ViewBag.CanonicalURL = url.ToLower();
            ViewBag.MetaTitle = "Freight Shipping Tutorial Videos | Shipafreight.com";
            ViewBag.MetaDescription = "ShipAfreight.com works perfectly for all your needs. Watch the video tutorials to quickly learn about the process of getting quick quotes, booking and tracking of shipments. Visit now!";
            var lowerurl = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos";
            ViewBag.CanonicalURL = lowerurl.ToLower();
            return View("ShipaVideos");
        }



        /// <summary>
        /// TrackaShipment method for TrackShipment video
        /// </summary>
        /// <returns>track-shipment video</returns>
        [ActionName("intro-shipa")]
        public ActionResult IntroShipaFreight()
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string VideoLink = mFactory.GetRegisterVideo("MarketingVideo");
            ViewBag.VideoLink = VideoLink;
            ViewBag.VideoTitle = "Get Started With Shipa Freight";
            ViewBag.VideoDescription = "Welcome to Shipa Freight, your online self-service platform for all your forwarding needs!.";
            ViewBag.Previous = "TrackaShipment";
            ViewBag.Next = "RegisterVideo";
            //for canonical tag          
            ViewBag.MetaTitle = "Get Started With Shipa Freight | Video | Shipafreight.com";
            ViewBag.MetaDescription = "Welcome to Shipa Freight, your online self-service platform for all your forwarding needs!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos/intro-shipa";
            return View("videoplayer");
        }

        /// <summary>
        /// RegisterVideo method for Register Video
        /// </summary>
        /// <returns>register video</returns>
        [ActionName("register")]
        public ActionResult RegisterVideo(string VideoType = "UNIVERSAL")
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string VideoLink = mFactory.GetRegisterVideo("RegisterVideo", VideoType);
            ViewBag.VideoLink = VideoLink;
            TempData["VideoLink"] = VideoLink;
            ViewBag.VideoTitle = "How to Register ?";
            ViewBag.VideoDescription = "You can get registered with ShipAfreight with a simple process. Registered users can save their quotes, book and track them online anytime.";
            ViewBag.Previous = "IntroShipaFreight";
            ViewBag.Next = "QuoteVideo";
            //for canonical tag     
            ViewBag.MetaTitle = "Register Your Account | Video | Shipafreight.com";
            ViewBag.MetaDescription = "Register with ShipAfreight.com and get access to all your quotes, bookings and shipments. Watch this video to learn more!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos/register";

            if (VideoType == "SWISS")
                return RedirectToAction("SwissLanding", "Quotation");
            else
                return View("videoplayer");
        }
        /// <summary>
        /// QuoteVideo method for Quote Video
        /// </summary>
        /// <returns>Quote video</returns>
        [ActionName("get-quote")]
        public ActionResult QuoteVideo(string VideoType = "UNIVERSAL")
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string VideoLink = mFactory.GetRegisterVideo("QuoteVideo", VideoType);
            ViewBag.VideoLink = VideoLink;
            TempData["VideoLink"] = VideoLink;
            ViewBag.VideoTitle = "Request A Quote";
            ViewBag.VideoDescription = "Simply choose your mode of transport, origin and destination, cargo details, and get access to hundreds of thousands of real-time instant quotes.";
            ViewBag.Previous = "RegisterVideo";
            ViewBag.Next = "BookaShipment";
            //for canonical tag            
            ViewBag.MetaTitle = "Get Freight Quote | Video | Shipafreight.com";
            ViewBag.MetaDescription = "Want to get a quick freight quote? Watch this video to get the best freight quote with ShipAfreight.com!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos/get-quote";
            if (VideoType == "SWISS")
                return RedirectToAction("SwissLanding", "Quotation");
            else
                return View("videoplayer");
        }
        /// <summary>
        /// BookaShipment method for BookaShipment video
        /// </summary>
        /// <returns>book-shipment video</returns>
        [ActionName("book-shipment")]
        public ActionResult BookaShipment(string VideoType = "UNIVERSAL")
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string VideoLink = mFactory.GetRegisterVideo("BookaShipment", VideoType);
            ViewBag.VideoLink = VideoLink;
            TempData["VideoLink"] = VideoLink;
            ViewBag.VideoTitle = "Book a Shipment";
            ViewBag.VideoDescription = "After getting a quote proceed to book your freight with a few simple steps. We will guide you through the most important parts and provide relevant information.";
            ViewBag.Previous = "QuoteVideo";
            ViewBag.Next = "ArrangePayment";
            //for canonical tag    
            ViewBag.MetaTitle = "Book Freight Shipment | Video | Shipafreight.com";
            ViewBag.MetaDescription = "Found your perfect Quote with ShipAfreight.com? Watch this video to book your shipment now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos/book-shipment";
            if (VideoType == "SWISS")
                return RedirectToAction("SwissLanding", "Quotation");
            else
                return View("videoplayer");
        }
        /// <summary>
        /// ArrangePayment method for ArrangePayment video
        /// </summary>
        /// <returns>arrange-payment video</returns>
        [ActionName("arrange-payment")]
        public ActionResult ArrangePayment(string VideoType = "UNIVERSAL")
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string VideoLink = mFactory.GetRegisterVideo("ArrangePayment", VideoType);
            ViewBag.VideoLink = VideoLink;
            TempData["VideoLink"] = VideoLink;
            ViewBag.VideoTitle = "Arrange Payment";
            ViewBag.VideoDescription = "Choose an option to pay online or offline with a wide range of options, including credit cards or applying for business credit online.";
            ViewBag.Previous = "BookaShipment";
            ViewBag.Next = "TrackaShipment";
            //for canonical tag   
            ViewBag.MetaTitle = "Arrange Freight Payment | Video | Shipafreight.com";
            ViewBag.MetaDescription = "ShipAfreight.com offers a variety of payment options both Online and Offline. Watch this video to choose your payment mode with us.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos/arrange-payment";
            if (VideoType == "SWISS")
                return RedirectToAction("SwissLanding", "Quotation");
            else
                return View("videoplayer");
        }
        /// <summary>
        /// TrackaShipment method for TrackShipment video
        /// </summary>
        /// <returns>track-shipment video</returns>
        [ActionName("track-shipment")]
        public ActionResult TrackaShipment(string VideoType = "UNIVERSAL")
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string VideoLink = mFactory.GetRegisterVideo("TrackaShipment", VideoType);
            ViewBag.VideoLink = VideoLink;
            TempData["VideoLink"] = VideoLink;
            ViewBag.VideoTitle = "Track Shipment";
            ViewBag.VideoDescription = "We make it easy to know where your freight is at every step of its journey. Automatic updates will be posted on your site and you will also receive email updates.";
            ViewBag.Previous = "ArrangePayment";
            ViewBag.Next = "IntroShipaFreight";
            //for canonical tag   
            ViewBag.MetaTitle = "Track Freight Shipment | Video | Shipafreight.com";
            ViewBag.MetaDescription = "Want to track your shipment at ShipAfreight.com? Watch this video to learn how to track your bookings with us.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "videos/track-shipment";
            if (VideoType == "SWISS")
                return RedirectToAction("SwissLanding", "Quotation");
            else
                return View("videoplayer");
        }

        /// <summary>
        /// Redirect method for Videos.
        /// </summary>
        /// <param name="videoname">video name</param>
        /// <returns></returns>
        public ActionResult Redirect(string videoname)
        {
            string actionname = null;
            if (videoname == "ArrangePayment")
            {
                actionname = "arrange-payment";
            }
            else if (videoname == "BookaShipment")
            {
                actionname = "book-shipment";
            }
            else if (videoname == "QuoteVideo")
            {
                actionname = "get-quote";
            }
            else if (videoname == "RegisterVideo")
            {
                actionname = "register";
            }
            else if (videoname == "TrackaShipment")
            {
                actionname = "track-shipment";
            }
            else
            {
                actionname = videoname;
            }
            return RedirectToRoutePermanent(new
            {
                controller = "Videos",
                action = actionname
            });
        }
    }
}