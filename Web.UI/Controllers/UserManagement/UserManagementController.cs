﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FOCiS.SSP.Library.Extensions;
using System.ComponentModel.DataAnnotations;
using System;
using System.Reflection;
using FOCiS.SSP.Models.UserManagement;
using System.Web.Security;
using System.Net.Mail;
using FOCiS.SSP.DBFactory.Factory;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using FOCiS.SSP.Models.DB;
using System.Drawing;
using FOCiS.SSP.Models.JP;
using Chikoti.Helpers;
using System.Threading.Tasks;
using FOCiS.SSP.Web.UI.Helpers;
using FindUserCountryName;
using System.Globalization;

namespace FOCiS.SSP.Web.UI.Controllers.UserManagement
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class UserManagementController : BaseController
    {
        IUserDBFactory _userdbfactory;

        public UserManagementController()
        {
            _userdbfactory = new UserDBFactory();
        }

        public UserManagementController(IUserDBFactory db)
        {
            _userdbfactory = db;
        }
        public ActionResult Login(string returnUrl, string ActiveTab, int? QuotationID)
        {

            if (returnUrl != null)
            {
                if (returnUrl.Contains("SessionTimeOut")) { returnUrl = returnUrl.Replace("SessionTimeOut", ""); }
            }
            UserModel mUIModel = new UserModel();
            TempData["ActiveTab"] = ActiveTab;

            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                //mUIModel.ReturnUrl = returnUrl;

                if (returnUrl == "/Quotation/SaveQuote")
                {
                    returnUrl = "/";
                }
                else
                {
                    ViewBag.ReturnUrl = returnUrl;
                }
            }

            if (QuotationID != null && QuotationID != 0)
            {
                UserDBFactory mFactory = new UserDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetQuoteDetails(Convert.ToInt32(QuotationID));
                ViewBag.FirstName = mDBEntity.GUESTNAME;
                ViewBag.LastName = mDBEntity.GUESTLASTNAME;
                ViewBag.EmailId = mDBEntity.GUESTEMAIL;
                ViewBag.CompanyName = mDBEntity.GUESTCOMPANY;
                ViewBag.QuoteId = QuotationID;
                ViewBag.GUESTCOUNTRYCODE = mDBEntity.GUESTCOUNTRYCODE;
                mUIModel.NOTIFICATIONSUBSCRIPTION = mDBEntity.NOTIFICATIONSUBSCRIPTION.Equals(1) ? true : false;
                mUIModel.SHIPMENTPROCESS = mDBEntity.SHIPMENTPROCESS;

            }
            //SetCaptchaKey();
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("Login", mUIModel);
        }
        public ActionResult LoginPage(string returnUrl, string ActiveTab, int? QuotationID)
        {

            if (returnUrl != null)
            {
                if (returnUrl.Contains("SessionTimeOut")) { returnUrl = returnUrl.Replace("SessionTimeOut", ""); }
            }
            UserModel mUIModel = new UserModel();
            TempData["ActiveTab"] = ActiveTab;

            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                //mUIModel.ReturnUrl = returnUrl;

                if (returnUrl == "/Quotation/SaveQuote")
                {
                    returnUrl = "/";
                }
                else
                {
                    ViewBag.ReturnUrl = returnUrl;
                }
            }

            if (QuotationID != null && QuotationID != 0)
            {
                UserDBFactory mFactory = new UserDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetQuoteDetails(Convert.ToInt32(QuotationID));
                ViewBag.FirstName = mDBEntity.GUESTNAME;
                ViewBag.LastName = mDBEntity.GUESTLASTNAME;
                ViewBag.EmailId = mDBEntity.GUESTEMAIL;
                ViewBag.CompanyName = mDBEntity.GUESTCOMPANY;
                ViewBag.QuoteId = QuotationID;
                ViewBag.GUESTCOUNTRYCODE = mDBEntity.GUESTCOUNTRYCODE;
                mUIModel.NOTIFICATIONSUBSCRIPTION = mDBEntity.NOTIFICATIONSUBSCRIPTION.Equals(1) ? true : false;
                mUIModel.SHIPMENTPROCESS = mDBEntity.SHIPMENTPROCESS;

            }
            //SetCaptchaKey();
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("LoginPage", mUIModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginAjax(UserModel umodel, string returnUrl)
        {
            bool IsLoginSuccess = false; string ResultMsg = string.Empty; string resendlnk = "";
            if (!string.IsNullOrEmpty(umodel.EmailId) && !string.IsNullOrEmpty(umodel.Password))
            {
                ReturnValues Obj = ValidateUser(umodel.EmailId, umodel.Password, umodel.RememberMe);
                IsLoginSuccess = Obj.IsLoginSuccess;
                if (IsLoginSuccess == true)
                {
                    QuotationDBFactory mFactory = new QuotationDBFactory();
                    string user = AESEncrytDecry.DecryptStringAES(umodel.EmailId);
                    string ipAddress = string.Empty;
                    if (Dns.GetHostAddresses(Dns.GetHostName()).Length > 0)
                    {
                        ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();
                    }

                    //---------------For Header Image - Added by Anil G-------------
                    // UpdateImageSession(user);
                    //---------------------------------------------------------------
                    HttpContext.Session["UserTrackid"] = mFactory.InsertUserTrackDetails(user, ipAddress, System.Configuration.ConfigurationManager.AppSettings["ServerName"]);
                }


                if (Obj.Message == "ResendActivationLink")
                {
                    //Obj.Message = "Account is not activated, please click on activation link sent in mail or to resend activation link ";
                    Obj.Message = FOCiS.Dictionary.App_GlobalResources.Resource.Accountisnotactivatedpleaseclickonactivationlinksentinmailortoresendactivationlink;
                    string strUserId = AESEncrytDecry.DecryptStringAES(umodel.EmailId);
                    string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                    resendlnk = Actionlink + "UserManagement/ResendActivation?mailid=" + strUserId;
                    ViewBag.resendlink = Actionlink + "UserManagement/ResendActivation?mailid=" + strUserId;

                }
                ResultMsg = Obj.Message;
            }

            returnUrl = returnUrl != null ? returnUrl : System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri.ToString();

            var Result = new
            {
                Result = IsLoginSuccess ? "Success" : "Error",
                ResultMsg = ResultMsg,
                ReturnUrl = returnUrl,
                Resendlink = resendlnk
            };
            return new JsonResult
            {
                Data = Result
            };
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(UserModel umodel, string ReturnUrl)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            ViewBag.ReturnUrl = ReturnUrl;
            if (!string.IsNullOrEmpty(umodel.EmailId) && !string.IsNullOrEmpty(umodel.Password))
            {
                ReturnValues Obj = ValidateUser(umodel.EmailId, umodel.Password, umodel.RememberMe);
                if (Obj.IsLoginSuccess)
                {
                    DBFactory.Entities.QuotationEntity mUIModel = (DBFactory.Entities.QuotationEntity)TempData["Quote"];
                    string user = AESEncrytDecry.DecryptStringAES(umodel.EmailId);
                    if (mUIModel != null)
                    {
                        mUIModel.CREATEDBY = user;
                        if (mUIModel.CurrencyMode != null)
                        {
                            string modifiedCargoValue = mUIModel.CurrencyMode.Replace(",", "");
                            decimal cargoValue = Convert.ToDecimal(string.Join("", modifiedCargoValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                            mUIModel.CARGOVALUE = cargoValue;
                        }

                        long mQuotationId = await Task.Run(() => mFactory.InsertSave(mUIModel, Session["HSCode"] == null ? "" : Session["HSCode"].ToString())).ConfigureAwait(false);
                        TempData["Quote"] = null;
                        Session["PreviewQUOTATIONID"] = mQuotationId;
                        return RedirectToAction("GetQuote", "Quotation", new { @id = mQuotationId });
                    }

                    string ipAddress = string.Empty;
                    if (Dns.GetHostAddresses(Dns.GetHostName()).Length > 0)
                    {
                        ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();
                    }
                    HttpContext.Session["UserTrackid"] = mFactory.InsertUserTrackDetails(user, ipAddress, System.Configuration.ConfigurationManager.AppSettings["ServerName"]);

                    //---------------For Header Image - Added by Anil G-------------
                    // UpdateImageSession(user);
                    //---------------------------------------------------------------

                    if ((Url.IsLocalUrl(ReturnUrl)) && (ReturnUrl.IndexOf("UserManagement/Login") < 0))
                        return Redirect(ReturnUrl);
                    else
                        return RedirectToAction("Index", "Quotation");
                }
                else
                {
                    if (Obj.Message == "ResendActivationLink")
                    {
                        string strUserId = AESEncrytDecry.DecryptStringAES(umodel.EmailId);
                        string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                        ViewBag.LoginMessage = FOCiS.Dictionary.App_GlobalResources.Resource.Accountisnotactivatedpleaseclickonactivationlinksentinmailortoresendactivationlink;
                        //ViewBag.LoginMessage = "Account is not activated, please click on activation link sent in mail or to resend activation link";
                        ViewBag.resendlink = Actionlink + "UserManagement/ResendActivation?mailid=" + strUserId;
                    }
                    else
                    {
                        ViewBag.LoginMessage = Obj.Message;
                    }
                }
            }
            //SetCaptchaKey();
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View(umodel);
        }

        private void UpdateImageSession(string user)
        {
            UserDBFactory UDF = new UserDBFactory();
            DataTable dt = UDF.GetUserImage(user);

            DataTable Dt1 = new DataTable();
            Dt1.Columns.Add("BASESTRING", typeof(string));
            Dt1.Columns.Add("IMGFILENAME", typeof(string));
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][1].ToString() != "")
                {
                    byte[] bt = (byte[])dt.Rows[0][1];
                    string base64 = Convert.ToBase64String(bt);

                    Dt1.Rows.Add(base64, dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1]);
                    Session["BaseIm"] = (string)base64; Session["filetype"] = dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1];
                }
            }
        }

        public ReturnValues ValidateUser(string UserId, string Password, bool RememberMe)
        {
            string strUserId = AESEncrytDecry.DecryptStringAES(UserId);
            string strPassword = AESEncrytDecry.DecryptStringAES(Password);
            UserDBFactory mFactory = new UserDBFactory();
            DBFactory.Entities.UserEntity mDBEntity = mFactory.GetUserDetails(strUserId);
            ReturnValues obj = new ReturnValues();

            var start = DateTime.Parse(mDBEntity.ServerDate.ToString());
            var oldDate = DateTime.Parse(mDBEntity.InstTime.ToString());
            bool timecheck = true;

            if (start.Subtract(oldDate) >= TimeSpan.FromMinutes(30))
            {
                //40 minutes were passed from start
                timecheck = false;
            }

            if (mDBEntity.ACCOUNTSTATUS != 2)
            {
                if (!string.IsNullOrEmpty(mDBEntity.PASSWORD))
                {
                    if ((mDBEntity.PASSWORD != Password) || (mDBEntity.ACCOUNTSTATUS == 0) || (mDBEntity.FAILEDPASSWORDATTEMPT >= 5))
                    {
                        obj.IsLoginSuccess = false;
                        if (timecheck == false)
                        {
                            mFactory.RestInstantPassword(strUserId);
                        }
                        if (timecheck == true)
                        {
                            if (!string.IsNullOrEmpty(mDBEntity.InstantPASSWORD))
                            {
                                if (mDBEntity.InstantPASSWORD == Password && mDBEntity.ACCOUNTSTATUS == 1)
                                {

                                    if (mDBEntity.FAILEDPASSWORDATTEMPT > 0) { mFactory.InsertFailedPasswordAttemtsCount(strUserId, "True"); }
                                    HttpContext.Session["USERINFO"] = mDBEntity;
                                    obj.IsLoginSuccess = true;
                                    FormsAuthentication.SetAuthCookie(strUserId, RememberMe);
                                    var myCookie = new HttpCookie("myCookie");
                                    myCookie.Values.Add("UserName", mDBEntity.FIRSTNAME);

                                    myCookie.Expires = DateTime.Now.AddYears(1);
                                    Response.Cookies.Add(myCookie);
                                    if (mDBEntity.IMGCONTENT != null)
                                    {
                                        byte[] bt = (byte[])mDBEntity.IMGCONTENT;
                                        string base64 = Convert.ToBase64String(bt);
                                        Session["BaseIm"] = (string)base64;
                                    }
                                    Session["LoginBy"] = "Support";

                                }
                                else if (mDBEntity.InstantPASSWORD != Password && mDBEntity.ACCOUNTSTATUS == 1)
                                {
                                    mFactory.InsertFailedPasswordAttemtsCount(strUserId, "False");
                                    //obj.Message = "Invalid Email Id or Password.";
                                    obj.Message = FOCiS.Dictionary.App_GlobalResources.Resource.InvalidEmailIdorPassword;
                                }

                            }
                        }


                        else if (mDBEntity.FAILEDPASSWORDATTEMPT >= 5 && mDBEntity.ACCOUNTSTATUS == 1)
                        {
                            obj.Message = "Your account has been locked out because of 5 or more unsuccessfull login attempts. Please Reset your Password or contact service desk";
                        }
                        else if (mDBEntity.PASSWORD != Password && mDBEntity.ACCOUNTSTATUS == 1)
                        {
                            //mFactory.InsertFailedPasswordAttemtsCount(strUserId, "False");
                            //obj.Message = "Invalid Email Id or Password.";
                            if (mDBEntity.InstantPASSWORD == Password) //Instant Pass check
                            {
                                //if (timecheck == true)
                                //{
                                if (mDBEntity.FAILEDPASSWORDATTEMPT > 0) { mFactory.InsertFailedPasswordAttemtsCount(strUserId, "True"); }
                                HttpContext.Session["USERINFO"] = mDBEntity;
                                obj.IsLoginSuccess = true;
                                FormsAuthentication.SetAuthCookie(strUserId, RememberMe);
                                var myCookie = new HttpCookie("myCookie");
                                myCookie.Values.Add("UserName", mDBEntity.FIRSTNAME);
                                myCookie.Expires = DateTime.Now.AddYears(1);
                                Response.Cookies.Add(myCookie);
                                if (mDBEntity.IMGCONTENT != null)
                                {
                                    byte[] bt = (byte[])mDBEntity.IMGCONTENT;
                                    string base64 = Convert.ToBase64String(bt);
                                    Session["BaseIm"] = (string)base64;
                                }
                                Session["LoginBy"] = "Support";
                                //}
                                //else
                                //{
                                //    obj.Message = "Your Instant password has been expired.Please regenerate again.";
                                //}
                            }
                            else
                            {
                                mFactory.InsertFailedPasswordAttemtsCount(strUserId, "False");
                                //obj.Message = "Invalid Email Id or Password.";
                                obj.Message = FOCiS.Dictionary.App_GlobalResources.Resource.InvalidEmailIdorPassword;
                            }

                        }

                        else
                            obj.Message = "ResendActivationLink";
                    }
                    else if ((mDBEntity.PASSWORD != Password)) // Admin Not matched
                    {
                        if (mDBEntity.InstantPASSWORD == Password) //Instant Pass check
                        {
                            if (timecheck == true)
                            {
                                if (mDBEntity.FAILEDPASSWORDATTEMPT > 0) { mFactory.InsertFailedPasswordAttemtsCount(strUserId, "True"); }
                                HttpContext.Session["USERINFO"] = mDBEntity;
                                obj.IsLoginSuccess = true;
                                FormsAuthentication.SetAuthCookie(strUserId, RememberMe);
                                var myCookie = new HttpCookie("myCookie");
                                myCookie.Values.Add("UserName", mDBEntity.FIRSTNAME);
                                myCookie.Expires = DateTime.Now.AddYears(1);
                                Response.Cookies.Add(myCookie);
                                if (mDBEntity.IMGCONTENT != null)
                                {
                                    byte[] bt = (byte[])mDBEntity.IMGCONTENT;
                                    string base64 = Convert.ToBase64String(bt);
                                    Session["BaseIm"] = (string)base64;
                                }
                                Session["LoginBy"] = "Support";
                            }
                            else
                            {
                                obj.Message = "Your Instant password has been expired.Please regenerate again.";
                            }
                        }
                        else
                        {
                            mFactory.InsertFailedPasswordAttemtsCount(strUserId, "False");
                            //obj.Message = "Invalid Email Id or Password.";
                            obj.Message = FOCiS.Dictionary.App_GlobalResources.Resource.InvalidEmailIdorPassword;
                        }
                    }
                    else
                    {
                        if (mDBEntity.FAILEDPASSWORDATTEMPT > 0) { mFactory.InsertFailedPasswordAttemtsCount(strUserId, "True"); }
                        HttpContext.Session["USERINFO"] = mDBEntity;
                        obj.IsLoginSuccess = true;
                        FormsAuthentication.SetAuthCookie(strUserId, RememberMe);
                        var myCookie = new HttpCookie("myCookie");
                        myCookie.Values.Add("UserName", mDBEntity.FIRSTNAME); //+ " " + mDBEntity.LASTNAME
                        myCookie.Expires = DateTime.Now.AddYears(1);
                        Response.Cookies.Add(myCookie);
                        Session["LoginBy"] = "";
                        if (mDBEntity.IMGCONTENT != null)
                        {
                            byte[] bt = (byte[])mDBEntity.IMGCONTENT;
                            string base64 = Convert.ToBase64String(bt);
                            Session["BaseIm"] = (string)base64;
                        }
                    }
                }
                else
                {
                    //obj.Message = "Invalid Email Id or Password.";
                    obj.Message = FOCiS.Dictionary.App_GlobalResources.Resource.InvalidEmailIdorPassword;
                }
            }
            else
            {
                obj.Message = "You are not authorized to login.";
            }
            return obj;
        }

        [Authorize]
        public ActionResult LogOut()
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if ((Object)HttpContext.Session["UserTrackid"] != null)
            {
                int TrackId = Convert.ToInt32(HttpContext.Session["UserTrackid"].ToString());
                mFactory.UpdateUserTrackDetails(TrackId, HttpContext.User.Identity.Name);
            }
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("W", ""));
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                if (cookie != "cookie_law")
                {
                    Request.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(Response.Cookies[cookie]);
                    Request.Cookies.Add(Request.Cookies[cookie]);
                }
            }
            HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();

            return RedirectToAction("Index", "Quotation");
        }
        public ActionResult IdleSessionOut()
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if ((Object)HttpContext.Session["UserTrackid"] != null)
            {
                int TrackId = Convert.ToInt32(HttpContext.Session["UserTrackid"].ToString());
                mFactory.UpdateUserTrackDetails(TrackId, HttpContext.User.Identity.Name);
            }
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                if (cookie != "cookie_law")
                {
                    Request.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(Response.Cookies[cookie]);
                    Request.Cookies.Add(Request.Cookies[cookie]);
                }
            }
            HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();

            return RedirectToAction("Index", "SessionTimeOut");

        }

        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];

            bool IsMailSent = false; string ResultMsg = string.Empty;
            TokenDetails T_obj = GenerateToken("PASSWORD_RESET_TOKEN_EXPIRY");
            UserDBFactory mFactory = new UserDBFactory();
            UserEntity mDBEntity = mFactory.GetUserDetails(model.EmailId);
            string strUserId = AESEncrytDecry.EncryptStringAES(model.EmailId);
            if (mDBEntity.USERID == null || mDBEntity.ACCOUNTSTATUS == 0)
            {
                ResultMsg = "Please provide valid credentials.";
            }
            //else if (mDBEntity.ACCOUNTSTATUS == 0)
            //{
            //    ResultMsg = "User is not activated, Please activate using activation link sent in mail.";
            //}
            else
            {
                mDBEntity.TOKEN = T_obj.Token;
                mDBEntity.TOKENEXPIRY = T_obj.TokenExpiry;
                mFactory.UpdateUserToken(mDBEntity);
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                string subject = "";
                subject = "Reset your password for " + model.EmailId + EnvironmentName;
                string userName = mDBEntity.FIRSTNAME;// +" " + mDBEntity.LASTNAME;

                string resetLink = Url.Action("ResetPassword", "UserManagement", new { rt = T_obj.Token, UserId = strUserId }, "http");
                resetLink = Actionlink + "UserManagement/ResetPassword?" + resetLink.Split('?')[1];

                string body = string.Empty;

                using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/Reset-Password-template.html")))
                { body = reader.ReadToEnd(); }

                body = body.Replace("{user}", ((string.IsNullOrEmpty(userName) != true) ? userName : string.Empty));
                body = body.Replace("{url}", resetLink);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                MailMessage message = new MailMessage();
                message.To.Add(model.EmailId);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                IsMailSent = SendMailNotification(message, "Forget password", model.EmailId, body);
                if (IsMailSent)
                {
                    ViewBag.Message = "Reset password email has been sent.";
                    ResultMsg = "Reset password email has been sent to" + userName + ".";
                }
            }

            var Result = new
            {
                Result = IsMailSent ? "Success" : "Error",
                ResultMsg = ResultMsg
            };
            return new JsonResult
            {
                Data = Result
            };
        }

        /// <summary>
        /// Create Email body by using HTML Template.
        /// Pass the replaceing Element
        /// </summary>
        /// <param name="userName">User Name</param>
        /// <param name="url">URL</param>
        /// <returns>String Email Body</returns>
        private string createEmailBody(string userName, string url)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/account-activation-temp.html")))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{user}", ((string.IsNullOrEmpty(userName) != true) ? userName : string.Empty));
            body = body.Replace("{url}", url);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            return body;
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string rt, string UserId)
        {
            TempData["ReturnToken"] = rt;
            TempData["EmailId"] = UserId;
            TempData["Message"] = "ResetPassword";
            return RedirectToAction("Index", "Quotation");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            UserDBFactory mFactory = new UserDBFactory();
            string returnUrl = string.Empty; string ResultMsg = string.Empty; bool IsLoginSuccess = false;
            string strUserId = AESEncrytDecry.DecryptStringAES(model.EmailId);
            string strPassword = AESEncrytDecry.EncryptStringAES(model.Password);
            bool resetResponse = false;
            byte[] data = Convert.FromBase64String(model.ReturnToken);
            //DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            DateTime when = DateTime.UtcNow;
            UserEntity mDBEntity = mFactory.GetUserDetails(strUserId);
            if (when < mDBEntity.TOKENEXPIRY && model.ReturnToken == mDBEntity.TOKEN)
            {
                resetResponse = true;
            }
            if (resetResponse)
            {
                ResultMsg = "Successfully Changed";
                mFactory.ResetPassword(strUserId, strPassword);
                FormsAuthentication.SetAuthCookie(strUserId, false);
                var myCookie = new HttpCookie("myCookie");
                myCookie.Values.Add("UserName", mDBEntity.FIRSTNAME); //+ " " + mDBEntity.LASTNAME
                myCookie.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(myCookie);

                IsLoginSuccess = true;
            }
            else
            {
                ViewBag.Message = "Error in Reset Password.";
            }
            returnUrl = Url.Action("ResetPassword", "UserManagement");
            var Result = new
            {
                Result = IsLoginSuccess ? "Success" : "Error",
                ResultMsg = ResultMsg,
                ReturnUrl = returnUrl
            };
            return new JsonResult
            {
                Data = Result
            };
        }

        public ActionResult SignUp()
        {
            TempData["ActiveTab"] = "signUp";
            return RedirectToAction("Login", "UserManagement");
        }

        /// <summary>
        /// Sign Up Post Method
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUp(UserModel model, string ReturnUrl, int? Id)
        {
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            UserDBFactory mFactory = new UserDBFactory();
            UserEntity mDBEntity = mFactory.GetUserDetails(model.EmailId);
            if (mDBEntity.USERID == null)
            {

                model.ISDCode = Request.Params["hdnISDCode"];

                mDBEntity.USERID = model.EmailId;
                mDBEntity.EMAILID = AESEncrytDecry.EncryptStringAES(model.EmailId);
                mDBEntity.PASSWORD = AESEncrytDecry.EncryptStringAES(model.Password);
                mDBEntity.USERTYPE = 2;
                mDBEntity.ISTEMPPASSWORD = 0;
                mDBEntity.ACCOUNTSTATUS = 0;
                mDBEntity.ISDCODE = model.ISDCode;
                mDBEntity.MOBILENUMBER = Regex.Replace(model.MobileNumber, @"[-+() ]", "");
                mDBEntity.FIRSTNAME = model.FirstName;
                mDBEntity.LASTNAME = model.LastName;

                mDBEntity.ISTERMSAGREED = 1;
                mDBEntity.COMPANYNAME = model.CompanyName;
                mDBEntity.COUNTRYNAME = model.CountryName;
                mDBEntity.COUNTRYCODE = model.CountryCode;
                mDBEntity.COUNTRYID = model.UACountryId;
                mDBEntity.ISDCODE = model.ISDCode;
                // Generae password token that will be used in the email link to authenticate user
                TokenDetails token = GenerateToken("USER_ACTIVATION_TOKEN_EXPIRY");
                mDBEntity.TOKEN = token.Token;
                mDBEntity.TOKENEXPIRY = token.TokenExpiry;
                mDBEntity.NOTIFICATIONSUBSCRIPTION = model.NOTIFICATIONSUBSCRIPTION;
                mDBEntity.SHIPMENTPROCESS = model.SHIPMENTPROCESS;
                mDBEntity.SHIPPINGEXPERIENCE = model.SHIPPINGEXPERIENCE;

                //Save Call...
                long Result = mFactory.Save(mDBEntity);
                QuotationDBFactory mFactoryq = new QuotationDBFactory();
                if (Id != null && Id != 0)
                {
                    mFactoryq.SaveQuoteForLater(model.EmailId, Convert.ToInt32(Id));
                }
                long mQuotationId = 0;
                DBFactory.Entities.QuotationEntity mUIModel = (DBFactory.Entities.QuotationEntity)TempData["Quote"];
                if (mUIModel != null)
                {
                    mUIModel.CREATEDBY = model.EmailId;
                    mQuotationId = await Task.Run(() => mFactoryq.InsertSave(mUIModel, Session["HSCode"] == null ? "" : Session["HSCode"].ToString())).ConfigureAwait(false);
                    TempData["Quote"] = null;
                }

                // Generate the html link sent via email
                string strUserId = AESEncrytDecry.EncryptStringAES(model.EmailId);
                //  string strEncryptURL = string.IsNullOrEmpty(ReturnUrl) ? null : AESEncrytDecry.EncryptStringAES(ReturnUrl);
                string strEncryptURL = AESEncrytDecry.EncryptStringAES("/");
                string subject = "";

                string EnvironmentName = DynamicClass.GetEnvironmentName();
                subject = "Account Activation for user " + model.EmailId + EnvironmentName;

                string resetLink = Url.Action("ActivateUser", "UserManagement", new { rt = token.Token, UserId = strUserId, ReturnUrl = strEncryptURL }, "http");
                //string resetLink = Url.Action("ActivateUser", "UserManagement", new { rt = token.Token, UserId = strUserId, ReturnUrl = strEncryptURL, QuoteId = mQuotationId }, "http");

                resetLink = Actionlink + "UserManagement/ActivateUser?" + resetLink.Split('?')[1];

                string userName = mDBEntity.FIRSTNAME;// +" " + mDBEntity.LASTNAME;               
                string body = createEmailBody(userName, resetLink);

                MailMessage message = new MailMessage();
                message.To.Add(model.EmailId);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                bool IsMailSent = SendMailNotification(message, "Signup request", model.EmailId, body);
                if (IsMailSent)
                {
                    TempData["RegSuccess"] = "Registration Successful";
                    //  TempData["TosterMsg"] = "Registration successful. Activation email has been sent.";
                }

                if ((ReturnUrl != null) && (ReturnUrl != "/") && (ReturnUrl.IndexOf("UserManagement/Login") < 0))
                {
                    if (ReturnUrl.Split('/').Length > 2)
                    {
                        if (ReturnUrl.Split('/') != null && ReturnUrl.Split('/')[2] != null)
                        {
                            if (ReturnUrl.Split('/').Length > 3)
                            {
                                int QuoteId = Convert.ToInt32(ReturnUrl.Split('/')[3].ToString());
                                QuotationDBFactory Qdb = new QuotationDBFactory();
                                Qdb.SaveQuoteForLater(model.EmailId, QuoteId);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Quotation");
                            }
                        }
                    }
                    return Redirect(ReturnUrl);
                }
                else
                    return RedirectToAction("Index", "Quotation");
            }
            else
            {
                if (mDBEntity.ACCOUNTSTATUS == 0)
                {
                    ViewBag.SignUpMessage = FOCiS.Dictionary.App_GlobalResources.Resource.Useralreadyexiststoresendactivationlink;
                    //ViewBag.SignUpMessage = "User already exists,to resend activation link";
                    // string strUserId = AESEncrytDecry.EncryptStringAES(model.EmailId);
                    ViewBag.activationlink = Actionlink + "UserManagement/ResendActivation?mailid=" + model.EmailId;
                    // TempData["TosterMsg"] = "User Already Exist!.";
                    TempData["ActiveTab"] = "signUp";
                    ViewBag.ReturnUrl = ReturnUrl;
                    // model = null;
                }
                else
                {
                    //model = null;
                    ViewBag.SignUpMessage = FOCiS.Dictionary.App_GlobalResources.Resource.Useralreadyexistspleaseloginwithyourcredentials;
                    //ViewBag.SignUpMessage = "User already exists, please login with your credentials.";
                    // TempData["TosterMsg"] = "User Already Exist!.";
                    TempData["ActiveTab"] = "signUp";
                    ViewBag.ReturnUrl = ReturnUrl;
                    //SetCaptchaKey();
                }
                //Meta title and Meta description
                ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
                ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
                return View("Login", model);
            }
        }

        public ActionResult ResendActivation(string mailid)
        {
            //string mail = mailid.Replace(" ", "+");
            //string email = AESEncrytDecry.DecryptStringAES(mail);
            // string strUserId = AESEncrytDecry.DecryptStringAES(mailid);

            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];

            UserDBFactory mFactory = new UserDBFactory();
            UserEntity mDBEntity = mFactory.GetUserDetails(mailid);
            // Generae password token that will be used in the email link to authenticate user
            TokenDetails token = GenerateToken("USER_ACTIVATION_TOKEN_EXPIRY");
            mDBEntity.TOKEN = token.Token;
            mDBEntity.TOKENEXPIRY = token.TokenExpiry;

            // Generate the html link sent via email
            string strUserId = AESEncrytDecry.EncryptStringAES(mailid);
            //  string strEncryptURL = string.IsNullOrEmpty(ReturnUrl) ? null : AESEncrytDecry.EncryptStringAES(ReturnUrl);
            string strEncryptURL = AESEncrytDecry.EncryptStringAES("/");

            string subject = "Account Activation for user " + mailid;
            string resetLink = Url.Action("ActivateUser", "UserManagement", new { rt = token.Token, UserId = strUserId, ReturnUrl = strEncryptURL }, "http");
            //string resetLink = Url.Action("ActivateUser", "UserManagement", new { rt = token.Token, UserId = strUserId, ReturnUrl = strEncryptURL, QuoteId = mQuotationId }, "http");

            resetLink = Actionlink + "UserManagement/ActivateUser?" + resetLink.Split('?')[1];

            string userName = mDBEntity.FIRSTNAME;// +" " + mDBEntity.LASTNAME;

            string body = createEmailBody(userName, resetLink);

            MailMessage message = new MailMessage();
            message.To.Add(mailid);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            bool IsMailSent = SendMailNotification(message, "Resend activation", mailid, body);
            if (IsMailSent)
            {
                mFactory.UpdateUserToken(mDBEntity.TOKEN, mDBEntity.TOKENEXPIRY, mailid);
                TempData["MasterPgMessage"] = FOCiS.Dictionary.App_GlobalResources.Resource.Activationemailsent;
                TempData["TosterMsg"] = FOCiS.Dictionary.App_GlobalResources.Resource.Activationemailsent;
            }
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            UserModel Umodel = new UserModel();

            return View("Login", Umodel);
        }
        public ActionResult ActivateUser(string rt, string UserId, string ReturnUrl)
        {
            try
            {
                bool resetResponse = false; byte[] data = null; string strUserId = string.Empty; string strDecryptURL = string.Empty;
                try
                {
                    data = Convert.FromBase64String(rt);
                }
                catch (Exception ex)
                {
                    TempData["MasterPgMessage"] = "Error in activation url, Please contact Shipa freight help desk.";
                    return RedirectToAction("Index", "Quotation");
                }

                DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
                UserDBFactory mFactory = new UserDBFactory();
                try
                {
                    strUserId = AESEncrytDecry.DecryptStringAES(UserId);
                    strDecryptURL = string.IsNullOrEmpty(ReturnUrl) ? null : AESEncrytDecry.DecryptStringAES(ReturnUrl);
                }
                catch (Exception ex)
                {
                    TempData["MasterPgMessage"] = "Error in activation url, Please contact Shipa freight help desk.";
                    return RedirectToAction("Index", "Quotation");
                }

                //clear temp data
                TempData["MasterPgMessage"] = "";

                UserEntity mDBEntity = mFactory.GetUserDetails(strUserId);
                if (mDBEntity.ACCOUNTSTATUS == 0)
                {
                    if (mDBEntity.TOKEN == rt)
                    {
                        if (when < mDBEntity.TOKENEXPIRY && rt == mDBEntity.TOKEN)
                        {
                            resetResponse = true;
                        }
                        if (resetResponse)
                        {
                            mFactory.ActivateUserAccountStatus(strUserId);
                            FormsAuthentication.SetAuthCookie(strUserId, false);
                            var myCookie = new HttpCookie("myCookie");
                            myCookie.Values.Add("UserName", mDBEntity.FIRSTNAME); //+ " " + mDBEntity.LASTNAME
                            myCookie.Expires = DateTime.Now.AddYears(1);
                            Response.Cookies.Add(myCookie);
                            TempData["UserProf"] = "Update User Profile";

                            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                            string EnvironmentName = DynamicClass.GetEnvironmentName();
                            string subject = "";
                            subject = "Your Shipa Freight account is now active" + EnvironmentName;

                            string body = string.Empty;

                            string mapPath = Server.MapPath("~/App_Data/account-active-temp.html");
                            using (StreamReader reader = new StreamReader(mapPath))
                            { body = reader.ReadToEnd(); }
                            body = body.Replace("{user}", (Request.Cookies["myCookie"].Values["UserName"].ToString()));
                            body = body.Replace("{appurl}", Actionlink);
                            body = body.Replace("{profilelink}", Actionlink + "/UserManagement/MyProfile");
                            body = body.Replace("{creditlink}", Actionlink + "/UserManagement/Credit");
                            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                            body = body.Replace("{videolink}", Actionlink + "/ShipaVideos");
                            SendMail(mDBEntity.USERID, body, subject, "ActiveUser", UserId, true);
                            QuotationDBFactory mFactory1 = new QuotationDBFactory();
                            string ipAddress = string.Empty;
                            if (Dns.GetHostAddresses(Dns.GetHostName()).Length > 0)
                            {
                                ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();
                            }
                            HttpContext.Session["UserTrackid"] = mFactory1.InsertUserTrackDetails(strUserId, ipAddress, EnvironmentName);
                        }
                        else
                        {
                            TempData["MasterPgMessage"] = "Error While activating the user.";
                        }

                        if (strDecryptURL != null)
                            return Redirect(strDecryptURL);
                        else
                            return RedirectToAction("Index", "Quotation");
                    }
                    return RedirectToAction("Index", "Quotation");
                }
                TempData["MasterPgMessage"] = "User already activated please login.";
                TempData["TosterMsg"] = "User already activated please login.";
                return RedirectToAction("Index", "Quotation");
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        public bool SendMailNotification(MailMessage message, string submodule, string referenceNo, string body)
        {
            bool IsMailSent = false;
            SmtpClient mySmtpClient = new SmtpClient();
            if (mySmtpClient.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory && mySmtpClient.PickupDirectoryLocation.StartsWith("~"))
            {
                string root = AppDomain.CurrentDomain.BaseDirectory;
                string pickupRoot = mySmtpClient.PickupDirectoryLocation.Replace("~/", root);
                pickupRoot = pickupRoot.Replace("/", @"\");
                mySmtpClient.PickupDirectoryLocation = pickupRoot;
            }
            try
            {
                mySmtpClient.Send(message);
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", referenceNo, "", "",
    "UserManagement", submodule, referenceNo, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());

                IsMailSent = true;
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Issue sending email: " + e.Message);
            }
            return IsMailSent;
        }

        public TokenDetails GenerateToken(string ConfigName)
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            UserDBFactory mFactory = new UserDBFactory();
            string ConfigValue = mFactory.GetUMConfigValue(ConfigName);
            TokenDetails tobj = new TokenDetails();
            tobj.Token = token;
            tobj.TokenExpiry = DateTime.UtcNow.AddHours(int.Parse(ConfigValue));
            return tobj;
        }

        [Authorize]
        public ActionResult MyProfile(int? activetab)
        {

            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            if (activetab == 2)
            {
                ViewBag.IncompleteProfile = "Credit details Not Saved, Please Complete your Personnal details before requesting for Credit.";
            }
            if (activetab == 3)
            {
                ViewBag.IncompleteProfile = "Profile not updated due to incorrect details";
            }
            UserDBFactory mFactory = new UserDBFactory();
            DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 1);
            UserProfileModel mUIModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);

            if (mUIModel.UACountryId == 0)
            {
                mUIModel.UACountryId = mUIModel.CountryId;
                mUIModel.UACountryCode = mUIModel.CountryCode;
                mUIModel.UACountryName = mUIModel.CountryName;
            }

            mUIModel.EmailId = mUIModel.UserId;

            int CountryId = 0;

            MDMDataFactory mdmFactory = new MDMDataFactory();
            // IList<CountryEntity> country = mdmFactory.GetCountries(string.Empty, string.Empty);
            if (mDBEntity.UASTATEID > 0)
                CountryId = Convert.ToInt32(mDBEntity.UACOUNTRYID);
            IList<StateEntity> state = mdmFactory.GetStatesByCountry(CountryId, string.Empty, string.Empty);
            //ViewBag.country = country;
            ViewBag.state = state;
            ViewBag.ImageName = mDBEntity.IMGFILENAME;
            if (mDBEntity.IMGCONTENT != null)
            {
                byte[] bt = (byte[])mDBEntity.IMGCONTENT;
                string base64 = Convert.ToBase64String(bt);
                Session["BaseIm"] = (string)base64;
            }
            MultipleModel MList = new MultipleModel();
            MList.UserProfileModel = mUIModel;
            ViewBag.TabStaus = "Profile"; ViewBag.ActiveTab = "personal-view";

            ViewBag.bookingcount = mFactory.getJobBookingCount(HttpContext.User.Identity.Name);

            if (ViewBag.bookingcount == 0)
            {
                ViewBag.QuotesCount = mFactory.GetQuotationCount(HttpContext.User.Identity.Name);
            }
            else
            { ViewBag.QuotesCount = 0; }
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("ProfilePage", mUIModel);
        }

        [Authorize]
        public ActionResult Parties(string SearchString = "")
        {
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            ViewBag.SearchString = SearchString;
            UserDBFactory mFactory = new UserDBFactory();
            UserProfileEntity mDBEntity = mFactory.GetUserPartyMasterForProfile(HttpContext.User.Identity.Name, SearchString);

            UserProfileModel Model = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
            //-----Map PartyList To Model----
            List<UserPartyMasterModel> mPartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(mDBEntity.PartyMaster.ToList());

            Model.PartiesModel = mPartyModel;
            ViewBag.ImageName = mDBEntity.IMGFILENAME;
            ViewBag.TabStaus = "Party";

            //Update Data to Session for Edit Calls
            Session["PartyData"] = (List<UserPartyMasterModel>)mPartyModel;
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("ProfilePage", Model);
        }

        [Authorize]
        public ActionResult Credit(int? activetab)
        {
            UserProfileModel model = new UserProfileModel();
            SendMailToGSSCforExistingCustomer(model);
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            if (activetab == 2)
            {
                ViewBag.IncompleteProfile = "Credit details Not Saved, Please Complete your Personnal details before requesting for Credit.";
            }
            if (activetab == 3)
            {
                ViewBag.IncompleteProfile = "Profile not updated due to incorrect details";
            }

            UserDBFactory mFactory = new UserDBFactory();
            DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 2);

            UserProfileModel mUIModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
            mUIModel.EmailId = mUIModel.UserId;

            if (mUIModel.UAUserAdtlsId != null && mUIModel.UAUserAdtlsId != 0) { ViewBag.Useraddressdata = 1; }

            if (mUIModel.REQUESTEDCREDITLIMIT != 0)
            {
                mUIModel.REQUESTEDCREDITLIMITCURRENCY = mUIModel.REQUESTEDCREDITLIMIT.ToString();
            }

            if (mUIModel.ANNUALTURNOVER != 0)
            {
                mUIModel.ANNUALTURNOVERCURRENCY = mUIModel.ANNUALTURNOVER.ToString();
            }
            if (mUIModel.INDUSTRYID == null)
            {
                mUIModel.INDUSTRYID = mUIModel.Industry;
            }
            int annualdecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PREFERREDCURRENCYCODE);
            mUIModel.AnnualTurnOverDecimal = annualdecimalPoint;
            MultipleModel MList = new MultipleModel();
            MList.UserProfileModel = mUIModel;
            ViewBag.ActiveTab = "credit-edit";
            ViewBag.TabStaus = "Credit";
            decimal credused = mUIModel.APPROVEDCREDITLIMIT - mUIModel.CURRENTOSBALANCE;
            decimal dc = Math.Round((decimal)credused, 2);
            ViewBag.CreditUsed = dc;
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("ProfilePage", mUIModel);

        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Credit(UserProfileModel model)
        {
            UserDBFactory mFactory = new UserDBFactory();
            string hdnvalue = Convert.ToString(Request.Params["hdncreditedit"]);
            string hdnCreditRequested = Convert.ToString(Request.Params["hdncreditrequested"]);
            if (hdnvalue != "1" && hdnvalue != "2")
            {
                // model.PREFERREDCURRENCY = model.CURRENCYID;
                // model.PREFERREDCURRENCYCODE = model.CURRENCYID;
                model.UACountryId = model.UACountryDummyId;
                model.MobileNumber = Regex.Replace(model.MobileNumber, @"[-+() ]", "");
                string UAHouseNo = (model.UAHouseNo != null) ? model.UAHouseNo + "," : null;
                string UABuildingName = (model.UABuildingName != null) ? model.UABuildingName + "," : null;
                string UAAddressline2 = (model.UAAddressline2 != null) ? model.UAAddressline2 + "," : null;
                model.UAFullAddress = UAHouseNo + UABuildingName + model.UAAddressline1 + ", " + UAAddressline2 + model.UACountryName + ", " + model.UAStateName + ", " + model.UACityName + ", " + model.UAPostCode;
                model.ISDCode = Request.Params["hdnISDCode"];
                model.CountryId = model.UACountryId;
                model.CountryCode = model.UACountryCode;
                model.CountryName = model.UACountryName;

                if (!model.EXISTINGCUSTOMER)
                {
                    model.USEMYCREIDT = false;
                }

                var myCookie = new HttpCookie("myCookie");
                myCookie.Values.Add("UserName", model.FirstName);
                myCookie.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(myCookie);
            }

            if (model.ANNUALTURNOVERCURRENCY != null)
            {
                string modifiedAnnualCurrencyValue = model.ANNUALTURNOVERCURRENCY.Replace(",", "");
                decimal CurrencyValue = Convert.ToDecimal(string.Join("", modifiedAnnualCurrencyValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                model.ANNUALTURNOVER = CurrencyValue;
            }
            if (model.REQUESTEDCREDITLIMITCURRENCY != null)
            {
                string modifiedRequestCurrencyValue = model.REQUESTEDCREDITLIMITCURRENCY.Replace(",", "");
                decimal CurrencyValue = Convert.ToDecimal(string.Join("", modifiedRequestCurrencyValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                model.REQUESTEDCREDITLIMIT = CurrencyValue;
            }
            UserProfileEntity Userobj = Mapper.Map<UserProfileModel, UserProfileEntity>(model);
            DataSet ds = new DataSet();
            int Count = 1;
            if (hdnvalue == "1")
            {
                ds = mFactory.IsProfileUpdated(HttpContext.User.Identity.Name.ToString());
                Count = Convert.ToInt32(ds.Tables[0].Rows[0]["NOTIFICATIONCOUNT"].ToString());
            }
            if (Userobj.USERID.ToUpper() != HttpContext.User.Identity.Name.ToString().ToUpper()) { return RedirectToAction("MyProfile", new { activetab = 3 }); } // Haccking (Update others profile)
            if (Count != 0)
            {
                if (hdnvalue == "1" || hdnvalue == string.Empty)
                {
                    string notificationmsg = "Profile Updated";
                    if (hdnvalue == "1")
                    {
                        notificationmsg = "Credit Requested";
                    }
                    mFactory.UpdateUserProfile(Userobj, hdnvalue, notificationmsg, "0");

                    SafeRun<bool>(() =>
                    {
                        var OneSignalService = new Push.OneSignal();
                        var pushdata = new { NOTIFICATIONTYPEID = 5115, NOTIFICATIONCODE = notificationmsg };
                        return OneSignalService.SendPushMessageByTag("Email", Userobj.USERID.ToLower(), notificationmsg, pushdata);
                    });
                }
                if (hdnvalue == "1" || hdnvalue == "2")
                {
                    //credit comments
                    DBFactory.Entities.UserProfileEntity creditcommentEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 2);
                    if (creditcommentEntity.CreditComments.Count == 0)
                    {
                        CreditCommentEntity objCreditComment = new CreditCommentEntity();
                        if (creditcommentEntity != null)
                            objCreditComment.CREDITAPPID = creditcommentEntity.CREDITAPPID;
                        objCreditComment.COMMENTS = model.COMMENTS;
                        objCreditComment.COMMENTSFROM = "SHIPA";
                        objCreditComment.CREATEDBY = HttpContext.User.Identity.Name;
                        mFactory.SaveCreditComments(objCreditComment);
                    }
                    //credit comments
                    //SendMailtoGSSC(HttpContext.User.Identity.Name.ToString()); 
                    //Mail to Country Finance
                    UserEntity mDBEntity = mFactory.GetUserDetails(HttpContext.User.Identity.Name);
                    if (mDBEntity.COUNTRYID != 0)
                    {
                        SendMailtoCountryFinance(mDBEntity.COUNTRYID, "Credit");
                    }
                    if (model.EXISTINGCUSTOMER == true && model.USEMYCREIDT == true)
                    {
                        SendMailToGSSCforExistingCustomer(model);
                    }
                }
                if (Session["UserImgChanged"] != null && Session["UserImgChanged"].ToString() == "true")
                {
                    var UserImage = Session["UserImgContent"] as byte[];
                    string fname = Session["UserImgFileName"].ToString();
                    var ms = new MemoryStream(UserImage);
                    System.Drawing.Bitmap image = (Bitmap)Bitmap.FromStream(ms);
                    Image imgblack = GrayScale(image);
                    Image resizeimg = ResizeImage(imgblack, 100, 100);
                    byte[] resizebytes = (byte[])(new ImageConverter()).ConvertTo(resizeimg, typeof(byte[]));
                    long Result = mFactory.UploadPhoto(resizebytes, fname, model.UserId);
                    byte[] bt = (byte[])resizebytes;
                    string base64 = Convert.ToBase64String(bt);
                    Session["BaseIm"] = (string)base64;
                    Session["filetype"] = fname;
                }


                return RedirectToAction("MyProfile");
            }
            else
            {
                return RedirectToAction("MyProfile", new { activetab = 2 }); // 2 is for Incomplete Profile
            }

        }
        //[Authorize]
        //public ActionResult Requests(string TabStatus = "", string SearchString = "", int PageNo = 1)
        //{
        //ViewBag.BodyClass = "";
        //ViewBag.FooterClass = "footer-bg";
        //UserDBFactory mFactory = new UserDBFactory();

        //switch (TabStatus)
        //{
        //    case "CLOSED":
        //        TabStatus = "Closed";
        //        break;
        //    case "PENDING REVIEW":
        //        TabStatus = "Pending Review";
        //        break;
        //    case "SUBMITTED":
        //        TabStatus = "Submitted";
        //        break;
        //}

        //UserProfileEntity mDBEntity = mFactory.GetSupportRequests(HttpContext.User.Identity.Name, TabStatus, SearchString, PageNo);
        //UserProfileModel Model = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
        ////-----Map REQList To Model----
        //List<RequestsModel> mREQModel = Mapper.Map<List<Requests>, List<RequestsModel>>(mDBEntity.Requests.ToList());
        //ViewBag.PageNo = PageNo;
        //ViewBag.TabStaus = "Requests";
        //ViewBag.SubTabStaus = TabStatus;
        //Model.RequestsModel = mREQModel;
        //return View("ProfilePage", Model);
        //}

        //public ActionResult _PartialChat(string REQID)
        //{
        //    UserDBFactory mFactory = new UserDBFactory();
        //    //string SEQNO = mFactory.GetSEQNObyREQID(REQID);
        //    //deletefolders(SEQNO);
        //    UserProfileEntity mDBEntity = mFactory.GetSupportChat(REQID);
        //    UserProfileModel Model = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
        //    //-----Map REQList To Model----
        //    List<ChatModel> mREQModel = Mapper.Map<List<Chat>, List<ChatModel>>(mDBEntity.Chat.ToList());

        //    ViewBag.TabStaus = "Requests";
        //    Model.ChatModel = mREQModel;
        //    return PartialView("_PartialChat", Model);
        //}

        [EncryptedActionParameter]
        public void DownloadAttchment(int id)
        {
            UserDBFactory mFactory = new UserDBFactory();
            DataSet ds = mFactory.GetbytesDownloadAttchment(id);
            var mBytess = ds.Tables[0].Rows[0]["FILECONTENT"];
            string FILENAME = ds.Tables[0].Rows[0]["FILENAME"].ToString();
            var mBytes = (Byte[])mBytess;
            System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
            Response.AddHeader("Content-disposition", "attachment; filename=" + FILENAME);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(mBytes);
            Response.End();
        }

        [Authorize]
        [EncryptedActionParameter]
        public ActionResult PartiesEncript(string SearchString = "")
        {
            return RedirectToAction("Parties", new { SearchString = SearchString });

        }
        [HttpPost]
        [Authorize]
        public JsonResult AddNewParty(string SSPClientID, string CompanyName, string HouseNO, string BulName, string PAddrLine1, string PAddrLine2, string PostalCode,
            string FirstName, string LastName, string Email, string MobNumber, string CountryID, string CountryName, string StateID,
            string StateName, string CityID, string CityName, string Title, string CountryCode, string StateCode, string CityCode, string NickName, string TaxID, string FullAddressChinese, string ISDCode)
        {
            UserDBFactory DBF = new UserDBFactory();
            Int64 Count = 0;
            if (SSPClientID == "0")
            {

                DBF.AddeNewPartyAddress(SSPClientID, CompanyName, HouseNO, BulName, PAddrLine1, PAddrLine2, PostalCode,
                 FirstName, LastName, Email, MobNumber, CountryName, StateName, CityName, HttpContext.User.Identity.Name.ToString(),
                 CountryID, StateID, CityID, Title, CountryCode, StateCode, CityCode, NickName, TaxID, FullAddressChinese, ISDCode); //To-Do
            }
            else
            {
                Count = DBF.UpdtePartyAddress(SSPClientID, CompanyName, HouseNO, BulName, PAddrLine1, PAddrLine2, PostalCode,
                FirstName, LastName, Email, MobNumber, CountryName, StateName, CityName, HttpContext.User.Identity.Name.ToString(), CountryID, StateID, CityID, Title,
                CountryCode, StateCode, CityCode, NickName, TaxID, string.Empty, ISDCode);//To-Do
            }
            if (Count > 0)
            {
                var JsonToReturn = new
                {
                    rows = "ShowAlert",
                };

                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult GetPartyDetailsByID(string ID)
        {
            List<UserPartyMasterModel> mUIModel = (List<UserPartyMasterModel>)Session["PartyData"];

            if (mUIModel != null)
            {
                List<UserPartyMasterModel> PartyByID = mUIModel.Where(i => i.SSPCLIENTID == Convert.ToInt64(ID)).ToList();
                var JsonToReturn = new
                {
                    rows = PartyByID,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }




        [Authorize]
        //[EncryptedActionParameter]
        public ActionResult Delete(Int64 SSPClientID)
        {
            UserDBFactory DBF = new UserDBFactory();

            DBF.DeleteParty(Convert.ToInt32(SSPClientID), HttpContext.User.Identity.Name.ToString());

            return RedirectToAction("Parties", new { status = "Parties" });
        }

        [HttpPost]
        public ActionResult UploadFiles(string seqno = "")
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    string Dates = DateTime.Today.ToString("yyyyMMdd");
                    string users = HttpContext.User.Identity.Name.ToString();
                    string FilePaths = Path.Combine(Server.MapPath("~/UploadedFiles/"), Dates);
                    bool existss = System.IO.Directory.Exists(FilePaths);
                    if (existss)
                    {
                        FilePaths = FilePaths + "\\" + users;
                        bool userfolderexistss = System.IO.Directory.Exists(FilePaths);
                        if (userfolderexistss)
                        {
                            if (Directory.Exists(FilePaths))
                                GC.Collect();
                            GC.WaitForPendingFinalizers();
                            Directory.Delete(FilePaths, true);
                        }
                    }
                    string returnjson = string.Empty;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string FilePathExtn = Path.GetExtension(file.FileName).ToLower();
                        if (file.ContentLength != 0)
                        {
                            if ((FilePathExtn != ".txt" && FilePathExtn != ".xls" && FilePathExtn != ".xlsx" && FilePathExtn != ".doc"
                                && FilePathExtn != ".docx" && FilePathExtn != ".pdf" && FilePathExtn != ".tif" && FilePathExtn != ".jpg"
                                && FilePathExtn != ".jpeg" && FilePathExtn != ".png" && FilePathExtn != ".gif" && FilePathExtn != ".msg"))
                            {
                                returnjson = "False";
                                break;
                            }
                        }
                    }
                    if (returnjson != "False")
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname;
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            else
                            {
                                fname = file.FileName;
                            }
                            string Date = DateTime.Today.ToString("yyyyMMdd");
                            string user = HttpContext.User.Identity.Name.ToString();
                            string FilePath = Path.Combine(Server.MapPath("~/UploadedFiles/"), Date);
                            bool exists = System.IO.Directory.Exists(FilePath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(FilePath);

                            FilePath = FilePath + "\\" + user;
                            bool userfolderexists = System.IO.Directory.Exists(FilePath);
                            if (!userfolderexists)
                                System.IO.Directory.CreateDirectory(FilePath);

                            if (seqno != null && seqno != "")
                            {
                                FilePath = FilePath + "\\" + seqno;
                                bool usersubfolderexists = System.IO.Directory.Exists(FilePath);
                                if (!usersubfolderexists)
                                    System.IO.Directory.CreateDirectory(FilePath);
                            }
                            fname = Path.Combine(FilePath, fname);
                            file.SaveAs(fname);
                        }
                        return Json("File Uploaded Successfully!");
                    }
                    else { return Json("Files not Uploaded due to invalid Filetypes..!"); }
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        [HttpPost]
        [Authorize]
        public JsonResult SaveTicket(string ReqID, string Module, string Type, string ReferenceID, string Query, string Description, string ModuleCode)
        {
            UserDBFactory dbfac = new UserDBFactory();
            string reqSeqNo = dbfac.SaveTicket(Module, Type, ReferenceID, Query, Description, HttpContext.User.Identity.Name.ToString(), ModuleCode);

            if (Module.ToUpperInvariant() == "DOCUMENTATION")
            {
                string Date = DateTime.Today.ToString("yyyyMMdd");
                string user = HttpContext.User.Identity.Name.ToString();
                string FilePath = Path.Combine(Server.MapPath("~/UploadedFiles/"), Date);
                FilePath = FilePath + "\\" + user;
                bool folderexists = System.IO.Directory.Exists(FilePath);
                if (folderexists)
                {
                    var dir = new System.IO.DirectoryInfo(FilePath);
                    System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
                    List<byte[]> items = new List<byte[]>();

                    foreach (var file in fileNames)
                    {
                        string filebyte = FilePath + "\\" + file;
                        byte[] fileBytes = GetFile(filebyte);
                        items.Add(fileBytes);
                        dbfac.SaveTicketDocuemnts(ReferenceID, HttpContext.User.Identity.Name.ToString(), reqSeqNo, fileBytes, file.Name, "", "Request");
                    }
                }
            }

            string Message = "Customer support Request has been done by the user : " + HttpContext.User.Identity.Name.ToString();

            SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + reqSeqNo + ")", Message, reqSeqNo, "", "CUSTOMERSUPPORT", HttpContext.User.Identity.Name.ToString()); //string User,string Email,string Subject,string Description,string RefNumber

            var JsonToReturn = new
            {
                rows = "success",
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public void SendMailForGSSCQuotes(string reqSeqNo)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            Email = Email + ";" + HttpContext.User.Identity.Name.ToString() + ";" + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string subject = "";
            subject = "Shipa Freight- Customer support Request" + EnvironmentName;
            string body = string.Empty;
            //string body = "Dear User,";
            //body = body + "<br/> <br/>" + "Customer support Request done by the user : " + HttpContext.User.Identity.Name.ToString();
            //body = body + "<br/> <br/>" + "and  Customer support Request Number : " + reqSeqNo; 
            //body = body + "<br/> <br/>" + "Regards";
            //body = body + "<br/>" + "ShipA Freight Team";

            string mapPath = Server.MapPath("~/App_Data/CustomerSupport.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{user_name}", (Request.Cookies["myCookie"].Values["UserName"].ToString()));
            body = body.Replace("{Message}", "Customer support Request has been done by the user : " + HttpContext.User.Identity.Name.ToString() + " and Customer support Request Number :");
            body = body.Replace("{Reference_No}", reqSeqNo);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMail(Email, body, subject, "support", reqSeqNo, false);
        }

        public void SendMail(string Email, string body, string subject, string submodule, string referenceno, bool IsRequired)
        {
            MailMessage message = new MailMessage();
            string to = string.Empty;
            if (Email.Contains(';'))
            {
                for (int i = 0; i <= (Email.Split(';').Length - 1); i++)
                {
                    message.To.Add(new MailAddress(Email.Split(';')[i]));
                    to = Email.Split(';')[i] + ",";
                }
            }
            else
            {
                message.To.Add(new MailAddress(Email));
                to = Email;
            }

            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", to, "", "",
      "UserManagement", submodule, referenceno, HttpContext.User.Identity.Name.ToString(), body, "", IsRequired, message.Subject.ToString());
            var nJSON = new JsonResult();
            nJSON.Data = true;
        }

        [HttpPost]
        [Authorize]
        public JsonResult InsertNewChat(string ChatMes, string REQID, string SEQNO, string MODULE)
        {
            UserDBFactory DB = new UserDBFactory();
            if (MODULE.ToUpperInvariant() == "DOCUMENTATION")
            {
                string Date = DateTime.Today.ToString("yyyyMMdd");
                string user = HttpContext.User.Identity.Name.ToString();
                string FilePath = Path.Combine(Server.MapPath("~/UploadedFiles/"), Date);
                FilePath = FilePath + "\\" + user;
                bool existss = System.IO.Directory.Exists(FilePath);
                if (existss)
                {
                    FilePath = FilePath + "\\" + SEQNO;
                    bool subexistss = System.IO.Directory.Exists(FilePath);
                    if (subexistss)
                    {
                        var dir = new System.IO.DirectoryInfo(FilePath);
                        System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
                        List<byte[]> items = new List<byte[]>();
                        foreach (var file in fileNames)
                        {
                            string filebyte = FilePath + "\\" + file;
                            byte[] fileBytes = GetFile(filebyte);
                            items.Add(fileBytes);

                            DB.SaveTicketDocuemnts("", HttpContext.User.Identity.Name.ToString(), SEQNO, fileBytes, file.Name, ChatMes, "Communication");
                            ChatMes = "";
                        }
                    }
                    else
                    {
                        DB.SaveOrCloseChat(ChatMes, REQID, HttpContext.User.Identity.Name.ToString(), 0);
                    }
                }
                else
                {
                    DB.SaveOrCloseChat(ChatMes, REQID, HttpContext.User.Identity.Name.ToString(), 0);
                }
            }
            else
            {
                DB.SaveOrCloseChat(ChatMes, REQID, HttpContext.User.Identity.Name.ToString(), 0);
            }

            SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + SEQNO + ")", ChatMes, SEQNO, "", "CUSTOMERSUPPORT", HttpContext.User.Identity.Name.ToString()); //string User,string Email,string Subject,string Description,string RefNumber

            var JsonToReturn = new
            {
                rows = "success",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult deletefolders(string SEQNO)
        {
            string Dates = DateTime.Today.ToString("yyyyMMdd");
            string users = HttpContext.User.Identity.Name.ToString();
            string FilePaths = Path.Combine(Server.MapPath("~/UploadedFiles/"), Dates);
            bool existss = System.IO.Directory.Exists(FilePaths);
            if (existss)
            {
                FilePaths = FilePaths + "\\" + users;
                bool userfolderexistss = System.IO.Directory.Exists(FilePaths);
                if (userfolderexistss)
                {
                    if (Directory.Exists(FilePaths))
                        GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Directory.Delete(FilePaths, true);
                }
            }
            var JsonToReturn = new
            {
                rows = "success",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult CloseChat(string REQID)
        {
            UserDBFactory DB = new UserDBFactory();
            DB.SaveOrCloseChat("", REQID, HttpContext.User.Identity.Name.ToString(), 1);
            return RedirectToAction("Requests", new { TabStatus = "", SearchString = "", PageNo = 1 });
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyProfile(UserProfileModel model)
        {
            UserDBFactory mFactory = new UserDBFactory();
            string hdnvalue = Convert.ToString(Request.Params["hdncreditedit"]);
            string ChangeState = Convert.ToString(Request.Params["Changestate"]);
            string hdnCreditRequested = Convert.ToString(Request.Params["hdncreditrequested"]);
            string firstName = Convert.ToString(Request.Params["userfirstname"]);
            if (hdnvalue != "1" && hdnvalue != "2")
            {
                // model.PREFERREDCURRENCY = model.CURRENCYID;
                // model.PREFERREDCURRENCYCODE = model.CURRENCYID;
                model.UACountryId = model.UACountryDummyId;
                if (model.MobileNumber != null)
                {
                    model.MobileNumber = Regex.Replace(model.MobileNumber, @"[-+() ]", "");
                }
                string UAAddressline1 = (model.UAAddressline1 != null) ? model.UAAddressline1.Trim() : null;
                string UAAddressline2 = (model.UAAddressline2 != null) ? model.UAAddressline2.Trim() + "," : null;
                string UACountryName = (model.UACountryName != null) ? model.UACountryName.Trim() : null;
                string UAStateName = (model.UAStateName != null) ? model.UAStateName.Trim() : null;
                string UACityName = (model.UACityName != null) ? model.UACityName.Trim() : null;

                model.UAFullAddress = UAAddressline1 + "," + UAAddressline2 + UACountryName + "," + UAStateName + "," + UACityName + "," + model.UAPostCode;
                model.ISDCode = Request.Params["hdnISDCode"];
                model.CountryId = model.UACountryId;
                model.CountryCode = model.UACountryCode;
                model.CountryName = model.UACountryName;

                if (!model.EXISTINGCUSTOMER)
                {
                    model.USEMYCREIDT = false;
                }
                var myCookie = new HttpCookie("myCookie");
                if (model.FirstName != null)
                {
                    myCookie.Values.Add("UserName", model.FirstName);
                }
                else
                {
                    myCookie.Values.Add("UserName", firstName);
                }
                myCookie.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(myCookie);
            }
            if (string.IsNullOrEmpty(model.UserId) && !string.IsNullOrEmpty(HttpContext.User.Identity.Name.ToString()))
            {
                model.UserId = HttpContext.User.Identity.Name.ToString();
            }
            if (model.ANNUALTURNOVERCURRENCY != null)
            {
                string modifiedAnnualCurrencyValue = model.ANNUALTURNOVERCURRENCY.Replace(",", "");
                decimal CurrencyValue = Convert.ToDecimal(string.Join("", modifiedAnnualCurrencyValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                model.ANNUALTURNOVER = CurrencyValue;
            }
            if (model.REQUESTEDCREDITLIMITCURRENCY != null)
            {
                string modifiedRequestCurrencyValue = model.REQUESTEDCREDITLIMITCURRENCY.Replace(",", "");
                decimal CurrencyValue = Convert.ToDecimal(string.Join("", modifiedRequestCurrencyValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                model.REQUESTEDCREDITLIMIT = CurrencyValue;
            }
            UserProfileEntity Userobj = Mapper.Map<UserProfileModel, UserProfileEntity>(model);
            DataSet ds = new DataSet();
            int Count = 1;
            if (hdnvalue == "1")
            {
                ds = mFactory.IsProfileUpdated(HttpContext.User.Identity.Name.ToString());
                Count = Convert.ToInt32(ds.Tables[0].Rows[0]["NOTIFICATIONCOUNT"].ToString());
            }
            if (Userobj.USERID.ToUpper() != HttpContext.User.Identity.Name.ToString().ToUpper()) { return RedirectToAction("MyProfile", new { activetab = 3 }); } // Haccking (Update others profile)
            if (Count != 0)
            {
                if (hdnvalue == "1" || hdnvalue == string.Empty)
                {
                    string notificationmsg = "Profile Updated";
                    if (hdnvalue == "1")
                    {
                        notificationmsg = "Credit Requested";
                    }
                    mFactory.UpdateUserProfile(Userobj, hdnvalue, notificationmsg, ChangeState);
                    SafeRun<bool>(() =>
                    {
                        var OneSignalService = new Push.OneSignal();
                        var pushdata = new { NOTIFICATIONTYPEID = 5115, NOTIFICATIONCODE = notificationmsg };
                        return OneSignalService.SendPushMessageByTag("Email", Userobj.USERID.ToLower(), notificationmsg, pushdata);
                    });
                }
                if (hdnvalue == "1" || hdnvalue == "2")
                {
                    //credit comments
                    DBFactory.Entities.UserProfileEntity creditcommentEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 2);
                    if (creditcommentEntity.CreditComments.Count == 0)
                    {
                        CreditCommentEntity objCreditComment = new CreditCommentEntity();
                        if (creditcommentEntity != null)
                            objCreditComment.CREDITAPPID = creditcommentEntity.CREDITAPPID;
                        objCreditComment.COMMENTS = model.COMMENTS;
                        objCreditComment.COMMENTSFROM = "SHIPA";
                        objCreditComment.CREATEDBY = HttpContext.User.Identity.Name;
                        mFactory.SaveCreditComments(objCreditComment);
                    }
                    //credit comments
                    //SendMailtoGSSC(HttpContext.User.Identity.Name.ToString()); 
                    //Mail to Country Finance
                    UserEntity mDBEntity = mFactory.GetUserDetails(HttpContext.User.Identity.Name);
                    if (mDBEntity.COUNTRYID != 0)
                    {
                        SendMailtoCountryFinance(mDBEntity.COUNTRYID, "Credit");
                    }
                    if (model.EXISTINGCUSTOMER == true && model.USEMYCREIDT == true)
                    {
                        SendMailToGSSCforExistingCustomer(model);
                    }
                }
                if (Session["UserImgChanged"] != null && Session["UserImgChanged"].ToString() == "true")
                {
                    var UserImage = Session["UserImgContent"] as byte[];
                    string fname = Session["UserImgFileName"].ToString();
                    var ms = new MemoryStream(UserImage);
                    System.Drawing.Bitmap image = (Bitmap)Bitmap.FromStream(ms);
                    Image imgblack = GrayScale(image);
                    Image resizeimg = ResizeImage(imgblack, 100, 100);
                    byte[] resizebytes = (byte[])(new ImageConverter()).ConvertTo(resizeimg, typeof(byte[]));
                    long Result = mFactory.UploadPhoto(resizebytes, fname, model.UserId);
                    byte[] bt = (byte[])resizebytes;
                    string base64 = Convert.ToBase64String(bt);
                    Session["BaseIm"] = (string)base64;
                    Session["filetype"] = fname;
                }


                return RedirectToAction("MyProfile");
            }
            else
            {
                return RedirectToAction("MyProfile", new { activetab = 2 }); // 2 is for Incomplete Profile
            }


        }
        public ActionResult _PartialCreditChat()
        {
            UserDBFactory mFactory = new UserDBFactory();
            DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 2);

            UserProfileModel mUIModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
            return PartialView("_PartialCreditChat", mUIModel);
        }
        private void SendMailtoUserforNotInNPCListedCountry(string CName, string FIRSTNAME)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(HttpContext.User.Identity.Name.ToString());
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Credit Request Staus" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/Credit-SubmitToEforms.html");


            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{CountryName}", CName);
            body = body.Replace("{user_name}", FIRSTNAME);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", "", "", "",
             "UserManagement", "Credit Request Status", message.To.ToString(), HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
        }
        [Authorize]
        [HttpPost]
        public ActionResult _AddCreditChat(long addcreditappid, long creditappId)
        {
            AdditionalCredit mUIModel = new AdditionalCredit();
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                string userid = HttpContext.User.Identity.Name;
                DBFactory.Entities.AdditionalCreditEntity mDBEntity = mFactory.GetAddCreditLimit(userid, Convert.ToInt32(creditappId));
                mDBEntity.CreditComments = mDBEntity.CreditComments.Where(x => x.ADDITIONALCREDITID == addcreditappid).ToList();
                mUIModel = Mapper.Map<AdditionalCreditEntity, AdditionalCredit>(mDBEntity);
            }
            catch (Exception ex)
            {
                mUIModel = null;
            }
            return PartialView("_AddCreditChat", mUIModel);
        }
        /// <summary>
        /// Get Additional Credit Details
        /// </summary>
        /// <param name="creditId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult GetAdditionalCreditLimit(int creditId)
        {
            AdditionalCredit mUIModel = new AdditionalCredit();
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                string userid = HttpContext.User.Identity.Name;
                DBFactory.Entities.AdditionalCreditEntity mDBEntity = mFactory.GetAddCreditLimit(userid, creditId);
                foreach (var item in mDBEntity.CreditDoc)
                {
                    string downloadlink = MyExtensions.EncodedDownloadLink(item.FILENAME, "DownloadAddCreditDoc", "UserManagement", new { filename = item.FILENAME }, null, "_blank", 0);
                    item.DocDownloadLink = downloadlink;
                }
                mDBEntity.CreditComments = mDBEntity.CreditComments.Where(x => x.ADDITIONALCREDITID != null && x.CREDITAPPID == creditId).ToList();
                mUIModel = Mapper.Map<AdditionalCreditEntity, AdditionalCredit>(mDBEntity);
            }
            catch (Exception ex)
            {
                mUIModel = null;
            }
            return Json(mUIModel, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Save Additional Credit Details
        /// </summary>
        /// <param name="addCredit"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult AdditionalCreditLimit(AdditionalCredit addCredit)
        {
            string saveStatus = "Success";
            int notificationCount = Convert.ToInt32(Session["NotificationCount"]);
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 2);
                UserProfileModel mUIModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
                mUIModel.CreditDoc = null;
                mUIModel.CreditComments = null;
                mUIModel.INDUSTRYID = addCredit.INDUSTRYID;
                mUIModel.NOOFEMP = Convert.ToInt32(addCredit.NOOFEMP);
                mUIModel.PARENTCOMPANY = addCredit.PARENTCOMPANY;
                mUIModel.ANNUALTURNOVER = addCredit.ANNUALTURNOVER;
                mUIModel.DBNUMBER = addCredit.DBNUMBER;
                mUIModel.HFMENTRYCODE = addCredit.HFMENTRYCODE;
                mUIModel.CURRENCYID = addCredit.CURRENCYID;
                mUIModel.COMMENTS = addCredit.COMMENTS;
                mUIModel.REQUESTEDCREDITLIMIT = addCredit.REQUESTEDCREDITLIMIT;
                mUIModel.CURRENTPAYTERMSNAME = addCredit.CURRENTPAYTERMSNAME;
                //mUIModel.EXISTINGCUSTOMER = addCredit.EXISTINGCUSTOMER;
                //mUIModel.USEMYCREIDT = addCredit.USEMYCREIDT;
                UserProfileEntity Userobj = Mapper.Map<UserProfileModel, UserProfileEntity>(mUIModel);
                long? addcreditID = mFactory.UpdateAdditionalCredit(Userobj);

                DataSet ds = new DataSet();
                int Count = 1;
                ds = mFactory.IsProfileUpdated(HttpContext.User.Identity.Name.ToString());
                Count = Convert.ToInt32(ds.Tables[0].Rows[0]["NOTIFICATIONCOUNT"].ToString());
                if (Count != 0)
                {
                    SafeRun<bool>(() =>
                    {
                        var OneSignalService = new Push.OneSignal();
                        var pushdata = new { NOTIFICATIONTYPEID = 5115, NOTIFICATIONCODE = "Credit Requested" };
                        return OneSignalService.SendPushMessageByTag("Email", Userobj.USERID.ToLower(), "Profile Updated", pushdata);
                    });
                }
                //credit comments  
                List<CreditCommentEntity> creditcommentList = mDBEntity.CreditComments.Where(x => x.ADDITIONALCREDITID == addcreditID).ToList();
                if (creditcommentList.Count == 0)
                {
                    if (addcreditID != null)
                    {
                        CreditCommentEntity objCreditComment = new CreditCommentEntity();
                        objCreditComment.ADDITIONALCREDITID = addcreditID;
                        objCreditComment.CREDITAPPID = mUIModel.CREDITAPPID;
                        objCreditComment.COMMENTS = mUIModel.COMMENTS;
                        objCreditComment.COMMENTSFROM = "SHIPA";
                        objCreditComment.CREATEDBY = HttpContext.User.Identity.Name;
                        mFactory.SaveCreditComments(objCreditComment);
                    }
                }
                //Mail to Country Finance
                if (mUIModel.CountryId != 0)
                {
                    SendMailtoCountryFinance(mUIModel.CountryId, "Additional");
                }
                if (mUIModel.EXISTINGCUSTOMER == true && mUIModel.USEMYCREIDT == true)
                {
                    SendMailToGSSCforExistingCustomer(mUIModel);
                }
                DataTable dt = mFactory.ShowNotificationCount(HttpContext.User.Identity.Name.ToString());
                notificationCount = Convert.ToInt32(dt.Rows[0]["NOTIFICATIONCOUNT"]);
            }
            catch (Exception ex)
            {
                saveStatus = "Fail";
                throw;
            }
            // return Json(saveStatus, JsonRequestBehavior.AllowGet);  
            return Json(new { saveStatus, notificationCount }, "text/x-json", JsonRequestBehavior.AllowGet);
        }
        private void SendMailtoCountryFinance(long CountryId, string request)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string body = string.Empty;
            string team = string.Empty;
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            string recipientsList = "";
            try
            {
                recipientsList = mFactory.GetEmailList("SSPCOUNTRYFINANCE", "", "CREDITAPPLICATION", CountryId.ToString());
                if (recipientsList == "")
                {
                    recipientsList = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
                }
                string[] bccid = recipientsList.Split(',');
                string User = HttpContext.User.Identity.Name.ToString();
                foreach (string bccEmailId in bccid)
                {
                    message.To.Add(new MailAddress(bccEmailId));
                }
                message.IsBodyHtml = true;
                string mapPath = Server.MapPath("~/App_Data/CountryFinance-AddCreditRequest.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                string EnvironmentName = DynamicClass.GetEnvironmentName();
                string ServerEnvironmentName = DynamicClass.GetEnvironmentNameDocs();

                if (request == "Credit")
                {
                    message.Subject = "Shipa Freight - Request for Business Credit" + EnvironmentName;
                }
                else
                {
                    message.Subject = "Shipa Freight - Request for Additional Business Credit" + EnvironmentName;
                }
                if (ServerEnvironmentName == "PRD")
                {
                    body = body.Replace("{focisurl}", "https://focis.agility.com");
                }
                else if (ServerEnvironmentName == "AGL")
                {
                    body = body.Replace("{focisurl}", "https://focisagile.agility.com/login.aspx");
                }
                else if (ServerEnvironmentName == "SIT")
                {
                    body = body.Replace("{focisurl}", "https://focissit.agility.com/login.aspx");
                }
                else if (ServerEnvironmentName == "DEM")
                {
                    body = body.Replace("{focisurl}", "https://focisdemo.agility.com/login.aspx");
                }
                else
                {
                    body = body.Replace("{focisurl}", "https://focisagile.agility.com/login.aspx");
                }

                if (request == "Credit")
                {
                    body = body.Replace("{Additional}", "");
                }
                else
                {
                    body = body.Replace("{Additional}", "Additional");
                }
                body = body.Replace("{User}", User);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                if (request == "Credit")
                {
                    DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                     "UserManagement", "Request for Business Credit", "", HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
                }
                else
                {
                    DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                 "UserManagement", "Request for Additional Business Credit", "", HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }
        /// <summary>
        /// Additional Credit Documents Upload
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddCreditUploadDocuments(long? commentid)
        {
            //Credit supporting docs
            byte[] fileData = null;
            long docid = 0;
            string downloadLink = "";
            if (Request.Files != null && Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    using (var binaryReader = new BinaryReader(Request.Files[i].InputStream))
                    {
                        if (Request.Files[i].ContentLength != 0)
                        {
                            fileData = binaryReader.ReadBytes(Request.Files[i].ContentLength);

                            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                            sspdoc.CREATEDBY = HttpContext.User.Identity.Name.ToString();
                            sspdoc.FILENAME = Request.Files[i].FileName;
                            sspdoc.FILEEXTENSION = Request.Files[i].ContentType;
                            sspdoc.FILESIZE = Request.Files[i].ContentLength;
                            sspdoc.FILECONTENT = fileData;
                            sspdoc.DocumentName = Request.Files[i].FileName;
                            sspdoc.DOCUMNETTYPE = Request.Files[i].FileName;
                            sspdoc.COMMENTID = commentid;
                            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                            UserDBFactory creditFactory = new UserDBFactory();
                            docid = creditFactory.AddCreditSaveDocumentsWithParams(docentity, HttpContext.User.Identity.Name, "AdditionalCreditLimit", "NO");
                            downloadLink = MyExtensions.EncodedDownloadLink(Request.Files[i].FileName, "DownloadAddCreditDoc", "UserManagement", new { filename = Request.Files[i].FileName }, null, "_blank", 0);
                        }
                    }
                }

            }

            var JsonToReturn = new
            {
                uploaddocId = docid,
                docdownloadlink = downloadLink
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UploadDocuments(long? commentid)
        {
            //Credit supporting docs
            byte[] fileData = null;
            long docid = 0;
            string downloadLink = "";
            if (Request.Files != null && Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    using (var binaryReader = new BinaryReader(Request.Files[i].InputStream))
                    {
                        if (Request.Files[i].ContentLength != 0)
                        {
                            fileData = binaryReader.ReadBytes(Request.Files[i].ContentLength);

                            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                            sspdoc.CREATEDBY = HttpContext.User.Identity.Name.ToString();
                            sspdoc.FILENAME = Request.Files[i].FileName;
                            sspdoc.FILEEXTENSION = Request.Files[i].ContentType;
                            sspdoc.FILESIZE = Request.Files[i].ContentLength;
                            sspdoc.FILECONTENT = fileData;
                            sspdoc.DocumentName = Request.Files[i].FileName;
                            sspdoc.DOCUMNETTYPE = Request.Files[i].FileName;
                            sspdoc.COMMENTID = commentid;
                            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                            UserDBFactory creditFactory = new UserDBFactory();
                            docid = creditFactory.SaveDocumentsWithParams(docentity, HttpContext.User.Identity.Name, "", "NO");
                            downloadLink = MyExtensions.EncodedDownloadLink(Request.Files[i].FileName, "DownloadCreditDoc", "UserManagement", new { filename = Request.Files[i].FileName }, null, "_blank", 0);
                        }
                    }
                }

            }

            var JsonToReturn = new
            {
                uploaddocId = docid,
                docdownloadlink = downloadLink
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public void SendMailtoGSSC(string User)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];
            string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            message.To.Add(new MailAddress(help));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Request for Business Credit" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/GSSC-CreditRequest.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{User}", User);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
           "UserManagement", "Request for Business Credit", "", HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
        }
        public void SendMailToGSSCforExistingCustomer(UserProfileModel model)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];
            string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            message.To.Add(new MailAddress(help));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Request for known customer" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;

            string Bodytext = "<b>Dear GSSC team</b>, <br/> <br/>           A user (" + model.UserId + ") has updated the profile mentioning as existing customer and has authorized to use the present credit limit setup for his account. Please check with country finance (" + "<b>" + model.CountryName + "</b>" + ") and update their credit limit from the user section.";
            body = Bodytext;

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
        "UserManagement", "Existing customer", model.UserId, HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
        }

        public void SendMailFocisCustSupport(string User, string Roles, string Subject, string Description, string RefNumber, string JobNumber = "", string NotifyType = "", string CreatedBy = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            if (Roles.Contains("@"))
            {
                message.To.Add(Roles);
            }
            else
            {
                recipientsList = mFactory.GetEmailList(Roles, JobNumber, NotifyType, "", CreatedBy);
                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
            }
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = Subject + EnvironmentName;

            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/CustomerSupport-Focis.html");

            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{User}", User);
            body = body.Replace("{Message}", Description);
            body = body.Replace("{Reference_No}", RefNumber);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            if (Roles.Contains("@"))
            {

                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Roles, "", "",
            "UserManagement", "Customer support", RefNumber, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
            }
        }

        public async Task<JsonResult> FillCountry()
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CountryEntity> country = await Task.Run(() => mdmFactory.GetCountries(string.Empty, string.Empty)).ConfigureAwait(false);
                if (country != null)
                {
                    var Countrylist = (
                             from items in country
                             select new
                             {
                                 Text = items.NAME,
                                 Value = items.COUNTRYID,
                                 COUNTRYCODE = items.CODE
                             }).Distinct().ToList().OrderBy(x => x.Text);
                    return Json(Countrylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        [Authorize]
        public async Task<JsonResult> FillStateByCountryId(string CountryId)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<StateEntity> States = await Task.Run(() => mdmFactory.GetStatesByCountry(Convert.ToInt32(CountryId), string.Empty, string.Empty)).ConfigureAwait(false);
                if (States != null)
                {
                    var StateList = (
                             from items in States
                             select new
                             {
                                 Text = items.DESCRIPTION,
                                 Value = items.SUBDIVISIONID,
                                 STATECODE = items.CODE
                             }).Distinct().ToList().OrderBy(x => x.Text);
                    return Json(StateList, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }
        [Authorize]
        public JsonResult GetCountryIDBasedonSelectedFlag(string CCode, string Name)
        {
            try
            {
                UserDBFactory mdmFactory = new UserDBFactory();

                int countryid = mdmFactory.GetCountryIDBasedonCode(CCode);
                var JsonToReturn = new
                {
                    rows = countryid,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            { throw new ShipaExceptions(ex.Message); }
        }

        public ActionResult AlternateEmails(string AlternateEmails, string Type, string partyname, long partyid)
        {
            MDMDataFactory mdmFactory = new MDMDataFactory();
            string message = mdmFactory.AddTeammembers(AlternateEmails, Type, HttpContext.User.Identity.Name, partyname, partyid);
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public async Task<JsonResult> FillCityByStateSubDivisionId(string SubDivisionId)
        {
            try
            {
                if (SubDivisionId != "undefined")
                {
                    MDMDataFactory mdmFactory = new MDMDataFactory();
                    IList<CityEntity> cities = await Task.Run(() => mdmFactory.GetCitiesByState(Convert.ToInt32(SubDivisionId), string.Empty, string.Empty)).ConfigureAwait(false);
                    if (cities != null)
                    {
                        var CityList = (
                                 from items in cities
                                 select new
                                 {
                                     Text = items.NAME,
                                     Value = items.UNLOCATIONID,
                                     CityCode = items.CODE
                                 }).Distinct().ToList().OrderBy(x => x.Text);
                        return Json(CityList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }
        [Authorize]
        public JsonResult UpdateStateByCityId(string SubdivisionCode)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<StateEntity> States = mdmFactory.GetStateByCityId(Convert.ToString(SubdivisionCode));
                if (States != null)
                {
                    var StateList = (
                             from items in States
                             select new
                             {
                                 Text = items.DESCRIPTION,
                                 Value = items.SUBDIVISIONID,
                                 STATECODE = items.CODE
                             }).Distinct().ToList().OrderBy(x => x.Text);
                    return Json(StateList, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            { throw new ShipaExceptions(ex.Message); }
        }

        [Authorize]
        public JsonResult FillCityByCountryID(string CountryID)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CityEntity> cities = mdmFactory.GetCitiesByCountry(Convert.ToInt32(CountryID));
                if (cities != null)
                {
                    var CityList = (
                             from items in cities
                             select new
                             {
                                 Text = items.NAME,
                                 Value = items.UNLOCATIONID,
                                 CityCode = items.CODE
                             }).Distinct().ToList().OrderBy(x => x.Text);
                    return Json(CityList, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            { throw new ShipaExceptions(ex.Message); }
        }
        public ActionResult UploadPhoto(HttpPostedFileBase FilePhotoupload, string UserId)
        {
            HttpPostedFileBase theFile = HttpContext.Request.Files["FilePhotoupload"];
            if (theFile.ContentLength != 0)
            {
                string path = theFile.InputStream.ToString();
                byte[] imageSize = new byte[theFile.ContentLength];
                theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                string fname = Path.GetFileName(theFile.FileName);
                string extension = Path.GetExtension(theFile.FileName).ToLowerInvariant();
                if (extension == ".png" || extension == ".tif" || extension == ".gif" || extension == ".jpg" || extension == ".jpeg")
                {
                    if (theFile.ContentLength <= 500000)
                    {
                        Session["UserImgContent"] = imageSize;
                        Session["UserImgFileName"] = fname;
                        Session["UserImgChanged"] = "true";
                        return Json(new { success = true, result = "File uploaded Successfully !!!" }, "text/html", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, result = "Image size should be less than 500kb." }, "text/html", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, result = "Please select png, tif, gif, jpg, jpeg images only." }, "text/html", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        public void SetCaptchaKey()
        {
            ViewBag.IsLocal = System.Configuration.ConfigurationManager.AppSettings["IsLocal"];
            string host = Request.Url.Host;
            int myInt;
            bool isNumerical = int.TryParse(host.Substring(0, 2), out myInt);

            if (isNumerical) //True Then IP : Host
                ViewBag.CaptchaKey = System.Configuration.ConfigurationManager.AppSettings["CaptchaKey"].ToString();
            else
                ViewBag.CaptchaKey = System.Configuration.ConfigurationManager.AppSettings["CaptchaKeyForHost"].ToString();
        }
        public ActionResult QuoteforSignUp()
        {
            try
            {
                UserModel mUIModel = new UserModel();
                TempData["ActiveTab"] = "GuestUser";
                //Meta title and Meta description
                ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
                ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
                return View("Login", mUIModel);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult GuestUserQuote(string FirstName, string LastName, string CompanyName, string Email, string Nsubscription, string GCountryCode, string Useofquote, string Shippingprocess)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            QuotationModel model = new QuotationModel();
            DBFactory.Entities.QuotationEntity mUIModel = Mapper.Map<QuotationModel, DBFactory.Entities.QuotationEntity>(model);
            if (mUIModel != null)
            {
                mUIModel.GUESTNAME = FirstName;
                mUIModel.GUESTLASTNAME = LastName;
                mUIModel.GUESTCOMPANY = CompanyName;
                mUIModel.GUESTEMAIL = Email;
                mUIModel.NOTIFICATIONSUBSCRIPTION = Convert.ToInt32(Nsubscription);
                mUIModel.GUESTCOUNTRYCODE = Convert.ToString(GCountryCode);
                mUIModel.USEOFQUOTE = Useofquote;
                mUIModel.SHIPMENTPROCESS = Shippingprocess;
                TempData["GQuote"] = mUIModel;
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult ChecGuestkUserExists(string Email)
        {
            try
            {
                //UserDBFactory UDF = new UserDBFactory();
                ProfileInfo PInfo = _userdbfactory.GetProfileInfo(Email);
                bool userExistStatus = false;
                long NoofQuotes = 0;
                if (PInfo != null && PInfo.USERID != null)
                {
                    userExistStatus = true;
                    return Json(new { userExistStatus, NoofQuotes }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    QuotationDBFactory mFactory = new QuotationDBFactory();
                    NoofQuotes = mFactory.NoofQuotesForGuest(Email);
                    return Json(new { userExistStatus, NoofQuotes }, JsonRequestBehavior.AllowGet);
                }
                //return Json(userExistStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }
        public ActionResult Notifications(string userid)
        {
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            UserDBFactory mFactory = new UserDBFactory();
            List<DBFactory.Entities.NotificationEntity> mDBEntity = mFactory.GetNotificationDetails(HttpContext.User.Identity.Name.ToString());
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            if (!String.IsNullOrEmpty(uiCulture) && uiCulture != "en")
            {
                foreach (var item in mDBEntity)
                {
                    if (item.NOTIFICATIONCODE != null)
                    {
                        string notificatiocode = item.NOTIFICATIONCODE.Replace(" ", "");
                        string translatednotificationcode = string.Empty;
                        if (item.NOTIFICATIONCODE.Contains("Party Approved for"))
                        {
                            translatednotificationcode = LocalizedConverter.GetNotificationConversionDataByLanguage("PartyApprovedfor");
                            translatednotificationcode = translatednotificationcode + "(" + item.PARTYNAME + ")";
                        }
                        else if (item.NOTIFICATIONCODE.Contains("Party Rejected for"))
                        {
                            translatednotificationcode = LocalizedConverter.GetNotificationConversionDataByLanguage("PartyRejectedfor");
                            translatednotificationcode = translatednotificationcode + "(" + item.PARTYNAME + ")";
                        }
                        else
                        {
                            translatednotificationcode = LocalizedConverter.GetNotificationConversionDataByLanguage(notificatiocode);
                        }
                        if (translatednotificationcode != null)
                        {
                            item.NOTIFICATIONCODE = translatednotificationcode;
                        }
                    }
                }
            }
            List<NotificationModel> mUIModel = Mapper.Map<List<DBFactory.Entities.NotificationEntity>, List<NotificationModel>>(mDBEntity);
            return View(mUIModel);
        }

        public JsonResult NotificationsScript()
        {
            UserDBFactory mFactory = new UserDBFactory();
            DataSet ds = new DataSet();
            ds = mFactory.GetNotificationDetailsScript(HttpContext.User.Identity.Name.ToString());
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            if (!String.IsNullOrEmpty(uiCulture) && uiCulture != "en")
            {
                ds.Tables[0].Columns["NOTIFICATIONCODE"].ReadOnly = false;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["NOTIFICATIONCODE"].ToString() != null)
                    {
                        string notificatiocode = dr["NOTIFICATIONCODE"].ToString().Replace(" ", "");
                        string translatednotificationcode = string.Empty;
                        if (notificatiocode.Contains("Party Approved for"))
                        {
                            translatednotificationcode = LocalizedConverter.GetNotificationConversionDataByLanguage("PartyApprovedfor");
                            translatednotificationcode = translatednotificationcode + "(" + dr["PARTYNAME"].ToString() + ")";
                        }
                        else if (notificatiocode.Contains("Party Rejected for"))
                        {
                            translatednotificationcode = LocalizedConverter.GetNotificationConversionDataByLanguage("PartyRejectedfor");
                            translatednotificationcode = translatednotificationcode + "(" + dr["PARTYNAME"].ToString() + ")";
                        }
                        else
                        {
                            translatednotificationcode = LocalizedConverter.GetNotificationConversionDataByLanguage(notificatiocode);
                        }
                        if (translatednotificationcode != null)
                        {
                            dr["NOTIFICATIONCODE"] = translatednotificationcode;
                        }
                    }
                    ds.Tables[0].AcceptChanges();

                }
            }
            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Ajax Request will fetech User profile picture for showing logged-in header
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBasestring()
        {
            if (Session["BaseIm"] == null || Session["BaseIm"] == "")
            {
                UserDBFactory UDF = new UserDBFactory();
                DataTable dt = UDF.GetUserImage(HttpContext.User.Identity.Name.ToString());

                DataTable Dt1 = new DataTable();
                Dt1.Columns.Add("BASESTRING", typeof(string));
                Dt1.Columns.Add("IMGFILENAME", typeof(string));
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][1].ToString() != "")
                    {
                        byte[] bt = (byte[])dt.Rows[0][1];
                        string base64 = Convert.ToBase64String(bt);

                        Dt1.Rows.Add(base64, dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1]);
                        Session["BaseIm"] = (string)base64; Session["filetype"] = dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1];
                    }
                }
                else
                {
                    Session["BaseIm"] = null; Session["filetype"] = null;
                }
            }

            var JsonToReturn = new
            {
                rows = "",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteNotification(int id)
        {
            try
            {
                //UserDBFactory mFactory = new UserDBFactory();
                //string Result = mFactory.DeleteNotification(id);
                string Result = _userdbfactory.DeleteNotification(id);
                return RedirectToAction("Notifications", "UserManagement");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        [Authorize]
        public JsonResult DeleteNotificationsScript(int id)
        {
            try
            {
                //UserDBFactory mFactory = new UserDBFactory();
                string Result = _userdbfactory.DeleteNotification(id);
                //var jsonData = new
                //   {

                //       Message = "Success"
                //   };

                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }

        }

        public JsonResult IsProfileUpdated()
        {
            try
            {

                // UserDBFactory mFactory = new UserDBFactory();
                DataSet ds = new DataSet();
                ds = _userdbfactory.IsProfileUpdated(HttpContext.User.Identity.Name.ToString());
                if (ds != null)
                {
                    List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
                    var JsonToReturn = new
                    {
                        rows = trows,
                    };
                    return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }
        [Authorize]
        public JsonResult NotificationsMark(int id)
        {
            try
            {
                //UserDBFactory mFactory = new UserDBFactory();
                // string Result = mFactory.NotificationsMark(id);
                string Result = _userdbfactory.NotificationsMark(id);
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        public JsonResult UnlocationMapping(int Countryid)
        {
            try
            {
                //UserDBFactory mFactory = new UserDBFactory();
                string Result = _userdbfactory.UnlocationMapping(Countryid);
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        public JsonResult EncryptId(string plainText)
        {
            StringBuilder Result = new StringBuilder();

            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

            string[] mul = plainText.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            string JResult = Result.ToString();
            return Json(JResult, JsonRequestBehavior.AllowGet);
        }

        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            Dictionary<string, long> dic = new Dictionary<string, long>();
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().Contains("DATE"))
                    {
                        if (dr[col].ToString().Trim() != "")
                        {
                            DateTime colDateTime = Convert.ToDateTime(Convert.ToString(dr[col]));
                            temp_row.Add(col.ColumnName, string.IsNullOrEmpty(Convert.ToString(colDateTime)) ? string.Empty : colDateTime.ToString("dd-MMM-yyyy hh:mm tt"));
                        }
                        else
                        {
                            temp_row.Add(col.ColumnName, dr[col]);
                        }
                    }
                    else
                    {
                        temp_row.Add(col.ColumnName, dr[col]);
                    }
                }
                trows.Add(temp_row);
            }
            return trows;
        }
        public JsonResult GetTaxdetails(int countryid)
        {
            TaxDetails tax = new TaxDetails();
            UserDBFactory mFactory = new UserDBFactory();
            DataSet Result = mFactory.GetCountryTaxDetails(countryid);
            if (!ReferenceEquals(Result, null) && Result.Tables[0].Rows.Count > 0)
            {
                tax.IndirectTaxapplicable = Convert.ToString(Result.Tables[0].Rows[0]["INDIRECTTAXAPPLICABILITY"]);
                tax.Taxlabel = Convert.ToString(Result.Tables[0].Rows[0]["INDIRECTTAXLABEL"]);
                tax.Ispostcode = Convert.ToString(Result.Tables[0].Rows[0]["ISPOSTCODE"]);
                tax.Indtaxmaxlength = Convert.ToString(Result.Tables[0].Rows[0]["INDTAXMAXIMUMLENGTH"]);
            }
            return Json(tax, JsonRequestBehavior.AllowGet);
        }
        //Dashboard tab
        public ActionResult Dashboard()
        {
            TrackingDBFactory mFactory = new TrackingDBFactory();
            List<DashBoardEntity> trackentity = mFactory.GetDashBoardData(HttpContext.User.Identity.Name.ToString());
            DBModel mUIModel = new DBModel();

            foreach (var val in trackentity.Where(i => i.Status != "Credit"))
            {
                mUIModel.BOOKINGSDONE = val.BOOKINGSDONE;
                mUIModel.TOTALVOLUME = val.TOTALVOLUME;
                mUIModel.TotalSpent = val.TotalSpent;
            }

            if (trackentity.Where(i => i.Status == "Credit").Count() > 0)
            {
                mUIModel.CREDITLIMIT = trackentity.Where(i => i.Status == "Credit").FirstOrDefault().TotalSpent;
                mUIModel.CURRENCYID = trackentity.Where(i => i.Status == "Credit").FirstOrDefault().CURRENCYID;
            }
            else
            {
                mUIModel.CREDITLIMIT = 0;
            }
            UserDBFactory userFactory = new UserDBFactory();
            DBFactory.Entities.UserProfileEntity mDBEntity = userFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 1);

            UserProfileModel muserModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
            muserModel.Dashboard = mUIModel;
            if (string.IsNullOrEmpty(muserModel.Dashboard.CURRENCYID))
            {
                muserModel.Dashboard.CURRENCYID = muserModel.PREFERREDCURRENCYCODE;
            }
            ViewBag.TabStaus = "Dashboard";
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("ProfilePage", muserModel);
        }
        //Dashboard tab
        //Credit status Mails

        [AllowAnonymous]
        public void SendCreditStatusMail(string userid, string type = "", string ammount = "", string reason = "")
        {
            UserDBFactory mFactory = new UserDBFactory();
            DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(userid, 2);
            UserProfileModel mUIModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
            string crm = System.Configuration.ConfigurationManager.AppSettings["CRMEmail"];
            string user = Convert.ToString(mUIModel.UserId);
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(user));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Business Credit Status" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/GSSC-CreditStatus.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            if (!ReferenceEquals(mDBEntity, null))
                body = body.Replace("{User}", mDBEntity.FIRSTNAME);
            if (mUIModel.CreditStatus == "Additional")
            {
                body = body.Replace("{CreditStatus}", "We need some additional details to process your Business Credit application."
                                                    + "<a href=" + System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "UserManagement/Credit>Click here</a> to access your application and provide information and / or documentation."
                                                    + "<br><br>"
                                                    + "Once you’ve provided us with the requested information we will resume the review process. We will notify you when your credit has been approved.");
            }
            else if (mUIModel.CreditStatus == "Rejected")
            {
                crm = string.IsNullOrEmpty(mUIModel.CRM) ? crm : mUIModel.CRM;
                body = body.Replace("{CreditStatus}", "Your credit application has been rejected.<br>Rejected reason: " + mUIModel.REJECTIONCOMMENTS + ".<br> Please contact your CRM (<a href=" + crm + ">" + crm + "</a>) for other information ");
            }
            else if (mUIModel.CreditStatus == "Approved")
            {
                if (type == "CreditAdjustment")
                    body = body.Replace("{CreditStatus}", "Your  credit limit has been adujsted with " + ammount + " " + mUIModel.CURRENCYID + ".<br> Current outstanding balance is " + mUIModel.CURRENTOSBALANCE + " " + mUIModel.CURRENCYID);
                else if (type == "Override")
                    body = body.Replace("{CreditStatus}", "Your  credit limit has been overridden with " + ammount + " " + mUIModel.CURRENCYID + ".<br> Approved credit amount is " + mUIModel.APPROVEDCREDITLIMIT + " " + mUIModel.CURRENCYID);
                else if (type == "Addtional" || type == "Additional")
                {
                    crm = string.IsNullOrEmpty(mUIModel.CRM) ? crm : mUIModel.CRM;
                    if (ammount != "")
                        body = body.Replace("{CreditStatus}", "Your Additional credit limit has been Approved with " + ammount + " " + mUIModel.CURRENCYID + ".<br> Current outstanding balance is " + mUIModel.CURRENTOSBALANCE + " " + mUIModel.CURRENCYID);
                    else
                        body = body.Replace("{CreditStatus}", "Your Additional credit application has been rejected.<br>Rejected reason: " + reason + " <br> Approved credit amount is " + mUIModel.APPROVEDCREDITLIMIT + " " + mUIModel.CURRENCYID + " <br> Please contact your CRM (<a href=" + crm + ">" + crm + "</a>) for other information ");
                }
                else if (type == "NeedMoreInfo")
                {
                    body = body.Replace("{CreditStatus}", "We need some additional details to process your Business Credit application."
                                                  + "<a href=" + System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "UserManagement/Credit>Click here</a> to access your application and provide information and / or documentation."
                                                  + "<br><br>"
                                                  + "Once you’ve provided us with the requested information we will resume the review process. We will notify you when your credit has been approved.");
                }
                else
                    body = body.Replace("{CreditStatus}", "Your approved credit limit is " + mUIModel.CURRENTOSBALANCE + " " + mUIModel.CURRENCYID);
            }
            else if (mUIModel.CreditStatus == "ReOpen")
            {
                body = body.Replace("{CreditStatus}", "Your credit application has reopened");
            }
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", user, "", "",
       "UserManagement", "Business credit status", userid, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());


        }


        //Credit Mails
        #region ProfileImageResize
        private static Image ResizeImage(Image image, int width, int height)
        {
            Bitmap resizedImage = new Bitmap(width, height);
            using (Graphics gfx = Graphics.FromImage(resizedImage))
            {
                gfx.DrawImage(image, new Rectangle(0, 0, width, height),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            return resizedImage;
        }
        public Image GrayScale(Bitmap Bmp)
        {
            int rgb;
            Color c;

            for (int y = 0; y < Bmp.Height; y++)
                for (int x = 0; x < Bmp.Width; x++)
                {
                    c = Bmp.GetPixel(x, y);
                    rgb = (int)((c.R + c.G + c.B) / 3);
                    Bmp.SetPixel(x, y, Color.FromArgb(rgb, rgb, rgb));
                }
            return Bmp;
        }
        #endregion
        [AllowAnonymous]
        [EncryptedActionParameter]
        public void DownloadCreditDoc(string filename)
        {
            try
            {
                // UserDBFactory mFactory = new UserDBFactory();
                IList<SSPDocumentsEntity> mDBEntity = _userdbfactory.GetCreditDocument(HttpContext.User.Identity.Name, filename);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    byte[] bytes;
                    memoryStream.Close();
                    bytes = (byte[])mDBEntity.FirstOrDefault().FILECONTENT;
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + mDBEntity.FirstOrDefault().FILENAME);
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                    Response.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }
        /// <summary>
        /// Download Additional Credit Documnets
        /// </summary>
        /// <param name="filename"></param>
        [AllowAnonymous]
        [EncryptedActionParameter]
        public void DownloadAddCreditDoc(string filename)
        {
            try
            {
                //UserDBFactory mFactory = new UserDBFactory();
                IList<SSPDocumentsEntity> mDBEntity = _userdbfactory.GetAddCreditDocument(HttpContext.User.Identity.Name, filename);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    byte[] bytes;
                    memoryStream.Close();
                    bytes = (byte[])mDBEntity.FirstOrDefault().FILECONTENT;
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + mDBEntity.FirstOrDefault().FILENAME);
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                    Response.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }
        [HttpPost]
        public JsonResult Deletecreditdoc(int docid)
        {
            try
            {
                // UserDBFactory mFactory = new UserDBFactory();
                _userdbfactory.DeleteCreditDocument(HttpContext.User.Identity.Name, docid);
                var JsonToReturn = new
                {
                    rows = "success",
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet); ;
            }
        }
        /// <summary>
        /// Delete Additional Credit Documents
        /// </summary>
        /// <param name="docid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteAddcreditdoc(int docid)
        {
            try
            {
                // UserDBFactory mFactory = new UserDBFactory();
                _userdbfactory.DeleteAddCreditDocument(HttpContext.User.Identity.Name, docid);
                var JsonToReturn = new
                {
                    rows = "success",
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet); ;
            }

        }
        public JsonResult SaveCreditComments(string comments, long? addcreditappid)
        {
            bool result = true;
            //credit comments
            try
            {
                UserDBFactory mFactory = new UserDBFactory();

                DBFactory.Entities.UserProfileEntity creditcommentEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name, 2);
                CreditCommentEntity objCreditComment = new CreditCommentEntity();
                if (creditcommentEntity != null)
                    objCreditComment.CREDITAPPID = creditcommentEntity.CREDITAPPID;
                objCreditComment.ADDITIONALCREDITID = addcreditappid;
                objCreditComment.COMMENTS = comments;
                objCreditComment.COMMENTSFROM = "SHIPA";
                objCreditComment.CREATEDBY = HttpContext.User.Identity.Name;
                mFactory.SaveCreditComments(objCreditComment);
            }
            catch (Exception ex)
            {
                result = false;
            }

            //credit comments
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }


    public class ReturnValues
    {
        public bool IsLoginSuccess { get; set; }
        public string Message { get; set; }
    }

    public class TokenDetails
    {
        public string Token { get; set; }
        public DateTime TokenExpiry { get; set; }
    }
    public class TaxDetails
    {
        public string IndirectTaxapplicable { get; set; }
        public string Taxlabel { get; set; }
        public string Ispostcode { get; set; }
        public string Indtaxmaxlength { get; set; }
    }
}

#region Commented code
//IList<CreditEntity> UserCredits = new List<CreditEntity>();
//UserCredits = mDBEntity.CreditItems;
//if (UserCredits != null && UserCredits.Count > 0)
//{
//    mDBEntity.CREDITAPPID = UserCredits[0].CREDITAPPID;
//    mDBEntity.INDUSTRYID = UserCredits[0].INDUSTRYID;
//    mDBEntity.NOOFEMP = UserCredits[0].NOOFEMP;
//    mDBEntity.PARENTCOMPANY = UserCredits[0].PARENTCOMPANY;
//    if (UserCredits[0].ANNUALTURNOVER != 0)
//    {
//        mDBEntity.ANNUALTURNOVERCURRENCY = UserCredits[0].ANNUALTURNOVER.ToString();
//    }
//    mDBEntity.DBNUMBER = UserCredits[0].DBNUMBER;
//    mDBEntity.HFMENTRYCODE = UserCredits[0].HFMENTRYCODE;
//    if (UserCredits[0].REQUESTEDCREDITLIMIT != 0)
//    {
//        mDBEntity.REQUESTEDCREDITLIMITCURRENCY = UserCredits[0].REQUESTEDCREDITLIMIT.ToString();
//    }
//    mDBEntity.CURRENTPAYTERMSNAME = UserCredits[0].CURRENTPAYTERMSNAME;
//    mDBEntity.COMMENTS = UserCredits[0].COMMENTS;
//    mDBEntity.CURRENCYID = UserCredits[0].CURRENCYID;
//    mDBEntity.APPROVEDCREDITLIMIT = UserCredits[0].APPROVEDCREDITLIMIT;
//    mDBEntity.CURRENTOSBALANCE = UserCredits[0].CURRENTOSBALANCE;
//}
//DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name);
//IList<UMUserAddressDetailsEntity> UserAdr = new List<UMUserAddressDetailsEntity>();
//UserAdr = mDBEntity.UserAddress;

//if (UserAdr != null && UserAdr.Count > 0)
//{
//    ViewBag.Useraddressdata = 1;
//    mDBEntity.UAUSERADTLSID = UserAdr[0].USERADTLSID;
//    mDBEntity.UAADDRESSTYPE = UserAdr[0].ADDRESSTYPE;
//    mDBEntity.UAHOUSENO = UserAdr[0].HOUSENO;
//    mDBEntity.UABUILDINGNAME = UserAdr[0].BUILDINGNAME;
//    mDBEntity.UAADDRESSLINE1 = UserAdr[0].ADDRESSLINE1;
//    mDBEntity.UAADDRESSLINE2 = UserAdr[0].ADDRESSLINE2;
//    mDBEntity.UACOUNTRYID = UserAdr[0].COUNTRYID;
//    mDBEntity.UACOUNTRYCODE = UserAdr[0].COUNTRYCODE;
//    mDBEntity.UACOUNTRYNAME = UserAdr[0].COUNTRYNAME;
//    mDBEntity.UASTATEID = UserAdr[0].STATEID;
//    mDBEntity.UASTATECODE = UserAdr[0].STATECODE;
//    mDBEntity.UASTATENAME = UserAdr[0].STATENAME;
//    mDBEntity.UACITYID = UserAdr[0].CITYID;
//    mDBEntity.UACITYCODE = UserAdr[0].CITYCODE;
//    mDBEntity.UACITYNAME = UserAdr[0].CITYNAME;
//    mDBEntity.UAPOSTCODE = UserAdr[0].POSTCODE;
//    mDBEntity.UAFULLADDRESS = UserAdr[0].FULLADDRESS;
//    mDBEntity.UAISDEFAULT = UserAdr[0].ISDEFAULT;
//    mDBEntity.COUNTRYNAME = UserAdr[0].COUNTRYNAME;
//}

//IList<CreditEntity> UserCredits = new List<CreditEntity>();
//UserCredits = mDBEntity.CreditItems;
//if (UserCredits != null && UserCredits.Count > 0)
//{
//    mDBEntity.CREDITAPPID = UserCredits[0].CREDITAPPID;
//    mDBEntity.INDUSTRYID = UserCredits[0].INDUSTRYID;
//    mDBEntity.NOOFEMP = UserCredits[0].NOOFEMP;
//    mDBEntity.PARENTCOMPANY = UserCredits[0].PARENTCOMPANY;
//    if (UserCredits[0].ANNUALTURNOVER != 0)
//    {
//        mDBEntity.ANNUALTURNOVERCURRENCY = UserCredits[0].ANNUALTURNOVER.ToString();
//    }
//    mDBEntity.DBNUMBER = UserCredits[0].DBNUMBER;
//    mDBEntity.HFMENTRYCODE = UserCredits[0].HFMENTRYCODE;
//    if (UserCredits[0].REQUESTEDCREDITLIMIT != 0)
//    {
//        mDBEntity.REQUESTEDCREDITLIMITCURRENCY = UserCredits[0].REQUESTEDCREDITLIMIT.ToString();
//    }
//    mDBEntity.CURRENTPAYTERMSNAME = UserCredits[0].CURRENTPAYTERMSNAME;
//    mDBEntity.COMMENTS = UserCredits[0].COMMENTS;
//    mDBEntity.CURRENCYID = UserCredits[0].CURRENCYID;
//    mDBEntity.APPROVEDCREDITLIMIT = UserCredits[0].APPROVEDCREDITLIMIT;
//    mDBEntity.CURRENTOSBALANCE = UserCredits[0].CURRENTOSBALANCE;
//}


//IList<DepartmentEntity> Departments = mdmFactory.GetOrganizationalUnits(string.Empty, string.Empty);
//IList<JobTitleEntity> JobTitleData = mFactory.GetJobTitleDetails(string.Empty);
//  ViewBag.JobTitleData = JobTitleData;
// ViewBag.Departments = Departments;
//ViewBag.LoadImage = (mDBEntity.IMGCONTENT != null && mDBEntity.IMGCONTENT.Length > 0) ? Convert.ToBase64String(mDBEntity.IMGCONTENT) : "";
#endregion