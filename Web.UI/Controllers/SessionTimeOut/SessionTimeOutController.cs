﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FOCiS.SSP.Library.Extensions;
using System.ComponentModel.DataAnnotations;
using System;
using System.Reflection;
using FOCiS.SSP.Models.UserManagement;
using System.Web.Security;
using System.Net.Mail;
using FOCiS.SSP.DBFactory.Factory;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Net;

namespace FOCiS.SSP.Web.UI.Controllers.UserManagement
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class SessionTimeOutController : BaseController
    {
        public ActionResult Index()
        {
            return View("SessionTimeOut");
        }

    }
}