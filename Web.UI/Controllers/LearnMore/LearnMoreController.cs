﻿using AutoMapper;
using FindUserCountryName;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace FOCiS.SSP.Web.UI.Controllers.LearnMore
{
    /// <summary>
    /// LearnMore Class for shipa documnets
    /// </summary>
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class LearnMoreController : BaseController
    {
        /// <summary>
        /// Learnmore method   about ShipA Freight 
        /// </summary>
        /// <returns></returns>
        [ActionName("learn-more")]
        public ActionResult Learnmore()
        {
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            //for canonical tag   
            ViewBag.MetaTitle = "Why Choose Us | Shipafreight.com";
            ViewBag.MetaDescription = "Know more about us and why you should choose us for all your freight needs. We offer end to end freight forwarding through Air & Ocean worldwide. Visit our website now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "learn-more";
            ViewBag.MetaRobots = "NOINDEX,NOFOLLOW,NOODP,NOYDIR";
            return View("Learnmore");
        }

        /// <summary>
        /// About How Shipa Works
        /// </summary>
        /// <returns></returns>
        [ActionName("how-it-works")]
        public ActionResult HowitWorks()
        {
            //for canonical tag     
            ViewBag.MetaTitle = "How It Works | Quick Guide | Shipafreight.com";
            ViewBag.MetaDescription = "Our quick guide shows how ShipAfreight.com offers quick online quote booking and tracking process. We provide international freight forwarding services that are hassle free, quick and easy to use. Visit now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "learn-more/how-it-works";
            return View("HowitWorks");
        }
        /// <summary>
        /// About Shipa Documents List
        /// </summary>
        /// <returns></returns>
        [ActionName("documents-list")]
        public ActionResult DocumentsList()
        {
            //for canonical tag    
            ViewBag.MetaTitle = "Key Freight Documents for International Shipping | Shipafreight.com";
            ViewBag.MetaDescription = "We have listed down the most important documents required in international inbound shipments - a Bill of Lading, and/or Shipper's Letter of Instruction, a commercial invoice, a packing list and more. Visit now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "learn-more/documents-list";
            return View("DocumentsList");
        }

        /// <summary>
        /// About shipa Terms and Conditions
        /// </summary>
        /// <returns></returns>
        [ActionName("terms-and-conditions")]
        public ActionResult TermsandConditions()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "Terms & Conditions | Shipafreight.com";
            ViewBag.MetaDescription = "All freight forwarding/shipping services provided by ShipAfreight.com to the “Customer”, whether the Customer is a manufacturer, distributor, exporter, importer, sender, consignor, consignee, transferor, or transferee of the shipment, will be subject to the terms and conditions set forth herein.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "terms-and-conditions";
            ViewBag.MetaRobots = "NOINDEX,NOFOLLOW,NOODP,NOYDIR";
            return View("TermsandConditions");
        }
        /// <summary>
        /// Redirect method for LernMore.
        /// </summary>
        /// <param name="actionname">action name</param>
        /// <returns></returns>
        public ActionResult Redirect(string actionname)
        {
            return RedirectToRoutePermanent(new
            {
                controller = "LearnMore",
                action = actionname
            });
            // return RedirectPermanent("LearnMore/"+actionname);
        }

        [ActionName("transit-time")]
        public ActionResult TransitTime()
        {
            //for canonical tag   
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "learn-more/transit-time";
            return View("TransitTime");
        }
        public ActionResult _TermsandConditions()
        {
            return PartialView("_TermsandConditions");
        }
        public ActionResult _CookiePolicy()
        {
            return PartialView("_CookiePolicy");
        }

        public ActionResult PrivacyPolicy()
        {
            return View("PrivacyPolicy");
        }
    }

}