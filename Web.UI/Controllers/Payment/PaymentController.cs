﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.Http;
using System.Data;
using System.Globalization;
using System.IO;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Factory;
using AutoMapper;
using FOCiS.SSP.Web.UI.Controllers.Base;
using FOCiS.SSP.Web.UI.Filters;
using log4net;
using Stripe;
using System.Threading;
using FOCiS.SSP.DBFactory.Entities;
using FindUserCountryName;
using FOCiS.SSP.Models.Payment;
using FOCiS.SSP.Web.UI.Controllers.JobBooking;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Text;
using FOCiS.SSP.Web.UI.Helpers;


namespace FOCiS.SSP.Web.UI.Controllers.Payment
{
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class PaymentController : BaseController
    {
        private static readonly ILog Log =
             LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IJobBookingDbFactory _JFactory;

        public PaymentController()
        {
            _JFactory = new JobBookingDbFactory();
        }

        public PaymentController(IJobBookingDbFactory jFactory)
        {
            _JFactory = jFactory;
        }

        public ActionResult OnlineCCSuccess(MPaymentResult e)
        {
            return View("OnlineCCSuccess", e);
        }

        public ActionResult OnlineCCFailure(MPaymentResult e)
        {          
            return View("OnlineCCFailure", e);
        }

        public ActionResult ServerError(MPaymentResult e)
        {           
            return View("ServerError", e);
        }

        public ActionResult PaymentDone(MPaymentResult e)
        {
            return View("PaymentDone", e);
        }

        public ActionResult OfflinePBSuccess(MPaymentResult e)
        {
            return View("OfflinePBSuccess", e);
        }

        public ActionResult OfflineWTSuccess(MPaymentResult e)
        {
            return View("OfflineWTSuccess", e);
        }

        public ActionResult OfflinePBFailure(MPaymentResult e)
        {
            return View("OfflinePBFailure", e);
        }

        public ActionResult OfflineWTFailure(MPaymentResult e)
        {
            return View("OfflineWTFailure", e);
        }

        public ActionResult BusinessCreditSuccess(MPaymentResult model)
        {
            return View("BusinessCreditSuccess",model);
        }

        public ActionResult BusinessCreditFailure(MPaymentResult model)
        {
            return View("BusinessCreditFailure", model);
        }

        public ActionResult MOnlineCCSuccess(MPaymentResult e)
        {
            return View("MOnlineCCSuccess", e);
        }

        public ActionResult MOnlineCCFailure(MPaymentResult e)
        {
            return View("MOnlineCCFailure", e);
        }

        public ActionResult MServerError(MPaymentResult e)
        {
            return View("MServerError", e);
        }

        public ActionResult MPaymentDone(MPaymentResult e)
        {
            return View("MPaymentDone", e);
        }

        public ActionResult MOfflinePBSuccess(MPaymentResult e)
        {
            return View("MOfflinePBSuccess", e);
        }

        public ActionResult MOfflineWTSuccess(MPaymentResult e)
        {
            return View("MOfflineWTSuccess", e);
        }

        public ActionResult MOfflinePBFailure(MPaymentResult e)
        {
            return View("MOfflinePBFailure", e);
        }

        public ActionResult MOfflineWTFailure(MPaymentResult e)
        {
            return View("MOfflineWTFailure", e);
        }

        public ActionResult MBusinessCreditSuccess(MPaymentResult model)
        {
            return View("MBusinessCreditSuccess", model);
        }

        public ActionResult MBusinessCreditFailure(MPaymentResult model)
        {
            return View("MBusinessCreditFailure", model);
        }

        public JsonResult StripePayoutEvent()
        {
            //JobBookingController jbcon = new JobBookingController();
            ThreadPool.QueueUserWorkItem(o =>
            {
                SafeRun<bool>(() =>
                {
                    //JobBookingDbFactory db = new JobBookingDbFactory();
                    var data = _JFactory.GetPendingPayouts();
                    var resp = new List<OnlinePaymentTransactionEntity>();

                    if (data.Any())
                    {
                        var balanceService = new StripeBalanceService();
                        foreach (var d in data)
                        {
                            try
                            {
                                StripeData stripeData = GetStripeData(d.CURRENCY);
                                StripeConfiguration.SetApiKey(stripeData.secretKey);
                                StripeBalanceTransaction transaction = balanceService.Get(d.STRIPETRANSACTIONID);
                                if (d.PAYOUTSTATUS != transaction.Status)
                                {
                                    d.PAYOUTSTATUS = transaction.Status;
                                    resp.Add(d);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.ErrorFormat("Payout Errror {0}, : STRIPETRANSACTIONID : {1}", ex.Message.ToString(), d.STRIPETRANSACTIONID);
                                Log.ErrorFormat("Payout StackTrace {0}", ex.StackTrace.ToString());
                            }
                        }
                    }

                    if (resp.Any())
                        _JFactory.UpdatePayoutStatus(data);

                    return true;
                });                
            });
            return Json("OK");
        }

        private StripeData GetStripeData(string currency)
        {
            StripeData stripeData = new StripeData();
            if (currency.ToLowerInvariant() == "eur" || currency.ToLowerInvariant() == "gbp")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_NL"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_NL"];
                stripeData.account = "stripeNetherlands";
            }
            else if (currency.ToLowerInvariant() == "chf")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_CH"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_CH"];
                stripeData.account = "stripeSwiss";
            }
            else if (currency.ToLowerInvariant() == "sgd")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_SG"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_SG"];
                stripeData.account = "stripeSingapore";
            }
            else
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_US"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_US"];
                stripeData.account = "stripeUSA";
            }
            return stripeData;
        }

        public ActionResult HsbcWebhook()
        {
            Request.InputStream.Position = 0;
            var input = new StreamReader(Request.InputStream).ReadToEnd();
            string json = HsbcEncryption.decryptAndVerify(input);

            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress("pannepu@agility.com"));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = "Exception notification" + EnvironmentName;
            message.IsBodyHtml = true;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"
                <table style='height: 74px;' width='284'>
                <tbody>
                <tr style='height: 17px;'>
                <td style='width: 134px; height: 17px;'>URL</td>"
            );
            sb.AppendLine("<td style='width: 134px; height: 17px;'>"); sb.Append("JsonResult HsbcWebhook"); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 17px;'>
                <td style='width: 134px; height: 17px;'>EXCEPTION</td>");
            sb.AppendLine("<td style='width: 134px; height: 17px;'>"); sb.Append(""); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 18px;'>
                <td style='width: 134px; height: 18px;'>STACKTRACE</td> ");
            sb.AppendLine("<td style='width: 134px; height: 18px;'>"); sb.Append(json); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                </tbody>
                </table>");

            message.Body = sb.ToString();
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();

            return Json("SUCCESS");
        }

        public ActionResult HsbcResponse(HsbcFrontResponse req)
        {
            var details = _JFactory.GetHsbcJobNumber(req.order_id);

            if (req.order_pay_code == "1111")
            {
                var routeData = new MPaymentResult()
                {
                    JOBNUMBER = details.JOBNUMBER,
                    QUOTATIONID = details.QUOTATIONID,
                    USERID = details.CREATEDBY,
                    DATA = req.order_pay_msg
                };
                return RedirectToActionPermanent("OnlineCCFailure", routeData);
            }
            else
            {
                var routeData = new MPaymentResult()
                {
                    JOBNUMBER = details.JOBNUMBER,
                    QUOTATIONID = details.QUOTATIONID,
                    USERID = details.CREATEDBY,
                    TOKEN = req.order_id
                };
                return RedirectToActionPermanent("OnlineCCSuccess", routeData);
            }
        }

        public ActionResult HsbcRefundResponse()
        {
            return Json("SUCCESS");
        }
    }

    public class HsbcFrontResponse
    {
        public string pay_type { get; set; }
        public string order_st { get; set; }
        public string fy_ssn { get; set; }
        public string order_pay_code { get; set; }
        public string mchnt_cd { get; set; }
        public string order_amt { get; set; }
        public string user_id { get; set; }
        public string card_no { get; set; }
        public string RSA { get; set; }
        public string order_date { get; set; }
        public string order_id { get; set; }
        public string order_pay_msg { get; set; }
    }
}
