﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Web.UI.Controllers.Base;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FOCiS.SSP.Models.Payment;
using System.Security.Cryptography;
using System.Net.Mail;
using FOCiS.SSP.Models.QM;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using Microsoft.Reporting.WebForms;
using System.Collections;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Web.Helpers;
using Newtonsoft.Json.Linq;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.Web.UI.Controllers.UserManagement;
using FOCiS.SSP.Library.Extensions;
using log4net;
using System.Web.Security;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Threading;
using Chikoti.Helpers;
using FOCiS.SSP.Web.UI.Helpers;
using FindUserCountryName;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Jose;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using System.Net.Http.Headers;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.Drawing.Imaging;

namespace FOCiS.SSP.Web.UI.Controllers.JobBooking
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]

    public sealed class ValidateJsonAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
    {


        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            var httpContext = filterContext.HttpContext;
            var cookie = httpContext.Request.Cookies[AntiForgeryConfig.CookieName];
            AntiForgery.Validate(cookie != null ? cookie.Value : null, httpContext.Request.Headers["__RequestVerificationToken"]);
        }

    }


    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class JobBookingController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        private static string format = "{0:N2}";
        private static QuotationPreviewModel mUIModels;

        private static UserProfileEntity mUserModel;
        private static int z = 0;
        private static byte[] bytes;
        private static Decimal mQuoteChargesTotal = 0;
        private static int PDFpage;
        string chargeDescription = string.Empty;
        private static string Ports = "";
        private static string Track = "";
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string ChargeSetTotal = string.Empty;
        [HttpPost]
        public JsonResult StripePayment(int mJobnumber, string mCurrency, string mDescription, string mSource, string mCouponCode, string mUser, string mPaySource, double requestedAmount = 0, string three_d_secure = "")
        {
            string UserHostAddress = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
            string PaymentStatus = CheckPaymentStatus(mJobnumber);
            MPaymentResult e = new MPaymentResult();
            if (PaymentStatus.ToLower() == "notexist")
            {
                try
                {
                    double PayAmount = 0;
                    string key = string.Empty;
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(mJobnumber, mUser, mCouponCode, requestedAmount);
                    PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    if (mUIModel.NOTSUPPAMOUNTUSD != 0)
                    {
                        PayAmount = Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) * 100;
                        mCurrency = "USD";
                    }
                    else
                    {
                        PayAmount = Convert.ToDouble(mUIModel.ReceiptNetAmount) * 100;
                    }
                    StripeData stripeData = GetStripeData(mCurrency);
                    var myCharge = new StripeChargeCreateOptions();
                    myCharge.Amount = Convert.ToInt32(PayAmount);
                    myCharge.Currency = mCurrency;
                    myCharge.Description = mDescription;
                    myCharge.SourceTokenOrExistingSourceId = mSource;
                    var dic = new Dictionary<string, string>();
                    dic.Add("id", mJobnumber.ToString());
                    dic.Add("email", mUser);
                    dic.Add("discountcode", mCouponCode);
                    myCharge.Metadata = dic;
                    myCharge.ReceiptEmail = mUser;
                    key = stripeData.secretKey;
                    var chargeService = new StripeChargeService(key);
                    chargeService.ExpandBalanceTransaction = true;
                    if (three_d_secure == "required") return Create3DSource(myCharge, mPaySource);
                    try
                    {
                        StripeCharge stripeCharge = chargeService.Create(myCharge);
                        if (stripeCharge.Status == "succeeded")
                        {
                            e.USERID = mUser;
                            e.TOKEN = mSource;
                            e.JOBNUMBER = mUIModel.JobNumber;
                            e.RECEIPTNETAMOUNT = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) : Convert.ToDouble(mUIModel.ReceiptNetAmount);
                            e.CURRENCY = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? "USD" : mCurrency;
                            ThreadPool.QueueUserWorkItem(o =>
                            {
                                try
                                {
                                    SafeRun<bool>(() =>
                                    {
                                        var Baltran = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                        mFactory.SaveStripeTransactionDetails(
                                           mJobnumber.ToString(),
                                           stripeCharge.Status,
                                           (Convert.ToDouble(stripeCharge.Amount) / 100).ToString(),
                                           mCurrency,
                                           "Credit/Debit",
                                           mSource,
                                           stripeCharge.Id,
                                           stripeCharge.BalanceTransactionId,
                                           stripeCharge.BalanceTransaction.Currency.ToUpperInvariant(),
                                           (Convert.ToDouble(stripeCharge.BalanceTransaction.Amount) / 100).ToString(),
                                           (Convert.ToDouble(stripeCharge.BalanceTransaction.Net) / 100).ToString(),
                                           (Convert.ToDouble(stripeCharge.BalanceTransaction.Fee) / 100).ToString(),
                                           stripeCharge.BalanceTransaction.Status,
                                           Baltran.balance_transaction.exchange_rate == null ? "1" : Baltran.balance_transaction.exchange_rate,
                                           stripeData.account
                                       );
                                        return true;
                                    });
                                    string brand = string.Empty;
                                    string last4 = string.Empty;
                                    try
                                    {
                                        StripeCardModel carddata = FetchCardData(stripeCharge);
                                        brand = carddata.Brand;
                                        last4 = carddata.Last4;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                        Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                    }

                                    SavePayment(
                                        mSource,
                                        mUIModel.RECEIPTHDRID.ToString(),
                                        mUIModel.JobNumber.ToString(),
                                        mUIModel.QuotationId.ToString(),
                                        brand,
                                        last4,
                                        "approved",
                                        mCouponCode,
                                        mUser,
                                        PayAmount,
                                        requestedAmount,
                                        UserHostAddress,
                                        "Success"
                                    );
                                    if (mCouponCode.ToUpper().Contains("M1"))
                                    {
                                        SendReferal_CreditAdded_Email(mCouponCode, mUser, mCurrency, requestedAmount);
                                    }
                                    else
                                    {
                                        SendReferal_CreditAdded_Email(mCouponCode, mUser, mCurrency, 0);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", "While saving to DB for success online Payment", DateTime.Now.ToString()));
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                    Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                }
                            });

                            if (mPaySource.ToLowerInvariant() == "web")
                            {
                                return Json(new
                                {
                                    redirectUrl = Url.Action("OnlineCCSuccess", "Payment", e),
                                    isRedirect = true
                                });
                            }
                            else
                            {
                                return Json(new
                                {
                                    redirectUrl = Url.Action("MOnlineCCSuccess", "Payment", e),
                                    isRedirect = true
                                });
                            }
                        }
                        else
                        {
                            e.DATA = stripeCharge.FailureMessage;
                            e.TOKEN = mSource;
                            e.JOBNUMBER = mUIModel.JobNumber;
                            e.QUOTATIONID = mUIModel.QuotationId;
                            e.RECEIPTNETAMOUNT = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) : Convert.ToDouble(mUIModel.ReceiptNetAmount);
                            e.CURRENCY = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? "USD" : mCurrency;
                            ThreadPool.QueueUserWorkItem(o =>
                            {
                                try
                                {
                                    SafeRun<bool>(() =>
                                    {
                                        var Baltran = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                        mFactory.SaveStripeTransactionDetails(
                                           mJobnumber.ToString(),
                                           stripeCharge.Status,
                                           (Convert.ToDouble(stripeCharge.Amount) / 100).ToString(),
                                            mCurrency,
                                           "Credit/Debit",
                                            mSource,
                                            stripeCharge.Id,
                                            stripeCharge.FailureMessage,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            Baltran.balance_transaction.exchange_rate == null ? "1" : Baltran.balance_transaction.exchange_rate,
                                            stripeData.account
                                        );
                                        return true;
                                    });
                                    string brand = string.Empty;
                                    string last4 = string.Empty;
                                    try
                                    {
                                        StripeCardModel carddata = FetchCardData(stripeCharge);
                                        brand = carddata.Brand;
                                        last4 = carddata.Last4;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                        Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                    }
                                    SavePayment(
                                        mSource,
                                        mUIModel.RECEIPTHDRID.ToString(),
                                        mUIModel.JobNumber.ToString(),
                                        mUIModel.QuotationId.ToString(),
                                        brand,
                                        last4,
                                        "failed",
                                        mCouponCode,
                                        mUser,
                                        PayAmount,
                                        requestedAmount,
                                        UserHostAddress,
                                        stripeCharge.FailureMessage
                                    );
                                }
                                catch (Exception ex)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", "While saving to DB for failure online Payment", DateTime.Now.ToString()));
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                    Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                }
                            });

                            if (mPaySource.ToLowerInvariant() == "web")
                            {
                                return Json(new
                                {
                                    redirectUrl = Url.Action("OnlineCCFailure", "Payment", e),
                                    isRedirect = true
                                });
                            }
                            else
                            {
                                return Json(new
                                {
                                    redirectUrl = Url.Action("MOnlineCCFailure", "Payment", e),
                                    isRedirect = true
                                });
                            }
                        }
                    }
                    catch (Exception stpEx)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", "While stripe charge online Payment(catch)", DateTime.Now.ToString()));
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", stpEx.Message.ToString(), DateTime.Now.ToString()));
                        Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", stpEx.StackTrace.ToString(), DateTime.Now.ToString()));

                        e.DATA = stpEx.Message.Substring(0, stpEx.Message.IndexOf("."));
                        e.TOKEN = mSource;
                        e.JOBNUMBER = mUIModel.JobNumber;
                        e.QUOTATIONID = mUIModel.QuotationId;
                        e.RECEIPTNETAMOUNT = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) : Convert.ToDouble(mUIModel.ReceiptNetAmount);
                        e.CURRENCY = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? "USD" : mCurrency;
                        SafeRun<bool>(() =>
                        {
                            mFactory.SaveStripeTransactionDetails(
                               mJobnumber.ToString(),
                               "failed",
                               Convert.ToDecimal(PayAmount / 100).ToString(),
                               mCurrency,
                               "Credit/Debit",
                               mSource,
                               "",
                               stpEx.Message.ToString(),
                               "",
                               "",
                               "",
                               "",
                               "",
                               "",
                               stripeData.account
                            );
                            return true;
                        });

                        if (mPaySource.ToLowerInvariant() == "web")
                        {
                            return Json(new
                            {
                                redirectUrl = Url.Action("ServerError", "Payment", e),
                                isRedirect = true
                            });
                        }
                        else
                        {
                            return Json(new
                            {
                                redirectUrl = Url.Action("MServerError", "Payment", e),
                                isRedirect = true
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ShipaExceptions(ex.ToString());
                }
            }
            else
            {
                try
                {
                    double PayAmount = 0;
                    string key = string.Empty;
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(mJobnumber, mUser, mCouponCode, requestedAmount);
                    PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    if (mUIModel.ADDITIONALCHARGESTATUS != null && mUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                    {
                        if (mUIModel.NOTSUPPAMOUNTUSD != 0)
                        {
                            PayAmount = Convert.ToDouble(mUIModel.ADDITIONALCHARGEAMOUNTINUSD) * 100;
                            mCurrency = "USD";
                        }
                        else
                        {
                            PayAmount = Convert.ToDouble(mUIModel.ADDITIONALCHARGEAMOUNT) * 100;
                        }
                        StripeData stripeData = GetStripeData(mCurrency);
                        var myCharge = new StripeChargeCreateOptions();
                        myCharge.Amount = Convert.ToInt32(PayAmount);
                        myCharge.Currency = mCurrency;
                        myCharge.Description = mDescription;
                        myCharge.SourceTokenOrExistingSourceId = mSource;
                        var dic = new Dictionary<string, string>();
                        dic.Add("id", mJobnumber.ToString());
                        dic.Add("email", mUser);
                        dic.Add("discountcode", mCouponCode);
                        dic.Add("additional", "true");
                        myCharge.Metadata = dic;
                        myCharge.ReceiptEmail = mUser;
                        key = stripeData.secretKey;
                        var chargeService = new StripeChargeService(key);
                        chargeService.ExpandBalanceTransaction = true;
                        if (three_d_secure == "required") return Create3DSource(myCharge, mPaySource);
                        try
                        {
                            StripeCharge stripeCharge = chargeService.Create(myCharge);
                            if (stripeCharge.Status == "succeeded")
                            {
                                e.USERID = mUser;
                                e.TOKEN = mSource;
                                e.JOBNUMBER = mUIModel.JobNumber;
                                e.RECEIPTNETAMOUNT = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) : Convert.ToDouble(mUIModel.ReceiptNetAmount);
                                e.CURRENCY = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? "USD" : mCurrency;
                                ThreadPool.QueueUserWorkItem(o =>
                                {
                                    try
                                    {
                                        SafeRun<bool>(() =>
                                        {
                                            var Baltran = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                            mFactory.SaveStripeTransactionDetails(
                                              mJobnumber.ToString(),
                                              stripeCharge.Status,
                                              (Convert.ToDouble(stripeCharge.Amount) / 100).ToString(),
                                              mCurrency,
                                              "Credit/Debit",
                                              mSource,
                                              stripeCharge.Id,
                                              stripeCharge.BalanceTransactionId,
                                              stripeCharge.BalanceTransaction.Currency.ToUpperInvariant(),
                                              (Convert.ToDouble(stripeCharge.BalanceTransaction.Amount) / 100).ToString(),
                                              (Convert.ToDouble(stripeCharge.BalanceTransaction.Net) / 100).ToString(),
                                              (Convert.ToDouble(stripeCharge.BalanceTransaction.Fee) / 100).ToString(),
                                              stripeCharge.BalanceTransaction.Status,
                                              Baltran.balance_transaction.exchange_rate == null ? "1" : Baltran.balance_transaction.exchange_rate,
                                              stripeData.account
                                            );
                                            return true;
                                        });
                                        string brand = string.Empty;
                                        string last4 = string.Empty;
                                        try
                                        {
                                            StripeCardModel carddata = FetchCardData(stripeCharge);
                                            brand = carddata.Brand;
                                            last4 = carddata.Last4;
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                            Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                        }
                                        SavePayment(
                                            mSource,
                                            mUIModel.RECEIPTHDRID.ToString(),
                                            mUIModel.JobNumber.ToString(),
                                            mUIModel.QuotationId.ToString(),
                                            brand,
                                            last4,
                                            "approved",
                                            mCouponCode,
                                            mUser,
                                            PayAmount,
                                            requestedAmount,
                                            UserHostAddress,
                                            "Success"
                                        );
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", "While saving to DB for success online Payment", DateTime.Now.ToString()));
                                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                        Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                    }
                                });
                                if (mPaySource.ToLowerInvariant() == "web")
                                {
                                    return Json(new
                                    {
                                        redirectUrl = Url.Action("OnlineCCSuccess", "Payment", e),
                                        isRedirect = true
                                    });
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        redirectUrl = Url.Action("MOnlineCCSuccess", "Payment", e),
                                        isRedirect = true
                                    });
                                }
                            }
                            else
                            {
                                e.DATA = stripeCharge.FailureMessage;
                                e.TOKEN = mSource;
                                e.JOBNUMBER = mUIModel.JobNumber;
                                e.QUOTATIONID = mUIModel.QuotationId;
                                e.RECEIPTNETAMOUNT = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) : Convert.ToDouble(mUIModel.ReceiptNetAmount);
                                e.CURRENCY = (mUIModel.NOTSUPPAMOUNTUSD != 0) ? "USD" : mCurrency;
                                ThreadPool.QueueUserWorkItem(o =>
                                {
                                    try
                                    {
                                        SafeRun<bool>(() =>
                                        {
                                            var Baltran = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                            mFactory.SaveStripeTransactionDetails(
                                                mJobnumber.ToString(),
                                                stripeCharge.Status,
                                                (Convert.ToDouble(stripeCharge.Amount) / 100).ToString(),
                                                mCurrency,
                                                "Credit/Debit",
                                                mSource,
                                                stripeCharge.Id,
                                                stripeCharge.FailureMessage,
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                Baltran.balance_transaction.exchange_rate == null ? "1" : Baltran.balance_transaction.exchange_rate,
                                                stripeData.account
                                            );
                                            return true;
                                        });
                                        string brand = string.Empty;
                                        string last4 = string.Empty;
                                        try
                                        {
                                            StripeCardModel carddata = FetchCardData(stripeCharge);
                                            brand = carddata.Brand;
                                            last4 = carddata.Last4;
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                            Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                        }
                                        SavePayment(
                                            mSource,
                                            mUIModel.RECEIPTHDRID.ToString(),
                                            mUIModel.JobNumber.ToString(),
                                            mUIModel.QuotationId.ToString(),
                                            brand,
                                            last4,
                                            "failed",
                                            mCouponCode,
                                            mUser,
                                            PayAmount,
                                            requestedAmount,
                                            UserHostAddress,
                                            stripeCharge.FailureMessage
                                        );
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", "While saving to DB for failure online Payment", DateTime.Now.ToString()));
                                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                                        Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                                    }
                                });

                                if (mPaySource.ToLowerInvariant() == "web")
                                {
                                    return Json(new
                                    {
                                        redirectUrl = Url.Action("OnlineCCFailure", "Payment", e),
                                        isRedirect = true
                                    });
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        redirectUrl = Url.Action("MOnlineCCFailure", "Payment", e),
                                        isRedirect = true
                                    });
                                }
                            }
                        }
                        catch (Exception stpEx)
                        {
                            e.DATA = stpEx.Message.Substring(0, stpEx.Message.IndexOf("."));
                            e.TOKEN = mSource;
                            e.JOBNUMBER = mUIModel.JobNumber;
                            e.QUOTATIONID = mUIModel.QuotationId;
                            SafeRun<bool>(() =>
                            {
                                mFactory.SaveStripeTransactionDetails(
                                   mJobnumber.ToString(),
                                   "failed",
                                   Convert.ToDecimal(PayAmount / 100).ToString(),
                                   mCurrency,
                                   "Credit/Debit",
                                   mSource,
                                   "",
                                   stpEx.Message.ToString(),
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   stripeData.account
                                );
                                return true;
                            });
                            if (mPaySource.ToLowerInvariant() == "web")
                            {
                                return Json(new
                                {
                                    redirectUrl = Url.Action("ServerError", "Payment", e),
                                    isRedirect = true
                                });
                            }
                            else
                            {
                                return Json(new
                                {
                                    redirectUrl = Url.Action("MServerError", "Payment", e),
                                    isRedirect = true
                                });
                            }
                        }
                    }
                    else
                    {
                        e.MODE = "Online";
                        e.JOBNUMBER = mUIModel.JobNumber;
                        e.QUOTATIONID = mUIModel.QuotationId;

                        if (mPaySource.ToLowerInvariant() == "web")
                        {
                            return Json(new
                            {
                                redirectUrl = Url.Action("PaymentDone", "Payment", e)
                            });
                        }
                        else
                        {
                            return Json(new
                            {
                                redirectUrl = Url.Action("MPaymentDone", "Payment", e)
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ShipaExceptions(ex.ToString());
                }
            }
        }

        // Stripe payment
        [HttpPost]
        public void SavePayment(string refNumber, string receiptHeaderId, string jobNumber, string quotationId, string cardType, string cardNumber, string resultStatus, string CouponCode, string user, double PayAmount, double requestedAmount, string UserHostAddress, string reason = "")
        {

            UserDBFactory UserFactory = new UserDBFactory();
            var OneSignalService = new Push.OneSignal();
            string str = string.Empty;
            string AdditionalChargeStatus = string.Empty;
            int id = Convert.ToInt32(jobNumber);
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, user, CouponCode, requestedAmount);
            PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
            AdditionalChargeStatus = (mUIModel.ADDITIONALCHARGESTATUS != null && mUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now") ? mUIModel.ADDITIONALCHARGESTATUS : "";
            decimal AdditionalChargeAmount = (mUIModel.ADDITIONALCHARGEAMOUNT > 0) ? mUIModel.ADDITIONALCHARGEAMOUNT : 0;
            decimal AdditionalChargeAmountINUSD = (mUIModel.ADDITIONALCHARGEAMOUNTINUSD > 0) ? mUIModel.ADDITIONALCHARGEAMOUNTINUSD : 0;
            try
            {
                TransResult(refNumber, receiptHeaderId, jobNumber, quotationId, resultStatus, "Online", CouponCode, mUIModel.DiscountAmount, user, PayAmount, AdditionalChargeStatus, AdditionalChargeAmount, AdditionalChargeAmountINUSD, reason);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
            CheckIsMandatoryDocsUploaded(jobNumber, user, Convert.ToInt32(quotationId), mDBEntity.OPJOBNUMBER);
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(quotationId));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(quotationId));
                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
                string BookingId = mDBEntity.OPJOBNUMBER;
                int JobNumber = Convert.ToInt32(mDBEntity.JOBNUMBER);
                int QuotationId = Convert.ToInt32(mDBEntity.QUOTATIONID);
                string QuotationNumber = mDBEntity.QUOTATIONNUMBER;
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(user);
                byte[] bytes = null;

                try
                {
                    bytes = bytes != null ? bytes : null;
                    if (resultStatus.ToLower() == "approved")
                    {
                        try
                        {
                            UserDBFactory mUFactory = new UserDBFactory();

                            mUserModel = mUFactory.GetUserProfileDetails(user, 1);

                            if (mUIModel.CHARGESETID > 0)
                            {
                                //For Additional Charges, will not generate Booking Confirmation.
                                bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(jobNumber), "Advance Receipt", Convert.ToInt64(quotationId), mUIModel.OPjobnumber, user, Convert.ToInt32(mUIModel.CHARGESETID));
                            }
                            else
                            {
                                GenerateBookingConfirmationPDF(Convert.ToInt32(jobNumber), "Booking Confirmation", Convert.ToInt64(quotationId), mUIModel.OPjobnumber, user);
                                bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(jobNumber), "Advance Receipt", Convert.ToInt64(quotationId), mUIModel.OPjobnumber, user, 0);
                            }
                        }
                        catch (Exception ex1)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex1.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while genarating Documents " + ex1.ToString());
                        }
                        try
                        {
                            if (AdditionalChargeStatus == null || AdditionalChargeStatus == "")
                            {
                                try
                                {
                                    SendMailWithCard(user, refNumber, "", Double.Parse(mUIModel.ReceiptNetAmount.ToString()).ToString(), cardNumber, cardType, mUIModel.Currency, "Job Booked", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, "Payment", UE.FIRSTNAME, "Online", reason);
                                }
                                catch (Exception excustomer)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excustomer.Message.ToString(), DateTime.Now.ToString()));
                                    throw new ShipaExceptions("Exception while sending mail to user(without additionalChargeAmount) " + excustomer.ToString());
                                }
                            }
                            else
                            {
                                try
                                {
                                    SendMailWithCard(user, refNumber, "", Convert.ToString(AdditionalChargeAmount), cardNumber, cardType, mUIModel.Currency, "Job Booked", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, "Additional Charge Payment", UE.FIRSTNAME, "Online", reason);
                                }
                                catch (Exception excustomercharge)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excustomercharge.Message.ToString(), DateTime.Now.ToString()));
                                    throw new ShipaExceptions("Exception while sending mail to user(with additionalChargeAmount) " + excustomercharge.ToString());
                                }
                            }
                        }
                        catch (Exception ex2)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex2.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to user " + ex2.ToString());
                        }
                        try
                        {
                            string roles = string.Empty;
                            string amount = string.Empty;
                            string flowType = string.Empty;
                            // Exclude GSSC while additional charges applied.
                            //roles = (AdditionalChargeStatus == null || AdditionalChargeStatus == "") ? "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC" : "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE";
                            if (AdditionalChargeStatus == null || AdditionalChargeStatus == "")
                            {
                                roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC"; amount = Convert.ToString(mUIModel.ReceiptNetAmount);
                                flowType = "Payment";
                            }
                            else { roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE"; amount = Convert.ToString(AdditionalChargeAmount); flowType = "Additional Charge Payment"; }
                            try
                            {
                                SendMailtoGroup(BookingId, roles, "Paymentstatus", JobNumber, user, "Online", flowType, amount, cardNumber, cardType, mUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, UE.COUNTRYNAME, quotationentity, mDBEntity);
                                //string CEEmail = string.Empty;
                                //QuotationDBFactory mQuotationDBFactory = new QuotationDBFactory();
                                //CEEmail = string.IsNullOrEmpty(quotationentity.CE) ? UE.CRMEmail : quotationentity.CE;
                                //if (string.IsNullOrEmpty(CEEmail))
                                //{
                                //    string CE = mQuotationDBFactory.UpdateCEForQuotation(QuotationId);
                                //    CEEmail = CE;
                                //}
                                SendThankingMailtoCustomer("cet@shipafreight.com", user, mUIModel.OPjobnumber);
                            }
                            catch (Exception exgroup)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exgroup.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to GSSC or Operation/Finance " + exgroup.ToString());
                            }
                        }
                        catch (Exception ex3)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex3.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to GSSC or Operation/Finance " + ex3.ToString());
                        }
                        UserFactory.NewNotification(5113, "Payment Completed", BookingId, JobNumber, QuotationId, QuotationNumber, user);
                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = JobNumber, BOOKINGID = BookingId, QUOTATIONID = QuotationId, QUOTATIONNUMBER = QuotationNumber, USERID = user };
                        SafeRun<bool>(() => OneSignalService.SendPushMessageByTag("Email", user.ToLowerInvariant(), "Payment Completed", pushdata));
                    }
                    else
                    {
                        string amount = string.Empty;
                        string flowType = string.Empty;
                        if (AdditionalChargeStatus == null || AdditionalChargeStatus == "") { amount = Convert.ToString(mUIModel.ReceiptNetAmount); flowType = "Payment"; } else { amount = Convert.ToString(AdditionalChargeAmount); flowType = "Additional Charge Payment"; }
                        try
                        {
                            SendMailWithCard(user, refNumber, "Online", amount, cardNumber, cardType, mUIModel.Currency, "Payment Failed", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, flowType, UE.FIRSTNAME, "Online", reason);
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending Payment Failed mail Online " + ex.ToString());
                        }
                        str = UserFactory.NewNotification(5113, "Payment failed", BookingId, JobNumber, QuotationId, QuotationNumber, user);
                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment failed", JOBNUMBER = JobNumber, BOOKINGID = BookingId, QUOTATIONID = QuotationId, USERID = user };
                        SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", user.ToLowerInvariant(), "Payment failed", pushdata); });
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                    throw new ShipaExceptions("Exception while sending mails " + ex.ToString());
                }
            }
        }


        public async Task<int> SendPaymentDatatoSalesForce(string PayementType, string UserHostAddress, string PayementMode, PaymentModel PaymentModel, PaymentTransactionDetailsModel PaymentTransactionModel, QuotationPreviewModel QuotationPreviewModel, double PayAmount)
        {
            try
            {
                SalesForceEntity salesForceEntity = new SalesForceEntity();
                HeaderSection objHeaderDetails = new HeaderSection();
                BookingDetails objBookingDetails = new BookingDetails();
                Details objDetails = new Details();
                Body objBody = new Body();
                if (PayementMode == "Online")
                {
                    string transcationType = PayementMode + "-" + "Payment-RegisterUser";
                    string PartnerID = PayementMode + "-" + "Payment-RegisterUser";
                    objHeaderDetails = await Task.Run(() => SalesForceIntegration.HeaderDetailsMapping(transcationType, PartnerID)).ConfigureAwait(false);
                    objBookingDetails = await Task.Run(() => SalesForceIntegration.PayementTranscationOnlineMapping(PaymentModel, QuotationPreviewModel, PayementType, PayementMode, PayAmount, UserHostAddress)).ConfigureAwait(false);
                }
                if (PayementMode == "Offline")
                {
                    string transcationType = PayementMode + "-" + "Payment-RegisterUser";
                    string PartnerID = PayementMode + "-" + "Payment-RegisterUser";
                    objHeaderDetails = await Task.Run(() => SalesForceIntegration.HeaderDetailsMapping(transcationType, PartnerID)).ConfigureAwait(false);
                    objBookingDetails = await Task.Run(() => SalesForceIntegration.PayementTranscationOfflineMapping(PaymentTransactionModel, PaymentModel, PayementType, PayementMode, PayAmount, UserHostAddress)).ConfigureAwait(false);
                }
                salesForceEntity.HeaderSection = objHeaderDetails;
                objDetails.UserDetails = null;
                objDetails.QuotationDetails = null;
                objDetails.BookingDetails = objBookingDetails;
                objBody.Details = objDetails;
                salesForceEntity.Body = objBody;
                string targetUrl = ApiUrl + "api/salesforce/PushDatatoMessageQueue";
                await Task.Run(() => GetDataFromApi<SalesForceEntity>(targetUrl, "POST", salesForceEntity)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
        //private void CheckIsMandatoryDocsUploaded(string jobNumber, string user)
        //{
        //    try
        //    {
        //        JobBookingDbFactory JBD = new JobBookingDbFactory();
        //        int Count = JBD.CheckIsMandatoryDocsUploaded(jobNumber);
        //        if (Count > 0)
        //            SendMailForMandatoryDocs(user);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ShipaExceptions(ex.ToString());
        //    }

        //}

        //private void CheckIsMandatoryDocsUploaded(string jobNumber)
        //{
        //    try
        //    {
        //        JobBookingDbFactory JBD = new JobBookingDbFactory();
        //        int Count = JBD.CheckIsMandatoryDocsUploaded(jobNumber);
        //        if (Count > 0)
        //            SendMailForMandatoryDocs(HttpContext.User.Identity.Name.ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ShipaExceptions(ex.ToString());
        //    }

        //}

        private void CheckIsMandatoryDocsUploaded(string jobNumber, string user, long quotationid, string BookigID)
        {
            try
            {
                JobBookingDbFactory JBD = new JobBookingDbFactory();
                int Count = JBD.CheckIsMandatoryDocsUploaded(jobNumber);
                if (Count > 0)
                    SendMailForMandatoryDocs(user, jobNumber, quotationid, BookigID);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }
        [HttpPost]
        [Authorize]
        public JsonResult AddNewParty(string SSPClientID, string CompanyName, string HouseNO, string BulName, string PAddrLine1, string PAddrLine2, string PostalCode,
            string FirstName, string LastName, string Email, string MobNumber, string CountryID, string CountryName, string StateID, string StateName, string CityID, string CityName, string Title, string CountryCode, string StateCode, string CityCode, string NickName, string TaxID, string FullAddressChinese, string ISDCode)
        {
            UserDBFactory DBF = new UserDBFactory();
            var ds = DBF.AddeNewPartyAddress(SSPClientID, CompanyName, HouseNO, BulName, PAddrLine1, PAddrLine2, PostalCode,
            FirstName, LastName, Email, MobNumber, CountryName, StateName, CityName, HttpContext.User.Identity.Name.ToString(), CountryID, StateID, CityID, Title, CountryCode, StateCode, CityCode, NickName, TaxID, FullAddressChinese, ISDCode); //To-Do
            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        [EncryptedActionParameter]
        public async Task<ActionResult> Index(int id, int? JobNumber)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            UserDBFactory UserFactory = new UserDBFactory();
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            decimal CountryId = 0;
            var Status = 0;
            var ds = mFactory.CheckExistingJob(id, HttpContext.User.Identity.Name);
            if (ds.Tables[0].Rows.Count != 0)
            {
                Status = 1;
                JobNumber = Convert.ToInt32(ds.Tables[0].Rows[0]["JOBNUMBER"]);
            }
            if (Convert.ToInt64(JobNumber) == 0 && Status == 0)
            {
                Session["Jobnumber"] = 0;

                //Creating Jobnumber based on requested Quotation ID ----- Anil G
                SSPJobDetailsEntity jobEntity = mFactory.CreateNewJobBasedonQID(Convert.ToInt32(id), HttpContext.User.Identity.Name);

                Generatequotepdf(Convert.ToInt64(id), jobEntity.JOBNUMBER);

                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id), HttpContext.User.Identity.Name, jobEntity);
                if (uiCulture != "en")
                {
                    await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<JobBookingEntity>(uiCulture, mDBEntity)).ConfigureAwait(false);

                }
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(HttpContext.User.Identity.Name, "", 0);
                List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);
                mUIModel.PartiesModel = PartyModel;

                UserPartyMasterModel user = PartyModel.Where(x => x.UPERSONAL == 1).FirstOrDefault();
                if (user == null)
                {
                    mDBEntity.SHCOUNTRYID = "0";
                    mUIModel.SHCOUNTRYID = "0";
                    mUIModel.SHCLIENTID = "0";
                }
                else
                {
                    mDBEntity.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
                    mUIModel.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
                    mUIModel.SHCLIENTID = Convert.ToString(user.SSPCLIENTID);
                }
                CountryId = mUIModel.OCOUNTRYID;
                //----HsCode Instructions----
                //Need to uncomment below code with respect to HSCode implementation
                List<HsCodeInstructionModel> hscodeList = new List<HsCodeInstructionModel>();
                mUIModel.HSCodeInsmodel = hscodeList;
            }
            else
            {
                Session["Jobnumber"] = JobNumber;
                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetails(Convert.ToInt32(id), Convert.ToInt64(JobNumber));
                if (uiCulture != "en")
                {
                    await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<JobBookingEntity>(uiCulture, mDBEntity)).ConfigureAwait(false);
                }

                var jobdet = mDBEntity.JobItems;
                var partydet = mDBEntity.PartyItems;
                if (jobdet != null && jobdet.Count > 0)
                {
                    mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                    mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                    mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                    mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                    mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                    mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                    mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                    mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                    mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                    mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                    mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                    mDBEntity.JSTATEID = jobdet[0].STATEID;
                    mDBEntity.SPECIALINSTRUCTION = jobdet[0].SPECIALINSTRUCTIONS == null ? "" : jobdet[0].SPECIALINSTRUCTIONS;
                    mDBEntity.LIVEUPLOADS = jobdet[0].LIVEUPLOADS;
                    mDBEntity.LOADINGDOCKAVAILABLE = jobdet[0].LOADINGDOCKAVAILABLE;
                    mDBEntity.CARGOPALLETIZED = jobdet[0].CARGOPALLETIZED;
                    mDBEntity.ORIGINALDOCUMENTSREQUIRED = jobdet[0].ORIGINALDOCUMENTSREQUIRED;
                    mDBEntity.TEMPERATURECONTROLREQUIRED = jobdet[0].TEMPERATURECONTROLREQUIRED;
                    mDBEntity.COMMERCIALPICKUPLOCATION = jobdet[0].COMMERCIALPICKUPLOCATION;
                    if (partydet.Any())
                    {
                        foreach (var items in partydet)
                        {
                            if (items.PARTYTYPE == "91")
                            {
                                mDBEntity.SHCLIENTID = Convert.ToString(items.SSPCLIENTID);
                                mDBEntity.SHPARTYDETAILSID = items.PARTYDETAILSID;
                                mDBEntity.SHCOUNTRYID = Convert.ToString(items.COUNTRYID);
                            }
                            else
                            {
                                mDBEntity.CONCLIENTID = Convert.ToString(items.SSPCLIENTID);
                                mDBEntity.CONPARTYDETAILSID = items.PARTYDETAILSID;
                                mDBEntity.CONCOUNTRYID = Convert.ToString(items.COUNTRYID);
                            }
                        }
                    }
                }
                CountryId = mDBEntity.OCOUNTRYID;
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(HttpContext.User.Identity.Name, "", 0);
                List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);
                mUIModel.PartiesModel = PartyModel;
                if (partydet.Count == 0)
                {
                    UserPartyMasterModel user = PartyModel.Where(x => x.UPERSONAL == 1).FirstOrDefault();
                    if (user == null)
                    {
                        mDBEntity.SHCOUNTRYID = "0";
                        mUIModel.SHCOUNTRYID = "0";
                        mUIModel.SHCLIENTID = "0";
                    }
                    else
                    {
                        mDBEntity.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
                        mUIModel.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
                        mUIModel.SHCLIENTID = Convert.ToString(user.SSPCLIENTID);
                    }

                }
                else
                {
                    //if shipper or consignee deleted from parties.
                    int index = PartyModel.FindIndex(item => item.SSPCLIENTID == Convert.ToInt64(mDBEntity.SHCLIENTID));
                    if (index < 0)
                    {
                        foreach (var item in PartyModel.Where(x => x.UPERSONAL == 1))
                        {
                            mUIModel.UPERSONALID = Convert.ToString(item.SSPCLIENTID);
                            mUIModel.UPERSONALCOUNTRYID = Convert.ToString(item.COUNTRYID);
                        }
                    }
                    int index1 = PartyModel.FindIndex(item => item.SSPCLIENTID == Convert.ToInt64(mDBEntity.CONCLIENTID));
                    if (index1 < 0)
                    {
                        foreach (var item in PartyModel.Where(x => x.UPERSONAL == 1))
                        {
                            mUIModel.CONUPERSONALID = Convert.ToString(item.SSPCLIENTID);
                            mUIModel.CONUPERSONALCOUNTRYID = Convert.ToString(item.COUNTRYID);
                        }
                    }
                }

                //----HsCode Instructions----
                //Need to uncomment below code with respect to HSCode implementation
                List<HsCodeInstructionModel> hscodeList = new List<HsCodeInstructionModel>();
                foreach (var obj in mDBEntity.HSCodeInsItems)
                {
                    HsCodeInstructionModel hscodemod = new HsCodeInstructionModel();

                    hscodemod.HSCODE = obj.HSCODE;
                    hscodeList.Add(hscodemod);
                }
                mUIModel.HSCodeInsmodel = hscodeList;
            }


            ////----HsCode Duty----
            //List<HsCodeDutyModel> hscodeDutyList = new List<HsCodeDutyModel>();
            //foreach (var obj in mDBEntity.HSCodeDutyItems)
            //{
            //    HsCodeDutyModel hscodeDuty = new HsCodeDutyModel();

            //    hscodeDuty.HSCODE = obj.HSCODE;
            //    hscodeDuty.DUTYTYPE = obj.DUTYTYPE;
            //    hscodeDuty.DUTYRATEPERCENTAGE = obj.DUTYRATEPERCENTAGE;

            //    hscodeDutyList.Add(hscodeDuty);
            //}
            //mUIModel.HSCodeDutymodel = hscodeDutyList;

            DBFactory.Entities.CargoAvabilityEntity CargoDate = mFactory.GetPickUpTransitDates(Convert.ToInt64(CountryId));
            mUIModel.CargoAvabilityModel = Mapper.Map<CargoAvabilityEntity, CargoAvabilityModel>(CargoDate);
            MDMDataFactory mdmFactory = new MDMDataFactory();
            IList<IncoTermsEntity> incoterms = mdmFactory.GetIncoTerms(string.Empty, string.Empty);
            ViewBag.incoterms = incoterms;


            List<string> ContainerTypes = new List<string>();
            foreach (var item in mUIModel.ShipmentItems)
            {
                if (item.PackageTypeId == 1)
                {
                    ContainerTypes.Add(item.ContainerName.Trim());
                }
            }
            if (ContainerTypes.Count > 0)
            {
                ViewBag.ContainerType = ContainerTypes;
            }
            mUIModel.CREATEDBY = HttpContext.User.Identity.Name;
            if (mUIModel.JSTATEID == "Payment in Progress")
            {
                return RedirectToAction("BookingSummary", new { id = mUIModel.QUOTATIONID, JobNumber = mUIModel.JJOBNUMBER });
            }
            else if (mUIModel.JSTATEID == "Payment Completed")
            {
                return RedirectToAction("BookingSummary", new { id = mUIModel.QUOTATIONID, JobNumber = mUIModel.JJOBNUMBER });
            }
            else if (mUIModel.JSTATEID == "Completed")
            {
                return RedirectToAction("BookingSummary", new { id = mUIModel.QUOTATIONID, JobNumber = mUIModel.JJOBNUMBER });
            }
            else
            {
                if (JobNumber == null)
                {
                    //----------------------------------------------//
                    //List<SSPDocumentsEntity> Docsentity = mFactory.InsertCountryBasedDocuments(Convert.ToInt64(mUIModel.OCOUNTRYID), Convert.ToInt64(mUIModel.DCOUNTRYID), mUIModel.JJOBNUMBER.ToString(), mUIModel.QUOTATIONID.ToString(), HttpContext.User.Identity.Name.ToString());
                    //List<SSPDocumentsModel> Docs = Mapper.Map<List<SSPDocumentsEntity>, List<SSPDocumentsModel>>(Docsentity);
                    //mUIModel.DocumentDetails = Docs;
                    //----------------------------------------------//
                    mUIModel.COLLABRATEDCONSIGNEE = null;
                    mUIModel.COLLABRATEDSHIPPER = null;
                }
                else
                {
                    if (mUIModel.JobItems.Any())
                    {
                        mUIModel.COLLABRATEDCONSIGNEE = mUIModel.JobItems[0].COLLABRATEDCONSIGNEE;
                        mUIModel.COLLABRATEDSHIPPER = mUIModel.JobItems[0].COLLABRATEDSHIPPER;
                    }
                }
                TempData["JobBookingData"] = mUIModel;
                //Meta title and Meta description
                ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
                ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";

                return View("Quote2Job", mUIModel);
            }
        }

        public ActionResult _PartialParties(string Searchstring)
        {
            UserDBFactory UserFactory = new UserDBFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(HttpContext.User.Identity.Name, Searchstring, 0);
            List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);
            ViewBag.PartySearchString = Searchstring;
            mUIModel.PartiesModel = PartyModel;
            if (String.IsNullOrEmpty(Searchstring))
            {
                return PartialView("_PartialParty", mUIModel);
            }
            else { return PartialView("_PartialSearchParty", mUIModel); }
        }

        public ActionResult _SearchParties(string Searchstring, int CountryId)
        {
            UserDBFactory UserFactory = new UserDBFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(HttpContext.User.Identity.Name, Searchstring, CountryId);
            List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);

            if (PartyModel != null)
            {
                var Parties = (
                         from items in PartyModel
                         select new
                         {
                             ADDRESSLINE1 = (items.ADDRESSLINE1 != null ? items.ADDRESSLINE1 : ""),
                             ADDRESSLINE2 = (items.ADDRESSLINE2 != null ? items.ADDRESSLINE2 : ""),
                             BUILDINGNAME = (items.BUILDINGNAME != null ? items.BUILDINGNAME : ""),
                             CITYID = items.CITYID,
                             CITYNAME = (items.CITYNAME != null ? items.CITYNAME : ""),
                             Clientname = (items.Clientname != null ? items.Clientname : ""),
                             COUNTRYID = items.COUNTRYID,
                             COUNTRYNAME = (items.COUNTRYNAME != null ? items.COUNTRYNAME : ""),
                             EMAILID = (items.EMAILID != null ? items.EMAILID : ""),
                             FIRSTNAME = (items.FIRSTNAME != null ? items.FIRSTNAME : ""),
                             HOUSENO = (items.HOUSENO != null ? items.HOUSENO : ""),
                             LASTNAME = (items.LASTNAME != null ? items.LASTNAME : ""),
                             PARTYSTATUS = items.PARTYSTATUS,
                             PHONE = (items.PHONE != null ? items.PHONE : ""),
                             Pincode = (items.Pincode != null ? items.Pincode : ""),
                             SALUTATION = (items.SALUTATION != null ? items.SALUTATION : ""),
                             SSPCLIENTID = items.SSPCLIENTID,
                             STATEID = items.STATEID,
                             STATENAME = (items.STATENAME != null ? items.STATENAME : ""),
                             STATUS = (items.STATUS != null ? items.STATUS : ""),
                             UPERSONAL = items.UPERSONAL
                         }).Distinct().OrderBy(x => x.Clientname).ToList();
                return Json(Parties, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            ViewBag.PartySearchString = Searchstring;
        }

        public void Generatequotepdf(long id, long JJOBNUMBER)
        {
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mQuoFactory.GetById(id);
                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                // PreferredCurrencyId = mUIModels.PreferredCurrencyId;
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = DynamicClass.GeneratePDF(memoryStream, mUIModels);
                    //Insert same data into db 
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                    sspdoc.QUOTATIONID = Convert.ToInt64(id);
                    sspdoc.DOCUMNETTYPE = "Quotation";
                    sspdoc.FILENAME = "Quotation_" + mUIModels.QuotationNumber + ".pdf";
                    sspdoc.FILEEXTENSION = "application/pdf";
                    sspdoc.FILESIZE = bytes.Length;
                    sspdoc.FILECONTENT = bytes;
                    sspdoc.JOBNUMBER = JJOBNUMBER;
                    sspdoc.DocumentName = "Quotation";
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

                    Guid obj = Guid.NewGuid();
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                    mjobFactory.SaveDocuments(docentity, HttpContext.User.Identity.Name, "System Generated", obj.ToString(), EnvironmentName);
                }
            }
        }

        public class CustomReportCredentials : IReportServerCredentials
        {
            private string _UserName;
            private string _PassWord;
            private string _DomainName;

            public CustomReportCredentials(string UserName, string PassWord, string DomainName)
            {
                _UserName = UserName;
                _PassWord = PassWord;
                _DomainName = DomainName;
            }

            public System.Security.Principal.WindowsIdentity ImpersonationUser
            {
                get { return null; }
            }

            public ICredentials NetworkCredentials
            {
                get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
            }

            public bool GetFormsCredentials(out Cookie authCookie, out string user, out string password, out string authority)
            {
                authCookie = null;
                user = password = authority = null;
                return false;
            }
        }
        #region Drodown filling
        public JsonResult FillPartyDetails(string Type)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                IList<PartyItems> partylist = mFactory.GetPartyData(HttpContext.User.Identity.Name, Session["Jobnumber"].ToString());

                if (partylist != null)
                {
                    var Countrylist = (
                             from items in partylist
                             select new
                             {
                                 Text = items.CLIENTNAME,
                                 Value = items.CLIENTID,
                                 PartyType = items.PARTYTYPE
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(Countrylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }
        #endregion

        public void SendMailtoGSSC(string Paymentmethod, string OPJobnumber, string PaymentType, int JobNumber, string userID, string FirstName = "")
        {
            string AttachmentIds = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet documents = mFactory.GetDocuments(JobNumber);
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");

            string recipientsList = "";
            recipientsList = mFactory.getMailsGSSCList(userID);

            if (recipientsList == "") { message.To.Add(new MailAddress("vsivaram@agility.com")); }
            else
            {
                string[] bccid = recipientsList.Split(',');
                foreach (string bccEmailId in bccid)
                {
                    message.To.Add(new MailAddress(bccEmailId)); //Adding Multiple BCC email Id
                }
            }
            message.To.Add(new MailAddress("help@shipafreight.com"));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Payment Process" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/GSSC-PaymentProcess.html");
            if (PaymentType == string.Empty)
                PaymentType = Paymentmethod;

            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{PaymentProcess}", Paymentmethod);
            body = body.Replace("{Reference_No}", OPJobnumber);
            body = body.Replace("{PaymentType}", PaymentType);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);


            if (documents != null)
            {
                foreach (DataRow dr in documents.Tables[0].Rows)
                {
                    string fileName = dr["FILENAME"].ToString();
                    byte[] fileContent = (byte[])dr["FILECONTENT"];
                    message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                    if (AttachmentIds == "")
                        AttachmentIds = dr["DOCID"].ToString();
                    else
                        AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                }
            }

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
              "JobBooking", "Payment Process SendtoGSSC", OPJobnumber, HttpContext.User.Identity.Name.ToString(), body, AttachmentIds, false, message.Subject.ToString());
        }
        public void SendMailtoOPsFinance(string Paymentmethod, string OPJobnumber, string PaymentType, int JobNumber)
        {
            string AttachmentIds = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet documents = mFactory.GetDocuments(JobNumber);
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            string recipientsList = "";
            recipientsList = mFactory.getMailsOPsFinanceList(OPJobnumber);

            if (recipientsList == "") { message.To.Add(new MailAddress("vsivaram@agility.com")); }
            else
            {
                string[] bccid = recipientsList.Split(',');
                foreach (string bccEmailId in bccid)
                {
                    message.To.Add(new MailAddress(bccEmailId)); //Adding Multiple BCC email Id
                }
            }
            message.To.Add(new MailAddress("help@shipafreight.com"));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Payment Process" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/GSSC-PaymentProcess.html");
            if (PaymentType == string.Empty)
                PaymentType = Paymentmethod;

            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{PaymentProcess}", Paymentmethod);
            body = body.Replace("{Reference_No}", OPJobnumber);
            body = body.Replace("{PaymentType}", PaymentType);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            if (documents != null)
            {
                foreach (DataRow dr in documents.Tables[0].Rows)
                {
                    string fileName = dr["FILENAME"].ToString();
                    byte[] fileContent = (byte[])dr["FILECONTENT"];
                    message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                    if (AttachmentIds == "")
                        AttachmentIds = dr["DOCID"].ToString();
                    else
                        AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();

                }
            }

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
            "JobBooking", "Payment Process Send to OPsFinance", OPJobnumber, HttpContext.User.Identity.Name.ToString(), body, AttachmentIds, false, message.Subject.ToString());
        }

        [AllowAnonymous]
        public void SendPayConfirmToCustomer(string UserEmailId, string Paymentmethod, string OPJobnumber, string PaymentType, int JobNumber, string ChargeSetId = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            int id = Convert.ToInt32(JobNumber);
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, UserEmailId);
            PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(miUIModel.QuotationId));
            DBFactory.Entities.QuotationEntity quotationentity = new QuotationEntity();
            if (count > 0)
            {
                quotationentity = mQuoFactory.GetById(Convert.ToInt32(miUIModel.QuotationId));
                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
            }
            UserDBFactory UD = new UserDBFactory();
            UserEntity UE = UD.GetUserFirstAndLastName(UserEmailId);
            string amount = string.Empty;
            string flowType = string.Empty;
            try
            {
                if ((ChargeSetId == null || ChargeSetId == "")) { amount = Convert.ToString(miUIModel.ReceiptNetAmount); flowType = "Payment"; } else { amount = Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT); flowType = "Additional Charge Payment"; }
                try
                {
                    //Need to go for approve so exchanged Paymentmethod,PaymentType
                    SendMailWithCard(UserEmailId, "", Paymentmethod, amount, null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, flowType, "", PaymentType, "");
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                    throw new ShipaExceptions("Exception while sending mail to user-PayConfirm " + ex.ToString());
                }
                try
                {
                    //SendMailtoGSSC(Paymentmethod, OPJobnumber, PaymentType, JobNumber, UserEmailId);
                    string roles = string.Empty;
                    //Payment Approved - Offline  - Internal
                    if (ChargeSetId == null || ChargeSetId == "") { roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC"; amount = Convert.ToString(miUIModel.ReceiptNetAmount); flowType = "Payment"; } else { roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE"; amount = Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT); flowType = "Additional Charge Payment"; }
                    try
                    {
                        SendMailtoGroup(OPJobnumber, roles, "Paymentstatus", JobNumber, UserEmailId, PaymentType, flowType, amount, "", "", miUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, quotationentity, mDBEntity);
                    }
                    catch (Exception exgroup)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exgroup.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending mail to OPS,SSPGSSC,FINANCE-PayConfirm " + exgroup.ToString());
                    }
                }
                catch { }
            }

            catch (Exception ex)
            {
                try
                {
                    if ((ChargeSetId == null || ChargeSetId == "")) { flowType = "Payment"; } else { flowType = "Additional Charge Payment"; }
                    SendMailWithCard(UserEmailId, "", "", amount, null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, flowType, "", PaymentType);
                }
                catch (Exception exfail)
                {
                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                    throw new ShipaExceptions("Exception while sending Payment Failed mail-PayConfirm " + exfail.ToString());
                }
            }
        }

        [AllowAnonymous]
        public void SendPartyMail(string EmailId, string PartyName, string UserName, string ActionType)
        {
            string AttachmentIds = string.Empty;

            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(EmailId));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight- Party" + EnvironmentName;
            string mapPath = Server.MapPath("~/App_Data/Party-Focis.html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            string stripeMessage = "The Requested Party Address of (" + PartyName + ") has been " + ActionType + ".";
            body = body.Replace("{User}", UserName);
            body = body.Replace("{Meeasge}", stripeMessage);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

            body = body.Replace("{Message}", stripeMessage);


            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", EmailId, "", "",
            "UserManagement", PartyName, EmailId, EmailId, body, AttachmentIds, true, message.Subject.ToString());
        }
        [Authorize]
        // [EncryptedActionParameter]
        public ActionResult Payment(int id, string sourceType = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            UserDBFactory uFactory = new UserDBFactory();
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
            PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
            if (mUIModel.BranchList.Any())
            {
                var x = mUIModel.BranchList.FirstOrDefault().BranchKey;
                ViewBag.BranchKeyNo = x;
            }
            StripeData stripeData = GetStripeData(mUIModel.Currency);
            mUIModel.stripePKKey = stripeData.publicKey;
            mUIModel.UserId = HttpContext.User.Identity.Name;
            var u = uFactory.GetUserDetails(HttpContext.User.Identity.Name);
            mUIModel.USERCOUNTRY = u.COUNTRYCODE;
            return View("Charge", mUIModel);
        }

        [Authorize]
        public JsonResult InitiateAlipay(int id, string discountCode, string user = "")
        {
            double Amount;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            var d = mFactory.AlipayInit(id, discountCode, EnvironmentName);

            var d2 = d.Rows[0]["AMOUNTPAY"].ToString();
            Amount = Convert.ToDouble(d2) * 100;
            if (d2 != "0")
            {
                string key = Session["strStripeSkKey"].ToString();
                StripeConfiguration.SetApiKey(key);
                string APPURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"].ToString();
                var userid = user == "" ? HttpContext.User.Identity.Name : user;
                var dic = new Dictionary<string, string>();
                dic.Add("id", id.ToString());
                dic.Add("email", userid);
                dic.Add("discountcode", discountCode);
                var sourceOptions = new StripeSourceCreateOptions()
                {
                    Type = "alipay",
                    Currency = "sgd",
                    Amount = Convert.ToInt32(Amount),
                    Metadata = dic,
                    RedirectReturnUrl = APPURL + "JobBooking/AliPayWebHook"
                };
                try
                {
                    var sourceService = new StripeSourceService();
                    StripeSource source = sourceService.Create(sourceOptions);
                    // to-do link source and job for more security
                    var data = new
                    {
                        Data = source.StripeResponse.ResponseJson
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    var data = new
                    {
                        Error = e.Message.ToString()
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var data = new
                {
                    Error = "Payment has been received already for this booking."
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult AliPayWebHook(string client_secret, string livemode, string source)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string key = Session["strStripeSkKey"].ToString();
            StripeConfiguration.SetApiKey(key);
            var sourceService = new StripeSourceService();
            StripeSource ssource = sourceService.Get(source);
            decimal amount = Convert.ToDecimal(ssource.Amount) / 100;

            string id = ssource.Metadata["id"];
            string coupon = ssource.Metadata.ContainsKey("discountcode") ? ssource.Metadata["discountcode"] : "";
            string user = ssource.Metadata.ContainsKey("email") ? ssource.Metadata["email"] : "Guest";

            if (ssource.Status.ToLowerInvariant() == "chargeable")
            {
                var myCharge = new StripeChargeCreateOptions();
                myCharge.Amount = ssource.Amount;
                myCharge.Currency = ssource.Currency;
                myCharge.Description = "AliPay";
                myCharge.SourceTokenOrExistingSourceId = source;
                var chargeService = new StripeChargeService(key);
                chargeService.ExpandBalanceTransaction = true;
                try
                {
                    StripeCharge stripeCharge = chargeService.Create(myCharge);

                    ThreadPool.QueueUserWorkItem(o =>
                    {
                        try
                        {
                            SafeRun<bool>(() =>
                            {
                                var resp = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                mFactory.SaveStripeTransactionDetails(
                                   id,
                                   stripeCharge.Status,
                                   amount.ToString(),
                                   ssource.Currency.ToUpperInvariant(),
                                   "AliPay",
                                   source,
                                   stripeCharge.Id,
                                   stripeCharge.BalanceTransactionId,
                                   stripeCharge.BalanceTransaction.Currency.ToUpperInvariant(),
                                   (Convert.ToDouble(stripeCharge.BalanceTransaction.Amount) / 100).ToString(),
                                   (Convert.ToDouble(stripeCharge.BalanceTransaction.Net) / 100).ToString(),
                                   (Convert.ToDouble(stripeCharge.BalanceTransaction.Fee) / 100).ToString(),
                                   stripeCharge.BalanceTransaction.Status,
                                   resp.balance_transaction.exchange_rate == null ? "1" : resp.balance_transaction.exchange_rate,
                                   "AliPay"
                               );
                                return true;
                            });
                            string sta = string.Empty;
                            if (stripeCharge.Status == "succeeded")
                            {
                                sta = "approved";
                            }
                            else
                            {
                                sta = "failed";
                            }
                            // AliPaySaveCharge(source, id, sta, coupon, user, stripeCharge.Currency.ToUpperInvariant(), stripeCharge.Status);
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorFormat(String.Format("stripeCharge Source: {0},  {1}, {2}", id, source, DateTime.Now.ToString()));
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                            Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                        }
                    });

                    ViewBag.status = stripeCharge.Status;
                    ViewBag.id = id;
                    ViewBag.transid = source;
                    return View("AliPayTransaction");
                }
                catch (Exception ex)
                {
                    SafeRun<bool>(() =>
                    {
                        mFactory.SaveStripeTransactionDetails(
                           id,
                           "failed",
                           amount.ToString(),
                           ssource.Currency.ToUpperInvariant(),
                           "AliPay",
                           source,
                           "",
                           ex.Message.ToString(),
                           "",
                           "",
                           "",
                           "",
                           "",
                           "",
                           "Alipay"
                       );
                        return true;
                    });

                    ViewBag.status = "failed";
                    ViewBag.id = id;
                    ViewBag.transid = source;
                    return View("AliPayTransaction");
                }
            }
            else
            {
                ViewBag.status = ssource.Status.ToLowerInvariant();
                ViewBag.id = id;
                ViewBag.transid = source;
                return View("AliPayTransaction");
            }
        }


        //internal void AliPaySaveCharge(string refNumber, string jobNumber, string resultStatus, string CouponCode, string user, string paidcurrency,double PayAmount, string reason = "")
        //{
        //    UserDBFactory UserFactory = new UserDBFactory();
        //    var OneSignalService = new Push.OneSignal();
        //    string str = string.Empty;
        //    string AdditionalChargeStatus = string.Empty;
        //    int id = Convert.ToInt32(jobNumber);
        //    JobBookingDbFactory mFactory = new JobBookingDbFactory();
        //    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, user, CouponCode);
        //    PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
        //    AdditionalChargeStatus = (mUIModel.ADDITIONALCHARGESTATUS != null && mUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now") ? mUIModel.ADDITIONALCHARGESTATUS : "";
        //    decimal AdditionalChargeAmount = (mUIModel.ADDITIONALCHARGEAMOUNT > 0) ? mUIModel.ADDITIONALCHARGEAMOUNT : 0;
        //    decimal AdditionalChargeAmountINUSD = (mUIModel.ADDITIONALCHARGEAMOUNTINUSD > 0) ? mUIModel.ADDITIONALCHARGEAMOUNTINUSD : 0;
        //    try
        //    {
        //        TransResult(refNumber, mUIModel.RECEIPTHDRID.ToString(), jobNumber, mUIModel.QuotationId.ToString(), resultStatus, "Online", CouponCode, mUIModel.DiscountAmount, user, PayAmount, AdditionalChargeStatus, AdditionalChargeAmount, AdditionalChargeAmountINUSD, reason, "Wallet", "AliPay", paidcurrency);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ShipaExceptions(ex.ToString());
        //    }
        //    CheckIsMandatoryDocsUploaded(jobNumber, user, Convert.ToInt32(mUIModel.QuotationId), mDBEntity.OPJOBNUMBER);
        //    QuotationDBFactory mQuoFactory = new QuotationDBFactory();
        //    DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(mUIModel.QuotationId));
        //    mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
        //    string BookingId = mDBEntity.OPJOBNUMBER;
        //    int JobNumber = Convert.ToInt32(mDBEntity.JOBNUMBER);
        //    int QuotationId = Convert.ToInt32(mDBEntity.QUOTATIONID);
        //    string QuotationNumber = mDBEntity.QUOTATIONNUMBER;
        //    UserDBFactory UD = new UserDBFactory();
        //    UserEntity UE = UD.GetUserFirstAndLastName(user);
        //    byte[] bytes = null;

        //    try
        //    {
        //        bytes = bytes != null ? bytes : null;
        //        if (resultStatus.ToLower() == "approved")
        //        {
        //            try
        //            {
        //                UserDBFactory mUFactory = new UserDBFactory();

        //                mUserModel = mUFactory.GetUserProfileDetails(user, 1);

        //                if (mUIModel.CHARGESETID > 0)
        //                {
        //                    //For Additional Charges, will not generate Booking Confirmation.
        //                    bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(jobNumber), "Advance Receipt", Convert.ToInt64(mUIModel.QuotationId), mUIModel.OPjobnumber, user, Convert.ToInt32(mUIModel.CHARGESETID));
        //                }
        //                else
        //                {
        //                    GenerateBookingConfirmationPDF(Convert.ToInt32(jobNumber), "Booking Confirmation", Convert.ToInt64(mUIModel.QuotationId), mUIModel.OPjobnumber, user);
        //                    bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(jobNumber), "Advance Receipt", Convert.ToInt64(mUIModel.QuotationId), mUIModel.OPjobnumber, user, 0);
        //                }
        //            }
        //            catch (Exception ex1)
        //            {
        //                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex1.Message.ToString(), DateTime.Now.ToString()));
        //                throw new ShipaExceptions("Exception while genarating Documents " + ex1.ToString());
        //            }
        //            try
        //            {
        //                if (AdditionalChargeStatus == null || AdditionalChargeStatus == "")
        //                {
        //                    try
        //                    {
        //                        SendMailWithCard(user, refNumber, "", Double.Parse(mUIModel.ReceiptNetAmount.ToString()).ToString(), "", "AliPay", mUIModel.Currency, "Job Booked", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, "Payment", UE.FIRSTNAME, "Online", reason);
        //                    }
        //                    catch (Exception excustomer)
        //                    {
        //                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excustomer.Message.ToString(), DateTime.Now.ToString()));
        //                        throw new ShipaExceptions("Exception while sending mail to user(without additionalChargeAmount) " + excustomer.ToString());
        //                    }
        //                }
        //                else
        //                {
        //                    try
        //                    {
        //                        SendMailWithCard(user, refNumber, "", AdditionalChargeAmount.ToString("N2"), "", "AliPay", mUIModel.Currency, "Job Booked", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, "Additional Charge Payment", UE.FIRSTNAME, "Online", reason);
        //                    }
        //                    catch (Exception excustomercharge)
        //                    {
        //                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excustomercharge.Message.ToString(), DateTime.Now.ToString()));
        //                        throw new ShipaExceptions("Exception while sending mail to user(with additionalChargeAmount) " + excustomercharge.ToString());
        //                    }
        //                }
        //            }
        //            catch (Exception ex2)
        //            {
        //                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex2.Message.ToString(), DateTime.Now.ToString()));
        //                throw new ShipaExceptions("Exception while sending mail to user " + ex2.ToString());
        //            }
        //            try
        //            {
        //                string roles = string.Empty;
        //                string amount = string.Empty;
        //                string flowType = string.Empty;
        //                // Exclude GSSC while additional charges applied.
        //                if (AdditionalChargeStatus == null || AdditionalChargeStatus == "")
        //                {
        //                    roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC"; amount = Convert.ToString(mUIModel.ReceiptNetAmount);
        //                    flowType = "Payment";
        //                }
        //                else { roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE"; amount = AdditionalChargeAmount.ToString("N2"); flowType = "Additional Charge Payment"; }
        //                try
        //                {
        //                    SendMailtoGroup(BookingId, roles, "Paymentstatus", JobNumber, user, "Online", flowType, amount, "", "AliPay", mUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName);
        //                }
        //                catch (Exception exgroup)
        //                {
        //                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exgroup.Message.ToString(), DateTime.Now.ToString()));
        //                    throw new ShipaExceptions("Exception while sending mail to GSSC or Operation/Finance " + exgroup.ToString());
        //                }

        //            }
        //            catch (Exception ex3)
        //            {
        //                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex3.Message.ToString(), DateTime.Now.ToString()));
        //                throw new ShipaExceptions("Exception while sending mail to GSSC or Operation/Finance " + ex3.ToString());
        //            }
        //            UserFactory.NewNotification(5113, "Payment Completed", BookingId, JobNumber, QuotationId, QuotationNumber, user);
        //            var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = JobNumber, BOOKINGID = BookingId, QUOTATIONID = QuotationId, QUOTATIONNUMBER = QuotationNumber, USERID = user };
        //            SafeRun<bool>(() => OneSignalService.SendPushMessageByTag("Email", user.ToLowerInvariant(), "Payment Completed", pushdata));
        //        }
        //        else
        //        {
        //            string amount = string.Empty;
        //            string flowType = string.Empty;
        //            if (AdditionalChargeStatus == null || AdditionalChargeStatus == "") { amount = Convert.ToString(mUIModel.ReceiptNetAmount); flowType = "Payment"; } else { amount = AdditionalChargeAmount.ToString("N2"); flowType = "Additional Charge Payment"; }
        //            try
        //            {
        //                SendMailWithCard(user, refNumber, "Online", amount, "", "AliPay", mUIModel.Currency, "Payment Failed", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName, flowType, UE.FIRSTNAME, "Online", reason);
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
        //                throw new ShipaExceptions("Exception while sending Payment Failed mail Online " + ex.ToString());
        //            }
        //            str = UserFactory.NewNotification(5113, "Payment failed", BookingId, JobNumber, QuotationId, QuotationNumber, user);
        //            var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment failed", JOBNUMBER = JobNumber, BOOKINGID = BookingId, QUOTATIONID = QuotationId, USERID = user };
        //            SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", user.ToLowerInvariant(), "Payment failed", pushdata); });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
        //        throw new ShipaExceptions("Exception while sending mails " + ex.ToString());
        //    }
        //}


        [Authorize]
        public JsonResult GetCurrenciesForPayment()
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var data = mFactory.GetCurrencies(HttpContext.User.Identity.Name.ToString());
            var JsonToReturn = new
            {
                Data = data
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MGetCurrenciesForPayment(string userid)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var data = mFactory.GetCurrencies(userid);
            var JsonToReturn = new
            {
                Data = data
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize]
        //public ActionResult PaymentWithCard(PaymentModel model)
        //{
        //    UserDBFactory UserFactory = new UserDBFactory();
        //    string str = string.Empty;
        //    string amount = "";
        //    if (Request.Form["txtReceiptNetAmount"] != "")
        //    {
        //        amount = Request.Form["txtReceiptNetAmount"];
        //    }
        //    string receiptHeaderId = Convert.ToString(model.RECEIPTHDRID);
        //    string jobNumber = Convert.ToString(model.JobNumber);
        //    string quotationId = Convert.ToString(model.QuotationId);
        //    string paymentOption = "Online";
        //    string invoice_number = GenerateToken();
        //    CreditCard crdtCard = new CreditCard();
        //    crdtCard.cvv2 = model.CVV;
        //    crdtCard.expire_month = Convert.ToInt32(model.ExpMonth);
        //    crdtCard.expire_year = Convert.ToInt32(model.ExpYear);
        //    crdtCard.first_name = model.CardHolderFirstName;
        //    crdtCard.last_name = model.CardHolderLastName;
        //    crdtCard.number = model.CardNumber.Trim();
        //    crdtCard.type = model.CardType.ToLower();

        //    Amount amnt = new Amount();
        //    amnt.currency = model.Currency;
        //    amnt.total = amount.ToString() != null ? amount : ""; //Convert.ToString(model.Amount).Trim();

        //    Transaction tran = new Transaction();
        //    tran.amount = amnt;
        //    tran.invoice_number = invoice_number;

        //    List<Transaction> transactions = new List<Transaction>();
        //    transactions.Add(tran);

        //    FundingInstrument fundInstrument = new FundingInstrument();
        //    fundInstrument.credit_card = crdtCard;

        //    List<FundingInstrument> fundingInstrumentList = new List<FundingInstrument>();
        //    fundingInstrumentList.Add(fundInstrument);

        //    Payer payr = new Payer();
        //    payr.funding_instruments = fundingInstrumentList;
        //    payr.payment_method = "credit_card";

        //    PayPal.Api.Payment pymnt = new PayPal.Api.Payment();
        //    pymnt.intent = "sale";
        //    pymnt.payer = payr;
        //    pymnt.transactions = transactions;

        //    int id = Convert.ToInt32(model.JobNumber);
        //    JobBookingDbFactory mFactory = new JobBookingDbFactory();
        //    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
        //    PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
        //    //added by siva for notifications
        //    string BookingId = mDBEntity.OPJOBNUMBER;
        //    int JobNumber = Convert.ToInt32(mDBEntity.JOBNUMBER);
        //    int QuotationId = Convert.ToInt32(mDBEntity.QUOTATIONID);
        //    string QuotationNumber = mDBEntity.QUOTATIONNUMBER;

        //    try
        //    {
        //        APIContext apiContext = Configuration.GetAPIContext();

        //        PayPal.Api.Payment createdPayment = pymnt.Create(apiContext);
        //        string refNumber = createdPayment.id;
        //        string state = createdPayment.state;

        //        if (createdPayment.state.ToLower() != "approved")
        //        {
        //            ViewBag.status = "Fail";
        //            bool transResult = TransResult("Id", receiptHeaderId, jobNumber, quotationId, "failed", paymentOption);
        //            str = UserFactory.NewNotification(5113, "Payment failed", BookingId, JobNumber, QuotationId, QuotationNumber, HttpContext.User.Identity.Name);
        //            //-----Anil G------------//
        //            SendMailWithCard(HttpContext.User.Identity.Name, refNumber, amnt.total, model.CardNumber, model.CardType, null, model.Currency, "fail", mUIModel.OPjobnumber);

        //            return View("Charge", mUIModel);
        //        }
        //        else
        //        {
        //            ViewBag.status = "Pass";
        //            ViewBag.token = refNumber;
        //            bool transResult = TransResult(refNumber, receiptHeaderId, jobNumber, quotationId, state, paymentOption);
        //            str = UserFactory.NewNotification(5113, "Payment Completed", BookingId, JobNumber, QuotationId, QuotationNumber, HttpContext.User.Identity.Name);


        //            GenerateBookingConfirmationPDF(Convert.ToInt32(jobNumber), "Booking Confirmation", Convert.ToInt64(quotationId), mUIModel.OPjobnumber, HttpContext.User.Identity.Name.ToString());
        //            byte[] bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(jobNumber), "Advance Receipt", Convert.ToInt64(quotationId), mUIModel.OPjobnumber, HttpContext.User.Identity.Name.ToString());

        //            //GenerateJobConfPDF(mUIModel.QuotationNumber, Convert.ToInt64(jobNumber), Convert.ToInt64(quotationId), mUIModel.OPjobnumber, "Booking Confirmation");
        //            //byte[] bytes = GenerateJobConfPDF(mUIModel.QuotationNumber, Convert.ToInt64(jobNumber), Convert.ToInt64(quotationId), mUIModel.OPjobnumber, "Advance Receipt");

        //            //-----Anil G------------//
        //            SendMailWithCard(HttpContext.User.Identity.Name, refNumber, amnt.total, model.CardNumber, model.CardType, bytes, model.Currency, "Success", mUIModel.OPjobnumber);

        //            return View("Charge", mUIModel);
        //        }
        //    }
        //    catch (PayPal.PayPalException ex)
        //    {
        //        //string refNumber =((PayPal.PaymentsException)ex).Details.debug_id;               
        //        //string errorMessage =(((PayPal.ConnectionException)ex).Response);
        //        //Logger.Log("Error: " + ex.Message);
        //        ViewBag.status = "Fail";
        //        bool transResult = TransResult("Id", receiptHeaderId, jobNumber, quotationId, "Failed - " + ex.Message, paymentOption);
        //        str = UserFactory.NewNotification(5113, "Payment failed", BookingId, JobNumber, QuotationId, QuotationNumber, HttpContext.User.Identity.Name);
        //        return View("Charge", mUIModel);
        //    }
        //}

        private void SendMailWithCard(string Email, string refNumber, string paymentType, string Total, string cardno, string cardtype, string currency, string status, string BookingID, long Jobnumber, long QuotationID, QuotationPreviewModel mUIModels, string mode, string movementType, string shipperName, string consigneeName, string flowType, string FirstName = "", string paymentMethod = "", string reason = "")
        {
            string AttachmentIds = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            QuotationDBFactory mFactoryRound = new QuotationDBFactory();
            DataSet documents = mFactory.GetDocuments(Convert.ToInt32(Jobnumber));

            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = DynamicClass.GetEnvironmentName();

            if (paymentMethod.ToUpperInvariant() == "ONLINE" || paymentMethod.ToUpperInvariant() == "OFFLINE" || paymentType.ToUpperInvariant() == "OFFLINE")
            {
                message.Subject = "Shipa Freight- " + flowType + "( Booking ID #" + BookingID + " )" + EnvironmentName;
            }
            else if (paymentMethod.ToUpperInvariant() == "BUSINESS CREDIT")
            {
                message.Subject = "Shipa Freight - " + flowType + " through Business credit" + "( Booking ID #" + BookingID + " )" + EnvironmentName;
            }
            else
            {
                message.Subject = "Shipa Freight - " + flowType + " in progress." + "( Booking ID #" + BookingID + " )" + EnvironmentName;
            }

            string mapPath = string.Empty;
            if (status.ToUpperInvariant() == "JOB BOOKED")
            {
                if (paymentType.ToUpperInvariant() == "WIRE TRANSFER" && paymentMethod.ToUpperInvariant() == "OFFLINE")
                {
                    mapPath = Server.MapPath("~/App_Data/payment-wiretransfer.html");
                }
                else
                {
                    mapPath = Server.MapPath("~/App_Data/payment-success.html");

                }
            }
            else
            {
                mapPath = Server.MapPath("~/App_Data/Payment-fail.html");
            }
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }

            if (FirstName == "")
            {
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(Email);
                body = body.Replace("{user_name}", UE.FIRSTNAME);
            }
            else
            {
                body = body.Replace("{user_name}", FirstName);
            }
            body = body.Replace("{currency}", currency);
            body = body.Replace("{Amount}", mFactoryRound.RoundedValue(Total, currency));
            if (paymentMethod.ToUpperInvariant() == "ONLINE")
            {
                if (cardtype.ToLowerInvariant() != "alipay")
                {
                    body = body.Replace("{PaymentMethod}", cardtype + "<br><span style=text-transform: lowercase>xxxxxxxxxxxx" + cardno + "<span>");
                }
                else
                {
                    body = body.Replace("{PaymentMethod}", cardtype);
                }
            }
            else if (paymentMethod.ToUpperInvariant() == "OFFLINE")
            {
                body = body.Replace("{PaymentMethod}", paymentType);
            }
            else
            {
                body = body.Replace("{PaymentMethod}", paymentMethod);
            }
            body = body.Replace("{Amount}", mFactoryRound.RoundedValue(Total, currency));
            body = body.Replace("{Status}", status);
            body = body.Replace("{BookingID}", BookingID);
            body = body.Replace("{Mode}", mode);
            body = body.Replace("{MovementType}", movementType);
            body = body.Replace("{ShipperOrConsignee}", shipperName + " / " + consigneeName);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            string Tbody = "";
            if (movementType.ToUpper() == "DOOR TO PORT" || movementType.ToUpper() == "PORT TO PORT")
            {
                Tbody = PrepareShipmentTrack("P");
                body = body.Replace("{shpnttracking}", Tbody);
            }
            else
            {
                Tbody = PrepareShipmentTrack("D");
                body = body.Replace("{shpnttracking}", Tbody);
            }


            if (status.ToUpperInvariant() != "JOB BOOKED" && paymentMethod.ToUpperInvariant() == "ONLINE")
            {
                body = body.Replace("{Reason}", "(" + reason + ")");
            }
            else
            {
                body = body.Replace("{Reason}", "");
            }

            body = body.Replace("{Trackingurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "/Tracking?SearchString=" + BookingID + "&status=ACTIVE");

            if (documents != null)
            {
                foreach (DataRow dr in documents.Tables[0].Rows)
                {
                    string fileName = dr["FILENAME"].ToString();
                    byte[] fileContent = (byte[])dr["FILECONTENT"];
                    message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                    if (AttachmentIds == "")
                        AttachmentIds = dr["DOCID"].ToString();
                    else
                        AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                }
            }
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
            "JobBooking", "Credit/Debit/Payment Confirmation", BookingID, Email, body, AttachmentIds, true, message.Subject.ToString());
        }

        private string PrepareShipmentTrack(string p)
        {
            string Tbody = "";
            if (p == "D")
            {
                Tbody = @"<table width='600px'><tbody><tr><td style='color: #333;font-size: 18px; font-weight: normal; padding-bottom: 5px'>Shipment Tracking</td></tr>
                                <tr><td style='color: #333;background-color: #FFFFFF; font-size: 16px; padding: 10px 10px 10px 10px; border: 1px solid #DDDDDD;'><table style='width: 100%; text-align: center; color: #333333; font-size: 12px;'>
                    <tbody>
                    <tr>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/job-b.png'></td>
                        <td width='16%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/receipt-b.png'></td>
                        <td width='16%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/departed-b.png'></td>
                        <td width='15%'><img style='width: 100%; height: 4px;'  src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/arrival-b.png'></td>
                        <td width='15%'><img style='width: 100%; height: 4px;'  src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/delivered-b.png'></td>
                    </tr>
                    <tr>
                        <td style='vertical-align: top;'>
                            Job Confirmed
                        </td>
                        <td width='16%'></td>
                        <td style='vertical-align: top;'>
                            Receipt of Goods
                        </td>
                        <td width='17%'></td>
                        <td style='vertical-align: top;'>
                            Departed
                        </td>
                        <td width='16%'></td>
                        <td style='vertical-align: top;'>
                            Arrival
                        </td>
                        <td width='15%'></td>
                        <td style='vertical-align: top;'>
                            Delivered
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>";
            }
            else
            {
                Tbody = @"<table width='600' style='background-color: #f0f0f0; width: 600px; margin: auto; padding: 10px 20px;'>
        <tbody>
            <tr>
                <td style='color: #333;font-size: 18px; font-weight: normal; padding-bottom: 5px'>
                    Shipment Tracking
                </td>
            </tr>
            <tr>
                <td style='color: #333;background-color: #FFFFFF; font-size: 16px; padding: 10px 10px 10px 10px; border: 1px solid #DDDDDD;'>
                    <table style=' width: 100%; text-align: center; color: #333333; font-size: 12px;'>
                        <tbody>
                        <tr>
                            <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-1.png'></td>
                            <td><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>
                            
                            <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/receipt-b.png'></td>
                            <td><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>
                            <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/departed-b.png'></td>
                            <td><img style='width: 100%; height: 3px;'  src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>
                            <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/arrival-b.png'></td>
                        </tr>
                        <tr>
                            <td style='vertical-align: top;'>
                                Job Confirmed
                            </td>
                            <td></td>
                            <td style='vertical-align: top;'>
                                Receipt of Goods
                            </td>
                            <td></td>
                            <td style='vertical-align: top;'>
                                Departure
                            </td>
                            <td></td>
                            <td style='vertical-align: top;'>
                                Arrival
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>";
            }
            return Tbody;
        }

        public bool TransResult(string refNumber, string receiptHeaderId, string jobNumber, string quotationId, string state, string paymentOption, string CouponCode, decimal DiscountAmount, string user, double PayAmount, string AdditionalChargeStatus = "", decimal AdditionalChargeAmount = 0, decimal AdditionalChargeAmountINUSD = 0, string reason = "", string transmode = "", string paymenttype = "", string paidcurrency = "")
        {
            PaymentTransactionDetailsModel model = new PaymentTransactionDetailsModel();
            try
            {
                model.RECEIPTHDRID = Convert.ToInt32(receiptHeaderId);
                model.JOBNUMBER = Convert.ToInt32(jobNumber);
                model.QUOTATIONID = Convert.ToInt32(quotationId);
                model.PAYMENTOPTION = paymentOption;
                model.ISPAYMENTSUCESS = ((state == "approved") ? 1 : 0);
                model.PAYMENTREFNUMBER = refNumber;
                model.PAYMENTREFMESSAGE = state;
                model.CREATEDBY = user;
                model.MODIFIEDBY = user;
                model.TRANSMODE = transmode == "" ? "Card" : transmode;
                model.PAYMENTTYPE = paymenttype == "" ? "Credit/Debit" : paymenttype;
                model.ADDITIONALCHARGEAMOUNT = AdditionalChargeAmount;
                model.ADDITIONALCHARGEAMOUNTINUSD = AdditionalChargeAmountINUSD;
                model.ADDITIONALCHARGESTATUS = (AdditionalChargeStatus != null && AdditionalChargeStatus.ToLower() == "pay now") ? (model.ISPAYMENTSUCESS == 1) ? "Paid" : "Rejected" : "";


                DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(model);
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.SavePaymentTransactionDetails(mUIModel, CouponCode, DiscountAmount, PayAmount, reason, paidcurrency);
                return true;
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        /// <summary>
        /// Fill Shipment Track and Port pairs -- Anil G
        /// </summary>
        /// <param name="QEntity"></param>
        private void PrepareTrackandPortPairs(QuotationPreviewModel QEntity)
        {
            Track = ""; Ports = "";


            if (QEntity.MovementTypeName.ToUpper() == "DOOR TO DOOR")
            {
                if (!string.IsNullOrWhiteSpace(QEntity.OriginPlaceName))
                {
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
                    Ports = "<tr><td>" + QEntity.OriginPlaceName + "</td>";
                }

                if (!string.IsNullOrWhiteSpace(QEntity.OriginPortName))
                {

                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/truck-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
                    Ports = Ports + "<td colspan='3'></td><td>" + QEntity.OriginPortCode + "<br/>" + QEntity.OriginPortName + "</td>";
                }

                if (!string.IsNullOrWhiteSpace(QEntity.DestinationPortName))
                {
                    if (QEntity.ProductName.ToUpper() == "AIR")
                        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td>";
                    else
                        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td>";

                    Track = Track + "<td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";

                    Ports = Ports + "<td colspan='3'></td><td>" + QEntity.DestinationPortCode + "<br/>" + QEntity.DestinationPortName + "</td>";
                }

                if (!string.IsNullOrWhiteSpace(QEntity.DestinationPlaceName))
                {
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/truck-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
                    Ports = Ports + "<td colspan='3'></td><td>" + QEntity.DestinationPlaceName + "</td></tr>";
                }
            }
            else if (QEntity.MovementTypeName.ToUpper() == "PORT TO PORT")
            {


                if (!string.IsNullOrWhiteSpace(QEntity.OriginPortName))
                {
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td>";

                    //if (QEntity.ProductName.ToUpper() == "AIR")
                    //    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td>";
                    //else
                    //Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td>";

                    Ports = "<tr><td>" + QEntity.OriginPortCode + "<br/>" + QEntity.OriginPortName + "</td>";

                }

                if (!string.IsNullOrWhiteSpace(QEntity.DestinationPortName))
                {
                    Track = Track + "<td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
                    Ports = Ports + "<td colspan='3'></td><td>" + QEntity.DestinationPortCode + "<br/>" + QEntity.DestinationPortName + "</td></tr>";
                }

                else if (!string.IsNullOrWhiteSpace(QEntity.DestinationPlaceName))
                {
                    Track = Track + "<td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
                    Ports = Ports + "<td colspan='3'></td><td >" + QEntity.DestinationPlaceName + "</td></tr>";
                }
            }

            else if (QEntity.MovementTypeName.ToUpper() == "DOOR TO PORT")
            {
                if (!string.IsNullOrWhiteSpace(QEntity.OriginPlaceName))
                {
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/truck-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
                    Ports = "<tr><td>" + QEntity.OriginPlaceName + "</td><td colspan='3'></td><td>" + QEntity.OriginPlaceCode + "<br/>" + QEntity.OriginPlaceName + "</td>";
                }


                if (!string.IsNullOrWhiteSpace(QEntity.DestinationPortName))
                {

                    if (QEntity.ProductName.ToUpper() == "AIR")
                        Track = Track + "<td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td>";
                    else
                        Track = Track + "<td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td>";

                    Track = Track + "<td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";

                    Ports = Ports + "<td colspan='3'></td><td>" + QEntity.DestinationPortName + "</td>";
                }
            }

            else if (QEntity.MovementTypeName.ToUpper() == "PORT TO DOOR")
            {
                if (!string.IsNullOrWhiteSpace(QEntity.OriginPortName))
                {
                    if (QEntity.ProductName.ToUpper() == "AIR")
                        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>";
                    else
                        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>";
                    Ports = "<tr><td>" + QEntity.OriginPortName + "</td><td colspan='3'></td>";
                }


                if (!string.IsNullOrWhiteSpace(QEntity.DestinationPlaceName))
                {
                    Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/truck-2.png'></td>";

                    Track = Track + "<td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";

                    Ports = Ports + "<td>" + QEntity.DestinationPlaceCode + "<br/>" + QEntity.DestinationPlaceName + "</td>";

                    Ports = Ports + "<td><td colspan='3'>" + QEntity.DestinationPlaceName + "</td>";
                }
            }


            //----------------------------------------
            //if (QEntity.ProductName.ToUpper() == "AIR" && QEntity.MovementTypeName.ToUpper() == "PORT TO PORT")
            //{
            //    if (!string.IsNullOrWhiteSpace(QEntity.OriginPlaceName))
            //    {
            //        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>";
            //        Ports = "<tr><td>" + QEntity.OriginPlaceName + "</td><td width='33%' colspan='3'></td>";
            //    }

            //    if (!string.IsNullOrWhiteSpace(QEntity.OriginPortName))
            //    {
            //        if (Track == "")
            //        {
            //            Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>";

            //            if (QEntity.ProductName.ToUpper() == "AIR")
            //                Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td>";
            //            else
            //                Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td>";

            //            Ports = "<tr><td>" + QEntity.OriginPortName + "</td><td colspan='3'></td>";
            //        }
            //        else
            //        {
            //            if (QEntity.ProductName.ToUpper() == "AIR")
            //                Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td>";
            //            else
            //                Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td>";

            //            Ports = "<tr><td>" + QEntity.OriginPortName + "</td><td colspan='3'></td>";
            //        }
            //    }

            //    if (!string.IsNullOrWhiteSpace(QEntity.DestinationPortName))
            //    {
            //        Track = Track + "<td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
            //        Ports = Ports + "<td >" + QEntity.DestinationPortName + "</td></tr>";
            //    }

            //    if (!string.IsNullOrWhiteSpace(QEntity.DestinationPlaceName))
            //    {
            //        Track = Track + "<td width='25%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td><td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
            //        Ports = Ports + "<td >" + QEntity.DestinationPlaceName + "</td></tr>";
            //    }
            //}
            //else
            //{
            //    //ocean block
            //    if (!string.IsNullOrWhiteSpace(QEntity.OriginPlaceName))
            //    {
            //        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
            //        Ports = "<tr><td>" + QEntity.OriginPlaceName + "</td>";
            //    }

            //    if (!string.IsNullOrWhiteSpace(QEntity.OriginPortName))
            //    {

            //        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/truck-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
            //        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
            //        Ports = Ports + "<td colspan='3'></td><td>" + QEntity.OriginPortCode + "<br/>" + QEntity.OriginPortName + "</td>";
            //    }

            //    if (!string.IsNullOrWhiteSpace(QEntity.DestinationPortName))
            //    {
            //        if (QEntity.ProductName.ToUpper() == "AIR")
            //            Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/flight-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
            //        else
            //            Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/ship-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
            //        Ports = Ports + "<td colspan='3'></td><td>" + QEntity.DestinationPortCode + "<br/>" + QEntity.DestinationPortName + "</td>";
            //    }

            //    if (!string.IsNullOrWhiteSpace(QEntity.DestinationPlaceName))
            //    {
            //        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/truck-2.png'></td><td width='40'><img src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>";
            //        Track = Track + "<td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-2.png'></td>";
            //        Ports = Ports + "<td colspan='3'></td><td>" + QEntity.DestinationPlaceName + "</td></tr>";
            //    }
            //}

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult OfflinePayment(PaymentModel model)
        {
            string FileName = string.Empty;
            MPaymentResult e = new MPaymentResult();
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(model.JobNumber));
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(model.QuotationId));
            if (PaymentStatus.ToLower() == "notexist")
            {
                decimal BranchKey = 0;
                string branchAddress = "";
                string DiscountCode = "";
                decimal DiscountAmount = 0;
                string PAIDCURRENCY = "";
                string branchName = "";
                if (Request.Form["txtbranchAddress"] != "")
                {
                    branchAddress = Request.Form["txtbranchAddress"];
                    Session["BranchAddress"] = branchAddress;
                }
                else
                {
                    Session["BranchAddress"] = null;
                }

                if (Request.Form["txtChequeDate"] != "")
                {
                    model.TransChequeDate = Convert.ToDateTime(Request.Form["txtChequeDate"]);
                }
                if (Request.Form["txtCashPayDate"] != "")
                {
                    model.TransChequeDate = Convert.ToDateTime(Request.Form["txtCashPayDate"]);
                }
                if (Request.Form["txtWTChequeDate"] != "")
                {
                    model.WTTransChequeDate = Convert.ToDateTime(Request.Form["txtWTChequeDate"]);
                }
                if (Request.Form["txtTransmode"] != "")
                {
                    model.TransactionMode = Convert.ToString(Request.Form["txtTransmode"]);
                }
                if (model.PaymentType.ToLower() == "wire transfer")
                {
                    if (model.EntityList != null)
                    {
                        if (Request.Form["EntityKey"] != "")
                        {
                            model.EntityList.EntityKey = Convert.ToInt64(Request.Form["EntityKey"]);
                        }
                    }
                }
                if (model.PaymentType.ToLower() == "pay at branch" && Request.Form["PBBranchKey"] != "")
                {
                    BranchKey = Convert.ToDecimal(Request.Form["PBBranchKey"]);
                }
                if (Request.Form["txtCouponCode"] != "")
                {
                    DiscountCode = Request.Form["txtCouponCode"];
                }
                else
                {
                    DiscountCode = "";
                }
                if (Request.Form["txtDiscountAmount"] != "")
                {
                    DiscountAmount = Convert.ToDecimal(Request.Form["txtDiscountAmount"]);
                }
                if (Request.Form["txtPaidCurrency"] != "")
                {
                    PAIDCURRENCY = Request.Form["txtPaidCurrency"];
                }
                if (Request.Form["txtbranchName"] != "")
                {
                    branchName = Request.Form["txtbranchName"] + ", ";
                }

                PaymentTransactionDetailsModel offlinemodel = new PaymentTransactionDetailsModel();
                try
                {
                    offlinemodel.RECEIPTHDRID = model.RECEIPTHDRID;
                    offlinemodel.JOBNUMBER = model.JobNumber;
                    offlinemodel.QUOTATIONID = model.QuotationId;
                    offlinemodel.PAYMENTOPTION = model.PaymentOption;
                    offlinemodel.ISPAYMENTSUCESS = 1;
                    offlinemodel.PAYMENTREFNUMBER = (model.PaymentType.ToLower() == "wire transfer") ? (model.WTTransChequeNo != null ? model.WTTransChequeNo : null) : (model.TransactionMode.ToLower() == "cash" ? model.TransactionMode : model.TransChequeNo);
                    offlinemodel.PAYMENTREFMESSAGE = "Pending";
                    offlinemodel.CREATEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                    offlinemodel.MODIFIEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                    offlinemodel.PAYMENTTYPE = model.PaymentType;
                    offlinemodel.TRANSMODE = model.TransactionMode != null ? model.TransactionMode : null;
                    offlinemodel.PAIDCURRENCY = PAIDCURRENCY;

                    if (model.PaymentType.ToLower() == "pay at branch")
                    {
                        if (model.TransactionMode.ToLower() == "cash")
                        {
                            offlinemodel.TRANSDESCRIPTION = model.TransDescription != null ? model.TransDescription : null;
                            offlinemodel.TRANSAMOUNT = Convert.ToDouble(model.TransAmount);
                            offlinemodel.TRANSCURRENCY = model.TransCurrency != null ? model.TransCurrency : null;
                            offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                        }
                        else
                        { // For Cheque and Demand draft
                            offlinemodel.TRANSBANKNAME = model.TransBankName != null ? model.TransBankName : null;
                            offlinemodel.TRANSBRANCHNAME = model.TransBranchName != null ? model.TransBranchName : null;
                            offlinemodel.TRANSCHEQUENO = model.TransChequeNo != null ? model.TransChequeNo : null;
                            offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                            offlinemodel.TRANSCURRENCY = model.TransCDCurrency != null ? model.TransCDCurrency : null;
                            offlinemodel.TRANSDESCRIPTION = model.TransCDDescription != null ? model.TransCDDescription : null;
                            if (model.TransCDAmount != null)
                            {
                                double TransCDAmount = Convert.ToDouble(model.TransCDAmount);
                                offlinemodel.TRANSAMOUNT = TransCDAmount;
                            }
                        }
                        offlinemodel.BRANCHENTITYKEY = BranchKey != null ? BranchKey : 0;
                    }
                    else if (model.PaymentType.ToLower() == "wire transfer")
                    {
                        offlinemodel.TRANSBANKNAME = model.WTTransBankName != null ? model.WTTransBankName : null;
                        offlinemodel.TRANSBRANCHNAME = model.WTTransBranchName != null ? model.WTTransBranchName : null;
                        offlinemodel.TRANSCHEQUENO = model.WTTransChequeNo != null ? model.WTTransChequeNo : null;
                        offlinemodel.TRANSCHEQUEDATE = model.WTTransChequeDate != null ? model.WTTransChequeDate : null;
                        offlinemodel.TRANSDESCRIPTION = model.WTTransDescription != null ? model.WTTransDescription : null;
                        offlinemodel.TRANSCURRENCY = model.WTTransCurrency != null ? model.WTTransCurrency : null;
                        offlinemodel.TRANSMODE = "Bank";
                        if (model.WTTransAmount != null)
                        {
                            double WTTransAmount = Convert.ToDouble(model.WTTransAmount);
                            offlinemodel.TRANSAMOUNT = WTTransAmount;
                        }
                        if (model.EntityList != null)
                        {
                            offlinemodel.BRANCHENTITYKEY = model.EntityList.EntityKey;
                        }
                    }

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(offlinemodel);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Session["TransactionDetails"] = mUIModel;
                    mFactory.SaveOfflinePaymentTransactionDetails(mUIModel, DiscountCode, DiscountAmount);
                    try
                    {
                        SendReferal_CreditAdded_Email(DiscountCode, HttpContext.User.Identity.Name, mUIModel.TRANSCURRENCY, 0);
                    }
                    catch (Exception ex) { }
                    CheckIsMandatoryDocsUploaded(mUIModel.JOBNUMBER.ToString(), HttpContext.User.Identity.Name, mUIModel.QUOTATIONID, model.OPjobnumber);

                    int id = Convert.ToInt32(model.JobNumber);
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    model.EntityList = miUIModel.EntityList;
                    model.ReceiptNetAmount = miUIModel.ReceiptNetAmount;
                    miUIModel.UserId = HttpContext.User.Identity.Name;
                    model.UserId = HttpContext.User.Identity.Name;

                    try
                    {
                        //SendMailtoGSSC("Offline", model.OPjobnumber, model.PaymentType, id, miUIModel.UserId);
                        byte[] paymentpdfdata = PaymentTransaction(Convert.ToInt32(model.JobNumber), Convert.ToInt32(model.QuotationId), HttpContext.User.Identity.Name);
                        Guid obj = Guid.NewGuid();
                        string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                        FileName = mFactory.SaveOfflinePaymentBookingSummaryDoc(paymentpdfdata, Convert.ToInt32(model.JobNumber), Convert.ToInt32(model.QuotationId), HttpContext.User.Identity.Name, model.OPjobnumber, obj.ToString(), EnvironmentName);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while generating PDF " + ex.ToString());
                    }
                    if (model.PaymentType.ToLower() == "pay at branch")
                    {
                        //customer
                        try
                        {
                            //To Customer
                            SendMailForSuccessPayAtBranch(miUIModel, "Pay at branch", mUIModels, model, branchName + "" + branchAddress, Convert.ToInt32(mUIModel.JOBNUMBER), "Payment", "", "");

                        }
                        catch (Exception exbranch)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbranch.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to user-Offline(Pay at branch) " + exbranch.ToString());
                        }
                        //SendMailWithCard(miUIModel.UserId, "", "Pay at branch", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "", "Offline");
                        //cahier
                        try
                        {
                            SendMailForCountryFinanceCashier("cashier", model, branchName + "" + branchAddress, "Payment", "", "", quotationentity, mDBEntity);
                        }
                        catch (Exception excashier)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excashier.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to cashier-Offline(Pay at branch) " + excashier.ToString());
                        }
                    }
                    else
                    {
                        //customer
                        //SendMailForSuccessPayAtBranch(miUIModel, "Wire transfer", mUIModels, Convert.ToInt32(mUIModel.JOBNUMBER));
                        try
                        {
                            // To Customer
                            SendMailWithCard(miUIModel.UserId, "", "Wire transfer", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Offline");
                        }
                        catch (Exception exWire)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exWire.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to user-Offline(Wire transfer) " + exWire.ToString());
                        }
                        //finance
                        try
                        {
                            SendMailForCountryFinanceCashier("finance", model, "", "Payment", "", "", quotationentity, mDBEntity);
                        }
                        catch (Exception exfinance)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfinance.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to finance-Offline(Wire transfer) " + exfinance.ToString());
                        }
                    }

                    e.USERID = model.UserId;
                    e.JOBNUMBER = model.JobNumber;
                    e.QUOTATIONID = model.QuotationId;
                    e.STATUS = FileName;
                    e.CURRENCY = offlinemodel.TRANSCURRENCY;
                    e.RECEIPTNETAMOUNT = offlinemodel.TRANSAMOUNT;
                    if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                    {
                        return RedirectToAction("OfflinePBSuccess", "Payment", e);
                    }
                    else
                    {
                        return RedirectToAction("OfflineWTSuccess", "Payment", e);
                    }
                }
                catch (Exception ex)
                {
                    int id = Convert.ToInt32(model.JobNumber);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = HttpContext.User.Identity.Name;
                    try
                    {
                        SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Offline");
                    }
                    catch (Exception exfail)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending Payment Failed mail-Offline " + exfail.ToString());
                    }
                    e.USERID = HttpContext.User.Identity.Name;
                    e.JOBNUMBER = model.JobNumber;
                    e.QUOTATIONID = model.QuotationId;
                    e.CURRENCY = miUIModel.Currency;
                    e.RECEIPTNETAMOUNT = Convert.ToDouble(model.ReceiptNetAmount);
                    if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                    {
                        return RedirectToAction("OfflinePBFailure", "Payment", e);
                    }
                    else
                    {
                        return RedirectToAction("OfflineWTFailure", "Payment", e);
                    }
                    throw new ShipaExceptions(ex.ToString());
                }
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                int id = Convert.ToInt32(model.JobNumber);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = HttpContext.User.Identity.Name;
                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {

                    decimal BranchKey = 0;
                    string branchAddress = "";
                    string DiscountCode = "";
                    decimal DiscountAmount = 0;
                    string PAIDCURRENCY = "";
                    string branchName = "";
                    if (Request.Form["txtbranchAddress"] != "")
                    {
                        branchAddress = Request.Form["txtbranchAddress"];
                        Session["BranchAddress"] = branchAddress;
                    }
                    else
                    {
                        Session["BranchAddress"] = null;
                    }
                    if (Request.Form["txtChequeDate"] != "")
                    {
                        model.TransChequeDate = Convert.ToDateTime(Request.Form["txtChequeDate"]);
                    }
                    if (Request.Form["txtCashPayDate"] != "")
                    {
                        model.TransChequeDate = Convert.ToDateTime(Request.Form["txtCashPayDate"]);
                    }
                    if (Request.Form["txtWTChequeDate"] != "")
                    {
                        model.WTTransChequeDate = Convert.ToDateTime(Request.Form["txtWTChequeDate"]);
                    }
                    if (Request.Form["txtTransmode"] != "")
                    {
                        model.TransactionMode = Convert.ToString(Request.Form["txtTransmode"]);
                    }
                    if (model.PaymentType.ToLower() == "wire transfer" && model.EntityList != null && Request.Form["EntityKey"] != "")
                    {
                        model.EntityList.EntityKey = Convert.ToInt64(Request.Form["EntityKey"]);
                    }
                    if (model.PaymentType.ToLower() == "pay at branch" && Request.Form["PBBranchKey"] != "")
                    {
                        BranchKey = Convert.ToDecimal(Request.Form["PBBranchKey"]);
                    }
                    if (Request.Form["txtCouponCode"] != "")
                    {
                        DiscountCode = Request.Form["txtCouponCode"];
                    }
                    else
                    {
                        DiscountCode = "";
                    }
                    if (Request.Form["txtDiscountAmount"] != "")
                    {
                        DiscountAmount = Convert.ToDecimal(Request.Form["txtDiscountAmount"]);
                    }
                    if (Request.Form["txtPaidCurrency"] != "")
                    {
                        PAIDCURRENCY = Request.Form["txtPaidCurrency"];
                    }
                    if (Request.Form["txtbranchName"] != "")
                    {
                        branchName = Request.Form["txtbranchName"] + ", ";
                    }
                    PaymentTransactionDetailsModel offlinemodel = new PaymentTransactionDetailsModel();
                    try
                    {
                        offlinemodel.RECEIPTHDRID = model.RECEIPTHDRID;
                        offlinemodel.JOBNUMBER = model.JobNumber;
                        offlinemodel.QUOTATIONID = model.QuotationId;
                        offlinemodel.PAYMENTOPTION = model.PaymentOption;
                        offlinemodel.ISPAYMENTSUCESS = 1;
                        offlinemodel.PAYMENTREFNUMBER = (model.PaymentType.ToLower() == "wire transfer") ? (model.WTTransChequeNo != null ? model.WTTransChequeNo : null) : (model.TransactionMode.ToLower() == "cash" ? model.TransactionMode : model.TransChequeNo);
                        offlinemodel.PAYMENTREFMESSAGE = "Pending";
                        offlinemodel.CREATEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                        offlinemodel.MODIFIEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                        offlinemodel.PAYMENTTYPE = model.PaymentType;
                        offlinemodel.TRANSMODE = model.TransactionMode != null ? model.TransactionMode : null;
                        offlinemodel.PAIDCURRENCY = PAIDCURRENCY;

                        if (model.PaymentType.ToLower() == "pay at branch")
                        {
                            if (model.TransactionMode.ToLower() == "cash")
                            {
                                offlinemodel.TRANSDESCRIPTION = model.TransDescription != null ? model.TransDescription : null;
                                offlinemodel.TRANSAMOUNT = Convert.ToDouble(model.TransAmount);
                                offlinemodel.TRANSCURRENCY = model.TransCurrency != null ? model.TransCurrency : null;
                                offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                            }
                            else
                            { // For Cheque and Demand draft
                                offlinemodel.TRANSBANKNAME = model.TransBankName != null ? model.TransBankName : null;
                                offlinemodel.TRANSBRANCHNAME = model.TransBranchName != null ? model.TransBranchName : null;
                                offlinemodel.TRANSCHEQUENO = model.TransChequeNo != null ? model.TransChequeNo : null;
                                offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                                offlinemodel.TRANSCURRENCY = model.TransCDCurrency != null ? model.TransCDCurrency : null;
                                offlinemodel.TRANSDESCRIPTION = model.TransCDDescription != null ? model.TransCDDescription : null;
                                if (model.TransCDAmount != null)
                                {
                                    double TransCDAmount = Convert.ToDouble(model.TransCDAmount);
                                    offlinemodel.TRANSAMOUNT = TransCDAmount;
                                }
                            }
                            offlinemodel.BRANCHENTITYKEY = BranchKey != null ? BranchKey : 0;
                        }
                        else if (model.PaymentType.ToLower() == "wire transfer")
                        {
                            offlinemodel.TRANSBANKNAME = model.WTTransBankName != null ? model.WTTransBankName : null;
                            offlinemodel.TRANSBRANCHNAME = model.WTTransBranchName != null ? model.WTTransBranchName : null;
                            offlinemodel.TRANSCHEQUENO = model.WTTransChequeNo != null ? model.WTTransChequeNo : null;
                            offlinemodel.TRANSCHEQUEDATE = model.WTTransChequeDate != null ? model.WTTransChequeDate : null;
                            offlinemodel.TRANSDESCRIPTION = model.WTTransDescription != null ? model.WTTransDescription : null;
                            offlinemodel.TRANSCURRENCY = model.WTTransCurrency != null ? model.WTTransCurrency : null;
                            offlinemodel.TRANSMODE = "Bank";
                            if (model.WTTransAmount != null)
                            {
                                double WTTransAmount = Convert.ToDouble(model.WTTransAmount);
                                offlinemodel.TRANSAMOUNT = WTTransAmount;
                            }
                            if (model.EntityList != null)
                            {
                                offlinemodel.BRANCHENTITYKEY = model.EntityList.EntityKey != null ? model.EntityList.EntityKey : 0;
                            }
                        }
                        offlinemodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                        offlinemodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                        offlinemodel.ADDITIONALCHARGESTATUS = miUIModel.ADDITIONALCHARGESTATUS;


                        DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(offlinemodel);
                        Session["TransactionDetails"] = mUIModel;
                        mFactory.SaveOfflinePaymentTransactionDetails(mUIModel, DiscountCode, DiscountAmount);

                        model.EntityList = miUIModel.EntityList;
                        model.ReceiptNetAmount = miUIModel.ReceiptNetAmount;
                        miUIModel.UserId = HttpContext.User.Identity.Name;
                        model.UserId = HttpContext.User.Identity.Name;
                        try
                        {
                            byte[] paymentpdfdata = PaymentTransaction(Convert.ToInt32(model.JobNumber), Convert.ToInt32(model.QuotationId), HttpContext.User.Identity.Name, miUIModel.CHARGESETID.ToString());
                            Guid obj = Guid.NewGuid();
                            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                            FileName = mFactory.SaveOfflinePaymentBookingSummaryDoc(paymentpdfdata, Convert.ToInt32(model.JobNumber), Convert.ToInt32(model.QuotationId), HttpContext.User.Identity.Name, model.OPjobnumber, obj.ToString(), EnvironmentName, miUIModel.CHARGESETID.ToString());
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while generating PDF " + ex.ToString());
                        }

                        if (model.PaymentType.ToLower() == "pay at branch")
                        {
                            try
                            {
                                // To customer                               
                                SendMailForSuccessPayAtBranch(miUIModel, "Pay at branch", mUIModels, model, branchName + "" + branchAddress, Convert.ToInt32(mUIModel.JOBNUMBER), "Additional Charge Payment", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT));
                            }
                            catch (Exception exbranch)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbranch.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to user-Offline(Pay at branch with ADDITIONALCHARGEAMOUNT) " + exbranch.ToString());
                            }
                            //SendMailWithCard(miUIModel.UserId, "", "Pay at branch", miUIModel.ADDITIONALCHARGEAMOUNT.ToString("N2"), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "", "Offline");
                            try
                            {
                                SendMailForCountryFinanceCashier("cashier", model, branchName + "" + branchAddress, "Additional Charge Payment", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), quotationentity, mDBEntity);
                            }
                            catch (Exception excashier)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excashier.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to cashier-Offline(Pay at branch with ADDITIONALCHARGEAMOUNT) " + excashier.ToString());
                            }
                        }
                        else
                        {
                            //SendMailForSuccessPayAtBranch(miUIModel, "Wire transfer", mUIModels, Convert.ToInt32(mUIModel.JOBNUMBER), "", miUIModel.ADDITIONALCHARGEAMOUNT.ToString("N2"));
                            try
                            {
                                // To customer
                                SendMailWithCard(miUIModel.UserId, "", "Wire transfer", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Offline");
                            }
                            catch (Exception exWire)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exWire.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to user-Offline(Wire transfer with ADDITIONALCHARGEAMOUNT) " + exWire.ToString());
                            }
                            try
                            {
                                SendMailForCountryFinanceCashier("finance", model, "", "Additional Charge Payment", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), quotationentity, mDBEntity);
                            }
                            catch (Exception exfinance)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfinance.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to finance-Offline(Wire transfer with ADDITIONALCHARGEAMOUNT) " + exfinance.ToString());
                            }
                        }
                        //SendMailtoGSSC("Offline", model.OPjobnumber, model.PaymentType, id, miUIModel.UserId);
                        e.USERID = model.UserId;
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.STATUS = FileName;
                        e.CURRENCY = offlinemodel.TRANSCURRENCY;
                        e.RECEIPTNETAMOUNT = offlinemodel.TRANSAMOUNT;
                        if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                        {
                            return RedirectToAction("OfflinePBSuccess", "Payment", e);
                        }
                        else
                        {
                            return RedirectToAction("OfflineWTSuccess", "Payment", e);
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.status = "Offline Fail";
                        mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                        miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                        miUIModel.UserId = HttpContext.User.Identity.Name;
                        try
                        {
                            SendMailWithCard(miUIModel.UserId, "", "", miUIModel.ADDITIONALCHARGEAMOUNT.ToString(), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Offline");
                        }
                        catch (Exception exfail)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending Payment Failed mail-Offline with ADDITIONALCHARGEAMOUNT " + exfail.ToString());
                        }
                        e.USERID = HttpContext.User.Identity.Name;
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.CURRENCY = miUIModel.Currency;
                        e.RECEIPTNETAMOUNT = Convert.ToDouble(miUIModel.ADDITIONALCHARGEAMOUNT);
                        if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                        {
                            return RedirectToAction("OfflinePBFailure", "Payment", e);
                        }
                        else
                        {
                            return RedirectToAction("OfflineWTFailure", "Payment", e);
                        }
                        throw new ShipaExceptions(ex.ToString());
                    }
                }
                else
                {
                    e.MODE = "Offline";
                    e.JOBNUMBER = model.JobNumber;
                    e.QUOTATIONID = model.QuotationId;

                    return RedirectToAction("PaymentDone", "Payment", e);
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreditBusinessPayment(PaymentModel model)
        {
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(model.JobNumber));
            MPaymentResult e = new MPaymentResult();
            if (PaymentStatus.ToLower() == "notexist")
            {
                UserDBFactory UserFactory = new UserDBFactory();
                var OneSignalService = new Push.OneSignal();
                string str = string.Empty;
                string DiscountCode = "";
                decimal DiscountAmount = 0;
                if (Request.Form["txtCreditTransmode"] != "")
                {
                    model.TransactionMode = Convert.ToString(Request.Form["txtCreditTransmode"]);
                }
                if (Request.Form["txtBCCouponCode"] != "")
                {
                    DiscountCode = Request.Form["txtBCCouponCode"];
                }
                else
                {
                    DiscountCode = "";
                }
                if (Request.Form["txtBCDiscountAmount"] != "")
                {
                    DiscountAmount = Convert.ToDecimal(Request.Form["txtBCDiscountAmount"]);
                }
                PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
                try
                {
                    BusinessCreditmodel.RECEIPTHDRID = model.RECEIPTHDRID;
                    BusinessCreditmodel.JOBNUMBER = model.JobNumber;
                    BusinessCreditmodel.QUOTATIONID = model.QuotationId;
                    BusinessCreditmodel.PAYMENTOPTION = model.PaymentOption;
                    BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                    BusinessCreditmodel.PAYMENTREFNUMBER = "Credit";
                    BusinessCreditmodel.PAYMENTREFMESSAGE = "approved";
                    BusinessCreditmodel.CREATEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                    BusinessCreditmodel.MODIFIEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                    BusinessCreditmodel.PAYMENTTYPE = "Business credit";
                    BusinessCreditmodel.TRANSMODE = model.TransactionMode;
                    BusinessCreditmodel.TRANSDESCRIPTION = "Business Credit";
                    BusinessCreditmodel.TRANSAMOUNT = model.Amount;
                    BusinessCreditmodel.TRANSCURRENCY = model.Currency;

                    e.CURRENCY = model.Currency;
                    e.RECEIPTNETAMOUNT = model.Amount;

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    mFactory.SaveBusinessCreditTransactionDetails(mUIModel, HttpContext.User.Identity.Name, DiscountCode, DiscountAmount);
                    if (DiscountCode.ToUpper().Contains("M1"))
                    {
                        SendReferal_CreditAdded_Email(DiscountCode, HttpContext.User.Identity.Name, model.Currency, Convert.ToDouble(DiscountAmount));
                    }
                    else
                    {
                        SendReferal_CreditAdded_Email(DiscountCode, HttpContext.User.Identity.Name, model.Currency, 0);
                    }
                    CheckIsMandatoryDocsUploaded(mUIModel.JOBNUMBER.ToString(), HttpContext.User.Identity.Name, mUIModel.QUOTATIONID, model.OPjobnumber);
                    int id = Convert.ToInt32(model.JobNumber);
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = HttpContext.User.Identity.Name;
                    byte[] bytes = null;

                    UserFactory.NewNotification(5113, "Payment Completed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), HttpContext.User.Identity.Name);
                    var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = HttpContext.User.Identity.Name };
                    SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", HttpContext.User.Identity.Name.ToLowerInvariant(), "Payment Completed", pushdata); });
                    DBFactory.Entities.QuotationEntity quotationentity = new QuotationEntity();
                    UserDBFactory UD = new UserDBFactory();
                    UserEntity UE = UD.GetUserFirstAndLastName(miUIModel.UserId);
                    try
                    {
                        QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                        int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(model.QuotationId));
                        if (count > 0)
                        {
                            quotationentity = mQuoFactory.GetById(Convert.ToInt32(model.QuotationId));
                            mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
                            //---------------------- Added By Anil G-------------------------------
                            GenerateBookingConfirmationPDF(Convert.ToInt32(model.JobNumber), "Booking Confirmation", Convert.ToInt64(model.QuotationId), model.OPjobnumber, HttpContext.User.Identity.Name.ToString());
                            bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(model.JobNumber), "Advance Receipt", Convert.ToInt64(model.QuotationId), model.OPjobnumber, HttpContext.User.Identity.Name.ToString(), 0);

                            //GenerateJobConfPDF(model.QuotationNumber, Convert.ToInt64(model.JobNumber), Convert.ToInt64(model.QuotationId), model.OPjobnumber, "Booking Confirmation");
                            //GenerateJobConfPDF(model.QuotationNumber, Convert.ToInt64(model.JobNumber), Convert.ToInt64(model.QuotationId), model.OPjobnumber, "Advance Receipt");
                            //--------------------------------------------------------------------------------
                        }
                    }
                    catch (Exception ex1)
                    {
                        e.USERID = HttpContext.User.Identity.Name;
                        e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                        e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                        e.JOBNUMBER = model.JobNumber;
                        return RedirectToAction("BusinessCreditSuccess", "Payment", e);
                    }

                    bytes = bytes != null ? bytes : null;
                    try
                    {
                        //SendMailForSuccessBusinessCredit(miUIModel, bytes, mUIModels);
                        SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Business credit");
                    }
                    catch (Exception exbc)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbc.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending mail to user-Business Credit " + exbc.ToString());
                    }
                    try
                    {
                        //Sending mail to GSSC, Operations and finance.  
                        try
                        {
                            SendMailtoGroup(model.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC", "Paymentstatus", Convert.ToInt32(model.JobNumber), miUIModel.UserId, "Business Credit", "Payment", Convert.ToString(miUIModel.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, quotationentity, mDBEntity);
                            //string CEEmail = string.Empty;
                            //UserEntity UE = UserFactory.GetUserFirstAndLastName(miUIModel.UserId);
                            //QuotationDBFactory mQuotationDBFactory = new QuotationDBFactory();
                            //CEEmail = string.IsNullOrEmpty(mUIModels.CE) ? UE.CRMEmail : mUIModels.CE;
                            //if (string.IsNullOrEmpty(CEEmail))
                            //{
                            //    string CE = mQuotationDBFactory.UpdateCEForQuotation(mUIModels.QuotationId);
                            //    CEEmail = CE;
                            //}
                            SendThankingMailtoCustomer("cet@shipafreight.com", miUIModel.UserId, miUIModel.OPjobnumber);
                        }
                        catch (Exception exops)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exops.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to COUNTRYJOBOPS,COUNTRYFINANCE,GSSC-Business Credit " + exops.ToString());
                        }
                    }
                    catch
                    {
                        //
                    }

                    e.USERID = HttpContext.User.Identity.Name;
                    e.JOBNUMBER = model.JobNumber;
                    return RedirectToAction("BusinessCreditSuccess", "Payment", e);

                }
                catch (Exception ex)
                {
                    int id = Convert.ToInt32(model.JobNumber);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = HttpContext.User.Identity.Name;
                    if (str != "Success")
                    {
                        try
                        {
                            SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ReceiptNetAmount), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Business credit");

                        }
                        catch (Exception exbc)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbc.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending Payment failed mail-Business Credit " + exbc.ToString());
                        }
                        UserFactory.NewNotification(5113, "Payment failed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), HttpContext.User.Identity.Name);
                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment failed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = HttpContext.User.Identity.Name };
                        SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", HttpContext.User.Identity.Name.ToLowerInvariant(), "Payment failed", pushdata); });

                    }
                    e.JOBNUMBER = model.JobNumber;
                    e.QUOTATIONID = model.QuotationId;
                    e.RECEIPTNETAMOUNT = Convert.ToDouble(miUIModel.ReceiptNetAmount);
                    e.CURRENCY = miUIModel.Currency;
                    e.USERID = HttpContext.User.Identity.Name;
                    return RedirectToAction("BusinessCreditFailure", "Payment", e);
                }
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                var OneSignalService = new Push.OneSignal();
                int id = Convert.ToInt32(model.JobNumber);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = HttpContext.User.Identity.Name;
                DBFactory.Entities.QuotationEntity quotationentity = new QuotationEntity();
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(miUIModel.UserId);
                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    UserDBFactory UserFactory = new UserDBFactory();
                    string str = string.Empty;
                    string DiscountCode = "";
                    decimal DiscountAmount = 0;
                    if (Request.Form["txtCreditTransmode"] != "")
                    {
                        model.TransactionMode = Convert.ToString(Request.Form["txtCreditTransmode"]);
                    }
                    if (Request.Form["txtBCCouponCode"] != "")
                    {
                        DiscountCode = Request.Form["txtBCCouponCode"];
                    }
                    else
                    {
                        DiscountCode = "";
                    }
                    if (Request.Form["txtBCDiscountAmount"] != "")
                    {
                        DiscountAmount = Convert.ToDecimal(Request.Form["txtBCDiscountAmount"]);
                    }
                    PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
                    try
                    {
                        BusinessCreditmodel.RECEIPTHDRID = model.RECEIPTHDRID;
                        BusinessCreditmodel.JOBNUMBER = model.JobNumber;
                        BusinessCreditmodel.QUOTATIONID = model.QuotationId;
                        BusinessCreditmodel.PAYMENTOPTION = model.PaymentOption;
                        BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                        BusinessCreditmodel.PAYMENTREFNUMBER = "Credit";
                        BusinessCreditmodel.PAYMENTREFMESSAGE = "approved";
                        BusinessCreditmodel.CREATEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                        BusinessCreditmodel.MODIFIEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                        BusinessCreditmodel.PAYMENTTYPE = "Business credit";
                        BusinessCreditmodel.TRANSMODE = model.TransactionMode;
                        BusinessCreditmodel.TRANSDESCRIPTION = "Business Credit";
                        BusinessCreditmodel.TRANSAMOUNT = model.Amount;
                        BusinessCreditmodel.TRANSCURRENCY = model.Currency;
                        BusinessCreditmodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                        BusinessCreditmodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                        BusinessCreditmodel.ADDITIONALCHARGESTATUS = (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now") ? (BusinessCreditmodel.ISPAYMENTSUCESS == 1) ? "Paid" : "Rejected" : "";

                        e.CURRENCY = model.Currency;
                        e.RECEIPTNETAMOUNT = model.Amount;
                        e.JOBNUMBER = model.JobNumber;
                        DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);

                        mFactory.SaveBusinessCreditTransactionDetails(mUIModel, HttpContext.User.Identity.Name, DiscountCode, DiscountAmount);

                        byte[] bytes = null;

                        UserFactory.NewNotification(5113, "Payment Completed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), HttpContext.User.Identity.Name);

                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = HttpContext.User.Identity.Name };
                        SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", HttpContext.User.Identity.Name.ToLowerInvariant(), "Payment Completed", pushdata); });


                        try
                        {
                            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(model.QuotationId));
                            if (count > 0)
                            {
                                quotationentity = mQuoFactory.GetById(Convert.ToInt32(model.QuotationId));
                                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
                                //---------------------- Added By Anil G-------------------------------
                                //GenerateBookingConfirmationPDF(Convert.ToInt32(model.JobNumber), "Booking Confirmation", Convert.ToInt64(model.QuotationId), model.OPjobnumber, HttpContext.User.Identity.Name.ToString());
                                bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(model.JobNumber), "Advance Receipt", Convert.ToInt64(model.QuotationId), model.OPjobnumber, HttpContext.User.Identity.Name.ToString(), Convert.ToInt32(miUIModel.CHARGESETID));

                                //GenerateJobConfPDF(model.QuotationNumber, Convert.ToInt64(model.JobNumber), Convert.ToInt64(model.QuotationId), model.OPjobnumber, "Booking Confirmation");
                                //GenerateJobConfPDF(model.QuotationNumber, Convert.ToInt64(model.JobNumber), Convert.ToInt64(model.QuotationId), model.OPjobnumber, "Advance Receipt");
                                //--------------------------------------------------------------------------------
                            }
                        }
                        catch
                        {
                            e.USERID = HttpContext.User.Identity.Name;
                            e.JOBNUMBER = model.JobNumber;
                            return RedirectToAction("BusinessCreditSuccess", "Payment", e);
                        }

                        bytes = bytes != null ? bytes : null;
                        try
                        {
                            SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Business credit");
                        }
                        catch (Exception ex2)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex2.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to user-Business Credit(ADDITIONALCHARGEAMOUNT) " + ex2.ToString());
                        }
                        try
                        {
                            //Sending mail to Operations and finance.   
                            try
                            {
                                SendMailtoGroup(model.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE", "Paymentstatus", Convert.ToInt32(model.JobNumber), miUIModel.UserId, "Business Credit", "Additional Charge Payment", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, quotationentity, mDBEntity);
                                //string CEEmail = string.Empty;
                                //UserEntity UE = UserFactory.GetUserFirstAndLastName(miUIModel.UserId);
                                //QuotationDBFactory mQuotationDBFactory = new QuotationDBFactory();
                                //CEEmail = string.IsNullOrEmpty(mUIModels.CE) ? UE.CRMEmail : mUIModels.CE;
                                //if (string.IsNullOrEmpty(CEEmail))
                                //{
                                //    string CE = mQuotationDBFactory.UpdateCEForQuotation(mUIModels.QuotationId);
                                //    CEEmail = CE;
                                //}
                                SendThankingMailtoCustomer("cet@shipafreight.com", miUIModel.UserId, miUIModel.OPjobnumber);
                            }
                            catch (Exception exops)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exops.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to COUNTRYJOBOPS,COUNTRYFINANCE-Business Credit(ADDITIONALCHARGEAMOUNT) " + exops.ToString());
                            }
                        }
                        catch
                        {
                            //
                        }

                        e.USERID = HttpContext.User.Identity.Name;
                        e.JOBNUMBER = model.JobNumber;
                        return RedirectToAction("BusinessCreditSuccess", "Payment", e);
                    }
                    catch (Exception ex)
                    {
                        mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, HttpContext.User.Identity.Name);
                        miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                        miUIModel.UserId = HttpContext.User.Identity.Name;
                        if (str != "Success")
                        {
                            try
                            {
                                SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Business credit");

                            }
                            catch (Exception exfail)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending Payment failed mail-Business Credit(ADDITIONALCHARGEAMOUNT) " + exfail.ToString());
                            }
                            UserFactory.NewNotification(5113, "Payment failed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), HttpContext.User.Identity.Name);
                            var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment failed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = HttpContext.User.Identity.Name };
                            SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", HttpContext.User.Identity.Name.ToLowerInvariant(), "Payment failed", pushdata); });

                        }
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.RECEIPTNETAMOUNT = Convert.ToDouble(miUIModel.ADDITIONALCHARGEAMOUNT);
                        e.CURRENCY = miUIModel.Currency;
                        e.USERID = HttpContext.User.Identity.Name;
                        return RedirectToAction("BusinessCreditFailure", "Payment", e);
                    }
                }
                else
                {
                    e.MODE = "Business Credit";
                    e.JOBNUMBER = model.JobNumber;
                    e.QUOTATIONID = model.QuotationId;

                    return RedirectToAction("PaymentDone", "Payment", e);
                }
            }
        }

        public ActionResult MPayment(int id, string user)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, user);
            PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
            if (mUIModel.BranchList.Any())
            {
                var x = mUIModel.BranchList.FirstOrDefault().BranchKey;
                ViewBag.BranchKeyNo = x;
            }
            StripeData stripeData = GetStripeData(mUIModel.Currency);
            mUIModel.stripePKKey = stripeData.publicKey;
            mUIModel.UserId = user;
            return View("MCharge", mUIModel);
        }

        [HttpPost]
        public ActionResult MOfflinePayment(PaymentModel model)
        {
            MPaymentResult e = new MPaymentResult();
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(model.JobNumber));
            if (PaymentStatus.ToLower() == "notexist")
            {
                decimal BranchKey = 0;
                string branchAddress = "";
                string DiscountCode = "";
                decimal DiscountAmount = 0;
                string PAIDCURRENCY = "";
                string branchName = "";
                if (Request.Form["txtbranchAddress"] != "")
                {
                    branchAddress = Request.Form["txtbranchAddress"];
                    Session["BranchAddress"] = branchAddress;
                }
                else
                {
                    Session["BranchAddress"] = null;
                }

                if (Request.Form["txtChequeDate"] != "")
                {
                    model.TransChequeDate = Convert.ToDateTime(Request.Form["txtChequeDate"]);
                }
                if (Request.Form["txtCashPayDate"] != "")
                {
                    model.TransChequeDate = Convert.ToDateTime(Request.Form["txtCashPayDate"]);
                }
                if (Request.Form["txtWTChequeDate"] != "")
                {
                    model.WTTransChequeDate = Convert.ToDateTime(Request.Form["txtWTChequeDate"]);
                }
                if (Request.Form["txtTransmode"] != "")
                {
                    model.TransactionMode = Convert.ToString(Request.Form["txtTransmode"]);
                }
                if (model.PaymentType.ToLower() == "wire transfer" && model.EntityList != null && Request.Form["EntityKey"] != "")
                {
                    model.EntityList.EntityKey = Convert.ToInt64(Request.Form["EntityKey"]);
                }
                if (model.PaymentType.ToLower() == "pay at branch" && Request.Form["PBBranchKey"] != "")
                {
                    BranchKey = Convert.ToDecimal(Request.Form["PBBranchKey"]);
                }
                if (Request.Form["txtCouponCode"] != "")
                {
                    DiscountCode = Request.Form["txtCouponCode"];
                }
                else
                {
                    DiscountCode = "";
                }
                if (Request.Form["txtDiscountAmount"] != "")
                {
                    DiscountAmount = Convert.ToDecimal(Request.Form["txtDiscountAmount"]);
                }
                if (Request.Form["txtPaidCurrency"] != "")
                {
                    PAIDCURRENCY = Request.Form["txtPaidCurrency"];
                }
                if (Request.Form["txtbranchName"] != "")
                {
                    branchName = Request.Form["txtbranchName"] + ", ";
                }

                PaymentTransactionDetailsModel offlinemodel = new PaymentTransactionDetailsModel();
                try
                {
                    offlinemodel.RECEIPTHDRID = model.RECEIPTHDRID;
                    offlinemodel.JOBNUMBER = model.JobNumber;
                    offlinemodel.QUOTATIONID = model.QuotationId;
                    offlinemodel.PAYMENTOPTION = model.PaymentOption;
                    offlinemodel.ISPAYMENTSUCESS = 1;
                    offlinemodel.PAYMENTREFNUMBER = (model.PaymentType.ToLower() == "wire transfer") ? (model.WTTransChequeNo != null ? model.WTTransChequeNo : null) : (model.TransactionMode.ToLower() == "cash" ? model.TransactionMode : model.TransChequeNo);
                    offlinemodel.PAYMENTREFMESSAGE = "Pending";
                    offlinemodel.CREATEDBY = model.UserId;
                    offlinemodel.MODIFIEDBY = model.UserId;
                    offlinemodel.PAYMENTTYPE = model.PaymentType;
                    offlinemodel.TRANSMODE = model.TransactionMode != null ? model.TransactionMode : null;
                    offlinemodel.PAIDCURRENCY = PAIDCURRENCY;

                    if (model.PaymentType.ToLower() == "pay at branch")
                    {
                        if (model.TransactionMode.ToLower() == "cash")
                        {
                            offlinemodel.TRANSDESCRIPTION = model.TransDescription != null ? model.TransDescription : null;
                            offlinemodel.TRANSAMOUNT = Convert.ToDouble(model.TransAmount);
                            offlinemodel.TRANSCURRENCY = model.TransCurrency != null ? model.TransCurrency : null;
                            offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                        }
                        else
                        { // For Cheque and Demand draft
                            offlinemodel.TRANSBANKNAME = model.TransBankName != null ? model.TransBankName : null;
                            offlinemodel.TRANSBRANCHNAME = model.TransBranchName != null ? model.TransBranchName : null;
                            offlinemodel.TRANSCHEQUENO = model.TransChequeNo != null ? model.TransChequeNo : null;
                            offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                            offlinemodel.TRANSCURRENCY = model.TransCDCurrency != null ? model.TransCDCurrency : null;
                            offlinemodel.TRANSDESCRIPTION = model.TransCDDescription != null ? model.TransCDDescription : null;
                            if (model.TransCDAmount != null)
                            {
                                double TransCDAmount = Convert.ToDouble(model.TransCDAmount);
                                offlinemodel.TRANSAMOUNT = TransCDAmount;
                            }
                        }
                        offlinemodel.BRANCHENTITYKEY = BranchKey != null ? BranchKey : 0;
                    }
                    else if (model.PaymentType.ToLower() == "wire transfer")
                    {
                        offlinemodel.TRANSBANKNAME = model.WTTransBankName != null ? model.WTTransBankName : null;
                        offlinemodel.TRANSBRANCHNAME = model.WTTransBranchName != null ? model.WTTransBranchName : null;
                        offlinemodel.TRANSCHEQUENO = model.WTTransChequeNo != null ? model.WTTransChequeNo : null;
                        offlinemodel.TRANSCHEQUEDATE = model.WTTransChequeDate != null ? model.WTTransChequeDate : null;
                        offlinemodel.TRANSDESCRIPTION = model.WTTransDescription != null ? model.WTTransDescription : null;
                        offlinemodel.TRANSCURRENCY = model.WTTransCurrency != null ? model.WTTransCurrency : null;
                        offlinemodel.TRANSMODE = "Bank";
                        if (model.WTTransAmount != null)
                        {
                            double WTTransAmount = Convert.ToDouble(model.WTTransAmount);
                            offlinemodel.TRANSAMOUNT = WTTransAmount;
                        }
                        if (model.EntityList != null)
                        {
                            offlinemodel.BRANCHENTITYKEY = model.EntityList.EntityKey != null ? model.EntityList.EntityKey : 0;
                        }
                    }

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(offlinemodel);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Session["TransactionDetails"] = mUIModel;
                    mFactory.SaveOfflinePaymentTransactionDetails(mUIModel, DiscountCode, DiscountAmount);
                    ViewBag.status = "Offline Success";
                    CheckIsMandatoryDocsUploaded(mUIModel.JOBNUMBER.ToString(), model.UserId, mUIModel.QUOTATIONID, model.OPjobnumber);
                    UserDBFactory UD = new UserDBFactory();
                    UserEntity UE = UD.GetUserFirstAndLastName(model.UserId);

                    int id = Convert.ToInt32(model.JobNumber);
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    model.EntityList = miUIModel.EntityList;
                    model.ReceiptNetAmount = miUIModel.ReceiptNetAmount;
                    miUIModel.UserId = model.UserId;
                    try
                    {
                        //---------------------------Payment Transaction-----------------------------------------
                        QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                        int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(model.QuotationId));
                        if (count > 0)
                        {
                            QuotationEntity mQuotationEntity = mQuoFactory.GetById(Convert.ToInt64(model.QuotationId));
                            QuotationPreviewModel mQuotaionpreview = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mQuotationEntity);

                            JobBookingModel mbookingModel = new JobBookingModel();
                            PaymentTransactionDetailsEntity paymentTransactEntity = mUIModel;
                            PaymentTransactionDetailsModel paymentTrans = Mapper.Map<PaymentTransactionDetailsEntity, PaymentTransactionDetailsModel>(paymentTransactEntity);
                            DBFactory.Entities.PaymentEntity paymentEntity = mFactory.GetPaymentDetailsByJobNumber(Convert.ToInt32(model.JobNumber), model.UserId); // HttpContext.User.Identity.Name
                            PaymentModel mpayment = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(paymentEntity);
                            DBFactory.Entities.JobBookingEntity bookingEntity = mFactory.GetQuotedJobDetails(Convert.ToInt32(model.QuotationId), Convert.ToInt64(model.JobNumber));
                            mbookingModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(bookingEntity);

                            GeneratePaymentTransactionPdfMobile(paymentTrans, mpayment, mbookingModel, mQuotaionpreview);
                            //---------------------------------------------------------------------------------------
                        }
                    }
                    catch
                    {
                        //
                    }

                    try
                    {
                        if (model.PaymentType.ToLower() == "pay at branch")
                        {
                            try
                            {
                                // To Customer
                                SendMailForSuccessPayAtBranch(miUIModel, "Pay at branch", mUIModels, model, branchName + "" + branchAddress, Convert.ToInt32(mUIModel.JOBNUMBER), "Payment", UE.FIRSTNAME, "");
                            }
                            catch (Exception exbranch)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbranch.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to user-Offline(Pay at branch-Mobile) " + exbranch.ToString());
                            }
                            //SendMailWithCard(miUIModel.UserId, "", "Pay at branch", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.FIRSTNAME, "Offline");
                            try
                            {
                                SendMailForCountryFinanceCashier("cashier", model, branchName + "" + branchAddress, "Payment", UE.FIRSTNAME);
                            }
                            catch (Exception excashier)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excashier.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to cashier-Offline(Pay at branch-Mobile) " + excashier.ToString());
                            }
                        }
                        else
                        {
                            //SendMailForSuccessPayAtBranch(miUIModel, "Wire transfer", mUIModels, Convert.ToInt32(mUIModel.JOBNUMBER), UE.FIRSTNAME);
                            try
                            {
                                SendMailWithCard(miUIModel.UserId, "", "Wire transfer", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", UE.FIRSTNAME, "Offline");
                            }
                            catch (Exception exWire)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exWire.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to user-Offline(Wire transfer-Mobile) " + exWire.ToString());
                            }
                            try
                            {
                                SendMailForCountryFinanceCashier("finance", model, "", "Payment", UE.FIRSTNAME);
                            }
                            catch (Exception exfinance)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfinance.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to finance-Offline(Wire transfer-Mobile) " + exfinance.ToString());
                            }
                        }

                        //  SendMailtoGSSC("Offline", model.OPjobnumber, model.PaymentType, id, model.UserId, UE.FIRSTNAME);
                        //PaymentTransaction(Convert.ToInt32(id), Convert.ToInt32(model.QuotationId), model.UserId.ToString());
                        e.USERID = model.UserId;
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.CURRENCY = offlinemodel.TRANSCURRENCY;
                        e.RECEIPTNETAMOUNT = offlinemodel.TRANSAMOUNT;
                        if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                        {
                            return RedirectToAction("MOfflinePBSuccess", "Payment", e);
                        }
                        else
                        {
                            return RedirectToAction("MOfflineWTSuccess", "Payment", e);
                        }
                    }
                    catch (Exception ex)
                    {
                        e.USERID = model.UserId;
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.CURRENCY = miUIModel.Currency;
                        e.RECEIPTNETAMOUNT = Convert.ToDouble(model.ReceiptNetAmount);
                        if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                        {
                            return RedirectToAction("MOfflinePBSuccess", "Payment", e);
                        }
                        else
                        {
                            return RedirectToAction("MOfflineWTSuccess", "Payment", e);
                        }
                    }
                }
                catch (Exception ex)
                {
                    int id = Convert.ToInt32(model.JobNumber);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = model.UserId;
                    try
                    {
                        SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Offline");
                    }
                    catch (Exception exfail)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending Payment Failed mail-Offline-Mobile " + exfail.ToString());
                    }
                    e.USERID = model.UserId;
                    e.JOBNUMBER = model.JobNumber;
                    e.QUOTATIONID = model.QuotationId;
                    e.CURRENCY = miUIModel.Currency;
                    e.RECEIPTNETAMOUNT = Convert.ToDouble(model.ReceiptNetAmount);
                    if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                    {
                        return RedirectToAction("MOfflinePBFailure", "Payment", e);
                    }
                    else
                    {
                        return RedirectToAction("MOfflineWTFailure", "Payment", e);
                    }
                }
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                int id = Convert.ToInt32(model.JobNumber);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = model.UserId;
                string PAIDCURRENCY = "";
                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    decimal BranchKey = 0;
                    string branchAddress = "";
                    string DiscountCode = "";
                    decimal DiscountAmount = 0;
                    string branchName = "";
                    if (Request.Form["txtbranchAddress"] != "")
                    {
                        branchAddress = Request.Form["txtbranchAddress"];
                        Session["BranchAddress"] = branchAddress;
                    }
                    else
                    {
                        Session["BranchAddress"] = null;
                    }

                    if (Request.Form["txtChequeDate"] != "")
                    {
                        model.TransChequeDate = Convert.ToDateTime(Request.Form["txtChequeDate"]);
                    }
                    if (Request.Form["txtCashPayDate"] != "")
                    {
                        model.TransChequeDate = Convert.ToDateTime(Request.Form["txtCashPayDate"]);
                    }
                    if (Request.Form["txtWTChequeDate"] != "")
                    {
                        model.WTTransChequeDate = Convert.ToDateTime(Request.Form["txtWTChequeDate"]);
                    }
                    if (Request.Form["txtTransmode"] != "")
                    {
                        model.TransactionMode = Convert.ToString(Request.Form["txtTransmode"]);
                    }
                    if (model.PaymentType.ToLower() == "wire transfer" && model.EntityList != null && Request.Form["EntityKey"] != "")
                    {
                        model.EntityList.EntityKey = Convert.ToInt64(Request.Form["EntityKey"]);
                    }
                    if (model.PaymentType.ToLower() == "pay at branch" && Request.Form["PBBranchKey"] != "")
                    {
                        BranchKey = Convert.ToDecimal(Request.Form["PBBranchKey"]);
                    }
                    if (Request.Form["txtCouponCode"] != "")
                    {
                        DiscountCode = Request.Form["txtCouponCode"];
                    }
                    else
                    {
                        DiscountCode = "";
                    }
                    if (Request.Form["txtDiscountAmount"] != "")
                    {
                        DiscountAmount = Convert.ToDecimal(Request.Form["txtDiscountAmount"]);
                    }
                    if (Request.Form["txtPaidCurrency"] != "")
                    {
                        PAIDCURRENCY = Request.Form["txtPaidCurrency"];
                    }
                    if (Request.Form["txtbranchName"] != "")
                    {
                        branchName = Request.Form["txtbranchName"] + ", ";
                    }

                    PaymentTransactionDetailsModel offlinemodel = new PaymentTransactionDetailsModel();
                    try
                    {
                        offlinemodel.RECEIPTHDRID = model.RECEIPTHDRID;
                        offlinemodel.JOBNUMBER = model.JobNumber;
                        offlinemodel.QUOTATIONID = model.QuotationId;
                        offlinemodel.PAYMENTOPTION = model.PaymentOption;
                        offlinemodel.ISPAYMENTSUCESS = 1;
                        offlinemodel.PAYMENTREFNUMBER = (model.PaymentType.ToLower() == "wire transfer") ? (model.WTTransChequeNo != null ? model.WTTransChequeNo : null) : (model.TransactionMode.ToLower() == "cash" ? model.TransactionMode : model.TransChequeNo);
                        offlinemodel.PAYMENTREFMESSAGE = "Pending";
                        offlinemodel.CREATEDBY = model.UserId;
                        offlinemodel.MODIFIEDBY = model.UserId;
                        offlinemodel.PAYMENTTYPE = model.PaymentType;
                        offlinemodel.TRANSMODE = model.TransactionMode != null ? model.TransactionMode : null;
                        offlinemodel.PAIDCURRENCY = PAIDCURRENCY;
                        if (model.PaymentType.ToLower() == "pay at branch")
                        {
                            if (model.TransactionMode.ToLower() == "cash")
                            {
                                offlinemodel.TRANSDESCRIPTION = model.TransDescription != null ? model.TransDescription : null;
                                offlinemodel.TRANSAMOUNT = Convert.ToDouble(model.TransAmount);
                                offlinemodel.TRANSCURRENCY = model.TransCurrency != null ? model.TransCurrency : null;
                                offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                            }
                            else
                            { // For Cheque and Demand draft
                                offlinemodel.TRANSBANKNAME = model.TransBankName != null ? model.TransBankName : null;
                                offlinemodel.TRANSBRANCHNAME = model.TransBranchName != null ? model.TransBranchName : null;
                                offlinemodel.TRANSCHEQUENO = model.TransChequeNo != null ? model.TransChequeNo : null;
                                offlinemodel.TRANSCHEQUEDATE = model.TransChequeDate != null ? model.TransChequeDate : null;
                                offlinemodel.TRANSCURRENCY = model.TransCDCurrency != null ? model.TransCDCurrency : null;
                                offlinemodel.TRANSDESCRIPTION = model.TransCDDescription != null ? model.TransCDDescription : null;
                                if (model.TransCDAmount != null)
                                {
                                    double TransCDAmount = Convert.ToDouble(model.TransCDAmount);
                                    offlinemodel.TRANSAMOUNT = TransCDAmount;
                                }
                            }
                            offlinemodel.BRANCHENTITYKEY = BranchKey != null ? BranchKey : 0;
                        }
                        else if (model.PaymentType.ToLower() == "wire transfer")
                        {
                            offlinemodel.TRANSBANKNAME = model.WTTransBankName != null ? model.WTTransBankName : null;
                            offlinemodel.TRANSBRANCHNAME = model.WTTransBranchName != null ? model.WTTransBranchName : null;
                            offlinemodel.TRANSCHEQUENO = model.WTTransChequeNo != null ? model.WTTransChequeNo : null;
                            offlinemodel.TRANSCHEQUEDATE = model.WTTransChequeDate != null ? model.WTTransChequeDate : null;
                            offlinemodel.TRANSDESCRIPTION = model.WTTransDescription != null ? model.WTTransDescription : null;
                            offlinemodel.TRANSCURRENCY = model.WTTransCurrency != null ? model.WTTransCurrency : null;
                            offlinemodel.TRANSMODE = "Bank";
                            if (model.WTTransAmount != null)
                            {
                                double WTTransAmount = Convert.ToDouble(model.WTTransAmount);
                                offlinemodel.TRANSAMOUNT = WTTransAmount;
                            }
                            if (model.EntityList != null)
                            {
                                offlinemodel.BRANCHENTITYKEY = model.EntityList.EntityKey != null ? model.EntityList.EntityKey : 0;
                            }
                        }
                        offlinemodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                        offlinemodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                        offlinemodel.ADDITIONALCHARGESTATUS = miUIModel.ADDITIONALCHARGESTATUS;


                        DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(offlinemodel);
                        Session["TransactionDetails"] = mUIModel;
                        mFactory.SaveOfflinePaymentTransactionDetails(mUIModel, DiscountCode, DiscountAmount);
                        ViewBag.status = "Offline Success";

                        model.EntityList = miUIModel.EntityList;
                        model.ReceiptNetAmount = miUIModel.ReceiptNetAmount;
                        miUIModel.UserId = model.UserId;

                        UserDBFactory UD = new UserDBFactory();
                        UserEntity UE = UD.GetUserFirstAndLastName(model.UserId);

                        try
                        {
                            if (model.PaymentType.ToLower() == "pay at branch")
                            {
                                try
                                {
                                    SendMailForSuccessPayAtBranch(miUIModel, "Pay at branch", mUIModels, model, branchName + "" + branchAddress, Convert.ToInt32(mUIModel.JOBNUMBER), "Additional Charge Payment", UE.FIRSTNAME, Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT));
                                }
                                catch (Exception exbranch)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbranch.Message.ToString(), DateTime.Now.ToString()));
                                    throw new ShipaExceptions("Exception while sending mail to user-Offline(Pay at branch with ADDITIONALCHARGEAMOUNT-Mobile) " + exbranch.ToString());
                                }
                                //SendMailWithCard(miUIModel.UserId, "", "Pay at branch", miUIModel.ADDITIONALCHARGEAMOUNT.ToString("N2"), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "", "Offline");
                                try
                                {
                                    SendMailForCountryFinanceCashier("cashier", model, branchName + "" + branchAddress, "Additional Charge Payment", UE.FIRSTNAME, Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT));
                                }
                                catch (Exception excashier)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", excashier.Message.ToString(), DateTime.Now.ToString()));
                                    throw new ShipaExceptions("Exception while sending mail to cashier-Offline(Pay at branch with ADDITIONALCHARGEAMOUNT-Mobile) " + excashier.ToString());
                                }
                            }
                            else
                            {
                                //SendMailForSuccessPayAtBranch(miUIModel, "Wire transfer", mUIModels, Convert.ToInt32(mUIModel.JOBNUMBER), UE.FIRSTNAME, miUIModel.ADDITIONALCHARGEAMOUNT.ToString("N2"));
                                try
                                {
                                    SendMailWithCard(miUIModel.UserId, "", "Wire transfer", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Offline");
                                }
                                catch (Exception exWire)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exWire.Message.ToString(), DateTime.Now.ToString()));
                                    throw new ShipaExceptions("Exception while sending mail to user-Offline(Wire transfer with ADDITIONALCHARGEAMOUNT-Mobile) " + exWire.ToString());
                                }
                                try
                                {
                                    SendMailForCountryFinanceCashier("finance", model, "", "Additional Charge Payment", UE.FIRSTNAME, Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT));
                                }
                                catch (Exception exfinance)
                                {
                                    Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfinance.Message.ToString(), DateTime.Now.ToString()));
                                    throw new ShipaExceptions("Exception while sending mail to finance-Offline(Wire transfer with ADDITIONALCHARGEAMOUNT-Mobile) " + exfinance.ToString());
                                }
                            }
                        }
                        catch { }
                        //SendMailtoGSSC("Offline", model.OPjobnumber, model.PaymentType, id, miUIModel.UserId);
                        try
                        {
                            byte[] paymentpdfdata = PaymentTransaction(Convert.ToInt32(model.JobNumber), Convert.ToInt32(model.QuotationId), model.UserId, miUIModel.CHARGESETID.ToString());
                            Guid obj = Guid.NewGuid();
                            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                            mFactory.SaveOfflinePaymentBookingSummaryDoc(paymentpdfdata, Convert.ToInt32(model.JobNumber), Convert.ToInt32(model.QuotationId), model.UserId, model.OPjobnumber, obj.ToString(), EnvironmentName, miUIModel.CHARGESETID.ToString());
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while generating PDF " + ex.ToString());
                        }
                        e.USERID = model.UserId;
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.CURRENCY = miUIModel.Currency;
                        e.RECEIPTNETAMOUNT = Convert.ToDouble(model.ReceiptNetAmount);
                        if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                        {
                            return RedirectToAction("MOfflinePBSuccess", "Payment", e);
                        }
                        else
                        {
                            return RedirectToAction("MOfflineWTSuccess", "Payment", e);
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.status = "Offline Fail";
                        mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                        miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                        try
                        {
                            SendMailWithCard(model.UserId, "", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Offline");
                        }
                        catch (Exception exfail)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending Payment Failed mail-Offline with ADDITIONALCHARGEAMOUNT-Mobile " + exfail.ToString());
                        }
                        e.USERID = model.UserId;
                        e.JOBNUMBER = model.JobNumber;
                        e.QUOTATIONID = model.QuotationId;
                        e.CURRENCY = miUIModel.Currency;
                        e.RECEIPTNETAMOUNT = Convert.ToDouble(model.ReceiptNetAmount);
                        if (model.PaymentType.ToLowerInvariant() == "pay at branch")
                        {
                            return RedirectToAction("MOfflinePBFailure", "Payment", e);
                        }
                        else
                        {
                            return RedirectToAction("MOfflineWTFailure", "Payment", e);
                        }
                    }
                }
                else
                {
                    ViewBag.status = "paymentdone-Offline";
                    mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                    miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    return View("MCharge", miUIModel);
                }
            }
        }

        [HttpPost]
        public ActionResult MCreditBusinessPayment(PaymentModel model)
        {
            MPaymentResult e = new MPaymentResult();
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(model.JobNumber));
            if (PaymentStatus.ToLower() == "notexist")
            {
                UserDBFactory UserFactory = new UserDBFactory();
                string str = string.Empty;
                string DiscountCode = "";
                decimal DiscountAmount = 0;
                if (Request.Form["txtCreditTransmode"] != "")
                {
                    model.TransactionMode = Convert.ToString(Request.Form["txtCreditTransmode"]);
                }
                if (Request.Form["txtBCCouponCode"] != "")
                {
                    DiscountCode = Request.Form["txtBCCouponCode"];
                }
                else
                {
                    DiscountCode = "";
                }
                if (Request.Form["txtBCDiscountAmount"] != "")
                {
                    DiscountAmount = Convert.ToDecimal(Request.Form["txtBCDiscountAmount"]);
                }
                PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
                try
                {
                    BusinessCreditmodel.RECEIPTHDRID = model.RECEIPTHDRID;
                    BusinessCreditmodel.JOBNUMBER = model.JobNumber;
                    BusinessCreditmodel.QUOTATIONID = model.QuotationId;
                    BusinessCreditmodel.PAYMENTOPTION = model.PaymentOption;
                    BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                    BusinessCreditmodel.PAYMENTREFNUMBER = "Credit";
                    BusinessCreditmodel.PAYMENTREFMESSAGE = "approved";
                    BusinessCreditmodel.CREATEDBY = model.UserId;
                    BusinessCreditmodel.MODIFIEDBY = model.UserId;
                    BusinessCreditmodel.PAYMENTTYPE = "Business credit";
                    BusinessCreditmodel.TRANSMODE = model.TransactionMode;
                    BusinessCreditmodel.TRANSDESCRIPTION = "Business Credit";
                    BusinessCreditmodel.TRANSAMOUNT = model.Amount;
                    BusinessCreditmodel.TRANSCURRENCY = model.Currency;

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    mFactory.SaveBusinessCreditTransactionDetails(mUIModel, model.UserId, DiscountCode, DiscountAmount);
                    ViewBag.status = "Credit Success";
                    CheckIsMandatoryDocsUploaded(mUIModel.JOBNUMBER.ToString(), model.UserId, mUIModel.QUOTATIONID, model.OPjobnumber);
                    int id = Convert.ToInt32(model.JobNumber);
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = model.UserId;
                    byte[] bytes = null;
                    DBFactory.Entities.QuotationEntity quotationentity = new QuotationEntity();
                    UserDBFactory UD = new UserDBFactory();
                    UserEntity UE = UD.GetUserFirstAndLastName(model.UserId);

                    var OneSignalService = new Push.OneSignal();

                    try
                    {
                        QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                        int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(model.QuotationId));
                        if (count > 0)
                        {
                            quotationentity = mQuoFactory.GetById(Convert.ToInt32(model.QuotationId));
                            mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);

                            //SendMailForSuccessBusinessCredit(miUIModel);
                            //SendMailtoGSSC("Business Credit", model.OPjobnumber, string.Empty, id);
                            str = UserFactory.NewNotification(5113, "Payment Completed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), model.UserId);

                            var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = HttpContext.User.Identity.Name };
                            SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", model.UserId.ToLowerInvariant(), "Payment Completed", pushdata); });


                            //---------------------- Added By Anil G-------------------------------
                            GenerateBookingConfirmationPDF(Convert.ToInt32(model.JobNumber), "Booking Confirmation", Convert.ToInt64(model.QuotationId), model.OPjobnumber, model.UserId);
                            bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(model.JobNumber), "Advance Receipt", Convert.ToInt64(model.QuotationId), model.OPjobnumber, model.UserId, 0);
                            //--------------------------------------------------------------------------------
                        }
                    }
                    catch (Exception ex1)
                    {
                        e.USERID = model.UserId;
                        e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                        e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                        e.JOBNUMBER = model.JobNumber;
                        return RedirectToAction("MBusinessCreditSuccess", "Payment", e);
                    }

                    bytes = bytes != null ? bytes : null;
                    try
                    {
                        //SendMailForSuccessBusinessCredit(miUIModel, bytes, mUIModels, UE.FIRSTNAME);
                        SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Business credit");
                    }
                    catch (Exception exbc)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbc.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending mail to user-Business Credit(Mobile) " + exbc.ToString());
                    }
                    try
                    {
                        //SendMailtoGroup(model.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC", "Paymentstatus", Convert.ToInt32(model.JobNumber), model.UserId, "Business Credit", string.Empty);
                        try
                        {
                            SendMailtoGroup(model.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC", "Paymentstatus", Convert.ToInt32(model.JobNumber), miUIModel.UserId, "Business Credit", "Payment", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, quotationentity, mDBEntity);
                            //string CEEmail = string.Empty;
                            //QuotationDBFactory mQuotationDBFactory = new QuotationDBFactory();
                            //CEEmail = string.IsNullOrEmpty(mUIModels.CE) ? UE.CRMEmail : mUIModels.CE;
                            //if (string.IsNullOrEmpty(CEEmail))
                            //{
                            //    string CE = mQuotationDBFactory.UpdateCEForQuotation(mUIModels.QuotationId);
                            //    CEEmail = CE;
                            //}
                            SendThankingMailtoCustomer("cet@shipafreight.com", miUIModel.UserId, miUIModel.OPjobnumber);
                        }
                        catch (Exception exops)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exops.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to COUNTRYJOBOPS,COUNTRYFINANCE,GSSC-Business Credit(Mobile) " + exops.ToString());
                        }
                    }
                    catch (Exception ex3)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex3.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending mail to GSSC or Operation/Finance " + ex3.ToString());
                    }
                    e.USERID = model.UserId;
                    e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                    e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                    e.JOBNUMBER = model.JobNumber;
                    return RedirectToAction("MBusinessCreditSuccess", "Payment", e);
                }
                catch (Exception ex)
                {
                    ViewBag.status = "Credit Fail";
                    int id = Convert.ToInt32(model.JobNumber);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    var OneSignalService = new Push.OneSignal();
                    DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = model.UserId;
                    if (str != "Success")
                    {
                        try
                        {
                            SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(model.ReceiptNetAmount), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment", "", "Business credit");

                        }
                        catch (Exception exbc)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exbc.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending Payment failed mail-Business Credit(Mobile) " + exbc.ToString());
                        }
                        str = UserFactory.NewNotification(5113, "Payment failed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), model.UserId);
                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment failed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = HttpContext.User.Identity.Name };
                        SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", miUIModel.UserId.ToLowerInvariant(), "Payment failed", pushdata); });
                    }
                    e.USERID = model.UserId;
                    e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                    e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                    e.JOBNUMBER = model.JobNumber;
                    return RedirectToAction("MBusinessCreditFailure", "Payment", e);
                }
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                var OneSignalService = new Push.OneSignal();
                int id = Convert.ToInt32(model.JobNumber);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = model.UserId;
                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    UserDBFactory UserFactory = new UserDBFactory();
                    string str = string.Empty;
                    string DiscountCode = "";
                    decimal DiscountAmount = 0;
                    if (Request.Form["txtCreditTransmode"] != "")
                    {
                        model.TransactionMode = Convert.ToString(Request.Form["txtCreditTransmode"]);
                    }
                    if (Request.Form["txtBCCouponCode"] != "")
                    {
                        DiscountCode = Request.Form["txtBCCouponCode"];
                    }
                    else
                    {
                        DiscountCode = "";
                    }
                    if (Request.Form["txtBCDiscountAmount"] != "")
                    {
                        DiscountAmount = Convert.ToDecimal(Request.Form["txtBCDiscountAmount"]);
                    }
                    PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
                    try
                    {
                        BusinessCreditmodel.RECEIPTHDRID = model.RECEIPTHDRID;
                        BusinessCreditmodel.JOBNUMBER = model.JobNumber;
                        BusinessCreditmodel.QUOTATIONID = model.QuotationId;
                        BusinessCreditmodel.PAYMENTOPTION = model.PaymentOption;
                        BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                        BusinessCreditmodel.PAYMENTREFNUMBER = "Credit";
                        BusinessCreditmodel.PAYMENTREFMESSAGE = "approved";
                        BusinessCreditmodel.CREATEDBY = model.UserId;
                        BusinessCreditmodel.MODIFIEDBY = model.UserId;
                        BusinessCreditmodel.PAYMENTTYPE = "Business credit";
                        BusinessCreditmodel.TRANSMODE = model.TransactionMode;
                        BusinessCreditmodel.TRANSDESCRIPTION = "Business Credit";
                        BusinessCreditmodel.TRANSAMOUNT = model.Amount;
                        BusinessCreditmodel.TRANSCURRENCY = model.Currency;
                        BusinessCreditmodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                        BusinessCreditmodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                        BusinessCreditmodel.ADDITIONALCHARGESTATUS = (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now") ? (BusinessCreditmodel.ISPAYMENTSUCESS == 1) ? "Paid" : "Rejected" : "";
                        DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);
                        mFactory.SaveBusinessCreditTransactionDetails(mUIModel, model.UserId, DiscountCode, DiscountAmount);
                        ViewBag.status = "Credit Success";
                        byte[] bytes = null;
                        str = UserFactory.NewNotification(5113, "Payment Completed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), model.UserId);
                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = model.UserId };
                        SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", model.UserId, "Payment Completed", pushdata); });

                        DBFactory.Entities.QuotationEntity quotationentity = new QuotationEntity();
                        UserDBFactory UD = new UserDBFactory();
                        UserEntity UE = UD.GetUserFirstAndLastName(model.UserId);

                        try
                        {
                            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(model.QuotationId));
                            if (count > 0)
                            {
                                quotationentity = mQuoFactory.GetById(Convert.ToInt32(model.QuotationId));
                                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
                                bytes = GenerateAdvanceReceiptPDF(Convert.ToInt32(model.JobNumber), "Advance Receipt", Convert.ToInt64(model.QuotationId), model.OPjobnumber, model.UserId, Convert.ToInt32(miUIModel.CHARGESETID));
                            }
                        }
                        catch (Exception ex1)
                        {
                            e.USERID = model.UserId;
                            e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                            e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                            e.JOBNUMBER = model.JobNumber;
                            return RedirectToAction("MBusinessCreditSuccess", "Payment", e);
                        }

                        bytes = bytes != null ? bytes : null;
                        try
                        {
                            //SendMailForSuccessBusinessCredit(miUIModel, bytes, mUIModels,UE.FIRSTNAME, miUIModel.ADDITIONALCHARGEAMOUNT.ToString("N2"));
                            SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Business credit");
                        }
                        catch (Exception ex2)
                        {
                            Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex2.Message.ToString(), DateTime.Now.ToString()));
                            throw new ShipaExceptions("Exception while sending mail to user-Business Credit-Mobile(ADDITIONALCHARGEAMOUNT) " + ex2.ToString());
                        }
                        try
                        {
                            //Sending mail to Operations and finance.                            
                            //SendMailtoGroup(model.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE", "Paymentstatus", Convert.ToInt32(model.JobNumber), miUIModel.UserId, "Business Credit", string.Empty);
                            try
                            {
                                SendMailtoGroup(model.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE", "Paymentstatus", Convert.ToInt32(model.JobNumber), miUIModel.UserId, "Business Credit", "Additional Charge Payment", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, quotationentity, mDBEntity);
                                //string CEEmail = string.Empty;
                                //QuotationDBFactory mQuotationDBFactory = new QuotationDBFactory();
                                //CEEmail = string.IsNullOrEmpty(mUIModels.CE) ? UE.CRMEmail : mUIModels.CE;
                                //if (string.IsNullOrEmpty(CEEmail))
                                //{
                                //    string CE = mQuotationDBFactory.UpdateCEForQuotation(mUIModels.QuotationId);
                                //    CEEmail = CE;
                                //}
                                SendThankingMailtoCustomer("cet@shipafreight.com", miUIModel.UserId, miUIModel.OPjobnumber);
                            }
                            catch (Exception exops)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exops.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending mail to COUNTRYJOBOPS,COUNTRYFINANCE-Business Credit-Mobile(ADDITIONALCHARGEAMOUNT) " + exops.ToString());
                            }
                        }
                        catch (Exception ex1)
                        {
                            //
                        }
                        e.USERID = model.UserId;
                        e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                        e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                        e.JOBNUMBER = model.JobNumber;
                        return RedirectToAction("MBusinessCreditSuccess", "Payment", e);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.status = "Credit Fail";
                        int idd = Convert.ToInt32(model.JobNumber);
                        mDBEntity = mFactory.GetPaymentDetailsByJobNumber(idd, model.UserId);
                        miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                        miUIModel.UserId = model.UserId;
                        if (str != "Success")
                        {
                            try
                            {
                                SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Payment Failed", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId, mUIModels, mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Additional Charge Payment", "", "Business credit");
                            }
                            catch (Exception exfail)
                            {
                                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", exfail.Message.ToString(), DateTime.Now.ToString()));
                                throw new ShipaExceptions("Exception while sending Payment failed mail-Business Credit-Mobile(ADDITIONALCHARGEAMOUNT) " + exfail.ToString());
                            }
                            UserFactory.NewNotification(5113, "Payment failed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), model.UserId);
                            var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment failed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = model.UserId };
                            SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", model.UserId, "Payment failed", pushdata); });
                        }
                        e.USERID = model.UserId;
                        e.RECEIPTNETAMOUNT = BusinessCreditmodel.TRANSAMOUNT;
                        e.CURRENCY = BusinessCreditmodel.TRANSCURRENCY;
                        e.JOBNUMBER = model.JobNumber;
                        return RedirectToAction("MBusinessCreditFailure", "Payment", e);
                    }
                }
                else
                {
                    ViewBag.status = "paymentdone-Business Credit";
                    mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, model.UserId);
                    miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    return View("MCharge", miUIModel);
                }
            }
        }

        public ActionResult PSuccess(string mode, string status, string token, string data, string user)
        {
            MPaymentResult model = new MPaymentResult();
            model.MODE = mode;
            model.STATUS = status;
            model.TOKEN = token;
            model.DATA = data;
            model.USERID = user;
            return View("PSuccess", model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Quote2Job(JobBookingEntity jobbooking)
        {
            return View();
        }

        [Authorize]
        public async Task<ActionResult> JobBookingList(string list, string status, string SearchString = "", int PageNo = 1, string Order = "DESC")
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            ViewBag.SearchString = SearchString;
            SearchString = SearchString.Replace("'", "''");
            ViewBag.PageNo = PageNo;
            if (list == null || list == "")
                list = "JobList";

            if (status == null || status == "")
                status = "INITIATED";
            ViewBag.jStatus = status;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            ViewBag.Order = Order;
            List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetSavedJobs(HttpContext.User.Identity.Name, status, list, PageNo, SearchString, Order);
            if (uiCulture != "en")
            {
                await Task.Run(() => LocalizedConverter.GetConversionQuotationListByLanguage<QuoteJobListEntity>(uiCulture, mDBEntity)).ConfigureAwait(false);
            }
            List<QuoteJobListModel> mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteJobListEntity>, List<QuoteJobListModel>>(mDBEntity);
            DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetJobStatusCount(HttpContext.User.Identity.Name, SearchString);
            ViewBag.QuoteJobStatusCount = mQuoteStatusEntity;
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            //for canonical tag      
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "JobBooking/JobBookingList";
            return View("JobBookingList", mUIModel);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> SaveJob(JobBookingModel model, JobBookingHidden HiddenItem)
        {
            try
            {
                if ((HiddenItem.SHCLIENTID != HiddenItem.CONCLIENTID))
                {
                    string oPNumber = model.JOPERATIONALJOBNUMBER;
                    long mQuotationId;

                    long id = model.QUOTATIONID;
                    model.SHCLIENTID = HiddenItem.SHCLIENTID;
                    model.CONCLIENTID = HiddenItem.CONCLIENTID;
                    model.JINCOTERMDESCRIPTION = HiddenItem.JINCOTERMDESCRIPTION;
                    model.JINCOTERMCODE = HiddenItem.JINCOTERMCODE;

                    //As MVC form submission changed to ajax call, these list has to be null
                    model.CargoAvabilityModel = null;
                    model.DocumentDetails = null;
                    model.HSCodeInsmodel = null;
                    model.HSCodeDutymodel = null;
                    model.JobItems = null;
                    model.PartiesModel = null;
                    model.PartyItems = null;
                    model.PaymentCharges = null;
                    model.JobShipmentItems = null;
                    model.ReceiptHeaderDetails = null;

                    //Oversize cargo start ************ Oversize cargo start//
                    if (!ReferenceEquals(model.ShipmentItems, null) && model.ShipmentItems.Any())
                    {
                        foreach (var item in model.ShipmentItems)
                        {
                            if (!string.IsNullOrEmpty(item.PackageTypeName) && (item.ItemLength != 0) && (item.ItemWidth != 0))
                            {
                                if (item.ItemHeight == 0)
                                    item.ItemHeight = 227;
                            }
                            if (item.ItemHeight > 0)
                            {
                                if (string.IsNullOrEmpty(item.PackageTypeName) || (item.ItemLength == 0) || (item.ItemWidth == 0))
                                    item.ItemHeight = 0;
                            }
                        }
                    }
                    //Oversize cargo end ************ Oversize cargo end//


                    //string OrgHSCode = Request.Form["hdnHSCode"] == null ? "" : Request.Form["hdnHSCode"].ToString();
                    //string DestHSCode = Request.Form["hdnHSCodeDest"] == null ? "" : Request.Form["hdnHSCodeDest"].ToString();
                    string HSCode = HiddenItem.HSCode;// +"$" + DestHSCode;
                    string DestHSCode = HiddenItem.DesHSCode;
                    DBFactory.Entities.JobBookingEntity mUIModel = Mapper.Map<JobBookingModel, DBFactory.Entities.JobBookingEntity>(model);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    mUIModel.QUOTATIONID = id;
                    if (mUIModel.SHPARTYDETAILSID > 0 && mUIModel.CONPARTYDETAILSID > 0)
                    {
                        if (!string.IsNullOrEmpty(HiddenItem.JCARGODELIVERYBY.ToString()))
                            mUIModel.JCARGODELIVERYBY = HiddenItem.JCARGODELIVERYBY;
                        if (!string.IsNullOrEmpty(HiddenItem.JCARGOAVAILABLEFROM.ToString()))
                            mUIModel.JCARGOAVAILABLEFROM = HiddenItem.JCARGOAVAILABLEFROM;

                        mQuotationId = mFactory.UpdateJob(mUIModel, HttpContext.User.Identity.Name, HiddenItem.HDNCONTINETOPAYMENT, HSCode, DestHSCode, HiddenItem.TXTFIELDSVALUE, HiddenItem.TXTFIELDSNAME);

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(HiddenItem.JCARGODELIVERYBY.ToString()))
                            mUIModel.JCARGODELIVERYBY = HiddenItem.JCARGODELIVERYBY;
                        if (!string.IsNullOrEmpty(HiddenItem.JCARGOAVAILABLEFROM.ToString()))
                            mUIModel.JCARGOAVAILABLEFROM = HiddenItem.JCARGOAVAILABLEFROM;
                        mFactory.SaveJob(mUIModel, HttpContext.User.Identity.Name, HiddenItem.HDNCONTINETOPAYMENT, HSCode, DestHSCode, HiddenItem.TXTFIELDSVALUE, HiddenItem.TXTFIELDSNAME);
                    }

                    //--------Send Mail notification if mandatory documents are empty... Anil G------------

                    //if (HiddenItem.MandatoryDocs != "" && HiddenItem.MandatoryDocs != null)
                    //    SendMailForMandatoryDocs(HttpContext.User.Identity.Name.ToString());
                    //-------------------------------------------------------------------------------------

                    if (HiddenItem.SUBMITTOCUSTOMER == "SUBMITTOCUSTOMER")
                    {
                        mFactory.SubmitToCustomer(mUIModel.JJOBNUMBER, HiddenItem.PARTY.ToLower());
                        SendSubmittedToCustomer(HiddenItem.CREATEDBY, HiddenItem.PARTY, oPNumber);
                        return Json("SUBMITTOCUSTOMER", JsonRequestBehavior.AllowGet);
                    }

                    if (HiddenItem.HDNCONTINETOPAYMENT == "SAVEJOB")
                        return Json("SAVEJOB", JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("QuoteJobList", "Quotation", new { list = "JobList" });
                    else
                    {
                        //Generatequotepdf(mUIModel.QUOTATIONID, mUIModel.JJOBNUMBER);
                        Session["QUOTATIONID"] = mUIModel.QUOTATIONID;
                        Session["JJOBNUMBER"] = mUIModel.JJOBNUMBER;
                        return Json("ContPayment", JsonRequestBehavior.AllowGet);
                        // return RedirectToAction("BookingSummary");
                    }
                }
                else
                {
                    return Json("dataAltered", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public async Task<int> SendBookDatatoSalesForce(string transcationType, string partnerId, string UserHostAddress, JobBookingModel mUIModel, JobBookingEntity jobBookingEntity)
        {
            try
            {
                SalesForceEntity salesForceEntity = new SalesForceEntity();
                HeaderSection objHeaderDetails = new HeaderSection();
                BookingDetails objBookingDetails = new BookingDetails();
                Details objDetails = new Details();
                Body objBody = new Body();
                string TransactionType = transcationType;
                string PartnerID = partnerId;
                if (TransactionType == "BookNow-RegisterUser")
                {
                    objBookingDetails = await Task.Run(() => SalesForceIntegration.BookingDetailsMapping(mUIModel, UserHostAddress)).ConfigureAwait(false);
                }
                else
                {
                    objBookingDetails = await Task.Run(() => SalesForceIntegration.PayementDetailsMapping(jobBookingEntity, UserHostAddress)).ConfigureAwait(false);
                }

                objHeaderDetails = await Task.Run(() => SalesForceIntegration.HeaderDetailsMapping(TransactionType, PartnerID)).ConfigureAwait(false);
                salesForceEntity.HeaderSection = objHeaderDetails;
                objDetails.UserDetails = null;
                objDetails.QuotationDetails = null;
                objDetails.BookingDetails = objBookingDetails;
                objBody.Details = objDetails;
                salesForceEntity.Body = objBody;
                string targetUrl = ApiUrl + "api/salesforce/PushDatatoMessageQueue";
                await Task.Run(() => GetDataFromApi<SalesForceEntity>(targetUrl, "POST", salesForceEntity)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
        public bool CheckDateValues(JobBookingModel modeldata, DateTime startdate, DateTime enddate)
        {
            bool datevalid = true;
            List<int> Destinationweefofflst = new List<int>();
            List<int> Originweefofflst = new List<int>();
            var originCountryCodeWeekoff = "";
            var destinationCountryCodeWeekoff = "";
            var originCountryCode = modeldata.OCOUNTRYID;
            var destinationCountryCode = modeldata.DCOUNTRYID;
            var productType = modeldata.PRODUCTNAME;
            List<GetHolidays> originHolidayList = new List<GetHolidays>();
            List<GetHolidays> destinationHolidayList = new List<GetHolidays>();
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                DBFactory.Entities.CargoAvabilityEntity CargoDate = JBFactory.GetPickUpTransitDates(Convert.ToInt64(modeldata.OCOUNTRYID));
                modeldata.CargoAvabilityModel = Mapper.Map<CargoAvabilityEntity, CargoAvabilityModel>(CargoDate);
                int transitDays = transitTimeCalculation(modeldata.TRANSITTIME, productType, modeldata.CargoAvabilityModel.OCEANDELIVERY, modeldata.CargoAvabilityModel.AIRDELIVERY);
                string pickupDays = productType.ToLower() == "ocean" ? modeldata.CargoAvabilityModel.OCEANAVAILABILITY : modeldata.CargoAvabilityModel.AIRAVAILABILITY;

                if (pickupDays.ToString() == "0")
                {
                    pickupDays = "5";
                }
                if (transitDays.ToString() == "0")
                {
                    transitDays = productType.ToLower() == "ocean" ? 10 : 5;
                }
                IList<GetCountryWeekends> weekOff = JBFactory.GetCountryWeekends(Convert.ToInt64(originCountryCode), Convert.ToInt64(destinationCountryCode));
                foreach (var item in weekOff)
                {
                    if (item.COUNTRYID == originCountryCode)
                    {
                        originCountryCodeWeekoff = Convert.ToString(item.WEEKOFF);
                        if (originCountryCodeWeekoff == "10000000")
                        {
                            originCountryCodeWeekoff = "10000011";
                        }
                        Originweefofflst = getoriginWeekoff(originCountryCodeWeekoff);

                    }
                    if (item.COUNTRYID == destinationCountryCode)
                    {
                        destinationCountryCodeWeekoff = Convert.ToString(item.WEEKOFF);
                        if (destinationCountryCodeWeekoff == "10000000")
                        {
                            destinationCountryCodeWeekoff = "10000011";
                        }
                        Destinationweefofflst = getDestinationWeekoff(destinationCountryCodeWeekoff);

                    }
                }
                DataSet ds = JBFactory.GetCountryHolidays(Convert.ToInt64(originCountryCode), Convert.ToInt64(destinationCountryCode));
                List<GetHolidays> holiday = new List<GetHolidays>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    GetHolidays obj = new GetHolidays();
                    obj.COUNTRYID = Convert.ToInt64(row["COUNTRYID"]);
                    obj.HOLIDAYDATE = Convert.ToDateTime(row["HOLIDAYDATE"]);
                    holiday.Add(obj);
                }
                foreach (var holid in holiday)
                {
                    if (holid.COUNTRYID == originCountryCode)
                    {
                        GetHolidays gholi = new GetHolidays();
                        if (holid.HOLIDAYDATE != null)
                        {
                            gholi.COUNTRYID = holid.COUNTRYID;
                            gholi.HOLIDAYDATE = holid.HOLIDAYDATE;
                            originHolidayList.Add(gholi);
                        }
                    }
                    if (holid.COUNTRYID == destinationCountryCode)
                    {
                        GetHolidays dholi = new GetHolidays();
                        if (holid.HOLIDAYDATE != null)
                        {
                            dholi.COUNTRYID = holid.COUNTRYID;
                            dholi.HOLIDAYDATE = holid.HOLIDAYDATE;
                            destinationHolidayList.Add(dholi);
                        }
                    }
                }

                int noOfDaystoDisable_CAW = int.Parse(pickupDays);
                DateTime cargoDeliverTODate;
                DateTime today = DateTime.Now;
                DateTime CargoAvailablefromDate;
                if (modeldata.JCARGOAVAILABLEFROM.ToString() == "1/1/0001 12:00:00 AM")
                {
                    CargoAvailablefromDate = today.AddDays(noOfDaystoDisable_CAW);
                }
                else
                {
                    CargoAvailablefromDate = modeldata.JCARGOAVAILABLEFROM;
                }
                while (Originweefofflst.FindIndex(p => p.Equals((int)CargoAvailablefromDate.DayOfWeek)) > -1 || (originHolidayList.FindIndex(r => r.HOLIDAYDATE.Date == CargoAvailablefromDate.Date)) > -1)
                {
                    CargoAvailablefromDate = CargoAvailablefromDate.AddDays(1);

                }
                DateTime CargoAvailableEnd = modeldata.DATEOFVALIDITY;
                CargoAvailableEnd = CargoAvailableEnd.AddDays(noOfDaystoDisable_CAW);
                while (Originweefofflst.FindIndex(p => p.Equals((int)CargoAvailableEnd.DayOfWeek)) > -1 || (originHolidayList.FindIndex(r => r.HOLIDAYDATE.Date == CargoAvailableEnd.Date)) > -1)
                {
                    CargoAvailableEnd = CargoAvailableEnd.AddDays(1);
                }
                // Assigning Date value to cargo available from  control.
                cargoDeliverTODate = CargoAvailablefromDate.AddDays(transitDays - 1);
                while (Destinationweefofflst.FindIndex(p => p.Equals((int)cargoDeliverTODate.DayOfWeek)) > -1 || (destinationHolidayList.FindIndex(r => r.HOLIDAYDATE.Date == cargoDeliverTODate.Date)) > -1)
                {
                    cargoDeliverTODate = cargoDeliverTODate.AddDays(1);
                }
                if ((startdate >= CargoAvailablefromDate.Date && startdate <= CargoAvailableEnd.Date) && enddate >= cargoDeliverTODate.Date)
                {
                    if (Originweefofflst.FindIndex(p => p.Equals((int)startdate.DayOfWeek)) > -1 || originHolidayList.FindIndex(r => r.HOLIDAYDATE.Date == startdate.Date) > -1)
                    {
                        datevalid = false;
                        return datevalid;
                    }
                    if (Destinationweefofflst.FindIndex(p => p.Equals((int)enddate.DayOfWeek)) > -1 || (destinationHolidayList.FindIndex(r => r.HOLIDAYDATE.Date == enddate.Date) > -1))
                    {
                        datevalid = false;
                        return datevalid;
                    }
                    datevalid = true;
                }
                else
                {
                    datevalid = false;
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
            return datevalid;
        }

        public int transitTimeCalculation(string transititme, string producttype, string oceandelivery, string airdelivery)
        {
            dynamic transitDays = "";
            try
            {
                string Tdays = "";
                string Thours = "";
                if (transititme != null && (transititme.ToUpper() != "NO TRANSMIT TIME AVAILABLE") && transititme != "")
                {
                    transitDays = transititme.ToUpper();
                    transitDays = transitDays.Split(' ');
                    for (var i = 0; i < transitDays.Length; i++)
                    {
                        if (transitDays[i].ToUpper().IndexOf("DAY") != -1)// Need to pass only days.
                        {
                            Tdays = (Math.Ceiling(Convert.ToDecimal(transitDays[i - 1]))).ToString();
                        }
                        if (transitDays[i].ToUpper().IndexOf("HOUR") != -1)// Need to pass only hours.
                        {
                            double t = Convert.ToInt32(transitDays[i - 1]) / 24.0;
                            Thours = (Math.Ceiling(t)).ToString();
                        }
                    }
                    if ((transititme.ToUpper().IndexOf("DAY") != -1) && (transititme.ToUpper().IndexOf("HOUR") != -1)) //Transit time available in both Day and hours.
                    {
                        int ttotal = Convert.ToInt32(Tdays) + Convert.ToInt32(Thours);
                        transitDays = Math.Ceiling(Convert.ToDecimal(ttotal));
                    }
                    else if (transititme.ToUpper().IndexOf("HOUR") != -1)//Transit time available in hours.
                    {
                        transitDays = Math.Ceiling(Convert.ToDecimal(Thours));// Need to pass only hours.
                    }
                    else if (transititme.ToUpper().IndexOf("DAY") != -1)//Transit time available in Day.
                    {
                        transitDays = Math.Ceiling(Convert.ToDecimal(Tdays));// Need to pass only days.
                    }
                }
                else
                {
                    if (producttype.ToLower() == "ocean")
                    {
                        transitDays = oceandelivery;
                    }
                    else
                    {
                        transitDays = airdelivery;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
            return Convert.ToInt32(transitDays);
        }

        public List<int> getoriginWeekoff(string originCountryCodeWeekoff)
        {
            List<int> Originweefofflst = new List<int>();
            string originCountryOccurance = originCountryCodeWeekoff;
            if (originCountryOccurance != "")
            {
                for (int i = 1; i < originCountryOccurance.Length; i++)
                {
                    char first = originCountryOccurance[i];
                    if (first.ToString() == "1")
                    {
                        int q = 0;
                        if ((i + 1) == 8)
                        {
                            q = 0;
                        }
                        else
                        {
                            q = i;
                        }
                        Originweefofflst.Add(q);

                    }
                }
            }
            return Originweefofflst;
        }

        public List<int> getDestinationWeekoff(string destinationCountryCodeWeekoff)
        {

            List<int> Destinationweefofflst = new List<int>();
            string destinationCountryOccurance = destinationCountryCodeWeekoff;
            if (destinationCountryOccurance != "")
            {
                for (int i = 1; i < destinationCountryOccurance.Length; i++)
                {
                    char first = destinationCountryOccurance[i];
                    if (first.ToString() == "1")
                    {
                        int r = 0;
                        if ((i + 1) == 8)
                        {
                            r = 0;
                        }
                        else
                        {
                            r = i;
                        }
                        Destinationweefofflst.Add(r);


                    }
                }
            }
            return Destinationweefofflst;
        }

        private void SendMailForMandatoryDocs(string Email, string jobNumber, long quotationid, string BookingID)
        {
            string resetLink = string.Empty;
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Mandatory documents are needed before goods availability to Agility" + EnvironmentName;
            string mapPath = Server.MapPath("~/App_Data/MandatoryDocs.html");
            string body = string.Empty;
            string url = "JobNumber=" + jobNumber + "?id=" + quotationid;
            JsonResult t1 = EncryptId(url);
            string reqdocument = t1.Data.ToString();
            resetLink = Actionlink + "JobBooking/BookingSummary?" + reqdocument;
            UserDBFactory UD = new UserDBFactory();
            UserEntity UE = UD.GetUserFirstAndLastName(Email);
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{Message}", "<p>You have not uploaded the mandatory documents needed to process your shipment.</p>");
            body = body.Replace("{Message1}", "<p>You need to upload these before the pickup date, or this might result in delay of pickup and the shipment.</p>");
            body = body.Replace("{user}", UE.FIRSTNAME);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{btnName}", "Upload document Now");
            body = body.Replace("{documentreq}", resetLink);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
          "JobBooking", "Mandatory Documents", BookingID, Email, body, "", true, message.Subject.ToString());
        }

        public void DownloadTemplate(string path)
        {
            string DocPath = System.Configuration.ConfigurationManager.AppSettings["ReportServerUrl"].ToString();

            var webClient = new WebClient(); byte[] imageBytes = null;

            string URl = DocPath + path.Split('/')[3];
            try
            {
                log.Info(path.Split('/')[3]);
                imageBytes = webClient.DownloadData(URl);
                log.Info("after webclient");
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    string Filename = path.Split('/')[3].Split('.')[0];
                    Response.Clear();
                    Response.ContentType = "application/jpeg";
                    Response.AddHeader("Content-Disposition", "inline; filename=" + path.Split('/')[3]);
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(imageBytes);
                    Response.End();
                    Response.Close();
                    log.InfoFormat(String.Format("Bytes Length: {0}", imageBytes.Length));
                    log.InfoFormat(String.Format("Path: {0}", Filename));
                    log.Info(imageBytes.Length);

                }
                webClient.Dispose();
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
            }
        }

        public ActionResult CheckDownloadTemplate(string Path)
        {
            string DocPath = System.Configuration.ConfigurationManager.AppSettings["ReportServerUrl"].ToString();

            var webClient = new WebClient();

            string URl = DocPath + Path.Split('/')[3];
            try
            {
                webClient.DownloadData(URl);
                webClient.Dispose();
                return Json(new { success = true, result = "" }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                webClient.Dispose();
                return Json(new { success = false, result = ex.ToString() }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        private byte[] GenerateJobConfPDF(string QUOTATIONNUMBER, long JOBNUMBER, long QuotationID, string JOPERATIONALJOBNUMBER, string ReportFileName)
        {
            byte[] bytes = null;
            try
            {
                //-------------------Read Config keys--------------------
                string URl = System.Configuration.ConfigurationManager.AppSettings["ReportServerUrl"].ToString();
                string UserName = System.Configuration.ConfigurationManager.AppSettings["UserName"].ToString();
                string Password = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
                string Domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();
                string FloderName = System.Configuration.ConfigurationManager.AppSettings["FolderName"].ToString();

                using (FOCiS.SSP.Web.UI.Controllers.JobBooking.JobBookingController.Formattingpdf.UserImpersonation user = new FOCiS.SSP.Web.UI.Controllers.JobBooking.JobBookingController.Formattingpdf.UserImpersonation(UserName, Domain, Password))
                {
                    if (user.ImpersonateValidUser())
                    {
                        string ReportName = "";
                        ReportParameter[] param = new ReportParameter[2];
                        if (ReportFileName == "Booking Confirmation")
                        {
                            ReportName = "FX_Booking Confirmation to Customer";
                            param[0] = new ReportParameter("PRM_DOCTYPE", "DRAFT");
                            param[1] = new ReportParameter("PRM_JOBNUMBER", Convert.ToString(JOBNUMBER));
                        }
                        else
                        {
                            ReportName = "FX_AdvanceCashReceipt";
                            param[0] = new ReportParameter("PRM_ISCREDITNOTE", "0");
                            param[1] = new ReportParameter("PRM_JOBNUMBER", Convert.ToString(JOBNUMBER));
                        }
                        ReportViewer rptViewer = new ReportViewer();
                        // ProcessingMode will be Either Remote or Local  
                        rptViewer.ProcessingMode = ProcessingMode.Remote;
                        rptViewer.SizeToReportContent = true;
                        rptViewer.AsyncRendering = true;
                        rptViewer.ServerReport.ReportServerUrl = new Uri(URl);
                        rptViewer.ServerReport.ReportPath = String.Format("/{0}/{1}", FloderName, ReportName);
                        IReportServerCredentials irsc = new CustomReportCredentials(UserName, Password, Domain);
                        rptViewer.ServerReport.ReportServerCredentials = irsc;
                        rptViewer.ServerReport.SetParameters(param);
                        rptViewer.ServerReport.Refresh();

                        bytes = rptViewer.ServerReport.Render("PDF", null);
                        if (bytes.Length > 0)
                        {
                            // MemoryStream stream = new MemoryStream(bytes);

                            //-----------------------Commented Upload to Folder concept--------------------------Anil G
                            //string url = Path.Combine(Server.MapPath("~/CustomerDocs/"), ReportFileName + "_" + JOBNUMBER + "_" + JOPERATIONALJOBNUMBER + ".pdf");
                            ////write to file
                            //FileStream file = new FileStream(url, FileMode.Create, FileAccess.Write);
                            //stream.WriteTo(file);
                            //file.Close();
                            //stream.Close();
                            //---------------------------------------------------------------------------------

                            //Insert same data into db : Anil G
                            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                            sspdoc.QUOTATIONID = Convert.ToInt64(QuotationID);
                            sspdoc.DOCUMNETTYPE = ReportFileName;
                            sspdoc.FILENAME = JOPERATIONALJOBNUMBER + ".pdf";
                            sspdoc.FILEEXTENSION = "application/pdf";
                            sspdoc.FILESIZE = bytes.Length;
                            sspdoc.FILECONTENT = bytes;
                            sspdoc.JOBNUMBER = Convert.ToInt64(JOBNUMBER);
                            sspdoc.DocumentName = ReportFileName;
                            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                            JobBookingDbFactory mjobFactory = new JobBookingDbFactory();
                            Guid obj = Guid.NewGuid();
                            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();

                            mjobFactory.SaveDocuments(docentity, HttpContext.User.Identity.Name, "System Generated", obj.ToString(), EnvironmentName);
                        }
                    }
                    else
                    {
                        QuotationDBFactory mFactory = new QuotationDBFactory();
                        mFactory.InsertErrorLog(HttpContext.User.Identity.Name, "JobBooking", "Payemntwithcard", "docs", "impersination");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
            return bytes;
        }

        private IReportServerCredentials FocisReportServerCredentials()
        {
            throw new NotImplementedException();
        }

        public JsonResult GetPartys(string Countryid)
        {
            try
            {
                JobBookingDbFactory JDF = new JobBookingDbFactory();
                DataTable dt = JDF.GetPartyNames(Countryid, HttpContext.User.Identity.Name.ToString());

                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                var JsonToReturn = new
                {
                    rows = trows
                };

                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult UpdateSaveforlater(string DOCID, string DocupDate, string ProvidedBy, string TMember)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.UpdateSaveforlaterfordocument(DOCID, DocupDate, ProvidedBy, TMember);

                //--------Send Mail notification if mandatory documents are empty... Anil G------------
                //if (TMember == "Upload later all")
                //    SendMailForMandatoryDocs(HttpContext.User.Identity.Name.ToString());
                //-------------------------------------------------------------------------------------
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult ClearUploadLater(string DOCID)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.ClearUploadLater(DOCID);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult GETNEXTFLYINGDATE(string QuotationId, string PickupDate)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                DataTable dt = JBDF.GETNEXTFLYINGDATE(QuotationId, PickupDate);

                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                var JsonToReturn = new
                {
                    rows = trows
                };

                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadDocuments(HttpPostedFileBase uploadedFile, string QuotationId, string DocumentType, string DocumentName, string JobNumber, string Status)
        {
            HttpPostedFileBase theFile = HttpContext.Request.Files["uploadedFile"];
            string FilePath = Path.GetExtension(uploadedFile.FileName).ToLower();
            if (theFile.ContentLength != 0)
            {
                if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                    && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                    && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                {
                    return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                }
                byte[] imageSize = new byte[uploadedFile.ContentLength];
                uploadedFile.InputStream.Read(imageSize, 0, uploadedFile.ContentLength);
                SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                sspdoc.QUOTATIONID = Convert.ToInt64(QuotationId);
                sspdoc.DOCUMNETTYPE = DocumentType;
                sspdoc.FILENAME = uploadedFile.FileName;
                sspdoc.FILEEXTENSION = uploadedFile.ContentType;
                sspdoc.FILESIZE = uploadedFile.ContentLength;
                sspdoc.FILECONTENT = imageSize;
                sspdoc.JOBNUMBER = Convert.ToInt64(JobNumber);
                if (DocumentName == "")
                {
                    sspdoc.DocumentName = uploadedFile.FileName.Split('.')[0];
                }
                else
                    sspdoc.DocumentName = DocumentName;

                DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                JobBookingDbFactory mFactory = new JobBookingDbFactory();

                //-----------------------Commented Upload to Folder concept--------------------------Anil G

                //string Docurl = Path.Combine(Server.MapPath("~/CustomerDocs/"), DocumentType + "_" + JobNumber + "_" + uploadedFile.FileName);

                //if (System.Configuration.ConfigurationManager.AppSettings["IsLocal"].ToString() == "false")
                //{
                //    Docurl = System.Configuration.ConfigurationManager.AppSettings["SSPDOCS"].ToString();
                //}


                //var path1 = Path.Combine(Docurl, DocumentType + "_" + JobNumber + "_" + uploadedFile.FileName);

                //if (System.IO.File.Exists(path1))
                //{
                //    System.IO.File.Delete(path1);
                //}

                //uploadedFile.SaveAs(path1);
                //----------------------------------------------------------------------------------------------------
                if (Status == "Book")
                {
                    //mQuotationId = mFactory.SaveDocumentsBook(docentity, HttpContext.User.Identity.Name, "Book");
                }
                else
                {
                    Guid obj = Guid.NewGuid();
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();

                    mFactory.SaveDocuments(docentity, HttpContext.User.Identity.Name, "System Generated", obj.ToString(), EnvironmentName);
                }
                return Json(new { success = true, result = "File uploaded Successfully!!!" }, "text/html", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult InsertMultipleDocs(string DOCID, string JobNumber, string QuotationID, string CONDMAND, string DocTemplate, string DocType, string docCode, string direction, string OJobnumber, string PaymentStatus, string ProviderMailid)
        {
            if (string.IsNullOrEmpty(ProviderMailid))
            {
                ProviderMailid = HttpContext.User.Identity.Name;
            }

            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;

                HttpPostedFileBase theFile = files[0];
                long docid = 0;

                string FilePath = Path.GetExtension(theFile.FileName).ToLower();
                if (theFile.ContentLength != 0)
                {
                    if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                        && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                        && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                    {
                        return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                    }
                    byte[] imageSize = new byte[theFile.ContentLength];
                    theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();

                    sspdoc.FILENAME = theFile.FileName;
                    sspdoc.FILEEXTENSION = theFile.ContentType;
                    sspdoc.FILESIZE = theFile.ContentLength;
                    sspdoc.FILECONTENT = imageSize;
                    sspdoc.JOBNUMBER = Convert.ToInt64(JobNumber);
                    sspdoc.QUOTATIONID = Convert.ToInt64(QuotationID);
                    sspdoc.CONDMAND = CONDMAND;
                    sspdoc.DOCUMENTTEMPLATE = DocTemplate;
                    sspdoc.DocumentName = DocType;
                    sspdoc.DOCUMNETTYPE = docCode;
                    sspdoc.DIRECTION = direction;
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Guid obj = Guid.NewGuid();
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                    docid = mFactory.SaveDocumentsWithParams(docentity, HttpContext.User.Identity.Name, "Uploaded_Shipa", obj.ToString(), EnvironmentName, ProviderMailid, "YES");

                    if (PaymentStatus == "Payment Completed")
                        SendMailForCountryOperations("SSPCountryJobOPS", OJobnumber, HttpContext.User.Identity.Name);
                }
                var JsonToReturn = new
                {
                    rows = docid,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadDocuments_New(string DOCID, string JobNumber, string OJobnumber, string PaymentStatus)
        {
            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;

                HttpPostedFileBase theFile = files[0];


                string FilePath = Path.GetExtension(theFile.FileName).ToLower();
                if (theFile.ContentLength != 0)
                {
                    if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                        && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                        && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                    {
                        return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                    }
                    byte[] imageSize = new byte[theFile.ContentLength];
                    theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();

                    sspdoc.FILENAME = theFile.FileName;
                    sspdoc.FILEEXTENSION = theFile.ContentType;
                    sspdoc.FILESIZE = theFile.ContentLength;
                    sspdoc.FILECONTENT = imageSize;
                    sspdoc.DOCID = Convert.ToInt64(DOCID);
                    sspdoc.JOBNUMBER = Convert.ToInt64(JobNumber);
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Guid obj = Guid.NewGuid();
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                    mFactory.UpdateDocumentBytes(docentity, HttpContext.User.Identity.Name, obj.ToString(), EnvironmentName);

                    if (PaymentStatus == "Payment Completed")
                        SendMailForCountryOperations("SSPCountryJobOPS", OJobnumber, HttpContext.User.Identity.Name);
                }
                return Json(new { success = true, result = "File uploaded Successfully!!!" }, "text/html", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadDocuments_NewCollabration(string DOCID, string JobNumber, string OJobnumber, string PaymentStatus, string ModifiedBy)
        {
            if (string.IsNullOrEmpty(ModifiedBy))
            {
                ModifiedBy = HttpContext.User.Identity.Name;
            }
            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;

                HttpPostedFileBase theFile = files[0];


                string FilePath = Path.GetExtension(theFile.FileName).ToLower();
                if (theFile.ContentLength != 0)
                {
                    if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                        && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                        && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                    {
                        return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                    }
                    byte[] imageSize = new byte[theFile.ContentLength];
                    theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();

                    sspdoc.FILENAME = theFile.FileName;
                    sspdoc.FILEEXTENSION = theFile.ContentType;
                    sspdoc.FILESIZE = theFile.ContentLength;
                    sspdoc.FILECONTENT = imageSize;
                    sspdoc.DOCID = Convert.ToInt64(DOCID);
                    sspdoc.JOBNUMBER = Convert.ToInt64(JobNumber);
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Guid obj = Guid.NewGuid();
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                    mFactory.UpdateDocumentBytesCollabration(docentity, HttpContext.User.Identity.Name, obj.ToString(), EnvironmentName, ModifiedBy);

                    if (PaymentStatus == "Payment Completed")
                        SendMailForCountryOperations("SSPCountryJobOPS", OJobnumber, HttpContext.User.Identity.Name);
                }
                return Json(new { success = true, result = "File uploaded Successfully!!!" }, "text/html", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }


        [Authorize]
        public ActionResult GetDocumentDetails(long QuotationId, long JobNumber, string Status)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            IList<SSPDocumentsEntity> mDBEntity = mFactory.GetDocumentDetailsbyQuotationId(QuotationId, JobNumber);
            string docdetailstable = string.Empty;
            docdetailstable = docdetailstable + "<div class='table-responsive'>";
            docdetailstable = docdetailstable + "<table class='agility-table table table-striped table-bordered mr-b35 mr-t30'>";
            docdetailstable = docdetailstable + "<thead>";
            docdetailstable = docdetailstable + "<tr>";
            docdetailstable = docdetailstable + "<th>Document Name</th>";
            docdetailstable = docdetailstable + "<th>Document Type</th>";
            docdetailstable = docdetailstable + "<th>Uploaded On</th>";
            docdetailstable = docdetailstable + "<th>Actions</th>";
            docdetailstable = docdetailstable + "</tr>";
            docdetailstable = docdetailstable + "</thead>";
            docdetailstable = docdetailstable + "<tbody>";
            foreach (var item in mDBEntity)
            {
                //string path = item.DOCUMNETTYPE + "_" + item.JOBNUMBER + "_" + item.FILENAME; //
                docdetailstable = docdetailstable + "<tr>";
                docdetailstable = docdetailstable + "<td>" + item.DocumentName + "</td>";
                docdetailstable = docdetailstable + "<td>" + item.DOCUMNETTYPE + "</td> ";
                docdetailstable = docdetailstable + "<td>" + item.DATECREATED.ToString("dd-MMM-yyyy HH:mm") + "</td>"; //string.Format("{0:MM MMM MMMM}", date)
                docdetailstable = docdetailstable + "<td class='actions'>";
                docdetailstable = docdetailstable + "<a  href=\"javascript:DownloadDoc('" + item.DOCID + "')\" id='Prev_" + item.DOCID + "' class='btn mr-r10'><i class='icon-preview'></i>Preview</a>";
                if (item.DOCUMNETTYPE != "Quote")
                {
                    docdetailstable = docdetailstable + "<a href='javascript:DeleteDocument(" + item.DOCID + ")'  id='Del_" + item.DOCID + "' class='btn'><i class='icon-trash'></i>Delete</a>";
                }
                docdetailstable = docdetailstable + "</td>";
                docdetailstable = docdetailstable + "</tr>";
            }
            docdetailstable = docdetailstable + "</tbody> ";
            docdetailstable = docdetailstable + "</table>";
            docdetailstable = docdetailstable + "</div> ";
            Response.Write(docdetailstable);
            return null;
        }

        [Authorize]
        public ActionResult GetDocumentDetailsBySummary(long QuotationId, long JobNumber, string Status)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            IList<SSPDocumentsEntity> mDBEntity = mFactory.GetDocumentDetailsbyQuotationIdAndSummary(QuotationId, JobNumber);
            string docdetailstable = string.Empty;
            docdetailstable = docdetailstable + "<div class='table-responsive'>";
            docdetailstable = docdetailstable + "<table class='agility-table table table-striped table-bordered mr-b35 mr-t30'>";
            docdetailstable = docdetailstable + "<thead>";
            docdetailstable = docdetailstable + "<tr>";
            docdetailstable = docdetailstable + "<th>Document Name</th>";
            docdetailstable = docdetailstable + "<th>Document Type</th>";
            docdetailstable = docdetailstable + "<th>Uploaded On</th>";
            docdetailstable = docdetailstable + "<th>Actions</th>";
            docdetailstable = docdetailstable + "</tr>";
            docdetailstable = docdetailstable + "</thead>";
            docdetailstable = docdetailstable + "<tbody>";
            foreach (var item in mDBEntity)
            {
                //string path = item.DOCUMNETTYPE + "_" + item.JOBNUMBER + "_" + item.FILENAME; //
                docdetailstable = docdetailstable + "<tr>";
                docdetailstable = docdetailstable + "<td>" + item.DocumentName + "</td>";
                docdetailstable = docdetailstable + "<td>" + item.DOCUMNETTYPE + "</td> ";
                docdetailstable = docdetailstable + "<td>" + item.DATECREATED.ToString("dd-MMM-yyyy HH:mm") + "</td>"; //string.Format("{0:MM MMM MMMM}", date)
                docdetailstable = docdetailstable + "<td class='actions'>";
                docdetailstable = docdetailstable + "<a  href=\"javascript:DownloadDoc('" + item.DOCID + "')\" id='Prev_" + item.DOCID + "' class='btn mr-r10'><i class='icon-preview'></i>Preview</a>";
                if (item.DOCUMNETTYPE != "Quote")
                {
                    docdetailstable = docdetailstable + "<a href='javascript:DeleteDocument(" + item.DOCID + ")'  id='Del_" + item.DOCID + "' class='btn'><i class='icon-trash'></i>Delete</a>";
                }
                docdetailstable = docdetailstable + "</td>";
                docdetailstable = docdetailstable + "</tr>";
            }
            docdetailstable = docdetailstable + "</tbody> ";
            docdetailstable = docdetailstable + "</table>";
            docdetailstable = docdetailstable + "</div> ";
            Response.Write(docdetailstable);
            return null;
        }

        [Authorize]
        public JsonResult DeleteDocsbyDocId(long DOCID)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.DeleteDocByDocId(DOCID, HttpContext.User.Identity.Name.ToString());
                return Json("Deleted Successfully", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult DeleteDocContentbyDocId(long DOCID)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.DeleteDocContentbyDocId(DOCID, HttpContext.User.Identity.Name.ToString());
                return Json("Content Deleted Successfully", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


        [Authorize]
        public void DownloadDocsbyDocId(string DOCID)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                IList<SSPDocumentsEntity> mDBEntity = mFactory.GetDocumentDetailsbyDocumetnId(DOCID, HttpContext.User.Identity.Name.ToString());
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    memoryStream.Close();
                    var bytes = mDBEntity.FirstOrDefault().FILECONTENT;
                    Response.Clear();
                    Response.ContentType = "application/jpeg";
                    Response.AddHeader("Content-Disposition", "inline; filename=" + mDBEntity.FirstOrDefault().DOCUMNETTYPE + "_" + mDBEntity.FirstOrDefault().FILENAME);
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        //Fill State dropdown by CountryId
        [Authorize]
        public JsonResult FillStateByCountryId(string CountryId)
        {
            try
            {
                if (!string.IsNullOrEmpty(CountryId))
                {
                    MDMDataFactory mdmFactory = new MDMDataFactory();
                    IList<StateEntity> States = mdmFactory.GetStatesByCountry(Convert.ToInt32(CountryId), string.Empty, string.Empty);
                    if (States != null)
                    {
                        var StateList = (
                                 from items in States
                                 select new
                                 {
                                     Text = items.DESCRIPTION,
                                     Value = items.SUBDIVISIONID,
                                     STATECODE = items.CODE
                                 }).Distinct().OrderBy(x => x.Text).ToList();
                        return Json(StateList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult GetStateIsapplicable(string CountryID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable dt = mFactory.GetStateConfigurationFlag(CountryID);
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                var JsonToReturn = new
                {
                    rows = trows,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        //Fill city dropdown by state subdivisionid
        [Authorize]
        public JsonResult FillCityByStateSubDivisionId(string SubDivisionId)
        {
            try
            {
                if (!string.IsNullOrEmpty(SubDivisionId))
                {
                    MDMDataFactory mdmFactory = new MDMDataFactory();
                    IList<CityEntity> cities = mdmFactory.GetCitiesByStateJob(Convert.ToInt32(SubDivisionId));
                    if (cities != null)
                    {
                        var CityList = (
                                 from items in cities
                                 select new
                                 {
                                     Text = items.NAME,
                                     Value = items.CODE
                                 }).Distinct().OrderBy(x => x.Text).ToList();
                        return Json(CityList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        [Authorize]
        public JsonResult FillCityByCountryID(string CountryID)
        {
            try
            {
                if (!string.IsNullOrEmpty(CountryID))
                {
                    MDMDataFactory mdmFactory = new MDMDataFactory();
                    IList<CityEntity> cities = mdmFactory.GetCitiesByCountry(Convert.ToInt32(CountryID));
                    if (cities != null)
                    {
                        var CityList = (
                                 from items in cities
                                 select new
                                 {
                                     Text = items.NAME,
                                     Value = items.UNLOCATIONID,
                                     CityCode = items.CODE
                                 }).Distinct().OrderBy(x => x.Text).ToList();
                        return Json(CityList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        [Authorize]
        public JsonResult GetStateByCityId(string CityCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(CityCode))
                {
                    MDMDataFactory mdmFactory = new MDMDataFactory();
                    IList<StateEntity> States = mdmFactory.GetStateByCityId(Convert.ToString(CityCode));
                    if (States != null)
                    {
                        var StateList = (
                                 from items in States
                                 select new
                                 {
                                     Text = items.DESCRIPTION,
                                     Value = items.SUBDIVISIONID
                                 }).Distinct().OrderBy(x => x.Text).ToList();
                        return Json(StateList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        [Authorize]
        [EncryptedActionParameter]
        public async Task<ActionResult> BookingSummary(int? JobNumber, int? id)
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            if (!JobNumber.HasValue || JobNumber.Value == 0)
            {
                JobNumber = Convert.ToInt32((Session["JJOBNUMBER"]).ToString());
            }
            if (!id.HasValue || id.Value == 0)
            {
                id = Convert.ToInt32((Session["QUOTATIONID"]).ToString());
            }

            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            QuotationDBFactory mQuotationFactory = new QuotationDBFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            if (Convert.ToInt64(JobNumber) == 0)
            {
                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            }
            else
            {
                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetails(Convert.ToInt32(id), Convert.ToInt64(JobNumber));
                if (uiCulture != "en")
                {
                    List<JobBookingShipmentEntity> mdbJobList = mDBEntity.JobShipmentItems.ToList();
                    await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<JobBookingEntity>(uiCulture, mDBEntity));
                    await Task.Run(() => LocalizedConverter.ChargesBooklanguageconversion<JobBookingEntity>(uiCulture, mDBEntity));
                    await Task.Run(() => LocalizedConverter.GetConversionObjectListByLanguage<JobBookingShipmentEntity>(uiCulture, mdbJobList)).ConfigureAwait(false);
                }

                //List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetSavedJobs(HttpContext.User.Identity.Name, status, list, PageNo, SearchString, Order);  
                var jobdet = mDBEntity.JobItems;
                var partydet = mDBEntity.PartyItems;
                var shipperdet = (from u in partydet where u.PARTYTYPE == "91" select u).ToArray();
                var consigneedet = (from u in partydet where u.PARTYTYPE == "92" select u).ToArray();
                if (jobdet != null)
                {
                    mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                    mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                    mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                    mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                    mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                    mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                    mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                    mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                    mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                    mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                    mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                    mDBEntity.JSTATEID = jobdet[0].STATEID;
                }
                if (shipperdet != null && shipperdet.Length > 0)
                {
                    mDBEntity.SHADDRESSTYPE = shipperdet[0].ADDRESSTYPE;
                    mDBEntity.SHBUILDINGNAME = shipperdet[0].BUILDINGNAME;
                    mDBEntity.SHCITYCODE = shipperdet[0].CITYCODE;
                    mDBEntity.SHCITYNAME = shipperdet[0].CITYNAME;
                    mDBEntity.SHCLIENTID = shipperdet[0].CLIENTID;
                    mDBEntity.SHCLIENTNAME = shipperdet[0].CLIENTNAME;
                    mDBEntity.SHFULLADDRESS = shipperdet[0].FULLADDRESS;
                    mDBEntity.SHCOUNTRYCODE = shipperdet[0].COUNTRYCODE;
                    mDBEntity.SHCOUNTRYNAME = shipperdet[0].COUNTRYNAME;
                    mDBEntity.SHFULLADDRESS = shipperdet[0].FULLADDRESS;
                    mDBEntity.SHHOUSENO = shipperdet[0].HOUSENO;
                    mDBEntity.SHISDEFAULTADDRESS = shipperdet[0].ISDEFAULTADDRESS;
                    mDBEntity.SHJOBNUMBER = shipperdet[0].JOBNUMBER;
                    mDBEntity.SHLATITUDE = shipperdet[0].LATITUDE;
                    mDBEntity.SHLONGITUDE = shipperdet[0].LONGITUDE;
                    mDBEntity.SHPARTYDETAILSID = shipperdet[0].PARTYDETAILSID;
                    mDBEntity.SHPARTYTYPE = shipperdet[0].PARTYTYPE;
                    mDBEntity.SHPINCODE = shipperdet[0].PINCODE;
                    mDBEntity.SHSTATECODE = shipperdet[0].STATECODE;
                    mDBEntity.SHSTATENAME = shipperdet[0].STATENAME;
                    mDBEntity.SHSTREET1 = shipperdet[0].STREET1;
                    mDBEntity.SHSTREET2 = shipperdet[0].STREET2;
                    mDBEntity.SHFIRSTNAME = shipperdet[0].FIRSTNAME;
                    mDBEntity.SHLASTNAME = shipperdet[0].LASTNAME;
                    mDBEntity.SHPHONE = shipperdet[0].PHONE;
                    mDBEntity.SHEMAILID = shipperdet[0].EMAILID;
                    mDBEntity.SHSALUTATION = shipperdet[0].SALUTATION;
                }

                if (consigneedet != null && consigneedet.Length > 0)
                {
                    mDBEntity.CONADDRESSTYPE = consigneedet[0].ADDRESSTYPE;
                    mDBEntity.CONBUILDINGNAME = consigneedet[0].BUILDINGNAME;
                    mDBEntity.CONCITYCODE = consigneedet[0].CITYCODE;
                    mDBEntity.CONCITYNAME = consigneedet[0].CITYNAME;
                    mDBEntity.CONCLIENTID = consigneedet[0].CLIENTID;
                    mDBEntity.CONCLIENTNAME = consigneedet[0].CLIENTNAME;
                    mDBEntity.CONCOUNTRYCODE = consigneedet[0].COUNTRYCODE;
                    mDBEntity.CONCOUNTRYNAME = consigneedet[0].COUNTRYNAME;
                    mDBEntity.CONFULLADDRESS = consigneedet[0].FULLADDRESS;
                    mDBEntity.CONHOUSENO = consigneedet[0].HOUSENO;
                    mDBEntity.CONISDEFAULTADDRESS = consigneedet[0].ISDEFAULTADDRESS;
                    mDBEntity.CONJOBNUMBER = consigneedet[0].JOBNUMBER;
                    mDBEntity.CONLATITUDE = consigneedet[0].LATITUDE;
                    mDBEntity.CONLONGITUDE = consigneedet[0].LONGITUDE;
                    mDBEntity.CONPARTYDETAILSID = consigneedet[0].PARTYDETAILSID;
                    mDBEntity.CONPARTYTYPE = consigneedet[0].PARTYTYPE;
                    mDBEntity.CONPINCODE = consigneedet[0].PINCODE;
                    mDBEntity.CONSTATECODE = consigneedet[0].STATECODE;
                    mDBEntity.CONSTATENAME = consigneedet[0].STATENAME;
                    mDBEntity.CONSTREET1 = consigneedet[0].STREET1;
                    mDBEntity.CONSTREET2 = consigneedet[0].STREET2;
                    mDBEntity.CONFIRSTNAME = consigneedet[0].FIRSTNAME;
                    mDBEntity.CONLASTNAME = consigneedet[0].LASTNAME;
                    mDBEntity.CONPHONE = consigneedet[0].PHONE;
                    mDBEntity.CONEMAILID = consigneedet[0].EMAILID;
                    mDBEntity.CONSALUTATION = consigneedet[0].SALUTATION;
                }
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            }

            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PREFERREDCURRENCYID);
            ViewBag.decimalCount = decimalCount;
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("BookingSummary", mUIModel);
        }

        //Loading documents in booking summary page
        [Authorize]
        public ActionResult LoadDocinBookingSummary(long QuotationId, long JobNumber)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            IList<SSPDocumentsEntity> mDBEntity = mFactory.GetDocumentDetailsbyQuotationId(QuotationId, JobNumber);
            string doc = string.Empty;
            doc = doc + "<div class='download-data-bkg'>";
            doc = doc + "<h4 class='upload-hdg'>uploaded documents</h4>";
            doc = doc + "<div class='row'>";
            doc = doc + "<div class='col-xs-12 col-sm-9 col-lg-6'>";
            doc = doc + "<div class='row'>";
            foreach (var item in mDBEntity)
            {
                doc = doc + "<div class='col-xs-12 col-sm-4' onclick='download(" + item.DOCID + ");'>";
                doc = doc + "<ul class='bkg-list jpg-bkg'>";
                doc = doc + "<li>";
                doc = doc + "<label class='bkg-label' >" + item.FILENAME + "</label>";
                doc = doc + "</li><li>";
                doc = doc + "<p class='bkg-note'>" + item.DATECREATED.ToString("dd-MMM-yyyy") + "</p>";
                doc = doc + "</li></ul>";
                doc = doc + "</div>";
            }
            doc = doc + "</div></div> </div></div>";
            Response.Write(doc);
            return null;
        }

        // Downloading  the documents
        public FileStreamResult DownloadFilesByDOCID(int DOCID)
        {
            SSPDocumentsEntity documents = new SSPDocumentsEntity();
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            if (DOCID > 0)
            {
                documents = mFactory.GetDocumentDetailsbyDOCID(DOCID);
            }
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = documents.FILENAME,
                Inline = true,
            };
            MemoryStream stream = new MemoryStream(documents.FILECONTENT);
            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.End();
            return File(stream, documents.FILEEXTENSION);
        }

        public JsonResult FillCountry()
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CountryEntity> country = mdmFactory.GetCountries(string.Empty, string.Empty);
                if (country != null)
                {
                    var Countrylist = (
                             from items in country
                             select new
                             {
                                 Text = items.NAME,
                                 Value = items.COUNTRYID
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(Countrylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        #region Autopopulate the partydetails by passing partydetailsid

        public JsonResult GetPartyDetForAutoFillByPartyDetailsId(long partydetailsid)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var party = mFactory.GetPartyDetForAutoFillByPartyDetailsId(partydetailsid);
            long CounryId = mFactory.GetCountryIdByCountryCode(party.COUNTRYCODE);
            long SubDivisionId = mFactory.GetSubDivisionIdByStateCode(party.STATECODE, CounryId);
            return Json(new { party, CounryId, SubDivisionId }, JsonRequestBehavior.AllowGet);
        }

        //Getting client master details for auto populating the values
        [Authorize]
        [ValidateJsonAntiForgeryToken]
        public JsonResult GetClientMasterForAutoFillBySSPclientId(string sspclientid)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var party = mFactory.GetClientMasterForAutoFillBySSPclientId(sspclientid, HttpContext.User.Identity.Name);
            long CounryId = mFactory.GetCountryIdByCountryCode(party.COUNTRYCODE);
            long SubDivisionId = mFactory.GetSubDivisionIdByStateCode(party.STATECODE, CounryId);
            return Json(new { party, CounryId, SubDivisionId }, JsonRequestBehavior.AllowGet);
        }
        //Filling ClientMasterDetails
        public JsonResult FillClientMaster()
        {
            try
            {
                JobBookingDbFactory jobFactory = new JobBookingDbFactory();
                IList<SSPClientMasterEntity> client = jobFactory.GetPartyDetailsFromSSPClientMaster(HttpContext.User.Identity.Name);
                if (client != null)
                {
                    var Countrylist = (
                             from items in client
                             select new
                             {
                                 Text = items.CLIENTNAME,
                                 Value = items.SSPCLIENTID
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(Countrylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        #endregion

        /// <summary>
        /// Getting the HS Code Details of Origin Country. --> Anil G
        /// </summary>
        /// <param name="CountryCode">Origin Country</param>
        /// <returns>Origin Country HS Code Details</returns>
        public JsonResult GetHsCodes(string CountryCode, string IHSCode)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable dt = mFactory.GetHsCodesBasedonCountryCode(CountryCode, IHSCode);
            List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Country based instructions on load
        /// </summary>
        /// <param name="OriginPlaceID"></param>
        /// <param name="DestinationPlaceID"></param>
        /// <returns></returns>
        public JsonResult GetCountryBasedInstructions(string OriginPlaceID, string DestinationPlaceID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet DsCountry = mFactory.GetCountryInstructions_Old(Convert.ToInt64(OriginPlaceID), Convert.ToInt64(DestinationPlaceID));
            if (DsCountry != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Name", typeof(string));
                dt.Columns.Add("Value", typeof(string));
                dt.Columns.Add("DocumentID", typeof(string));
                dt.Columns.Add("DOCUMENTNAME", typeof(string));
                dt.Columns.Add("Status", typeof(int));
                dt.Columns.Add("HSCode", typeof(string));
                dt.Columns.Add("Direction", typeof(string));
                foreach (DataRow dr in DsCountry.Tables[0].Rows)
                {
                    dt.Rows.Add(dr["INSTRNTYPE"].ToString(), dr["EXPORTINSTRUCTION"].ToString(),
                        dr["DOCUMENTID"], dr["DOCUMENTNAME"].ToString(), 1, "", dr["DIRECTION"].ToString());
                }
                foreach (DataRow dr in DsCountry.Tables[1].Rows)
                {
                    dt.Rows.Add(dr["FIELDID"].ToString(), dr["FIELDDESCRIPTION"].ToString(),
                     dr["FIELDLENGTH"].ToString(), "", 4, "", "");
                }
                //Filter Distinct Docs
                var dtDoc = dt.DefaultView.ToTable(true, "DocumentName");
                Session["HscodeDocs"] = (DataTable)dtDoc;
                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                var JsonToReturn = new
                {
                    rows = trows,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }
        /// <summary>
        /// Getting  the Compliance And Dutys Of Selected HsCode 
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public JsonResult GetComplianceAndDutysOfHsCode(string OriginPlaceID, string DestinationPlaceID, string HSCodeOrg, string HSCodeDes)//, string HSCodeDes
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();
            if (HSCodeOrg != "")
            {
                if (HSCodeOrg.IndexOf(',') > 0)
                {
                    for (int r = 0; r < HSCodeOrg.Split(',').Length; r++)
                    {
                        DataSet ds1 = mFactory.GetComplianceAndDutysOfHsCode(Convert.ToInt64(OriginPlaceID), Convert.ToInt64(DestinationPlaceID), HSCodeOrg.Split(',')[r].ToString(), "O");
                        ds.Merge(ds1);
                    }
                }
                else
                {
                    ds = mFactory.GetComplianceAndDutysOfHsCode(Convert.ToInt64(OriginPlaceID), Convert.ToInt64(DestinationPlaceID), HSCodeOrg.ToString(), HSCodeDes);
                }
            }

            DataSet DsCountry = mFactory.GetCountryInstructions_Old(Convert.ToInt64(OriginPlaceID), Convert.ToInt64(DestinationPlaceID));
            ds.Merge(DsCountry.Tables[0]); ds.Merge(DsCountry.Tables[1]);
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Value", typeof(string));
            dt.Columns.Add("DocumentID", typeof(string));
            dt.Columns.Add("DOCUMENTNAME", typeof(string));
            dt.Columns.Add("Status", typeof(int));
            dt.Columns.Add("HSCode", typeof(string));
            dt.Columns.Add("Direction", typeof(string));
            dt.Columns.Add("HS_Code", typeof(string));

            DataTable DTIns = new DataTable();

            if (ds.Tables.Count > 0)
            {
                DTIns = ds.Tables[0].Clone();

                foreach (DataRow dr in ds.Tables[0].Rows) //Instru
                {
                    DTIns.ImportRow(dr);
                }


                foreach (DataRow dr in ds.Tables[1].Rows) //Hscode Destination
                {
                    dt.Rows.Add(dr["HSCODE"].ToString(), dr["DESCRIPTION"].ToString(),
                      dr["DESCRIPTION"], dr["COUNTRYCODE"], 5, "", "", "");
                }

                foreach (DataRow dr in ds.Tables[2].Rows) //Hscode Description
                {
                    dt.Rows.Add(dr["HSCODE"].ToString(), dr["HSCODE4DESCRIPTION"].ToString(),
                       "", "", 6, dr["HSCODE"].ToString(), dr["DIRECTION"].ToString(), dr["HSCODE"].ToString());
                }

                foreach (DataRow dr in ds.Tables[3].Rows) //Duty
                {
                    dt.Rows.Add(dr["DUTYRATETYPE"].ToString(), dr["DUTYRATEPERCENTAGE"].ToString(), 0, dr["CURRENCY"].ToString(), 2, dr["ORG_HSCODE"].ToString(), dr["DIRECTION"].ToString(), dr["HSCODE"].ToString());//,""
                }


                //foreach (DataRow dr in ds.Tables[4].Rows)
                //{
                //    dt.Rows.Add(dr["INSTRNTYPE"].ToString(), dr["EXPORTINSTRUCTION"].ToString(),
                //       dr["DOCUMENTID"], dr["DOCUMENTNAME"].ToString(), 3, "", dr["DIRECTION"].ToString(), "");
                //}

                //foreach (DataRow dr in ds.Tables[5].Rows)
                //{
                //    dt.Rows.Add(dr["FIELDID"].ToString(), dr["FIELDDESCRIPTION"].ToString(),
                //      dr["FIELDLENGTH"].ToString(), "", 4, "", "", "");
                //}
            }

            //Filter Distinct Docs
            var dtDoc = dt.DefaultView.ToTable(true, "DocumentName");
            Session["HscodeDocs"] = dtDoc;

            List<Dictionary<string, object>> tIns = ReturnJsonResult(DTIns);

            List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
            var JsonToReturn = new
            {
                rows = trows,
                tIns = tIns
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSavedInstructions(string Jobnumber, string QuotationID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();

            if (Jobnumber != "")
            {
                ds = mFactory.GetSavedHscodeInstructionsDutys(Jobnumber);
            }
            else
            {
                ds = mFactory.GetSavedHscodeInstructionsDutys(QuotationID, Jobnumber);
            }

            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocumentList(string Jobnumber, string QuotationID, string OriginPlaceID, string DestinationPlaceID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet DsCountry = new DataSet(); DataTable dt = new DataTable();

            DsCountry = mFactory.GetCountryInstructions(Convert.ToInt64(OriginPlaceID), Convert.ToInt64(DestinationPlaceID));

            if (DsCountry != null)
            {
                if (DsCountry.Tables[0].Rows.Count > 0)
                {
                    dt = DsCountry.Tables[0].DefaultView.ToTable();
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["DocumentName"].ToString().Length == 0)
                            dr.Delete();
                    }

                }
                var DtIns = DsCountry.Tables[1].Clone();
                var DTFIELDS = DsCountry.Tables[2].Clone();

                foreach (DataRow dr in DsCountry.Tables[1].Rows)
                {
                    DtIns.ImportRow(dr);
                }

                foreach (DataRow dr in DsCountry.Tables[2].Rows)
                {

                    DTFIELDS.ImportRow(dr);
                }
                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                List<Dictionary<string, object>> tFIELDS = ReturnJsonResult(DTFIELDS);
                List<Dictionary<string, object>> Insrows = ReturnJsonResult(DtIns);
                var JsonToReturn = new
                {
                    rows = trows,
                    Insrows = Insrows,
                    FIELDS = tFIELDS
                };

                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSavedHscodeDuties(string Jobnumber, string QuotationID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();

            if (Jobnumber != "")
            {

                ds = mFactory.GetSavedHscodeDuties(Jobnumber);
            }
            else
            {
                ds = mFactory.GetSavedHscodeDuties(QuotationID, Jobnumber);
            }

            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSavedHSCodes(string Jobnumber, string QuotationID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();

            if (Jobnumber != "")
            {
                ds = mFactory.GetSavedHSCodes(Jobnumber);
            }
            //else
            //{
            //    ds = mFactory.GetSavedHSCodes(QuotationID, Jobnumber, CountryId);
            //}

            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSavedFieldValues(string Jobnumber, string QuotationID)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();
            if (Jobnumber != "")
            {
                ds = mFactory.GetSavedFieldValues(Jobnumber);
            }


            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetComplianceLevelsData(string MASTERTYPE, int MASTERID)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                DataSet ds = mFactory.GetComplianceLevelsData(MASTERTYPE, MASTERID);

                List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
                var JsonToReturn = new
                {
                    rows = trows,
                };

                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //public JsonResult GetSavedDesHSCodes(string Jobnumber, string QuotationID)
        //{
        //    JobBookingDbFactory mFactory = new JobBookingDbFactory();
        //    DataSet ds = new DataSet();

        //    if (Jobnumber != "")
        //    {
        //        ds = mFactory.GetSavedDesHSCodes(Jobnumber);
        //    }
        //    //else
        //    //{
        //    //    ds = mFactory.GetSavedHSCodes(QuotationID, Jobnumber, CountryId);
        //    //}

        //    List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
        //    var JsonToReturn = new
        //    {
        //        rows = trows,
        //    };

        //    return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetUpdatedHSCodeInstructions(string Jobnumber, string QuotationID, string HSCode)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();

            if (Jobnumber != "")
            {
                ds = mFactory.GetUpdatedHSCodeInstructions(Jobnumber, HSCode);
            }

            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUpdatedHSCodeDuties(string Jobnumber, string QuotationID, string HSCode)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = new DataSet();
            if (Jobnumber != "")
            {

                ds = mFactory.GetUpdatedHSCodeDuties(Jobnumber, HSCode);
            }
            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    temp_row.Add(col.ColumnName, dr[col]);
                }
                trows.Add(temp_row);
            }
            return trows;
        }

        [HttpPost]
        public ActionResult ShareLink(string email, string link)
        {
            JobBookingModel tm = (JobBookingModel)TempData["JobBookingData"];
            TempData["JobBookingData"] = tm;
            SendMail(tm, email, link);
            var nJSON = new JsonResult();
            nJSON.Data = true;
            return nJSON;
        }

        private void SendMail(JobBookingModel tm, string email, string link)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(email));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Job Booking" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mode = tm.PRODUCTNAME.ToString().ToUpper() == "AIR FREIGHT" || tm.PRODUCTNAME.ToString().ToUpper() == "AIR" ? "Air" : "Ocean";
            string mPlaceOfReceipt = ""; string mPlaceOfDelivery = "";
            string mPRLableName = ""; string mPDLableName = "";
            if (tm.MOVEMENTTYPENAME == "Door to door" || tm.MOVEMENTTYPENAME == "Door to Door")
            {
                mPlaceOfReceipt = string.Format("{0}, {1}", tm.ORIGINPLACECODE, tm.ORIGINPLACENAME);
                mPlaceOfDelivery = string.Format("{0}, {1}", tm.DESTINATIONPLACECODE, tm.DESTINATIONPLACENAME);
                mPRLableName = "Origin Place"; mPDLableName = "Destination Place";
            }
            else if (tm.MOVEMENTTYPENAME == "Door to port")
            {
                mPlaceOfReceipt = string.Format("{0}, {1}", tm.ORIGINPLACECODE, tm.ORIGINPLACENAME);
                mPlaceOfDelivery = string.Format("{0}, {1}", tm.DESTINATIONPORTCODE, tm.DESTINATIONPORTNAME);
                mPRLableName = "Origin Place";
                mPDLableName = (mode.ToLower() == "air" ? "Destination Airport" : "Destination Port");
            }
            else if (tm.MOVEMENTTYPENAME == "Port to Port" || tm.MOVEMENTTYPENAME == "Port to port")
            {
                mPlaceOfReceipt = string.Format("{0}, {1}", tm.ORIGINPORTCODE, tm.ORIGINPORTNAME);
                mPlaceOfDelivery = string.Format("{0}, {1}", tm.DESTINATIONPORTCODE, tm.DESTINATIONPORTNAME);
                mPRLableName = (mode.ToLower() == "air" ? "Origin Airport" : "Origin Port");
                mPDLableName = (mode.ToLower() == "air" ? "Destination Airport" : "Destination Port");
            }
            else if (tm.MOVEMENTTYPENAME == "Port to door")
            {
                mPlaceOfReceipt = string.Format("{0}, {1}", tm.ORIGINPORTCODE, tm.ORIGINPORTNAME);
                mPlaceOfDelivery = string.Format("{0}, {1}", tm.DESTINATIONPLACECODE, tm.DESTINATIONPLACENAME);
                mPRLableName = (mode.ToLower() == "air" ? "Origin Airport" : "Origin Port");
                mPDLableName = "Destination Place";
            }

            string cargodetails = string.Empty;
            foreach (var mShipment in tm.ShipmentItems)
            {
                cargodetails = cargodetails + "<tr><td style='padding: 20px;'><span style='font-weight: 600; color: #868686;'>Package Type</span><br>" +
                                "<span style='font-weight: 600; color: #000'>" + mShipment.PackageTypeName + "</span> </td> <td style=''>" +
                                "<span style='font-weight: 600; color: #868686;'>Quantity in Pieces</span><br>" +
                                "<span style='font-weight: 600; color: #000'> " + mShipment.Quantity.ToString() + "</span></td> </tr> ";
            }

            //using streamreader for reading my email template 
            using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/booking-link-share-temp.html")))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{Email}", email);
            body = body.Replace("{Created_by}", HttpContext.User.Identity.Name); //To-DO
            body = body.Replace("{orginplace}", mPRLableName);
            body = body.Replace("{destinationPlace}", mPDLableName);
            body = body.Replace("{plc_rcpt}", mPlaceOfReceipt);
            body = body.Replace("{plc_del}", mPlaceOfDelivery);
            body = body.Replace("{QuoteAmount}", "0");
            body = body.Replace("{bookingID}", tm.JOPERATIONALJOBNUMBER.ToString());
            body = body.Replace("{Movement_Type}", tm.MOVEMENTTYPENAME.ToString());
            body = body.Replace("{mode_imag}", mode == "Air" ? "air" : "ship");
            body = body.Replace("{Mode_transport}", mode);
            body = body.Replace("{pick_city}", tm.ORIGINPLACENAME);
            body = body.Replace("{del_City}", tm.DESTINATIONPLACENAME);
            body = body.Replace("{pickup_dep}", tm.ORIGINPORTNAME);
            body = body.Replace("{del_depature}", tm.DESTINATIONPORTNAME);
            body = body.Replace("{CargoDetails}", cargodetails);
            body = body.Replace("{quote_Amt}", "0.0");
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", email, "", "",
         "JobBooking", "Share", tm.JOPERATIONALJOBNUMBER.ToString(), HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
        }

        public string GenerateToken()
        {
            RNGCryptoServiceProvider cryptRNG = new RNGCryptoServiceProvider();
            byte[] tokenBuffer = new byte[8];
            cryptRNG.GetBytes(tokenBuffer);
            return Convert.ToBase64String(tokenBuffer);
        }

        //public static byte[] GeneratePDF(MemoryStream memoryStream, QuotationPreviewModel mUIModels)
        //{
        //    z = 0;
        //    UserDBFactory mFactory = new UserDBFactory();
        //    mUserModel = mFactory.GetUserProfileDetails(mUIModels.CreatedBy, 1);
        //    mQuoteChargesTotal = 0;
        //    Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
        //    PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
        //    doc.Open();
        //    writer.PageEvent = new ITextEvents();
        //    PdfPTable mHeadingtable = new PdfPTable(1);
        //    mHeadingtable.WidthPercentage = 100;
        //    mHeadingtable.DefaultCell.Border = 0;
        //    mHeadingtable.SpacingAfter = 5;
        //    string mDocumentHeading = (mUIModels.ProductId == 2) ? "Ocean Freight Quote" : "Air Freight Quote";
        //    Formattingpdf mFormattingpdf = new Formattingpdf();
        //    PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
        //    mHeadingtable.AddCell(quotationheading);
        //    doc.Add(mHeadingtable);
        //    doc.Add(mFormattingpdf.LineSeparator());
        //    doc.Add(QuoteInfoTableModel(mUIModels));
        //    doc.Add(ShipInfoTableModel(mUIModels));
        //    PdfPTable mitemdescriptiontable = new PdfPTable(1);
        //    mitemdescriptiontable.WidthPercentage = 100;
        //    mitemdescriptiontable.SpacingBefore = 10;
        //    mitemdescriptiontable.DefaultCell.Border = 0;
        //    mitemdescriptiontable.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Item Description", Element.ALIGN_LEFT, false)));
        //    PdfPTable mItemDescription = new PdfPTable(1);
        //    mItemDescription.SetWidths(new float[] { 100 });
        //    mItemDescription.WidthPercentage = 100;
        //    mItemDescription.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mUIModels.ShipmentDescription == null ? "" : mUIModels.ShipmentDescription, Element.ALIGN_LEFT)));

        //    PdfPCell mRoutecell = new PdfPCell(mItemDescription);
        //    mRoutecell.Colspan = 2;
        //    mRoutecell.BorderColor = mFormattingpdf.TableBorderColor;
        //    mitemdescriptiontable.AddCell(mRoutecell);
        //    doc.Add(mitemdescriptiontable);

        //    if (mUIModels.QuotationCharges.Count() > 0)
        //    {
        //        var mOrigincharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        var mDestinationcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        var mINTcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        var mAddcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        if (mOrigincharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mUIModels.PreferredCurrencyId));

        //        if (mINTcharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mINTcharges, "International Charges", mUIModels.PreferredCurrencyId));

        //        if (mDestinationcharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mUIModels.PreferredCurrencyId));

        //        if (mAddcharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mAddcharges, "Additional Charges", mUIModels.PreferredCurrencyId));
        //    }
        //    PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
        //    mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
        //    mQuoteChargeTotalBorder.WidthPercentage = 100;
        //    mQuoteChargeTotalBorder.SpacingBefore = 10;
        //    mQuoteChargeTotalBorder.DefaultCell.Border = 0;
        //    mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

        //    PdfPTable mQuoteChargeTotal = new PdfPTable(2);
        //    mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
        //    mQuoteChargeTotal.WidthPercentage = 100;
        //    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
        //    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PreferredCurrencyId + " " + Double.Parse(mQuoteChargesTotal.ToString()), Element.ALIGN_RIGHT)));

        //    PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
        //    mQuoteChargeTotalBorder.AddCell(mTotalcells);
        //    doc.Add(mQuoteChargeTotalBorder);
        //    doc.NewPage();
        //    mHeadingtable.AddCell(quotationheading);
        //    doc.Add(TermsandConditionsTableModel(mUIModels));

        //    doc.Close();

        //    bytes = memoryStream.ToArray(); ;
        //    Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        PdfReader reader = new PdfReader(bytes);
        //        using (PdfStamper stamper = new PdfStamper(reader, stream))
        //        {
        //            int pages = reader.NumberOfPages;
        //            for (int i = 1; i <= pages; i++)
        //            {
        //                ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
        //            }
        //        }
        //        bytes = stream.ToArray();
        //    }
        //    return bytes;
        //}
        public static PdfPTable ShipInfoTableModel(QuotationPreviewModel model)
        {
            int mProductId = model.ProductId;
            int mProductTypeId = model.ProductTypeId;
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mShipbordertable = new PdfPTable(1);
                mShipbordertable.WidthPercentage = 100;
                mShipbordertable.SpacingBefore = 10;
                mShipbordertable.DefaultCell.Border = 0;
                List<PdfPCell> mHeaderLabels = new List<PdfPCell>();
                PdfPTable mShipTab = null;
                mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Quantity", Element.ALIGN_LEFT, false)));
                if (mProductTypeId == 5)
                {
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Container Size", Element.ALIGN_LEFT, false)));
                    mShipTab = new PdfPTable(2);
                    mShipTab.SetWidths(new float[] { 20, 80 });
                }
                else
                {
                    mShipTab = new PdfPTable(5);
                    mShipTab.SetWidths(new float[] { 10, 10, 10, 10, 10 });
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Package Type", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Dimension(L*W*H)", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Per Piece", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Gross Weight", Element.ALIGN_LEFT, false)));
                }
                mShipTab.WidthPercentage = 100;
                mShipbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Cargo Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                foreach (var mLabel in mHeaderLabels)
                {
                    mShipTab.AddCell(mLabel);
                }
                foreach (var mShipment in model.ShipmentItems)
                {
                    mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.Quantity.ToString(), Element.ALIGN_LEFT)));
                    if (mProductTypeId == 5)
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ContainerName, Element.ALIGN_LEFT)));
                    else
                    {
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.PackageTypeName, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ItemLength + " x " + mShipment.ItemWidth + " x " + mShipment.ItemHeight + " " + mShipment.LengthUOMCode, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTPERPIECE) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTPERPIECE) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTTOTAL) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTTOTAL) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                    }
                }
                PdfPCell mShipcell = new PdfPCell(mShipTab);
                mShipcell.BorderColor = mFormattingpdf.TableBorderColor;
                mShipbordertable.AddCell(mShipcell);

                if (mProductTypeId != 5)
                {
                    string mTotalCharegebleVolume = string.Empty;
                    if (mProductId == 2)
                        mTotalCharegebleVolume = "Revenue Ton" + " : " + (IsNullOrDefault(model.ChargeableVolume) ? "0.00" : (model.ChargeableVolume) <= 1 ? String.Format(culture, format, model.ChargeableVolume) + " " + "(minimum)" : String.Format(culture, format, model.ChargeableVolume));
                    else
                        mTotalCharegebleVolume = "Volumetric Weight (Kgs)" + " : " + (IsNullOrDefault(model.VolumetricWeight) ? "0.0" : String.Format(culture, format, model.VolumetricWeight));

                    string mTotalGW = "Total Gross Weight (Kgs)" + " : " + (IsNullOrDefault(model.TotalGrossWeight) ? "0.0" : String.Format(culture, format, model.TotalGrossWeight));
                    string mTotalCBM = "Total Cubic Meters" + " : " + (IsNullOrDefault(model.TotalCBM) ? "0.000" : String.Format(culture, format, model.TotalCBM));
                    string mTotalCharegebleWeight = "Chargeable Weight" + " : " + (IsNullOrDefault(model.ChargeableWeight) ? "0.00" : String.Format(culture, format, model.ChargeableWeight));
                    PdfPTable mShipmentTotalWeights;

                    if (mProductId == 2)
                    {
                        mShipmentTotalWeights = new PdfPTable(3);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.SetWidths(new float[] { 53, 56, 77 });
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    else
                    {
                        mShipmentTotalWeights = new PdfPTable(4);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleWeight, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    PdfPCell mTableFooterCell = mFormattingpdf.TableHeading("ShipmentDetails", Element.ALIGN_LEFT, false);
                    mTableFooterCell.Colspan = mShipTab.NumberOfColumns;
                    mTableFooterCell.Padding = 0;
                    mTableFooterCell.BackgroundColor = BaseColor.WHITE;
                    mTableFooterCell.AddElement(mShipmentTotalWeights);
                    mShipbordertable.AddCell(mTableFooterCell);
                }
                return mShipbordertable;
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        static bool IsNullOrDefault<T>(T value)
        {
            return object.Equals(value, default(T));
        }

        public static PdfPCell ImageCell(string path, float scale, int align)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(System.Web.Hosting.HostingEnvironment.MapPath(path));
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 0f;
            cell.PaddingTop = 0f;
            return cell;
        }

        public static PdfPTable QuoteInfoTableModel(QuotationPreviewModel model)
        {
            try
            {
                var MName = string.Empty;
                var Ploading = string.Empty;
                var Pdischarge = string.Empty;
                if (model.ProductId == 3)
                {
                    if (model.MovementTypeName == "Port to port") { MName = "Airport to airport"; }
                    if (model.MovementTypeName == "Door to port") { MName = "Door to airport"; }
                    if (model.MovementTypeName == "Port to door") { MName = "Airport to door"; }
                    if (model.MovementTypeName == "Door to door") { MName = "Door to door"; }
                    Ploading = "Airport of loading";
                    Pdischarge = "Airport of discharge";
                }
                else
                {
                    MName = model.MovementTypeName;
                    Ploading = "Port of loading";
                    Pdischarge = "Port of discharge";
                }

                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mQuotebordertable = new PdfPTable(3);
                mQuotebordertable.WidthPercentage = 100;
                mQuotebordertable.SpacingBefore = 10;
                mQuotebordertable.DefaultCell.Border = 0;
                PdfPTable mQuoteTab = new PdfPTable(9);
                mQuoteTab.SetWidths(new float[] { 15, 1, 15, 13, 1, 20, 15, 1, 20 });
                mQuoteTab.WidthPercentage = 100;
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Mode of transport")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.ProductName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.OriginPlaceName == null ? "" : " Origin City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : model.OriginPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.DestinationPlaceName == null ? "" : "Destination City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : model.DestinationPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Movement type")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(MName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(Ploading)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPortName == null ? "" : model.OriginPortName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(Pdischarge)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPortName == null ? "" : model.DestinationPortName)));
                if (model.co2emission != 0)
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("CO2 Emission")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.co2emission == 0 ? "" : Convert.ToString(model.co2emission) + " KG")));
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
                if (model.OriginZipCode != null || model.DestinationZipCode != null)
                {
                    if (model.OriginZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("ZipCode")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginZipCode)));
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                    if (model.DestinationZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("ZipCode")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationZipCode)));
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
                PdfPCell mRoutecell = new PdfPCell(mQuoteTab);
                mRoutecell.Colspan = 3;
                mQuotebordertable.AddCell(mRoutecell);

                return mQuotebordertable;
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public class ITextEvents : PdfPageEventHelper
        {
            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                string format = "MMMM d, yyyy";
                CultureInfo ci = new CultureInfo("en-US");
                string TaxNum = "";
                if (z == 0)
                {
                    string FullName = (mUserModel.FIRSTNAME != null) ? mUserModel.FIRSTNAME + " " + mUserModel.LASTNAME : mUIModels.GuestName;
                    if (string.IsNullOrWhiteSpace(FullName))
                        FullName = mUserModel.USERID;
                    string Company = (mUserModel.COMPANYNAME != null) ? mUserModel.COMPANYNAME : mUIModels.GuestCompany;
                    string Address = (mUserModel.UAFULLADDRESS != null) ? mUserModel.UAFULLADDRESS : "";
                    string Phone = (mUserModel.WORKPHONE != null) ? (mUserModel.ISDCODE + " - " + mUserModel.WORKPHONE) : "";
                    string EMail = (mUserModel.USERID != null) ? mUserModel.USERID : mUIModels.GuestEmail;
                    if (!string.IsNullOrWhiteSpace(mUserModel.COUNTRYNAME))
                    {
                        if (mUserModel.COUNTRYID == Convert.ToDouble(Country.INDIA))
                        {
                            if (!string.IsNullOrEmpty(mUserModel.VATREGNUMBER))
                                TaxNum = "GST number : " + mUserModel.VATREGNUMBER;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(mUserModel.VATREGNUMBER))
                                TaxNum = "Indirect Tax number : " + mUserModel.VATREGNUMBER;
                        }
                    }
                    PdfPTable mUserTab = new PdfPTable(2);
                    mUserTab.WidthPercentage = 100;
                    mUserTab.SpacingAfter = 0;
                    mUserTab.DefaultCell.Padding = 0;
                    mUserTab.DefaultCell.Border = 0;
                    mUserTab.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                    float[] tblUserwidths = new float[] { 50, 50 };
                    mUserTab.SetWidths(tblUserwidths);

                    int mUserInfoAlign = Element.ALIGN_LEFT;
                    PdfPTable mHeaderUserTable = new PdfPTable(1);
                    mHeaderUserTable.WidthPercentage = 100;
                    mHeaderUserTable.TotalWidth = 230;
                    mHeaderUserTable.DefaultCell.Padding = 0;
                    mHeaderUserTable.HorizontalAlignment = 0;
                    mUserTab.AddCell(mHeaderUserTable);

                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell("For,", mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(FullName, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Company, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Address))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Address, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Phone))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Phone, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(EMail, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(TaxNum, mUserInfoAlign));
                    mUserTab.AddCell(mHeaderUserTable);
                    mHeaderUserTable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 35), writer.DirectContent);
                }
                z++;
                string mQuotationNumber = mUIModels.QuotationNumber;
                string mDateCreated = mUIModels.DateofEnquiry.ToString();
                string mDateofValidity = mUIModels.DateOfValidity.ToString();
                string mIssueDate = "Issue Date" + " : " + ((!String.IsNullOrEmpty(mDateCreated)) ? Convert.ToDateTime(mDateCreated).ToString(format, ci) : string.Empty);
                string mValidTillDate = "Quote valid until" + " : " + ((!String.IsNullOrEmpty(mDateofValidity)) ? Convert.ToDateTime(mDateofValidity).ToString(format, ci) : string.Empty);
                string mShipmentDate = string.Empty;
                string DateOfShipment = mUIModels.DateofShipment.ToString();
                if (String.IsNullOrEmpty(DateOfShipment))
                    mShipmentDate = "DateOfShipment" + " : " + ((!String.IsNullOrEmpty(DateOfShipment)) ? Convert.ToDateTime(DateOfShipment).ToString(format, ci) : string.Empty);

                PdfPTable mHeadermaintable = new PdfPTable(3);
                mHeadermaintable.WidthPercentage = 100;
                mHeadermaintable.SpacingAfter = 0;
                mHeadermaintable.DefaultCell.Padding = 0;
                mHeadermaintable.DefaultCell.Border = 0;
                mHeadermaintable.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                float[] tblwidths = new float[] { 50, 29, 36 };
                mHeadermaintable.SetWidths(tblwidths);

                int mQuoteInfoAlign = Element.ALIGN_LEFT;

                //Company Logo
                PdfPTable mHeaderImageTable = new PdfPTable(1);
                mHeaderImageTable.WidthPercentage = 100;
                mHeadermaintable.TotalWidth = 500;
                mHeaderImageTable.DefaultCell.Padding = 0;
                mHeaderImageTable.DefaultCell.Border = 0;
                PdfPCell imgcel = ImageCell("~/Images/AgilityPrint.png", 48f, mQuoteInfoAlign);
                imgcel.Border = 0;
                imgcel.PaddingBottom = 3;
                mHeaderImageTable.AddCell(imgcel);
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell(mHeaderImageTable);
                mHeadermaintable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 10), writer.DirectContent);
            }
        }

        public static PdfPTable ChargesTableModel(IEnumerable<QuotationChargePreviewModel> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Where(x => x.ISCHARGEINCLUDED == true).Sum(sum => Convert.ToDouble(sum.RoundedTotalPrice)).ToString()); //.ToString("N2");
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                var roundedChargesTotal = mFactoryRound.RoundedValue(Convert.ToString(ChargesTotal), CurrencyId);
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(roundedChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + roundedChargesTotal, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        if (QC.ISCHARGEINCLUDED)
                        {
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.ChargeName, Element.ALIGN_LEFT)));
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.RoundedTotalPrice, Element.ALIGN_RIGHT)));
                        }
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public static PdfPTable ChargesTableAddModel(IEnumerable<PaymentCharges> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Where(x => x.CHARGESOURCE.ToLower() == "f" && (x.CHARGESTATUS != null && (x.CHARGESTATUS.ToLower() == "pay now" || x.CHARGESTATUS.ToLower() == "pay later"))).Sum(sum => sum.NETAMOUNT).ToString());
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                    ChargeSetTotal = Convert.ToString(ChargesTotal);
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(ChargesTotal), CurrencyId), Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.CHARGENAME, Element.ALIGN_LEFT)));
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mFactoryRound.RoundedValue(Convert.ToString(QC.NETAMOUNT), CurrencyId), Element.ALIGN_RIGHT)));
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public static PdfPTable TermsandConditionsTableModel(QuotationPreviewModel model)
        {
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mTACtable = new PdfPTable(1);
                mTACtable.WidthPercentage = 100;
                mTACtable.HeaderRows = 0;
                mTACtable.DefaultCell.Border = 0;
                mTACtable.SetWidths(new float[] { 100 });
                mTACtable.KeepTogether = true;

                PdfPCell quotationheading = mFormattingpdf.DocumentHeading("Terms and Conditions");
                quotationheading.PaddingBottom = -7;
                quotationheading.Border = 0;
                mTACtable.AddCell(quotationheading);

                Phrase mQuoteCondLinephrase = new Phrase();
                mQuoteCondLinephrase.Add(mFormattingpdf.LineSeparator());
                mTACtable.AddCell(mQuoteCondLinephrase);
                mTACtable.AddCell(new PdfPCell() { FixedHeight = 5, Border = 0 });
                int i = 0;
                foreach (var QT in model.TermAndConditions)
                {
                    i = i + 1;
                    PdfPCell mQuoteDesciption = new PdfPCell(mFormattingpdf.TableContentCell(i + "." + QT.DESCRIPTION)) { Padding = 2 };
                    mTACtable.AddCell(mQuoteDesciption);
                }
                return mTACtable;
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public class Formattingpdf
        {
            public Font Heading4 { get; private set; }
            public Font Heading5 { get; private set; }
            public Font Heading6 { get; private set; }
            public Font Heading7 { get; private set; }
            public Font TableCaption7 { get; private set; }
            public Font Heading8 { get; private set; }
            public Font Heading9 { get; private set; }
            public Font Normal { get; private set; }
            public Font NormalRedColor { get; private set; }
            public Font Normal6 { get; private set; }
            public Font NormalItalic6 { get; private set; }
            public Font NormalItalic7 { get; private set; }
            public Font NormalBold { get; private set; }
            public Font BoldForQuoteTotal { get; private set; }

            public BaseColor TableHeadingBgColor { get; private set; }
            public BaseColor TableFooterBgColor { get; private set; }
            public BaseColor TableBorderColor { get; private set; }
            public BaseFont chinesebasefont { get; private set; }
            public BaseFont koreanbasefont { get; private set; }

            public FontSelector selectorTableCaption { get; private set; }
            public FontSelector selectorTableHeading { get; private set; }
            public FontSelector selectorTableContent { get; private set; }

            public Font mChineseFontTabCaption { get; private set; }
            public Font mChineseFontTabContent { get; private set; }
            public Font mChineseFontTabContentItalic { get; private set; }
            public Font mKoreanFontTabCaption { get; private set; }
            public Font mKoreanFontTabContent { get; private set; }
            public Font mKoreanFontTabContentItalic { get; private set; }

            public BaseColor TableCaptionFontColour { get; private set; }
            public BaseColor FontColour { get; private set; }

            public string mQuotePdfOutputLangId { get; private set; }

            public Formattingpdf(string QuoteOutPutLanguage = "")
            {
                TableCaptionFontColour = new BaseColor(175, 40, 46);//(151, 71, 6)

                TableCaption7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, TableCaptionFontColour);
                Heading4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, Font.NORMAL, BaseColor.BLACK);
                Heading5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, Font.NORMAL, BaseColor.BLACK);
                Heading7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                Heading6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, Font.NORMAL, BaseColor.BLACK);
                Heading8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, Font.NORMAL, BaseColor.BLACK);
                Heading9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.NORMAL, BaseColor.BLACK);
                Normal = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
                NormalRedColor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.RED);
                Normal6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
                NormalItalic6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
                NormalItalic7 = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.ITALIC, BaseColor.BLACK);
                NormalBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                FontColour = new BaseColor(176, 41, 46);
                BoldForQuoteTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, Font.NORMAL, FontColour);
                TableHeadingBgColor = new BaseColor(215, 215, 215);
                TableFooterBgColor = new BaseColor(253, 233, 217);
                TableBorderColor = BaseColor.GRAY;
                mQuotePdfOutputLangId = QuoteOutPutLanguage;


                //if (mQuotePdfOutputLangId.EquivalentTo("CHI"))
                //    chinesebasefont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~/PDFFonts/SimSun.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                //else
                //    chinesebasefont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~/PDFFonts/ARIALUNI.TTF"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                //koreanbasefont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~/PDFFonts/ARIALUNI.TTF"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                //For Chinese characters display
                mChineseFontTabCaption = new Font(chinesebasefont, 7, Font.BOLD, TableCaptionFontColour);
                mChineseFontTabContent = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mChineseFontTabContentItalic = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);

                //For Korean characters display
                mKoreanFontTabCaption = new Font(koreanbasefont, 7, Font.BOLD, TableCaptionFontColour);
                mKoreanFontTabContent = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mKoreanFontTabContentItalic = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);

                selectorTableCaption = new FontSelector();
                selectorTableCaption.AddFont(TableCaption7);
                selectorTableCaption.AddFont(mChineseFontTabCaption);
                selectorTableCaption.AddFont(mKoreanFontTabCaption);

                selectorTableContent = new FontSelector();
                selectorTableContent.AddFont(NormalItalic7);
                selectorTableContent.AddFont(mChineseFontTabContentItalic);
                selectorTableContent.AddFont(mKoreanFontTabContentItalic);

                selectorTableHeading = new FontSelector();
            }

            /// <summary>
            /// Document Heading
            /// </summary>
            /// <param name="headName">Tha Document Name</param>
            /// <param name="hAlign">The h Align</param>
            /// <param name="underline"></param>
            /// <returns></returns>
            ///
            public PdfPCell DocumentHeadingIncrease(string headName, int hAlign = Element.ALIGN_RIGHT, bool underline = true)
            {

                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading = new FontSelector();
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 13, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 13, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                //return new PdfPCell(new Phrase(mChunk)) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_LEFT, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            public PdfPCell DocumentHeading(string headName, int hAlign = Element.ALIGN_CENTER, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 10, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                //return new PdfPCell(new Phrase(mChunk)) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            /// <summary>
            /// Subs the heading cell.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <returns></returns>
            public PdfPCell TableCaption(string caption, int vAlign = Element.ALIGN_MIDDLE, int hAlign = Element.ALIGN_LEFT, bool isbold = true, bool IsColor = false, bool isBorder = false)
            {
                //return new PdfPCell(new Phrase(caption, Heading8)) { VerticalAlignment = Element.ALIGN_MIDDLE, Padding = 0, PaddingBottom = 3f, Border = 0 };
                if (isbold)
                {
                    FontSelector selectorTableCaptionbold = new FontSelector();
                    Font TableChiHead7;
                    Font TableKorHead7;
                    if (IsColor == false)
                    {
                        selectorTableCaptionbold.AddFont(Heading7);
                        TableChiHead7 = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                        TableKorHead7 = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                        selectorTableCaptionbold.AddFont(TableChiHead7);
                        selectorTableCaptionbold.AddFont(TableKorHead7);
                    }
                    else
                    {
                        selectorTableCaptionbold.AddFont(TableCaption7);
                        Font TableChiCap7 = new Font(chinesebasefont, 9, Font.BOLD, TableCaptionFontColour);
                        Font TableKorCap7 = new Font(koreanbasefont, 9, Font.BOLD, TableCaptionFontColour);
                        selectorTableCaptionbold.AddFont(TableChiCap7);
                        selectorTableCaptionbold.AddFont(TableKorCap7);
                    }

                    //return new PdfPCell(new Phrase(caption, (IsColor == false ? Heading7 : TableCaption7))) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                    return new PdfPCell(selectorTableCaptionbold.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
                else
                {
                    FontSelector selectorTabCaption = new FontSelector();
                    selectorTabCaption.AddFont(Normal6);
                    Font ChiTableNormal6 = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    Font KorTableNormal6 = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    selectorTabCaption.AddFont(ChiTableNormal6);
                    selectorTabCaption.AddFont(KorTableNormal6);
                    //return new PdfPCell(new Phrase(caption, Normal6)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                    return new PdfPCell(selectorTabCaption.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true, bool isLowFont = false)
            {
                if (isLowFont)
                    return TableHeading(caption, Heading5, hAlign, underline);
                else
                    return TableHeading(caption, Heading6, hAlign, underline);
            }

            /// <summary>
            /// Tables the footer.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <param name="underline">if set to <c>true</c> [underline].</param>
            /// <returns></returns>
            public PdfPCell TableFooter(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
                mPdfCell.BackgroundColor = TableFooterBgColor;
                return mPdfCell;
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="headingFont">The heading font.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, Font headingFont, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                int padding = 3;
                var mChunk = new Chunk(caption, headingFont);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);

                FontSelector selectorTabHeading = new FontSelector();
                selectorTabHeading.AddFont(headingFont);
                Font ChiTabHeading = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabHeading = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabHeading.AddFont(ChiTabHeading);
                selectorTabHeading.AddFont(KorTabHeading);
                //return new PdfPCell(new Phrase(mChunk)) { BorderColor = TableBorderColor, BackgroundColor = TableHeadingBgColor, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = 2, PaddingRight = 2 };//padding - 2
                return new PdfPCell(selectorTabHeading.Process(mChunk.ToString())) { BorderColor = TableBorderColor, BackgroundColor = TableHeadingBgColor, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = 2, PaddingRight = 2 };//padding - 2
            }

            /// <summary>
            /// Tables Content
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCell(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabContentCell = new FontSelector();
                selectorTabContentCell.AddFont(Normal);
                Font ChiTabContentcell = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabContentcell = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabContentCell.AddFont(ChiTabContentcell);
                selectorTabContentCell.AddFont(KorTabContentcell);
                //return new PdfPCell(new Phrase(cellData, Normal)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
                return new PdfPCell(selectorTabContentCell.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BorderWidth = 5, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            public PdfPCell TableContentCellNormalandItalic(string cellData, string cellItalicData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                Paragraph mPgh = new Paragraph();
                FontSelector selectorNormal = new FontSelector();
                selectorNormal.AddFont(Normal);
                Font fntChi1 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor1 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormal.AddFont(fntChi1);
                selectorNormal.AddFont(fntKor1);

                Phrase ph1 = selectorNormal.Process(cellData); //new Phrase(cellData, Normal);
                FontSelector selectorNormItalic = new FontSelector();
                selectorNormItalic.AddFont(Normal);
                Font fntChi2 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor2 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormItalic.AddFont(fntChi2);
                selectorNormItalic.AddFont(fntKor2);
                Phrase ph2 = selectorNormItalic.Process(cellItalicData); //new Phrase(cellItalicData, NormalItalic7);
                mPgh.Add(ph1);
                if (!string.IsNullOrEmpty(cellItalicData))
                    mPgh.Add(ph2);
                return new PdfPCell(mPgh) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content Italic
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellItalic(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellItalic = new FontSelector();
                selectorTabConentcellItalic.AddFont(NormalItalic7);
                Font ChiContentcell = new Font(chinesebasefont, 9, Font.ITALIC, BaseColor.BLACK);
                Font KorContentcell = new Font(koreanbasefont, 9, Font.ITALIC, BaseColor.BLACK);
                selectorTabConentcellItalic.AddFont(ChiContentcell);
                selectorTabConentcellItalic.AddFont(KorContentcell);
                //return new PdfPCell(new Phrase(cellData, NormalItalic7)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
                return new PdfPCell(selectorTabConentcellItalic.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content with red color
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellRedColor(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellRedColor = new FontSelector();
                selectorTabConentcellRedColor.AddFont(NormalRedColor);
                Font fntChiRed = new Font(chinesebasefont, 10, Font.NORMAL, BaseColor.RED);
                Font fntKorRed = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.RED);
                selectorTabConentcellRedColor.AddFont(fntChiRed);
                selectorTabConentcellRedColor.AddFont(fntKorRed);
                //return new PdfPCell(new Phrase(cellData, NormalRedColor)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
                return new PdfPCell(selectorTabConentcellRedColor.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBold(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBold = new FontSelector();
                selectorTabConentcellBold.AddFont(NormalBold);
                Font fntChiBold = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                Font fntKorBold = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                selectorTabConentcellBold.AddFont(fntChiBold);
                selectorTabConentcellBold.AddFont(fntKorBold);

                //return new PdfPCell(new Phrase(cellData, NormalBold)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
                return new PdfPCell(selectorTabConentcellBold.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold for Quotation Grand Total
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldForQuoteTotal(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBoldQT = new FontSelector();
                selectorTabConentcellBoldQT.AddFont(BoldForQuoteTotal);
                Font fntChiBorderQT = new Font(chinesebasefont, 10, Font.BOLD, FontColour);
                Font fntKorBorderQT = new Font(koreanbasefont, 10, Font.BOLD, FontColour);
                selectorTabConentcellBoldQT.AddFont(fntChiBorderQT);
                selectorTabConentcellBoldQT.AddFont(fntKorBorderQT);

                //return new PdfPCell(new Phrase(cellData, BoldForQuoteTotal)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding };
                return new PdfPCell(selectorTabConentcellBoldQT.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding };
            }

            /// <summary>
            /// Table Content Cell Data Bold,Underline
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="underline"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldUnderline(string cellData, int hAlign = Element.ALIGN_LEFT, bool underline = false)
            {
                int padding = 2;
                var mChunk = new Chunk(cellData, Heading6);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                FontSelector selectorTabConentcellBoldline = new FontSelector();
                selectorTabConentcellBoldline.AddFont(Heading6);
                Font fntChiBoldline = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                Font fntKorBoldline = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                selectorTabConentcellBoldline.AddFont(fntChiBoldline);
                selectorTabConentcellBoldline.AddFont(fntKorBoldline);

                //return new PdfPCell(new Phrase(mChunk)) { Border = 0, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
                return new PdfPCell(selectorTabConentcellBoldline.Process(mChunk.ToString())) { Border = 0, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }



            ///// <summary>
            ///// Tables Content with border.
            ///// </summary>
            ///// <param name="caption">The caption.</param>
            ///// <param name="hAlign">The h align.</param>
            ///// <param name="underline">if set to <c>true</c> [underline].</param>
            ///// <returns></returns>
            //public PdfPCell TableContentBorder(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            //{
            //    var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
            //    mPdfCell.BackgroundColor = BaseColor.WHITE;
            //    return mPdfCell;
            //}



            ///// <summary>
            ///// Lines the separator.
            ///// </summary>
            ///// <returns></returns>
            public Chunk LineSeparator()
            {
                return new Chunk(new LineSeparator(4f, 100f, TableHeadingBgColor, Element.ALIGN_CENTER, -1));
            }

            public Chunk ChunkText(string caption, bool isColor = false)
            {
                if (isColor)
                    return new Chunk(caption, TableCaption7);
                else
                    return new Chunk(caption, Heading7);
            }



            #region Impersonate Code
            /// <summary>
            /// Object to change the user authticated
            /// </summary>

            public class UserImpersonation : IDisposable
            {
                /// <summary>
                /// Logon method (check athetification) from advapi32.dll
                /// </summary>
                /// <param name="lpszUserName"></param>
                /// <param name="lpszDomain"></param>
                /// <param name="lpszPassword"></param>
                /// <param name="dwLogonType"></param>
                /// <param name="dwLogonProvider"></param>
                /// <param name="phToken"></param>
                /// <returns></returns>
                [DllImport("advapi32.dll")]
                private static extern bool LogonUser(String lpszUserName,
                    String lpszDomain,
                    String lpszPassword,
                    int dwLogonType,
                    int dwLogonProvider,
                    ref IntPtr phToken);

                /// <summary>
                /// Close
                /// </summary>
                /// <param name="handle"></param>
                /// <returns></returns>
                [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
                public static extern bool CloseHandle(IntPtr handle);

                private WindowsImpersonationContext _windowsImpersonationContext;
                private IntPtr _tokenHandle;
                private string _userName;
                private string _domain;
                private string _passWord;

                const int LOGON32_PROVIDER_DEFAULT = 0;
                const int LOGON32_LOGON_INTERACTIVE = 2;

                /// <summary>
                /// Initialize a UserImpersonation
                /// </summary>
                /// <param name="userName"></param>
                /// <param name="domain"></param>
                /// <param name="passWord"></param>
                public UserImpersonation(string userName, string domain, string passWord)
                {
                    _userName = userName;
                    _domain = domain;
                    _passWord = passWord;
                }

                /// <summary>
                /// Valiate the user inforamtion
                /// </summary>
                /// <returns></returns>
                public bool ImpersonateValidUser()
                {
                    bool returnValue = LogonUser(_userName, _domain, _passWord,
                            LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                            ref _tokenHandle);

                    if (false == returnValue)
                    {
                        return false;
                    }

                    WindowsIdentity newId = new WindowsIdentity(_tokenHandle);
                    _windowsImpersonationContext = newId.Impersonate();
                    return true;
                }

                #region IDisposable Members

                /// <summary>
                /// Dispose the UserImpersonation connection
                /// </summary>
                public void Dispose()
                {
                    if (_windowsImpersonationContext != null)
                        _windowsImpersonationContext.Undo();
                    if (_tokenHandle != IntPtr.Zero)
                        CloseHandle(_tokenHandle);
                }

                #endregion
            }
            #endregion


        }

        //Get's all site city name for offline payment (Branch List)       
        public JsonResult FillBranchCity()
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                IList<BranchCityEntity> city = JBFactory.GetBranchCity(HttpContext.User.Identity.Name.ToUpper());
                if (city != null)
                {
                    var Citylist = (
                             from items in city
                             select new
                             {
                                 Text = items.NAME,
                                 Value = items.NAME
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(Citylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult FillWireCity(string currency)
        {
            try
            {
                var db = new JobBookingDbFactory();
                var city = db.GetWireCity(HttpContext.User.Identity.Name.ToUpper(), currency.ToUpper());
                return Json(city);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult GetBranList(string currency)
        {
            try
            {
                var db = new JobBookingDbFactory();
                var city = db.GetBranList(HttpContext.User.Identity.Name.ToUpper(), currency.ToUpper());
                return Json(city);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public async Task<JsonResult> CurrencyConversion(string amount, string currency, string tocurrency)
        {
            try
            {
                return await Task<dynamic>.Run(() =>
                {
                    var db = new JobBookingDbFactory();
                    var value = db.GetCurrencyConversion(amount, currency.ToUpper(), tocurrency.ToUpper());
                    return Json(new { Data = value.Rows[0]["currency"] });
                });
            }
            catch (Exception ex)
            {
                return Json(new { Error = "Something Went Wrong" });
            }
        }


        public JsonResult FillBranchCityM(string user)
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                IList<BranchCityEntity> city = JBFactory.GetBranchCity(user.ToUpper());
                if (city != null)
                {
                    var Citylist = (
                             from items in city
                             select new
                             {
                                 Text = items.NAME,
                                 Value = items.NAME
                             }).Distinct().ToList().OrderBy(x => x.Text);
                    return Json(Citylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult FindMyLocation(string location)
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                IList<SSPBranchListEntity> locations = JBFactory.GetMyLocation(location);
                return Json(locations, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        private void SendMailForSuccessBusinessCredit(PaymentModel miUIModel, byte[] bytes, QuotationPreviewModel mUIModels, string FirstName = "", string additionalChargeAmount = "")
        {
            try
            {


                string AttachmentIds = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                DataSet documents = mFactory.GetDocuments(Convert.ToInt32(miUIModel.JobNumber));
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(miUIModel.UserId));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - payment successful through Business credit" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = Server.MapPath("~/App_Data/payment-credit-success.html");
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                if (FirstName == "")
                    body = body.Replace("{user_name}", (Request.Cookies["myCookie"].Values["UserName"].ToString()));
                else
                    body = body.Replace("{user_name}", FirstName);
                body = body.Replace("{BookingID}", miUIModel.OPjobnumber);
                body = body.Replace("{currency}", miUIModel.Currency);
                if (additionalChargeAmount == "")
                {
                    body = body.Replace("{Amount}", mFactoryRound.RoundedValue(miUIModel.ReceiptNetAmount.ToString(), miUIModel.Currency));
                }
                else
                {
                    body = body.Replace("{Amount}", mFactoryRound.RoundedValue(additionalChargeAmount, miUIModel.Currency));
                }
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);

                string Tbody = "";
                if (mUIModels.MovementTypeName.ToUpper() == "DOOR TO PORT" || mUIModels.MovementTypeName.ToUpper() == "PORT TO PORT")
                {
                    Tbody = PrepareShipmentTrack("P");
                    body = body.Replace("{shpnttracking}", Tbody);
                }
                else
                {
                    Tbody = PrepareShipmentTrack("D");
                    body = body.Replace("{shpnttracking}", Tbody);
                }

                string refNumber = "BC" + miUIModel.OPjobnumber;
                //if (bytes != null)
                //    message.Attachments.Add(new Attachment(new MemoryStream(bytes), refNumber + ".pdf"));
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.IsBodyHtml = true;
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", miUIModel.UserId, "", "",
        "JobBooking", "BusinessCredit", miUIModel.OPjobnumber, miUIModel.UserId, body, AttachmentIds, true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        private void SendMailForSuccessPayAtBranch(PaymentModel miUIModel, string paymentType, QuotationPreviewModel mUIModels, PaymentModel model, string branchAddress, int jobNumber, string flowType, string FirstName = "", string additionalChargeAmount = "")
        {
            try
            {
                string AttachmentIds = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                DataSet documents = mFactory.GetDocuments(jobNumber);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(miUIModel.UserId));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - " + flowType + " in progress" + "( Booking ID #" + model.OPjobnumber + " )" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                if (model.TransactionMode.ToLower() == "cash")
                {
                    mapPath = Server.MapPath("~/App_Data/payment-PayAtBranch-success.html");
                }
                else
                {
                    mapPath = Server.MapPath("~/App_Data/payment-PayAtBranch-success-cheque.html");
                }
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                if (FirstName == "")
                {
                    UserDBFactory UD = new UserDBFactory();
                    UserEntity UE = UD.GetUserFirstAndLastName(model.UserId);
                    body = body.Replace("{user_name}", UE.FIRSTNAME);
                }
                else
                {
                    body = body.Replace("{user_name}", FirstName);
                }
                if (additionalChargeAmount == "")
                {
                    body = body.Replace("{Amount}", model.Currency.ToString() + " " + mFactoryRound.RoundedValue(Convert.ToString(model.ReceiptNetAmount), model.Currency.ToString()));
                }
                else
                {
                    body = body.Replace("{Amount}", model.Currency.ToString() + " " + mFactoryRound.RoundedValue(additionalChargeAmount, model.Currency.ToString()));
                }

                body = body.Replace("{PaymentMode}", model.PaymentType);
                body = body.Replace("{TransactionMode}", model.TransactionMode);
                string branchname = branchAddress;
                body = body.Replace("{Branch}", branchname);
                body = body.Replace("{TransDate}", Convert.ToString(model.TransChequeDate.Value.ToShortDateString()));
                body = body.Replace("{OpJobNumber}", model.OPjobnumber);


                if (model.TransactionMode.ToLower() == "cash")
                {
                    body = body.Replace("{Comment}", model.TransDescription);
                }
                else
                {

                    body = body.Replace("{TransDate}", Convert.ToString(model.TransChequeDate.Value.ToShortDateString()));
                    body = body.Replace("{TransBankName}", model.TransBankName);
                    body = body.Replace("{TransBranchName}", model.TransBranchName);
                    body = body.Replace("{TransChequeNo}", model.TransChequeNo);
                    body = body.Replace("{TransChequeDate}", Convert.ToString(model.TransChequeDate.Value.ToShortDateString()));
                    body = body.Replace("{TransCDDescription}", model.TransCDDescription);
                }
                //body = body.Replace("{PaymentType}", paymentType);
                //body = body.Replace("{Reference_No}", miUIModel.OPjobnumber);
                //body = body.Replace("{currency}", miUIModel.Currency);
                //body = body.Replace("{Track_Cons}", Track);
                //body = body.Replace("{Ports_cons}", Ports);
                //if (additionalChargeAmount == "")
                //{
                //    body = body.Replace("{Amount}", miUIModel.ReceiptNetAmount.ToString());
                //}
                //else
                //{
                //    body = body.Replace("{Amount}", additionalChargeAmount);
                //}
                //body = body.Replace("{BookingID}", miUIModel.OPjobnumber.ToString());
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", miUIModel.UserId, "", "",
      "JobBooking", paymentType, miUIModel.OPjobnumber, miUIModel.UserId, body, AttachmentIds, true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        [HttpPost]
        public JsonResult ShareBenefeciary(string email, string bookingid, string accholdername, string bankname, string branch, string accountno, string swiftcode, string ifscibancode)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(email));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - Benefeciary details" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = Server.MapPath("~/App_Data/BenefeciaryDetails.html");
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{user_name}", "");
                body = body.Replace("{Booking_Id}", bookingid);
                body = body.Replace("{AccHolder_Name}", accholdername);
                body = body.Replace("{Bank_Name}", bankname);
                body = body.Replace("{Branch}", branch);
                body = body.Replace("{Account_No}", accountno);
                body = body.Replace("{Swift_Code}", swiftcode);
                body = body.Replace("{IFSC_Code}", ifscibancode);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", email, "", "",
      "JobBooking", "Benefeciary", bookingid, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        public JsonResult getCountryWeekends(Int64 originCountryID, Int64 destinationCountryID)
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                IList<GetCountryWeekends> weekOff = JBFactory.GetCountryWeekends(originCountryID, destinationCountryID);
                if (weekOff != null)
                {
                    var weekOfflist = (
                             from items in weekOff
                             select new
                             {
                                 Text = items.COUNTRYID,
                                 Value = items.WEEKOFF
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(weekOfflist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult getCountryHolidays(Int64 originCountryID, Int64 destinationCountryID)
        {
            JobBookingDbFactory JBFactory = new JobBookingDbFactory();
            var ds = JBFactory.GetCountryHolidays(originCountryID, destinationCountryID);
            List<Dictionary<string, object>> trows = ReturnJsonResults(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public static List<Dictionary<string, object>> ReturnJsonResults(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().Contains("DATE"))
                    {
                        if (dr[col].ToString().Trim() != "")
                        {
                            DateTime colDateTime = Convert.ToDateTime(Convert.ToString(dr[col]));
                            temp_row.Add(col.ColumnName, string.IsNullOrEmpty(Convert.ToString(colDateTime)) ? string.Empty : colDateTime.ToString("dd-MMM-yyyy"));
                        }
                        else
                        {
                            temp_row.Add(col.ColumnName, dr[col]);
                        }
                    }
                    else
                    {
                        temp_row.Add(col.ColumnName, dr[col]);
                    }
                }
                trows.Add(temp_row);
            }
            return trows;
        }

        public JsonResult CheckExistingJob(int quotationId)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var ds = mFactory.CheckExistingJob(Convert.ToInt32(quotationId), HttpContext.User.Identity.Name);
            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        private void SendMailForCountryFinanceCashier(string role, PaymentModel model, string branchAddress, string flowType, string FirstName = "", string additionalChargeAmount = "", QuotationEntity quotation = null, PaymentEntity payment = null)
        {
            try
            {
                string AttachmentIds = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                DataSet documents = mFactory.GetDocuments(Convert.ToInt32(model.JobNumber));
                string recipientsList = "";
                string roles = string.Empty;
                if (role.ToLower() == "cashier")
                {
                    roles = "SSPCOUNTRYCASHIER";
                }
                else if (role.ToLower() == "finance")
                {
                    roles = "SSPCOUNTRYFINANCE";
                }
                recipientsList = mFactory.GetEmailList(roles, model.OPjobnumber, "Paymentstatus");
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                if (recipientsList == "") { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId)); //Adding Multiple BCC email Id
                    }
                }
                string DevEmailids = System.Configuration.ConfigurationManager.AppSettings["GlobalPaymentNotifier"];
                string[] ids = DevEmailids.Split(',');
                foreach (var item in ids)
                {
                    message.Bcc.Add(new MailAddress(item));
                }
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - offline " + flowType.ToLower() + " notification" + "(Booking ID #" + model.OPjobnumber + ")" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                if (model.PaymentType.ToLower() == "wire transfer")
                {
                    mapPath = Server.MapPath("~/App_Data/WireTransferNotification.html");
                }
                else if (model.TransactionMode.ToLower() == "cash")
                {
                    mapPath = Server.MapPath("~/App_Data/CashNotification.html");
                }
                else
                {
                    mapPath = Server.MapPath("~/App_Data/ChequeNotification.html");
                }
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(model.UserId);
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                if (FirstName == "")
                {
                    body = body.Replace("{user_name}", UE.FIRSTNAME);
                }
                else
                {
                    body = body.Replace("{user_name}", FirstName);
                }
                body = body.Replace("{country}", UE.COUNTRYNAME);
                if (additionalChargeAmount == "")
                {
                    body = body.Replace("{Amount}", model.Currency.ToString() + " " + mFactoryRound.RoundedValue(Convert.ToString(model.ReceiptNetAmount), model.Currency.ToString()));
                }
                else
                {
                    body = body.Replace("{Amount}", model.Currency.ToString() + " " + mFactoryRound.RoundedValue(additionalChargeAmount, model.Currency.ToString()));
                }

                body = body.Replace("{PaymentMode}", model.PaymentType);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
                if (model.PaymentType.ToLower() == "wire transfer")
                {
                    string branchname = model.EntityList.BankName + " " + model.EntityList.Branch;

                    body = body.Replace("{User}", model.UserId);
                    body = body.Replace("{BeneficiaryDetails}", "Benefeciary details:");
                    body = body.Replace("{AccHolderName}", model.EntityList.BeneficiaryName);
                    body = body.Replace("{BankBranchName}", branchname);
                    body = body.Replace("{AccountNo}", model.EntityList.BankAccountNumber);
                    body = body.Replace("{SwiftCode}", model.EntityList.SwiftCode);
                    body = body.Replace("{IFSCIBANCode}", model.EntityList.IFSCCode);

                    body = body.Replace("{TransactionDetails}", "Transaction details:");
                    body = body.Replace("{TransBank}", model.WTTransBankName);
                    body = body.Replace("{TransBranchName}", model.WTTransBranchName);
                    body = body.Replace("{TransReffNo}", model.WTTransChequeNo);
                    body = body.Replace("{TransDate}", Convert.ToString(model.WTTransChequeDate.Value.ToShortDateString()));
                    body = body.Replace("{TransDesc}", model.WTTransDescription);
                }
                else if (model.PaymentType.ToLower() == "pay at branch")
                {
                    body = body.Replace("{TransactionMode}", model.TransactionMode);
                    body = body.Replace("{Branch}", branchAddress);
                    body = body.Replace("{TransDate}", Convert.ToString(model.TransChequeDate.Value.ToShortDateString()));
                    if (model.TransactionMode.ToLower() == "cash")
                    {
                        body = body.Replace("{Comment}", model.TransDescription);
                    }
                    else
                    {

                        body = body.Replace("{TransDate}", Convert.ToString(model.TransChequeDate.Value.ToShortDateString()));
                        body = body.Replace("{TransBankName}", model.TransBankName);
                        body = body.Replace("{TransBranchName}", model.TransBranchName);
                        body = body.Replace("{TransChequeNo}", model.TransChequeNo);
                        body = body.Replace("{TransChequeDate}", Convert.ToString(model.TransChequeDate.Value.ToShortDateString()));
                        body = body.Replace("{TransCDDescription}", model.TransCDDescription);
                    }
                }
                body = body.Replace("{OpJobNumber}", model.OPjobnumber);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);               
                body = body.Replace("{Mode}", quotation.PRODUCTNAME);
                body = body.Replace("{MovementType}", quotation.MOVEMENTTYPENAME);
                body = body.Replace("{OriginCountry}", quotation.OCOUNTRYNAME);
                body = body.Replace("{DestinationCountry}", quotation.DCOUNTRYNAME);
                body = body.Replace("{PayingCountry}", UE.COUNTRYNAME);
                string placeportconent = string.Empty;
                string specilaHandlingRequirements = string.Empty;
                placeportconent = PrapareCityPortData(quotation);     
                body = body.Replace("{placeportconent}", placeportconent);
                specilaHandlingRequirements = PrepareSpecialHandlingInstructions(payment);
                body = body.Replace("{specilaHandlingRequirements}", specilaHandlingRequirements);
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
      "JobBooking", "Offline payment", model.OPjobnumber, model.UserId, body, AttachmentIds, false, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        private void SendMailForCountryOperations(string role, string OPjobnumber, string FirstName = "")
        {
            try
            {
                string AttachmentIds = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                string recipientsList = "";

                recipientsList = mFactory.GetEmailList(role, OPjobnumber, "DOCUMENTSUPLOAD");
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                if (recipientsList == "")
                {
                    message.To.Add(new MailAddress("vsivaram@agility.com"));
                    message.To.Add(new MailAddress("help@shipafreight.com"));
                }
                else
                {
                    message.To.Add(recipientsList); //Adding Multiple To email Id
                }

                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;

                mapPath = Server.MapPath("~/App_Data/MandatoryDocs.html");

                // using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                string EnvironmentName = DynamicClass.GetEnvironmentName();
                string ServerEnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                message.Subject = "Shipa Freight - Document upload" + "-" + "( Booking ID #" + OPjobnumber + " )" + EnvironmentName;

                if (ServerEnvironmentName == "PRD")
                {
                    body = body.Replace("{documentreq}", "https://focis.agility.com");
                }
                else if (ServerEnvironmentName == "AGL")
                {
                    body = body.Replace("{documentreq}", "https://focisagile.agility.com/login.aspx");
                }
                else if (ServerEnvironmentName == "SIT")
                {
                    body = body.Replace("{documentreq}", "https://focissit.agility.com/login.aspx");
                }
                else if (ServerEnvironmentName == "DEM")
                {
                    body = body.Replace("{documentreq}", "https://focisdemo.agility.com/login.aspx");
                }
                else
                {
                    body = body.Replace("{documentreq}", "https://focisagile.agility.com/login.aspx");
                }

                string Content = "We have received a new document uploaded by the customer [" + HttpContext.User.Identity.Name.ToString() + "] through Shipa Freight (booking ID #" + OPjobnumber + ")";
                string Content1 = "Please login to your FOCIS Shipa Freight Booking dashboard to review details of the documents.";
                body = body.Replace("{user}", "Team");
                body = body.Replace("{Message}", "<p>" + Content + "</p>");
                body = body.Replace("{Message1}", "<p>" + Content1 + "</p>");
                body = body.Replace("{OpJobNumber}", OPjobnumber);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                body = body.Replace("{btnName}", "View Booking");
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
      "JobBooking", "Offline payment", OPjobnumber, HttpContext.User.Identity.Name.ToString(), body, AttachmentIds, false, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        #region PDF related Methods by Sindhu

        [AllowAnonymous]
        public void CallAdvanceReceiptPDF(int jobnumber, string ReportName, Int64 QuotaionID, string OperationJobNumber, string CreatedBy)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                byte[] mbytes = GenerateAdvanceReceiptPDF(jobnumber, ReportName, QuotaionID, OperationJobNumber, CreatedBy, 0);

                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline; filename=FX_AdvanceCashReceipt.pdf");
                Response.Buffer = true;
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.BinaryWrite(mbytes);
                Response.End();
                Response.Close();
            }
        }

        [AllowAnonymous]
        public void CallBookingConfirmationPDF(int jobnumber, string ReportName, Int64 QuotaionID, string OperationJobNumber, string CreatedBy)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                byte[] mbytes = GenerateBookingConfirmationPDF(jobnumber, ReportName, QuotaionID, OperationJobNumber, CreatedBy);

                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline; filename=FX_Booking Confirmation to Customer.pdf");
                Response.Buffer = true;
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.BinaryWrite(mbytes);
                Response.End();
                Response.Close();
            }
        }



        public static byte[] GenerateAdvanceReceiptPDF(int jobnumber, string ReportName, Int64 QuotaionID, string OperationJobNumber, string CreatedBy, int ChargeSetId)
        {
            MemoryStream memoryStream = new MemoryStream();
            string discountvalue = string.Empty;
            string SumAdditionalCharges = string.Empty;
            string TCount = string.Empty;
            DataTable AdvReceiptChargeDtls = null;
            PDFpage = 0;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();

            DBFactory.Entities.AdvanceReceiptEntity AdvReceiptDtls = mFactory.GetAdvanceReceiptDtls(Convert.ToInt32(jobnumber));
            DataTable ShipmentDtls = mFactory.GetShipmentDtls(jobnumber);
            if (ChargeSetId <= 0)
            {
                discountvalue = mFactory.GETDiscounts(Convert.ToInt32(jobnumber));
                AdvReceiptChargeDtls = mFactory.GetChargeDtls(jobnumber);
            }
            else
            {
                AdvReceiptChargeDtls = mFactory.GetAdditionalChargeSum(jobnumber, ChargeSetId);
            }
            DataTable UserDtls = mFactory.GetUserDtls(jobnumber);


            var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.CurrencySymbol = "";


            DataTable VATDtls = mFactory.GetVATDtls(jobnumber);
            string origintotalprice = string.Empty;
            string desttotalprice = string.Empty;
            string addtotalprice = string.Empty;
            string distotalprice = string.Empty;
            string intfrgttotalprice = string.Empty;

            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(AdvReceiptDtls.PREFERREDCURRENCYID);

            double totalamount = 0;
            string Total = string.Empty;

            double decorigintotalprice = 0, decintfrgttotalprice = 0, decdesttotalprice = 0, decaddtotalprice = 0, decdiscount = 0;

            if (ChargeSetId > 0)
            {
                if (AdvReceiptChargeDtls.Rows.Count > 0)
                {
                    SumAdditionalCharges = AdvReceiptChargeDtls.Rows[0]["TOTALPRICE"].ToString();
                    TCount = AdvReceiptChargeDtls.Rows[0]["TCOUNT"].ToString();
                }
            }
            else
            {
                foreach (DataRow row in AdvReceiptChargeDtls.Rows)
                {
                    string ROUTETYPEID = row["ROUTETYPEID"].ToString();
                    string TOTALPRICE = row["TOTALPRICE"].ToString();

                    if (ROUTETYPEID == "1118")//"Origin"
                    {
                        origintotalprice = TOTALPRICE;//
                    }
                    if (ROUTETYPEID == "1116")//"international freight"
                    {
                        intfrgttotalprice = TOTALPRICE;
                    }
                    if (ROUTETYPEID == "1117")//"Destination"
                    {
                        desttotalprice = TOTALPRICE;
                    }
                    if (ROUTETYPEID == "250001")//"Additional"
                    {
                        addtotalprice = TOTALPRICE;
                    }
                    distotalprice = discountvalue;
                }
            }

            Document doc = new Document(PageSize.A4, 50, 50, 100, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();

            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = "Advance Receipt";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);

            string VATRegNo = string.Empty;
            if (VATDtls.Rows.Count > 0)
            {
                VATRegNo = VATDtls.Rows[0]["VATREGNUMBER"].ToString();
            }



            string ReceivedFrom = string.Empty;
            if (UserDtls.Rows.Count > 0)
            {
                ReceivedFrom = UserDtls.Rows[0]["FIRSTNAME"].ToString() + "," + UserDtls.Rows[0]["FULLADDRESS"].ToString();
            }

            //////////////////////

            Font boldFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
            Font normalFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new float[] { 5f, 47.5f, 47.5f });
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 100;
            table.SpacingBefore = 10;

            PdfPCell verticalcell = new PdfPCell(new Phrase("", new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL)));//if needed replace empty string with desired string to show vertical message in document.
            verticalcell.Rotation = 90;
            verticalcell.HorizontalAlignment = Element.ALIGN_CENTER;
            verticalcell.VerticalAlignment = Element.ALIGN_TOP;
            verticalcell.Border = 0;
            verticalcell.Rowspan = 6;
            table.AddCell(verticalcell);

            Phrase phrase = new Phrase();
            phrase.Add(
                new Chunk("Received From :", boldFont)
            );
            phrase.Add(Chunk.NEWLINE);
            if (ReceivedFrom != string.Empty)
            {
                for (int j = 0; j < ReceivedFrom.Split(',').Length; j++)
                {
                    if (ReceivedFrom.Split(',')[j].Trim() != string.Empty)
                    {
                        phrase.Add(new Chunk(ReceivedFrom.Split(',')[j].Trim(), normalFont));
                        phrase.Add(Chunk.NEWLINE);
                    }
                }
            }
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(
                new Chunk("Customer VAT Registered No :", boldFont)
            );
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk(VATRegNo, normalFont));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(Chunk.NEWLINE);

            if (ShipmentDtls.Rows[0]["ISHAZARDOUSCARGO"].ToString() != "0")
            {
                phrase.Add(
                    new Chunk("Hazardous Cargo", boldFont)
                );
                phrase.Add(new Chunk(" : " + ShipmentDtls.Rows[0]["HAZARDOUSGOODSTYPE"].ToString(), normalFont));
                phrase.Add(Chunk.NEWLINE);
                phrase.Add(Chunk.NEWLINE);
            }


            PdfPCell cell1 = new PdfPCell();
            cell1.PaddingLeft = 10;
            cell1.PaddingTop = 10;
            cell1.Colspan = 1;
            cell1.AddElement(phrase);


            table.AddCell(cell1);

            Phrase phrase1 = new Phrase();
            phrase1.Add(
                new Chunk("Receipt No", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            if (ChargeSetId <= 0)
            {
                phrase1.Add(new Chunk(": " + AdvReceiptDtls.RECEIPTNUMBER, normalFont));
            }
            else
            {
                phrase1.Add(new Chunk(": " + AdvReceiptDtls.RECEIPTNUMBER + "AR" + ChargeSetId, normalFont));
            }



            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Date of Issue", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            phrase1.Add(new Chunk(": " + AdvReceiptDtls.RECEIPTDATE, normalFont));

            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Payment Type", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            phrase1.Add(new Chunk(": " + AdvReceiptDtls.PAYMENTOPTION + " - " + AdvReceiptDtls.PAYMENTTYPE, normalFont));

            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Transaction Reference ID :", boldFont)
            );
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(new Chunk(AdvReceiptDtls.PAYMENTREFNUMBER, normalFont));
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);

            PdfPCell cell2 = new PdfPCell();
            cell2.PaddingLeft = 10;
            cell2.PaddingTop = 10;
            cell2.Colspan = 1;
            cell2.AddElement(phrase1);
            table.AddCell(cell2);


            Phrase bookingphrase = new Phrase();
            bookingphrase.Add(
                new Chunk("Booking ID :", boldFont)
            );
            bookingphrase.Add(Chunk.SPACETABBING);
            bookingphrase.Add(new Chunk(AdvReceiptDtls.OPERATIONALJOBNUMBER, normalFont));
            bookingphrase.Add(Chunk.NEWLINE);
            bookingphrase.Add(Chunk.NEWLINE);


            PdfPCell bookingcell = new PdfPCell();
            bookingcell.PaddingLeft = 10;
            bookingcell.PaddingTop = 10;
            bookingcell.AddElement(bookingphrase);
            bookingcell.Colspan = 2;
            table.AddCell(bookingcell);


            Phrase chargedescphrase = new Phrase();
            chargedescphrase.Add(
                new Chunk("Charges Description", boldFont)
            );
            chargedescphrase.Add(Chunk.NEWLINE);
            chargedescphrase.Add(Chunk.NEWLINE);

            PdfPCell chargedesccell = new PdfPCell();
            chargedesccell.PaddingLeft = 10;
            chargedesccell.PaddingTop = 10;
            chargedesccell.Colspan = 1;
            chargedesccell.AddElement(chargedescphrase);
            table.AddCell(chargedesccell);


            Phrase amountphrase = new Phrase();
            amountphrase.Add(new Chunk(new VerticalPositionMark()));
            amountphrase.Add(
                new Chunk("Amount", boldFont)
                );
            amountphrase.Add(Chunk.NEWLINE);
            amountphrase.Add(Chunk.NEWLINE);

            PdfPCell amountcell = new PdfPCell();
            amountcell.PaddingRight = 10;
            amountcell.PaddingTop = 10;
            amountcell.Colspan = 1;
            amountcell.AddElement(amountphrase);
            table.AddCell(amountcell);
            Phrase chargesphrase = new Phrase();
            if (ChargeSetId <= 0)
            {
                chargesphrase.Add(
                    new Chunk("Origin Charges", normalFont)
                    );

                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                chargesphrase.Add(
                    new Chunk("International Freight Charges", normalFont)
                    );

                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                chargesphrase.Add(
                    new Chunk("Destination Charges", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                chargesphrase.Add(
                       new Chunk("Optional Charges", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                if (distotalprice != "0")
                {
                    chargesphrase.Add(
                        new Chunk("Discount", normalFont)
                        );
                    chargesphrase.Add(Chunk.NEWLINE);
                    chargesphrase.Add(Chunk.NEWLINE);
                }
            }
            else
            {
                if (SumAdditionalCharges != "0" && SumAdditionalCharges != "")
                {
                    chargesphrase.Add(
                       new Chunk("Additional Charges", normalFont)
                       );
                    chargesphrase.Add(Chunk.NEWLINE);
                    chargesphrase.Add(Chunk.NEWLINE);
                }
            }




            //if (origintotalprice != string.Empty)
            //    decorigintotalprice = Convert.ToDecimal(origintotalprice);
            //if (intfrgttotalprice != string.Empty)
            //    decintfrgttotalprice = Convert.ToDecimal(intfrgttotalprice);
            //if (desttotalprice != string.Empty)
            //    decdesttotalprice = Convert.ToDecimal(desttotalprice);
            //if (addtotalprice != string.Empty)
            //    decaddtotalprice = Convert.ToDecimal(addtotalprice);
            //if (distotalprice != string.Empty)
            //    decdiscount = Convert.ToDecimal(distotalprice);

            if (origintotalprice != string.Empty)
                decorigintotalprice = Double.Parse(origintotalprice);
            if (intfrgttotalprice != string.Empty)
                decintfrgttotalprice = Double.Parse(intfrgttotalprice);
            if (desttotalprice != string.Empty)
                decdesttotalprice = Double.Parse(desttotalprice);
            if (addtotalprice != string.Empty)
                decaddtotalprice = Double.Parse(addtotalprice);
            if (distotalprice != string.Empty)
                decdiscount = Double.Parse(distotalprice);

            totalamount = decorigintotalprice + decintfrgttotalprice + decdesttotalprice + decaddtotalprice - decdiscount;
            if (ChargeSetId <= 0)
            {
                //Total = string.Format(culture, "{0:c}", totalamount);
                Total = Double.Parse(totalamount.ToString()).ToString("N" + decimalCount);
            }
            else
            {
                //Total = string.Format(culture, "{0:c}", SumAdditionalCharges);
                Total = Double.Parse(SumAdditionalCharges.ToString()).ToString("N" + decimalCount);
            }

            PdfPCell chargescell = new PdfPCell();
            chargescell.PaddingLeft = 10;
            chargescell.PaddingTop = 10;
            chargescell.Colspan = 1;
            chargescell.AddElement(chargesphrase);
            table.AddCell(chargescell);

            Phrase amountsphrase = new Phrase();
            if (ChargeSetId <= 0)
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                    //new Chunk(string.Format(culture, "{0:c}", decorigintotalprice) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             new Chunk(decorigintotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                        );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);

                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                    //new Chunk(string.Format(culture, "{0:c}", decintfrgttotalprice) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             new Chunk(decintfrgttotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                        );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);


                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                    //new Chunk(string.Format(culture, "{0:c}", decdesttotalprice) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                              new Chunk(decdesttotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                            );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);

                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                    //new Chunk(string.Format(culture, "{0:c}", decaddtotalprice) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                              new Chunk(decaddtotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
                if (distotalprice != "0")
                {
                    amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                    amountsphrase.Add(
                        //new Chunk(string.Format(culture, "- {0:c}", distotalprice) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                                       new Chunk(distotalprice + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                                 );
                    amountsphrase.Add(Chunk.NEWLINE);
                    amountsphrase.Add(Chunk.NEWLINE);
                }
            }
            else
            {
                if (SumAdditionalCharges != "0" && SumAdditionalCharges != "")
                {
                    amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                    amountsphrase.Add(
                        //new Chunk(string.Format(culture, "{0:c}", SumAdditionalCharges) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                                  new Chunk(Double.Parse(SumAdditionalCharges).ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                                 );
                    amountsphrase.Add(Chunk.NEWLINE);
                    amountsphrase.Add(Chunk.NEWLINE);
                }
            }

            PdfPCell amountscell = new PdfPCell();
            amountscell.PaddingRight = 10;
            amountscell.PaddingTop = 10;
            amountcell.Colspan = 1;
            amountscell.AddElement(amountsphrase);
            table.AddCell(amountscell);


            Phrase totalphrase = new Phrase();
            totalphrase.Add(
                new Chunk("Total", boldFont)
            );
            totalphrase.Add(Chunk.NEWLINE);
            totalphrase.Add(Chunk.NEWLINE);

            PdfPCell totalcell = new PdfPCell();
            totalcell.PaddingLeft = 10;
            totalcell.PaddingTop = 10;
            totalcell.Colspan = 1;
            totalcell.AddElement(totalphrase);
            table.AddCell(totalcell);



            Phrase totalamountphrase = new Phrase();
            totalamountphrase.Add(new Chunk(new VerticalPositionMark()));
            totalamountphrase.Add(
                new Chunk(Total + " " + AdvReceiptDtls.PREFERREDCURRENCYID, boldFont)
                );
            totalamountphrase.Add(Chunk.NEWLINE);
            totalamountphrase.Add(Chunk.NEWLINE);
            PdfPCell totalamountcell = new PdfPCell();
            totalamountcell.PaddingRight = 10;
            totalamountcell.PaddingTop = 10;
            totalamountcell.Colspan = 1;
            totalamountcell.AddElement(totalamountphrase);
            table.AddCell(totalamountcell);

            try
            {
                DataTable BillingInfo = mFactory.GetBillingInfo(jobnumber);
                var count = BillingInfo.Rows.Count - 1;
                var paidcurrency = BillingInfo.Rows[count]["PAIDCURRENCY"].ToString();
                var jobtotal = BillingInfo.Rows[count]["JOBTOTAL"].ToString();
                if (AdvReceiptDtls.PREFERREDCURRENCYID.ToLowerInvariant() != paidcurrency.ToLowerInvariant())
                {
                    int NewdecimalCount = mFactory.GetDecimalPointByCurrencyCode(paidcurrency);
                    Phrase currencyphrase = new Phrase();
                    currencyphrase.Add(
                        new Chunk("Payment Amount", boldFont)
                    );
                    currencyphrase.Add(Chunk.NEWLINE);
                    currencyphrase.Add(Chunk.NEWLINE);

                    PdfPCell currencycell = new PdfPCell();
                    currencycell.PaddingLeft = 10;
                    currencycell.PaddingTop = 10;
                    currencycell.Colspan = 1;
                    currencycell.AddElement(currencyphrase);
                    table.AddCell(currencycell);


                    Phrase totalcurrencyphrase = new Phrase();
                    totalcurrencyphrase.Add(new Chunk(new VerticalPositionMark()));
                    totalcurrencyphrase.Add(
                        new Chunk(Double.Parse(jobtotal).ToString("N" + NewdecimalCount) + " " + paidcurrency, boldFont)
                        );
                    totalcurrencyphrase.Add(Chunk.NEWLINE);
                    totalcurrencyphrase.Add(Chunk.NEWLINE);
                    PdfPCell totalcurrencycell = new PdfPCell();
                    totalcurrencycell.PaddingRight = 10;
                    totalcurrencycell.PaddingTop = 10;
                    totalcurrencycell.Colspan = 1;
                    totalcurrencycell.AddElement(totalcurrencyphrase);
                    table.AddCell(totalcurrencycell);
                }
            }
            catch
            {
                //
            }

            ////Billing Amount cell Added -----
            //if (ChargeSetId <= 0)
            //{                
            //    if (AdvReceiptDtls.NOTSUPPAMOUNTUSD != 0)
            //    {
            //        Phrase currencyphrase = new Phrase();
            //        currencyphrase.Add(
            //            new Chunk("Payment Amount", boldFont)
            //        );
            //        currencyphrase.Add(Chunk.NEWLINE);
            //        currencyphrase.Add(Chunk.NEWLINE);

            //        PdfPCell currencycell = new PdfPCell();
            //        currencycell.PaddingLeft = 10;
            //        currencycell.PaddingTop = 10;
            //        currencycell.Colspan = 1;
            //        currencycell.AddElement(currencyphrase);
            //        table.AddCell(currencycell);


            //        Phrase totalcurrencyphrase = new Phrase();
            //        totalcurrencyphrase.Add(new Chunk(new VerticalPositionMark()));
            //        totalcurrencyphrase.Add(
            //            new Chunk(AdvReceiptDtls.NOTSUPPAMOUNTUSD + " " + "USD", boldFont)
            //            );
            //        totalcurrencyphrase.Add(Chunk.NEWLINE);
            //        totalcurrencyphrase.Add(Chunk.NEWLINE);
            //        PdfPCell totalcurrencycell = new PdfPCell();
            //        totalcurrencycell.PaddingRight = 10;
            //        totalcurrencycell.PaddingTop = 10;
            //        totalcurrencycell.Colspan = 1;
            //        totalcurrencycell.AddElement(totalcurrencyphrase);
            //        table.AddCell(totalcurrencycell);
            //        //Billing Amount cell Added -----
            //    }
            //}


            doc.Add(table);

            PdfPTable note = new PdfPTable(2);
            note.SetWidths(new float[] { 5f, 95f });
            note.WidthPercentage = 100;
            note.SpacingBefore = 10;
            note.DefaultCell.Border = 0;
            note.AddCell(" ");
            note.AddCell(new PdfPCell(mFormattingpdf.TableContentCell("IMPORTANT: All business handled by Agility is subject to Agility's trading terms and conditions, which contain limitations of liability." +
            "Copies of these applicable terms and conditions are available upon request.", Element.ALIGN_LEFT)));

            doc.Add(note);

            ///////////

            doc.Close();

            bytes = memoryStream.ToArray();
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }

            //Insert same data into db : Anil G
            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
            sspdoc.QUOTATIONID = Convert.ToInt64(QuotaionID);
            sspdoc.DOCUMNETTYPE = "ShipAFreightDoc";
            if (ChargeSetId > 0)
            {
                sspdoc.FILENAME = "AdvanceCashReceipt_AdditionalCharges_" + TCount + ".pdf";
            }
            else
            {
                sspdoc.FILENAME = "AdvanceCashReceipt_" + OperationJobNumber.ToString() + ".pdf";
            }
            sspdoc.FILEEXTENSION = "application/pdf";
            sspdoc.FILESIZE = bytes.Length;
            sspdoc.FILECONTENT = bytes;
            sspdoc.JOBNUMBER = Convert.ToInt64(jobnumber);
            sspdoc.DocumentName = ReportName;
            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
            JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

            Guid obj = Guid.NewGuid();
            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
            mjobFactory.SaveDocuments(docentity, Convert.ToString(CreatedBy), "System Generated", obj.ToString(), EnvironmentName);

            return bytes;
        }

        public static byte[] GenerateBookingConfirmationPDF(int jobnumber, string ReportName, Int64 QuotaionID, string OperationJobNumber, string CreatedBy)
        {
            MemoryStream memoryStream = new MemoryStream();

            PDFpage = 0;
            if (mUserModel == null)
            {
                UserDBFactory mFactory1 = new UserDBFactory();
                mUserModel = mFactory1.GetUserProfileDetails(CreatedBy, 1);
            }
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable QuoteDtls = mFactory.GetQuoteDtls(jobnumber);
            DataTable PartyDtls = mFactory.GetPartyDtls(jobnumber);
            DataTable BillingInfo = mFactory.GetBillingInfo(jobnumber);
            DataTable ShipmentDtls = mFactory.GetShipmentDtls(jobnumber);
            DataTable VATDtls = mFactory.GetVATDtls(jobnumber);

            DataTable CargoDtls = mFactory.GetCargoDtls(Convert.ToInt32(jobnumber));

            Document doc = new Document(PageSize.A4, 50, 50, 125, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();

            Font boldFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
            Font normalFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);

            PdfPTable table = new PdfPTable(9);
            table.SetWidths(new float[] { 5f, 5f, 14.5f, 9f, 12.5f, 15.5f, 15.5f, 12.5f, 10.5f });
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 100;
            table.SpacingBefore = 10;

            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 2;
            string mDocumentHeading = "Booking Confirmation";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);


            PdfPCell verticalcell = new PdfPCell(new Phrase("IMPORTANT: All business handled by Agility is subject to Agility's trading terms and conditions, which contain limitations of liability." + Environment.NewLine + " Copies of these applicable terms and conditions are available upon request.", new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL)));
            verticalcell.Rotation = 90;
            verticalcell.HorizontalAlignment = Element.ALIGN_CENTER;
            verticalcell.VerticalAlignment = Element.ALIGN_TOP;
            verticalcell.Rowspan = 12;

            table.AddCell(verticalcell);

            Phrase phrase = new Phrase();
            phrase.Add(
                new Chunk("Quote No", normalFont)
            );
            PdfPCell quotenolblCell = new PdfPCell();
            quotenolblCell.AddElement(phrase);
            quotenolblCell.PaddingBottom = 6;
            quotenolblCell.Colspan = 2;
            table.AddCell(quotenolblCell);

            Phrase quotenophrase = new Phrase();
            quotenophrase.Add(
                new Chunk(QuoteDtls.Rows[0]["QUOTATIONNUMBER"].ToString(), boldFont)
            );
            PdfPCell quotenocell = new PdfPCell();
            quotenocell.AddElement(quotenophrase);
            quotenocell.PaddingBottom = 6;
            quotenocell.Colspan = 2;
            table.AddCell(quotenocell);


            Phrase bookingIdPhrase = new Phrase();
            bookingIdPhrase.Add(
                new Chunk("Booking Id", normalFont)
                );
            bookingIdPhrase.Add(Chunk.SPACETABBING);
            bookingIdPhrase.Add(new Chunk(QuoteDtls.Rows[0]["OPERATIONALJOBNUMBER"].ToString(), boldFont));


            bookingIdPhrase.Add(Chunk.SPACETABBING);
            bookingIdPhrase.Add(new Chunk("Date", normalFont));
            bookingIdPhrase.Add(Chunk.SPACETABBING);
            bookingIdPhrase.Add(new Chunk(System.DateTime.Now.ToString("dd-MMM-yyyy"), boldFont));


            PdfPCell bookingIdCell = new PdfPCell();
            bookingIdCell.AddElement(bookingIdPhrase);
            bookingIdCell.PaddingBottom = 6;
            bookingIdCell.Colspan = 4;
            table.AddCell(bookingIdCell);

            PdfPCell shipperlblcell = new PdfPCell(new Phrase(new Chunk("Shipper", boldFont)));
            shipperlblcell.PaddingBottom = 5;
            shipperlblcell.Colspan = 2;
            table.AddCell(shipperlblcell);

            PdfPCell shippercntlblcell = new PdfPCell(new Phrase(new Chunk("Contact", boldFont)));
            shippercntlblcell.PaddingBottom = 5;
            shippercntlblcell.Colspan = 2;
            table.AddCell(shippercntlblcell);

            PdfPCell consigneelblcell = new PdfPCell(new Phrase(new Chunk("Consignee", boldFont)));
            consigneelblcell.PaddingBottom = 5;
            consigneelblcell.Colspan = 2;
            table.AddCell(consigneelblcell);

            PdfPCell consigneecntlblcell = new PdfPCell(new Phrase(new Chunk("Contact", boldFont)));
            consigneecntlblcell.PaddingBottom = 5;
            consigneecntlblcell.Colspan = 2;
            table.AddCell(consigneecntlblcell);

            DataTable dtShipper = new DataTable();
            DataTable dtConsignee = new DataTable();
            if (PartyDtls.Rows.Count > 0)
            {
                dtShipper = PartyDtls.AsEnumerable().Where(i => i.Field<string>("PARTYTYPE") == "91").CopyToDataTable();
                dtConsignee = PartyDtls.AsEnumerable().Where(i => i.Field<string>("PARTYTYPE") == "92").CopyToDataTable();
            }

            string ShipperContact = string.Empty;
            string ConsigneeContact = string.Empty;
            string ShipperAddress = string.Empty;
            string ConsigneeAddress = string.Empty;

            string VATRegNo = string.Empty;
            if (VATDtls.Rows.Count > 0)
            {
                VATRegNo = "VAT Reg No: " + VATDtls.Rows[0]["VATREGNUMBER"].ToString();
            }

            if (dtShipper.Rows.Count > 0)
            {
                ShipperContact = dtShipper.Rows[0]["SALUTATION"].ToString() + " " + dtShipper.Rows[0]["FIRSTNAME"].ToString() + " " + dtShipper.Rows[0]["LASTNAME"].ToString() + "\n" + dtShipper.Rows[0]["EMAILID"].ToString() + "\n" + dtShipper.Rows[0]["PHONE"].ToString();
                ShipperAddress = dtShipper.Rows[0]["CLIENTNAME"].ToString() + Environment.NewLine + dtShipper.Rows[0]["FULLADDRESS"].ToString() + Environment.NewLine + VATRegNo;
            }
            if (dtConsignee.Rows.Count > 0)
            {
                ConsigneeContact = dtConsignee.Rows[0]["SALUTATION"].ToString() + " " + dtConsignee.Rows[0]["FIRSTNAME"].ToString() + " " + dtConsignee.Rows[0]["LASTNAME"].ToString() + "\n" + dtConsignee.Rows[0]["EMAILID"].ToString() + "\n" + dtConsignee.Rows[0]["PHONE"].ToString();
                ConsigneeAddress = dtConsignee.Rows[0]["CLIENTNAME"].ToString() + Environment.NewLine + dtConsignee.Rows[0]["FULLADDRESS"].ToString();
            }

            Phrase shipperaddphrase = new Phrase();
            shipperaddphrase.Add(new Chunk(ShipperAddress, normalFont));
            shipperaddphrase.Add(Chunk.NEWLINE);
            shipperaddphrase.Add(Chunk.NEWLINE);
            shipperaddphrase.Add(Chunk.NEWLINE);

            PdfPCell shippercell = new PdfPCell(shipperaddphrase);
            shippercell.PaddingBottom = 10;
            shippercell.PaddingRight = 10;
            shippercell.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            shippercell.Colspan = 2;
            shippercell.SetLeading(0, 1.5f);
            table.AddCell(shippercell);

            Phrase shippercntphrase = new Phrase();
            shippercntphrase.Add(new Chunk(ShipperContact, normalFont));
            shippercntphrase.Add(Chunk.NEWLINE);
            shippercntphrase.Add(Chunk.NEWLINE);
            shippercntphrase.Add(Chunk.NEWLINE);


            PdfPCell shippercntcell = new PdfPCell(shippercntphrase);
            shippercntcell.PaddingBottom = 10;
            shippercntcell.PaddingRight = 10;
            shippercntcell.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            shippercntcell.Colspan = 2;
            shippercntcell.SetLeading(0, 1.5f);
            table.AddCell(shippercntcell);

            Phrase consigneeaddphrase = new Phrase();
            consigneeaddphrase.Add(new Chunk(ConsigneeAddress, normalFont));
            consigneeaddphrase.Add(Chunk.NEWLINE);
            consigneeaddphrase.Add(Chunk.NEWLINE);
            consigneeaddphrase.Add(Chunk.NEWLINE);


            PdfPCell consigneecell = new PdfPCell(consigneeaddphrase);
            consigneecell.PaddingBottom = 10;
            consigneecell.PaddingRight = 10;
            consigneecell.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            consigneecell.Colspan = 2;
            consigneecell.SetLeading(0, 1.5f);
            table.AddCell(consigneecell);

            Phrase consigneecntphrase = new Phrase();
            consigneecntphrase.Add(new Chunk(ConsigneeContact, normalFont));
            consigneecntphrase.Add(Chunk.NEWLINE);
            consigneecntphrase.Add(Chunk.NEWLINE);
            consigneecntphrase.Add(Chunk.NEWLINE);

            PdfPCell consigneecntcell = new PdfPCell(consigneecntphrase);
            consigneecntcell.PaddingBottom = 10;
            consigneecntcell.PaddingRight = 10;
            consigneecntcell.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            consigneecntcell.Colspan = 2;
            consigneecntcell.SetLeading(0, 1.5f);
            table.AddCell(consigneecntcell);


            PdfPCell paymentdtlslblcell = new PdfPCell(new Phrase(new Chunk("Payment Details", boldFont)));
            paymentdtlslblcell.Colspan = 4;
            paymentdtlslblcell.PaddingBottom = 5;
            table.AddCell(paymentdtlslblcell);

            PdfPCell billinginfolblcell = new PdfPCell(new Phrase(new Chunk("Billing Information", boldFont)));
            billinginfolblcell.Colspan = 4;
            billinginfolblcell.PaddingBottom = 5;
            table.AddCell(billinginfolblcell);


            string PAYMENTREFNUMBER = string.Empty;
            string PAYMENTTYPE = string.Empty;
            string RECEIPTNETAMOUNT = string.Empty;
            string RECEIPTNUMBER = string.Empty;
            string RECEIPTDATE = string.Empty;
            string CURRENCY = string.Empty;

            var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.CurrencySymbol = "";

            if (BillingInfo.Rows.Count > 0)
            {
                PAYMENTREFNUMBER = BillingInfo.Rows[0]["PAYMENTREFNUMBER"].ToString();
                PAYMENTTYPE = BillingInfo.Rows[0]["PAYMENTOPTION"].ToString() + " - " + BillingInfo.Rows[0]["PAYMENTTYPE"].ToString();
                RECEIPTNETAMOUNT = BillingInfo.Rows[0]["RECEIPTNETAMOUNT"].ToString();
                RECEIPTNUMBER = BillingInfo.Rows[0]["RECEIPTNUMBER"].ToString();
                RECEIPTDATE = Convert.ToDateTime(BillingInfo.Rows[0]["RECEIPTDATE"]).ToString("dd-MMM-yyyy");
                CURRENCY = BillingInfo.Rows[0]["CURRENCY"].ToString();
                var decimalCount = mFactory.GetDecimalPointByCurrencyCode(CURRENCY);

                if (RECEIPTNETAMOUNT != "")
                    RECEIPTNETAMOUNT = Double.Parse(RECEIPTNETAMOUNT.ToString()).ToString("N" + decimalCount);
            }

            Phrase paymentdtlsphrase = new Phrase();
            paymentdtlsphrase.Add(new Chunk("Transaction Reference ID :", normalFont));
            paymentdtlsphrase.Add(Chunk.SPACETABBING);
            paymentdtlsphrase.Add(new Chunk(PAYMENTREFNUMBER, boldFont));
            paymentdtlsphrase.Add(Chunk.NEWLINE);
            paymentdtlsphrase.Add(Chunk.NEWLINE);


            PdfPCell paymentdtlscell = new PdfPCell();
            paymentdtlscell.AddElement(paymentdtlsphrase);
            paymentdtlscell.Colspan = 4;
            paymentdtlscell.PaddingBottom = 10;
            paymentdtlscell.PaddingRight = 10;
            table.AddCell(paymentdtlscell);


            Phrase billinginfophrase = new Phrase();
            billinginfophrase.Add(new Chunk("Payment Type", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + PAYMENTTYPE, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);

            billinginfophrase.Add(new Chunk("Net Amount", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + RECEIPTNETAMOUNT + " " + CURRENCY, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);

            billinginfophrase.Add(new Chunk("Receipt Number", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + RECEIPTNUMBER, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);

            billinginfophrase.Add(new Chunk("Date of Receipt", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + RECEIPTDATE, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);
            billinginfophrase.Add(Chunk.NEWLINE);

            PdfPCell billinginfocell = new PdfPCell();
            billinginfocell.AddElement(billinginfophrase);
            billinginfocell.Colspan = 4;
            billinginfocell.PaddingBottom = 10;
            billinginfocell.PaddingRight = 10;
            table.AddCell(billinginfocell);

            PdfPCell shipmentdtlslblcell = new PdfPCell(new Phrase(new Chunk("Shipment Details", boldFont)));
            shipmentdtlslblcell.Colspan = 8;
            shipmentdtlslblcell.PaddingBottom = 5;
            table.AddCell(shipmentdtlslblcell);


            string Product = string.Empty;
            string MovementType = string.Empty;
            string PlaceOfReceipt = string.Empty;
            string PlaceOfDelivery = string.Empty;
            var MName = string.Empty;

            if (ShipmentDtls.Rows.Count > 0)
            {
                Product = ShipmentDtls.Rows[0]["PRODUCTNAME"].ToString();
                MovementType = ShipmentDtls.Rows[0]["MOVEMENTTYPENAME"].ToString();

                if (MovementType == "Door to Door" || MovementType == "Door to door")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPLACENAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPLACENAME"].ToString();
                }
                else if (MovementType == "Door to port")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPLACENAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPORTNAME"].ToString();
                }
                else if (MovementType == "Port to Port" || MovementType == "Port to port")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPORTNAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPORTNAME"].ToString();
                }
                else if (MovementType == "Port to door")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPORTNAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPLACENAME"].ToString();
                }
            }

            if (ShipmentDtls.Rows[0]["PRODUCTID"].ToString() == "3")
            {
                if (MovementType == "Port to port") { MName = "Airport to airport"; }
                if (MovementType == "Door to port") { MName = "Door to airport"; }
                if (MovementType == "Port to door") { MName = "Airport to door"; }
                if (MovementType == "Door to door") { MName = "Door to door"; }
            }
            else
            {
                MName = MovementType;
            }

            Phrase shipmentdtlsphrase1 = new Phrase();
            shipmentdtlsphrase1.Add(new Chunk("Product", normalFont));
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            shipmentdtlsphrase1.Add(new Chunk(Product, boldFont));
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell1 = new PdfPCell(shipmentdtlsphrase1);
            shipmentdtlscell1.Colspan = 2;
            shipmentdtlscell1.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell1.PaddingBottom = 5;


            Phrase shipmentdtlsphrase2 = new Phrase();
            shipmentdtlsphrase2.Add(new Chunk("Movement Type", normalFont));
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            shipmentdtlsphrase2.Add(new Chunk(MName, boldFont));
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell2 = new PdfPCell(shipmentdtlsphrase2);
            shipmentdtlscell2.Colspan = 2;
            shipmentdtlscell2.Border = PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell2.PaddingBottom = 5;

            Phrase shipmentdtlsphrase3 = new Phrase();
            shipmentdtlsphrase3.Add(new Chunk("Place of Receipt", normalFont));
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            shipmentdtlsphrase3.Add(new Chunk(PlaceOfReceipt, boldFont));
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell3 = new PdfPCell(shipmentdtlsphrase3);
            shipmentdtlscell3.Colspan = 2;
            shipmentdtlscell3.Border = PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell3.PaddingBottom = 5;

            Phrase shipmentdtlsphrase4 = new Phrase();
            shipmentdtlsphrase4.Add(new Chunk("Place of Delivery", normalFont));
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            shipmentdtlsphrase4.Add(new Chunk(PlaceOfDelivery, boldFont));
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell4 = new PdfPCell(shipmentdtlsphrase4);
            shipmentdtlscell4.Colspan = 2;
            shipmentdtlscell4.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell4.PaddingBottom = 5;

            table.AddCell(shipmentdtlscell1);
            table.AddCell(shipmentdtlscell2);
            table.AddCell(shipmentdtlscell3);
            table.AddCell(shipmentdtlscell4);

            if (Convert.ToString(ShipmentDtls.Rows[0]["ISHAZARDOUSCARGO"]) != "0")
            {
                Phrase hazardous = new Phrase();
                hazardous.Add(new Chunk("Hazardous Cargo", normalFont));
                hazardous.Add(Chunk.NEWLINE);
                hazardous.Add(Chunk.NEWLINE);
                hazardous.Add(new Chunk(Convert.ToString(ShipmentDtls.Rows[0]["HAZARDOUSGOODSTYPE"]), boldFont));
                hazardous.Add(Chunk.NEWLINE);
                PdfPCell hazardouspdf = new PdfPCell(hazardous);
                hazardouspdf.Colspan = 8;
                hazardouspdf.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                hazardouspdf.PaddingBottom = 5;
                table.AddCell(hazardouspdf);
            }
            PdfPCell cargodtlslblcell = new PdfPCell(new Phrase(new Chunk("Cargo Details", boldFont)));
            cargodtlslblcell.Colspan = 8;
            cargodtlslblcell.PaddingBottom = 5;
            table.AddCell(cargodtlslblcell);

            Phrase cargodtlsphrase1 = new Phrase();
            cargodtlsphrase1.Add(new Chunk("Sno", normalFont));
            cargodtlsphrase1.Add(Chunk.NEWLINE);
            cargodtlsphrase1.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell1 = new PdfPCell(cargodtlsphrase1);
            cargodtlscell1.PaddingBottom = 5;
            cargodtlscell1.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase2 = new Phrase();
            cargodtlsphrase2.Add(new Chunk("Package Type", normalFont));
            cargodtlsphrase2.Add(Chunk.NEWLINE);
            cargodtlsphrase2.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell2 = new PdfPCell(cargodtlsphrase2);
            cargodtlscell2.PaddingBottom = 5;
            cargodtlscell2.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase3 = new Phrase();
            cargodtlsphrase3.Add(new Chunk("Quantity", normalFont));
            cargodtlsphrase3.Add(Chunk.NEWLINE);
            cargodtlsphrase3.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell3 = new PdfPCell(cargodtlsphrase3);
            cargodtlscell3.PaddingBottom = 5;
            cargodtlscell3.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase4 = new Phrase();
            cargodtlsphrase4.Add(new Chunk("LWH", normalFont));
            cargodtlsphrase4.Add(Chunk.NEWLINE);
            cargodtlsphrase4.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell4 = new PdfPCell(cargodtlsphrase4);
            cargodtlscell4.PaddingBottom = 5;
            cargodtlscell4.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase5 = new Phrase();
            cargodtlsphrase5.Add(new Chunk("UOM of LWH", normalFont));
            cargodtlsphrase5.Add(Chunk.NEWLINE);
            cargodtlsphrase5.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell5 = new PdfPCell(cargodtlsphrase5);
            cargodtlscell5.PaddingBottom = 5;
            cargodtlscell5.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase6 = new Phrase();
            cargodtlsphrase6.Add(new Chunk("Per Piece Weight", normalFont));
            cargodtlsphrase6.Add(Chunk.NEWLINE);
            cargodtlsphrase6.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell6 = new PdfPCell(cargodtlsphrase6);
            cargodtlscell6.PaddingBottom = 5;
            cargodtlscell6.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase7 = new Phrase();
            cargodtlsphrase7.Add(new Chunk("Total Weight", normalFont));
            cargodtlsphrase7.Add(Chunk.NEWLINE);
            cargodtlsphrase7.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell7 = new PdfPCell(cargodtlsphrase7);
            cargodtlscell7.PaddingBottom = 5;
            cargodtlscell7.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase8 = new Phrase();
            cargodtlsphrase8.Add(new Chunk("Total CBM", normalFont));
            cargodtlsphrase8.Add(Chunk.NEWLINE);
            cargodtlsphrase8.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell8 = new PdfPCell(cargodtlsphrase8);
            cargodtlscell8.PaddingBottom = 5;
            cargodtlscell8.HorizontalAlignment = Element.ALIGN_CENTER;

            string PERPIECEWEIGHT = string.Empty;
            string TOTALWEIGHT = string.Empty;

            for (int i = 0; i < CargoDtls.Rows.Count; i++)
            {
                if (CargoDtls.Rows[i]["WEIGHTPERPIECE"].ToString() != "")
                    PERPIECEWEIGHT = string.Format(culture, "{0:c}", Convert.ToDecimal(CargoDtls.Rows[i]["WEIGHTPERPIECE"].ToString()));
                if (CargoDtls.Rows[i]["WEIGHTTOTAL"].ToString() != "")
                    TOTALWEIGHT = string.Format(culture, "{0:c}", Convert.ToDecimal(CargoDtls.Rows[i]["WEIGHTTOTAL"].ToString()));

                cargodtlsphrase1.Add(new Chunk(CargoDtls.Rows[i]["SEQNO"].ToString(), boldFont));
                cargodtlsphrase1.Add(Chunk.NEWLINE);
                cargodtlsphrase1.Add(Chunk.NEWLINE);

                cargodtlsphrase2.Add(new Chunk(CargoDtls.Rows[i]["PACKAGETYPENAME"].ToString(), boldFont));
                cargodtlsphrase2.Add(Chunk.NEWLINE);
                cargodtlsphrase2.Add(Chunk.NEWLINE);

                cargodtlsphrase3.Add(new Chunk(CargoDtls.Rows[i]["QUANTITY"].ToString(), boldFont));
                cargodtlsphrase3.Add(Chunk.NEWLINE);
                cargodtlsphrase3.Add(Chunk.NEWLINE);

                cargodtlsphrase4.Add(new Chunk(CargoDtls.Rows[i]["ITEMLENGTH"].ToString() + "," + CargoDtls.Rows[i]["ITEMWIDTH"].ToString() + "," + CargoDtls.Rows[i]["ITEMHEIGHT"].ToString(), boldFont));
                cargodtlsphrase4.Add(Chunk.NEWLINE);
                cargodtlsphrase4.Add(Chunk.NEWLINE);

                cargodtlsphrase5.Add(new Chunk(CargoDtls.Rows[i]["LENGTHUOMNAME"].ToString(), boldFont));
                cargodtlsphrase5.Add(Chunk.NEWLINE);
                cargodtlsphrase5.Add(Chunk.NEWLINE);

                cargodtlsphrase6.Add(new Chunk(PERPIECEWEIGHT, boldFont));
                cargodtlsphrase6.Add(Chunk.NEWLINE);
                cargodtlsphrase6.Add(Chunk.NEWLINE);

                cargodtlsphrase7.Add(new Chunk(TOTALWEIGHT, boldFont));
                cargodtlsphrase7.Add(Chunk.NEWLINE);
                cargodtlsphrase7.Add(Chunk.NEWLINE);

                cargodtlsphrase8.Add(new Chunk(CargoDtls.Rows[i]["TOTALCBM"].ToString(), boldFont));
                cargodtlsphrase8.Add(Chunk.NEWLINE);
                cargodtlsphrase8.Add(Chunk.NEWLINE);
            }

            //cargodtlscell1.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            //cargodtlscell2.Border = PdfPCell.BOTTOM_BORDER;
            //cargodtlscell3.Border = PdfPCell.BOTTOM_BORDER;
            //cargodtlscell4.Border = PdfPCell.BOTTOM_BORDER;
            //cargodtlscell5.Border = PdfPCell.BOTTOM_BORDER;
            //cargodtlscell6.Border = PdfPCell.BOTTOM_BORDER;
            //cargodtlscell7.Border = PdfPCell.BOTTOM_BORDER;
            //cargodtlscell8.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;


            table.AddCell(cargodtlscell1);
            table.AddCell(cargodtlscell2);
            table.AddCell(cargodtlscell3);
            table.AddCell(cargodtlscell4);
            table.AddCell(cargodtlscell5);
            table.AddCell(cargodtlscell6);
            table.AddCell(cargodtlscell7);
            table.AddCell(cargodtlscell8);

            Phrase cargodtlsphrase9 = new Phrase();
            cargodtlsphrase9.Add(new Chunk("Cargo Available from", normalFont));
            cargodtlsphrase9.Add(Chunk.NEWLINE);
            cargodtlsphrase9.Add(Chunk.NEWLINE);
            cargodtlsphrase9.Add(new Chunk(Convert.ToDateTime(QuoteDtls.Rows[0]["CARGOAVAILABLEFROM"]).ToString("dd-MMM-yyyy"), boldFont));
            cargodtlsphrase9.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell9 = new PdfPCell(cargodtlsphrase9);
            cargodtlscell9.Colspan = 2;
            cargodtlscell9.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            cargodtlscell9.PaddingBottom = 5;


            Phrase cargodtlsphrase10 = new Phrase();
            cargodtlsphrase10.Add(new Chunk("Origin Customs Clearance by", normalFont));
            cargodtlsphrase10.Add(Chunk.NEWLINE);
            cargodtlsphrase10.Add(Chunk.NEWLINE);
            cargodtlsphrase10.Add(new Chunk(QuoteDtls.Rows[0]["ORIGINCUSTOMSCLEARANCEBY"].ToString(), boldFont));
            cargodtlsphrase10.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell10 = new PdfPCell(cargodtlsphrase10);
            cargodtlscell10.Colspan = 2;
            cargodtlscell10.Border = PdfPCell.BOTTOM_BORDER;
            cargodtlscell10.PaddingBottom = 5;

            Phrase cargodtlsphrase11 = new Phrase();
            cargodtlsphrase11.Add(new Chunk("Cargo Deliver to", normalFont));
            cargodtlsphrase11.Add(Chunk.NEWLINE);
            cargodtlsphrase11.Add(Chunk.NEWLINE);
            //cargodtlsphrase11.Add(new Chunk(Convert.ToDateTime(QuoteDtls.Rows[0]["CARGODELIVERYBY"]).ToString("dd-MMM-yyyy"), boldFont));
            cargodtlsphrase11.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell11 = new PdfPCell(cargodtlsphrase11);
            cargodtlscell11.Colspan = 2;
            cargodtlscell11.Border = PdfPCell.BOTTOM_BORDER;
            cargodtlscell11.PaddingBottom = 5;

            Phrase cargodtlsphrase12 = new Phrase();
            cargodtlsphrase12.Add(new Chunk("Destination Customs Clearance by", normalFont));
            cargodtlsphrase12.Add(Chunk.NEWLINE);
            cargodtlsphrase12.Add(Chunk.NEWLINE);
            cargodtlsphrase12.Add(new Chunk(QuoteDtls.Rows[0]["DESTINATIONCUSTOMSCLEARANCEBY"].ToString(), boldFont));
            cargodtlsphrase12.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell12 = new PdfPCell(cargodtlsphrase12);
            cargodtlscell12.Colspan = 2;
            cargodtlscell12.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            cargodtlscell12.PaddingBottom = 5;

            table.AddCell(cargodtlscell9);
            table.AddCell(cargodtlscell10);
            table.AddCell(cargodtlscell11);
            table.AddCell(cargodtlscell12);


            PdfPCell specialinstrlblcell = new PdfPCell(new Phrase(new Chunk("Special Instruction", boldFont)));
            specialinstrlblcell.Colspan = 8;
            specialinstrlblcell.PaddingBottom = 5;
            table.AddCell(specialinstrlblcell);

            PdfPCell specialinstrcell = new PdfPCell(new Phrase(new Chunk(QuoteDtls.Rows[0]["SPECIALINSTRUCTIONS"].ToString(), normalFont)));
            specialinstrcell.Colspan = 8;
            specialinstrcell.PaddingBottom = 5;
            specialinstrcell.FixedHeight = 80f;
            table.AddCell(specialinstrcell);

            doc.Add(table);
            doc.Close();

            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }

            //Insert same data into db : Anil G
            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
            sspdoc.QUOTATIONID = Convert.ToInt64(QuotaionID);
            sspdoc.DOCUMNETTYPE = "BKGCONF";
            sspdoc.FILENAME = "BookingConfirmation_" + OperationJobNumber.ToString() + ".pdf";
            sspdoc.FILEEXTENSION = "application/pdf";
            sspdoc.FILESIZE = bytes.Length;
            sspdoc.FILECONTENT = bytes;
            sspdoc.JOBNUMBER = Convert.ToInt64(jobnumber);
            sspdoc.DocumentName = ReportName;
            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
            JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

            Guid obj = Guid.NewGuid();
            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();

            mjobFactory.SaveDocuments(docentity, CreatedBy, "System Generated", obj.ToString(), EnvironmentName);

            return bytes;
        }

        public void GeneratePaymentTransactionPdfMobile(PaymentTransactionDetailsModel transaction,
            PaymentModel payment, JobBookingModel booking, QuotationPreviewModel mQuotaionpreview)
        {

            MemoryStream memoryStream = new MemoryStream();
            JobBookingDbFactory jdf = new JobBookingDbFactory();

            z = 0;
            UserDBFactory mFactory = new UserDBFactory();
            mUserModel = mFactory.GetUserProfileDetails(mQuotaionpreview.CreatedBy, 1);
            mUIModels = mQuotaionpreview;
            Document document = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            document.Open();
            writer.PageEvent = new ITextEvents();
            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 1;
            string mDocumentHeading = "Booking Request";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            document.Add(mHeadingtable);
            PdfPTable bookingTable = new PdfPTable(1);

            document.Add(mFormattingpdf.LineSeparator());
            document.Add(bookingInfoTableModel(booking));
            if (mQuotaionpreview.ProductTypeId == 5)
            {
                document.Add(containerTableModel(booking));
                if (!ReferenceEquals(booking.JobShipmentItems, null) && (booking.JobShipmentItems.Count > 0))
                {
                    document.Add(cargoTableModel(booking));
                }
            }
            else if (mQuotaionpreview.ProductTypeId == 6)
            {
                document.Add(cargoTableModel(booking));
            }
            if (mQuotaionpreview.QuotationCharges.Any())
            {
                var mOrigincharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mDestinationcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mINTcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mAddcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                if (mOrigincharges.Any())
                    document.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mQuotaionpreview.PreferredCurrencyId));

                if (mINTcharges.Any())
                    document.Add(ChargesTableModel(mINTcharges, "International Charges", mQuotaionpreview.PreferredCurrencyId));

                if (mDestinationcharges.Any())
                    document.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mQuotaionpreview.PreferredCurrencyId));

                if (mAddcharges.Any())
                    document.Add(ChargesTableModel(mAddcharges, "Optional Charges", mQuotaionpreview.PreferredCurrencyId));
            }
            PdfPTable mgrossTotalBorder = new PdfPTable(2);
            PdfPTable mdiscountTotalBorder = new PdfPTable(2);
            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            QuotationDBFactory mFactoryRound = new QuotationDBFactory();

            if (booking.ReceiptHeaderDetails[0].DISCOUNTAMOUNT != 0)
            {

                mgrossTotalBorder.SetWidths(new float[] { 50, 50 });
                mgrossTotalBorder.WidthPercentage = 100;
                mgrossTotalBorder.SpacingBefore = 10;
                mgrossTotalBorder.DefaultCell.Border = 0;
                mgrossTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mgrossChargeTotal = new PdfPTable(2);
                mgrossChargeTotal.SetWidths(new float[] { 50, 50 });
                mgrossChargeTotal.WidthPercentage = 100;
                mgrossChargeTotal.AddCell((mFormattingpdf.TableHeading("Gross Total", Element.ALIGN_LEFT)));
                mgrossChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].RECEIPTAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));


                PdfPCell mgrosscells = new PdfPCell(mgrossChargeTotal);
                mgrossTotalBorder.AddCell(mgrosscells);

                mdiscountTotalBorder.SetWidths(new float[] { 50, 50 });
                mdiscountTotalBorder.WidthPercentage = 100;
                mdiscountTotalBorder.SpacingBefore = 10;
                mdiscountTotalBorder.DefaultCell.Border = 0;
                mdiscountTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mdiscountChargeTotal = new PdfPTable(2);
                mdiscountChargeTotal.SetWidths(new float[] { 50, 50 });
                mdiscountChargeTotal.WidthPercentage = 100;
                mdiscountChargeTotal.AddCell((mFormattingpdf.TableHeading("Discount Amount", Element.ALIGN_LEFT)));
                mdiscountChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].DISCOUNTAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));

                PdfPCell mdiscountcells = new PdfPCell(mdiscountChargeTotal);
                mdiscountTotalBorder.AddCell(mdiscountcells);
                mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotalBorder.WidthPercentage = 100;
                mQuoteChargeTotalBorder.SpacingBefore = 10;
                mQuoteChargeTotalBorder.DefaultCell.Border = 0;
                mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mQuoteChargeTotal = new PdfPTable(2);
                mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotal.WidthPercentage = 100;
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].RECEIPTNETAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                if (transaction.PAIDCURRENCY != mQuotaionpreview.PreferredCurrencyId)
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Payment Amount", Element.ALIGN_LEFT)));
                    var ct = jdf.GetCurrencyConversion(booking.ReceiptHeaderDetails[0].RECEIPTNETAMOUNT.ToString(), mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                }
                PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
                mQuoteChargeTotalBorder.AddCell(mTotalcells);
                document.Add(mgrossTotalBorder);
                document.Add(mdiscountTotalBorder);
                document.Add(mQuoteChargeTotalBorder);

            }
            else
            {
                mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotalBorder.WidthPercentage = 100;
                mQuoteChargeTotalBorder.SpacingBefore = 10;
                mQuoteChargeTotalBorder.DefaultCell.Border = 0;
                mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mQuoteChargeTotal = new PdfPTable(2);
                mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotal.WidthPercentage = 100;
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(mQuotaionpreview.GrandTotal), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));

                if (transaction.PAIDCURRENCY != mQuotaionpreview.PreferredCurrencyId)
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Payment Amount", Element.ALIGN_LEFT)));
                    var ct = jdf.GetCurrencyConversion(mQuotaionpreview.GrandTotal.ToString(), mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                }

                PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
                mQuoteChargeTotalBorder.AddCell(mTotalcells);
                document.Add(mQuoteChargeTotalBorder);
            }

            document.Add(ShipperTableModel(booking));
            if (transaction.PAYMENTTYPE.ToLower() == "wire transfer")
            {
                document.Add(PaymenTabletModel(payment));
                document.Add(TransactionTabletModel(transaction));
            }
            else if (transaction.PAYMENTTYPE.ToLower() == "pay at branch")
            {
                document.Add(payatbranchModel(transaction));
            }
            document.Close();
            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }

            Guid obj = Guid.NewGuid();
            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();

            jdf.SaveOfflinePaymentBookingSummaryDoc(bytes, Convert.ToInt32(payment.JobNumber), Convert.ToInt32(payment.QuotationId), payment.UserId, payment.OPjobnumber, obj.ToString(), EnvironmentName);

        }

        #endregion;
        #region GetpaymentTransaction Details
        [AllowAnonymous]
        [EncryptedActionParameter]
        public void GetBookingRequestDocument(int JobNumber, int QuotationId, string FileName = "")
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();

                IList<SSPDocumentsEntity> mDBEntity = mFactory.GetBookingRequestDetails(JobNumber, QuotationId, FileName);

                using (MemoryStream memoryStream = new MemoryStream())
                {

                    byte[] bytes;
                    memoryStream.Close();
                    bytes = mDBEntity.FirstOrDefault().FILECONTENT;
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + mDBEntity.FirstOrDefault().DOCUMNETTYPE + "_" + mDBEntity.FirstOrDefault().FILENAME + ".pdf");
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                    Response.Close();
                }
            }
            catch
            {
                //
            }
        }


        public byte[] PaymentTransaction(int JobNumber, int QuotationId, string UserID, string ChargeSetId = "")
        {
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(QuotationId));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mQuoFactory.GetById(QuotationId);
                QuotationPreviewModel mQuotaionpreview = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);

                QuotationPreviewModel DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);

                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                JobBookingModel mbookingModel = new JobBookingModel();
                PaymentTransactionDetailsEntity paymentTransactEntity = (PaymentTransactionDetailsEntity)Session["TransactionDetails"];
                PaymentTransactionDetailsModel paymentTrans = Mapper.Map<PaymentTransactionDetailsEntity, PaymentTransactionDetailsModel>(paymentTransactEntity);
                DBFactory.Entities.PaymentEntity paymentEntity = mFactory.GetPaymentDetailsByJobNumber(JobNumber, UserID); // HttpContext.User.Identity.Name
                PaymentModel mpayment = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(paymentEntity);
                DBFactory.Entities.JobBookingEntity bookingEntity = mFactory.GetQuotedJobDetails(Convert.ToInt32(QuotationId), Convert.ToInt64(JobNumber));
                mbookingModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(bookingEntity);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = GeneratePaymentTransactionPdf(memoryStream, paymentTrans, mpayment, mbookingModel, mQuotaionpreview, JobNumber, QuotationId, ChargeSetId);
                    memoryStream.Close();
                }
            }
            return bytes;

        }

        public byte[] GeneratePaymentTransactionPdf(MemoryStream memoryStream, PaymentTransactionDetailsModel transaction, PaymentModel payment, JobBookingModel booking, QuotationPreviewModel mQuotaionpreview, int JobNumber, int QuotationId, string ChargeSetId = "")
        {
            z = 0;
            UserDBFactory mFactory = new UserDBFactory();
            JobBookingDbFactory JFactory = new JobBookingDbFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            mUserModel = mFactory.GetUserProfileDetails(mQuotaionpreview.CreatedBy, 1);
            int DecimalCount = mFactory.GetDecimalPointByCurrencyCode(mQuotaionpreview.PreferredCurrencyId);
            mUIModels = mQuotaionpreview;
            Document document = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            document.Open();
            writer.PageEvent = new ITextEvents();
            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 1;
            string mDocumentHeading = "Booking Request";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            document.Add(mHeadingtable);

            document.Add(mFormattingpdf.LineSeparator());
            document.Add(bookingInfoTableModel(booking));
            if (mQuotaionpreview.ProductTypeId == 5)
            {
                document.Add(containerTableModel(booking));
                if (!ReferenceEquals(booking.JobShipmentItems, null) && (booking.JobShipmentItems.Count > 0))
                {
                    document.Add(cargoTableModel(booking));
                }
            }
            else if (mQuotaionpreview.ProductTypeId == 6)
            {
                document.Add(cargoTableModel(booking));
            }
            if (string.IsNullOrEmpty(ChargeSetId))
            {
                if (mQuotaionpreview.QuotationCharges.Any())
                {
                    var mOrigincharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    var mDestinationcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    var mINTcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    var mAddcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    if (mOrigincharges.Any())
                        document.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mQuotaionpreview.PreferredCurrencyId));

                    if (mINTcharges.Any())
                        document.Add(ChargesTableModel(mINTcharges, "International Charges", mQuotaionpreview.PreferredCurrencyId));

                    if (mDestinationcharges.Any())
                        document.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mQuotaionpreview.PreferredCurrencyId));

                    if (mAddcharges.Any())
                        document.Add(ChargesTableModel(mAddcharges, "Optional Charges", mQuotaionpreview.PreferredCurrencyId));
                }
            }
            else
            {
                try
                {
                    DBFactory.Entities.JobBookingEntity mDBEntity = JFactory.GetQuotedJobDetails(QuotationId, JobNumber);
                    mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                    var mAddcharges1 = mUIModel.PaymentCharges.Where(x => x.CHARGESOURCE.ToLower() == "f" && (x.CHARGESTATUS != null && (x.CHARGESTATUS.ToLower() == "pay now" || x.CHARGESTATUS.ToLower() == "pay later")));
                    if (mAddcharges1.Any())
                        document.Add(ChargesTableAddModel(mAddcharges1, "Additional Charges", mQuotaionpreview.PreferredCurrencyId));
                }
                catch (Exception ex)
                {
                    //
                }
            }
            PdfPTable mgrossTotalBorder = new PdfPTable(2);
            PdfPTable mdiscountTotalBorder = new PdfPTable(2);
            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            QuotationDBFactory mFactoryRound = new QuotationDBFactory();
            if (booking.ReceiptHeaderDetails[0].DISCOUNTAMOUNT != 0)
            {

                mgrossTotalBorder.SetWidths(new float[] { 50, 50 });
                mgrossTotalBorder.WidthPercentage = 100;
                mgrossTotalBorder.SpacingBefore = 10;
                mgrossTotalBorder.DefaultCell.Border = 0;
                mgrossTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mgrossChargeTotal = new PdfPTable(2);
                mgrossChargeTotal.SetWidths(new float[] { 50, 50 });
                mgrossChargeTotal.WidthPercentage = 100;
                mgrossChargeTotal.AddCell((mFormattingpdf.TableHeading("Gross Total", Element.ALIGN_LEFT)));
                mgrossChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].RECEIPTAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));

                PdfPCell mgrosscells = new PdfPCell(mgrossChargeTotal);
                mgrossTotalBorder.AddCell(mgrosscells);

                mdiscountTotalBorder.SetWidths(new float[] { 50, 50 });
                mdiscountTotalBorder.WidthPercentage = 100;
                mdiscountTotalBorder.SpacingBefore = 10;
                mdiscountTotalBorder.DefaultCell.Border = 0;
                mdiscountTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mdiscountChargeTotal = new PdfPTable(2);
                mdiscountChargeTotal.SetWidths(new float[] { 50, 50 });
                mdiscountChargeTotal.WidthPercentage = 100;
                mdiscountChargeTotal.AddCell((mFormattingpdf.TableHeading("Discount Amount", Element.ALIGN_LEFT)));
                mdiscountChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].DISCOUNTAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));

                PdfPCell mdiscountcells = new PdfPCell(mdiscountChargeTotal);
                mdiscountTotalBorder.AddCell(mdiscountcells);
                mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotalBorder.WidthPercentage = 100;
                mQuoteChargeTotalBorder.SpacingBefore = 10;
                mQuoteChargeTotalBorder.DefaultCell.Border = 0;
                mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mQuoteChargeTotal = new PdfPTable(2);
                mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotal.WidthPercentage = 100;
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].RECEIPTNETAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                //////////////////////////////////////////////////////////////////////////////
                if (transaction.PAIDCURRENCY != mQuotaionpreview.PreferredCurrencyId)
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Payment Amount", Element.ALIGN_LEFT)));
                    var ct = JFactory.GetCurrencyConversion(booking.ReceiptHeaderDetails[0].RECEIPTNETAMOUNT.ToString(), mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                }

                PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
                mQuoteChargeTotalBorder.AddCell(mTotalcells);
                document.Add(mgrossTotalBorder);
                document.Add(mdiscountTotalBorder);
                document.Add(mQuoteChargeTotalBorder);

            }
            else
            {

                mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotalBorder.WidthPercentage = 100;
                mQuoteChargeTotalBorder.SpacingBefore = 10;
                mQuoteChargeTotalBorder.DefaultCell.Border = 0;
                mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mQuoteChargeTotal = new PdfPTable(2);
                mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotal.WidthPercentage = 100;
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
                if (string.IsNullOrEmpty(ChargeSetId))
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(mQuotaionpreview.GrandTotal), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                }
                else
                {
                    if (!string.IsNullOrEmpty(ChargeSetTotal))
                        mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(ChargeSetTotal, mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                }
                /////////////////////////////////////////////////////////////////////////////////////////
                if (transaction.PAIDCURRENCY != mQuotaionpreview.PreferredCurrencyId)
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Payment Amount", Element.ALIGN_LEFT)));
                    if (string.IsNullOrEmpty(ChargeSetId))
                    {
                        var ct = JFactory.GetCurrencyConversion(mQuotaionpreview.GrandTotal.ToString(), mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                        mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ChargeSetTotal))
                        {
                            var ct = JFactory.GetCurrencyConversion(ChargeSetTotal, mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                        }
                    }
                }

                PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
                mQuoteChargeTotalBorder.AddCell(mTotalcells);
                document.Add(mQuoteChargeTotalBorder);
            }

            document.Add(ShipperTableModel(booking));
            if (transaction.PAYMENTTYPE.ToLower() == "wire transfer")
            {
                document.Add(PaymenTabletModel(payment));
                document.Add(TransactionTabletModel(transaction));
            }
            else if (transaction.PAYMENTTYPE.ToLower() == "pay at branch")
            {
                document.Add(payatbranchModel(transaction));
            }
            document.Close();
            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        public static PdfPTable bookingInfoTableModel(JobBookingModel model)
        {
            SSPJobDetailsModel jobdetails = new SSPJobDetailsModel();
            if (model.JobItems.Count > 0)
            {
                foreach (var item in model.JobItems)
                {
                    jobdetails = item;
                }
            }

            var MName = string.Empty;
            var Ploading = string.Empty;
            var Pdischarge = string.Empty;
            if (model.PRODUCTID == 3)
            {
                if (model.MOVEMENTTYPENAME == "Port to port") { MName = "Airport to airport"; }
                if (model.MOVEMENTTYPENAME == "Door to port") { MName = "Door to airport"; }
                if (model.MOVEMENTTYPENAME == "Port to door") { MName = "Airport to door"; }
                if (model.MOVEMENTTYPENAME == "Door to door") { MName = "Door to door"; }
                Ploading = "Airport of loading";
                Pdischarge = "Airport of discharge";
            }
            else
            {
                MName = model.MOVEMENTTYPENAME;
                Ploading = "Port of loading";
                Pdischarge = "Port of discharge";
            }

            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mbookingbordertable = new PdfPTable(3);
            mbookingbordertable.WidthPercentage = 100;
            mbookingbordertable.SpacingBefore = 10;
            mbookingbordertable.DefaultCell.Border = 0;
            PdfPTable mbookingTab = new PdfPTable(9);
            mbookingTab.SetWidths(new float[] { 10, 1, 13, 17, 1, 20, 18, 1, 20 });
            mbookingTab.WidthPercentage = 100;
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Booking ID")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(jobdetails.OPERATIONALJOBNUMBER == null ? "" : jobdetails.OPERATIONALJOBNUMBER)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Origin City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.ORIGINPLACENAME == null ? "" : model.ORIGINPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold(model.DESTINATIONPLACENAME == null ? "" : "Destination City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPLACENAME == null ? "" : ":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPLACENAME == null ? "" : model.DESTINATIONPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Mode of transport")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.PRODUCTNAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Movement type")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(MName)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Enquiry Date")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DATEOFENQUIRY == null ? "" : Convert.ToString(model.DATEOFENQUIRY.ToShortDateString()))));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Quote valid until")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DATEOFVALIDITY == null ? "" : Convert.ToString(model.DATEOFVALIDITY.ToShortDateString()))));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Pickup City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.ORIGINPLACENAME == null ? "" : model.ORIGINPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold(Ploading)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.ORIGINPORTNAME == null ? "" : model.ORIGINPORTNAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Deliver City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPLACENAME == null ? "" : model.DESTINATIONPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold(Pdischarge)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPORTNAME == null ? "" : model.DESTINATIONPORTNAME)));
            if (model.ISHAZARDOUSCARGO != 0)
            {
                mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Hazardous Cargo")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.HAZARDOUSGOODSTYPE == null ? "" : model.HAZARDOUSGOODSTYPE)));
            }
            else
            {
                mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell("")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell("")));
            }
            PdfPCell mRoutecell = new PdfPCell(mbookingTab);
            mRoutecell.Colspan = 3;
            mbookingbordertable.AddCell(mRoutecell);

            return mbookingbordertable;
        }
        public static PdfPTable cargoTableModel(JobBookingModel model)
        {
            PdfPTable mcargoTab = new PdfPTable(10);
            Formattingpdf mformatcargopdf = new Formattingpdf();
            PdfPTable mCargobordertable = new PdfPTable(1);
            mCargobordertable.WidthPercentage = 100;
            mCargobordertable.SpacingBefore = 10;
            mCargobordertable.DefaultCell.Border = 0;
            if (model.PRODUCTTYPEID == 6)
                mcargoTab = new PdfPTable(9);
            mCargobordertable.AddCell(new PdfPCell(mformatcargopdf.TableCaption("Cargo Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Package Type", 0, false)));
            if (model.PRODUCTTYPEID == 5)
                mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Container", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Quantity", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Unit", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Length", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Width", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Height", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("PPW", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("TW", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Unit", 0, false)));
            if (model.PRODUCTTYPEID == 5)
                mcargoTab.SetWidths(new float[] { 20, 10, 10, 5, 10, 10, 10, 5, 5, 5 });
            else if (model.PRODUCTTYPEID == 6)
                mcargoTab.SetWidths(new float[] { 20, 10, 10, 10, 10, 10, 10, 10, 10 });

            mcargoTab.WidthPercentage = 100;
            if (model.PRODUCTTYPEID == 5)
            {
                List<JobBookingShipmentModel> SortedList = model.JobShipmentItems.OrderBy(o => o.ContainerCode).ToList();

                foreach (var item in SortedList)
                {

                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.PackageTypeName == null ? "" : item.PackageTypeName)));
                    if (item.ContainerCode == "22G0")
                        item.ContainerCode = "20'GP";
                    else if (item.ContainerCode == "42G0")
                        item.ContainerCode = "40'GP";
                    else if (item.ContainerCode == "45G0")
                        item.ContainerCode = "45'HC";
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ContainerCode == null ? "" : Convert.ToString(item.ContainerCode))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.Quantity == 0 ? "" : Convert.ToString(item.Quantity))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.LengthUOMCode == null ? "" : item.LengthUOMCode)));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemLength == 0 ? "" : Convert.ToString(item.ItemLength))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemWidth == 0 ? "" : Convert.ToString(item.ItemWidth))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemHeight == 0 ? "" : Convert.ToString(item.ItemHeight))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightPerPiece == 0 ? "" : Convert.ToString(item.WeightPerPiece))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightTotal == 0 ? "" : Convert.ToString(item.WeightTotal))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightUOMName == null ? "" : item.WeightUOMName)));
                }

            }
            else if (model.PRODUCTTYPEID == 6)
            {
                foreach (var item in model.ShipmentItems)
                {
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.PackageTypeName == null ? "" : item.PackageTypeName)));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.Quantity == 0 ? "" : Convert.ToString(item.Quantity))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.LengthUOMCode == null ? "" : item.LengthUOMCode)));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemLength == 0 ? "" : Convert.ToString(item.ItemLength))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemWidth == 0 ? "" : Convert.ToString(item.ItemWidth))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemHeight == 0 ? "" : Convert.ToString(item.ItemHeight))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightPerPiece == 0 ? "" : Convert.ToString(item.WeightPerPiece))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightTotal == 0 ? "" : Convert.ToString(item.WeightTotal))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightUOMName == null ? "" : item.WeightUOMName)));

                }
            }
            PdfPCell mRoutecell = new PdfPCell(mcargoTab);
            mCargobordertable.AddCell(mRoutecell);
            return mCargobordertable;
        }
        public static PdfPTable containerTableModel(JobBookingModel model)
        {
            Formattingpdf mformatcargopdf = new Formattingpdf();
            PdfPTable mCargobordertable = new PdfPTable(1);
            mCargobordertable.WidthPercentage = 100;
            mCargobordertable.SpacingBefore = 10;
            mCargobordertable.DefaultCell.Border = 0;
            PdfPTable mcargoTab = new PdfPTable(3);
            mCargobordertable.AddCell(new PdfPCell(mformatcargopdf.TableCaption("Container Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Package Type", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Quantity", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Container", 0, false)));
            mcargoTab.SetWidths(new float[] { 40, 10, 60 });
            mcargoTab.WidthPercentage = 100;
            foreach (var item in model.ShipmentItems)
            {
                mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.PackageTypeName == null ? "" : item.PackageTypeName)));
                mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.Quantity == 0 ? "" : Convert.ToString(item.Quantity))));
                mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ContainerName == null ? "" : item.ContainerName.Trim())));
            }
            PdfPCell mRoutecell = new PdfPCell(mcargoTab);
            mCargobordertable.AddCell(mRoutecell);
            return mCargobordertable;
        }
        public static PdfPTable ShipperTableModel(JobBookingModel model)
        {
            if (model.PartyItems.Count > 0)
            {
                foreach (var item in model.PartyItems)
                {
                    if (item.PARTYTYPE == "91")
                    {
                        model.SHCLIENTNAME = item.CLIENTNAME;
                    }
                    else if (item.PARTYTYPE == "92")
                    {
                        model.CONCLIENTNAME = item.CLIENTNAME;
                    }
                }
            }

            if (model.JobItems.Count > 0)
            {
                foreach (var item in model.JobItems)
                {
                    model.JCARGOAVAILABLEFROM = item.CARGOAVAILABLEFROM;
                    model.JCARGODELIVERYBY = item.CARGODELIVERYBY;
                }
            }
            Formattingpdf mformatshippdf = new Formattingpdf();
            PdfPTable mshipperbordertable = new PdfPTable(1);
            mshipperbordertable.WidthPercentage = 100;
            mshipperbordertable.SpacingBefore = 10;
            mshipperbordertable.DefaultCell.Border = 0;
            PdfPTable mshipperTab = new PdfPTable(2);
            mshipperTab.SetWidths(new float[] { 25, 75 });
            mshipperTab.WidthPercentage = 100;
            mshipperbordertable.AddCell(new PdfPCell(mformatshippdf.TableCaption("Shipper&Consignee Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Shipper Name")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.SHCLIENTNAME == null ? "" : model.SHCLIENTNAME)));
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Consignee Name")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.SHCLIENTNAME == null ? "" : model.CONCLIENTNAME)));
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Expected Cargo Available Date")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.JCARGOAVAILABLEFROM == DateTime.MinValue ? "" : Convert.ToString(model.JCARGOAVAILABLEFROM.ToShortDateString()))));
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Estimated Cargo Delivery Date")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.JCARGODELIVERYBY == DateTime.MinValue ? "" : Convert.ToString(model.JCARGODELIVERYBY.ToShortDateString()))));
            PdfPCell mRoutecell = new PdfPCell(mshipperTab);
            mRoutecell.Colspan = 3;
            mshipperbordertable.AddCell(mRoutecell);
            return mshipperbordertable;
        }
        public static PdfPTable PaymenTabletModel(PaymentModel model)
        {
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mshipperbordertable = new PdfPTable(1);
            mshipperbordertable.WidthPercentage = 100;
            mshipperbordertable.SpacingBefore = 10;
            mshipperbordertable.DefaultCell.Border = 0;
            PdfPTable mQuoteTab = new PdfPTable(2);
            mQuoteTab.SetWidths(new float[] { 25, 75 });
            mQuoteTab.WidthPercentage = 100;
            mshipperbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Benefeciary Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Acc. Holder Name")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.BeneficiaryName)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Account No")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.BankAccountNumber)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Swift Code")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.SwiftCode == null ? "" : model.EntityList.SwiftCode)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("IFSC/IBAN Code")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.IFSCCode)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Bank/Branch Name")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.BankName + "/" + model.EntityList.Branch)));
            PdfPCell mRoutecell = new PdfPCell(mQuoteTab);
            mshipperbordertable.AddCell(mRoutecell);
            return mshipperbordertable;
        }

        public static PdfPTable TransactionTabletModel(PaymentTransactionDetailsModel transaction)
        {
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mPaymentbordertable = new PdfPTable(1);
            mPaymentbordertable.WidthPercentage = 100;
            mPaymentbordertable.SpacingBefore = 10;
            mPaymentbordertable.DefaultCell.Border = 0;
            PdfPTable mpaymentTab = new PdfPTable(2);
            mpaymentTab.SetWidths(new float[] { 25, 75 });
            mpaymentTab.WidthPercentage = 100;
            mPaymentbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Transaction Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Bank")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBANKNAME)));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBRANCHNAME)));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Transaction Ref No.")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.PAYMENTREFNUMBER)));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Date")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(transaction.TRANSCHEQUEDATE.Value.ToShortDateString()))));
            if (!string.IsNullOrEmpty(transaction.TRANSDESCRIPTION))
            {
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Description")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSDESCRIPTION)));
            }
            PdfPCell mpRoutecell = new PdfPCell(mpaymentTab);
            mPaymentbordertable.AddCell(mpRoutecell);
            return mPaymentbordertable;
        }

        public PdfPTable payatbranchModel(PaymentTransactionDetailsModel transaction)
        {

            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mPaymentbordertable = new PdfPTable(1);
            mPaymentbordertable.WidthPercentage = 100;
            mPaymentbordertable.SpacingBefore = 10;
            mPaymentbordertable.DefaultCell.Border = 0;
            PdfPTable mpaymentTab = new PdfPTable(2);
            mpaymentTab.SetWidths(new float[] { 25, 75 });
            mpaymentTab.WidthPercentage = 100;
            mPaymentbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Transaction Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            if (transaction.TRANSMODE.ToLower() == "cash")
            {
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Transaction Mode")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSMODE)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Comments")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSDESCRIPTION)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Date")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(transaction.TRANSCHEQUEDATE.Value.ToShortDateString()))));
                if (!ReferenceEquals(Session["BranchAddress"], null))
                {
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch Address")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(Session["BranchAddress"]))));
                }
            }
            else if (transaction.TRANSMODE.ToLower() == "cheque" || transaction.TRANSMODE.ToLower() == "demand draft")
            {
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Bank")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBANKNAME)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBRANCHNAME)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Cheque/DD Number")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSCHEQUENO)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Date")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(transaction.TRANSCHEQUEDATE.Value.ToShortDateString()))));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Comments")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSDESCRIPTION)));
                if (!ReferenceEquals(Session["BranchAddress"], null))
                {
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch Address")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(Session["BranchAddress"]))));
                }
                else
                {
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
            }
            PdfPCell mpRoutecell = new PdfPCell(mpaymentTab);
            mPaymentbordertable.AddCell(mpRoutecell);
            return mPaymentbordertable;
        }

        #endregion

        [AllowAnonymous]
        public void SendCreditAppMail(string Approvedamount, string Comments, string Name)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string ToMailIds = mFactory.GetGlobalMngmntMailIds();

            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(ToMailIds);
            message.To.Add("help@shipafreight.com");
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Approve Credit Request" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/Approve-CreditRequest.html");


            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{ApprovedAmount}", Approvedamount);
            body = body.Replace("{Comments}", Comments);
            body = body.Replace("{User}", Name);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", ToMailIds, "", "",
              "JobBooking", "Credit Request", "", HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
        }

        [AllowAnonymous]
        public void SendCreditEformsSubmitMail(string RequestedAmount, string Comments, string Name)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string ToMailIds = mFactory.GetGlobalMngmntMailIds();

            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(ToMailIds);
            message.To.Add("help@shipafreight.com");
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Credit Request Submitted to Eforms" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/Credit-SubmitToEforms.html");


            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{RequestedAmount}", RequestedAmount);
            body = body.Replace("{Comments}", Comments);
            body = body.Replace("{User}", Name);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", ToMailIds, "", "",
             "JobBooking", "Credit Request Submitted to Eforms", ToMailIds, HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
        }

        [AllowAnonymous]
        public void FocisCallAdvanceReceiptPDF(string id, string JobNumber, string OperationJobNumber, string CreatedBy, string ChargeSetId = "")
        {
            long Quoteid = Convert.ToInt64(id);
            int jobNumber1 = Convert.ToInt32(JobNumber);

            UserDBFactory mFactory = new UserDBFactory();
            mUserModel = mFactory.GetUserProfileDetails(CreatedBy, 1);
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(Quoteid));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mQuoFactory.GetById(Quoteid);
                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {
                if (ChargeSetId != null && ChargeSetId != "")
                {
                    GenerateAdvanceReceiptPDF(jobNumber1, "Advance Receipt", Quoteid, OperationJobNumber, CreatedBy, Convert.ToInt32(ChargeSetId));
                }
                else
                {
                    GenerateAdvanceReceiptPDF(jobNumber1, "Advance Receipt", Quoteid, OperationJobNumber, CreatedBy, 0);
                    try
                    {
                        JobBookingDbFactory mJBFactory = new JobBookingDbFactory();
                        string dtls = mJBFactory.GetCouponCode(jobNumber1);
                        string[] data = dtls.Split(',');

                        if (data[0].ToUpper().Contains("M1"))
                        {
                            SendReferal_CreditAdded_Email(data[0], CreatedBy, data[2].ToString(), Convert.ToDouble(data[1]));
                        }
                        else if (data[0].ToUpper().Contains("M3"))
                        {
                            SendReferal_CreditAdded_Email(data[0], CreatedBy, data[2].ToString(), 0);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            if (ChargeSetId == null || ChargeSetId == "")
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    GenerateBookingConfirmationPDF(jobNumber1, "Booking Confirmation", Quoteid, OperationJobNumber, CreatedBy);
                }
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public JsonResult InviteParty(int SelectedClient, string Previlages, string SelectEmailID, int JobNumber, int Countryid, string Party)
        {
            string Status = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            try
            {
                TokenDetails token = GenerateToken("USER_ACTIVATION_TOKEN_EXPIRY");
                string invitedUserId = AESEncrytDecry.EncryptStringAES(SelectEmailID);
                string Token = token.Token;
                DateTime TokenExpiry = token.TokenExpiry;
                string password = AESEncrytDecry.EncryptStringAES("Ux1AES");
                Status = mFactory.InviteParty(SelectedClient, Previlages, Token, TokenExpiry, invitedUserId, password, JobNumber, Countryid, SelectEmailID, Party);
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public TokenDetails GenerateToken(string ConfigName)
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            UserDBFactory mFactory = new UserDBFactory();
            string ConfigValue = mFactory.GetUMConfigValue(ConfigName);
            TokenDetails tobj = new TokenDetails();
            tobj.Token = token;
            tobj.TokenExpiry = DateTime.UtcNow.AddHours(int.Parse(ConfigValue));
            return tobj;
        }

        public JsonResult ResendActivation(string mailid, string status, string party)
        {

            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            string Status = string.Empty;
            UserDBFactory mFactory = new UserDBFactory();
            UserEntity mDBEntity = mFactory.GetUserDetails(mailid);
            // Generae password token that will be used in the email link to authenticate user
            TokenDetails token = GenerateToken("USER_ACTIVATION_TOKEN_EXPIRY");
            mDBEntity.TOKEN = token.Token;
            mDBEntity.TOKENEXPIRY = token.TokenExpiry;

            // Generate the html link sent via email
            string strUserId = AESEncrytDecry.EncryptStringAES(mailid);
            string strEncryptURL = AESEncrytDecry.EncryptStringAES("/");
            string subject = "";
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            subject = "Account Activation for user " + mailid + EnvironmentName;
            string resetLink = Url.Action("ActivateUser", "UserManagement", new { rt = token.Token, UserId = strUserId, ReturnUrl = strEncryptURL }, "http");

            resetLink = Actionlink + "UserManagement/ActivateUser?" + resetLink.Split('?')[1];

            string userName = mDBEntity.FIRSTNAME;
            string body = string.Empty;
            if (status.ToLower() == "active")
            {
                var SuccessinviteLink = Actionlink + "UserManagement/Login";
                body = createEmailBody(userName, SuccessinviteLink, status, mailid, party);
            }
            else
            {
                body = createEmailBody(userName, resetLink, status, mailid, party);
            }

            MailMessage message = new MailMessage();
            message.To.Add(mailid);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            bool IsMailSent = SendMailNotification(message, body, mailid, "Account Activation");
            if (IsMailSent)
            {
                mFactory.UpdateUserToken(mDBEntity.TOKEN, mDBEntity.TOKENEXPIRY, mailid);
                Status = "success";
            }
            else
            {
                Status = "fail";
            }
            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        private string createEmailBody(string userName, string url, string status, string mailid, string party)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/InvitePartyMail.html")))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{user}", ((string.IsNullOrEmpty(userName) != true) ? "Hi " + userName : string.Empty));
            body = body.Replace("{url}", url);

            if (status.ToLower() == "active")
            {
                body = body.Replace("{btnText}", "Login");
                body = body.Replace("{Username}", "");
                body = body.Replace("{Password}", "");
            }
            else if (status.ToLower() == "notactive" || status.ToLower() == "newuser")
            {
                if (status.ToLower() == "newuser")
                {
                    body = body.Replace("{Username}", "Username: " + mailid);
                    body = body.Replace("{Password}", "Password: " + "Ux1AES");
                }
                else
                {
                    body = body.Replace("{Username}", "");
                    body = body.Replace("{Password}", "");
                }
                body = body.Replace("{btnText}", "Activate Account");
            }
            body = body.Replace("{party}", party);
            body = body.Replace("{invitedBy}", HttpContext.User.Identity.Name);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            return body;
        }

        public bool SendMailNotification(MailMessage message, string body, string mailid, string submodule)
        {
            bool IsMailSent = false;
            SmtpClient mySmtpClient = new SmtpClient();
            if (mySmtpClient.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory && mySmtpClient.PickupDirectoryLocation.StartsWith("~"))
            {
                string root = AppDomain.CurrentDomain.BaseDirectory;
                string pickupRoot = mySmtpClient.PickupDirectoryLocation.Replace("~/", root);
                pickupRoot = pickupRoot.Replace("/", @"\");
                mySmtpClient.PickupDirectoryLocation = pickupRoot;
            }
            try
            {
                mySmtpClient.Send(message);
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", mailid, "", "",
              "JobBooking", submodule, mailid, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
                IsMailSent = true;
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Issue sending email: " + e.Message);
            }
            return IsMailSent;
        }

        [HttpGet]
        [Authorize]
        [EncryptedActionParameter]
        public ActionResult Collaborated(int id, int? JobNumber)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            UserDBFactory UserFactory = new UserDBFactory();
            decimal CountryId = 0;

            Session["Jobnumber"] = JobNumber;
            DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetails(Convert.ToInt32(id), Convert.ToInt64(JobNumber));
            mUIModel.CREATEDBY = mDBEntity.CREATEDBY;
            var jobdet = mDBEntity.JobItems;
            var partydet = mDBEntity.PartyItems;
            if (jobdet != null && jobdet.Count > 0)
            {
                mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                mDBEntity.JSTATEID = jobdet[0].STATEID;
                mDBEntity.SPECIALINSTRUCTION = jobdet[0].SPECIALINSTRUCTIONS == null ? "" : jobdet[0].SPECIALINSTRUCTIONS;
                if (partydet.Any())
                {
                    foreach (var items in partydet)
                    {
                        if (items.PARTYTYPE == "91")
                        {
                            mDBEntity.SHCLIENTID = Convert.ToString(items.SSPCLIENTID);
                            mDBEntity.SHPARTYDETAILSID = items.PARTYDETAILSID;
                            CountryId = items.COUNTRYID;
                            mDBEntity.SHCOUNTRYID = Convert.ToString(items.COUNTRYID);
                        }
                        else
                        {
                            mDBEntity.CONCLIENTID = Convert.ToString(items.SSPCLIENTID);
                            mDBEntity.CONPARTYDETAILSID = items.PARTYDETAILSID;
                            mDBEntity.CONCOUNTRYID = Convert.ToString(items.COUNTRYID);
                        }
                    }
                }
            }

            mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(HttpContext.User.Identity.Name, "", 0);
            List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);
            mUIModel.PartiesModel = PartyModel;
            if (partydet.Count == 0)
            {
                UserPartyMasterModel user = PartyModel.Where(x => x.UPERSONAL == 1).FirstOrDefault();
                if (user == null)
                {
                    mDBEntity.SHCOUNTRYID = "0";
                    mUIModel.SHCOUNTRYID = "0";
                    mUIModel.SHCLIENTID = "0";
                }
                else
                {
                    mDBEntity.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
                    mUIModel.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
                    mUIModel.SHCLIENTID = Convert.ToString(user.SSPCLIENTID);
                }

            }
            else
            {
                //if shipper or consignee deleted from parties.
                int index = PartyModel.FindIndex(item => item.SSPCLIENTID == Convert.ToInt64(mDBEntity.SHCLIENTID));
                if (index < 0)
                {
                    foreach (var item in PartyModel.Where(x => x.UPERSONAL == 1))
                    {
                        mUIModel.UPERSONALID = Convert.ToString(item.SSPCLIENTID);
                        mUIModel.UPERSONALCOUNTRYID = Convert.ToString(item.COUNTRYID);
                    }
                }
                int index1 = PartyModel.FindIndex(item => item.SSPCLIENTID == Convert.ToInt64(mDBEntity.CONCLIENTID));
                if (index1 < 0)
                {
                    foreach (var item in PartyModel.Where(x => x.UPERSONAL == 1))
                    {
                        mUIModel.CONUPERSONALID = Convert.ToString(item.SSPCLIENTID);
                        mUIModel.CONUPERSONALCOUNTRYID = Convert.ToString(item.COUNTRYID);
                    }
                }
            }

            //----HsCode Instructions----
            //Need to uncomment below code with respect to HSCode implementation
            List<HsCodeInstructionModel> hscodeList = new List<HsCodeInstructionModel>();
            foreach (var obj in mDBEntity.HSCodeInsItems)
            {
                HsCodeInstructionModel hscodemod = new HsCodeInstructionModel();

                hscodemod.HSCODE = obj.HSCODE;
                hscodeList.Add(hscodemod);
            }
            mUIModel.HSCodeInsmodel = hscodeList;



            ////----HsCode Duty----
            //List<HsCodeDutyModel> hscodeDutyList = new List<HsCodeDutyModel>();
            //foreach (var obj in mDBEntity.HSCodeDutyItems)
            //{
            //    HsCodeDutyModel hscodeDuty = new HsCodeDutyModel();

            //    hscodeDuty.HSCODE = obj.HSCODE;
            //    hscodeDuty.DUTYTYPE = obj.DUTYTYPE;
            //    hscodeDuty.DUTYRATEPERCENTAGE = obj.DUTYRATEPERCENTAGE;

            //    hscodeDutyList.Add(hscodeDuty);
            //}
            //mUIModel.HSCodeDutymodel = hscodeDutyList;

            DBFactory.Entities.CargoAvabilityEntity CargoDate = mFactory.GetPickUpTransitDates(Convert.ToInt64(CountryId));
            mUIModel.CargoAvabilityModel = Mapper.Map<CargoAvabilityEntity, CargoAvabilityModel>(CargoDate);
            MDMDataFactory mdmFactory = new MDMDataFactory();
            IList<IncoTermsEntity> incoterms = mdmFactory.GetIncoTerms(string.Empty, string.Empty);
            ViewBag.incoterms = incoterms;


            List<string> ContainerTypes = new List<string>();
            foreach (var item in mUIModel.ShipmentItems)
            {
                if (item.PackageTypeId == 1)
                {
                    ContainerTypes.Add(item.ContainerName.Trim());
                }
            }
            if (ContainerTypes.Count > 0)
            {
                ViewBag.ContainerType = ContainerTypes;
            }
            ViewBag.loginuser = HttpContext.User.Identity.Name.ToLower();
            TempData["JobBookingData"] = mUIModel;
            return View("Collaborated", mUIModel);
        }

        private void SendSubmittedToCustomer(string Email, string party, string OPNumber)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight- Collaboration request completed -" + OPNumber + EnvironmentName;
            string mapPath = Server.MapPath("~/App_Data/SubmittedtoCustomerCollaborated.html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{user}", Email);
            body = body.Replace("{OPNumber}", OPNumber);
            body = body.Replace("{party}", party);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
             "JobBooking", "Collaboration request", OPNumber, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
        }

        public JsonResult GetDiscountedValue(string CouponCode, string JobNumber)
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                string result = JBFactory.GetDiscountedValue(CouponCode, JobNumber, HttpContext.User.Identity.Name);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult GetDiscountedReferralValue(string CouponCode, string JobNumber)
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                string result = JBFactory.GetDiscountedReferralValue(CouponCode, JobNumber, HttpContext.User.Identity.Name);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public DateTime GetDiscountData(string CouponCode)
        {
            JobBookingDbFactory JBFactory = new JobBookingDbFactory();
            DateTime dicountExpiryDate = JBFactory.GetDiscountDateByDiscountCode(CouponCode.ToUpper());
            return dicountExpiryDate;
        }
        //[Authorize]
        //[HttpPost]
        //public JsonResult GetAlternateEmail(string User, string Type, int PartyId)
        //{
        //    try
        //    {
        //        JobBookingDbFactory mFactory = new JobBookingDbFactory();
        //        DataTable dt = mFactory.GetAlternateEmail(User, Type, PartyId);
        //        if (dt.Rows.Count > 0)
        //        {
        //            List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
        //            var JsonToReturn = new
        //            {
        //                rows = trows,
        //            };
        //            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        //        }
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //    catch
        //    {
        //        return Json(Url.Action("Error", "Error"));
        //    }
        //}       

        public string CheckPaymentStatus(int jobnumber)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string result = string.Empty;
            var ds = mFactory.CheckPaymentStatus(jobnumber);
            if (ds.Tables[0].Rows.Count > 0)
            {
                result = ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                result = "NOTEXIST";
            }
            return result;
        }

        [AllowAnonymous]
        public ActionResult AdditionalChangesSummary(int JobNumber, int id, string sourceType, int chargeSetid)
        {
            string BookingId = string.Empty;
            string users = string.Empty;
            string Currency = string.Empty;
            decimal AdditionalChargeAmount = 0;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var OneSignalService = new Push.OneSignal();
            DataTable AdditionalCharges = mFactory.GetAdditionalCharges(JobNumber, id, sourceType);
            if (AdditionalCharges.Rows.Count > 0)
            {
                BookingId = AdditionalCharges.Rows[0]["OPERATIONALJOBNUMBER"].ToString();
                users = AdditionalCharges.Rows[0]["CREATEDBY"].ToString();
                AdditionalChargeAmount = Convert.ToDecimal(AdditionalCharges.Rows[0]["ADDITIONALCHARGEAMOUNT"]);
                Currency = AdditionalCharges.Rows[0]["CURRENCY"].ToString();
            }
            try
            {
                string str = mFactory.NewNotification(5119, "Additional Charges", BookingId, JobNumber, id, chargeSetid, users);
                var pushdata = new { NOTIFICATIONTYPEID = 5119, NOTIFICATIONCODE = "Additional Charges", JOBNUMBER = JobNumber, BOOKINGID = BookingId, QUOTATIONID = id, CHARGESETID = chargeSetid, USERID = users };
                SafeRun<bool>(() => { return OneSignalService.SendPushMessageByTag("Email", users.ToLowerInvariant(), "Additional Charges", pushdata); });
            }
            catch (Exception ex)
            {
                //
            }
            try
            {
                SendMailForAdditionalCharges(JobNumber, BookingId, sourceType, AdditionalChargeAmount, users, id, chargeSetid, Currency);
            }
            catch (Exception ex1)
            {
                //
            }

            return null;
        }

        //[AllowAnonymous]
        //public void testing(int quotationId)
        //{
        //    QuotationDBFactory mQuoFactory = new QuotationDBFactory();
        //    DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(quotationId));
        //    mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);



        //    string AttachmentIds = string.Empty;
        //    JobBookingDbFactory mFactory = new JobBookingDbFactory();

        //    MailMessage message = new MailMessage();
        //    message.From = new MailAddress("noreply@shipafreight.com");
        //    message.To.Add(new MailAddress("agaddipati@agility.com"));
        //    string EnvironmentName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
        //    if (EnvironmentName == "AgileServer" || EnvironmentName.ToLowerInvariant().Contains("demo"))
        //    {
        //        EnvironmentName = "AGILE";
        //        message.Subject = "ShipA Freight - payment successful through Business credit" + "-" + EnvironmentName;
        //    }
        //    else
        //    {
        //        message.Subject = "ShipA Freight - payment successful through Business credit";
        //    }
        //    message.IsBodyHtml = true;
        //    string body = string.Empty;
        //    string mapPath = Server.MapPath("~/App_Data/payment-credit-success.html");
        //    //using streamreader for reading my email template 
        //    using (StreamReader reader = new StreamReader(mapPath))
        //    { body = reader.ReadToEnd(); }
        //    body = body.Replace("{user_name}", "Anil");
        //    body = body.Replace("{BookingID}", "1234");
        //    body = body.Replace("{currency}", "USD");

        //    body = body.Replace("{Amount}", "1000");

        //    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
        //    body = body.Replace("{Track_Cons}", Track);
        //    body = body.Replace("{Ports_cons}", Ports);

        //    string refNumber = "BC" + "1234";

        //    message.IsBodyHtml = true;
        //    message.Body = body;
        //    SmtpClient client = new SmtpClient();
        //    client.Send(message);
        //    client.Dispose();
        //    message.Dispose();
        //}


        [EncryptedActionParameter]
        public ActionResult AdditionalChargesList(int JobNumber, int id, int chargeSetid, string sourceType = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string user = string.Empty;
            string FirstName = string.Empty;
            string AddChargeSrc = string.Empty;
            DataTable UserDtls = mFactory.GetUserDtls(JobNumber);
            if (UserDtls.Rows.Count > 0)
            {
                FirstName = UserDtls.Rows[0]["FIRSTNAME"].ToString();
                user = UserDtls.Rows[0]["USERID"].ToString();
            }
            FormsAuthentication.SetAuthCookie(user, false);
            var myCookie = new HttpCookie("myCookie");
            myCookie.Values.Add("UserName", FirstName);
            myCookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(myCookie);

            JobBookingModel mUIModel = new JobBookingModel();
            DataTable AdditionalCharges = mFactory.GetAdditionalCharges(JobNumber, id, sourceType);
            if (AdditionalCharges.Rows.Count > 0)
            {
                AddChargeSrc = AdditionalCharges.Rows[0]["ADDITIONALCHARGESTATUS"].ToString();
            }

            if (AddChargeSrc != null && (AddChargeSrc.ToLower() == "pay now" || AddChargeSrc.ToLower() == "pay later"))
            {
                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetAdditionalChargeDetails(Convert.ToInt32(id), Convert.ToInt64(JobNumber), AddChargeSrc, chargeSetid);
                var jobdet = mDBEntity.JobItems;
                var partydet = mDBEntity.PartyItems;
                var shipperdet = (from u in partydet where u.PARTYTYPE == "91" select u).ToArray();
                var consigneedet = (from u in partydet where u.PARTYTYPE == "92" select u).ToArray();
                if (jobdet != null)
                {
                    mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                    mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                    mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                    mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                    mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                    mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                    mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                    mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                    mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                    mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                    mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                    mDBEntity.JSTATEID = jobdet[0].STATEID;

                }
                if (shipperdet != null && shipperdet.Length > 0)
                {
                    mDBEntity.SHCLIENTNAME = shipperdet[0].CLIENTNAME;
                }
                if (consigneedet != null && consigneedet.Length > 0)
                {
                    mDBEntity.CONCLIENTNAME = consigneedet[0].CLIENTNAME;
                }
                mDBEntity.CHARGESETID = chargeSetid;
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                ViewBag.decimalCount = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PREFERREDCURRENCYID);
                return View("AdditionalChargeSummary", mUIModel);
            }
            else
                return null;
        }

        public void SendMailForAdditionalCharges(int JobNumber, string BookingId, string sourceType, decimal AdditionalChargeAmount, string user, int id, int chargeSetid, string Currency)
        {
            try
            {
                Assignvalues(JobNumber, chargeSetid, sourceType);
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                string FirstName = string.Empty;
                string Message = string.Empty;
                string resetLink = string.Empty;
                DataTable UserDtls = mFactory.GetUserDtls(JobNumber);
                if (UserDtls.Rows.Count > 0)
                {
                    FirstName = UserDtls.Rows[0]["FIRSTNAME"].ToString();
                }
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(user));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight Alert - Additional Charges Apply to Your Shipment" + "( Booking ID #" + BookingId + " )" + EnvironmentName;
                if (sourceType.ToLower() == "pay now")
                {
                    Message = "Additional charges have been incurred during the handling of your shipment. See charge details below. Please arrange for payment of these charges to ensure your cargo is delivered on schedule.";
                }
                else if (sourceType.ToLower() == "pay later")
                {
                    Message = "Additional charges have been incurred during the handling of your shipment. See charge details below. Please confirm acceptance of these charges to ensure your cargo is delivered on schedule. You will be invoiced for these charges at a later date.";
                }

                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                string url = "JobNumber=" + JobNumber + "?id=" + id + "?chargeSetid=" + chargeSetid + "?sourceType=" + sourceType;
                JsonResult t1 = EncryptId(url);
                string rs = t1.Data.ToString();

                resetLink = Actionlink + "JobBooking/AdditionalChargesList?" + rs;

                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = Server.MapPath("~/App_Data/AddtionalChargeNotification.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{statusMessage}", Message);
                body = body.Replace("{link}", resetLink);
                body = body.Replace("{user_name}", FirstName);
                if (sourceType.ToLower() == "pay now")
                {
                    body = body.Replace("{chargeType}", "Upon Receipt");
                    body = body.Replace("{SourceType}", sourceType);
                }
                else if (sourceType.ToLower() == "pay later")
                {
                    body = body.Replace("{chargeType}", "Upon Receipt of Final Invoice");
                    body = body.Replace("{SourceType}", "Accept Charges");
                }
                string amount = mFactoryRound.RoundedValue(Convert.ToString(AdditionalChargeAmount), Currency) + " " + Currency;

                body = body.Replace("{Amount}", amount);
                body = body.Replace("{BookingID}", BookingId);
                body = body.Replace("{ChargeDescriptionList}", chargeDescription);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", user, "", "",
            "JobBooking", "Additional Charges Apply", BookingId, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        private void SendMailForAdditionalChargesApproved(int JobNumber, string BookingId, decimal AdditionalChargeAmount, string Currency)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                QuotationEntity qEntity = new QuotationEntity();
                PaymentEntity pEntity = new PaymentEntity();
                string FirstName = string.Empty;
                string UserID = string.Empty;
                string Message = string.Empty;
                DataTable UserDtls = mFactory.GetUserDtls(JobNumber);
                if (UserDtls.Rows.Count > 0)
                {
                    FirstName = UserDtls.Rows[0]["FIRSTNAME"].ToString();
                    UserID = UserDtls.Rows[0]["USERID"].ToString();
                }
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(UserID));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - Additional charges accepted by customer" + "( Booking ID #" + BookingId + " )" + EnvironmentName;

                Message = "Additional charge accepted by customer.";
                string amount = mFactoryRound.RoundedValue(AdditionalChargeAmount.ToString(), Currency) + " " + Currency;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = Server.MapPath("~/App_Data/AddtionalChargeApprovedNotification.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{statusMessage}", Message);
                body = body.Replace("{user_name}", FirstName);
                body = body.Replace("{Amount}", amount);
                body = body.Replace("{BookingID}", BookingId);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", UserID, "", "",
              "JobBooking", "Additional charges accepted", BookingId, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
               JobandQuoteEntity mJobandQuote= mFactory.GetJobandQuotedetails(JobNumber);
               qEntity.ORIGINPLACENAME = mJobandQuote.ORIGINPLACENAME;
               qEntity.DESTINATIONPLACENAME = mJobandQuote.DESTINATIONPLACENAME;
               qEntity.ORIGINPORTNAME = mJobandQuote.ORIGINPORTNAME;
               qEntity.DESTINATIONPORTNAME = mJobandQuote.DESTINATIONPORTNAME;
               qEntity.OCOUNTRYNAME = mJobandQuote.OCOUNTRYNAME;
               qEntity.DCOUNTRYNAME = mJobandQuote.DCOUNTRYNAME;
               pEntity.LIVEUPLOADS=mJobandQuote.LIVEUPLOADS;
               pEntity.LOADINGDOCKAVAILABLE = mJobandQuote.LOADINGDOCKAVAILABLE;
               pEntity.CARGOPALLETIZED = mJobandQuote.CARGOPALLETIZED;
               pEntity.ORIGINALDOCUMENTSREQUIRED = mJobandQuote.ORIGINALDOCUMENTSREQUIRED;
               pEntity.TEMPERATURECONTROLREQUIRED = mJobandQuote.TEMPERATURECONTROLREQUIRED;
               pEntity.COMMERCIALPICKUPLOCATION = mJobandQuote.COMMERCIALPICKUPLOCATION;
               pEntity.SPECIALINSTRUCTIONS = mJobandQuote.SPECIALINSTRUCTIONS;
               SendMailtoGroup(BookingId, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE", "Paymentstatus", JobNumber, UserID, "Additional Charges", "Additional Charge Payment","","","","","",mJobandQuote.PRODUCTNAME,mJobandQuote.MOVEMENTTYPENAME,"","",mJobandQuote.COUNTRYNAME,qEntity,pEntity);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }
        public ActionResult AdditionalChargeAutoApprove(int JobNumber, int ChargeSetId)
        {
            string FirstName = string.Empty;
            string user = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable UserDtls = mFactory.GetUserDtls(JobNumber);
            if (UserDtls.Rows.Count > 0)
            {
                FirstName = UserDtls.Rows[0]["FIRSTNAME"].ToString();
                user = UserDtls.Rows[0]["USERID"].ToString();
            }
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(JobNumber, user);
            PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);

            miUIModel.UserId = user;
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(miUIModel.QuotationId));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(miUIModel.QuotationId));
                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
            }
            else
            {
                return RedirectToAction("Index", "Quotation");
            }
            try
            {
                GenerateApprovalpdf(miUIModel.QuotationId, miUIModel.JobNumber, ChargeSetId);
            }
            catch (Exception ex)
            {
                string tes = ex.ToString();
            }

            if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay later")
            {
                PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
                try
                {
                    BusinessCreditmodel.RECEIPTHDRID = miUIModel.RECEIPTHDRID;
                    BusinessCreditmodel.JOBNUMBER = miUIModel.JobNumber;
                    BusinessCreditmodel.QUOTATIONID = miUIModel.QuotationId;
                    BusinessCreditmodel.PAYMENTOPTION = "Pay Later";
                    BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                    BusinessCreditmodel.PAYMENTREFNUMBER = "Pay Later";
                    BusinessCreditmodel.PAYMENTREFMESSAGE = "approved_Additional";
                    BusinessCreditmodel.CREATEDBY = Request.IsAuthenticated ? user : "Guest";
                    BusinessCreditmodel.MODIFIEDBY = Request.IsAuthenticated ? user : "Guest";
                    BusinessCreditmodel.PAYMENTTYPE = "Approved by customer";
                    BusinessCreditmodel.TRANSMODE = "Approved by customer";
                    BusinessCreditmodel.TRANSAMOUNT = Convert.ToDouble(miUIModel.ReceiptNetAmount);
                    BusinessCreditmodel.TRANSCURRENCY = miUIModel.Currency;
                    BusinessCreditmodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                    BusinessCreditmodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                    BusinessCreditmodel.ADDITIONALCHARGESTATUS = "Approved by customer";

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);
                    string result = mFactory.AdditionalChargeAutoApprove(mUIModel, user);
                    SendMailForAdditionalChargesApproved(Convert.ToInt32(miUIModel.JobNumber), miUIModel.OPjobnumber, Convert.ToDecimal(miUIModel.ADDITIONALCHARGEAMOUNT), miUIModel.Currency);
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    string result = "fail";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return null;
            }
        }

        public JsonResult EncryptId(string plainText)
        {
            StringBuilder Result = new StringBuilder();

            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

            string[] mul = plainText.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            string JResult = Result.ToString();
            return Json(JResult, JsonRequestBehavior.AllowGet);
        }

        public static byte[] GenerateAdditionalChargeApprovePDF(MemoryStream memoryStream, JobBookingModel mUIModels, string bookingId, decimal amount)
        {
            z = 0;
            UserDBFactory mFactory = new UserDBFactory();
            mUserModel = mFactory.GetUserProfileDetails(mUIModels.CREATEDBY, 1);

            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mUIModels.PREFERREDCURRENCYID);

            mQuoteChargesTotal = 0;
            Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();
            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = "Approval Letter";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);
            doc.Add(mFormattingpdf.LineSeparator());

            Formattingpdf mSubpdf = new Formattingpdf();
            PdfPTable mSubtable = new PdfPTable(1);
            mSubtable.WidthPercentage = 100;
            mSubtable.SpacingBefore = 25;
            mSubtable.DefaultCell.Border = 0;

            mSubtable.AddCell((mSubpdf.TableContentCell("Sub: Approval of Additional charges to be billed during final invoice for booking " + bookingId + ".")));
            doc.Add(mSubtable);

            Formattingpdf mSubpdf1 = new Formattingpdf();
            PdfPTable mSubtable1 = new PdfPTable(1);
            mSubtable1.WidthPercentage = 100;
            mSubtable1.SpacingBefore = 25;
            mSubtable1.SpacingAfter = 20;
            mSubtable1.DefaultCell.Border = 0;

            mSubtable1.AddCell((mSubpdf1.TableContentCell("There is an additional charges of " + mUIModels.PREFERREDCURRENCYID + " " + amount.ToString("N" + decimalCount) + " for the booking " + bookingId + ". This is an approval letter for consent to be billed later for this particular booking and for reference by the origin and destination branches. ")));
            doc.Add(mSubtable1);

            if (mUIModels.PaymentCharges.Any())
            {
                var mAddcharges = mUIModels.PaymentCharges.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase);
                if (mAddcharges.Count() > 0)
                    doc.Add(ChargesTableAutoApprovalModel(mAddcharges, "Additional Charges", mUIModels.PREFERREDCURRENCYID, decimalCount));
            }

            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotalBorder.WidthPercentage = 100;
            mQuoteChargeTotalBorder.SpacingBefore = 10;
            mQuoteChargeTotalBorder.DefaultCell.Border = 0;
            mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

            PdfPTable mQuoteChargeTotal = new PdfPTable(2);
            mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotal.WidthPercentage = 100;
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PREFERREDCURRENCYID + " " + mQuoteChargesTotal.ToString("N" + decimalCount), Element.ALIGN_RIGHT)));

            PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
            mQuoteChargeTotalBorder.AddCell(mTotalcells);
            doc.Add(mQuoteChargeTotalBorder);

            Formattingpdf mFrompdf = new Formattingpdf();
            PdfPTable mFromtable = new PdfPTable(1);
            mFromtable.WidthPercentage = 100;
            mFromtable.SpacingBefore = 10;
            mFromtable.DefaultCell.Border = 0;

            mFromtable.AddCell((mFrompdf.TableContentCell("Approved By,")));
            mFromtable.AddCell((mFrompdf.TableContentCell(mUserModel.FIRSTNAME + " " + mUserModel.LASTNAME)));
            DateTime today = DateTime.Today;
            mFromtable.AddCell(mFrompdf.TableContentCell(today.ToString("dd/MM/yyyy")));
            doc.Add(mFromtable);

            doc.Close();

            bytes = memoryStream.ToArray();
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        public void GenerateApprovalpdf(long id, long JJOBNUMBER, int ChargeSetId)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            JobBookingModel mUIModels = new JobBookingModel();
            string TCount = string.Empty;
            DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetAdditionalChargeDetails(Convert.ToInt32(id), Convert.ToInt64(JJOBNUMBER), "Pay Later", ChargeSetId);
            mUIModels = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            // PreferredCurrencyId = mUIModels.PREFERREDCURRENCYID;
            DataTable AdvReceiptChargeDtls = null;
            DBFactory.Entities.AdvanceReceiptEntity AdvReceiptDtls = mFactory.GetAdvanceReceiptDtls(Convert.ToInt32(JJOBNUMBER));
            AdvReceiptChargeDtls = mFactory.GetAdditionalChargeSum(Convert.ToInt32(JJOBNUMBER), ChargeSetId);
            if (AdvReceiptChargeDtls.Rows.Count > 0)
            {
                TCount = AdvReceiptChargeDtls.Rows[0]["TCOUNT"].ToString();
            }
            else
            {
                TCount = "1";
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {
                byte[] mbytes = GenerateAdditionalChargeApprovePDF(memoryStream, mUIModels, mUIModels.JobItems[0].OPERATIONALJOBNUMBER, mUIModels.JobItems[0].ADDITIONALCHARGEAMOUNT);
                MemoryStream stream = new MemoryStream(mbytes);
                //Insert same data into db 
                SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                sspdoc.QUOTATIONID = Convert.ToInt64(id);
                sspdoc.DOCUMNETTYPE = "ShipAFreightDoc";
                DateTime today = DateTime.Today;
                sspdoc.FILENAME = "AddChargeApproval_" + TCount + ".pdf";
                sspdoc.FILEEXTENSION = "application/pdf";
                sspdoc.FILESIZE = mbytes.Length;
                sspdoc.FILECONTENT = mbytes;
                sspdoc.JOBNUMBER = JJOBNUMBER;
                sspdoc.DocumentName = "Approval Letter";
                DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

                Guid obj = Guid.NewGuid();
                string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();

                mjobFactory.SaveDocuments(docentity, HttpContext.User.Identity.Name, "System Generated", obj.ToString(), EnvironmentName);
            }
        }

        public static PdfPTable ChargesTableAutoApprovalModel(IEnumerable<PaymentCharges> model, string mHeaderText, string CurrencyId, int decimalCount)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Sum(sum => sum.NETAMOUNT).ToString());
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + ChargesTotal.ToString("N" + decimalCount), Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.CHARGENAME, Element.ALIGN_LEFT)));
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(Double.Parse(QC.NETAMOUNT.ToString()).ToString("N" + decimalCount).ToString(), Element.ALIGN_RIGHT)));
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;

                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        private void SendMailtoGroup(string BookingId, string roles, string partyType, int JobNumber, string user, string Paymentmethod, string flowType, string amount = "", string cardno = "", string cardtype = "", string currency = "", string status = "", string mode = "", string movementType = "", string shipperName = "", string consigneeName = "", string payingCountry = "", QuotationEntity quotation = null, PaymentEntity payment = null)
        {
            string AttachmentIds = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            QuotationDBFactory mFactoryRound = new QuotationDBFactory();
            string recipientsList = "";
            try
            {
                recipientsList = mFactory.GetEmailList(roles, BookingId, partyType);
                DataSet documents = mFactory.GetDocuments(JobNumber);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");

                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - " + flowType + " Process" + "( Booking ID #" + BookingId + " )" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                if (roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC" || roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE" && Paymentmethod != "Additional Charges")
                {
                    string DevEmailids = System.Configuration.ConfigurationManager.AppSettings["GlobalPaymentNotifier"];
                    string[] ids = DevEmailids.Split(',');
                    foreach (var item in ids)
                    {
                        message.Bcc.Add(new MailAddress(item));
                    }
                    mapPath = Server.MapPath("~/App_Data/PaymentProcess-Operator-Finance-GSSC.html");
                }
                else
                {
                    mapPath = Server.MapPath("~/App_Data/GSSC-PaymentProcess.html");
                }

                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{Reference_No}", BookingId);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC" || roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE")
                {
                    body = body.Replace("{currency}", currency);
                    body = body.Replace("{Amount}", mFactoryRound.RoundedValue(amount, currency));

                    if (Paymentmethod == "Online")
                    {
                        if (cardtype.ToLowerInvariant() != "alipay")
                        {
                            body = body.Replace("{PaymentProcess}", cardtype + "<br><span style=text-transform: lowercase>xxxxxxxxxxxx" + cardno + "<span>");
                        }
                        else
                        {
                            body = body.Replace("{PaymentProcess}", cardtype);
                        }
                    }
                    else
                    {
                        body = body.Replace("{PaymentProcess}", Paymentmethod);
                        body = body.Replace("{PaymentType}", Paymentmethod);
                    }
                    body = body.Replace("{Status}", status);
                    body = body.Replace("{Mode}", mode);
                    body = body.Replace("{MovementType}", movementType);
                    body = body.Replace("{ShipperOrConsignee}", shipperName + " / " + consigneeName);
                    body = body.Replace("{OriginCountry}", quotation.OCOUNTRYNAME);
                    body = body.Replace("{DestinationCountry}", quotation.DCOUNTRYNAME);
                    body = body.Replace("{PayingCountry}", payingCountry);
                    string placeportconent = string.Empty;
                    string specilaHandlingRequirements = string.Empty;
                    placeportconent=PrapareCityPortData(quotation);                  
                    body = body.Replace("{placeportconent}", placeportconent);
                    specilaHandlingRequirements = PrepareSpecialHandlingInstructions(payment);
                    body = body.Replace("{specilaHandlingRequirements}", specilaHandlingRequirements);
                    if (documents != null)
                    {
                        foreach (DataRow dr in documents.Tables[0].Rows)
                        {
                            string fileName = dr["FILENAME"].ToString();
                            byte[] fileContent = (byte[])dr["FILECONTENT"];
                            message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                            if (AttachmentIds == "")
                                AttachmentIds = dr["DOCID"].ToString();
                            else
                                AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                        }
                    }
                    message.Body = body;
                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                    client.Dispose();
                    message.Dispose();
                    DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
               "JobBooking", "Payment Process", BookingId, user, body, AttachmentIds, false, message.Subject.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public string PrepareSpecialHandlingInstructions(PaymentEntity payment)
        {
            string specilaHandlingRequirements = string.Empty;
            if (payment.LIVEUPLOADS > 0)
            {
                specilaHandlingRequirements = "Live load/unload";
            }
            if (payment.LOADINGDOCKAVAILABLE > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Lift gate required";
            }
            if (payment.CARGOPALLETIZED > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Non-palletized cargo";
            }
            if (payment.ORIGINALDOCUMENTSREQUIRED > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Original documents required";
            }
            if (payment.TEMPERATURECONTROLREQUIRED > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Temperature control required";
            }
            if (payment.COMMERCIALPICKUPLOCATION > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Residential location(s)";
            }
            if (string.IsNullOrEmpty(specilaHandlingRequirements))
            {
                specilaHandlingRequirements = (string.IsNullOrEmpty(payment.SPECIALINSTRUCTIONS) ? specilaHandlingRequirements : payment.SPECIALINSTRUCTIONS);
            }
            else
            {
                specilaHandlingRequirements = (string.IsNullOrEmpty(payment.SPECIALINSTRUCTIONS) ? specilaHandlingRequirements : specilaHandlingRequirements + ", " + payment.SPECIALINSTRUCTIONS);
            }
            if (!string.IsNullOrEmpty(specilaHandlingRequirements))
            {
                specilaHandlingRequirements = specilaHandlingRequirements.TrimStart(',');
                specilaHandlingRequirements = "<tr style=\"vertical-align: top;\"><td colspan=2 style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Special handling requirements" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + specilaHandlingRequirements + "</div></td>";
            }
            return specilaHandlingRequirements;
        }

        public string PrapareCityPortData(QuotationEntity quotation)
        {
            string placeportconent = string.Empty;
            if (quotation.ORIGINPLACENAME != null && quotation.DESTINATIONPLACENAME != null)
            {
                placeportconent = placeportconent + "<tr style=\"vertical-align: top;\"><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Origin City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.ORIGINPLACENAME + "</div></td><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Destination City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.DESTINATIONPLACENAME + "</div></td>";
            }
            else
            {
                if (quotation.ORIGINPLACENAME != null)
                {
                    placeportconent = placeportconent + "<tr style=\"vertical-align: top;\"><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Origin City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.ORIGINPLACENAME + "</div></td>";
                }
                if (quotation.DESTINATIONPLACENAME != null)
                {
                    placeportconent = placeportconent + "<tr><td> <div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.ORIGINPLACENAME + "</div></td><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Destination City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.DESTINATIONPLACENAME + "</div></td>";
                }
            }
            if (quotation.ORIGINPORTNAME != null && quotation.DESTINATIONPORTNAME != null)
            {
                placeportconent = placeportconent + "<tr style=\"vertical-align: top;\"><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Origin Port" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.ORIGINPORTNAME + "</div></td><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Destination Port" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + quotation.DESTINATIONPORTNAME + "</div></td>";
            }
            return placeportconent;
        }



        public void Assignvalues(int jobNumber, int chargeSetID, string ChargeType)
        {
            chargeDescription = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable chargeDescriptionList = null;
            chargeDescriptionList = mFactory.GetChargeDescriptionList(jobNumber, chargeSetID, ChargeType.ToUpper());
            if (chargeDescriptionList.Rows.Count > 0)
            {
                for (int i = 0; i < chargeDescriptionList.Rows.Count; i++)
                {
                    chargeDescription = chargeDescription + "<span style= color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;>" +
                    chargeDescriptionList.Rows[i]["CHARGENAME"] + "</span><br>";
                }
            }
        }

        //Sending mail to customer and GSSC/SSPCOUNTRYJOBOPS on Request cancel job
        [Authorize]
        public JsonResult RequestCancelJob(int JobNumber, string OpJobNumber, string ReasonDescription, string Reason)
        {
            string result = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            try
            {
                result = mFactory.RequestCancelJob(JobNumber, OpJobNumber, ReasonDescription, Reason, HttpContext.User.Identity.Name.ToString());
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                throw new ShipaExceptions("Exception while Cancelling Job " + ex.ToString());
            }
            try
            {
                if (result.ToLower() == "success")
                {
                    try
                    {
                        SendEmailUpdateJobStatus(JobNumber, OpJobNumber, "Cancellation Pending", HttpContext.User.Identity.Name.ToString(), "User");
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending mail in Cancelling Job for user." + ex.ToString());
                    }
                    try
                    {
                        //DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(quotationId));
                        SendEmailUpdateJobStatus(JobNumber, OpJobNumber, "Cancellation Pending", HttpContext.User.Identity.Name.ToString(),"");
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                        throw new ShipaExceptions("Exception while sending mail in Cancelling Job for GSSC/Ops." + ex.ToString());
                    }
                    return Json("sucess", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("fail", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                throw new ShipaExceptions("Exception while sending Cancel Job " + ex.ToString());
            }
        }

        private void SendEmailUpdateJobStatus(int JobNumber, string BookingId, string Status, string User, string type = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            string recipientsList = "";
            try
            {
                MailMessage message = new MailMessage();
                string body = string.Empty;
                string team = string.Empty;
                message.From = new MailAddress("noreply@shipafreight.com");
                if (string.IsNullOrEmpty(type))
                {
                    recipientsList = mFactory.GetEmailList("SSPCOUNTRYFINANCE,SSPGSSC,SSPCOUNTRYCASHIER", BookingId, "BOOKINGCANCEL");
                    if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                    else
                    {
                        string[] bccid = recipientsList.Split(',');
                        foreach (string bccEmailId in bccid)
                        {
                            message.To.Add(new MailAddress(bccEmailId));
                        }
                    }
                }
                else
                {
                    message.To.Add(new MailAddress(User));
                }
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                string mapPath = string.Empty;
                if (string.IsNullOrEmpty(type))
                {
                    mapPath = Server.MapPath("~/App_Data/UpdateJobStatusCancellation_Internal.html");
                    message.Subject = "ShipA Freight - Cancellation Approval Pending  of BookingId " + BookingId + EnvironmentName;
                }
                else
                {
                    mapPath = Server.MapPath("~/App_Data/UpdateJobStatusCancellation.html");
                    message.Subject = "ShipA Freight - Cancellation Requested of BookingId " + BookingId + EnvironmentName;
                }
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(User);
                message.IsBodyHtml = true;
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                if (string.IsNullOrEmpty(type))
                {
                    DBFactory.Entities.QuotationEntity quotation = new QuotationEntity();
                    DBFactory.Entities.PaymentEntity payment = new PaymentEntity();

                    JobandQuoteEntity mJobandQuote = mFactory.GetJobandQuotedetails(JobNumber);

                    string l1 = "User (" + User + ") has made a request to cancel  job booking " + BookingId + ".";
                    string l2 = "Please review the booking details and facilitate the cancellation.";
                    body = body.Replace("{Message1}", l1);
                    body = body.Replace("{Message2}", l2);
                    body = body.Replace("{Message3}", string.Empty);
                    body = body.Replace("{CnlButton}", string.Empty);
                    body = body.Replace("{Mode}", mJobandQuote.PRODUCTNAME);
                    body = body.Replace("{MovementType}", mJobandQuote.MOVEMENTTYPENAME);
                    body = body.Replace("{OriginCountry}", mJobandQuote.OCOUNTRYNAME);
                    body = body.Replace("{DestinationCountry}", mJobandQuote.DCOUNTRYNAME);
                    body = body.Replace("{PayingCountry}", UE.COUNTRYNAME);
                    string specilaHandlingRequirements = string.Empty;
                    string placeportconent = string.Empty;
                    quotation.ORIGINPLACENAME = mJobandQuote.ORIGINPLACENAME;
                    quotation.DESTINATIONPLACENAME = mJobandQuote.DESTINATIONPLACENAME;
                    quotation.ORIGINPORTNAME = mJobandQuote.ORIGINPORTNAME;
                    quotation.DESTINATIONPORTNAME = mJobandQuote.DESTINATIONPORTNAME;
                    quotation.OCOUNTRYNAME = mJobandQuote.OCOUNTRYNAME;
                    quotation.DCOUNTRYNAME = mJobandQuote.DCOUNTRYNAME;
                    payment.LIVEUPLOADS = mJobandQuote.LIVEUPLOADS;
                    payment.LOADINGDOCKAVAILABLE = mJobandQuote.LOADINGDOCKAVAILABLE;
                    payment.CARGOPALLETIZED = mJobandQuote.CARGOPALLETIZED;
                    payment.ORIGINALDOCUMENTSREQUIRED = mJobandQuote.ORIGINALDOCUMENTSREQUIRED;
                    payment.TEMPERATURECONTROLREQUIRED = mJobandQuote.TEMPERATURECONTROLREQUIRED;
                    payment.COMMERCIALPICKUPLOCATION = mJobandQuote.COMMERCIALPICKUPLOCATION;
                    payment.SPECIALINSTRUCTIONS = mJobandQuote.SPECIALINSTRUCTIONS;
                    placeportconent = PrapareCityPortData(quotation);                  
                    body = body.Replace("{placeportconent}", placeportconent);
                    specilaHandlingRequirements = PrepareSpecialHandlingInstructions(payment);
                    body = body.Replace("{specilaHandlingRequirements}", specilaHandlingRequirements);
                }
                else
                {
                    string resetLink = string.Empty;
                    int id = mFactory.getQuotationId(JobNumber);
                    string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                    string url = "JobNumber=" + JobNumber + "?id=" + id;
                    JsonResult t1 = EncryptId(url);
                    string rs = t1.Data.ToString();
                    resetLink = Actionlink + "JobBooking/BookingSummary?" + rs;
                    string CnlButton = "<td style=font-family: 'Open Sans', sans-serif; background-color: #f29221; padding: 10px; text-align: center; color: #FFF;><a href=" + resetLink + " style=height: 50px; width: 100px; text-align: center; color: #FFF; background-color: #f29221; text-decoration: none; text-transform: uppercase>" + Status.ToUpper() + "</a></td>";

                    string l1 = "We’ve received your request to cancel job booking " + BookingId + " . Our team is reviewing the current status of your shipment and assessing the necessary arrangements to cancel the job.";
                    string l2 = "We’ll send you a confirmation once we have cancelled the job and issued a refund for the quote amount.";
                    string l3 = "Click on the link below to monitor the status of your cancellation through the ShipA Freight portal.";
                    body = body.Replace("{Message1}", l1);
                    body = body.Replace("{Message2}", l2);
                    body = body.Replace("{Message3}", l3);
                    body = body.Replace("{CnlButton}", CnlButton);
                }
                team = (string.IsNullOrEmpty(type)) ? "Team" : UE.FIRSTNAME;
                body = body.Replace("{Team}", team);
                body = body.Replace("{Reference_No}", BookingId);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                if (string.IsNullOrEmpty(type))
                {
                    DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
               "JobBooking", "Job Cancellation", BookingId, User, body, "", false, message.Subject.ToString());
                }
                else
                {
                    DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                  "JobBooking", "Job Cancellation", BookingId, User, body, "", true, message.Subject.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        //Focis on status change and comments.
        public void AssignComments(string Comments)
        {
            chargeDescription = string.Empty;
            string[] rComments = Comments.Split(',');
            foreach (string comments1 in rComments)
            {
                chargeDescription = chargeDescription + "<span style= color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;>" + comments1 + "</span><br>";
            }
        }
        public void FocisSendEmailNotificationUpdateJobStatus(string BookingId, string Comments, string Roles, string CountryId)
        {
            if (!string.IsNullOrEmpty(Comments))
            {
                AssignComments(Comments);
            }
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");

                recipientsList = mFactory.GetEmailList(Roles, BookingId, "", CountryId);

                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - Job status and comments of BookingId " + BookingId + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = Server.MapPath("~/App_Data/FocisSendEmailNotificationUpdateJobStatus.html");

                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                body = body.Replace("{Reference_No}", BookingId);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                body = body.Replace("{CommentList}", chargeDescription);

                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public void BookingCancellation(string JobNumber, string id, string Type, string Source)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            string l1 = string.Empty;
            string l2 = string.Empty;
            string l3 = string.Empty;
            string AttachmentIds = string.Empty;
            string cancellationReceiptNo = string.Empty;
            try
            {
                DBFactory.Entities.RefundPaymentEntity mDBEntity = mFactory.GetBookingCancellationDetails(id);
                RefundPaymentModel mUIModel = Mapper.Map<DBFactory.Entities.RefundPaymentEntity, RefundPaymentModel>(mDBEntity);
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.IsBodyHtml = true;
                string body = string.Empty;
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(mUIModel.CREATEDBY);
                if (Type.ToLower() == "reject")
                {
                    message.To.Add(new MailAddress(mUIModel.CREATEDBY));
                    message.Subject = "Shipa Freight - Job status and comments of BookingId (" + mUIModel.OPJOBNUMBER + ") has been REJECTED." + EnvironmentName;
                    string mapPath = Server.MapPath("~/App_Data/UpdateJobStatusCancellation.html");
                    using (StreamReader reader = new StreamReader(mapPath))
                    { body = reader.ReadToEnd(); }

                    l1 = "Your cancellation request of booking " + mUIModel.OPJOBNUMBER + " has been rejected.";
                    l2 = "Rejection comments - " + mUIModel.REJCOMMENTS;
                    l3 = "Please contact Shipa freight helpdesk for further information.";
                    body = body.Replace("{Message1}", l1);
                    body = body.Replace("{Message2}", l2);
                    body = body.Replace("{Message3}", l3);
                    body = body.Replace("{Message3}", string.Empty);
                    body = body.Replace("{CnlButton}", string.Empty);
                    body = body.Replace("{Team}", UE.FIRSTNAME);
                }
                else if (Type.ToLower() == "approve")
                {
                    if (Source.ToLower() == "gssc")
                    {
                        string mapPath = Server.MapPath("~/App_Data/UpdateJobStatusCancellation_Internal.html");
                        DBFactory.Entities.QuotationEntity quotation = new QuotationEntity();
                        DBFactory.Entities.PaymentEntity payment = new PaymentEntity();
                        JobandQuoteEntity mJobandQuote = mFactory.GetJobandQuotedetails(Convert.ToInt32(JobNumber));
                        using (StreamReader reader = new StreamReader(mapPath))
                        { body = reader.ReadToEnd(); }
                        if (mUIModel.TRANSACTIONMODE.ToLower() == "cash")
                        {
                            recipientsList = mFactory.GetEmailList("SSPCOUNTRYCASHIER", "", "BOOKINGCANCEL", "", mUIModel.CREATEDBY);
                        }
                        else
                        {
                            recipientsList = mFactory.GetEmailList("SSPCOUNTRYFINANCE", "", "BOOKINGCANCEL", "", mUIModel.CREATEDBY);
                        }

                        if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                        else
                        {
                            string[] bccid = recipientsList.Split(',');
                            foreach (string bccEmailId in bccid)
                            {
                                message.To.Add(new MailAddress(bccEmailId));
                            }
                        }

                        message.Subject = "Shipa Freight - Cancellation Approval Pending for " + mUIModel.PAYMENTOPTION + " payment." + EnvironmentName;
                        if (mUIModel.PAYMENTOPTION.ToLower() == "online")
                        {
                            l1 = "Booking " + mUIModel.OPJOBNUMBER + " has been cancelled. A refund needs to be issued to the customer for amount " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + ".";
                            l2 = "Please review the job details in FOCiS and arrange for a online payment reversal through payment gateway.";
                        }
                        else if (mUIModel.PAYMENTOPTION.ToLower() == "offline")
                        {
                            if (mUIModel.PAYMENTTYPE.ToLower() == "wire transfer")
                            {
                                l1 = "Booking " + mUIModel.OPJOBNUMBER + " has been cancelled. A refund needs to be issued to the customer in the form of a wire transfer for amount " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + ".";
                                l2 = "Please review the job details in FOCiS and arrange for a wire transfer.";
                            }
                            else
                            {
                                l1 = "Booking " + mUIModel.OPJOBNUMBER + " has been cancelled. A refund needs to be issued to the customer in the form of a Cash/Cheque/Demand Draft for amount " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + ".";
                                l2 = "Please review the job details in FOCiS and arrange for offline payment(Pay at branch).";
                            }
                        }
                        else if (mUIModel.PAYMENTOPTION.ToLower() == "business credit")
                        {
                            l1 = "Booking " + mUIModel.OPJOBNUMBER + " has been cancelled. A refund needs to be issued to the customer in the form of a business credit for amount " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + ".";
                            l2 = "Please review the job details in FOCiS, post approval business credit will be automatically adjusted.";
                        }
                        body = body.Replace("{Message3}", string.Empty);
                        body = body.Replace("{Team}", "Team");
                        body = body.Replace("{Mode}", mJobandQuote.PRODUCTNAME);
                        body = body.Replace("{MovementType}", mJobandQuote.MOVEMENTTYPENAME);
                        body = body.Replace("{OriginCountry}", mJobandQuote.OCOUNTRYNAME);
                        body = body.Replace("{DestinationCountry}", mJobandQuote.DCOUNTRYNAME);
                        body = body.Replace("{PayingCountry}", UE.COUNTRYNAME);
                        string specilaHandlingRequirements = string.Empty;
                        string placeportconent = string.Empty;
                        quotation.ORIGINPLACENAME = mJobandQuote.ORIGINPLACENAME;
                        quotation.DESTINATIONPLACENAME = mJobandQuote.DESTINATIONPLACENAME;
                        quotation.ORIGINPORTNAME = mJobandQuote.ORIGINPORTNAME;
                        quotation.DESTINATIONPORTNAME = mJobandQuote.DESTINATIONPORTNAME;
                        quotation.OCOUNTRYNAME = mJobandQuote.OCOUNTRYNAME;
                        quotation.DCOUNTRYNAME = mJobandQuote.DCOUNTRYNAME;
                        payment.LIVEUPLOADS = mJobandQuote.LIVEUPLOADS;
                        payment.LOADINGDOCKAVAILABLE = mJobandQuote.LOADINGDOCKAVAILABLE;
                        payment.CARGOPALLETIZED = mJobandQuote.CARGOPALLETIZED;
                        payment.ORIGINALDOCUMENTSREQUIRED = mJobandQuote.ORIGINALDOCUMENTSREQUIRED;
                        payment.TEMPERATURECONTROLREQUIRED = mJobandQuote.TEMPERATURECONTROLREQUIRED;
                        payment.COMMERCIALPICKUPLOCATION = mJobandQuote.COMMERCIALPICKUPLOCATION;
                        payment.SPECIALINSTRUCTIONS = mJobandQuote.SPECIALINSTRUCTIONS;
                        placeportconent = PrapareCityPortData(quotation);
                        body = body.Replace("{placeportconent}", placeportconent);
                        specilaHandlingRequirements = PrepareSpecialHandlingInstructions(payment);
                        body = body.Replace("{specilaHandlingRequirements}", specilaHandlingRequirements);
                    }
                    else if (Source.ToLower() == "countrylevel")
                    {
                        try
                        {
                            cancellationReceiptNo = GenerateCancellationReceiptPDF(Convert.ToInt32(JobNumber), "Cancellation Receipt", mUIModel.QUOTATIONID, mUIModel.OPJOBNUMBER, mUIModel.CREATEDBY);
                        }
                        catch (Exception ex)
                        {
                            //Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                            //throw new Exception("Exception while generating Cancellation Receipt." + ex.ToString());
                        }

                        message.To.Add(new MailAddress(mUIModel.CREATEDBY));
                        string mapPath = Server.MapPath("~/App_Data/UpdateJobStatusCancellation.html");
                        using (StreamReader reader = new StreamReader(mapPath))
                        { body = reader.ReadToEnd(); }
                        body = body.Replace("{Team}", UE.FIRSTNAME);
                        message.Subject = "Shipa Freight - Cancellation Approved for " + mUIModel.PAYMENTOPTION + " payment." + EnvironmentName;
                        if (mUIModel.PAYMENTOPTION.ToLower() == "online")
                        {
                            l2 = "We’ve refunded " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + " to the credit card you used for your payment. It may take 5-7 business days for the amount to appear in your account.";
                        }
                        else if (mUIModel.PAYMENTOPTION.ToLower() == "offline")
                        {
                            if (mUIModel.PAYMENTTYPE.ToLower() == "wire transfer")
                            {
                                l2 = "We’ve initiated a refund of " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + " to your bank account. It may take 5-7 business days for the amount to appear in your account.";
                            }
                            else
                            {
                                l2 = "We have arranged to provide a pay at branch option(Cash/Cheque/Demand Draft) refund of  " + mUIModel.CURRENCY + " " + mUIModel.MAILRECEIPTNETAMOUNT + " which can be collected at your branch, please review branch details below and contact our team to arrange for collection.";
                                l3 = mUIModel.ENTITYID + ", " + mUIModel.ENTITYDETAILS;
                            }
                        }
                        else if (mUIModel.PAYMENTOPTION.ToLower() == "business credit")
                        {
                            l2 = "Post approval business credit will be automatically adjusted.";
                        }
                        if (string.IsNullOrEmpty(cancellationReceiptNo))
                        {
                            l1 = "Your job booking " + mUIModel.OPJOBNUMBER + " has been cancelled successfully.";
                        }
                        else
                        {
                            l1 = "Your job booking " + mUIModel.OPJOBNUMBER + " has been cancelled successfully. Cancellation receipt No. " + cancellationReceiptNo;
                        }
                        body = body.Replace("{Message3}", l3);

                        DataSet documents = mFactory.GetDocuments(Convert.ToInt32(JobNumber));
                        if (documents != null)
                        {
                            foreach (DataRow dr in documents.Tables[0].Rows)
                            {
                                if (dr["DOCUMENTNAME"].ToString().ToLower() == "cancellation receipt")
                                {
                                    string fileName = dr["FILENAME"].ToString();
                                    byte[] fileContent = (byte[])dr["FILECONTENT"];
                                    message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                                    if (AttachmentIds == "")
                                        AttachmentIds = dr["DOCID"].ToString();
                                    else
                                        AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                                }
                            }
                        }
                    }
                }
                body = body.Replace("{CnlButton}", string.Empty);
                body = body.Replace("{Message1}", l1);
                body = body.Replace("{Message2}", l2);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;

                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public JsonResult OnlineRefund(string JobNumber)
        {
            var d = Request.Headers.Get("X_Source");
            if (d == null)
            {
                var headerdmissing = new
                {
                    StatusCode = 500,
                    Data = "not authorized"
                };
                return Json(headerdmissing, JsonRequestBehavior.AllowGet);
            }
            else if (d.Contains("focis"))
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                var resp = new List<OnlineRefundEntity>();
                var err = new List<OnlineRefundEntity>();
                var data = mFactory.GetOnlineTransactionDetails(JobNumber);
                var list = data.Where(o => o.STATUS == "succeeded" && o.REFUNDID == null && o.STRIPEACCOUNT != "HSBC");
                foreach (var item in list)
                {
                    StripeData stripeData = GetStripeAccount(item.STRIPEACCOUNT);
                    StripeConfiguration.SetApiKey(stripeData.secretKey);
                    int refundAmount = Convert.ToInt32(Convert.ToDecimal(item.AMOUNT) * 100);
                    var refundOptions = new StripeRefundCreateOptions()
                    {
                        Amount = refundAmount,
                        Reason = StripeRefundReasons.RequestedByCustomer
                    };
                    var refundService = new StripeRefundService();
                    try
                    {
                        StripeRefund refund = refundService.Create(item.CHARGEID, refundOptions);

                        resp.Add(new OnlineRefundEntity
                        {
                            Id = item.TRANSATIONID,
                            JobNumber = JobNumber,
                            Currency = refund.Currency,
                            RefundAmount = (Convert.ToDecimal(refundAmount) / 100).ToString(),
                            RefundRefNumber = refund.Id,
                            RefundStatus = refund.Status
                        });
                    }
                    catch (StripeException ex)
                    {
                        err.Add(new OnlineRefundEntity
                        {
                            Id = item.TRANSATIONID,
                            JobNumber = JobNumber,
                            Currency = item.CURRENCY,
                            RefundAmount = (Convert.ToDecimal(refundAmount) / 100).ToString(),
                            RefundStatus = "failed"
                        });

                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", "While processing refund of online Payment", DateTime.Now.ToString()));
                        Log.ErrorFormat(String.Format("Error Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                        Log.ErrorFormat(String.Format("StackTrace Message: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                    }
                }

                // HSBC Refunds
                #region HSBC Refunds
                var list2 = data.Where(o => o.STATUS == "succeeded" && o.REFUNDID == null && o.STRIPEACCOUNT == "HSBC");
                if (list2.Any())
                {
                    foreach (var item in list2)
                    {
                        var ran = new Random();
                        var dd = DateTime.Now.ToString("yymmdd") + DateTime.Now.ToString("hhmmss") + ran.Next(9999);
                        var amount = Convert.ToInt32(Convert.ToDouble(item.PAYOUTTOTAL) * 100).ToString();
                        var dto = new HsbcRefundDto()
                        {
                            orderCurr = "CNY",
                            orderNo = ConfigurationManager.AppSettings["MerchantID"].ToString() + dd,
                            orderAmt = amount,
                            merId = ConfigurationManager.AppSettings["MerchantID"].ToString(),
                            merIp = ConfigurationManager.AppSettings["MerchantIP"].ToString(),
                            orderTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                            transType = "02",
                            notifyUrl = ConfigurationManager.AppSettings["APPURL"] + "api/Payment/HsbcRefundResponse",
                            orgOrderNo = item.TOKENID,
                            orgOrderDate = item.CREATEDDATE.ToString("yyyyMMddHHmmss"),
                            orgAmt = amount
                        };

                        string strOrder = JsonConvert.SerializeObject(dto);
                        string postData = HsbcEncryption.signAndEncrypt(strOrder);
                        HSBCResponseDto res = sendRequest(postData, string.Empty, "merchant/payRefund");

                        var status = string.Empty;
                        if (res.proCode == "000000")
                        {
                            status = "succeeded";
                        }
                        else if (res.proCode == "111111")
                        {
                            status = "pending";
                        }

                        if (status != string.Empty)
                        {
                            resp.Add(new OnlineRefundEntity
                            {
                                Id = item.TRANSATIONID,
                                JobNumber = JobNumber,
                                Currency = "CNY",
                                RefundAmount = (Convert.ToDecimal(amount) / 100).ToString(),
                                RefundRefNumber = dto.orderNo,
                                RefundStatus = status
                            });
                        }
                        else
                        {
                            err.Add(new OnlineRefundEntity
                            {
                                Id = item.TRANSATIONID,
                                JobNumber = JobNumber,
                                Currency = item.CURRENCY,
                                RefundAmount = (Convert.ToDecimal(amount) / 100).ToString(),
                                RefundStatus = "failed"
                            });
                        }
                    }
                }
                #endregion HSBC Refunds

                if (resp.Any())
                {
                    mFactory.UpdateRefundStatus(resp);
                }

                var code = 200;
                if (err.Any())
                {
                    code = 417;
                }
                else if (list.Any() || list2.Any())
                {
                    code = 200;
                }
                else
                {
                    code = 404;
                }

                var jsontoreturn = new
                {
                    StatusCode = code
                };
                return Json(jsontoreturn, JsonRequestBehavior.AllowGet);
            }
            var errresp = new
            {
                StatusCode = 500,
                Data = "invalid auth header"
            };
            return Json(errresp, JsonRequestBehavior.AllowGet);
        }

        //Cancellation Receipt
        public string GenerateCancellationReceiptPDF(int jobnumber, string ReportName, Int64 QuotaionID, string OperationJobNumber, string CreatedBy)
        {
            z = 0;
            if (mUserModel == null)
            {
                UserDBFactory mFactory1 = new UserDBFactory();
                mUserModel = mFactory1.GetUserProfileDetails(CreatedBy, 1);
            }
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            int count = mQuoFactory.GetQuotationIdCount(Convert.ToInt32(QuotaionID));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(QuotaionID));
                mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
            }
            MemoryStream memoryStream = new MemoryStream();
            string discountvalue = string.Empty;
            string SumAdditionalCharges = string.Empty;
            string TCount = string.Empty;
            DataTable AdvReceiptChargeDtls = null;
            PDFpage = 0;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();

            DBFactory.Entities.AdvanceReceiptEntity AdvReceiptDtls = mFactory.GetAdvanceReceiptDtls(Convert.ToInt32(jobnumber));

            discountvalue = mFactory.GETDiscounts(Convert.ToInt32(jobnumber));
            AdvReceiptChargeDtls = mFactory.GetCancellationReceiptDtls(jobnumber);

            DataTable UserDtls = mFactory.GetUserDtls(jobnumber);

            var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.CurrencySymbol = "";

            DataTable VATDtls = mFactory.GetVATDtls(jobnumber);
            string origintotalprice = string.Empty;
            string desttotalprice = string.Empty;
            string addtotalprice = string.Empty;
            string distotalprice = string.Empty;
            string intfrgttotalprice = string.Empty;

            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(AdvReceiptDtls.PREFERREDCURRENCYID);
            double totalamount = 0;
            string Total = string.Empty;
            double decorigintotalprice = 0, decintfrgttotalprice = 0, decdesttotalprice = 0, decaddtotalprice = 0, decdiscount = 0, deSumAdditionalCharges = 0;

            foreach (DataRow row in AdvReceiptChargeDtls.Rows)
            {
                string ROUTETYPEID = row["ROUTETYPEID"].ToString();
                string TOTALPRICE = row["TOTALPRICE"].ToString();

                if (ROUTETYPEID == "1118")//"Origin"
                {
                    origintotalprice = TOTALPRICE;//
                }
                if (ROUTETYPEID == "1116")//"international freight"
                {
                    intfrgttotalprice = TOTALPRICE;
                }
                if (ROUTETYPEID == "1117")//"Destination"
                {
                    desttotalprice = TOTALPRICE;
                }
                if (ROUTETYPEID == "250001")//"Optional"
                {
                    addtotalprice = TOTALPRICE;
                }
                if ((ROUTETYPEID != "250001") && (ROUTETYPEID != "1118") && (ROUTETYPEID != "1117") && (ROUTETYPEID != "1116"))//"Additional"
                {
                    SumAdditionalCharges = TOTALPRICE;
                }
                distotalprice = discountvalue;
            }

            Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();

            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = "Cancellation Receipt";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);

            string VATRegNo = string.Empty;
            if (VATDtls.Rows.Count > 0)
            {
                VATRegNo = VATDtls.Rows[0]["VATREGNUMBER"].ToString();
            }

            string ReceivedFrom = string.Empty;
            if (UserDtls.Rows.Count > 0)
            {
                ReceivedFrom = UserDtls.Rows[0]["FIRSTNAME"].ToString() + "," + UserDtls.Rows[0]["FULLADDRESS"].ToString();
            }

            Font boldFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
            Font normalFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new float[] { 5f, 47.5f, 47.5f });
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 100;
            table.SpacingBefore = 10;

            PdfPCell verticalcell = new PdfPCell(new Phrase("", new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL)));//if needed replace empty string with desired string to show vertical message in document.
            verticalcell.Rotation = 90;
            verticalcell.HorizontalAlignment = Element.ALIGN_CENTER;
            verticalcell.VerticalAlignment = Element.ALIGN_TOP;
            verticalcell.Border = 0;
            verticalcell.Rowspan = 6;
            table.AddCell(verticalcell);

            Phrase phrase = new Phrase();
            phrase.Add(
                new Chunk("Received From :", boldFont)
            );
            phrase.Add(Chunk.NEWLINE);
            if (ReceivedFrom != string.Empty)
            {
                for (int j = 0; j < ReceivedFrom.Split(',').Length; j++)
                {
                    if (ReceivedFrom.Split(',')[j].Trim() != string.Empty)
                    {
                        phrase.Add(new Chunk(ReceivedFrom.Split(',')[j].Trim(), normalFont));
                        phrase.Add(Chunk.NEWLINE);
                    }
                }
            }
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(
                new Chunk("Customer VAT Registered No :", boldFont)
            );
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk(VATRegNo, normalFont));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(Chunk.NEWLINE);


            PdfPCell cell1 = new PdfPCell();
            cell1.PaddingLeft = 10;
            cell1.PaddingTop = 10;
            cell1.Colspan = 1;
            cell1.AddElement(phrase);

            table.AddCell(cell1);

            Phrase phrase1 = new Phrase();
            phrase1.Add(
                new Chunk("Cancellation Receipt No", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);

            phrase1.Add(new Chunk(": CAN-" + AdvReceiptDtls.RECEIPTNUMBER, normalFont));

            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Date of Issue", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            phrase1.Add(new Chunk(": " + DateTime.Today.ToString("dd-MM-yyyy"), normalFont));

            PdfPCell cell2 = new PdfPCell();
            cell2.PaddingLeft = 10;
            cell2.PaddingTop = 10;
            cell2.Colspan = 1;
            cell2.AddElement(phrase1);
            table.AddCell(cell2);

            Phrase bookingphrase = new Phrase();
            bookingphrase.Add(
                new Chunk("Booking ID :", boldFont)
            );
            bookingphrase.Add(Chunk.SPACETABBING);
            bookingphrase.Add(new Chunk(AdvReceiptDtls.OPERATIONALJOBNUMBER, normalFont));
            bookingphrase.Add(Chunk.NEWLINE);
            bookingphrase.Add(Chunk.NEWLINE);

            PdfPCell bookingcell = new PdfPCell();
            bookingcell.PaddingLeft = 10;
            bookingcell.PaddingTop = 10;
            bookingcell.AddElement(bookingphrase);
            bookingcell.Colspan = 2;
            table.AddCell(bookingcell);

            Phrase chargedescphrase = new Phrase();
            chargedescphrase.Add(
                new Chunk("Charges Description", boldFont)
            );
            chargedescphrase.Add(Chunk.NEWLINE);
            chargedescphrase.Add(Chunk.NEWLINE);

            PdfPCell chargedesccell = new PdfPCell();
            chargedesccell.PaddingLeft = 10;
            chargedesccell.PaddingTop = 10;
            chargedesccell.Colspan = 1;
            chargedesccell.AddElement(chargedescphrase);
            table.AddCell(chargedesccell);

            Phrase amountphrase = new Phrase();
            amountphrase.Add(new Chunk(new VerticalPositionMark()));
            amountphrase.Add(
                new Chunk("Amount", boldFont)
                );
            amountphrase.Add(Chunk.NEWLINE);
            amountphrase.Add(Chunk.NEWLINE);

            PdfPCell amountcell = new PdfPCell();
            amountcell.PaddingRight = 10;
            amountcell.PaddingTop = 10;
            amountcell.Colspan = 1;
            amountcell.AddElement(amountphrase);
            table.AddCell(amountcell);
            Phrase chargesphrase = new Phrase();
            if (origintotalprice != "0" && origintotalprice != "")
            {
                chargesphrase.Add(
                    new Chunk("Origin Charges", normalFont)
                    );

                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);
            }
            if (intfrgttotalprice != "0" && intfrgttotalprice != "")
            {
                chargesphrase.Add(
                    new Chunk("International Freight Charges", normalFont)
                    );

                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);
            }
            if (desttotalprice != "0" && desttotalprice != "")
            {
                chargesphrase.Add(
                    new Chunk("Destination Charges", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);
            }
            if (addtotalprice != "0" && addtotalprice != "")
            {
                chargesphrase.Add(
                       new Chunk("Optional Charges", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);
            }
            if (SumAdditionalCharges != "0" && SumAdditionalCharges != "")
            {
                chargesphrase.Add(
                   new Chunk("Additional Charges", normalFont)
                   );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);
            }
            if (distotalprice != "0")
            {
                chargesphrase.Add(
                    new Chunk("Discount", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);
            }
            if (origintotalprice != string.Empty)
                decorigintotalprice = Double.Parse(origintotalprice);
            if (intfrgttotalprice != string.Empty)
                decintfrgttotalprice = Double.Parse(intfrgttotalprice);
            if (desttotalprice != string.Empty)
                decdesttotalprice = Double.Parse(desttotalprice);
            if (addtotalprice != string.Empty)
                decaddtotalprice = Double.Parse(addtotalprice);
            if (SumAdditionalCharges != string.Empty)
                deSumAdditionalCharges = Double.Parse(SumAdditionalCharges);

            totalamount = decorigintotalprice + decintfrgttotalprice + decdesttotalprice + decaddtotalprice - decdiscount + deSumAdditionalCharges;
            Total = Double.Parse(totalamount.ToString()).ToString("N" + decimalCount);

            PdfPCell chargescell = new PdfPCell();
            chargescell.PaddingLeft = 10;
            chargescell.PaddingTop = 10;
            chargescell.Colspan = 1;
            chargescell.AddElement(chargesphrase);
            table.AddCell(chargescell);

            Phrase amountsphrase = new Phrase();
            if (decorigintotalprice != 0)
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                             new Chunk(decorigintotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                        );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
            }
            if (decintfrgttotalprice != 0)
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                             new Chunk(decintfrgttotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                        );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
            }
            if (decdesttotalprice != 0)
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                              new Chunk(decdesttotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                            );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
            }
            if (decaddtotalprice != 0)
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                              new Chunk(decaddtotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
            }
            if (distotalprice != "0")
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                                   new Chunk(distotalprice + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
            }
            if (SumAdditionalCharges != "0" && SumAdditionalCharges != "")
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                              new Chunk(Double.Parse(SumAdditionalCharges).ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
            }

            PdfPCell amountscell = new PdfPCell();
            amountscell.PaddingRight = 10;
            amountscell.PaddingTop = 10;
            amountcell.Colspan = 1;
            amountscell.AddElement(amountsphrase);
            table.AddCell(amountscell);

            Phrase totalphrase = new Phrase();
            totalphrase.Add(
                new Chunk("Total", boldFont)
            );
            totalphrase.Add(Chunk.NEWLINE);
            totalphrase.Add(Chunk.NEWLINE);

            PdfPCell totalcell = new PdfPCell();
            totalcell.PaddingLeft = 10;
            totalcell.PaddingTop = 10;
            totalcell.Colspan = 1;
            totalcell.AddElement(totalphrase);
            table.AddCell(totalcell);

            Phrase totalamountphrase = new Phrase();
            totalamountphrase.Add(new Chunk(new VerticalPositionMark()));
            totalamountphrase.Add(
                new Chunk(Total + " " + AdvReceiptDtls.PREFERREDCURRENCYID, boldFont)
                );
            totalamountphrase.Add(Chunk.NEWLINE);
            totalamountphrase.Add(Chunk.NEWLINE);
            PdfPCell totalamountcell = new PdfPCell();
            totalamountcell.PaddingRight = 10;
            totalamountcell.PaddingTop = 10;
            totalamountcell.Colspan = 1;
            totalamountcell.AddElement(totalamountphrase);
            table.AddCell(totalamountcell);

            doc.Add(table);

            PdfPTable note = new PdfPTable(2);
            note.SetWidths(new float[] { 5f, 95f });
            note.WidthPercentage = 100;
            note.SpacingBefore = 10;
            note.DefaultCell.Border = 0;
            note.AddCell(" ");
            note.AddCell(new PdfPCell(mFormattingpdf.TableContentCell("IMPORTANT: All business handled by Agility is subject to Agility's trading terms and conditions, which contain limitations of liability." +
            "Copies of these applicable terms and conditions are available upon request.", Element.ALIGN_LEFT)));

            doc.Add(note);

            doc.Close();

            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }

            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
            sspdoc.QUOTATIONID = Convert.ToInt64(QuotaionID);
            sspdoc.DOCUMNETTYPE = "ShipAFreightDoc";

            sspdoc.FILENAME = "CancellationReceipt_" + OperationJobNumber.ToString() + ".pdf";

            sspdoc.FILEEXTENSION = "application/pdf";
            sspdoc.FILESIZE = bytes.Length;
            sspdoc.FILECONTENT = bytes;
            sspdoc.JOBNUMBER = Convert.ToInt64(jobnumber);
            sspdoc.DocumentName = ReportName;
            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
            JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

            Guid obj = Guid.NewGuid();
            string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();

            mjobFactory.SaveDocuments(docentity, Convert.ToString(CreatedBy), "System Generated", obj.ToString(), EnvironmentName);
            return "CAN-" + AdvReceiptDtls.RECEIPTNUMBER;


        }

        public JsonResult TradeFinance(string TradeType, string BookingId, string QuotationNo, string BookingNo)
        {
            string Emailids = "";
            string name = HttpContext.User.Identity.Name;
            int bookingid = Convert.ToInt32(BookingId);
            int quotationid = Convert.ToInt32(QuotationNo);
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            Emailids = System.Configuration.ConfigurationManager.AppSettings["TradeFinanceEmail"];
            string[] ids = Emailids.Split(',');
            foreach (var item in ids)
            {
                message.To.Add(new MailAddress(item));
            }
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Trade Finance" + "( Booking ID #" + BookingNo + " )" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/TradeFinance.html");

            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{Message}", "Booking Id:" + BookingNo + "<p> customer interested on trade finance for </p>" + TradeType);
            body = body.Replace("{user}", "");
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();

            try
            {
                string base64 = AESEncrytDecry.EncryptStringAES(body);
                byte[] bytes = System.Convert.FromBase64String(Convert.ToString(base64));
                JobBookingDbFactory JBD = new JobBookingDbFactory();
                JBD.TradeFinanceSave(name, "YES", TradeType, bytes, "noreply@shipafreight.com", "AlGupta@agility.com", bookingid, quotationid);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
            return Json("success", JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult Getdocumentslist(string jobnumber, string quotationid)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string userid = HttpContext.User.Identity.Name.ToString();

            DataTable dt = mFactory.Getdocumentnames(jobnumber, quotationid, userid);
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                var JsonToReturn = new
                {
                    rows = trows,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult CollabrateSave(string DOCID, string DocupDate, string ProvidedBy, string Provideremailid, string jobnumber, string quoteno, string cargoavailable, string ShipperName, string ConName, string DocTypeNames, string notetoshipper, string ChinnaCheck)
        {
            try
            {
                string Name = "";
                string Con = "";
                string[] names;
                string docnames = string.Empty;
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(HttpContext.User.Identity.Name);
                if (!string.IsNullOrEmpty(ShipperName))
                {
                    string[] ShName = ShipperName.Split(' ');
                    Name = ShName[0];
                }
                if (!string.IsNullOrEmpty(ConName))
                {
                    string[] ConnName = ConName.Split(' ');
                    Con = ConnName[0];
                }
                if (!string.IsNullOrEmpty(DocTypeNames))
                {

                    if (DocTypeNames.Length > 1)
                    {
                        names = DocTypeNames.Split(',');
                        foreach (var item in names)
                        {
                            docnames = docnames + "<ul><li>" + item + "</li></ul>";
                        }
                    }
                    else
                    {
                        docnames = DocTypeNames;
                    }
                }
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.UpdateCollabratedocument(DOCID, DocupDate, ProvidedBy, Provideremailid);
                //string Jobnumber = AESEncrytDecry.EncryptStringAES(jobnumber);
                //string Quoteid = AESEncrytDecry.EncryptStringAES(quoteno);
                //string Providermailid = AESEncrytDecry.EncryptStringAES(Provideremailid);

                string resetLink = string.Empty;
                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                string url = "jobnumber=" + jobnumber + "?quoteno=" + quoteno + "?Provideremailid=" + Provideremailid + "?CargoAvailable=" + cargoavailable + "?Con=" + Con + "?Name=" + Name;
                JsonResult t1 = EncryptId(url);
                string rs = t1.Data.ToString();
                resetLink = Actionlink + "Documents/OpendocumentsNew" + rs;

                //string resetLink = Url.Action("Opendocuments", "Documents", new { jobnumber = Jobnumber, quoteno = Quoteid, Provideremailid = Providermailid, CargoAvailable = cargoavailable, Con = Con, Name = Name }, "http");
                //resetLink = Actionlink + "Documents/Opendocuments?" + resetLink.Split('?')[1];
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(Provideremailid));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Action Requested: Upload Documents for Upcoming Shipment" + EnvironmentName; ;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                mapPath = Server.MapPath("~/App_Data/Collabate.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{user}", ((!string.IsNullOrEmpty(Name)) ? Name : string.Empty));
                body = body.Replace("{user_created}", UE.FIRSTNAME + " " + UE.LASTNAME + " (" + HttpContext.User.Identity.Name + ") ");
                body = body.Replace("{url}", resetLink);
                body = body.Replace("{docnames}", docnames);
                body = body.Replace("{Identity}", notetoshipper);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        private void SendThankingMailtoCustomer(string fromEmail, string toEmail, string bookingId)
        {
            try
            {
                MailMessage message = new MailMessage();
                UserDBFactory UD = new UserDBFactory();
                string firstName = string.Empty;
                var UE = UD.GetUserFirstAndLastName(toEmail);
                firstName = UE.FIRSTNAME;
                if (!string.IsNullOrEmpty(fromEmail))
                {
                    message.From = new MailAddress(fromEmail); //CE 
                }
                else
                {
                    message.From = new MailAddress("prem@shipafreight.com"); //CE 
                }
                message.To.Add(toEmail);
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight_Thanks For Payment" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                mapPath = Server.MapPath("~/App_Data/Customer-Thanking.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                body = body.Replace("{user_name}", firstName);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication(fromEmail, toEmail, "", "",
           "JobBooking", "Payment Process-Thanking mail to customer", bookingId, toEmail, body, "", true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public void FocisBookingCancellation(string BookingId, string User)
        {
            try
            {
                MailMessage message = new MailMessage();
                string body = string.Empty;
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(User));

                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "ShipA Freight - Cancellation of BookingId " + BookingId + EnvironmentName;
                message.IsBodyHtml = true;
                string mapPath = Server.MapPath("~/App_Data/FocisBookingCancellation.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                string l1 = "Unfortunately we've had to cancel your booking (" + BookingId + ") with us. The cargo you are trying to ship is restricted and therefore we are unable to proceed with the movement. We will issue a refund for the amount you've paid. We hope to be able to serve you on another booking in the near future.";
                body = body.Replace("{Message1}", l1);
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(User);
                body = body.Replace("{Team}", UE.FIRSTNAME);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();

                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", User, "", "",
              "JobBooking", "Focis Job Cancellation", BookingId, User, body, "", true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        [HttpPost]
        public JsonResult CollaborationInitiate(string BookingId, int Id, string SupplierUser, string SupplierName, string Owner, string note, string ChinnaCheck)
        {
            string result = null;
            int chinacountry = Convert.ToInt32(ChinnaCheck);
            int othercountry = 0;
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            try
            {
                JobBookingDbFactory UD = new JobBookingDbFactory();
                string OwnerFN = UD.GetUserFirstNInsertSupplier(BookingId, SupplierUser, Owner, note);

                MailMessage message = new MailMessage();
                string body = string.Empty;
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(SupplierUser));

                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "ShipA Freight - Action Requested: Provide Details for Upcoming Shipment" + EnvironmentName;

                message.IsBodyHtml = true;
                string l1 = "Shipa Freight user " + OwnerFN + " has requested that you provide exact weights, dimensions, pickup location and/or other details for an upcoming shipment.";
                string l2 = "To ensure timely pickup of your cargo, please provide shipment details as soon as possible by clicking the link below and following the instructions on the page.";
                string l3 = "Shipa Freight User Notes:";

                string resetLink = string.Empty;
                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                string url = "id=" + Id + "?col=" + SupplierUser + "?ChinaCheck=" + othercountry + "?language=" + uiCulture;
                JsonResult t1 = EncryptId(url);
                string rs = t1.Data.ToString();
                resetLink = Actionlink + "Quotation/CargoCheck" + rs;
                string ColButton = "<td style=font-family: 'Open Sans', sans-serif; background-color: #f29221; padding: 10px; text-align: center; color: #FFF;><a href=" + resetLink + " style=height: 50px; width: 100px; text-align: center; color: #FFF; background-color: #f29221; text-decoration: none; text-transform: uppercase>Update Shipment Details</a></td>";
                if (ChinnaCheck.Equals("48"))
                {
                    string resetLinkChinese = string.Empty;
                    string ActionlinkChinese = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                    string urlchinese = "id=" + Id + "?col=" + SupplierUser + "?ChinaCheck=" + chinacountry + "?language=" + uiCulture;
                    JsonResult t2 = EncryptId(urlchinese);
                    string rs1 = t2.Data.ToString();
                    resetLinkChinese = ActionlinkChinese + "Quotation/CargoCheck" + rs1;
                    string ColButtonChinese = "<td style=font-family: 'Open Sans', sans-serif; background-color: #f29221; padding: 10px; text-align: center; color: #FFF;><a href=" + resetLinkChinese + " style=height: 50px; width: 100px; text-align: center; color: #FFF; background-color: #f29221; text-decoration: none; text-transform: uppercase>Update Shipment Details</a></td>";
                    string mapPath = Server.MapPath("~/App_Data/ChineseCollaborationInitiate.html");
                    using (StreamReader reader = new StreamReader(mapPath))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{Team}", SupplierName);
                    body = body.Replace("{user_created}", OwnerFN);
                    body = body.Replace("{Message1}", l1);
                    body = body.Replace("{Message2}", l2);
                    body = body.Replace("{Message3}", l3);
                    body = body.Replace("{Message4}", note);
                    body = body.Replace("{ColButton}", ColButton);
                    body = body.Replace("{ColButtonChinese}", ColButtonChinese);
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                    message.Body = body;
                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                    client.Dispose();
                    message.Dispose();

                }


                else
                {
                    string mapPath = Server.MapPath("~/App_Data/CollaborationInitiate.html");
                    using (StreamReader reader = new StreamReader(mapPath))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{Team}", SupplierName);
                    body = body.Replace("{Message1}", l1);
                    body = body.Replace("{Message2}", l2);
                    body = body.Replace("{Message3}", l3);
                    body = body.Replace("{Message4}", note);
                    body = body.Replace("{ColButton}", ColButton);
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                    message.Body = body;
                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                    client.Dispose();
                    message.Dispose();
                }

                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", SupplierUser, "", "",
              "JobBooking", "Collaboration Initiate", BookingId, SupplierUser, body, "", true, message.Subject.ToString());
                result = "success";
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAmazonAddressselected(string Code)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                var data = mFactory.GetAmazonAddressselected(Code);
                var JsonToReturn = new
                {
                    Data = data
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        public StripeData GetStripeData(string currency)
        {
            StripeData stripeData = new StripeData();
            if (currency.ToLowerInvariant() == "eur" || currency.ToLowerInvariant() == "gbp")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_NL"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_NL"];
                stripeData.account = "stripeNetherlands";
            }
            else if (currency.ToLowerInvariant() == "chf")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_CH"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_CH"];
                stripeData.account = "stripeSwiss";
            }
            else if (currency.ToLowerInvariant() == "sgd")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_SG"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_SG"];
                stripeData.account = "stripeSingapore";
            }
            else
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_US"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_US"];
                stripeData.account = "stripeUSA";
            }
            return stripeData;
        }

        public StripeData GetStripeAccount(string account)
        {
            StripeData stripeData = new StripeData();
            if (account.ToLowerInvariant() == "stripeNetherlands")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_NL"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_NL"];
                stripeData.account = "stripeNetherlands";
            }
            else if (account.ToLowerInvariant() == "stripeSwiss")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_CH"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_CH"];
                stripeData.account = "stripeSwiss";
            }
            else if (account.ToLowerInvariant() == "stripeSingapore")
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_SG"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_SG"];
                stripeData.account = "stripeSingapore";
            }
            else
            {
                stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_US"];
                stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_US"];
                stripeData.account = "stripeUSA";
            }
            return stripeData;
        }

        [AllowAnonymous]
        public void SendRemittanceMail(string opjobnumber, string type)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            string recipientsList = string.Empty;
            string role = string.Empty;
            string userId = string.Empty;
            string messagebody = string.Empty;
            if (type.ToUpper() == "AWAITINGGSSCREVIEW")
            {
                role = "SSPGSSC";
                messagebody = "Remittance details are now available to be reviewed for job " + opjobnumber + ". Please review and "
                              + "approve the information and submit for country finance approval.";
            }
            else if (type.ToUpper() == "AWAITINGCOUNTRYFINANCEAPPROVAL")
            {
                role = "SSPCOUNTRYFINANCE";
                messagebody = "Remittance details are now available to be reviewed for job " + opjobnumber + ". Please review and approve the information."
                               + "Once you have reviewed and approved the payment will go to the Treasury team to be remitted to your country through MCS Euronetting.";
                userId = mFactory.GetUserIdByOpJobNumber(opjobnumber);
            }
            else if (type == "AWAITINGTREASURYAPPROVAL")
            {
                role = "SSPREMITTANCE";
                messagebody = "Job " + opjobnumber + " is now ready to be remitted. Please review the information and add a date for remittance through MCS Euronetting.";
            }
            recipientsList = mFactory.GetEmailList(role, "", "", "", userId);
            if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
            else
            {
                string[] mailIds = recipientsList.Split(',');
                foreach (string toEmailId in mailIds)
                {
                    message.To.Add(new MailAddress(toEmailId));
                }
            }
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight- Remittance details are now available -" + opjobnumber + EnvironmentName;
            string mapPath = Server.MapPath("~/App_Data/RemittanceFocis.html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{Message}", messagebody);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
             "JobBooking", "RemittanceMailFocis", opjobnumber, "", body, "", true, message.Subject.ToString());
        }

        JsonResult Create3DSource(StripeChargeCreateOptions data, string mPaySource)
        {
            StripeData stripeData = GetStripeData(data.Currency);
            var url = System.Configuration.ConfigurationManager.AppSettings["APPURL"]
                        + "JobBooking/StripeResponse?currency=" + data.Currency;

            var dic = new Dictionary<string, string>();
            dic.Add("id", data.Metadata["id"]);
            dic.Add("email", data.Metadata["email"]);
            dic.Add("discountcode", data.Metadata["discountcode"]);
            dic.Add("mPaySource", mPaySource);

            var sourceOptions = new StripeSourceCreateOptions
            {
                Type = StripeSourceType.ThreeDSecure,
                Currency = data.Currency,
                Amount = data.Amount,
                ThreeDSecureCardOrSourceId = data.SourceTokenOrExistingSourceId,
                RedirectReturnUrl = url,
                Metadata = dic,
                StatementDescriptor = data.Description
            };

            var sourceService = new StripeSourceService(stripeData.publicKey);
            try
            {
                StripeSource source = sourceService.Create(sourceOptions);
                return Json(new
                {
                    redirectUrl = source.Redirect.Url,
                    isRedirect = true
                });
            }
            catch
            {
                if (mPaySource.ToLowerInvariant() == "web")
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("ServerError", "Payment", null),
                        isRedirect = true
                    });
                }
                else
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("MServerError", "Payment", null),
                        isRedirect = true
                    });
                }
            }
        }

        public ActionResult StripeResponse(string currency, string client_secret, string livemode, string source)
        {
            StripeData stripeData = GetStripeData(currency);
            var sourceService = new StripeSourceService(stripeData.secretKey);

            StripeSource threeDSource = sourceService.Get(source);
            if (threeDSource.Status.ToLowerInvariant() == "chargeable")
            {
                var d = StripePayment(
                        Convert.ToInt32(threeDSource.Metadata["id"]),
                        currency,
                        threeDSource.StatementDescriptor,
                        source,
                        threeDSource.Metadata.ContainsKey("discountcode") ? threeDSource.Metadata["discountcode"] : "",
                        threeDSource.Metadata["email"],
                        threeDSource.Metadata["mPaySource"]
                     );

                OnlinePaymentResponseDto resp =
                            JsonConvert.DeserializeObject<OnlinePaymentResponseDto>(JsonConvert.SerializeObject(d.Data));
                return Redirect(resp.redirectUrl);
            }
            else
            {
                if (threeDSource.Metadata["mPaySource"].ToLowerInvariant() == "web")
                {
                    return Redirect(Url.Action("OnlineCCFailure", "Payment", new MPaymentResult
                    {
                        JOBNUMBER = Convert.ToInt32(threeDSource.Metadata["id"]),
                        TOKEN = source,
                        DATA = threeDSource.Status,
                        USERID = threeDSource.Metadata["email"]
                    }));
                }
                else
                {
                    return Redirect(Url.Action("MOnlineCCFailure", "Payment", new MPaymentResult
                    {
                        JOBNUMBER = Convert.ToInt32(threeDSource.Metadata["id"]),
                        TOKEN = source,
                        DATA = threeDSource.Status,
                        USERID = threeDSource.Metadata["email"]
                    }));
                }
            }
        }

        StripeCardModel FetchCardData(StripeCharge data)
        {
            StripeCardModel card = new StripeCardModel();
            try
            {
                StripeData stripeData = GetStripeData(data.Currency);
                var sourceService = new StripeSourceService(stripeData.secretKey);
                StripeSource SourceObject = sourceService.Get(data.Source.Id);
                if (SourceObject.Type == "three_d_secure")
                {
                    var carddata = JsonConvert.DeserializeObject<StripeChargeResponseModel>(data.StripeResponse.ResponseJson);
                    card.Brand = carddata.Source.three_d_secure.Brand;
                    card.Last4 = carddata.Source.three_d_secure.Last4;
                }
                else
                {
                    card.Brand = SourceObject.Card.Brand;
                    card.Last4 = SourceObject.Card.Last4;
                }
            }
            catch
            {
                card.Brand = string.Empty;
                card.Last4 = string.Empty;
            }
            return card;
        }

        public void SendReferal_CreditAdded_Email(string couponcode, string user, string currency, double discountValue)
        {
            if (!string.IsNullOrEmpty(couponcode))
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                string result = JBFactory.SendReferal_CreditAdded_Email(couponcode, user, currency, discountValue);
                string[] referal = result.Split(',');

                if (!string.IsNullOrEmpty(referal[0]) && referal[0] != "0")
                {
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress("noreply@shipafreight.com");
                    message.To.Add(new MailAddress(referal[0]));
                    string EnvironmentName = DynamicClass.GetEnvironmentName();
                    message.Subject = "Shipa Freight - Congratulations !" + EnvironmentName;

                    string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                    var profileLink = Actionlink + "UserManagement/MyProfile";

                    message.IsBodyHtml = true;
                    string body = string.Empty;
                    string mapPath = Server.MapPath("~/App_Data/referral-credit-added.html");
                    using (StreamReader reader = new StreamReader(mapPath))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{{username}}", referal[2]);
                    body = body.Replace("{{amount}}", referal[3]);
                    body = body.Replace("{{profileurl}}", profileLink);
                    message.Body = body;
                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                    client.Dispose();
                    message.Dispose();
                }
            }
        }

        [HttpPost]
        public JsonResult HsbcPayment(int mJobnumber, string mUser, string mCouponCode, double requestedAmount = 0)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(mJobnumber, mUser, mCouponCode, requestedAmount);
            HsbcOrder order = createOrder(mDBEntity, mCouponCode);
            if (string.IsNullOrEmpty(order.orderNo))
            {
                return Json(new { Data = new HSBCResponseDto { StatusCode = "False", proCode = "999998", proMsg = "Payment Done Already" } });
            }
            string strOrder = JsonConvert.SerializeObject(order);
            string postData = HsbcEncryption.signAndEncrypt(strOrder);
            HSBCResponseDto res = sendRequest(postData, order.payType, "merchant/pcPay");
            res.orderid = order.orderNo;
            var data = new
            {
                Data = res
            };
            return Json(data);
        }

        public HsbcOrder createOrder(PaymentEntity model, string mCouponCode)
        {
            long rand = 999999999999999;

            HsbcOrder order = new HsbcOrder()
            {
                limitPay = string.Empty,
                goodsInfo = string.Empty,
                remarks = string.Empty,
                orderTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                transType = "01",
                payType = "QCARDPAY",
                orderCurr = "CNY",
                merId = ConfigurationManager.AppSettings["MerchantID"].ToString(),
                merIp = ConfigurationManager.AppSettings["MerchantIP"].ToString(),
                userId = ConfigurationManager.AppSettings["UserID"].ToString(),
                notifyUrl = ConfigurationManager.AppSettings["NotifyUrl"].ToString(),
                frontUrl = ConfigurationManager.AppSettings["FrontUrl"].ToString(),
                goodsDes = model.OPJOBNUMBER,
                expireTime = "15"
            };

            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(model.JOBNUMBER));
            decimal PayAmount;
            if (PaymentStatus.ToLower() == "notexist")
            {
                PayAmount = model.RECEIPTNETAMOUNT;
            }
            else if (model.ADDITIONALCHARGESTATUS != null && model.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
            {
                PayAmount = model.ADDITIONALCHARGEAMOUNT;
            }
            else
            {
                order.orderNo = string.Empty;
                return order;
            }

            var db = new JobBookingDbFactory();
            var value = db.GetCurrencyAndOrder(PayAmount.ToString(), model.CURRENCY.ToUpper(), "CNY");
            string[] val = value.Split(',');

            order.orderAmt = Convert.ToInt32(Convert.ToDecimal(val[0]) * 100).ToString();
            order.orderNo = ConfigurationManager.AppSettings["MerchantID"].ToString()
                            + (rand - Convert.ToDouble(val[1])).ToString();

            db.SaveStripeTransactionDetails(
                model.JOBNUMBER.ToString(),
                "pending",
                PayAmount.ToString(),
                model.CURRENCY.ToUpper(),
                "Credit/Debit",
                order.orderNo,
                string.Empty,
                mCouponCode,
                "CNY",
                val[0],
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                "HSBC"
            );

            return order;
        }

        public HSBCResponseDto sendRequest(string postData, string PaymentType, string urlpath)
        {
            try
            {
                HSBCResponseDto resp = new HSBCResponseDto();
                string url = ConfigurationManager.AppSettings["hsbcurl"].ToString() + urlpath;
                string clientID = ConfigurationManager.AppSettings["clientID"].ToString();
                string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
                HttpContent httpContent = new StringContent(postData);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("x-hsbc-client-id", clientID);
                httpClient.DefaultRequestHeaders.Add("x-hsbc-client-secret", clientSecret);
                HttpResponseMessage response = httpClient.PostAsync(url, httpContent).Result;
                resp.StatusCode = response.IsSuccessStatusCode.ToString();
                if (response.IsSuccessStatusCode)
                {
                    string json = HsbcEncryption.decryptAndVerify(response.Content.ReadAsStringAsync().Result);
                    //string json = response.Content.ReadAsStringAsync().Result;
                    var jo = (JObject)JsonConvert.DeserializeObject(json);
                    var res = (JObject)jo["response"];
                    if (jo["response"]["proCode"].ToString() != "999999")
                    {
                        if (PaymentType.ToLowerInvariant() == "qcardpay" || PaymentType.ToLowerInvariant() == "card" || PaymentType.ToLowerInvariant() == "pcpay")
                        {
                            var pageContent = HttpUtility.UrlDecode(jo["response"]["htmlPage"].ToString());
                            resp.HtmlCode = Convert.ToBase64String(Encoding.UTF8.GetBytes(pageContent));
                        }
                    }
                    resp.proCode = jo["response"]["proCode"].ToString();
                    return resp;
                }
                else return resp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static void generateQRcode(string url)
        {
            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = qrEncoder.Encode(url);

            var render = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Two));

            using (FileStream stream = new FileStream(System.Web.HttpContext.Current.Server.MapPath("~/images/url.png"), FileMode.Create))
            {
                render.WriteToStream(qrCode.Matrix, ImageFormat.Png, stream);
            }
        }

        [HttpGet]
        public JsonResult HsbcStatusEnquiry(string order)
        {
            string url = ConfigurationManager.AppSettings["hsbcurl"].ToString() + "merchant/payEnq";
            string clientID = ConfigurationManager.AppSettings["clientID"].ToString();
            string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
            var dd = DateTime.Now.ToString("yymmdd") + DateTime.Now.ToString("hhmmss");
            HSBCStatusDto data = new HSBCStatusDto()
            {
                orderNo = ConfigurationManager.AppSettings["MerchantID"].ToString() + dd,
                merId = ConfigurationManager.AppSettings["MerchantID"].ToString(),
                merIp = ConfigurationManager.AppSettings["MerchantIP"].ToString(),
                transType = "03",
                orgOrderNo = order
            };
            var reqdata = HsbcEncryption.signAndEncrypt(JsonConvert.SerializeObject(data));
            HttpContent httpContent = new StringContent(reqdata);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("x-hsbc-client-id", clientID);
            httpClient.DefaultRequestHeaders.Add("x-hsbc-client-secret", clientSecret);
            HttpResponseMessage response = httpClient.PostAsync(url, httpContent).Result;

            HSBCResponseDto resp = new HSBCResponseDto()
            {
                StatusCode = response.IsSuccessStatusCode.ToString()
            };

            if (response.IsSuccessStatusCode)
            {
                string json = HsbcEncryption.decryptAndVerify(response.Content.ReadAsStringAsync().Result);
                var jo = (JObject)JsonConvert.DeserializeObject(json);
                var res = (JObject)jo["response"];
                resp.proCode = jo["response"]["proCode"].ToString();
                resp.proMsg = jo["response"]["proMsg"] == null ? "" : jo["response"]["proMsg"].ToString();
            }
            return Json(resp, JsonRequestBehavior.AllowGet);
        }
    }
}