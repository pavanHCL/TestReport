﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text.html.simpleparser;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using iTextSharp.text.pdf.draw;
using System.Text.RegularExpressions;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.JP;
using System.Web.Routing;
using System.Security.Cryptography;
using System.Web.Helpers;
using System.Net;
using FOCiS.SSP.Web.UI.CacheMechanisim;
using Newtonsoft.Json;
using FOCiS.SSP.Models.CacheManagement;
using System.Collections;
using System.Threading.Tasks;
using FOCiS.SSP.Library.Extensions;
using System.Web.Security;
using System.Web;
using FOCiS.SSP.Web.UI.Helpers;
using FindUserCountryName;
using System.Threading;

namespace FOCiS.SSP.Web.UI.Controllers.Quotation
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class QuotationController : BaseController
    {

        IQuotationDBFactory _qdb;

        public QuotationController()
        {
            _qdb = new QuotationDBFactory();
        }

        public QuotationController(IQuotationDBFactory db)
        {
            _qdb = db;
        }

        private static string preferredCurrencyId = string.Empty;
        public string PreferredCurrencyId
        {
            get { return preferredCurrencyId; }
            set { preferredCurrencyId = value; }
        }
        private static QuotationPreviewModel dUIModels;
        public QuotationPreviewModel DUIModels
        {
            get { return dUIModels; }
            set { dUIModels = value; }
        }
        private static byte[] Bytes;
        public byte[] bytes
        {
            get { return Bytes; }
            set { Bytes = value; }
        }
        string body = string.Empty;
        string subject = string.Empty;
        string mPlaceOfReceipt = string.Empty;
        string mPlaceOfDelivery = string.Empty;
        string mPRLableName = string.Empty;
        string mPDLableName = string.Empty;
        string cargodetails = string.Empty;
        string productname = string.Empty;
        private string templateName = string.Empty;
        public string TemplateName
        {
            get { return templateName; }
            set { templateName = value; }
        }

        [AllowAnonymous]
        [HttpPost]
        public void ShareLink(string email, string link, int id, string name, string notes, string AddresBook)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id);
                DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                subject = "Quote from Shipa Freight" + EnvironmentName;
                if (!ReferenceEquals(DUIModels, null))
                {
                    string loginuser = string.Empty;
                    if (!ReferenceEquals(Request.Cookies["myCookie"], null))
                    {
                        loginuser = Request.Cookies["myCookie"].Values["UserName"].ToString();
                    }
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                        memoryStream.Close();
                    }
                    Assignvalues(DUIModels);
                    if (DUIModels.ProductName.ToUpperInvariant() == "AIR")
                        productname = "air";
                    else
                        productname = "ship";

                    string str = Encryption.Encrypt(("id=" + id).ToString());
                    str = "?q=" + str;
                    string links = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "Quotation/Preview" + str;
                    using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/quote-link-temp.html")))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{link}", links);
                    body = body.Replace("{user}", ((string.IsNullOrEmpty(name)) ? email : name));
                    body = body.Replace("{loginuser}", (DUIModels.CreatedBy == null || DUIModels.CreatedBy == "guest" || DUIModels.CreatedBy == "") ? DUIModels.GuestName : loginuser);
                    body = body.Replace("{notes}", String.IsNullOrEmpty(notes) ? "" : notes);
                    body = body.Replace("{DateofEnquiry}", DUIModels.DateofEnquiry.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{PlaceofReceipt}", mPRLableName);
                    body = body.Replace("{PlaceofDepature}", mPDLableName);
                    body = body.Replace("{OriginPlaceName}", mPlaceOfReceipt);
                    body = body.Replace("{DestinationPlaceName}", mPlaceOfDelivery);
                    body = body.Replace("{PreferredCurrencyId}", DUIModels.PreferredCurrencyId);
                    body = body.Replace("{GrandTotal}", Double.Parse(DUIModels.GrandTotal.ToString()).ToString());
                    body = body.Replace("{DateCreated}", DUIModels.DateofEnquiry.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{DateOfValidity}", DUIModels.DateOfValidity.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{ProductName}", DUIModels.ProductName);
                    body = body.Replace("{MovementTypeName}", DUIModels.MovementTypeName);
                    body = body.Replace("{OriginPlaceName}", DUIModels.OriginPlaceName);
                    body = body.Replace("{DestinationPlaceName}", DUIModels.DestinationPlaceName);
                    body = body.Replace("{productname}", productname);
                    body = body.Replace("{CargoDetails}", cargodetails);
                    body = body.Replace("{howitworks}", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/LearnMore/how-it-works");
                    body = body.Replace("{faq}", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/LearnMore/Faq");
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                }
                SendMailWithAddressBook(AddresBook, body, subject, bytes, DUIModels.QuotationNumber);
                if (AddresBook != "")
                {
                    mFactory.InsertNewEmailsIntoAddressBook(AddresBook, HttpContext.User.Identity.Name.ToString());
                }
            }
        }

        private void SendMailWithAddressBook(string AddresBook, string body, string subject, byte[] bytes = null, string QuotationNumber = "")
        {
            MailMessage message = new MailMessage();
            if (AddresBook != "")
            {
                if (AddresBook.Contains(','))
                {
                    foreach (string s in AddresBook.Split(','))
                    {
                        message.Bcc.Add(new MailAddress(s));
                    }
                }
                else
                {
                    message.CC.Add(new MailAddress(AddresBook));
                }
            }
            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            if (bytes != null)
                message.Attachments.Add(new Attachment(new MemoryStream(bytes), QuotationNumber + ".pdf"));
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", "", "", AddresBook, "Quotation", "Share", QuotationNumber.ToString(), HttpContext.User.Identity.Name.ToString(), body, "", false, subject);
            var nJSON = new JsonResult();
            nJSON.Data = true;
        }

        [AllowAnonymous]
        public void SendMailWithAttachment(int id)
        {
            string str = Encryption.Encrypt(("id=" + id).ToString());
            str = "?q=" + str;
            string link = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "Quotation/Preview" + str;
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string EnvironmentNameDocs = DynamicClass.GetEnvironmentNameDocs();
            subject = "Quote from Shipa Freight" + EnvironmentName;
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id);
                DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                string Email = (DUIModels.CreatedBy == null || DUIModels.CreatedBy == "guest" || DUIModels.CreatedBy == "") ? DUIModels.GuestEmail : DUIModels.CreatedBy;
                string UserName = (DUIModels.CreatedBy == null || DUIModels.CreatedBy == "guest" || DUIModels.CreatedBy == "") ? DUIModels.GuestName : mDBEntity.Users[0].FIRSTNAME;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                    memoryStream.Close();
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                    sspdoc.QUOTATIONID = Convert.ToInt64(id);
                    sspdoc.DOCUMNETTYPE = "ShipAFreightDoc";
                    sspdoc.FILENAME = DUIModels.QuotationNumber.ToString() + ".pdf";
                    sspdoc.FILEEXTENSION = "application/pdf";
                    sspdoc.FILESIZE = bytes.Length;
                    sspdoc.FILECONTENT = bytes;
                    sspdoc.JOBNUMBER = 0;
                    sspdoc.DocumentName = "Quotation";
                    JobBookingDbFactory mjobFactory = new JobBookingDbFactory();
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    long docid = mjobFactory.SaveDocuments(docentity, Convert.ToString(DUIModels.CreatedBy), "", "", EnvironmentNameDocs, "NO", 2);
                    Assignvalues(DUIModels);
                    if (DUIModels.ProductName.ToUpperInvariant() == "AIR")
                        productname = "air";
                    else
                        productname = "ship";

                    using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/quote-ready-temp.html")))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{user}", ((string.IsNullOrEmpty(UserName) != true) ? UserName : string.Empty));
                    body = body.Replace("{loginuser}", DUIModels.CreatedBy);
                    body = body.Replace("{DateofEnquiry}", DUIModels.DateofEnquiry.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{PlaceofReceipt}", mPRLableName);
                    body = body.Replace("{PlaceofDepature}", mPDLableName);
                    body = body.Replace("{OriginPlaceName}", mPlaceOfReceipt);
                    body = body.Replace("{DestinationPlaceName}", mPlaceOfDelivery);
                    body = body.Replace("{PreferredCurrencyId}", DUIModels.PreferredCurrencyId);
                    body = body.Replace("{GrandTotal}", mFactory.RoundedValue(DUIModels.GrandTotal.ToString(), DUIModels.PreferredCurrencyId));
                    body = body.Replace("{DateOfValidity}", DUIModels.DateOfValidity.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{ProductName}", DUIModels.ProductName);
                    body = body.Replace("{MovementTypeName}", DUIModels.MovementTypeName);
                    body = body.Replace("{OriginPlaceName}", DUIModels.OriginPlaceName);
                    body = body.Replace("{DestinationPlaceName}", DUIModels.DestinationPlaceName);
                    body = body.Replace("{productname}", productname);
                    body = body.Replace("{QuotationNumber}", DUIModels.QuotationNumber);
                    body = body.Replace("{CargoDetails}", cargodetails);
                    body = body.Replace("{howitworks}", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/LearnMore/how-it-works");
                    body = body.Replace("{faq}", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/LearnMore/Faq");
                    body = body.Replace("{link}", link);
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                    if ((DUIModels.SubmissionComments) != null && (DUIModels.SubmissionComments) != "")
                    {
                        string strSub = "<tbody><tr><td style=3D'color: #333; font-family=: 'Open Sans', sans-serif; font-size: 16px;'>Submission Comments:</td></tr><tr>" +
                                "<td style=3D'color: #333; font-family=: 'Open Sans', sans-serif; font-size: 16px;'>" + DUIModels.SubmissionComments + "</td></tr></tbody>";
                        body = body.Replace("{SubmissionComments}", strSub);
                    }
                    else
                    {
                        body = body.Replace("{SubmissionComments}", "");
                    }
                    SendMail(Email, body, subject, "Gssc Approved", true, bytes, DUIModels.QuotationNumber, docid);
                }
            }
        }

        public void SendMailForGSSCQuotes(string number, int threshold, int hazordous, int notavailable)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            Email = Email + "," + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];
            subject = "Request for Quote submitted in GSSC Flow : " + number;
            string mbody = "Dear User,";
            mbody = mbody + "<br/> <br/>" + "A Request for Quote is  set to GSSC and available because of";

            if (notavailable > 0)
                mbody = mbody + "<br/> " + "\u2022 " + " Rates are not available";
            if (threshold == 1)
                mbody = mbody + "<br/> " + "\u2022 " + " Threshold value met";
            if (hazordous == 1)
                mbody = mbody + "<br/> " + "\u2022 " + " Hazordous Goods available";

            mbody = mbody + "<br/> <br/>" + " Please use the below URL to access the application";
            mbody = mbody + "<br/>" + URL;
            mbody = mbody + "<br/> <br/>" + "Regards";
            mbody = mbody + "<br/>" + "Shipa Freight Team";
            SendMail(Email, mbody, subject, "Request for Quote submitted in GSSC Flow", false, null, number);
        }

        public void SendMailForMissingChargesGSSCQuotes(QuotationEntity mDBEntity)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCMCEmail"];
            string PAEmail = System.Configuration.ConfigurationManager.AppSettings["GSSCMCPAEmail"];
            string CountryManager = System.Configuration.ConfigurationManager.AppSettings["GSSCManagerEmail"];
            string CountryNPC = System.Configuration.ConfigurationManager.AppSettings["GSSCNPCEmail"];
            string NPCAndManager = CountryNPC + "," + CountryManager;
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            subject = "Attention: Quote " + DUIModels.QuotationNumber + " with missing " + " " + mDBEntity.GSSCREASON.ToLower() + " " + "charges sent to customer" + EnvironmentName;
            string mbody = string.Empty;
            string quoteInfo = string.Empty;
            quoteInfo = "<table cellpadding=" + 5 + " cellspacing=" + 2 + " width = " + 600 + ">";
            quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Mode of Transport" + "</td><td>" + " : " + "</td><td>" + DUIModels.ProductName + "</td><td style=\"font-weight:bold;\"> " + "Movement Type " + "</td><td>" + " : " + "</td><td>" + DUIModels.MovementTypeName + "</td> </tr>";
            quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin country" + "</td><td>" + " : " + "</td><td>" + mDBEntity.OCOUNTRYNAME + "</td><td style=\"font-weight:bold;\"> " + "Destination country " + "</td><td>" + " : " + "</td><td>" + mDBEntity.DCOUNTRYNAME;
            if (DUIModels.OriginPlaceName != null && DUIModels.DestinationPlaceName != null)
            {
                quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin City" + "</td><td>" + " : " + "</td><td>" + DUIModels.OriginPlaceName + "</td><td style=\"font-weight:bold;\"> " + "Destination City" + "</td><td>" + " : " + "</td><td>" + DUIModels.DestinationPlaceName;
            }
            else
            {
                if (DUIModels.OriginPlaceName != null)
                {
                    quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin City" + "</td><td>" + " : " + "</td><td>" + DUIModels.OriginPlaceName;
                }
                if (DUIModels.DestinationPlaceName != null)
                {
                    quoteInfo = quoteInfo + "<tr><td>" + "</td><td>" + "</td><td>" + DUIModels.OriginPlaceName + "</td><td style=\"font-weight:bold;\"> " + "Destination City" + "</td><td>" + " : " + "</td><td>" + DUIModels.DestinationPlaceName;
                }
            }
            if (DUIModels.OriginPortName != null && DUIModels.DestinationPortName != null)
            {
                quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin Port" + "</td><td>" + " : " + "</td><td>" + DUIModels.OriginPortName + "</td><td style=\"font-weight:bold;\"> " + "Destination Port" + "</td><td>" + " : " + "</td><td>" + DUIModels.DestinationPortName;
            }
            quoteInfo = quoteInfo + "</table>";

            string mapPath = Server.MapPath("~/App_Data/MissingChargesQuote.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{QuotationNumber}", DUIModels.QuotationNumber);
            mbody = mbody.Replace("{GsscReason}", mDBEntity.GSSCREASON.ToLower());
            mbody = mbody.Replace("{QuoteInfo}", quoteInfo);
            mbody = mbody.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                memoryStream.Close();
            }
            string countryId = string.Empty;
            if (string.IsNullOrEmpty(EnvironmentName))
            {
                string reason = mDBEntity.GSSCREASON; ;
                if (!string.IsNullOrEmpty(reason))
                {
                    string[] gsscReason = reason.Split(',');
                    int gsscReasonCount = gsscReason.Count();
                    if (gsscReasonCount > 1)
                    {
                        countryId = mDBEntity.OCOUNTRYID + "," + mDBEntity.DCOUNTRYID;
                    }
                    else
                    {
                        countryId = gsscReason[0].ToUpper() == "ORIGIN" ? Convert.ToString(mDBEntity.OCOUNTRYID) : Convert.ToString(mDBEntity.DCOUNTRYID);
                    }
                }
                string emails = mFactory.GetNPCAndCountryManagerEmailsForNPC(countryId);
                NPCAndManager = string.IsNullOrEmpty(emails) ? NPCAndManager : emails;
            }
            Email = Email + "," + PAEmail + "," + NPCAndManager;
            SendMail(Email, mbody, subject, "Quote with missing Origin/Destination charges", false, bytes, DUIModels.QuotationNumber);
        }

        public void SendMail(string Email, string body, string subject, string submodule, bool MailStore, byte[] bytes = null, string QuotationNumber = "", long docid = 0)
        {
            string TO = string.Empty;
            string BCC = string.Empty;
            string CC = string.Empty;
            string attachment = string.Empty;
            MailMessage message = new MailMessage();
            if (Email.Contains(';'))
            {
                if (submodule.ToUpper() == "QUOTE WITH MISSING ORIGIN/DESTINATION CHARGES")
                {

                    foreach (string s in Email.Split(';'))
                    {
                        message.To.Add(new MailAddress(s));
                        TO = s + ",";
                    }
                }
                else
                {
                    foreach (string s in Email.Split(';'))
                    {
                        message.Bcc.Add(new MailAddress(s));
                        BCC = s + ",";
                    }
                }
            }
            else if (Email.Contains(','))
            {
                if (submodule.ToUpper() == "QUOTE WITH MISSING ORIGIN/DESTINATION CHARGES")
                {

                    foreach (string s in Email.Split(','))
                    {
                        message.To.Add(new MailAddress(s));
                    }
                    TO = Email;
                }
                else
                {
                    foreach (string s in Email.Split(','))
                    {
                        message.Bcc.Add(new MailAddress(s));
                    }
                    BCC = Email;
                }
            }
            else
            {
                if (submodule.ToUpper() == "QUOTE WITH MISSING ORIGIN/DESTINATION CHARGES")
                {
                    message.To.Add(new MailAddress(Email));
                    TO = Email;
                }
                else if (submodule.ToUpper() == "CONTACT US REQUEST")
                {
                    message.To.Add(new MailAddress(Email));
                    TO = Email;
                }
                else
                {
                    message.CC.Add(new MailAddress(Email));
                    CC = Email;
                }
            }
            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            if (bytes != null)
                message.Attachments.Add(new Attachment(new MemoryStream(bytes), QuotationNumber + ".pdf"));

            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", TO, CC, BCC,
             "Quotation", submodule, QuotationNumber.ToString(), HttpContext.User.Identity.Name.ToString(), body, docid.ToString(), MailStore, message.Subject.ToString());
            var nJSON = new JsonResult();
            nJSON.Data = true;
        }

        public void SendMailForContactUs(QueryInformationModel QueryInformation)
        {
            string Email;
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            if (string.IsNullOrEmpty(EnvironmentName))
            {
                Email = System.Configuration.ConfigurationManager.AppSettings["ContactusEmailForProduction"];
            }
            else
            {
                Email = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            }
            subject = "Contact us request" + EnvironmentName;
            string mbody = string.Empty;
            string data = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/App_Data/ContactUs.html")))
            { mbody = reader.ReadToEnd(); }
            data = data + "A Contact us request for Shipa Freight with the following details as below:";
            data = data + "<br/> <br/>" + "CompanyName : " + QueryInformation.CompanyName;
            data = data + "<br/> <br/>" + "ContactNumber : " + QueryInformation.ContactNumber;
            data = data + "<br/> <br/>" + "Description : " + QueryInformation.Discription;
            data = data + "<br/> <br/>" + "EmailId : " + QueryInformation.EmailId;
            data = data + "<br/> <br/>" + "Requester Name : " + QueryInformation.QueryName;
            data = data + "<br/> <br/>" + "QueryType : " + QueryInformation.QueryType;
            data = data + "<br/> <br/>" + "How to know : " + QueryInformation.Howtoknow;
            mbody = mbody.Replace("{body}", data);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            SendMail(Email, mbody, subject, "Contact us request", false, null, QueryInformation.EmailId);
        }

        public void Sendmail_CollaborationQuote(string userId, string userId_supplier)
        {
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string msubject = "New Quote Available for Your Shipment" + EnvironmentName;
            string toEmail = userId;
            string mbody = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/Collaboration-Quote.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{UserId}", userId_supplier);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMail(toEmail, mbody, msubject, "Collaboration quote request", false, null, "");
        }

        public void Sendmail_NoCollaborationQuote(string userId, string userId_supplier)
        {
            string msubject = "No change to Weight/Dimensions";
            string toEmail = userId;
            string mbody = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/Collaboration-NoQuote.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{UserId}", userId_supplier);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMail(toEmail, mbody, msubject, "Collaboration No quote", false, null, "");
        }

        public void SendmailtoNPC(QuotationEntity mDBEntity)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string msubject = "Overweight/CBM threshold quote request - " + DUIModels.QuotationNumber + EnvironmentName;
            string NPCEmail = System.Configuration.ConfigurationManager.AppSettings["NPCEmail"];
            string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string mbody = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/OverweightCBM_threshold.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{Country}", mDBEntity.OCOUNTRYNAME);
            mbody = mbody.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

            if (mDBEntity.PRODUCTNAME == "Ocean")
            {
                try
                {
                    DataTable dt = mFactory.GetNPCdetails(Convert.ToInt64(mDBEntity.OCOUNTRYID));
                    if ((dt.Rows.Count > 0) && (mDBEntity.TOTALGROSSWEIGHT > Convert.ToDecimal(dt.Rows[0]["OCEANMAXWEIGHT"].ToString()) || mDBEntity.TOTALCBM > Convert.ToDecimal(dt.Rows[0]["OCEANCBM"].ToString())))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                            memoryStream.Close();
                        }
                        string Emailid = (dt.Rows[0]["OCEANNPCMAILID"].ToString() != null && dt.Rows[0]["OCEANNPCMAILID"].ToString().Trim() != "") ? dt.Rows[0]["OCEANNPCMAILID"].ToString() : NPCEmail;
                        Emailid = Emailid + "," + help;
                        SendMail(Emailid, mbody, msubject, "Overweight/CBM threshold quote request", false, bytes, DUIModels.QuotationNumber);
                    }
                }
                catch
                {
                    //
                }
            }
            else
            {
                try
                {
                    DataTable dt = mFactory.GetNPCdetails(Convert.ToInt64(mDBEntity.OCOUNTRYID));

                    if ((dt.Rows.Count > 0) && (mDBEntity.TOTALGROSSWEIGHT > Convert.ToDecimal(dt.Rows[0]["AIRMAXWEIGHT"].ToString()) || mDBEntity.TOTALCBM > Convert.ToDecimal(dt.Rows[0]["AIRCBM"].ToString())))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                            memoryStream.Close();
                        }
                        string Emailid = (dt.Rows[0]["AIRNPCMAILID"].ToString() != null && dt.Rows[0]["AIRNPCMAILID"].ToString().Trim() != "") ? dt.Rows[0]["AIRNPCMAILID"].ToString() : NPCEmail;
                        Emailid = Emailid + "," + help;
                        SendMail(Emailid, mbody, msubject, "Overweight/CBM threshold quote request", false, bytes, DUIModels.QuotationNumber);
                    }
                }
                catch
                {
                    //
                }
            }
        }

        [AllowAnonymous]
        public void SendFocNpcmail(string QuotationNumber, string CountryId)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string msubject = "ShipA Freight- Request for Quote submitted in NPC Flow :" + QuotationNumber + EnvironmentName;
            string NPCEmail = System.Configuration.ConfigurationManager.AppSettings["NPCEmail"];
            string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string npcEmail = mFactory.GetNPCdetails_Focis(Convert.ToInt64(CountryId));
            string userEmail = mFactory.GetUserEmailByQuotationNumber(QuotationNumber);
            NPCEmail = string.IsNullOrEmpty(npcEmail) ? NPCEmail : npcEmail;
            NPCEmail = NPCEmail + ";" + help;
            string mbody = string.Empty;
            try
            {
                string mapPath = Server.MapPath("~/App_Data/NPC helpdesk quote-focis.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { mbody = reader.ReadToEnd(); }
                mbody = mbody.Replace("{usermail}", userEmail);
                mbody = mbody.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
                mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                SendMail(NPCEmail, mbody, msubject, "Request for Quote submitted in NPC Flow-FOCIS", false, null, QuotationNumber);
            }
            catch
            {
                //
            }
        }

        [AllowAnonymous]
        public void SendFocNpcToGSSCmail(string QuotationNumber, string Type)
        {
            JobBookingDbFactory mFactoryJob = new JobBookingDbFactory();
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string NPCEmail = System.Configuration.ConfigurationManager.AppSettings["NPCEmail"] + "," + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string msubject = "ShipA Freight- Request for Quote submitted in NPC Flow :" + QuotationNumber + EnvironmentName;
            string recipientsList = mFactoryJob.GetEmailList("SSPGSSC");
            recipientsList = string.IsNullOrEmpty(recipientsList) ? NPCEmail : recipientsList;
            string mbody = string.Empty;
            string body = "A Request for Quote " + QuotationNumber + " is sent to GSSC from " + Type + " NPC and available.";
            try
            {
                string mapPath = Server.MapPath("~/App_Data/FocNpcToGSSCmail.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { mbody = reader.ReadToEnd(); }
                mbody = mbody.Replace("{message}", body);
                mbody = mbody.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
                mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                SendMail(recipientsList, mbody, msubject, "Request for Quote submitted in NPC Flow-NpcToGSSCmail-FOCIS", false, null, QuotationNumber);
            }
            catch
            {
            }
        }

        public JsonResult TradeFinance(string TradeType, string QuotationNo, int QuoteId)
        {
            string Emailids = "";
            string name = HttpContext.User.Identity.Name;
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            Emailids = System.Configuration.ConfigurationManager.AppSettings["TradeFinanceEmail"];
            string[] ids = Emailids.Split(',');
            foreach (var item in ids)
            {
                message.To.Add(new MailAddress(item));
            }
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Trade Finance" + "(Quote No #" + QuotationNo + " )" + EnvironmentName;
            message.IsBodyHtml = true;
            string mbody = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/TradeFinance.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{Message}", "<p>Quote No:" + QuotationNo + " " + "customer interested on trade finance for" + " " + TradeType + "</p>");
            mbody = mbody.Replace("{user}", "");
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = mbody;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            try
            {
                string base64 = AESEncrytDecry.EncryptStringAES(mbody);
                var fbytes = System.Convert.FromBase64String(Convert.ToString(base64));
                JobBookingDbFactory JBD = new JobBookingDbFactory();
                JBD.TradeFinanceSave(name, "YES", TradeType, fbytes, "noreply@shipafreight.com", "AlGupta@agility.com", 0, QuoteId);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public void Assignvalues(QuotationPreviewModel mUIModelPreview)
        {
            if (!ReferenceEquals(mUIModelPreview, null))
            {
                foreach (var mShipment in mUIModelPreview.ShipmentItems)
                {
                    cargodetails = cargodetails +
                                      "<tr>" +
                                      "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                      "<span class='inner-detail-head'" + ">Package Type</span><br>" +
                                      "<span class='inner-detail-cont'" + ">" + mShipment.PackageTypeName + "</span></td>" +
                                      "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                      "<span class='inner-detail-head'" + ">Quantity in Pieces</span><br>" +
                                      "<span class='inner-detail-cont'" + "> " + mShipment.Quantity.ToString() + "</span></td> </tr>";
                }
                if (mUIModelPreview.MovementTypeName == "Door to door" || mUIModelPreview.MovementTypeName == "Door to Door")
                {
                    mPlaceOfReceipt = string.Format("{0} {1}", mUIModelPreview.OriginPlaceName, mUIModelPreview.OriginSubDivisionCode != null ? ", " + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0} {1}", mUIModelPreview.DestinationPlaceName, mUIModelPreview.DestinationSubDivisionCode != null ? ", " + mUIModelPreview.DestinationSubDivisionCode : "");
                    mPRLableName = "Origin City"; mPDLableName = "Destination City";
                }
                else if (mUIModelPreview.MovementTypeName == "Door to port")
                {
                    mPlaceOfReceipt = string.Format("{0} {1}", mUIModelPreview.OriginPlaceName, mUIModelPreview.OriginSubDivisionCode != null ? ", " + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0}, {1}", mUIModelPreview.DestinationPortCode, mUIModelPreview.DestinationPortName);
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination AirPort";
                    }
                    else
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Port";
                    }
                }
                else if (mUIModelPreview.MovementTypeName == "Port to port" || mUIModelPreview.MovementTypeName == "Port to Port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", mUIModelPreview.OriginPortCode, mUIModelPreview.OriginPortName);
                    mPlaceOfDelivery = string.Format("{0}, {1}", mUIModelPreview.DestinationPortCode, mUIModelPreview.DestinationPortName);
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin AirPort"; mPDLableName = "Destination AirPort";
                    }
                    else
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination Port";
                    }
                }
                else if (mUIModelPreview.MovementTypeName == "Port to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", mUIModelPreview.OriginPortCode, mUIModelPreview.OriginPortName);
                    mPlaceOfDelivery = string.Format("{0} {1}", mUIModelPreview.DestinationPlaceName, mUIModelPreview.DestinationSubDivisionCode != null ? ", " + mUIModelPreview.DestinationSubDivisionCode : "");
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin AirPort"; mPDLableName = "Destination City";
                    }
                    else
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination City";
                    }
                }
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public void Download(int id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id);
                DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                PreferredCurrencyId = DUIModels.PreferredCurrencyId;
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                    memoryStream.Close();
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "inline; filename=" + DUIModels.QuotationNumber + ".pdf");
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public async Task<ActionResult> Index(int? id, string col = "")
        {
            TempData["GQuote"] = null;
            TempData["Quote"] = null;
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            ViewBag.uiCulture = uiCulture.ToLower();
            QuotationModel mUIModel = new QuotationModel();
            MDMDataFactory mFactoryValue = new MDMDataFactory();
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if (id.HasValue)
            {
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id.Value));
                if (count > 0)
                {
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id.Value);
                    mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                    if (uiCulture != "en")
                    {
                        await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<QuotationModel>(uiCulture, mUIModel));
                    }
                    Session["ModifyQuote"] = mUIModel;
                    if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
                        mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
                    else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
                        mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
                    else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
                        mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
                    else
                        mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
                    if (mUIModel.CargoValue != 0)
                    {
                        mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
                    }
                    else
                    {
                        mUIModel.CurrencyMode = string.Empty;
                    }
                    mUIModel.OriginCountryName = mUIModel.OCountryName;
                    mUIModel.DestinationCountryName = mUIModel.DCountryName;
                    mUIModel.OriginCountryId = mUIModel.OCountryId;
                    mUIModel.DestinationCountryId = mUIModel.DCountryId;
                    int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
                    mUIModel.PreferredDecimal = preferreddecimalPoint;
                    int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
                    mUIModel.CargoDecimal = cargodecimalPoint;
                    string PreferredCurrencyCode = mUIModel.PreferredCurrencyId;

                    DataTable mFactoryEntityPrederred = mFactoryValue.FindCurrencyData(PreferredCurrencyCode);

                    ViewBag.PREFERREDCURRENCY = mUIModel.PreferredCurrencyId + "-" + mFactoryEntityPrederred.Rows[0].ItemArray[1];

                    if (mUIModel.IsInsuranceRequired != false)
                    {
                        string CargoValueCurrencyCode = mUIModel.CargoValueCurrencyId;

                        DataTable mFactoryEntityCargo = mFactoryValue.FindCurrencyData(CargoValueCurrencyCode);

                        ViewBag.CargoValueCurrency = mUIModel.CargoValueCurrencyId + "-" + mFactoryEntityCargo.Rows[0].ItemArray[1];
                    }
                    DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();
                            if (ViewBag.WeightUOMCode == "KG")
                            {
                                ViewBag.WeightUOMName = "KG";
                                ViewBag.WeightUOM = 95;
                                ViewBag.MaxWeightPerPiece = 5000;
                            }
                            else if (ViewBag.WeightUOMCode == "LB")
                            {
                                ViewBag.WeightUOMName = "LB";
                                ViewBag.WeightUOM = 96;
                                ViewBag.MaxWeightPerPiece = 11024;
                            }
                            else if (ViewBag.WeightUOMCode == "TON")
                            {
                                ViewBag.WeightUOMName = "TON";
                                ViewBag.WeightUOM = 97;
                                ViewBag.MaxWeightPerPiece = 5;
                            }
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Quotation");
                }
            }
            else
            {
                ViewBag.LengthUOMCode = "CM";
                ViewBag.LengthUOMName = "CENTIMETER";
                ViewBag.LengthUOM = 98;
                ViewBag.WeightUOMCode = "KG";
                ViewBag.WeightUOMName = "KG";
                ViewBag.WeightUOM = 95;
                ViewBag.MaxWeightPerPiece = 5000;
                mUIModel.PreferredCurrencyId = "USD";
                mUIModel.PreferredDecimal = 2;
                mUIModel.CargoValueCurrencyId = "USD";
                mUIModel.CargoDecimal = 2;
                ViewBag.CargoValueCurrency = "US Dollar";
                ViewBag.PREFERREDCURRENCY = "US Dollar";
                if (Request.IsAuthenticated)
                {
                    DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetQuotationTemplates(HttpContext.User.Identity.Name);
                    mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                    mUIModel.PreferredCurrencyId = "USD";
                    mUIModel.PreferredDecimal = 2;
                    mUIModel.CargoValueCurrencyId = "USD";
                    mUIModel.CargoDecimal = 2;
                    mUIModel.MovementTypeName = "Door to door";
                    mUIModel.ProductId = ProductEnum.OceanFreight;
                    mUIModel.PackageTypes = PackageTypeEnum.LCL;
                    mUIModel.DensityRatio = 1000;
                    mUIModel.MovementTypeId = (MovementTypeEnum)1;
                    mUIModel.PartyType = PartyTypeEnum.EXPORTING;
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        mUIModel.PreferredCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                        int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
                        mUIModel.PreferredDecimal = preferreddecimalPoint;
                        ViewBag.PREFERREDCURRENCY = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                        mUIModel.CargoValueCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString();
                        int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
                        mUIModel.CargoDecimal = cargodecimalPoint;
                        ViewBag.CargoValueCurrency = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                        ViewBag.LengthUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString()) ? "CM" : ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString(); //ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString();
                        if (ViewBag.LengthUOMCode == "CM")
                        {
                            ViewBag.LengthUOMName = "CENTIMETER";
                            ViewBag.LengthUOM = 98;
                        }
                        else if (ViewBag.LengthUOMCode == "IN")
                        {
                            ViewBag.LengthUOMName = "INCH";
                            ViewBag.LengthUOM = 99;
                        }
                        else if (ViewBag.LengthUOMCode == "M")
                        {
                            ViewBag.LengthUOMName = "METER";
                            ViewBag.LengthUOM = 100;
                        }
                        else if (ViewBag.LengthUOMCode == "MM")
                        {
                            ViewBag.LengthUOMName = "MILLIMETER";
                            ViewBag.LengthUOM = 101;
                        }
                        ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();

                        if (ViewBag.WeightUOMCode == "KG")
                        {
                            ViewBag.WeightUOMName = "KG";
                            ViewBag.WeightUOM = 95;
                            ViewBag.MaxWeightPerPiece = 5000;
                        }
                        else if (ViewBag.WeightUOMCode == "LB")
                        {
                            ViewBag.WeightUOMName = "LB";
                            ViewBag.WeightUOM = 96;
                            ViewBag.MaxWeightPerPiece = 11024;
                        }
                        else if (ViewBag.WeightUOMCode == "TON")
                        {
                            ViewBag.WeightUOMName = "TON";
                            ViewBag.WeightUOM = 97;
                            ViewBag.MaxWeightPerPiece = 5;
                        }
                    }
                }
            }
            mUIModel.IsOfferApplicable = false;
            mUIModel.OfferCode = string.Empty;
            if (mUIModel.ShipmentItems == null)
            {
                mUIModel.ShipmentItems = null;
            }
           // IList<IncoTermsEntity> incoterms = mFactoryValue.GetIncoTerms(string.Empty, string.Empty);
            long movementTypeId = (long)mUIModel.MovementTypeId;
            IList<IncoTermsEntity> incoterms = mFactory.GetIncoTermsByMovementType(movementTypeId, (long)mUIModel.PartyType);
            ViewBag.incoterms = incoterms;
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            if (mUIModel.QUOTETYPE == "QAMZ")
            {
                mUIModel.DestinationCountryId = 232;
                mUIModel.DestinationCountryName = "United States";
                mUIModel.AmazonPlaceId = mUIModel.AMAZONFCCODE;
                mUIModel.AmazonPlaceName = mUIModel.AMAZONFCCODE + " - " + mUIModel.DestinationPlaceName;
                DataTable DTAMZINFO = mFactory.GETAMZINFO(mUIModel.AMAZONFCCODE);
                if (DTAMZINFO.Rows.Count > 0)
                {
                    mUIModel.DestinationStateName = DTAMZINFO.Rows[0]["SUBDIVISONNAME"].ToString();
                    mUIModel.DestinationStateId = Convert.ToDecimal(DTAMZINFO.Rows[0]["SUBDIVISIONID"]);
                }

                if (mUIModel.LABELLINGBY != null && mUIModel.LABELLINGBY == "Myself")
                    mUIModel.LabellingId = PalletEnum.Myself;
                else if (mUIModel.LABELLINGBY != null)
                    mUIModel.LabellingId = PalletEnum.Shipafreight;

                if (mUIModel.PALLETIZINGBY != null && mUIModel.PALLETIZINGBY == "Myself")
                    mUIModel.PalletId = PalletEnum.Myself;
                else if (mUIModel.PALLETIZINGBY != null)
                    mUIModel.PalletId = PalletEnum.Shipafreight;

                return View("Create_Amazon", mUIModel);
            }
            else
            {
                ModelState.Remove("AmazonSupplierID");
                return View("Create", mUIModel);
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public async Task<ActionResult> IndexPage(int? id, string col = "")
        {
            TempData["GQuote"] = null;
            TempData["Quote"] = null;
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            ViewBag.uiCulture = uiCulture.ToLower();
            QuotationModel mUIModel = new QuotationModel();
            MDMDataFactory mFactoryValue = new MDMDataFactory();
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if (id.HasValue)
            {
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id.Value));
                if (count > 0)
                {
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id.Value);
                    mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                    if (uiCulture != "en")
                    {
                        await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<QuotationModel>(uiCulture, mUIModel));
                    }
                    Session["ModifyQuote"] = mUIModel;
                    if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
                        mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
                    else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
                        mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
                    else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
                        mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
                    else
                        mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
                    if (mUIModel.CargoValue != 0)
                    {
                        mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
                    }
                    else
                    {
                        mUIModel.CurrencyMode = string.Empty;
                    }
                    mUIModel.OriginCountryName = mUIModel.OCountryName;
                    mUIModel.DestinationCountryName = mUIModel.DCountryName;
                    mUIModel.OriginCountryId = mUIModel.OCountryId;
                    mUIModel.DestinationCountryId = mUIModel.DCountryId;
                    int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
                    mUIModel.PreferredDecimal = preferreddecimalPoint;
                    int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
                    mUIModel.CargoDecimal = cargodecimalPoint;
                    string PreferredCurrencyCode = mUIModel.PreferredCurrencyId;

                    DataTable mFactoryEntityPrederred = mFactoryValue.FindCurrencyData(PreferredCurrencyCode);

                    ViewBag.PREFERREDCURRENCY = mUIModel.PreferredCurrencyId + "-" + mFactoryEntityPrederred.Rows[0].ItemArray[1];

                    if (mUIModel.IsInsuranceRequired != false)
                    {
                        string CargoValueCurrencyCode = mUIModel.CargoValueCurrencyId;

                        DataTable mFactoryEntityCargo = mFactoryValue.FindCurrencyData(CargoValueCurrencyCode);

                        ViewBag.CargoValueCurrency = mUIModel.CargoValueCurrencyId + "-" + mFactoryEntityCargo.Rows[0].ItemArray[1];
                    }
                    DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();
                            if (ViewBag.WeightUOMCode == "KG")
                            {
                                ViewBag.WeightUOMName = "KG";
                                ViewBag.WeightUOM = 95;
                                ViewBag.MaxWeightPerPiece = 5000;
                            }
                            else if (ViewBag.WeightUOMCode == "LB")
                            {
                                ViewBag.WeightUOMName = "LB";
                                ViewBag.WeightUOM = 96;
                                ViewBag.MaxWeightPerPiece = 11024;
                            }
                            else if (ViewBag.WeightUOMCode == "TON")
                            {
                                ViewBag.WeightUOMName = "TON";
                                ViewBag.WeightUOM = 97;
                                ViewBag.MaxWeightPerPiece = 5;
                            }
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Quotation");
                }
            }
            else
            {
                ViewBag.LengthUOMCode = "CM";
                ViewBag.LengthUOMName = "CENTIMETER";
                ViewBag.LengthUOM = 98;
                ViewBag.WeightUOMCode = "KG";
                ViewBag.WeightUOMName = "KG";
                ViewBag.WeightUOM = 95;
                ViewBag.MaxWeightPerPiece = 5000;
                mUIModel.PreferredCurrencyId = "USD";
                mUIModel.PreferredDecimal = 2;
                mUIModel.CargoValueCurrencyId = "USD";
                mUIModel.CargoDecimal = 2;
                ViewBag.CargoValueCurrency = "US Dollar";
                ViewBag.PREFERREDCURRENCY = "US Dollar";
                if (Request.IsAuthenticated)
                {
                    DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetQuotationTemplates(HttpContext.User.Identity.Name);
                    mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                    mUIModel.PreferredCurrencyId = "USD";
                    mUIModel.PreferredDecimal = 2;
                    mUIModel.CargoValueCurrencyId = "USD";
                    mUIModel.CargoDecimal = 2;
                    mUIModel.MovementTypeName = "Door to door";
                    mUIModel.ProductId = ProductEnum.OceanFreight;
                    mUIModel.PackageTypes = PackageTypeEnum.LCL;
                    mUIModel.DensityRatio = 1000;
                    mUIModel.MovementTypeId = (MovementTypeEnum)1;
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        mUIModel.PreferredCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                        int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
                        mUIModel.PreferredDecimal = preferreddecimalPoint;
                        ViewBag.PREFERREDCURRENCY = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                        mUIModel.CargoValueCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString();
                        int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
                        mUIModel.CargoDecimal = cargodecimalPoint;
                        ViewBag.CargoValueCurrency = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                        ViewBag.LengthUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString()) ? "CM" : ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString(); //ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString();
                        if (ViewBag.LengthUOMCode == "CM")
                        {
                            ViewBag.LengthUOMName = "CENTIMETER";
                            ViewBag.LengthUOM = 98;
                        }
                        else if (ViewBag.LengthUOMCode == "IN")
                        {
                            ViewBag.LengthUOMName = "INCH";
                            ViewBag.LengthUOM = 99;
                        }
                        else if (ViewBag.LengthUOMCode == "M")
                        {
                            ViewBag.LengthUOMName = "METER";
                            ViewBag.LengthUOM = 100;
                        }
                        else if (ViewBag.LengthUOMCode == "MM")
                        {
                            ViewBag.LengthUOMName = "MILLIMETER";
                            ViewBag.LengthUOM = 101;
                        }
                        ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();

                        if (ViewBag.WeightUOMCode == "KG")
                        {
                            ViewBag.WeightUOMName = "KG";
                            ViewBag.WeightUOM = 95;
                            ViewBag.MaxWeightPerPiece = 5000;
                        }
                        else if (ViewBag.WeightUOMCode == "LB")
                        {
                            ViewBag.WeightUOMName = "LB";
                            ViewBag.WeightUOM = 96;
                            ViewBag.MaxWeightPerPiece = 11024;
                        }
                        else if (ViewBag.WeightUOMCode == "TON")
                        {
                            ViewBag.WeightUOMName = "TON";
                            ViewBag.WeightUOM = 97;
                            ViewBag.MaxWeightPerPiece = 5;
                        }
                    }
                }
            }
            mUIModel.IsOfferApplicable = false;
            mUIModel.OfferCode = string.Empty;
            if (mUIModel.ShipmentItems == null)
            {
                mUIModel.ShipmentItems = null;
            }
            IList<IncoTermsEntity> incoterms = mFactoryValue.GetIncoTerms(string.Empty, string.Empty);
            ViewBag.incoterms = incoterms;
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            if (mUIModel.QUOTETYPE == "QAMZ")
            {
                mUIModel.DestinationCountryId = 232;
                mUIModel.DestinationCountryName = "United States";
                mUIModel.AmazonPlaceId = mUIModel.AMAZONFCCODE;
                mUIModel.AmazonPlaceName = mUIModel.AMAZONFCCODE + " - " + mUIModel.DestinationPlaceName;
                DataTable DTAMZINFO = mFactory.GETAMZINFO(mUIModel.AMAZONFCCODE);
                if (DTAMZINFO.Rows.Count > 0)
                {
                    mUIModel.DestinationStateName = DTAMZINFO.Rows[0]["SUBDIVISONNAME"].ToString();
                    mUIModel.DestinationStateId = Convert.ToDecimal(DTAMZINFO.Rows[0]["SUBDIVISIONID"]);
                }

                if (mUIModel.LABELLINGBY != null && mUIModel.LABELLINGBY == "Myself")
                    mUIModel.LabellingId = PalletEnum.Myself;
                else if (mUIModel.LABELLINGBY != null)
                    mUIModel.LabellingId = PalletEnum.Shipafreight;

                if (mUIModel.PALLETIZINGBY != null && mUIModel.PALLETIZINGBY == "Myself")
                    mUIModel.PalletId = PalletEnum.Myself;
                else if (mUIModel.PALLETIZINGBY != null)
                    mUIModel.PalletId = PalletEnum.Shipafreight;

                return View("Create_Amazon", mUIModel);
            }
            else
            {
                ModelState.Remove("AmazonSupplierID");
                return View("CreatePage", mUIModel);
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public ActionResult TemplateIndex(int? id)
        {
            if (!id.HasValue)
                id = Convert.ToInt32((Session["TemplateIndexid"]).ToString());

            TempData["GQuote"] = null;
            TempData["Quote"] = null;
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            QuotationModel mUIModel = new QuotationModel();
            MDMDataFactory mFactoryValue = new MDMDataFactory();
            if (id.HasValue)
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetByTemplateId(id.Value);
                mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
                    mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
                else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
                    mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
                else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
                    mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
                else
                    mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
                if (mUIModel.CargoValue != 0)
                {
                    mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
                }
                else
                {
                    mUIModel.CurrencyMode = string.Empty;
                }
                if (mUIModel.RequestType == "OFFER")
                {
                    ViewBag.RequestType = "OFFER";
                }
                else
                {
                    mUIModel.IsOfferApplicable = false;
                    mUIModel.OfferCode = string.Empty;
                }
                DataSet ds = mFactory.GetCountryID((mUIModel.OCountryCode).Trim());
                if (ds.Tables.Count > 0
                    && ds.Tables[0].Rows.Count > 0)
                {
                    mUIModel.OriginCountryName = ds.Tables[0].Rows[0]["NAME"].ToString();
                    mUIModel.OriginCountryId = Convert.ToDecimal(ds.Tables[0].Rows[0]["COUNTRYID"].ToString());

                }

                DataSet dsd = mFactory.GetCountryID((mUIModel.DCountryCode).Trim());
                if (dsd.Tables.Count > 0 && dsd.Tables[0].Rows.Count > 0)
                {
                    mUIModel.DestinationCountryName = dsd.Tables[0].Rows[0]["NAME"].ToString();
                    mUIModel.DestinationCountryId = Convert.ToDecimal(dsd.Tables[0].Rows[0]["COUNTRYID"].ToString());
                }

                string PreferredCurrencyCode = mUIModel.PreferredCurrencyId;
                DataTable mFactoryEntityPrederred = mFactoryValue.FindCurrencyData(PreferredCurrencyCode);
                ViewBag.PREFERREDCURRENCY = mUIModel.PreferredCurrencyId + "-" + mFactoryEntityPrederred.Rows[0].ItemArray[1];

                if (mUIModel.IsInsuranceRequired)
                {
                    string CargoValueCurrencyCode = mUIModel.CargoValueCurrencyId;
                    DataTable mFactoryEntityCargo = mFactoryValue.FindCurrencyData(CargoValueCurrencyCode);
                    ViewBag.CargoValueCurrency = mUIModel.CargoValueCurrencyId + "-" + mFactoryEntityCargo.Rows[0].ItemArray[1];
                }
            }
            //IList<IncoTermsEntity> incoterms = mFactoryValue.GetIncoTerms(string.Empty, string.Empty);
            QuotationDBFactory mQFactory = new QuotationDBFactory();
            long movementTypeId = (long)mUIModel.MovementTypeId;
            IList<IncoTermsEntity> incoterms = mQFactory.GetIncoTermsByMovementType(movementTypeId, (long)mUIModel.PartyType);
            ViewBag.incoterms = incoterms;
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            mUIModel.ShipmentDescription = mUIModel.ShipmentDescription.Replace("\r\n", " ");
            return View("Create", mUIModel);
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public ActionResult OfferIndex(int? id)
        {
            TempData["GQuote"] = null;
            TempData["Quote"] = null;
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";

            QuotationDBFactory mFactory = new QuotationDBFactory();
            DBFactory.Entities.QuoteOffersEntity mDBEntity = mFactory.GetOfferbyId(id.Value);
            var mUIModels = Mapper.Map<DBFactory.Entities.QuoteOffersEntity, QuoteOffersModel>(mDBEntity);

            QuotationModel mUIModel = new QuotationModel();
            ViewBag.RequestType = "OFFER";
            ViewBag.LengthUOMCode = "CM";
            ViewBag.LengthUOMName = "CENTIMETER";
            ViewBag.LengthUOM = 98;
            ViewBag.WeightUOMCode = "KG";
            ViewBag.WeightUOMName = "KG";
            ViewBag.WeightUOM = 95;
            ViewBag.MaxWeightPerPiece = 5000;
            mUIModel.PreferredCurrencyId = "USD";
            mUIModel.CargoValueCurrencyId = "USD";
            ViewBag.CargoValueCurrency = "US Dollar";
            ViewBag.PREFERREDCURRENCY = "US Dollar";
            mUIModel.OriginCountryName = mUIModels.OCountryName;
            mUIModel.DestinationCountryName = mUIModels.DCountryName;
            mUIModel.OriginCountryId = mUIModels.OCountryId;
            mUIModel.DestinationCountryId = mUIModels.DCountryId;
            if (Request.IsAuthenticated)
            {
                DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
                mUIModel.PreferredCurrencyId = "USD";
                mUIModel.CargoValueCurrencyId = "USD";
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    mUIModel.PreferredCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                    ViewBag.PREFERREDCURRENCY = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                    mUIModel.CargoValueCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString();
                    ViewBag.CargoValueCurrency = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
                    ViewBag.LengthUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString()) ? "CM" : ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString(); //ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString();
                    if (ViewBag.LengthUOMCode == "CM")
                    {
                        ViewBag.LengthUOMName = "CENTIMETER";
                        ViewBag.LengthUOM = 98;
                    }
                    else if (ViewBag.LengthUOMCode == "IN")
                    {
                        ViewBag.LengthUOMName = "INCH";
                        ViewBag.LengthUOM = 99;
                    }
                    else if (ViewBag.LengthUOMCode == "M")
                    {
                        ViewBag.LengthUOMName = "METER";
                        ViewBag.LengthUOM = 100;
                    }
                    else if (ViewBag.LengthUOMCode == "MM")
                    {
                        ViewBag.LengthUOMName = "MILLIMETER";
                        ViewBag.LengthUOM = 101;
                    }
                    ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();

                    if (ViewBag.WeightUOMCode == "KG")
                    {
                        ViewBag.WeightUOMName = "KG";
                        ViewBag.WeightUOM = 95;
                        ViewBag.MaxWeightPerPiece = 5000;
                    }
                    else if (ViewBag.WeightUOMCode == "LB")
                    {
                        ViewBag.WeightUOMName = "LB";
                        ViewBag.WeightUOM = 96;
                        ViewBag.MaxWeightPerPiece = 11024;
                    }
                    else if (ViewBag.WeightUOMCode == "TON")
                    {
                        ViewBag.WeightUOMName = "TON";
                        ViewBag.WeightUOM = 97;
                        ViewBag.MaxWeightPerPiece = 5;
                    }
                }
            }

            mUIModel.MovementTypeName = mUIModels.MovementTypeName;
            mUIModel.ProductId = (ProductEnum)mUIModels.ProductId;
            mUIModel.DensityRatio = Convert.ToDecimal(mUIModels.DensityRatio);
            mUIModel.MovementTypeId = (MovementTypeEnum)2;
            mUIModel.OriginPortId = Convert.ToInt64(mUIModels.OriginPortId);
            mUIModel.OriginPortCode = mUIModels.OriginPortCode;
            mUIModel.OriginPortName = mUIModels.OriginPortName;
            mUIModel.OCountryId = mUIModels.OCountryId;
            mUIModel.OCountryCode = mUIModels.OCountryCode;
            mUIModel.OriginsubdivisionCode = mUIModels.OriginsubdivisionCode;
            mUIModel.DestinationPortid = Convert.ToInt64(mUIModels.DestinationPortId);
            mUIModel.DestinationPortCode = mUIModels.DestinationPortCode;
            mUIModel.DestinationPortName = mUIModels.DestinationPortName;
            mUIModel.DCountryId = mUIModels.DCountryId;
            mUIModel.DCountryCode = mUIModels.DCountryCode;
            mUIModel.DestinationsubdivisionCode = mUIModels.DestinationSubDivisionCode;
            mUIModel.IsOfferApplicable = true;
            mUIModel.OfferCode = mUIModels.TEMPLATENAME;
            if (mUIModel.ShipmentItems == null)
            {
                mUIModel.ShipmentItems = null;
            }
            MDMDataFactory mFactoryValue = new MDMDataFactory();
            IList<IncoTermsEntity> incoterms = mFactoryValue.GetIncoTerms(string.Empty, string.Empty);
            ViewBag.incoterms = incoterms;
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("Create", mUIModel);
        }

        public void TemplateIndexhidden(int? id)
        {
            Session["TemplateIndexid"] = id;
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public ActionResult CargoCheck(int? id, string col, int? ChinaCheck, string language)
        {
            FormsAuthentication.SignOut();
            string userId = col;
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            QuotationModel mUIModel = new QuotationModel();
            MDMDataFactory mFactoryValue = new MDMDataFactory();
            UserDBFactory UD = new UserDBFactory();
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            if (TempData["ChangeLanguage"] != null)
            {
                language = TempData["ChangeLanguage"].ToString();
            }
            else
            {
                language = uiCulture;
            }
            if (ChinaCheck != 0)
            {
                if (ChinaCheck == 48)
                {
                    //new Languages().SetLanguage("zh-hk");
                    if (TempData["ChangeLanguage"] != null)
                    {
                        new Languages().SetLanguage(TempData["ChangeLanguage"].ToString());
                    }
                    else
                    {
                        new Languages().SetLanguage("zh-hk");
                    }
                }
                else
                {
                    //new Languages().SetLanguage("en");
                    new Languages().SetLanguage(language.ToLower());
                }
            }
            else
            {
                //new Languages().SetLanguage("en");
                new Languages().SetLanguage(language.ToLower());
            }

            if (id.HasValue)
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
                if (count > 0)
                {

                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id.Value);
                    mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                    if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
                        mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
                    else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
                        mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
                    else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
                        mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
                    else
                        mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
                    if (mUIModel.CargoValue != 0)
                    {
                        mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
                    }
                    else
                    {
                        mUIModel.CurrencyMode = string.Empty;
                    }
                    mUIModel.OriginCountryName = mUIModel.OCountryName;
                    mUIModel.DestinationCountryName = mUIModel.DCountryName;
                    mUIModel.OriginCountryId = mUIModel.OCountryId;
                    mUIModel.DestinationCountryId = mUIModel.DCountryId;
                    int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
                    mUIModel.PreferredDecimal = preferreddecimalPoint;
                    int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
                    mUIModel.CargoDecimal = cargodecimalPoint;
                    string PreferredCurrencyCode = mUIModel.PreferredCurrencyId;

                    DataTable mFactoryEntityPrederred = mFactoryValue.FindCurrencyData(PreferredCurrencyCode);

                    ViewBag.PREFERREDCURRENCY = mUIModel.PreferredCurrencyId + "-" + mFactoryEntityPrederred.Rows[0].ItemArray[1];

                    if (mUIModel.IsInsuranceRequired != false)
                    {
                        string CargoValueCurrencyCode = mUIModel.CargoValueCurrencyId;

                        DataTable mFactoryEntityCargo = mFactoryValue.FindCurrencyData(CargoValueCurrencyCode);

                        ViewBag.CargoValueCurrency = mUIModel.CargoValueCurrencyId + "-" + mFactoryEntityCargo.Rows[0].ItemArray[1];
                    }
                    DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();
                            if (ViewBag.WeightUOMCode == "KG")
                            {
                                ViewBag.WeightUOMName = "KG";
                                ViewBag.WeightUOM = 95;
                                ViewBag.MaxWeightPerPiece = 5000;
                            }
                            else if (ViewBag.WeightUOMCode == "LB")
                            {
                                ViewBag.WeightUOMName = "LB";
                                ViewBag.WeightUOM = 96;
                                ViewBag.MaxWeightPerPiece = 11024;
                            }
                            else if (ViewBag.WeightUOMCode == "TON")
                            {
                                ViewBag.WeightUOMName = "TON";
                                ViewBag.WeightUOM = 97;
                                ViewBag.MaxWeightPerPiece = 5;
                            }
                        }
                    }
                    else
                    {
                        ViewBag.WeightUOMName = "KG";
                        ViewBag.WeightUOM = 95;
                        ViewBag.MaxWeightPerPiece = 5000;
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Quotation");
                }
            }
            mUIModel.IsOfferApplicable = false;
            mUIModel.OfferCode = string.Empty;
            if (mUIModel.ShipmentItems == null)
            {
                mUIModel.ShipmentItems = null;
            }
            //IList<IncoTermsEntity> incoterms = mFactoryValue.GetIncoTerms(string.Empty, string.Empty);
            QuotationDBFactory mQFactory = new QuotationDBFactory();
            long movementTypeId = (long)mUIModel.MovementTypeId;
            IList<IncoTermsEntity> incoterms = mQFactory.GetIncoTermsByMovementType(movementTypeId, (long)mUIModel.PartyType);
            ViewBag.incoterms = incoterms;
            if (mUIModel.INCOTERMID == "0")
            {
                mUIModel.INCOTERMID = "1";
                mUIModel.INCOTERMDESC = "DDP - Delivered Duty Paid";
            }
            ViewBag.dims = mUIModel.CREATEDBY;
            ViewBag.userId = userId;
            UserEntity UE = UD.GetUserFirstAndLastName(mUIModel.CREATEDBY);
            mUIModel.FirstName = UE.FIRSTNAME;
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("Create", mUIModel);
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public async Task<ActionResult> Preview(int? id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            if (!id.HasValue || id.Value == 0)
            {
                ViewBag.NoThanks = "NoThanks";
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
                if (count > 0)
                {
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                    double co2 = Co2EmissionCalculation(mDBEntity);
                    QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                    if (mUIModels.co2emission == 0)
                    {
                        mUIModels.co2emission = co2;
                        mDBEntity.co2emission = Convert.ToDecimal(co2);
                    }
                    if ((Request.IsAuthenticated) && ((mUIModels.HasMissingCharges != 0) || (mUIModels.HasMissingCharges == 0 && mUIModels.GrandTotal == 0))) // RATES NOT AVAILABLE & Zero Quote
                    {
                        int quoteid = Convert.ToInt32(mUIModels.QuotationId);
                        mFactory.RequestQuote(quoteid);
                        SendMailForGSSCQuotes(mUIModels.QuotationNumber, mUIModels.ThresholdQty, Convert.ToInt32(mUIModels.IsHazardousCargo), Convert.ToInt32(mUIModels.HasMissingCharges));
                    }
                    if (!string.IsNullOrEmpty(mUIModels.GSSCReason) && (mUIModels.HasMissingCharges == 0))
                    {
                        mFactory.InsertGSSCREASON(mUIModels.QuotationId, mUIModels.GSSCReason);
                        int usercount = mFactory.CheckUserRestriction(mDBEntity.CREATEDBY.ToUpper() == "GUEST" ? mDBEntity.GUESTEMAIL.ToUpper() : mDBEntity.CREATEDBY.ToUpper());
                        if (usercount == 0)
                            SendMailForMissingChargesGSSCQuotes(mDBEntity);
                    }
                    if (mDBEntity.HASMISSINGCHARGES <= 0)
                        SendmailtoNPC(mDBEntity);

                    return View("Preview", mUIModels);
                }
                else
                {
                    return RedirectToAction("Index", "Quotation");
                }
            }
            else
            {
                string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
                if (count > 0)
                {
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                    int QuoteBooked = mFactory.CheckQuoteBooked(Convert.ToInt32(id));
                    string ProductName = mDBEntity.PRODUCTNAME;
                    if (uiCulture != "en")
                    {
                        await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<QuotationEntity>(uiCulture, mDBEntity));
                        await Task.Run(() => LocalizedConverter.ChargesQuotelanguageconversion<QuotationEntity>(uiCulture, mDBEntity));
                    }
                    if (QuoteBooked == 0)
                        ViewBag.NoThanks = "NoThanks";

                    QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                    if (mUIModels.co2emission == 0)
                    {
                        double co2 = Co2EmissionCalculation(mDBEntity);
                        mUIModels.co2emission = co2;
                    }
                    if (Request.IsAuthenticated)
                    {
                        if ((mUIModels.HasMissingCharges != 0) || (mUIModels.HasMissingCharges == 0 && mUIModels.GrandTotal == 0)) // RATES NOT AVAILABLE & Zero Quote
                        {
                            int quoteid = Convert.ToInt32(mUIModels.QuotationId);
                            mFactory.RequestQuote(quoteid);
                            SendMailForGSSCQuotes(mUIModels.QuotationNumber, mUIModels.ThresholdQty, Convert.ToInt32(mUIModels.IsHazardousCargo), Convert.ToInt32(mUIModels.HasMissingCharges));
                        }
                    }
                    ViewBag.ShowHideTradeDiv = mFactory.showhidetradefinancediv(id);
                    return View("Preview", mUIModels);
                }
                else
                {
                    return RedirectToAction("Index", "Quotation");
                }
            }
        }

        [AllowAnonymous]
        public ActionResult Dimspreview(int? id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if (id.HasValue || id.Value != 0)
            {
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
                if (count > 0)
                {
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));

                    QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                    if (mDBEntity.co2emission == 0)
                    {
                        double co2 = Co2EmissionCalculation(mDBEntity);
                        mUIModels.co2emission = co2;
                        mDBEntity.co2emission = Convert.ToDecimal(co2);

                        if ((mUIModels.HasMissingCharges != 0) || (mUIModels.HasMissingCharges == 0 && mUIModels.GrandTotal == 0)) // RATES NOT AVAILABLE & Zero Quote
                        {
                            int quoteid = Convert.ToInt32(mUIModels.QuotationId);
                            mFactory.RequestQuote(quoteid);
                            SendMailForGSSCQuotes(mUIModels.QuotationNumber, mUIModels.ThresholdQty, Convert.ToInt32(mUIModels.IsHazardousCargo), Convert.ToInt32(mUIModels.HasMissingCharges));
                        }
                        if (!string.IsNullOrEmpty(mUIModels.GSSCReason))
                        {
                            if ((mUIModels.HasMissingCharges == 0))
                                mFactory.InsertGSSCREASON(mUIModels.QuotationId, mUIModels.GSSCReason);

                            int usercount = mFactory.CheckUserRestriction(mDBEntity.CREATEDBY.ToUpper() == "GUEST" ? mDBEntity.GUESTEMAIL.ToUpper() : mDBEntity.CREATEDBY.ToUpper());
                            if (usercount == 0)
                                SendMailForMissingChargesGSSCQuotes(mDBEntity);
                        }
                        if (mDBEntity.HASMISSINGCHARGES <= 0)
                            SendmailtoNPC(mDBEntity);
                    }
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Quotation");
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public ActionResult QuotePreview(int id)//multi carrier purpose preview
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int QuoteBooked = mFactory.CheckQuoteBooked(Convert.ToInt32(id));
            if (QuoteBooked == 0)
                ViewBag.NoThanks = "NoThanks";

            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                if (mUIModels.co2emission == 0)
                {
                    double co2 = Co2EmissionCalculation(mDBEntity);
                    mUIModels.co2emission = co2;
                    mDBEntity.co2emission = Convert.ToDecimal(co2);
                }
                return View("Preview", mUIModels);
            }
            else
            {
                return RedirectToAction("Index", "Quotation");
            }
        }

        [AllowAnonymous]
        [EncryptedActionParameter]
        public async Task<ActionResult> GetQuote(int? id)
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if (id.HasValue || id.Value != 0)
            {
                ViewBag.NoThanks = "NoThanks";
                int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
                if (count > 0)
                {
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                    if ((HttpContext.User.Identity.Name.ToUpper() == mDBEntity.CREATEDBY.ToUpper()) || (mDBEntity.CREATEDBY.ToUpper() == "GUEST" && string.IsNullOrEmpty(HttpContext.User.Identity.Name)))
                    {
                        mDBEntity.LANGUAGEMOVEMENTTYPENAME = mDBEntity.MOVEMENTTYPENAME;
                        if (uiCulture != "en")
                        {
                            await Task.Run(() => LocalizedConverter.GetConversionDataByLanguage<QuotationEntity>(uiCulture, mDBEntity));
                            await Task.Run(() => LocalizedConverter.ChargesQuotelanguageconversion<QuotationEntity>(uiCulture, mDBEntity));
                        }
                        QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                        if (mDBEntity.co2emission == 0)
                        {
                            double co2 = Co2EmissionCalculation(mDBEntity);
                            mUIModels.co2emission = co2;
                            mDBEntity.co2emission = Convert.ToDecimal(co2);

                            //if (!string.IsNullOrEmpty(mUIModels.Transittime))
                            //{
                            //    if (mUIModels.Transittime.ToUpperInvariant() != "NO TRANSMIT TIME AVAILABLE")
                            //    {
                            //        mUIModels.Transittime = TransitTimeCalculationByMovementType(mUIModels.MovementTypeName, mUIModels.Transittime, mUIModels.QuotationId);
                            //    }
                            //}
                            if (Request.IsAuthenticated)
                            {
                                if ((mUIModels.HasMissingCharges != 0) || (mUIModels.HasMissingCharges == 0 && mUIModels.GrandTotal == 0)) // RATES NOT AVAILABLE & Zero Quote
                                {
                                    int quoteid = Convert.ToInt32(mUIModels.QuotationId);
                                    mFactory.RequestQuote(quoteid);
                                    SendMailForGSSCQuotes(mUIModels.QuotationNumber, mUIModels.ThresholdQty, Convert.ToInt32(mUIModels.IsHazardousCargo), Convert.ToInt32(mUIModels.HasMissingCharges));
                                }
                            }
                            else
                            {
                                if (mUIModels.HasMissingCharges == 0 && mUIModels.GrandTotal == 0) // RATES NOT AVAILABLE & Zero Quote -- Guest Quote
                                {
                                    int quoteid = Convert.ToInt32(mUIModels.QuotationId);
                                    mFactory.RequestQuote(quoteid);
                                    SendMailForGSSCQuotes(mUIModels.QuotationNumber, mUIModels.ThresholdQty, Convert.ToInt32(mUIModels.IsHazardousCargo), Convert.ToInt32(mUIModels.HasMissingCharges));
                                }
                            }
                            if (!string.IsNullOrEmpty(mUIModels.GSSCReason))
                            {
                                if ((mUIModels.HasMissingCharges == 0))
                                {
                                    mFactory.InsertGSSCREASON(mUIModels.QuotationId, mUIModels.GSSCReason);
                                }
                                int usercount = mFactory.CheckUserRestriction(mDBEntity.CREATEDBY.ToUpper() == "GUEST" ? mDBEntity.GUESTEMAIL.ToUpper() : mDBEntity.CREATEDBY.ToUpper());
                                if (usercount == 0)
                                {
                                    SendMailForMissingChargesGSSCQuotes(mDBEntity);
                                }
                            }
                            if (mDBEntity.HASMISSINGCHARGES <= 0)
                                SendmailtoNPC(mDBEntity);
                            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
                            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
                        }
                        return View("Preview", mUIModels);

                    }
                    else
                    {
                        return RedirectToAction("Index", "Quotation");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Quotation");
                }
            }
            else { return RedirectToActionPermanent("Quotelist"); }
        }

        [AllowAnonymous]
        public ActionResult DimsNoChange()
        {
            return View("DimsNoChange");
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult SubmitReasonforUnbook(int quotationId, int mquotationId, string reason)
        {
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            _qdb.SubmitReasonforUnbook(quotationId, mquotationId, reason);
            var JsonToReturn = new
            {
                rows = "success",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist = Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) * Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }
            return dist;
        }

        public double Co2EmissionCalculation(QuotationEntity model)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            double objText = 0;
            double OCo2 = 0; double PCo2 = 0; double DCo2 = 0;
            double oplacelat = 0; double oplacelon = 0;
            double oportlat = 0; double oportlon = 0;
            double dplacelat = 0; double dplacelon = 0;
            double dportlat = 0; double dportlon = 0;

            DataSet ds = new DataSet();
            if (model.MOVEMENTTYPENAME == "Port to port")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPORTID, model.DESTINATIONPORTID);
                    DataRow[] orows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (orows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] drows = ds.Tables[0].Select("UNLOCATIONID = '" + model.DESTINATIONPORTID + "'");
                    if (drows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Door to door")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.ORIGINPLACEID != 0) && (model.ORIGINPLACEID != null)) && ((model.DESTINATIONPLACEID != 0) && (model.DESTINATIONPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPLACEID, model.ORIGINPORTID, model.DESTINATIONPLACEID, model.DESTINATIONPORTID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID);
                    if (oprows.Length > 0)
                    {
                        oplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[1]);
                        oplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] orows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (orows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] drows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID);
                    if (drows.Length > 0)
                    {
                        dplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[1]);
                        dplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (dprows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Door to port")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.ORIGINPLACEID != 0) && (model.ORIGINPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPLACEID, model.ORIGINPORTID, model.DESTINATIONPORTID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = '" + model.ORIGINPLACEID + "'");
                    if (oprows.Length > 0)
                    {
                        oplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[1]);
                        oplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (dprows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] prows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (prows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Port to door")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.DESTINATIONPLACEID != 0) && (model.DESTINATIONPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPORTID, model.DESTINATIONPORTID, model.DESTINATIONPLACEID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (oprows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] prows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (prows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID);
                    if (dprows.Length > 0)
                    {
                        dplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[1]);
                        dplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[2]);
                    }
                }
            }

            if (oplacelat != 0 && oplacelon != 0 && oportlat != 0 && oportlon != 0)
            {
                double Origindistance = DistanceTo(oplacelat, oplacelon, oportlat, oportlon, 'K');
                if (!Double.IsNaN(Origindistance) && !Double.IsInfinity(Origindistance))
                {
                    double OADP = 0.2;
                    double Adjusteddistance = Origindistance + (Origindistance * OADP);
                    double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                    double OCWMT = 0.001;
                    double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                    double TonneKM = Adjusteddistance * OcargoweightMT;
                    double KGCo2 = 0.13096;
                    double KGCo2emitted = KGCo2 * TonneKM;
                    OCo2 = KGCo2emitted;//* 0.001  
                }
                else { OCo2 = 0; }
            }
            if (oportlat != 0 && oportlon != 0 && dportlat != 0 && dportlon != 0)
            {
                double Origindistance = DistanceTo(oportlat, oportlon, dportlat, dportlon, 'K');
                if (!Double.IsNaN(Origindistance) && !Double.IsInfinity(Origindistance))
                {
                    double OADP = 0;
                    if (model.PRODUCTNAME.ToUpperInvariant() == "AIR")
                    {
                        OADP = 0.08;
                    }
                    else
                    {
                        OADP = 0.15;
                    }
                    double Adjusteddistance = Origindistance + (Origindistance * OADP);
                    double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                    double OCWMT = 0.001;
                    double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                    double TonneKM = Adjusteddistance * OcargoweightMT;
                    double KGCo2 = 0;
                    if (model.PRODUCTNAME.ToUpperInvariant() == "OCEAN")
                    {
                        KGCo2 = 0.019065;
                    }
                    else { KGCo2 = 0.87249; }

                    double KGCo2emitted = KGCo2 * TonneKM;
                    PCo2 = KGCo2emitted;//* 0.001  
                }
                else { PCo2 = 0; }
            }
            if (dportlat != 0 && dportlon != 0 && dplacelat != 0 && dplacelon != 0)
            {
                double Origindistance = DistanceTo(dportlat, dportlon, dplacelat, dplacelon, 'K');
                if (!Double.IsNaN(Origindistance) && !Double.IsInfinity(Origindistance))
                {
                    double OADP = 0.2;
                    double Adjusteddistance = Origindistance + (Origindistance * OADP);
                    double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                    double OCWMT = 0.001;
                    double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                    double TonneKM = Adjusteddistance * OcargoweightMT;
                    double KGCo2 = 0.13096;
                    double KGCo2emitted = KGCo2 * TonneKM;
                    DCo2 = KGCo2emitted;
                }
                else { DCo2 = 0; }
            }
            objText = OCo2 + PCo2 + DCo2;
            mFactory.UpdateCo2(model.QUOTATIONID, objText);

            return objText;
        }

        [Authorize]
        public ActionResult SaveQuoteForLater(int id)
        {
            return RedirectToActionPermanent("Quotelist");
        }

        [Authorize]
        [EncryptedActionParameter]
        public ActionResult RequestQuote(int id, string ticks, string number, int threshold, int hazordous, int notavailable)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.RequestQuote(id);
            SendMailForGSSCQuotes(number, threshold, hazordous, notavailable);
            return RedirectToActionPermanent("Quotelist");
        }

        public JsonResult MultiCarrierSaveQuote(int QuotationId)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int Quoteexist = mFactory.MultiCarrierSaveQuote(QuotationId);
            if (Quoteexist == 0)
            {
                QuotationId = 0;
            }
            return Json(QuotationId, JsonRequestBehavior.AllowGet);
        }

        [EncryptedActionParameter]
        [AllowAnonymous]
        public ActionResult RequestGuestQuote(int id, string ticks, string number, int threshold, int hazordous, int notavailable)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.RequestQuote(id);
            SendMailForGSSCQuotes(number, threshold, hazordous, notavailable);
            TempData["TosterMsg"] = "Your quote has submitted to Shipa Freight Service Desk.";
            return RedirectToActionPermanent("Index");
        }

        [Authorize]
        [EncryptedActionParameter]
        public async Task<ActionResult> RegenerateQuote(int id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            long mNewId = await Task.Run(() => mFactory.RegenerateQuote(HttpContext.User.Identity.Name, id, "")).ConfigureAwait(false);
            return RedirectToAction("Preview", "Quotation", new { @id = mNewId });
        }

        [Authorize]
        public long NoofQuotes()
        {
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            long NoofQuotes = _qdb.NoofQuotes(HttpContext.User.Identity.Name);
            return NoofQuotes;
        }

        [Authorize]
        public long QuoteLimit()
        {
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            long QuoteLimit = _qdb.QuoteLimit(HttpContext.User.Identity.Name);
            return QuoteLimit;
        }

        public long NoofQuotesForGuest(string Email)
        {
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            long NoofQuotes = _qdb.NoofQuotesForGuest(Email);
            return NoofQuotes;
        }

        public long Precatchmentarea(int OriginPlaceId, int DestinationPlaceId, string ProductTypeId, int OriginPortId, int DestinationPortId, int MovementTypeId, string OfferCode)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            long Precatchmentarea = mFactory.Precatchmentarea(OriginPlaceId, DestinationPlaceId, ProductTypeId, OriginPortId, DestinationPortId, MovementTypeId, OfferCode);
            return Precatchmentarea;
        }

        public int IsOversizeCargo(QuotationModel model)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            var dsOverSize = mFactory.GetShipmentItemsDetails(Convert.ToInt32(model.OriginCountryId));
            int i = 0;
            if (dsOverSize.Tables[0].Rows.Count > 0)
            {
                Double Length_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]); // CM
                Double Width_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]);
                Double Height_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]);
                Double Length_IN = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]) * 0.393701);// CMS to IN
                Double Width_IN = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]) * 0.393701);
                Double Height_IN = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]) * 0.393701);
                string airUOM = Convert.ToString(dsOverSize.Tables[0].Rows[0]["AIRUOM"]);
                if (!string.IsNullOrEmpty(airUOM))
                {
                    switch (airUOM.ToUpperInvariant())
                    {
                        case "MM":
                            Length_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]) / 10); // MM to CM
                            Width_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]) / 10);
                            Height_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]) / 10);
                            break;
                        case "M":
                            Length_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]) * 100); // M to CM
                            Width_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]) * 100);
                            Height_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]) * 100);
                            break;

                        case "IN":
                            Length_IN = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]); // IN
                            Width_IN = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]);
                            Height_IN = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]);
                            break;
                        case "CM":
                            Length_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]); // CM
                            Width_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]);
                            Height_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]);
                            break;
                    }
                }
                foreach (var ShipmentItems in model.ShipmentItems)
                {
                    if (ShipmentItems.PackageTypeCode != "CNT" && i != 1)
                    {
                        long Unit = ShipmentItems.LengthUOM;
                        Double Length;
                        Double Width;
                        Double Height;
                        switch (Unit)
                        {
                            case 101: //MM
                                Length = Convert.ToDouble(ShipmentItems.ItemLength / 10); // Convert MM to CMS
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth / 10); // Convert MM to CMS
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight / 10); // Convert MM to CMS
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_CM || Width >= Width_CM || Height >= Height_CM) { i = 1; }
                                }
                                else { if (Height >= 259.08) { i = 1; } }
                                break;
                            case 100: //M
                                Length = Convert.ToDouble(ShipmentItems.ItemLength * 100); // Convert M to CMS
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth * 100); // Convert M to CMS
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight * 100); // Convert M to CMS
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_CM || Width >= Width_CM || Height >= Height_CM) { i = 1; }
                                }
                                else { if (Height >= 259.08) { i = 1; } }
                                break;
                            case 99: //IN
                                Length = Convert.ToDouble(ShipmentItems.ItemLength);
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth);
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight);
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_IN || Width >= Width_IN || Height >= Height_IN) { i = 1; }
                                }
                                else
                                {
                                    Height = Convert.ToDouble(ShipmentItems.ItemHeight / 12); //Convert IN to FEET
                                    if (Height >= 8.5) { i = 1; }
                                }
                                break;
                            case 98: //CMS
                                Length = Convert.ToDouble(ShipmentItems.ItemLength);
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth);
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight);
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_CM || Width >= Width_CM || Height >= Height_CM) { i = 1; }
                                }
                                else { if (Height >= 259.08) { i = 1; } }
                                break;
                        }
                    }
                }
            }
            return i;
        }

        public string SaveTemplate(string TemplateName, long id)
        {
            string Result = null;
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            int count = _qdb.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = _qdb.GetById(id);
                Result = _qdb.SaveTemplate(mDBEntity, TemplateName);
            }

            return Result;
        }

        [HttpPost]
        public async Task<ActionResult> SaveQuote(QuotationModel model)
        {
            long QuotePreviewID = 0;
            string dimscreatedby = string.Empty;
            string userId = string.Empty;
            QuotationDBFactory mFactory = new QuotationDBFactory();
            dimscreatedby = Request.Form["hdndims"].ToString();
            userId = Request.Form["hdnuserId"].ToString();
            if (model.OriginCountryId == model.DestinationCountryId)
            {
                return RedirectToAction("Index", string.Empty);
            }
            else
            {
                if (!string.IsNullOrEmpty(dimscreatedby))
                {
                    string initialDims = Convert.ToString(Request.Form["hdnInitialDims"]);
                    string finalDims = Convert.ToString(Request.Form["hdnFinalDims"]);
                    if (initialDims == finalDims)
                    {
                        Sendmail_NoCollaborationQuote(dimscreatedby, userId);
                        return RedirectToAction("DimsNoChange", string.Empty);
                    }
                }
                if (model.PreferredCurrencyId == string.Empty)
                {
                    model.PreferredCurrencyId = "USD";
                }
                if (model.CargoValueCurrencyId == string.Empty)
                {
                    model.CargoValueCurrencyId = "USD";
                }
                string OfferCode = Request.Form["hdnOfferCode"];
                model.OfferCode = OfferCode;
                string ProhibitedCargo = Request.Form["hdnProhibitedCargo"];
                if (ProhibitedCargo == "Prohibited")
                {
                    model.IsProhibitedCargo = 1;
                }
                if (string.IsNullOrEmpty(model.OriginCountryName))
                {
                    ModelState.AddModelError("OriginCountryName", "Country Name is Required");
                }
                if (string.IsNullOrEmpty(model.ShipmentDescription))
                {
                    return RedirectToAction("Index", string.Empty);
                }
                if (model.IsEventCargo != false)
                {
                    return RedirectToAction("Index", string.Empty);
                }
                if (model.CurrencyMode != null)
                {
                    string modifiedCargoValue = model.CurrencyMode.Replace(",", "");
                    decimal cargoValue = Convert.ToDecimal(string.Join("", modifiedCargoValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                    model.CargoValue = cargoValue;
                }
                if (model.IsHazardousCargo)
                {
                    string HazValue = Request.Form["hazrdVal"];
                    model.HazardousGoodsType = HazValue;
                }
                else
                    model.HazardousGoodsType = null;

                if (Session["ModifyQuote"] != null && Session["ModifyQuote"] != "")
                {
                    QuotationModel modifymodel = (QuotationModel)Session["ModifyQuote"];
                    if (modifymodel.ProductId != model.ProductId)
                    {
                        model.QuotationNumber = null;
                        model.QuotationId = 0;
                    }
                    Session["ModifyQuote"] = "";
                }

                if (!model.IsInsuranceRequired)
                {
                    model.CargoValue = 0;
                    model.CargoValueCurrencyId = null;
                }

                //--------------------Amazon Quote----------------------
                if (model.AmazonPlaceId != null)
                {
                    //model.DestinationPlaceId = model.AmazonPlaceId;
                    model.DestinationPlaceName = model.AmazonPlaceName;
                    model.QUOTETYPE = "QAMZ";
                    if (model.PalletId == PalletEnum.Myself)
                        model.PALLETIZINGBY = "Myself";
                    else
                        model.PALLETIZINGBY = "Shipa Freight";

                    if (model.LabellingId == PalletEnum.Myself)
                        model.LABELLINGBY = "Myself";
                    else
                        model.LABELLINGBY = "Shipa Freight";
                }
                else
                {

                    model.QUOTETYPE = "QAGL";
                }

                //---------------------------------------------
                model.QuoteSource = "WEB";

                model.IsOversizeCargo = IsOversizeCargo(model);
                decimal TotalGrossWeight = 0;
                foreach (var ShipmentItems in model.ShipmentItems)
                {
                    if (ShipmentItems.PackageTypeName == "Container(s)")
                    {
                        if (ShipmentItems.PackageTypeId == 0)
                        {
                            ShipmentItems.PackageTypeId = 1;
                        }
                    }
                    if (ShipmentItems.PackageTypeId == 1)
                    {
                        ShipmentItems.ItemHeight = 0;
                        ShipmentItems.ItemLength = 0;
                        ShipmentItems.TotalCBM = 0;
                        ShipmentItems.ItemWidth = 0;

                        if (ShipmentItems.WeightPerPiece == 0)
                        {
                            switch (ShipmentItems.ContainerCode)
                            {
                                case "22G0":
                                    ShipmentItems.WeightPerPiece = 14000;
                                    ShipmentItems.WeightTotal = 14000 * ShipmentItems.Quantity;
                                    TotalGrossWeight += (14000 * ShipmentItems.Quantity);
                                    break;
                                case "42G0":
                                    ShipmentItems.WeightPerPiece = 16000;
                                    ShipmentItems.WeightTotal = 16000 * ShipmentItems.Quantity;
                                    TotalGrossWeight += (16000 * ShipmentItems.Quantity);
                                    break;
                                case "45G0":
                                    ShipmentItems.WeightPerPiece = 17000;
                                    ShipmentItems.WeightTotal = 17000 * ShipmentItems.Quantity;
                                    TotalGrossWeight += (17000 * ShipmentItems.Quantity);
                                    break;
                            }
                            ShipmentItems.WeightUOMCode = "KG";
                            ShipmentItems.WeightUOMName = "KG";
                            ShipmentItems.WeightUOM = 95;
                        }
                        else
                        {
                            if (ShipmentItems.WeightUOM == 95)
                                TotalGrossWeight = TotalGrossWeight + (ShipmentItems.WeightPerPiece * ShipmentItems.Quantity);
                            else if (ShipmentItems.WeightUOM == 96)
                                TotalGrossWeight = TotalGrossWeight + ((ShipmentItems.WeightPerPiece * ShipmentItems.Quantity) * Convert.ToDecimal(0.45359237));
                            else if (ShipmentItems.WeightUOM == 97)
                                TotalGrossWeight = TotalGrossWeight + ((ShipmentItems.WeightPerPiece * ShipmentItems.Quantity) * 1000);
                        }
                        model.TotalGrossWeight = TotalGrossWeight;
                    }
                    else
                    {
                        ShipmentItems.ContainerCode = null;
                        ShipmentItems.ContainerId = 0;
                        ShipmentItems.ContainerName = null;
                    }
                }
                if (model.ProductId == ProductEnum.OceanFreight)
                {
                    model.ProductName = "Ocean";
                    model.ProductTypeId = model.ShipmentItems.Count(x => x.ContainerId > 0) > 0 ? 5 : 6;
                    model.ProductTypeName = model.ShipmentItems.Count(x => x.ContainerId > 0) > 0 ? "FCL (NVOCC)" : "LCL (NVOCC)";
                }
                else
                {
                    model.ProductTypeId = 8;
                    model.ProductTypeName = "Premier";
                    model.ProductName = "Air";
                    model.PackageTypes = 0;
                }
                model.MovementTypeName = model.MovementTypeId.Value.GetType().GetMember(model.MovementTypeId.Value.ToString()).First().GetCustomAttribute<DisplayAttribute>().Name;
                DBFactory.Entities.QuotationEntity mUIModel = Mapper.Map<QuotationModel, DBFactory.Entities.QuotationEntity>(model);
                switch (model.MovementTypeId.Value)
                {
                    case MovementTypeEnum.PorttoPort:
                        mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 28 : 30;
                        if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 1;
                        break;

                    case MovementTypeEnum.DoortoPort:
                        mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 24 : 26;
                        if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 37;
                        break;

                    case MovementTypeEnum.PorttoDoor:
                        mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 31 : 33;
                        if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 44;
                        break;

                    default:
                        mUIModel.MOVEMENTTYPEID = 4;
                        break;
                }

                if (!string.IsNullOrEmpty(dimscreatedby)) { mUIModel.CREATEDBY = dimscreatedby; }
                else
                {
                    mUIModel.CREATEDBY = Request.IsAuthenticated ? HttpContext.User.Identity.Name : "Guest";
                }

                if ((mUIModel.PRODUCTID == 2 && mUIModel.DENSITYRATIO != 1000) || (mUIModel.PRODUCTID == 3 && mUIModel.DENSITYRATIO != 166.667))
                {
                    if (mUIModel.PRODUCTID == 2)
                    {
                        mUIModel.DENSITYRATIO = 1000;
                        mUIModel.VOLUMETRICWEIGHT = Math.Truncate((mUIModel.TOTALCBM * Convert.ToDecimal(mUIModel.DENSITYRATIO)) * 1000) / 1000;
                        mUIModel.CHARGEABLEWEIGHT = Math.Truncate((Math.Max(mUIModel.TOTALGROSSWEIGHT, mUIModel.VOLUMETRICWEIGHT)) * 1000) / 1000;
                        mUIModel.CHARGEABLEVOLUME = Math.Truncate((Math.Max(mUIModel.TOTALCBM, (mUIModel.TOTALGROSSWEIGHT / Convert.ToDecimal(mUIModel.DENSITYRATIO)))) * 1000) / 1000;
                        if (mUIModel.CHARGEABLEVOLUME < 1)
                        {
                            mUIModel.CHARGEABLEVOLUME = 1;
                        }
                    }
                    else
                    {
                        mUIModel.DENSITYRATIO = 166.667;
                        mUIModel.VOLUMETRICWEIGHT = Math.Truncate((mUIModel.TOTALCBM * Convert.ToDecimal(mUIModel.DENSITYRATIO)) * 1000) / 1000;
                        mUIModel.CHARGEABLEWEIGHT = Math.Truncate((Math.Max(mUIModel.TOTALGROSSWEIGHT, mUIModel.VOLUMETRICWEIGHT)) * 1000) / 1000;
                        mUIModel.CHARGEABLEVOLUME = Math.Truncate((Math.Max(mUIModel.TOTALCBM, (mUIModel.TOTALGROSSWEIGHT / Convert.ToDecimal(mUIModel.DENSITYRATIO)))) * 1000) / 1000;
                    }
                }
                if (mUIModel.ISOFFERAPPLICABLE == 1 && mUIModel.OFFERCODE == string.Empty)
                {
                    mUIModel.ISOFFERAPPLICABLE = 0;
                    mUIModel.OFFERCODE = null;
                }

                if ((string.IsNullOrEmpty(dimscreatedby)) && (!Request.IsAuthenticated))
                {
                    if (string.IsNullOrEmpty(mUIModel.GUESTEMAIL))
                    {
                        return RedirectToAction("Index", string.Empty);
                    }
                }

                if (model.alternateSearchOrigin == "true")
                {
                    var modifiedOriginPlaceName = model.OriginPlaceName.Split('-')[0];
                    mUIModel.ORIGINPLACENAME = modifiedOriginPlaceName;
                }
                if (model.alternateSearchDestination == "true")
                {
                    var modifiedDestPlaceName = model.DestinationPlaceName.Split('-')[0];
                    mUIModel.DESTINATIONPLACENAME = modifiedDestPlaceName;
                }
                if (!string.IsNullOrEmpty(HttpContext.User.Identity.Name))
                {
                    long stakeholderId = await Task.Run(() => mFactory.GetStakeholderIdByUserId(HttpContext.User.Identity.Name)).ConfigureAwait(false);
                    if (stakeholderId != 0)
                    {
                        mUIModel.CUSTOMERID = stakeholderId;
                    }
                }

                if (model.GoogleTaxonamyCategory == "A" || model.GoogleTaxonamyCategory == "PC")
                {
                    mUIModel.ISPROHIBITEDCARGO = 1;
                }
                if (model.OriginZipCode != null)
                {
                    if (model.OriginZipCodeEnable == "true")
                    {
                        mUIModel.ORIGINZIPCODE = model.OriginZipCode.Trim();
                    }
                    else
                    {
                        return RedirectToAction("Index", string.Empty);
                    }
                }

                if (model.DestinationZipCode != null)
                {
                    if (model.DestinationZipCodeEnable == "true")
                    {
                        mUIModel.DESTINATIONZIPCODE = model.DestinationZipCode.Trim();
                    }
                    else
                    {
                        return RedirectToAction("Index", string.Empty);
                    }
                }
                //--------Quotaion Save Call-----------------------
                long mQuotationId = mFactory.InsertSave(mUIModel, "");
                //----------------------------------------------
                QuotePreviewID = mQuotationId;
            }

            string quotenumber = await mFactory.GetQuotationNumberById(QuotePreviewID);

            if (!string.IsNullOrEmpty(dimscreatedby))
            {
                Sendmail_CollaborationQuote(dimscreatedby, userId);
                UserDBFactory UserFactory = new UserDBFactory();

                UserFactory.NewNotification(5121, "New Quote Available for Your Shipment", "", 0, Convert.ToInt32(QuotePreviewID), quotenumber, dimscreatedby);


                return RedirectToAction("Dimspreview", "Quotation", new { @id = QuotePreviewID });
            }
            else { return RedirectToAction("GetQuote", "Quotation", new { @id = QuotePreviewID }); }
        }

        [Authorize]
        [HttpPost, ValidateHeaderAntiForgeryToken]
        public ActionResult DeleteQuote(int id)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                string Result = mFactory.DeleteQuote(id, HttpContext.User.Identity.Name);
                if (Result == "Deleted")
                    return Json(Url.Action("Quotelist", "Quotation"));
                else
                    return Json(Url.Action("Error", "Error"));
            }
            catch
            {
                return Json(Url.Action("Error", "Error"));
            }
        }

        [Authorize]
        [EncryptedActionParameter]
        public ActionResult DeleteTemplate(int id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.DeleteTemplate(id, HttpContext.User.Identity.Name);
            return RedirectToAction("Index");
        }

        public void UpdateMulticarrier(int QuotationId)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.UpdateMulticarrier(QuotationId);
        }

        [Authorize]
        public ActionResult GetAddressBook()
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DataTable dt = mFactory.GetAddressBook(HttpContext.User.Identity.Name.ToString());
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                    var JsonToReturn = new
                    {
                        rows = trows,
                    };
                    return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(Url.Action("Error", "Error"));
            }
        }

        [Authorize]
        public ActionResult GetAddressBookfornominate()
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DataTable dt = mFactory.GetAddressBookfornominate(HttpContext.User.Identity.Name.ToString());
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                    var JsonToReturn = new
                    {
                        rows = trows,
                    };
                    return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(Url.Action("Error", "Error"));
            }
        }

        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    temp_row.Add(col.ColumnName, dr[col]);
                }
                trows.Add(temp_row);
            }
            return trows;
        }

        public async Task<JsonResult> ZipCode(string CountryId)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string Result = await Task.Run(() => mFactory.ZipCodeConfigbyCountryId(CountryId)).ConfigureAwait(false);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TemplateNameCheck(string TemplateName, long Id)
        {
            string Message = string.Empty;
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            int Result = _qdb.TemplateNameCheck(TemplateName, HttpContext.User.Identity.Name);
            if (Result == 0)
            {
                Message = SaveTemplate(TemplateName, Id);
            }
            else
            {
                //Message = "Template name already exists.";
                Message = FOCiS.Dictionary.App_GlobalResources.Resource.Templatenamealreadyexists;
            }
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult QuoteJobList(string list, string status, string SearchString = "", int PageNo = 1, string Order = "DESC")
        {
            ViewBag.PageNo = PageNo;
            if (list == null || list == "")
                list = "QuoteList";

            if (status == null || status == "")
                status = "ACTIVE";

            QuotationDBFactory mFactory = new QuotationDBFactory();
            DBFactory.Entities.QuoteStatusCountEntity mDBStatusEntity = mFactory.GetQuoteJobCount(HttpContext.User.Identity.Name, SearchString);
            ViewBag.StatusCount = mDBStatusEntity;
            ViewBag.SearchString = SearchString;
            ViewBag.Order = Order;
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            if (list.ToUpperInvariant() == "QUOTELIST")
            {
                List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetSavedQuotes(HttpContext.User.Identity.Name, status, list, PageNo, SearchString, Order);
                List<QuoteJobListModel> mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteJobListEntity>, List<QuoteJobListModel>>(mDBEntity);

                DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetQuoteStatusCount(HttpContext.User.Identity.Name, SearchString, status);
                ViewBag.QuoteJobStatusCount = mQuoteStatusEntity;

                return View("QuoteBookList", mUIModel);
            }
            else
            {
                List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetSavedQuotes(HttpContext.User.Identity.Name, status, list, PageNo, SearchString, Order);
                List<QuoteJobListModel> mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteJobListEntity>, List<QuoteJobListModel>>(mDBEntity);

                DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetJobStatusCount(HttpContext.User.Identity.Name, SearchString);
                ViewBag.QuoteJobStatusCount = mQuoteStatusEntity;

                return View("QuoteBookList", mUIModel);
            }
        }

        [Authorize]
        public async Task<ActionResult> QuoteList(string list, string status, string SearchString = "", int PageNo = 1, string Order = "DESC")
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            ViewBag.SearchString = SearchString;
            SearchString = SearchString.Replace("'", "''");
            ViewBag.PageNo = PageNo;
            if (list == null || list == "")
                list = "QuoteList";

            if (status == null || status == "")
                status = "ACTIVE";

            QuotationDBFactory mFactory = new QuotationDBFactory();
            ViewBag.Order = Order;
            List<QuoteJobListModel> mUIModel;
            if (status != "NOMINATIONS")
            {
                List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetOnlySavedQuotes(HttpContext.User.Identity.Name, status, list, PageNo, SearchString, Order);
                if (uiCulture != "en")
                {
                    await Task.Run(() => LocalizedConverter.GetConversionQuotationListByLanguage<QuoteJobListEntity>(uiCulture, mDBEntity)).ConfigureAwait(false);
                }
                mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteJobListEntity>, List<QuoteJobListModel>>(mDBEntity);
            }
            else
            {
                List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetOnlySavedTemplates(HttpContext.User.Identity.Name, status, list, PageNo, SearchString, Order);
                if (uiCulture != "en")
                {
                    await Task.Run(() => LocalizedConverter.GetConversionQuotationListByLanguage<QuoteJobListEntity>(uiCulture, mDBEntity)).ConfigureAwait(false);
                }
                mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteJobListEntity>, List<QuoteJobListModel>>(mDBEntity);
            }

            DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetQuoteStatusCount(HttpContext.User.Identity.Name, SearchString, status);
            ViewBag.QuoteJobStatusCount = mQuoteStatusEntity;
            if (mUIModel.Count > 0)
            {
                List<QuotationTermsAndConditionsModel> QuotationTermsAndConditionsModel;
                List<DBFactory.Entities.QuotationTermsConditionEntity> mTermsAndConditions = mFactory.GetTermsAndConditions();
                QuotationTermsAndConditionsModel = Mapper.Map<List<DBFactory.Entities.QuotationTermsConditionEntity>, List<QuotationTermsAndConditionsModel>>(mTermsAndConditions);
                mUIModel[0].TermAndConditions = QuotationTermsAndConditionsModel;
            }
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "Quotation/QuoteList";
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";

            return View("Quotelist", mUIModel);
        }

        [AllowAnonymous]
        public JsonResult GetEncodedId(string Id)
        {
            StringBuilder Result = new StringBuilder();
            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };
            string[] mul = Id.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            string JResult = Result.ToString();
            return Json(JResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Sanctioned(string FromCountryId, string ToCountryId, string ProductId, string PackageTypes)
        {
            if (string.IsNullOrEmpty(FromCountryId))
                FromCountryId = "0";
            if (string.IsNullOrEmpty(ToCountryId))
                ToCountryId = "0";
            if (string.IsNullOrEmpty(ProductId))
                ProductId = "0";
            if (string.IsNullOrEmpty(PackageTypes) || ProductId == "3")
                PackageTypes = "0";

            QuotationDBFactory mFactory = new QuotationDBFactory();
            string Result = await Task.Run(() => mFactory.SanctionedCountrybyCountryId(FromCountryId, ToCountryId, ProductId, PackageTypes)).ConfigureAwait(false);
            if (string.IsNullOrEmpty(Result)) Result = "No";
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public void SaveQuoteDocument(int id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int count = mFactory.GetQuotationIdCount(Convert.ToInt32(id));
            if (count > 0)
            {
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                PreferredCurrencyId = DUIModels.PreferredCurrencyId;
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = DynamicClass.GeneratePDF(memoryStream, DUIModels);
                    memoryStream.Close();
                }
                mFactory.SaveQuoteDocument(bytes, id.ToString());
            }
        }

        public JsonResult ClearCustoms(int id, int QuotationId, bool IsChecked)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.UpdateCustomCharges(Convert.ToInt32(QuotationId), Convert.ToInt32(id), IsChecked);
            return Json(GetEncodedId(Convert.ToString(QuotationId)), JsonRequestBehavior.AllowGet);
        }

        [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
        public sealed class ValidateHeaderAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
        {
            public void OnAuthorization(AuthorizationContext filterContext)
            {
                if (filterContext == null)
                {
                    throw new ArgumentNullException("filterContext");
                }
                var httpContext = filterContext.HttpContext;
                var cookie = httpContext.Request.Cookies[AntiForgeryConfig.CookieName];
                AntiForgery.Validate(cookie != null ? cookie.Value : null, httpContext.Request.Headers["__RequestVerificationToken"]);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ValidateMinWeight(string Countryid)
        {
            //QuotationDBFactory mFactory = new QuotationDBFactory();
            DataSet ds = _qdb.ValidateMinWeight(Countryid);
            List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
            var JsonToReturn = new
            {
                rows = trows,
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult QueryInformation(QueryInformationModel QueryInformation)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            var options = Request.Form["Howtoknow"];
            int QueryId = mFactory.SaveQueryInformation(QueryInformation);
            SendMailForContactUs(QueryInformation);
            return Json(Convert.ToString(QueryId), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Descriptionvalidation(Int64 OCountryId, Int64 DCountryId, string Description)
        {
            // QuotationDBFactory mFactory = new QuotationDBFactory();
            DataTable dtMessage = _qdb.Descriptionvalidation(OCountryId, DCountryId, Description);
            if (dtMessage.Rows.Count > 0)
            {
                List<Dictionary<string, object>> trows = ReturnJsonResult(dtMessage);
                var JsonToReturn = new
                {
                    rows = trows,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else { return Json("", JsonRequestBehavior.AllowGet); }
        }

        public async Task<JsonResult> GetCountriesListFromCache(string Code)
        {
            try
            {
                List<CountryInformation> SourceItems = null;
                MyCacheSource objCache = new MyCacheSource();
                if (Code == string.Empty)
                {
                    SourceItems = await Task.Run(() => objCache.GetAllDefaultCountryList(Code).ToList()).ConfigureAwait(false);
                }
                else
                {
                    var objcountry = System.Web.HttpRuntime.Cache["countries"];
                    if (objcountry == null)
                    {
                        SourceItems = await Task.Run(() => objCache.GetAllDefaultCountryList(Code).ToList()).ConfigureAwait(false);
                        if (Code != string.Empty)
                        {
                            SourceItems = SourceItems.Where(x => x.Value != Convert.ToInt32(Code)).ToList();
                        }
                    }
                    else
                    {
                        List<CountryInformation> objectCountryList = objcountry as List<CountryInformation>;
                        SourceItems = await Task.Run(() => objectCountryList).ConfigureAwait(false);
                        if (Code != string.Empty)
                        {
                            SourceItems = SourceItems.Where(x => x.Value != Convert.ToInt32(Code)).ToList();
                        }
                    }
                }
                return Json(SourceItems, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        public async Task<JsonResult> SearchCountriesFromCache(string SearchTerm, string CountryId)
        {
            try
            {
                MyCacheSource objCache = new MyCacheSource();
                var SourceItems = await Task.Run(() => objCache.SearchCountriesFromCache(SearchTerm, CountryId).ToList()).ConfigureAwait(false);
                return Json(SourceItems, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        public JsonResult FillCountryDefaultList(string Code)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<DefaultCountryEntity> country = mdmFactory.GetDefaultCountries(Code);
                if (country != null)
                {
                    var Countrylist = (
                             from items in country
                             select new
                             {
                                 Text = items.TEXT,
                                 Value = items.ID,
                                 COUNTRYCODE = items.CODE
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(Countrylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        public async Task<JsonResult> GetCurrencyListFromCache()
        {
            try
            {
                List<CurrencyInformation> SourceItems = null;
                MyCacheSource objCache = new MyCacheSource();
                SourceItems = await Task.Run(() => objCache.GetAllCurrencyList().ToList()).ConfigureAwait(false);
                return Json(SourceItems, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        public JsonResult FillDefaultCurrencyData()
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<DefaultCurrencyEntity> currency = mdmFactory.GetDefaultCurrencies();
                if (currency != null)
                {
                    var Currencylist = (
                             from items in currency
                             select new
                             {
                                 Text = items.TEXT,
                                 Value = items.ID,
                                 DECIMALPOINT = items.DECIMALPOINT
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(Currencylist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }

        public ActionResult GetRegisterVideo(string Name)
        {
            // QuotationDBFactory mFactory = new QuotationDBFactory();
            string videolink = _qdb.GetRegisterVideo(Name);
            return Json(videolink, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult CouponVerification(string Coupon, int ProductId, int OriginCountry, int DestinationCountry)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DataTable dtMessage = mFactory.CouponVerification(Coupon.ToUpper(), ProductId);
            if (dtMessage.Rows.Count > 0)
            {
                if ((Convert.ToInt32(dtMessage.Rows[0]["ORIGINCOUNTRYID"]) == OriginCountry) && (Convert.ToInt32(dtMessage.Rows[0]["DESTINATIONCOUNTRYID"]) == DestinationCountry))
                {
                    List<Dictionary<string, object>> trows = ReturnJsonResult(dtMessage);
                    var JsonToReturn = new
                    {
                        rows = trows,
                    };
                    return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
                }
                else { return Json("", JsonRequestBehavior.AllowGet); }
            }
            else { return Json("", JsonRequestBehavior.AllowGet); }
        }

        [AllowAnonymous]
        public async Task<ActionResult> InsuranceRequiredOrNot(Int64? orgingCountryId, Int64? destinationCountryId)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if (orgingCountryId != null && destinationCountryId != null)
            {
                string insuranceRequired = await Task.Run(() => mFactory.InsuranceRequiredOrNot(orgingCountryId, destinationCountryId)).ConfigureAwait(false);
                if (insuranceRequired != null)
                {
                    string isInsurance = insuranceRequired.ToString();
                    return Json(isInsurance, JsonRequestBehavior.AllowGet);
                }
                else { return Json("", JsonRequestBehavior.AllowGet); }
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UploadMSDSCertificate()
        {
            long returneddocid = 0;
            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;
                HttpPostedFileBase theFile = files[0];
                string FilePath = Path.GetExtension(theFile.FileName).ToLower();
                if (theFile.ContentLength != 0)
                {
                    if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                        && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                        && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                    {
                        return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                    }
                    string path = theFile.InputStream.ToString();
                    byte[] imageSize = new byte[theFile.ContentLength];
                    theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                    sspdoc.FILENAME = theFile.FileName;
                    sspdoc.FILEEXTENSION = theFile.ContentType;
                    sspdoc.FILESIZE = theFile.ContentLength;
                    sspdoc.FILECONTENT = imageSize;
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    QuotationDBFactory mFactory = new QuotationDBFactory();
                    Guid obj = Guid.NewGuid();
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                    returneddocid = mFactory.UploadMSDSCertificate(docentity, obj.ToString(), EnvironmentName);
                }
                return Json(new { success = true, docid = returneddocid, result = "File uploaded Successfully!!!" }, "text/html", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public JsonResult DeleteMsdsDocByDocId(long DOCID)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.DeleteMsdsDocByDocId(DOCID);
                return Json("Content Deleted Successfully", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetMsdsdocdetails(string QUOTEID)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                var msdsdocdetails = mFactory.GetMsdsdocdetails(QUOTEID);
                return Json(msdsdocdetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public void savesubscription(string email)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            if (email != null) mFactory.savesubscription(email);


        }
        public ActionResult Contactus()
        {
            return View("Contactus");
        }

        public ActionResult SwissLanding()
        {
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";

            if (TempData["VideoLink"] != null)
                ViewBag.VideoLink = TempData["VideoLink"];

            return View("SwissLanding");
        }

        public JsonResult GetIncoTermsByMovementType(long MovementTypeId, long partyTypeId)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                IList<IncoTermsEntity> incoterms = mFactory.GetIncoTermsByMovementType(MovementTypeId, partyTypeId);
                if (incoterms != null)
                {
                    var incotermslist = (
                             from items in incoterms
                             select new
                             {
                                 Text = items.DESCRIPTION,
                                 Value = items.TERMSOFTRADEID,
                                 Code = items.CODE
                             }).Distinct().OrderBy(x => x.Text).ToList();
                    return Json(incotermslist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
        }
        //public string TransitTimeCalculationByMovementType(string movementType, string transitTime, Int64 quotationId)
        //{
        //    string calculatedTransitTime = string.Empty;
        //    try
        //    {
        //        var transitTiming = transitTime.Split(' ');
        //        double time = Convert.ToDouble(transitTiming[0]);
        //        if (time > 0)
        //        {
        //            string dayOrhour = transitTiming[1];
        //            if (movementType.ToUpperInvariant() == "DOOR TO DOOR")
        //            {
        //                calculatedTransitTime = (dayOrhour.ToUpperInvariant() == "DAYS") ? (time + 3) + " " + dayOrhour : "3 Days" + " " + transitTime;
        //            }
        //            else if (movementType.ToUpperInvariant() == "DOOR TO PORT" || movementType.ToUpperInvariant() == "PORT TO DOOR")
        //            {
        //                calculatedTransitTime = (dayOrhour.ToUpperInvariant() == "DAYS") ? (time + 2) + " " + dayOrhour : "2 Days" + " " + transitTime;
        //            }
        //            else if (movementType.ToUpperInvariant() == "PORT TO PORT")
        //            {
        //                calculatedTransitTime = (dayOrhour.ToUpperInvariant() == "DAYS") ? (time + 1) + " " + dayOrhour : "1 Days" + " " + transitTime;
        //            }
        //            else
        //            {
        //                calculatedTransitTime = transitTime;
        //            }
        //        }
        //        else
        //        {
        //            calculatedTransitTime = "No transmit time available";
        //        }
        //    }
        //    catch
        //    {
        //        calculatedTransitTime = "No transmit time available";
        //    }
        //    QuotationDBFactory mFactory = new QuotationDBFactory();
        //    mFactory.UpdateTransitTime(quotationId, calculatedTransitTime);
        //    return calculatedTransitTime;
        //}

        //[AllowAnonymous]
        //[EncryptedActionParameter]
        //public ActionResult AmzIndex(int? id, string col = "")
        //{
        //    TempData["GQuote"] = null;
        //    TempData["Quote"] = null;
        //    ViewBag.BodyClass = "";
        //    ViewBag.FooterClass = "footer-bg";

        //    QuotationModel mUIModel = new QuotationModel();
        //    MDMDataFactory mFactoryValue = new MDMDataFactory();
        //    QuotationDBFactory mFactory = new QuotationDBFactory();
        //    if (id.HasValue)
        //    {
        //        DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id.Value);
        //        mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
        //        Session["ModifyQuote"] = mUIModel;
        //        if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
        //            mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
        //        else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
        //            mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
        //        else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
        //            mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
        //        else
        //            mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
        //        if (mUIModel.CargoValue != 0)
        //        {
        //            mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
        //        }
        //        else
        //        {
        //            mUIModel.CurrencyMode = string.Empty;
        //        }
        //        mUIModel.OriginCountryName = mUIModel.OCountryName;
        //        mUIModel.DestinationCountryName = mUIModel.DCountryName;
        //        mUIModel.OriginCountryId = mUIModel.OCountryId;
        //        mUIModel.DestinationCountryId = mUIModel.DCountryId;
        //        int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
        //        mUIModel.PreferredDecimal = preferreddecimalPoint;
        //        int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
        //        mUIModel.CargoDecimal = cargodecimalPoint;
        //        string PreferredCurrencyCode = mUIModel.PreferredCurrencyId;

        //        DataTable mFactoryEntityPrederred = mFactoryValue.FindCurrencyData(PreferredCurrencyCode);

        //        ViewBag.PREFERREDCURRENCY = mUIModel.PreferredCurrencyId + "-" + mFactoryEntityPrederred.Rows[0].ItemArray[1];

        //        if (mUIModel.IsInsuranceRequired != false)
        //        {
        //            string CargoValueCurrencyCode = mUIModel.CargoValueCurrencyId;

        //            DataTable mFactoryEntityCargo = mFactoryValue.FindCurrencyData(CargoValueCurrencyCode);

        //            ViewBag.CargoValueCurrency = mUIModel.CargoValueCurrencyId + "-" + mFactoryEntityCargo.Rows[0].ItemArray[1];
        //        }
        //        DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
        //        if (ds.Tables.Count > 0)
        //        {
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();
        //                if (ViewBag.WeightUOMCode == "KG")
        //                {
        //                    ViewBag.WeightUOMName = "KG";
        //                    ViewBag.WeightUOM = 95;
        //                    ViewBag.MaxWeightPerPiece = 5000;
        //                }
        //                else if (ViewBag.WeightUOMCode == "LB")
        //                {
        //                    ViewBag.WeightUOMName = "LB";
        //                    ViewBag.WeightUOM = 96;
        //                    ViewBag.MaxWeightPerPiece = 11024;
        //                }
        //                else if (ViewBag.WeightUOMCode == "TON")
        //                {
        //                    ViewBag.WeightUOMName = "TON";
        //                    ViewBag.WeightUOM = 97;
        //                    ViewBag.MaxWeightPerPiece = 5;
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.LengthUOMCode = "CM";
        //        ViewBag.LengthUOMName = "CENTIMETER";
        //        ViewBag.LengthUOM = 98;
        //        ViewBag.WeightUOMCode = "KG";
        //        ViewBag.WeightUOMName = "KG";
        //        ViewBag.WeightUOM = 95;
        //        ViewBag.MaxWeightPerPiece = 5000;
        //        mUIModel.PreferredCurrencyId = "USD";
        //        mUIModel.PreferredDecimal = 2;
        //        mUIModel.CargoValueCurrencyId = "USD";
        //        mUIModel.CargoDecimal = 2;
        //        ViewBag.CargoValueCurrency = "US Dollar";
        //        ViewBag.PREFERREDCURRENCY = "US Dollar";
        //        if (Request.IsAuthenticated)
        //        {
        //            //QuotationDBFactory mFactory = new QuotationDBFactory();
        //            DataSet ds = mFactory.GetUserCurrencyandUOM(HttpContext.User.Identity.Name);
        //            DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetQuotationTemplates(HttpContext.User.Identity.Name);
        //            mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
        //            mUIModel.PreferredCurrencyId = "USD";
        //            mUIModel.PreferredDecimal = 2;
        //            mUIModel.CargoValueCurrencyId = "USD";
        //            mUIModel.CargoDecimal = 2;
        //            mUIModel.MovementTypeName = "Door to door";
        //            mUIModel.ProductId = ProductEnum.OceanFreight;
        //            mUIModel.PackageTypes = PackageTypeEnum.LCL;
        //            mUIModel.DensityRatio = 1000;
        //            mUIModel.MovementTypeId = (MovementTypeEnum)1;
        //            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //            {
        //                mUIModel.PreferredCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
        //                int preferreddecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.PreferredCurrencyId);
        //                mUIModel.PreferredDecimal = preferreddecimalPoint;
        //                ViewBag.PREFERREDCURRENCY = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
        //                mUIModel.CargoValueCurrencyId = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString()) ? "USD" : ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString();
        //                int cargodecimalPoint = mFactory.GetDecimalPointByCurrencyCode(mUIModel.CargoValueCurrencyId);
        //                mUIModel.CargoDecimal = cargodecimalPoint;
        //                ViewBag.CargoValueCurrency = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString()) ? "US Dollar" : ds.Tables[0].Rows[0]["PREFERREDCURRENCY"].ToString(); //ds.Tables[0].Rows[0]["PREFERREDCURRENCYCODE"].ToString();
        //                ViewBag.LengthUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString()) ? "CM" : ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString(); //ds.Tables[0].Rows[0]["LENGTHUNIT"].ToString();
        //                if (ViewBag.LengthUOMCode == "CM")
        //                {
        //                    ViewBag.LengthUOMName = "CENTIMETER";
        //                    ViewBag.LengthUOM = 98;
        //                }
        //                else if (ViewBag.LengthUOMCode == "IN")
        //                {
        //                    ViewBag.LengthUOMName = "INCH";
        //                    ViewBag.LengthUOM = 99;
        //                }
        //                else if (ViewBag.LengthUOMCode == "M")
        //                {
        //                    ViewBag.LengthUOMName = "METER";
        //                    ViewBag.LengthUOM = 100;
        //                }
        //                else if (ViewBag.LengthUOMCode == "MM")
        //                {
        //                    ViewBag.LengthUOMName = "MILLIMETER";
        //                    ViewBag.LengthUOM = 101;
        //                }
        //                ViewBag.WeightUOMCode = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString()) ? "KG" : ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString(); //ds.Tables[0].Rows[0]["WEIGHTUNIT"].ToString();

        //                if (ViewBag.WeightUOMCode == "KG")
        //                {
        //                    ViewBag.WeightUOMName = "KG";
        //                    ViewBag.WeightUOM = 95;
        //                    ViewBag.MaxWeightPerPiece = 5000;
        //                }
        //                else if (ViewBag.WeightUOMCode == "LB")
        //                {
        //                    ViewBag.WeightUOMName = "LB";
        //                    ViewBag.WeightUOM = 96;
        //                    ViewBag.MaxWeightPerPiece = 11024;
        //                }
        //                else if (ViewBag.WeightUOMCode == "TON")
        //                {
        //                    ViewBag.WeightUOMName = "TON";
        //                    ViewBag.WeightUOM = 97;
        //                    ViewBag.MaxWeightPerPiece = 5;
        //                }
        //            }
        //        }
        //    }
        //    mUIModel.IsOfferApplicable = false;
        //    mUIModel.OfferCode = string.Empty;
        //    if (mUIModel.ShipmentItems == null)
        //    {
        //        mUIModel.ShipmentItems = null;
        //    }
        //    IList<IncoTermsEntity> incoterms = mFactoryValue.GetIncoTerms(string.Empty, string.Empty);
        //    ViewBag.incoterms = incoterms;
        //    mUIModel.DestinationCountryId = 232;
        //    mUIModel.DestinationCountryName = "United States";
        //    mUIModel.AmazonPlaceId = mUIModel.AMAZONFCCODE;
        //    mUIModel.AmazonPlaceName = mUIModel.AMAZONFCCODE + " - " + mUIModel.DestinationPlaceName;
        //    DataTable DTAMZINFO = mFactory.GETAMZINFO(mUIModel.AMAZONFCCODE);
        //    if (DTAMZINFO.Rows.Count > 0)
        //    {
        //        mUIModel.DestinationStateName = DTAMZINFO.Rows[0]["SUBDIVISONNAME"].ToString();
        //        mUIModel.DestinationStateId = Convert.ToDecimal(DTAMZINFO.Rows[0]["SUBDIVISIONID"]);
        //    }
        //    if (mUIModel.LABELLINGBY != null && mUIModel.LABELLINGBY == "Myself")
        //        mUIModel.LabellingId = PalletEnum.Myself;
        //    else if (mUIModel.LABELLINGBY != null)
        //        mUIModel.LabellingId = PalletEnum.Shipafreight;

        //    if (mUIModel.PALLETIZINGBY != null && mUIModel.PALLETIZINGBY == "Myself")
        //        mUIModel.PalletId = PalletEnum.Myself;
        //    else if (mUIModel.PALLETIZINGBY != null)
        //        mUIModel.PalletId = PalletEnum.Shipafreight;

        //    return View("Create_Amazon", mUIModel);
        //}

    }
}
