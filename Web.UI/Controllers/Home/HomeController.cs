﻿using Chikoti.Helpers;
using FOCiS.SSP.Web.UI.Controllers.Base;
using FOCiS.SSP.Web.UI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Controllers.Home
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Change Language
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public JsonResult ChangeLanguage(string lang)
        {
            TempData["ChangeLanguage"] = lang;
            new Languages().SetLanguage(lang);
            return Json(lang, JsonRequestBehavior.AllowGet);
            /// return RedirectToAction("Index", "Tracking");
        }
        public JsonResult LoadCookie()
        {
            HttpCookie langCookie = Request.Cookies["shipaculture"];
            if (langCookie != null)
            {
                return Json(langCookie.Value, JsonRequestBehavior.AllowGet);
            }
            else {
                return Json("en", JsonRequestBehavior.AllowGet);
            }
        }
        public void ClearLanguageCookie()
        {
                Request.Cookies["shipaculture"].Expires = DateTime.Now.AddDays(-1);
                Request.Cookies["shipaculture"].Value = "";
                Response.Cookies["shipaculture"].Value = "";
                Response.Cookies["shipaculture"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(Response.Cookies["shipaculture"]);
                Request.Cookies.Add(Request.Cookies["shipaculture"]);
        }


        #region Accept Cookie Law
        [HttpPost]
        public JsonResult AcceptCookieLaw()
        {
            new DynamicClass().AcceptCookieLaw();
            return Json("Success", JsonRequestBehavior.AllowGet);            
        }
        #endregion Accept Cookie Law
    }
}