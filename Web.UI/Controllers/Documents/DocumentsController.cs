﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.JP;
using System.Net;
using System.IO;
using FOCiS.SSP.Library.Extensions;
using System.Data;
using FOCiS.SSP.Models.UserManagement;
using System.Net.Mail;
using FOCiS.SSP.Web.UI.Helpers;
using FindUserCountryName;

namespace FOCiS.SSP.Web.UI.Controllers.Documents
{
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class DocumentsController : Controller
    {
      [AllowAnonymous]
       
        public ActionResult Opendocuments(string jobnumber, string quoteno, string Provideremailid, string CargoAvailable, string Con, string Name, string ChinnaCheck)
        {
            string shname = "";
            string conname = "";
            if (!string.IsNullOrEmpty(ChinnaCheck))
            {
                if (ChinnaCheck.Equals("48"))
                {
                    new Languages().SetLanguage("zh-hk");
                }
                else
                {
                    new Languages().SetLanguage("en");
                }
            }
            else
            {
                new Languages().SetLanguage("en");
            }
            ViewBag.CargoAvailable = CargoAvailable;
            string Jobnumber = AESEncrytDecry.DecryptStringAES(jobnumber);
            string Quoteid = AESEncrytDecry.DecryptStringAES(quoteno);
            string emailid = AESEncrytDecry.DecryptStringAES(Provideremailid);
            if (!string.IsNullOrEmpty( Name))
            {
                shname = AESEncrytDecry.DecryptStringAES(Name);
            }
            if (!string.IsNullOrEmpty(Con))
            {
                conname = AESEncrytDecry.DecryptStringAES(Con);
            }
            int? JobNumber = Convert.ToInt32(Jobnumber);
            ViewBag.ModifiedBy = emailid;
            ViewBag.SHNAME = shname;
            ViewBag.CONNAME = conname;
            int? id = Convert.ToInt32(Quoteid);

            if (!JobNumber.HasValue || JobNumber.Value == 0)
            {
                JobNumber = Convert.ToInt32((Session["JJOBNUMBER"]).ToString());
            }
          
            if (!id.HasValue || id.Value == 0)
            {
                id = Convert.ToInt32((Session["QUOTATIONID"]).ToString());
            }
         

            //JobBookingDbFactory mFactory = new JobBookingDbFactory();
            //JobBookingModel mUIModel = new JobBookingModel();
            //if (Convert.ToInt64(JobNumber) == 0)
            //{
            //    DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
            //    mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            //}
            //else
            //{
            //    DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetailsCollabration(Convert.ToInt32(id), Convert.ToInt64(JobNumber), emailid);
            //    IList<SSPJobDetailsEntity> jobdet = new List<SSPJobDetailsEntity>();
            //    jobdet = mDBEntity.JobItems;
            //    if (jobdet != null)
            //    {
            //        mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
            //        mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
            //        mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
            //        mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
            //        mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
            //        mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
            //        mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
            //        mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
            //        mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
            //        mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
            //        mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
            //        mDBEntity.JSTATEID = jobdet[0].STATEID;
            //    }
            //     mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                 
            //}
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            if (Convert.ToInt64(JobNumber) == 0)
            {
                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            }
            else
            {
                DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetailsCollabration(Convert.ToInt32(id), Convert.ToInt64(JobNumber), emailid);
                var jobdet = mDBEntity.JobItems;
                var partydet = mDBEntity.PartyItems;
                var shipperdet = (from u in partydet where u.PARTYTYPE == "91" select u).ToArray();
                var consigneedet = (from u in partydet where u.PARTYTYPE == "92" select u).ToArray();
                if (jobdet != null && jobdet.Count > 0)
                {
                    mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                    mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                    mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                    mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                    mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                    mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                    mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                    mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                    mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                    mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                    mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                    mDBEntity.JSTATEID = jobdet[0].STATEID;
                }
                if (shipperdet != null && shipperdet.Length > 0)
                {
                    mDBEntity.SHADDRESSTYPE = shipperdet[0].ADDRESSTYPE;
                    mDBEntity.SHBUILDINGNAME = shipperdet[0].BUILDINGNAME;
                    mDBEntity.SHCITYCODE = shipperdet[0].CITYCODE;
                    mDBEntity.SHCITYNAME = shipperdet[0].CITYNAME;
                    mDBEntity.SHCLIENTID = shipperdet[0].CLIENTID;
                    mDBEntity.SHCLIENTNAME = shipperdet[0].CLIENTNAME;
                    mDBEntity.SHFULLADDRESS = shipperdet[0].FULLADDRESS;
                    mDBEntity.SHCOUNTRYCODE = shipperdet[0].COUNTRYCODE;
                    mDBEntity.SHCOUNTRYNAME = shipperdet[0].COUNTRYNAME;
                    mDBEntity.SHFULLADDRESS = shipperdet[0].FULLADDRESS;
                    mDBEntity.SHHOUSENO = shipperdet[0].HOUSENO;
                    mDBEntity.SHISDEFAULTADDRESS = shipperdet[0].ISDEFAULTADDRESS;
                    mDBEntity.SHJOBNUMBER = shipperdet[0].JOBNUMBER;
                    mDBEntity.SHLATITUDE = shipperdet[0].LATITUDE;
                    mDBEntity.SHLONGITUDE = shipperdet[0].LONGITUDE;
                    mDBEntity.SHPARTYDETAILSID = shipperdet[0].PARTYDETAILSID;
                    mDBEntity.SHPARTYTYPE = shipperdet[0].PARTYTYPE;
                    mDBEntity.SHPINCODE = shipperdet[0].PINCODE;
                    mDBEntity.SHSTATECODE = shipperdet[0].STATECODE;
                    mDBEntity.SHSTATENAME = shipperdet[0].STATENAME;
                    mDBEntity.SHSTREET1 = shipperdet[0].STREET1;
                    mDBEntity.SHSTREET2 = shipperdet[0].STREET2;
                    mDBEntity.SHFIRSTNAME = shipperdet[0].FIRSTNAME;
                    mDBEntity.SHLASTNAME = shipperdet[0].LASTNAME;
                    mDBEntity.SHPHONE = shipperdet[0].PHONE;
                    mDBEntity.SHEMAILID = shipperdet[0].EMAILID;
                    mDBEntity.SHSALUTATION = shipperdet[0].SALUTATION;
                    mDBEntity.SHCLIENTNAME = shipperdet[0].FIRSTNAME;
                }

                if (consigneedet != null && consigneedet.Length > 0)
                {
                    mDBEntity.CONADDRESSTYPE = consigneedet[0].ADDRESSTYPE;
                    mDBEntity.CONBUILDINGNAME = consigneedet[0].BUILDINGNAME;
                    mDBEntity.CONCITYCODE = consigneedet[0].CITYCODE;
                    mDBEntity.CONCITYNAME = consigneedet[0].CITYNAME;
                    mDBEntity.CONCLIENTID = consigneedet[0].CLIENTID;
                    mDBEntity.CONCLIENTNAME = consigneedet[0].CLIENTNAME;
                    mDBEntity.CONCOUNTRYCODE = consigneedet[0].COUNTRYCODE;
                    mDBEntity.CONCOUNTRYNAME = consigneedet[0].COUNTRYNAME;
                    mDBEntity.CONFULLADDRESS = consigneedet[0].FULLADDRESS;
                    mDBEntity.CONHOUSENO = consigneedet[0].HOUSENO;
                    mDBEntity.CONISDEFAULTADDRESS = consigneedet[0].ISDEFAULTADDRESS;
                    mDBEntity.CONJOBNUMBER = consigneedet[0].JOBNUMBER;
                    mDBEntity.CONLATITUDE = consigneedet[0].LATITUDE;
                    mDBEntity.CONLONGITUDE = consigneedet[0].LONGITUDE;
                    mDBEntity.CONPARTYDETAILSID = consigneedet[0].PARTYDETAILSID;
                    mDBEntity.CONPARTYTYPE = consigneedet[0].PARTYTYPE;
                    mDBEntity.CONPINCODE = consigneedet[0].PINCODE;
                    mDBEntity.CONSTATECODE = consigneedet[0].STATECODE;
                    mDBEntity.CONSTATENAME = consigneedet[0].STATENAME;
                    mDBEntity.CONSTREET1 = consigneedet[0].STREET1;
                    mDBEntity.CONSTREET2 = consigneedet[0].STREET2;
                    mDBEntity.CONFIRSTNAME = consigneedet[0].FIRSTNAME;
                    mDBEntity.CONLASTNAME = consigneedet[0].LASTNAME;
                    mDBEntity.CONPHONE = consigneedet[0].PHONE;
                    mDBEntity.CONEMAILID = consigneedet[0].EMAILID;
                    mDBEntity.CONSALUTATION = consigneedet[0].SALUTATION;
                    mDBEntity.CONCLIENTNAME = consigneedet[0].FIRSTNAME;
                }
                mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                Session["CreatedBy"] = mDBEntity.CREATEDBY;
            }
            if (mUIModel.SHCLIENTNAME == null)
            {
                mUIModel.SHCLIENTNAME = ViewBag.SHNAME;
               
               
            }
            if (mUIModel.CONCLIENTNAME == null) {
                mUIModel.CONCLIENTNAME = ViewBag.CONNAME;

            }


            mUIModel.CREATEDBY = HttpContext.User.Identity.Name;
            return View("Documents", mUIModel);
        }

      [AllowAnonymous]
      [EncryptedActionParameter]
      public ActionResult OpendocumentsNew(int? jobnumber, int? quoteno, string Provideremailid, string CargoAvailable, string Con, string Name, string ChinnaCheck)
      {
          //string shname = "";
          //string conname = "";
          if (!string.IsNullOrEmpty(ChinnaCheck))
          {
              if (ChinnaCheck.Equals("48"))
              {
                  new Languages().SetLanguage("zh-hk");
              }
              else
              {
                  new Languages().SetLanguage("en");
              }
          }
          else
          {
              new Languages().SetLanguage("en");
          }
          ViewBag.CargoAvailable = CargoAvailable;
          //string Jobnumber = AESEncrytDecry.DecryptStringAES(jobnumber);
          //string Quoteid = AESEncrytDecry.DecryptStringAES(quoteno);
          //string emailid = AESEncrytDecry.DecryptStringAES(Provideremailid);
          //if (!string.IsNullOrEmpty(Name))
          //{
          //    shname = AESEncrytDecry.DecryptStringAES(Name);
          //}
          //if (!string.IsNullOrEmpty(Con))
          //{
          //    conname = AESEncrytDecry.DecryptStringAES(Con);
          //}
          string emailid = Provideremailid;
          int? JobNumber = jobnumber;
          ViewBag.ModifiedBy = Provideremailid;
          ViewBag.SHNAME = Name;
          ViewBag.CONNAME = Con;
          int? id = quoteno;

          if (!JobNumber.HasValue || JobNumber.Value == 0)
          {
              JobNumber = Convert.ToInt32((Session["JJOBNUMBER"]).ToString());
          }

          if (!id.HasValue || id.Value == 0)
          {
              id = Convert.ToInt32((Session["QUOTATIONID"]).ToString());
          }


          //JobBookingDbFactory mFactory = new JobBookingDbFactory();
          //JobBookingModel mUIModel = new JobBookingModel();
          //if (Convert.ToInt64(JobNumber) == 0)
          //{
          //    DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
          //    mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
          //}
          //else
          //{
          //    DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetailsCollabration(Convert.ToInt32(id), Convert.ToInt64(JobNumber), emailid);
          //    IList<SSPJobDetailsEntity> jobdet = new List<SSPJobDetailsEntity>();
          //    jobdet = mDBEntity.JobItems;
          //    if (jobdet != null)
          //    {
          //        mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
          //        mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
          //        mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
          //        mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
          //        mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
          //        mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
          //        mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
          //        mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
          //        mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
          //        mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
          //        mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
          //        mDBEntity.JSTATEID = jobdet[0].STATEID;
          //    }
          //     mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);

          //}
          JobBookingDbFactory mFactory = new JobBookingDbFactory();
          JobBookingModel mUIModel = new JobBookingModel();
          if (Convert.ToInt64(JobNumber) == 0)
          {
              DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(id));
              mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
          }
          else
          {
              DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetailsCollabration(Convert.ToInt32(id), Convert.ToInt64(JobNumber), emailid);
              var jobdet = mDBEntity.JobItems;
              var partydet = mDBEntity.PartyItems;
              var shipperdet = (from u in partydet where u.PARTYTYPE == "91" select u).ToArray();
              var consigneedet = (from u in partydet where u.PARTYTYPE == "92" select u).ToArray();
              if (jobdet != null && jobdet.Count > 0)
              {
                  mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                  mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                  mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                  mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                  mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                  mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                  mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                  mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                  mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                  mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                  mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                  mDBEntity.JSTATEID = jobdet[0].STATEID;
              }
              if (shipperdet != null && shipperdet.Length > 0)
              {
                  mDBEntity.SHADDRESSTYPE = shipperdet[0].ADDRESSTYPE;
                  mDBEntity.SHBUILDINGNAME = shipperdet[0].BUILDINGNAME;
                  mDBEntity.SHCITYCODE = shipperdet[0].CITYCODE;
                  mDBEntity.SHCITYNAME = shipperdet[0].CITYNAME;
                  mDBEntity.SHCLIENTID = shipperdet[0].CLIENTID;
                  mDBEntity.SHCLIENTNAME = shipperdet[0].CLIENTNAME;
                  mDBEntity.SHFULLADDRESS = shipperdet[0].FULLADDRESS;
                  mDBEntity.SHCOUNTRYCODE = shipperdet[0].COUNTRYCODE;
                  mDBEntity.SHCOUNTRYNAME = shipperdet[0].COUNTRYNAME;
                  mDBEntity.SHFULLADDRESS = shipperdet[0].FULLADDRESS;
                  mDBEntity.SHHOUSENO = shipperdet[0].HOUSENO;
                  mDBEntity.SHISDEFAULTADDRESS = shipperdet[0].ISDEFAULTADDRESS;
                  mDBEntity.SHJOBNUMBER = shipperdet[0].JOBNUMBER;
                  mDBEntity.SHLATITUDE = shipperdet[0].LATITUDE;
                  mDBEntity.SHLONGITUDE = shipperdet[0].LONGITUDE;
                  mDBEntity.SHPARTYDETAILSID = shipperdet[0].PARTYDETAILSID;
                  mDBEntity.SHPARTYTYPE = shipperdet[0].PARTYTYPE;
                  mDBEntity.SHPINCODE = shipperdet[0].PINCODE;
                  mDBEntity.SHSTATECODE = shipperdet[0].STATECODE;
                  mDBEntity.SHSTATENAME = shipperdet[0].STATENAME;
                  mDBEntity.SHSTREET1 = shipperdet[0].STREET1;
                  mDBEntity.SHSTREET2 = shipperdet[0].STREET2;
                  mDBEntity.SHFIRSTNAME = shipperdet[0].FIRSTNAME;
                  mDBEntity.SHLASTNAME = shipperdet[0].LASTNAME;
                  mDBEntity.SHPHONE = shipperdet[0].PHONE;
                  mDBEntity.SHEMAILID = shipperdet[0].EMAILID;
                  mDBEntity.SHSALUTATION = shipperdet[0].SALUTATION;
                  mDBEntity.SHCLIENTNAME = shipperdet[0].FIRSTNAME;
              }

              if (consigneedet != null && consigneedet.Length > 0)
              {
                  mDBEntity.CONADDRESSTYPE = consigneedet[0].ADDRESSTYPE;
                  mDBEntity.CONBUILDINGNAME = consigneedet[0].BUILDINGNAME;
                  mDBEntity.CONCITYCODE = consigneedet[0].CITYCODE;
                  mDBEntity.CONCITYNAME = consigneedet[0].CITYNAME;
                  mDBEntity.CONCLIENTID = consigneedet[0].CLIENTID;
                  mDBEntity.CONCLIENTNAME = consigneedet[0].CLIENTNAME;
                  mDBEntity.CONCOUNTRYCODE = consigneedet[0].COUNTRYCODE;
                  mDBEntity.CONCOUNTRYNAME = consigneedet[0].COUNTRYNAME;
                  mDBEntity.CONFULLADDRESS = consigneedet[0].FULLADDRESS;
                  mDBEntity.CONHOUSENO = consigneedet[0].HOUSENO;
                  mDBEntity.CONISDEFAULTADDRESS = consigneedet[0].ISDEFAULTADDRESS;
                  mDBEntity.CONJOBNUMBER = consigneedet[0].JOBNUMBER;
                  mDBEntity.CONLATITUDE = consigneedet[0].LATITUDE;
                  mDBEntity.CONLONGITUDE = consigneedet[0].LONGITUDE;
                  mDBEntity.CONPARTYDETAILSID = consigneedet[0].PARTYDETAILSID;
                  mDBEntity.CONPARTYTYPE = consigneedet[0].PARTYTYPE;
                  mDBEntity.CONPINCODE = consigneedet[0].PINCODE;
                  mDBEntity.CONSTATECODE = consigneedet[0].STATECODE;
                  mDBEntity.CONSTATENAME = consigneedet[0].STATENAME;
                  mDBEntity.CONSTREET1 = consigneedet[0].STREET1;
                  mDBEntity.CONSTREET2 = consigneedet[0].STREET2;
                  mDBEntity.CONFIRSTNAME = consigneedet[0].FIRSTNAME;
                  mDBEntity.CONLASTNAME = consigneedet[0].LASTNAME;
                  mDBEntity.CONPHONE = consigneedet[0].PHONE;
                  mDBEntity.CONEMAILID = consigneedet[0].EMAILID;
                  mDBEntity.CONSALUTATION = consigneedet[0].SALUTATION;
                  mDBEntity.CONCLIENTNAME = consigneedet[0].FIRSTNAME;
              }
              mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
              Session["CreatedBy"] = mDBEntity.CREATEDBY;
          }
          if (mUIModel.SHCLIENTNAME == null)
          {
              mUIModel.SHCLIENTNAME = ViewBag.SHNAME;


          }
          if (mUIModel.CONCLIENTNAME == null)
          {
              mUIModel.CONCLIENTNAME = ViewBag.CONNAME;

          }


          mUIModel.CREATEDBY = HttpContext.User.Identity.Name;
          return View("Documents", mUIModel);
      }

        public JsonResult UpdateSaveforlater(string DOCID, string DocupDate, string ProvidedBy, string TMember)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.UpdateSaveforlaterfordocument(DOCID, DocupDate, ProvidedBy, TMember);

                //--------Send Mail notification if mandatory documents are empty... Anil G------------
                //if (TMember == "Upload later all")
                //    SendMailForMandatoryDocs(HttpContext.User.Identity.Name.ToString());
                //-------------------------------------------------------------------------------------
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ClearUploadLater(string DOCID)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.ClearUploadLater(DOCID);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }
        public void DownloadTemplate(string path)
        {
            string DocPath = System.Configuration.ConfigurationManager.AppSettings["ReportServerUrl"].ToString();

            var webClient = new WebClient(); byte[] imageBytes = null;

            string URl = DocPath + path.Split('/')[3];
            try
            {
                imageBytes = webClient.DownloadData(URl);
               
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    string Filename = path.Split('/')[3].Split('.')[0];
                    Response.Clear();
                    Response.ContentType = "application/jpeg";
                    Response.AddHeader("Content-Disposition", "inline; filename=" + path.Split('/')[3]);
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(imageBytes);
                    Response.End();
                    Response.Close();
                   
                }
                webClient.Dispose();
            }
            catch (Exception ex)
            {
                
            }
        }
        [AllowAnonymous]
        public ActionResult UploadDocuments_NewCollabration(string DOCID, string JobNumber, string OJobnumber, string PaymentStatus, string ModifiedBy)
        {
            string createdby = Session["CreatedBy"].ToString();
            if (string.IsNullOrEmpty(ModifiedBy))
            {
                ModifiedBy = createdby;
            }
            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;

                HttpPostedFileBase theFile = files[0];


                string FilePath = Path.GetExtension(theFile.FileName).ToLower();
                if (theFile.ContentLength != 0)
                {
                    if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                        && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                        && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                    {
                        return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                    }
                    string path = theFile.InputStream.ToString();
                    byte[] imageSize = new byte[theFile.ContentLength];
                    theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();

                    sspdoc.FILENAME = theFile.FileName;
                    sspdoc.FILEEXTENSION = theFile.ContentType;
                    sspdoc.FILESIZE = theFile.ContentLength;
                    sspdoc.FILECONTENT = imageSize;
                    sspdoc.DOCID = Convert.ToInt64(DOCID);
                    sspdoc.JOBNUMBER = Convert.ToInt64(JobNumber);
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Guid obj = Guid.NewGuid();
                    string EnvironmentName=DynamicClass.GetEnvironmentNameDocs();
                    mFactory.UpdateDocumentBytesCollabration(docentity,createdby, obj.ToString(), EnvironmentName, ModifiedBy);

                   
                }
                return Json(new { success = true, result = "File uploaded Successfully!!!" }, "text/html", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteDocsbyDocId(long DOCID)
        {
            try
            {
                string createdby = Session["CreatedBy"].ToString();
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.DeleteDocByDocId(DOCID, createdby);
                return Json("Deleted Successfully", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteDocContentbyDocId(long DOCID)
        {
            try
            {
                string createdby = Session["CreatedBy"].ToString();
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.DeleteDocContentbyDocId(DOCID, createdby);
                return Json("Content Deleted Successfully", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        [AllowAnonymous]
        public ActionResult InsertMultipleDocs(string DOCID, string JobNumber, string QuotationID, string CONDMAND, string DocTemplate, string DocType, string docCode, string direction, string OJobnumber, string PaymentStatus, string ProviderMailid)
        {
            string createdby = Session["CreatedBy"].ToString();
            if (string.IsNullOrEmpty(ProviderMailid))
            {
                ProviderMailid = createdby;
            }

            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;

                HttpPostedFileBase theFile = files[0];
                long docid = 0;

                string FilePath = Path.GetExtension(theFile.FileName).ToLower();
                if (theFile.ContentLength != 0)
                {
                    if ((FilePath != ".txt" && FilePath != ".xls" && FilePath != ".xlsx" && FilePath != ".doc"
                        && FilePath != ".docx" && FilePath != ".pdf" && FilePath != ".tif"
                        && FilePath != ".jpg" && FilePath != ".jpeg" && FilePath != ".png" && FilePath != ".gif" && FilePath != ".msg"))
                    {
                        return Json(new { success = false, result = "Please refer the info icon for the allowed file types." }, "text/x-json", JsonRequestBehavior.AllowGet);
                    }
                    string path = theFile.InputStream.ToString();
                    byte[] imageSize = new byte[theFile.ContentLength];
                    theFile.InputStream.Read(imageSize, 0, (int)theFile.ContentLength);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();

                    sspdoc.FILENAME = theFile.FileName;
                    sspdoc.FILEEXTENSION = theFile.ContentType;
                    sspdoc.FILESIZE = theFile.ContentLength;
                    sspdoc.FILECONTENT = imageSize;
                    sspdoc.JOBNUMBER = Convert.ToInt64(JobNumber);
                    sspdoc.QUOTATIONID = Convert.ToInt64(QuotationID);
                    sspdoc.CONDMAND = CONDMAND;
                    sspdoc.DOCUMENTTEMPLATE = DocTemplate;
                    sspdoc.DocumentName = DocType;
                    sspdoc.DOCUMNETTYPE = docCode;
                    sspdoc.DIRECTION = direction;
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    JobBookingDbFactory mFactory = new JobBookingDbFactory();
                    Guid obj = Guid.NewGuid();                  
                    string EnvironmentName = DynamicClass.GetEnvironmentNameDocs();
                    docid = mFactory.SaveDocumentsWithParams(docentity,createdby, "Uploaded_Shipa", obj.ToString(), EnvironmentName, ProviderMailid, "YES");

                   
                }
                var JsonToReturn = new
                {
                    rows = docid,
                };
                return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, result = "You have uploded the empty file. Please upload the correct file." }, "text/x-json", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SendUploadCompleteMailToUser(string DOCID, string UploadedBy, string ShipperMailid, string operationaljobnumber, int Jobnumber, int QuoteId, string QuotationNumber)
        {
            string createdby = Session["CreatedBy"].ToString();
            UserDBFactory UserFactory = new UserDBFactory();
            try
            {
                string[] names;
                string docnames = string.Empty;
               
                if (!string.IsNullOrEmpty(DOCID)) {

                    if (DOCID.Length > 1)
                    {
                        names = DOCID.Split(',');
                        foreach (var item in names)
                        {
                            docnames = docnames + "<ul><li>" + item + "</li></ul>";
                        }
                    }
                    else {
                        docnames = DOCID;
                    }
                }
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(createdby);
                string user = createdby;
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(user));
                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Notice: Shipper" + " " + ShipperMailid + " " + "Uploaded" + " " + DOCID + " " + "for Booking" + " " + operationaljobnumber + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = Server.MapPath("~/App_Data/UploadComplte.html");
                string bodymsg = "<p>Shipper" +" "+ ShipperMailid +" "+"uploaded" +" "+DOCID+" "+ "for booking " +" "+ operationaljobnumber +".</p>"+"<p>Visit Shipa Freight to view this and other documents relating to this booking.</p>";
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{user}", UE.FIRSTNAME);
                //body = body.Replace("{user_Uploaded}", (!string.IsNullOrEmpty(UploadedBy) ? UploadedBy : string.Empty));
                //body = body.Replace("{Doc_Names}", (!string.IsNullOrEmpty(docnames) ? docnames : string.Empty));
                body = body.Replace("{body}", bodymsg);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                UserFactory.NewNotification(5120, "Shipper Uploaded Documents", operationaljobnumber, Jobnumber,QuoteId, QuotationNumber, user);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}