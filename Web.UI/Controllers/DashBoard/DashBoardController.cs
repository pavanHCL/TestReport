﻿using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.DB;
using AutoMapper;
using FindUserCountryName;
namespace FOCiS.SSP.Web.UI.Controllers.Home
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class DashBoardController : BaseController
    {
        public ActionResult Index()
        {
            TrackingDBFactory mFactory = new TrackingDBFactory();
            List<DashBoardEntity> trackentity = mFactory.GetDashBoardData(HttpContext.User.Identity.Name.ToString());
            DBModel mUIModel = new DBModel();

            foreach (var val in trackentity.Where(i=>i.Status!="Credit"))
            {
                mUIModel.BOOKINGSDONE = val.BOOKINGSDONE;
                mUIModel.TOTALVOLUME = val.TOTALVOLUME;
                mUIModel.TotalSpent = val.TotalSpent;
            }

            if (trackentity.Count(i => i.Status == "Credit") > 0)
            {
                mUIModel.CREDITLIMIT = trackentity.FirstOrDefault(i => i.Status == "Credit").TotalSpent;
            }
            else
            {
                mUIModel.CREDITLIMIT = 0;
            }
            
            return View("DashBoard", mUIModel);
        }
    }
}