﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Controllers
{
     [Error(View = "NotFound")]
    public class ErrorController : Controller
    {
        public ViewResult Index()
        {
            return View("NotFound");
        }        
	}
}