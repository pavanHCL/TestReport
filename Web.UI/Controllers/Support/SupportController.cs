﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FOCiS.SSP.Library.Extensions;
using System.ComponentModel.DataAnnotations;
using System;
using System.Reflection;
using FOCiS.SSP.Models.UserManagement;
using System.Web.Security;
using System.Net.Mail;
using FOCiS.SSP.DBFactory.Factory;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using FOCiS.SSP.Models.DB;
using System.Drawing;
using FOCiS.SSP.Models.JP;
using Chikoti.Helpers;
using FindUserCountryName;
using System.Threading.Tasks;
using FOCiS.SSP.Web.UI.Helpers;
using System.Globalization;
using System.Collections.Concurrent;


namespace FOCiS.SSP.Web.UI.Controllers.Support
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [FindUserCountry]
    public class SupportController : BaseController
    {
        [Authorize]
        public async Task<ActionResult> Index(string TabStatus = "", string SearchString = "", int PageNo = 1, string RefID = "", string ReqPage = "", string language = "")
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            if (RefID != "")
            {
                ViewBag.RefID = RefID;
                ViewBag.ReqPage = ReqPage;
            }
            else
            {
                ViewBag.RefID = "";
                ViewBag.ReqPage = "";
            }
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            UserDBFactory mFactory = new UserDBFactory();

            switch (TabStatus)
            {
                case "CLOSED":
                    TabStatus = "Closed";
                    break;
                case "PENDING REVIEW":
                    TabStatus = "Pending Review";
                    break;
                case "SUBMITTED":
                    TabStatus = "Submitted";
                    break;
            }

            SupportEntity mDBEntity = mFactory.GetSupportRequests_New(HttpContext.User.Identity.Name, TabStatus, SearchString, PageNo);
            if (uiCulture != "en")
            {
                await Task.Run(() => LocalizedConverter.GetConversionSupportDataByMultiObject<SupportEntity>(uiCulture.ToLower(), mDBEntity)).ConfigureAwait(false);
            }
            SupportRequestsEnv Model = Mapper.Map<SupportEntity, SupportRequestsEnv>(mDBEntity);
            //-----Map REQList To Model----
            List<RequestsModel> mREQModel = Mapper.Map<List<Requests>, List<RequestsModel>>(mDBEntity.Requests.ToList());
            ViewBag.PageNo = PageNo;
            ViewBag.SubTabStaus = TabStatus;
            Model.Requests = mREQModel;
            ViewBag.SearchString = SearchString;
            //Meta title and Meta description
            ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
            ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
            return View("Support", Model);
        }

        public async Task<ActionResult> _SupportChat(string REQID)
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            UserDBFactory mFactory = new UserDBFactory();
            SupportEntity mDBEntity = mFactory.GetSupportChat(REQID);
            if (uiCulture != "en")
            {
                await Task.Run(() => LocalizedConverter.GetConversionSupportDataByMultiObject<SupportEntity>(uiCulture, mDBEntity)).ConfigureAwait(false);
            }
            SupportRequestsEnv Model = Mapper.Map<SupportEntity, SupportRequestsEnv>(mDBEntity);
            //-----Map REQList To Model----
            List<ChatModel> mREQModel = Mapper.Map<List<Chat>, List<ChatModel>>(mDBEntity.Chat.ToList());

            ViewBag.TabStaus = "Requests";
            Model.Chat = mREQModel;
            return PartialView("_SupportChat", Model);
        }

        [EncryptedActionParameter]
        public void DownloadAttchment(int id)
        {
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                DataSet ds = mFactory.GetbytesDownloadAttchment(id);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    var mBytess = ds.Tables[0].Rows[0]["FILECONTENT"];
                    string FILENAME = ds.Tables[0].Rows[0]["FILENAME"].ToString();
                    FILENAME = FILENAME.Replace(",", "");
                    var mBytes = (Byte[])mBytess;
                    System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
                    Response.AddHeader("Content-disposition", "attachment; filename=" + FILENAME);
                    Response.ContentType = "application/octet-stream";
                    Response.BinaryWrite(mBytes);
                    Response.Clear();
                    Response.End();
                    Response.Close();
                }

            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        [HttpPost]
        public ActionResult UploadFiles(string seqno = "")
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    string Dates = DateTime.Today.ToString("yyyyMMdd");
                    string users = HttpContext.User.Identity.Name.ToString();
                    string FilePaths = Path.Combine(Server.MapPath("~/UploadedFiles/"), Dates);
                    bool existss = System.IO.Directory.Exists(FilePaths);
                    if (existss)
                    {
                        FilePaths = FilePaths + "\\" + users;
                        bool userfolderexistss = System.IO.Directory.Exists(FilePaths);
                        if (userfolderexistss)
                        {
                            if (Directory.Exists(FilePaths))
                                GC.Collect();
                            GC.WaitForPendingFinalizers();
                            Directory.Delete(FilePaths, true);
                        }
                    }
                    string returnjson = string.Empty;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string FilePathExtn = Path.GetExtension(file.FileName).ToLower();
                        if (file.ContentLength != 0)
                        {
                            if ((FilePathExtn != ".txt" && FilePathExtn != ".xls" && FilePathExtn != ".xlsx" && FilePathExtn != ".doc"
                                && FilePathExtn != ".docx" && FilePathExtn != ".pdf" && FilePathExtn != ".tif" && FilePathExtn != ".jpg"
                                && FilePathExtn != ".jpeg" && FilePathExtn != ".png" && FilePathExtn != ".gif" && FilePathExtn != ".msg"))
                            {
                                returnjson = "False";
                                break;
                            }
                        }
                    }
                    if (returnjson != "False")
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname;
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            else
                            {
                                fname = file.FileName;
                            }
                            string Date = DateTime.Today.ToString("yyyyMMdd");
                            string user = HttpContext.User.Identity.Name.ToString();
                            string FilePath = Path.Combine(Server.MapPath("~/UploadedFiles/"), Date);
                            bool exists = System.IO.Directory.Exists(FilePath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(FilePath);

                            FilePath = FilePath + "\\" + user;
                            bool userfolderexists = System.IO.Directory.Exists(FilePath);
                            if (!userfolderexists)
                                System.IO.Directory.CreateDirectory(FilePath);

                            if (seqno != null && seqno != "")
                            {
                                FilePath = FilePath + "\\" + seqno;
                                bool usersubfolderexists = System.IO.Directory.Exists(FilePath);
                                if (!usersubfolderexists)
                                    System.IO.Directory.CreateDirectory(FilePath);
                            }
                            fname = Path.Combine(FilePath, fname);
                            file.SaveAs(fname);
                        }
                        return Json("File Uploaded Successfully!");
                    }
                    else { return Json("Files not Uploaded due to invalid Filetypes..!"); }
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        [HttpPost]
        [Authorize]
        public async Task<JsonResult> SaveTicket(string ReqID, string Module, string Type, string ReferenceID, string Query, string Description, string ModuleCode)
        {
            string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
            UserDBFactory dbfac = new UserDBFactory();
            string reqSeqNo = string.Empty;
            if (uiCulture != "en")
            {
                string[] ConvertModule = await Task.Run(() => LocalizedConverter.GetConversionDataBySingleObject<string[]>(uiCulture, Module, Type)).ConfigureAwait(false);
                reqSeqNo = dbfac.SaveTicket(ConvertModule[0], ConvertModule[1], ReferenceID, Query, Description, HttpContext.User.Identity.Name.ToString(), ModuleCode);
            }
            else
            {
                reqSeqNo = dbfac.SaveTicket(Module, Type, ReferenceID, Query, Description, HttpContext.User.Identity.Name.ToString(), ModuleCode);
            }
            string Date = DateTime.Today.ToString("yyyyMMdd");
            string user = HttpContext.User.Identity.Name.ToString();
            string FilePath = Path.Combine(Server.MapPath("~/UploadedFiles/"), Date);
            FilePath = FilePath + "\\" + user;
            bool folderexists = System.IO.Directory.Exists(FilePath);
            if (folderexists)
            {
                var dir = new System.IO.DirectoryInfo(FilePath);
                System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
                List<byte[]> items = new List<byte[]>();
                foreach (var file in fileNames)
                {
                    string filebyte = FilePath + "\\" + file;
                    byte[] fileBytes = GetFile(filebyte);
                    items.Add(fileBytes);
                    dbfac.SaveTicketDocuemnts(ReferenceID, HttpContext.User.Identity.Name.ToString(), reqSeqNo, fileBytes, file.Name, "", "Request");
                }
                if (Directory.Exists(FilePath))
                    GC.Collect();
                GC.WaitForPendingFinalizers();
                Directory.Delete(FilePath, true);
            }

            string Message = "Customer support Request has been done by the user : " + HttpContext.User.Identity.Name.ToString();

            SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + reqSeqNo + ")", Message, reqSeqNo, "", "CUSTOMERSUPPORT", HttpContext.User.Identity.Name.ToString()); //string User,string Email,string Subject,string Description,string RefNumber

            var JsonToReturn = new
            {
                rows = "success",
            };

            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Authorize]
        public JsonResult InsertNewChat(string ChatMes, string REQID, string SEQNO, string MODULE)
        {
            UserDBFactory DB = new UserDBFactory();

            string Date = DateTime.Today.ToString("yyyyMMdd");
            string user = HttpContext.User.Identity.Name.ToString();
            string FilePath = Path.Combine(Server.MapPath("~/UploadedFiles/"), Date);
            FilePath = FilePath + "\\" + user;
            bool existss = System.IO.Directory.Exists(FilePath);
            if (existss)
            {
                FilePath = FilePath + "\\" + SEQNO;
                bool subexistss = System.IO.Directory.Exists(FilePath);
                if (subexistss)
                {
                    var dir = new System.IO.DirectoryInfo(FilePath);
                    System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
                    List<byte[]> items = new List<byte[]>();
                    foreach (var file in fileNames)
                    {
                        string filebyte = FilePath + "\\" + file;
                        byte[] fileBytes = GetFile(filebyte);
                        items.Add(fileBytes);

                        DB.SaveTicketDocuemnts("", HttpContext.User.Identity.Name.ToString(), SEQNO, fileBytes, file.Name, ChatMes, "Communication");
                        ChatMes = "";
                    }
                }
                else
                {
                    DB.SaveOrCloseChat(ChatMes, REQID, HttpContext.User.Identity.Name.ToString(), 0);
                    SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + SEQNO + ")", ChatMes, SEQNO, "", "CUSTOMERSUPPORT", HttpContext.User.Identity.Name.ToString()); //string User,string Email,string Subject,string Description,string RefNumber
                }
            }
            else
            {
                DB.SaveOrCloseChat(ChatMes, REQID, HttpContext.User.Identity.Name.ToString(), 0);
                SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + SEQNO + ")", ChatMes, SEQNO, "", "CUSTOMERSUPPORT", HttpContext.User.Identity.Name.ToString()); //string User,string Email,string Subject,string Description,string RefNumber

            }

            var JsonToReturn = new
            {
                rows = "success",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult deletefolders(string SEQNO)
        {
            string Dates = DateTime.Today.ToString("yyyyMMdd");
            string users = HttpContext.User.Identity.Name.ToString();
            string FilePaths = Path.Combine(Server.MapPath("~/UploadedFiles/"), Dates);
            bool existss = System.IO.Directory.Exists(FilePaths);
            if (existss)
            {
                FilePaths = FilePaths + "\\" + users;
                bool userfolderexistss = System.IO.Directory.Exists(FilePaths);
                if (userfolderexistss)
                {
                    if (Directory.Exists(FilePaths))
                        GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Directory.Delete(FilePaths, true);
                }
            }
            var JsonToReturn = new
            {
                rows = "success",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult CloseChat(string REQID)
        {
            UserDBFactory DB = new UserDBFactory();
            DB.SaveOrCloseChat("", REQID, HttpContext.User.Identity.Name.ToString(), 1);
            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult UploadDocuments()
        {
            //Credit supporting docs
            byte[] fileData = null;
            long docid = 0;
            string downloadLink = "";
            if (Request.Files != null && Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    using (var binaryReader = new BinaryReader(Request.Files[i].InputStream))
                    {
                        if (Request.Files[i].ContentLength != 0)
                        {
                            fileData = binaryReader.ReadBytes(Request.Files[i].ContentLength);

                            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                            sspdoc.CREATEDBY = HttpContext.User.Identity.Name.ToString();
                            sspdoc.FILENAME = Request.Files[i].FileName;
                            sspdoc.FILEEXTENSION = Request.Files[i].ContentType;
                            sspdoc.FILESIZE = Request.Files[i].ContentLength;
                            sspdoc.FILECONTENT = fileData;
                            sspdoc.DocumentName = Request.Files[i].FileName;
                            sspdoc.DOCUMNETTYPE = Request.Files[i].FileName;
                            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                            UserDBFactory creditFactory = new UserDBFactory();
                            docid = creditFactory.SaveDocumentsWithParams(docentity, HttpContext.User.Identity.Name, "", "NO");
                            downloadLink = MyExtensions.EncodedDownloadLink(Request.Files[i].FileName, "DownloadCreditDoc", "UserManagement", new { filename = Request.Files[i].FileName }, null, "_blank", 0);
                        }
                    }
                }

            }

            var JsonToReturn = new
            {
                uploaddocId = docid,
                docdownloadlink = downloadLink
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public void SendMailtoGSSC(string User)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            message.To.Add(new MailAddress("help@shipafreight.com"));
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Request for Business Credit" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/GSSC-CreditRequest.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{User}", User);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
           "UserManagement", "Request for Business Credit", "", HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject.ToString());
        }


        public void SendMailFocisCustSupport(string User, string Roles, string Subject, string Description, string RefNumber, string JobNumber = "", string NotifyType = "", string CreatedBy = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            if (Roles.Contains("@"))
            {
                message.To.Add(Roles);
            }
            else
            {
                recipientsList = mFactory.GetEmailList(Roles, JobNumber, NotifyType, "", CreatedBy);
                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
            }
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            message.Subject = Subject + EnvironmentName;


            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = Server.MapPath("~/App_Data/CustomerSupport-Focis.html");

            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{User}", User);
            body = body.Replace("{Message}", Description);
            body = body.Replace("{Reference_No}", RefNumber);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            if (Roles.Contains("@"))
            {

                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Roles, "", "",
            "UserManagement", "Customer support", RefNumber, HttpContext.User.Identity.Name.ToString(), body, "", true, message.Subject.ToString());
            }
        }


        public ActionResult AlternateEmails(string AlternateEmails, string Type, string partyname, long partyid)
        {
            MDMDataFactory mdmFactory = new MDMDataFactory();
            string message = mdmFactory.AddTeammembers(AlternateEmails, Type, HttpContext.User.Identity.Name, partyname, partyid);
            return Json(message, JsonRequestBehavior.AllowGet);
        }


        public void SendMailForGSSCQuotes(string reqSeqNo)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            Email = Email + ";" + HttpContext.User.Identity.Name.ToString() + ";" + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string EnvironmentName = DynamicClass.GetEnvironmentName();
            string subject = "";
            subject = "Shipa Freight- Customer support Request " + EnvironmentName;
            string body = string.Empty;

            string mapPath = Server.MapPath("~/App_Data/CustomerSupport.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{user_name}", (Request.Cookies["myCookie"].Values["UserName"].ToString()));
            body = body.Replace("{Message}", "Customer support Request has been done by the user : " + HttpContext.User.Identity.Name.ToString() + " and Customer support Request Number :");
            body = body.Replace("{Reference_No}", reqSeqNo);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMail(Email, body, subject, "support", reqSeqNo, false);
        }

        public void SendMail(string Email, string body, string subject, string submodule, string referenceno, bool IsRequired)
        {
            MailMessage message = new MailMessage();
            string to = string.Empty;
            if (Email.Contains(';'))
            {
                for (int i = 0; i <= (Email.Split(';').Length - 1); i++)
                {
                    message.To.Add(new MailAddress(Email.Split(';')[i]));
                    to = Email.Split(';')[i] + ",";
                }
            }
            else
            {
                message.To.Add(new MailAddress(Email));
                to = Email;
            }

            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", to, "", "",
      "UserManagement", submodule, referenceno, HttpContext.User.Identity.Name.ToString(), body, "", IsRequired, message.Subject.ToString());
            var nJSON = new JsonResult();
            nJSON.Data = true;
        }


        /// <summary>
        /// This Ajax Request will fetech User profile picture for showing logged-in header
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBasestring()
        {
            if (Session["BaseIm"] == null || Session["BaseIm"] == "")
            {
                UserDBFactory UDF = new UserDBFactory();
                DataTable dt = UDF.GetUserImage(HttpContext.User.Identity.Name.ToString());

                DataTable Dt1 = new DataTable();
                Dt1.Columns.Add("BASESTRING", typeof(string));
                Dt1.Columns.Add("IMGFILENAME", typeof(string));
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][1].ToString() != "")
                    {
                        byte[] bt = (byte[])dt.Rows[0][1];
                        string base64 = Convert.ToBase64String(bt);

                        Dt1.Rows.Add(base64, dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1]);
                        Session["BaseIm"] = (string)base64; Session["filetype"] = dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1];
                    }
                }
                else
                {
                    Session["BaseIm"] = null; Session["filetype"] = null;
                }
            }

            var JsonToReturn = new
            {
                rows = "",
            };
            return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EncryptId(string plainText)
        {
            StringBuilder Result = new StringBuilder();

            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

            string[] mul = plainText.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            string JResult = Result.ToString();
            return Json(JResult, JsonRequestBehavior.AllowGet);
        }

        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper().Contains("DATE"))
                    {
                        if (dr[col].ToString().Trim() != "")
                        {
                            DateTime colDateTime = Convert.ToDateTime(Convert.ToString(dr[col]));
                            temp_row.Add(col.ColumnName, string.IsNullOrEmpty(Convert.ToString(colDateTime)) ? string.Empty : colDateTime.ToString("dd-MMM-yyyy hh:mm tt"));
                        }
                        else
                        {
                            temp_row.Add(col.ColumnName, dr[col]);
                        }
                    }
                    else
                    {
                        temp_row.Add(col.ColumnName, dr[col]);
                    }
                }
                trows.Add(temp_row);
            }
            return trows;
        }

    }
}

#region Commented code
//IList<CreditEntity> UserCredits = new List<CreditEntity>();
//UserCredits = mDBEntity.CreditItems;
//if (UserCredits != null && UserCredits.Count > 0)
//{
//    mDBEntity.CREDITAPPID = UserCredits[0].CREDITAPPID;
//    mDBEntity.INDUSTRYID = UserCredits[0].INDUSTRYID;
//    mDBEntity.NOOFEMP = UserCredits[0].NOOFEMP;
//    mDBEntity.PARENTCOMPANY = UserCredits[0].PARENTCOMPANY;
//    if (UserCredits[0].ANNUALTURNOVER != 0)
//    {
//        mDBEntity.ANNUALTURNOVERCURRENCY = UserCredits[0].ANNUALTURNOVER.ToString();
//    }
//    mDBEntity.DBNUMBER = UserCredits[0].DBNUMBER;
//    mDBEntity.HFMENTRYCODE = UserCredits[0].HFMENTRYCODE;
//    if (UserCredits[0].REQUESTEDCREDITLIMIT != 0)
//    {
//        mDBEntity.REQUESTEDCREDITLIMITCURRENCY = UserCredits[0].REQUESTEDCREDITLIMIT.ToString();
//    }
//    mDBEntity.CURRENTPAYTERMSNAME = UserCredits[0].CURRENTPAYTERMSNAME;
//    mDBEntity.COMMENTS = UserCredits[0].COMMENTS;
//    mDBEntity.CURRENCYID = UserCredits[0].CURRENCYID;
//    mDBEntity.APPROVEDCREDITLIMIT = UserCredits[0].APPROVEDCREDITLIMIT;
//    mDBEntity.CURRENTOSBALANCE = UserCredits[0].CURRENTOSBALANCE;
//}
//DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(HttpContext.User.Identity.Name);
//IList<UMUserAddressDetailsEntity> UserAdr = new List<UMUserAddressDetailsEntity>();
//UserAdr = mDBEntity.UserAddress;

//if (UserAdr != null && UserAdr.Count > 0)
//{
//    ViewBag.Useraddressdata = 1;
//    mDBEntity.UAUSERADTLSID = UserAdr[0].USERADTLSID;
//    mDBEntity.UAADDRESSTYPE = UserAdr[0].ADDRESSTYPE;
//    mDBEntity.UAHOUSENO = UserAdr[0].HOUSENO;
//    mDBEntity.UABUILDINGNAME = UserAdr[0].BUILDINGNAME;
//    mDBEntity.UAADDRESSLINE1 = UserAdr[0].ADDRESSLINE1;
//    mDBEntity.UAADDRESSLINE2 = UserAdr[0].ADDRESSLINE2;
//    mDBEntity.UACOUNTRYID = UserAdr[0].COUNTRYID;
//    mDBEntity.UACOUNTRYCODE = UserAdr[0].COUNTRYCODE;
//    mDBEntity.UACOUNTRYNAME = UserAdr[0].COUNTRYNAME;
//    mDBEntity.UASTATEID = UserAdr[0].STATEID;
//    mDBEntity.UASTATECODE = UserAdr[0].STATECODE;
//    mDBEntity.UASTATENAME = UserAdr[0].STATENAME;
//    mDBEntity.UACITYID = UserAdr[0].CITYID;
//    mDBEntity.UACITYCODE = UserAdr[0].CITYCODE;
//    mDBEntity.UACITYNAME = UserAdr[0].CITYNAME;
//    mDBEntity.UAPOSTCODE = UserAdr[0].POSTCODE;
//    mDBEntity.UAFULLADDRESS = UserAdr[0].FULLADDRESS;
//    mDBEntity.UAISDEFAULT = UserAdr[0].ISDEFAULT;
//    mDBEntity.COUNTRYNAME = UserAdr[0].COUNTRYNAME;
//}

//IList<CreditEntity> UserCredits = new List<CreditEntity>();
//UserCredits = mDBEntity.CreditItems;
//if (UserCredits != null && UserCredits.Count > 0)
//{
//    mDBEntity.CREDITAPPID = UserCredits[0].CREDITAPPID;
//    mDBEntity.INDUSTRYID = UserCredits[0].INDUSTRYID;
//    mDBEntity.NOOFEMP = UserCredits[0].NOOFEMP;
//    mDBEntity.PARENTCOMPANY = UserCredits[0].PARENTCOMPANY;
//    if (UserCredits[0].ANNUALTURNOVER != 0)
//    {
//        mDBEntity.ANNUALTURNOVERCURRENCY = UserCredits[0].ANNUALTURNOVER.ToString();
//    }
//    mDBEntity.DBNUMBER = UserCredits[0].DBNUMBER;
//    mDBEntity.HFMENTRYCODE = UserCredits[0].HFMENTRYCODE;
//    if (UserCredits[0].REQUESTEDCREDITLIMIT != 0)
//    {
//        mDBEntity.REQUESTEDCREDITLIMITCURRENCY = UserCredits[0].REQUESTEDCREDITLIMIT.ToString();
//    }
//    mDBEntity.CURRENTPAYTERMSNAME = UserCredits[0].CURRENTPAYTERMSNAME;
//    mDBEntity.COMMENTS = UserCredits[0].COMMENTS;
//    mDBEntity.CURRENCYID = UserCredits[0].CURRENCYID;
//    mDBEntity.APPROVEDCREDITLIMIT = UserCredits[0].APPROVEDCREDITLIMIT;
//    mDBEntity.CURRENTOSBALANCE = UserCredits[0].CURRENTOSBALANCE;
//}


//IList<DepartmentEntity> Departments = mdmFactory.GetOrganizationalUnits(string.Empty, string.Empty);
//IList<JobTitleEntity> JobTitleData = mFactory.GetJobTitleDetails(string.Empty);
//  ViewBag.JobTitleData = JobTitleData;
// ViewBag.Departments = Departments;
//ViewBag.LoadImage = (mDBEntity.IMGCONTENT != null && mDBEntity.IMGCONTENT.Length > 0) ? Convert.ToBase64String(mDBEntity.IMGCONTENT) : "";
#endregion