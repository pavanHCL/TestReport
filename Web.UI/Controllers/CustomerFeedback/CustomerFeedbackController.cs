﻿using AutoMapper;
using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.Tracking;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOCiS.SSP.DBFactory.Entities;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Library.Extensions;
using FOCiS.SSP.Web.UI.Helpers;
using FindUserCountryName;

namespace FOCiS.SSP.Web.UI.Controllers.CustomerFeedback
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class CustomerFeedbackController : BaseController
    {
        [EncryptedActionParameter]
        public ActionResult Index(string ConsignmentId, int Jobnumber)
        {            
            TrackingDBFactory mFactory = new TrackingDBFactory();
            List<DBFactory.Entities.TrackingEntity> mDBEntity = mFactory.GetTrackDetails(HttpContext.User.Identity.Name, ConsignmentId, Convert.ToString(Jobnumber));
            List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);
            ListTrackingModel tm = new ListTrackingModel();
            tm.TrackingItems = mUIModel;
            return View("CustomerFeedback", tm);
        }
        /// <summary>
        /// This event insert entered event comments into DB
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Comments"></param>
        /// <returns></returns>
        public void InsertFeedBackComments(string business, string ConfirmGoods, string Schedule, string TeamSuppport, string Material, string Payment,
        string businessRatings, string ConfirmGoodsRatings, string ScheduleRatings, string TeamSuppportRatings, string MaterialRatings, string PaymentRatings,
            string jobnumber, string ConsignmentId,string NPS)
        {
            //DbFactory object intiation
            TrackingDBFactory mFactory = new TrackingDBFactory();
            mFactory.InsertFeedBackComments(business, ConfirmGoods, Schedule, TeamSuppport, Material, Payment,
                businessRatings, ConfirmGoodsRatings, ScheduleRatings, TeamSuppportRatings, MaterialRatings, PaymentRatings, jobnumber,NPS);

            TempData["ConsignmentId"] = ConsignmentId;
            TempData["jobnumber"] = jobnumber;
        }

        [HttpPost]
        public ActionResult Feedback()
        {
            return RedirectToAction("TrackSummary", "Tracking");
        }
        [AllowAnonymous]
        [EncryptedActionParameter]
        public FileResult Preview(string q)
        {
            string decrptedString = Encryption.Decrypt(q.ToString());
            string[] paramArr = decrptedString.Split('=');

            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                IList<SSPDocumentsEntity> mDBEntity = mFactory.GetDocumentDetailsbyDocumetnId(paramArr[1],HttpContext.User.Identity.Name.ToString());
                byte[] bytes = mDBEntity.FirstOrDefault().FILECONTENT;
                return File(bytes, mDBEntity.FirstOrDefault().FILEEXTENSION, mDBEntity.FirstOrDefault().FILENAME.ToString());
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        #region Unsubscribe from notification mails
        [AllowAnonymous]
        public ActionResult UnsubscribefromEmailNotifications(string userId)
        {
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                //string user = AESEncrytDecry.DecryptStringAES(userId);
                mFactory.UpdateUserforUnsubscribefromEmailNotifications(userId);
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.Message);
            }
            return View();
        }
        #endregion
    }
}