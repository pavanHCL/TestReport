﻿using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Library.Extensions;
using FOCiS.SSP.Web.UI.Helpers;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace FOCiS.SSP.Web.UI.Controllers.Base
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BaseController : Controller
    {
        private static readonly ILog Log =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string ApiUrl
        {
            get { return ShipaConfiguration.InternalUrls.ShipaApiUrl; }
        }

        public BaseController()
        {
            ViewBag.BodyClass = "agility-quote-bg agility-illus-bg";
        }
        public ActionResult RedirectToActionSecure(string action, string controller, object args)
        {
            string queryString = string.Empty;
            if (args != null)
            {
                RouteValueDictionary d = new RouteValueDictionary(args);
                for (int i = 0; i < d.Keys.Count; i++)
                {
                    if (!d.Keys.Contains("IsRoute"))
                    {
                        if (i > 0)
                        {
                            queryString += "?";
                        }
                        queryString += d.Keys.ElementAt(i) + "=" + d.Values.ElementAt(i);
                    }
                    else
                    {
                        if (!d.Keys.ElementAt(i).Contains("IsRoute"))
                        {
                            queryString += d.Values.ElementAt(i);
                        }
                    }
                }
            }
            var encText = AESEncrytDecry.EncryptStringAES(queryString);
            return RedirectToAction(action, controller, new { q = encText });
        }

        public async Task<T> GetDataFromApi<T>(string endpoint, string method, T pun)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(endpoint);
                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Method = method;
                string inputJson = Newtonsoft.Json.JsonConvert.SerializeObject(pun);
                byte[] bytes = Encoding.UTF8.GetBytes(inputJson);
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }
                string json;
                using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    {
                        json = (new StreamReader(stream)).ReadToEnd();

                    }
                }
                var responseObject = JsonConvert.DeserializeObject<T>(json.ToString());

                return await Task.Run(() => responseObject).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public bool IsList(object o)
        {
            return o is IList &&
                   o.GetType().IsGenericType &&
                  o.GetType().GetGenericTypeDefinition() == typeof(List<>);
        }

        /// <summary>
        /// Generic Exception Handler written for OneSignal
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objAction"></param>
        /// <returns></returns>
        protected T SafeRun<T>(Func<T> objAction)
        {
            try
            {
                return objAction();
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(String.Format("SafeRun Message: {0},  {1}", ex.Message.ToString(), DateTime.Now.ToString()));
                Log.ErrorFormat(String.Format("SafeRun StackTrace: {0},  {1}", ex.StackTrace.ToString(), DateTime.Now.ToString()));
                return default(T);
            }
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = GetCurrentCulture();
            if (!String.IsNullOrEmpty(lang))
            {
                new Languages().SetLanguage(lang);
            }
            return base.BeginExecuteCore(callback, state);
        }

        private string GetCurrentCulture()
        {
            string lang = null;
            string defaultLanguage = "en-us";
            if (Request.UserLanguages != null)
            {
                defaultLanguage = Request.UserLanguages[0];
            }
            if (RouteData.Values["controller"].ToString() == "LearnMore" && (RouteData.Values["action"].ToString() == "_CookiePolicy" || RouteData.Values["action"].ToString() == "_TermsandConditions"))
            {
                return null;
            }
            else if (RouteData.Values["controller"].ToString() == "Quotation" && (RouteData.Values["action"].ToString() == "Sanctioned" || RouteData.Values["action"].ToString() == "GetIncoTermsByMovementType" || RouteData.Values["action"].ToString() == "GetCountriesListFromCache" || RouteData.Values["action"].ToString() == "images"))
            {
                return null;
            }
            HttpCookie langCookie = Request.Cookies["shipaculture"];
            if (langCookie != null)
            {
                return lang = langCookie.Value;
            }
            else
            {
                return lang = defaultLanguage.ToLower();
            }
           
        }

    }


}