﻿using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Controllers.Sitemap
{
    public class SitemapController : BaseController
    {
        public ActionResult Index()
        {
            //for canonical tag 
            ViewBag.MetaTitle = "Sitemap | Quote, Book, Track | Shipafreight.com";
            ViewBag.MetaDescription = "The Sitemap enlists all the important website pages for Shipafreight.com. Shipa Freight offers one stop online freight Platform for Quote booking, Payment and Tracking. Visit now!";
            ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "sitemap";
            return View("sitemap");
        }

    }
}