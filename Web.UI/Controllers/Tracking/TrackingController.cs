﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FOCiS.SSP.Library.Extensions;
using System.ComponentModel.DataAnnotations;
using System;
using System.Reflection;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.Tracking;
using System.Net.Mail;
using System.IO;
using System.Data;
using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using System.Globalization;
using FOCiS.SSP.Models.JP;
using System.Text;
using System.Security.Cryptography;
using FOCiS.SSP.Web.UI.Helpers;
using FindUserCountryName;


namespace FOCiS.SSP.Web.UI.Controllers.Tracking
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    [FindUserCountry]
    public class TrackingController : BaseController
    {
        ITrackingDBFactory _tdb;
        IJobBookingDbFactory _jdb;

        public TrackingController()
        {
            _tdb = new TrackingDBFactory();
            _jdb = new JobBookingDbFactory();
        }

        public TrackingController(ITrackingDBFactory tdb, IJobBookingDbFactory jdb)
        {
            _tdb = tdb;
            _jdb = jdb;
        }

        /// <summary>
        /// This event fetech all the details and pass model to view : Anil G
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int? id, string status, string SearchString = "", int PageNo = 1)
        {
            try
            {
                if (status == null)
                {
                    status = "ACTIVE";
                }
                ViewBag.SearchString = SearchString;
                ViewBag.PageNo = PageNo;
                SearchString = SearchString.Replace("'", "''");
                string name = RegionInfo.CurrentRegion.DisplayName;
                //DbFactory object intiation and collect data from DB
                // TrackingDBFactory mFactory = new TrackingDBFactory();
                ListTrackingEntity trackentity = _tdb.ReturnTrackEntityList(HttpContext.User.Identity.Name, "", 0, status, PageNo, SearchString.ToUpper());

                //Main Data(Quote,Job Booking)
                List<DBFactory.Entities.TrackingEntity> mDBEntity = trackentity.TrackingItems.ToList();
                List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);

                //Package Details Lsit
                List<DBFactory.Entities.PackageDetailsEntity> mDBEntityLinq = (from w1 in trackentity.PackageDetailsItems where trackentity.TrackingItems.Any(w2 => w1.JobNumber == w2.JOBNUMBER) select w1).ToList();

                List<PackageDetailsModel> mDPackageModel = Mapper.Map<List<DBFactory.Entities.PackageDetailsEntity>, List<PackageDetailsModel>>(mDBEntityLinq);

                //Track Service
                List<DBFactory.Entities.Track> mDBEntityTrackLinq = (from w1 in trackentity.TrackItems where trackentity.TrackingItems.Any(w2 => w1.ConsignmentId == w2.CONSIGNMENTID) select w1).ToList();
                List<TrackModel> mDTrackModel = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(mDBEntityTrackLinq);
                List<DBFactory.Entities.TrackStatusCount> EntityCount = _tdb.GetTrackStatusCount(HttpContext.User.Identity.Name, SearchString);
                ViewBag.TrackStatusCount = EntityCount;
                ListTrackingModel tm = new ListTrackingModel();
                tm.TrackingItems = mUIModel;
                tm.PackageDetailsItems = mDPackageModel;
                tm.TrackItems = mDTrackModel;
                //add current view to session
                Session["TrackingList"] = (ListTrackingModel)tm;
                //for canonical tag             
                ViewBag.CanonicalURL = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "Tracking";
                //Meta title and Meta description
                ViewBag.MetaTitle = "Track Freight Shipment | Shipafreight.com";
                ViewBag.MetaDescription = "Track your shipments online at Shipa Freight. It is an online freight platform to help you get quick air and ocean freight quotes, book, pay, track online.";
                return View("TrackingList", tm);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        /// <summary>
        /// (Event Picks All Tracking Details for selected Quote)--Anil.G
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [EncryptedActionParameter]
        public ActionResult TrackSummary(int? Jobnumber, string ConsignmentId = "")
        {
            try
            {
                if (Jobnumber == null)
                {
                    ConsignmentId = TempData["ConsignmentId"].ToString();
                    Jobnumber = Convert.ToInt32(TempData["jobnumber"]);
                }
                ListTrackingModel tm = new ListTrackingModel();
                ListTrackingModel SessionView = (ListTrackingModel)Session["TrackingList"];
                //Main Call
                //TrackingDBFactory mFactory = new TrackingDBFactory();
                //DbFactory object intiation
                ListTrackingEntity trackentity = _tdb.ReturnTrackEntityList(HttpContext.User.Identity.Name, ConsignmentId, Convert.ToInt32(Jobnumber), "", 0, "");
                //Main Data(Quote,Job Booking)
                List<DBFactory.Entities.TrackingEntity> mDBEntity = trackentity.TrackingItems.ToList();
                List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);
                //Package Details Lsit
                List<DBFactory.Entities.PackageDetailsEntity> mDPackageEntity = trackentity.PackageDetailsItems.ToList();
                List<PackageDetailsModel> mDPackageModel = Mapper.Map<List<DBFactory.Entities.PackageDetailsEntity>, List<PackageDetailsModel>>(mDPackageEntity);
                //Service
                List<DBFactory.Entities.Track> EntityTrack = trackentity.TrackItems.ToList();
                List<TrackModel> mDTrackModel = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(EntityTrack);
                // Party Details
                List<DBFactory.Entities.PartyDetails> mDPartyBEntity = _tdb.GetPartyDetails(HttpContext.User.Identity.Name, Convert.ToInt64(Jobnumber));
                List<TrackingPartyDetailsModel> mUIPartyModel = Mapper.Map<List<DBFactory.Entities.PartyDetails>, List<TrackingPartyDetailsModel>>(mDPartyBEntity);
                //Comments
                List<DBFactory.Entities.ShpimentEventComments> MShpEvntCmnts = _tdb.GetShipmentEventComments(HttpContext.User.Identity.Name, Convert.ToInt32(Jobnumber), ConsignmentId);
                List<ShpimentEventCommentsModel> MUIShpcmnts = Mapper.Map<List<DBFactory.Entities.ShpimentEventComments>, List<ShpimentEventCommentsModel>>(MShpEvntCmnts);
                //Uploaded Documents
                List<DBFactory.Entities.SSPDocumentsEntity> DocumentsE = _tdb.GetUploadedDocuments(HttpContext.User.Identity.Name, Convert.ToInt32(Jobnumber), ConsignmentId);
                List<SSPDocumentsModel> SSPDocuments = Mapper.Map<List<DBFactory.Entities.SSPDocumentsEntity>, List<SSPDocumentsModel>>(DocumentsE);
                tm.TrackingItems = mUIModel;
                tm.PackageDetailsItems = mDPackageModel;
                tm.TrackItems = mDTrackModel;
                tm.TrackingPartyItems = mUIPartyModel;
                tm.shipmentCommentsItems = MUIShpcmnts;
                tm.DocumentDetails = SSPDocuments;
                Session["Comments"] = MUIShpcmnts;
                Session["TrackingList"] = (ListTrackingModel)tm;
                //Meta title and Meta description
                ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
                ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";
                ViewBag.JOBNUMBER = Convert.ToInt64(mUIModel.FirstOrDefault().JOBNUMBER);
                ViewBag.CONSIGNMENTID = Convert.ToString(mUIModel.FirstOrDefault().CONSIGNMENTID);
                return View("TrackingSummary", tm);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }


        [Authorize]
        public ActionResult ShipaTracking(string BookingID = "", string ConsignmentId = "")
        {
            try
            {
                ListTrackingModel tm = new ListTrackingModel();
                //Main Call
                //DbFactory object intiation
                ListTrackingEntity trackentity = _tdb.ShipmentTracking(HttpContext.User.Identity.Name, ConsignmentId, BookingID);

                if (trackentity.TrackingItems == null || trackentity.TrackingItems.Count() == 0)
                    return View("TrackingSummary", tm);

                //Main Data(Quote,Job Booking)
                List<DBFactory.Entities.TrackingEntity> mDBEntity = trackentity.TrackingItems.ToList();
                List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);
                //Package Details Lsit
                List<DBFactory.Entities.PackageDetailsEntity> mDPackageEntity = trackentity.PackageDetailsItems.ToList();
                List<PackageDetailsModel> mDPackageModel = Mapper.Map<List<DBFactory.Entities.PackageDetailsEntity>, List<PackageDetailsModel>>(mDPackageEntity);
                //Service
                List<DBFactory.Entities.Track> EntityTrack = trackentity.TrackItems.ToList();
                List<TrackModel> mDTrackModel = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(EntityTrack);
                // Party Details
                List<DBFactory.Entities.PartyDetails> mDPartyBEntity = _tdb.GetPartyDetails(HttpContext.User.Identity.Name, Convert.ToInt64(mUIModel.FirstOrDefault().JOBNUMBER));
                List<TrackingPartyDetailsModel> mUIPartyModel = Mapper.Map<List<DBFactory.Entities.PartyDetails>, List<TrackingPartyDetailsModel>>(mDPartyBEntity);
                //Comments
                List<DBFactory.Entities.ShpimentEventComments> MShpEvntCmnts = _tdb.GetShipmentEventComments(HttpContext.User.Identity.Name, Convert.ToInt32(mUIModel.FirstOrDefault().JOBNUMBER), ConsignmentId);
                List<ShpimentEventCommentsModel> MUIShpcmnts = Mapper.Map<List<DBFactory.Entities.ShpimentEventComments>, List<ShpimentEventCommentsModel>>(MShpEvntCmnts);
                //Uploaded Documents
                List<DBFactory.Entities.SSPDocumentsEntity> DocumentsE = _tdb.GetUploadedDocuments(HttpContext.User.Identity.Name, Convert.ToInt32(mUIModel.FirstOrDefault().JOBNUMBER), ConsignmentId);
                List<SSPDocumentsModel> SSPDocuments = Mapper.Map<List<DBFactory.Entities.SSPDocumentsEntity>, List<SSPDocumentsModel>>(DocumentsE);
                tm.TrackingItems = mUIModel;
                tm.PackageDetailsItems = mDPackageModel;
                tm.TrackItems = mDTrackModel;
                tm.TrackingPartyItems = mUIPartyModel;
                tm.shipmentCommentsItems = MUIShpcmnts;
                tm.DocumentDetails = SSPDocuments;
                Session["Comments"] = MUIShpcmnts;
                Session["TrackingList"] = (ListTrackingModel)tm;
                //Meta title and Meta description
                ViewBag.MetaTitle = "Shipa Freight | Quote, book, pay and track freight online";
                ViewBag.MetaDescription = "Shipa Freight is a new online platform powered by Agility that make it easy to get air and ocean freight quotes,book, pay and track your shipments online.";

                ViewBag.JOBNUMBER = Convert.ToInt64(mUIModel.FirstOrDefault().JOBNUMBER);
                ViewBag.CONSIGNMENTID = Convert.ToString(mUIModel.FirstOrDefault().CONSIGNMENTID);
                return View("TrackingSummary", tm);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        [Authorize]
        public void TrackDownLoad(string docid)
        {
            try
            {
                //JobBookingDbFactory mFactory = new JobBookingDbFactory();
                IList<SSPDocumentsEntity> mDBEntity = _jdb.GetDocumentDetailsbyDocumetnId(docid, HttpContext.User.Identity.Name.ToString());
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    byte[] bytes;
                    memoryStream.Close();
                    bytes = (byte[])mDBEntity.FirstOrDefault().FILECONTENT;
                    Response.Clear();
                    Response.ContentType = "application/jpeg";
                    Response.AddHeader("Content-Disposition", "inline; filename=" + mDBEntity.FirstOrDefault().FILENAME + "_" + mDBEntity.FirstOrDefault().FILENAME);
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ShareLink(string email, string link, string status, string CONSIGNMENTID, string name, string notes, string AddresBook)
        {
            try
            {
                ListTrackingModel tm = (ListTrackingModel)Session["TrackingList"];
                Int64 jobnumber = tm.TrackingItems.FirstOrDefault().JOBNUMBER;
                SendMail(tm, email, link, jobnumber, status, CONSIGNMENTID, name, notes, AddresBook);
                QuotationDBFactory mFactory = new QuotationDBFactory();
                //Insert new emails into Address Book -- Anil G
                if (AddresBook != "")
                {
                    mFactory.InsertNewEmailsIntoAddressBook(AddresBook, HttpContext.User.Identity.Name.ToString());
                }
                var nJSON = new JsonResult();
                nJSON.Data = true;
                return nJSON;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        /// <summary>
        /// This event will insert entered event level comments into DB 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Comments"></param>
        /// <param name="JobNumber"></param>
        /// <returns></returns>
        public ActionResult InsertComments(string ID, string Comments, string JobNumber)
        {
            try
            {
                //DbFactory object intiation
                //TrackingDBFactory mFactory = new TrackingDBFactory();
                Int64 jobnumber = Convert.ToInt64(JobNumber);
                _tdb.InsertShareComments(ID, Comments, HttpContext.User.Identity.Name, jobnumber, 0);
                var nJSON = new JsonResult();
                nJSON.Data = true;
                return nJSON;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        /// <summary>
        /// This event will update old record status to inactive state & insert the new record 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Comments"></param>
        /// <param name="JobNumber"></param>
        /// <returns></returns>
        public ActionResult UpdateComments(string ID, string Comments, string JobNumber)
        {
            try
            {
                //DbFactory object intiation
                //TrackingDBFactory mFactory = new TrackingDBFactory();
                _tdb.InsertShareComments(ID.Split('_')[1], Comments, HttpContext.User.Identity.Name, Convert.ToInt64(JobNumber), 1);
                var nJSON = new JsonResult();
                nJSON.Data = true;
                return nJSON;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        public ActionResult GetEnteredComments(string ID)
        {
            try
            {
                //DbFactory object intiation
                //TrackingDBFactory mFactory = new TrackingDBFactory();
                ListTrackingModel tm = (ListTrackingModel)Session["TrackingList"];
                Int64 jobnumber = tm.TrackingItems.FirstOrDefault().JOBNUMBER;
                string Comments = "";
                if (tm.shipmentCommentsItems.Where(i => i.EVENTNAME == ID.Split('_')[1].ToString()).Count() > 0)
                {
                    Comments = tm.shipmentCommentsItems.Where(i => i.EVENTNAME == ID.Split('_')[1].ToString()).FirstOrDefault().COMMENTS.ToString();
                }
                var nJSON = new JsonResult()
                {
                    Data = new
                    {
                        EnteredComments = Comments
                    }
                };
                return nJSON;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }

        [Authorize]
        public ActionResult GetAddressBook()
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DataTable dt = mFactory.GetAddressBook(HttpContext.User.Identity.Name.ToString());
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                    var JsonToReturn = new
                    {
                        rows = trows,
                    };
                    return Json(JsonToReturn, JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(Url.Action("Error", "Error"));
            }
        }

        /// <summary>
        /// This Method will sent mail to given mailID 
        /// </summary>
        /// <param name="tm"></param>
        /// <param name="EmailID"></param>
        /// <param name="link"></param>
        /// <param name="jobnumber"></param>
        /// <param name="status"></param>
        private void SendMail(ListTrackingModel tm, string Email, string link, long jobnumber, string status, string consigneementID, string name, string notes, string AddresBook)
        {
            try
            {
                string RateLink = string.Empty; string Receiver = string.Empty; string BCC = string.Empty; string CC = string.Empty;
                MailMessage message = new MailMessage();
                message.From = new MailAddress("no-reply@agility.com");
                if (AddresBook != "")
                {
                    if (AddresBook.Contains(','))
                    {
                        foreach (string s in AddresBook.Split(','))
                        {
                            message.Bcc.Add(new MailAddress(s));
                            BCC = s + ",";
                        }
                    }
                    else
                    {
                        message.CC.Add(new MailAddress(AddresBook));
                        CC = AddresBook;

                    }
                }

                string EnvironmentName = DynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - Tracking" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mode = tm.TrackingItems.FirstOrDefault().PRODUCTNAME.ToString().ToUpper() == "AIR FREIGHT" || tm.TrackingItems.FirstOrDefault().PRODUCTNAME.ToString().ToUpper() == "AIR" ? "Air" : "Ocean";
                string mPlaceOfReceipt = ""; string mPlaceOfDelivery = ""; string mPRLableName = string.Empty;
                string mPDLableName = string.Empty;
                if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Door to Door" || tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Door to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPLACECODE, tm.TrackingItems.FirstOrDefault().ORIGINPLACENAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPLACECODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPLACENAME);

                    mPRLableName = "Origin City"; mPDLableName = "Destination City";
                }
                else if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Door to port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPLACECODE, tm.TrackingItems.FirstOrDefault().ORIGINPLACENAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPORTCODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPORTNAME);

                    if (tm.TrackingItems.FirstOrDefault().PRODUCTID != 3)
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Port";
                    }
                    else
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Airport";
                    }
                }
                else if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Port to Port" || tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Port to port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPORTCODE, tm.TrackingItems.FirstOrDefault().ORIGINPORTNAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPORTCODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPORTNAME);

                    if (tm.TrackingItems.FirstOrDefault().PRODUCTID != 3)
                    {
                        mPRLableName = "Origin Port "; mPDLableName = "Destination Port";
                    }
                    else
                    {
                        mPRLableName = "Origin Airport "; mPDLableName = "Destination Airport";
                    }
                }
                else if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Port to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPORTCODE, tm.TrackingItems.FirstOrDefault().ORIGINPORTNAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPLACECODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPLACENAME);
                    if (tm.TrackingItems.FirstOrDefault().PRODUCTID != 3)
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination City";
                    }
                    else
                    {
                        mPRLableName = "Origin Airport"; mPDLableName = "Destination City";
                    }
                }
                string cargodetails = string.Empty;
                foreach (var mShipment in tm.PackageDetailsItems)
                {
                    cargodetails = cargodetails +
                                    "<tr>" +
                                    "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                    "<span class='inner-detail-head'" + ">Package Type</span><br>" +
                                    "<span class='inner-detail-cont'" + ">" + mShipment.PACKAGETYPENAME + "</span></td>" +
                                    "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                    "<span class='inner-detail-head'" + ">Quantity in Pieces</span><br>" +
                                    "<span class='inner-detail-cont'" + "> " + mShipment.QUANTITY.ToString() + "</span></td> </tr>";
                }

                string mapPath = "";
                if (status != "Delivered")
                {
                    if (tm.TrackingItems.FirstOrDefault().CONSIGNMENTID == null)
                    {
                        mapPath = Server.MapPath("~/App_Data/tracking-email-WC.html");
                    }
                    else
                    {
                        mapPath = Server.MapPath("~/App_Data/tracking-email-Think.html");
                    }
                }
                else
                {
                    string linkParam = link.Split('?')[1];
                    RateLink = Session["Actionlink"] + "CustomerFeedback?" + linkParam;
                    mapPath = Server.MapPath("~/App_Data/tracking-email-rating.html");
                }
                string loginuser = string.Empty;
                if (!ReferenceEquals(Request.Cookies["myCookie"], null))
                {
                    loginuser = Request.Cookies["myCookie"].Values["UserName"].ToString();
                }
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{user_name}", ((string.IsNullOrEmpty(name)) ? Email : name));
                body = body.Replace("{notes}", notes);
                body = body.Replace("{consignment_id}", tm.TrackingItems.FirstOrDefault().CONSIGNMENTID);
                body = body.Replace("{booking_date}", tm.TrackingItems.FirstOrDefault().DATECREATED.ToString("dd-MMM-yyyy"));
                body = body.Replace("{transport_mode}", mode.ToString());
                body = body.Replace("{Shipper_name}", tm.TrackingPartyItems.Where(i => i.PARTYTYPE == "91").FirstOrDefault().ClientName.ToString());
                body = body.Replace("{consignee_name}", tm.TrackingPartyItems.Where(i => i.PARTYTYPE == "92").FirstOrDefault().ClientName.ToString());
                body = body.Replace("{receipt_place}", mPlaceOfReceipt.ToString());
                body = body.Replace("{del_place}", mPlaceOfDelivery.ToString());
                body = body.Replace("{lblNameOrigin}", mPRLableName);
                body = body.Replace("{lblNameDest}", mPDLableName);
                body = body.Replace("{qty}", tm.TrackingItems.FirstOrDefault().TOTALQUANTITY.ToString());
                body = body.Replace("{weight}", tm.TrackingItems.FirstOrDefault().TOTALGROSSWEIGHT.ToString());
                body = body.Replace("{cbm}", tm.TrackingItems.FirstOrDefault().TOTALCBM.ToString());
                body = body.Replace("{booking_id}", tm.TrackingItems.FirstOrDefault().OPERATIONALJOBNUMBER.ToString());
                body = body.Replace("{imag}", mode == "Air" ? "air" : "ship");
                body = body.Replace("{dep_place}", mPlaceOfDelivery.ToString());
                body = body.Replace("{Mvnt_type}", tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME);
                body = body.Replace("{pckage_type}", cargodetails.ToString());
                body = body.Replace("{Status}", status.ToString());
                body = body.Replace("{url}", link.ToString());
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (status == "Delivered")
                {
                    body = body.Replace("{feddbackmess}", "Delivery completed successfully !!!");
                    body = body.Replace("{a1}", "Please");
                    body = body.Replace("{a2}", "provide us feedback");
                    body = body.Replace("{a3}", " so that we can improve your Shipa Freight experience.");
                    body = body.Replace("{feedback}", link);
                }
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", "", CC, BCC,
       "Tracking", "share", tm.TrackingItems.FirstOrDefault().OPERATIONALJOBNUMBER.ToString(), HttpContext.User.Identity.Name.ToString(), body, "", false, message.Subject);
                //DynamicClass.StoreMailCommunication("Tracking", "Share", jobnumber.ToString(), message.From.Address,
                //    AddresBook, HttpContext.User.Identity.Name.ToString(), body);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }
        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            Dictionary<string, long> dic = new Dictionary<string, long>();
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    temp_row.Add(col.ColumnName, dr[col]);
                }
                trows.Add(temp_row);
            }
            return trows;
        }

        /// <summary>
        /// Resend Mail based on HistoryID and update the Resend flag
        /// </summary>
        /// <param name="HISTORYID"></param>
        /// <param name="CreatedBy:Anil Gaddipati"></param>
        public void ResendMail(long HISTORYID, string CreatedBy)
        {
            try
            {
                TrackingDBFactory tbd = new TrackingDBFactory();
                DataTable dtHis = tbd.GetMailCommunicationHistory(HISTORYID);

                if (dtHis.Rows.Count > 0)
                {
                    byte[] mailbyte = (byte[])dtHis.Rows[0]["HTMLBODY"];
                    string temp_inBase64 = Convert.ToBase64String(mailbyte);
                    string MailBody = AESEncrytDecry.DecryptMailBody(temp_inBase64);

                    MailMessage message = new MailMessage();
                    message.From = new MailAddress(dtHis.Rows[0]["SENDER"].ToString());

                    if (!string.IsNullOrEmpty(dtHis.Rows[0]["RECEIVER"].ToString()))
                        message.To.Add(dtHis.Rows[0]["RECEIVER"].ToString());
                    string to = dtHis.Rows[0]["RECEIVER"].ToString();

                    if (!string.IsNullOrEmpty(dtHis.Rows[0]["CC"].ToString()))
                        message.CC.Add(dtHis.Rows[0]["CC"].ToString());
                    string cc = dtHis.Rows[0]["CC"].ToString();

                    if (!string.IsNullOrEmpty(dtHis.Rows[0]["BCC"].ToString()))
                        message.Bcc.Add(dtHis.Rows[0]["BCC"].ToString());
                    string bcc = dtHis.Rows[0]["BCC"].ToString();

                    if (!string.IsNullOrEmpty(dtHis.Rows[0]["SUBJECT"].ToString()))
                        message.Subject = dtHis.Rows[0]["SUBJECT"].ToString();


                    string DOCID = dtHis.Rows[0]["ATTACHEMENTIDS"].ToString();
                    if (DOCID != null && DOCID != "")
                    {
                        DataTable document = tbd.GetDocumnets(DOCID);
                        if (document != null)
                        {
                            foreach (DataRow dr in document.Rows)
                            {
                                string fileName = dr["FILENAME"].ToString();
                                byte[] fileContent = (byte[])dr["FILECONTENT"];
                                message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                            }

                        }

                    }


                    message.Body = MailBody;
                    message.IsBodyHtml = true;
                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                    client.Dispose();
                    message.Dispose();

                    tbd.UpdateCommunicationHistory(HISTORYID, CreatedBy, Convert.ToInt32(dtHis.Rows[0]["RESEND"]));
                }
            }
            catch (Exception ex)
            {
                throw new ShipaExceptions(ex.ToString());
            }
        }
    }
}
