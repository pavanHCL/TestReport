﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Controllers.Offers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Error(View = "NotFound")]
    public class OffersController : BaseController
    {
        [ActionName("offers")]
        public ActionResult ExclusiveOffers(string SearchString = "")
        {
            ViewBag.BodyClass = "";
            ViewBag.FooterClass = "footer-bg";
            QuotationDBFactory mFactory = new QuotationDBFactory();
            ViewBag.SearchString = SearchString;
            List<QuoteOffersModel> mUIModel;
            List<DBFactory.Entities.QuoteOffersEntity> mDBEntity = mFactory.GetSavedOffers(SearchString);
            mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteOffersEntity>, List<QuoteOffersModel>>(mDBEntity);

            DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetSavedOffersCount(SearchString);
            ViewBag.QuoteJobStatusCount = mQuoteStatusEntity;
            //for canonical tag 
            ViewBag.MetaTitle = "Freight Discounts & Offers | Shipafreight.com";
            ViewBag.MetaDescription = "Get exclusive deals and offers on specific trade lanes only at ShipAfreight.com. Visit our website now to check the latest offers and discounts on international freight forwarding.";
            var lowerurl = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "offers";
            ViewBag.CanonicalURL = lowerurl.ToLower();
            ViewBag.MetaRobots = "NOINDEX,NOFOLLOW,NOODP,NOYDIR";
           return View("ExclusiveOffers", mUIModel);
        }
        /// <summary>
        /// Redirect method for KnowledgeSeries.
        /// </summary>
        /// <param name="actionname">action name</param>
        /// <returns></returns>
        public ActionResult Redirect(string actionname)
        {
            return RedirectToRoutePermanent(new
            {
                controller = "Offers",
                action = actionname
            });
        }
    }
}