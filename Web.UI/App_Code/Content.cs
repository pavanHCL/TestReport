﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOCiS.SSP.DBFactory;

namespace FOCiS.SSP.Library.Extensions
{
    public static class Content
    {
        public static string GetThemedLayout(this UrlHelper urlHelper, string theme, string layoutFile = "_Layout")
        {
            if (string.IsNullOrWhiteSpace(theme)) theme = System.Configuration.ConfigurationManager.AppSettings["SiteTheme"];

          // var mFileName = urlHelper.Content(string.Format("~/Views/Themes/{0}/{1}.cshtml", theme, layoutFile));

            var mFileName = urlHelper.Content(string.Format("~/Views/Shared/{0}.cshtml",  layoutFile));

            if (string.Compare(theme, "default", true) == 0) mFileName = urlHelper.Content(string.Format("~/Views/Shared/{1}.cshtml", theme, layoutFile)); ;
            return mFileName;
        }

        public static T GetAttributeFrom<T>(this object instance, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var property = instance.GetType().GetProperty(propertyName);
            return (T)property.GetCustomAttributes(attrType, false).First();
        }

        public static string GetMandatoryMark(this object instance, string propertyName, string text)
        {
            var property = instance.GetType().GetProperty(propertyName);
            var hasIsIdentity = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.RequiredAttribute));
            if (hasIsIdentity) return string.Format("* {0}", text);
            return text;
        }

        public static string GetRoundedValue(this object instance, string value)
        {
            string currency = ((FOCiS.SSP.Models.QM.QuotationPreviewModel)(instance)).PreferredCurrencyId;
            string returnvalue = string.Empty;
            QuotationDBFactory mFactory = new QuotationDBFactory();
            returnvalue = mFactory.RoundedValue(value, currency);
            return returnvalue;
        }

        public static string GetRoundedValues(this object instance, string value, string currency)
        {
            string returnvalue = string.Empty;
            QuotationDBFactory mFactory = new QuotationDBFactory();
            returnvalue = mFactory.RoundedValue(value, currency);
            return returnvalue;
        }
    }
}