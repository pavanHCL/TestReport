﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOCiS.SSP.Web.UI.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthenticateActionParameter : ActionFilterAttribute
    {
         public override void OnActionExecuting(ActionExecutingContext filterContext)
         {
             if (filterContext.HttpContext.User.Identity.IsAuthenticated)
             {
                 base.OnActionExecuting(filterContext);
             }
             else
             {
                 filterContext.Controller.ViewData.Add("LoginStatus", "False");
             }
         }
    }
}