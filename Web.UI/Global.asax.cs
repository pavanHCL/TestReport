﻿using FOCiS.SSP.WebApi;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using FOCiS.SSP.Web.UI.App_Start;
using System.Web.Optimization;
using FOCiS.SSP.DBFactory;
using System.Data;
using System.IO;
using System.Threading;
using System.Globalization;
using FOCiS.SSP.Web.UI.Helpers;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Generic;
using System.Security.Permissions;
using log4net;

namespace FOCiS.SSP.Web.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        protected void Application_Start()
        {
            RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperConfig.RegisterMappings();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MvcHandler.DisableMvcResponseHeader = true;
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
            
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Session != null)
                {
                    if (Session["SiteTheme"] == null)
                        Session["SiteTheme"] = System.Configuration.ConfigurationManager.AppSettings["SiteTheme"];
                    if (Session["IsSiteLocal"] == null)
                        Session["IsSiteLocal"] = System.Configuration.ConfigurationManager.AppSettings["IsLocal"];
                    if (Session["SiteCaptchaKey"] == null)
                        Session["SiteCaptchaKey"] = GetCaptchaKey();
                    Session["Actionlink"] = System.Configuration.ConfigurationManager.AppSettings["APPURL"];

                    if (Session["strStripePKKey"] == null)
                    {
                        Session["strStripePKKey"] = GetStripeKey().Split('/')[0];
                        Session["strStripeSkKey"] = GetStripeKey().Split('/')[1];
                    }




                    //------------ getting notification count--------------------- siva
                    if (HttpContext.Current.User.Identity.Name.ToString() != "")
                    {
                        UserDBFactory UDF = new UserDBFactory();
                        DataTable dt = UDF.ShowNotificationCount(HttpContext.Current.User.Identity.Name.ToString());
                        Session["NotificationCount"] = dt.Rows[0]["NOTIFICATIONCOUNT"];
                        Session["TotalNotificationsCount"] = dt.Rows[0]["TOTALNOTIFICATIONSCOUNT"];
                    }
                    //----------------------         
                }

                ////language culture  change 
                //string lang = null;
                //string defaultLanguage = "en-us";
                //if (Request.UserLanguages != null)
                //{
                //    //Set the Language.
                //    defaultLanguage = Request.UserLanguages[0];
                //}
                //HttpCookie langCookie = Request.Cookies["shipaculture"];
                //if (langCookie != null)
                //{
                //    lang = langCookie.Value;
                //}
                //else
                //{
                //    lang = defaultLanguage.ToLower();
                //}
                //new Languages().SetLanguage(lang);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
        }



        private DataTable JsonstrintoDatatable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        if (idx != -1)
                        {
                            string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "").Replace("\n", "").Replace(" ", "");
                            if (!ColumnsName.Contains(ColumnsNameString))
                            {
                                ColumnsName.Add(ColumnsNameString);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        if (idx != -1)
                        {
                            string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "").Replace("\n", "").Replace(" ", "");
                            string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                            nr[RowColumns] = RowDataString;
                        }
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }

        /// <summary>
        /// Get User profile image for showing as logged in header
        /// </summary>
        /// <param name="user"></param>
        private void UpdateImageSession(string user)
        {
            UserDBFactory UDF = new UserDBFactory();
            DataTable dt = UDF.GetUserImage(user);

            DataTable Dt1 = new DataTable();
            Dt1.Columns.Add("BASESTRING", typeof(string));
            Dt1.Columns.Add("IMGFILENAME", typeof(string));
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][1].ToString() != "")
                {
                    byte[] bt = (byte[])dt.Rows[0][1];
                    string base64 = Convert.ToBase64String(bt);

                    Dt1.Rows.Add(base64, dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1]);
                    Session["BaseIm"] = (string)base64; Session["filetype"] = dt.Rows[0]["IMGFILENAME"].ToString().Split('.')[1];
                }
            }
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                         new System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");              //Remove Server Header   
            Response.Headers.Remove("X-AspNet-Version");    //Remove X-AspNet-Version Header     
            Response.Headers.Remove("X-Powered-By");        //Remove X-Powered-By Header     
            Response.Headers.Remove("X-AspNetMvc-Version"); //Remove X-AspNetMvc-Version Header           
        }

        protected string GetCaptchaKey()
        {

            string host = Request.Url.Host;
            int myInt;
            string strCaptchaKey = "";
            bool isNumerical = int.TryParse(host.Substring(0, 2), out myInt);

            if (isNumerical) //True Then IP : Host
            {
                strCaptchaKey = System.Configuration.ConfigurationManager.AppSettings["CaptchaKey"].ToString();
            }
            else
            {
                strCaptchaKey = System.Configuration.ConfigurationManager.AppSettings["CaptchaKeyForHost"].ToString();
            }
            return strCaptchaKey;
        }

        protected string GetStripeKey()
        {
            string host = Request.Url.Host;
            string strStripePKKey, strStripeSkKey = "";
            string sitename = System.Configuration.ConfigurationManager.AppSettings["APPURL"].ToString();
            if (host.Contains("localhost") || sitename.Contains("demo"))
            {
                strStripePKKey = System.Configuration.ConfigurationManager.AppSettings["AgilePK"].ToString();
                strStripeSkKey = System.Configuration.ConfigurationManager.AppSettings["AgileSK"].ToString();
            }
            else
            {
                if (host.Contains(sitename) || sitename.Contains(host))
                {
                    strStripePKKey = System.Configuration.ConfigurationManager.AppSettings["ProdPK"].ToString();
                    strStripeSkKey = System.Configuration.ConfigurationManager.AppSettings["ProdSK"].ToString();
                }
                else
                {
                    strStripePKKey = System.Configuration.ConfigurationManager.AppSettings["AgilePK"].ToString();
                    strStripeSkKey = System.Configuration.ConfigurationManager.AppSettings["AgileSK"].ToString();
                }
            }

            return strStripePKKey + "/" + strStripeSkKey;
        }
    }

    public class Error : HandleErrorAttribute
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(ExceptionContext filterContext)
        {
            try
            {
                Exception ex = filterContext.Exception;
                filterContext.ExceptionHandled = true;
                var model = new HandleErrorInfo(filterContext.Exception, "Controller", "Action");

                string Controller = filterContext.RouteData.Values["controller"].ToString();
                string Action = filterContext.RouteData.Values["action"].ToString();
                string ERRMesage = filterContext.Exception.Message;
                string trace = filterContext.Exception.StackTrace;

                if (ERRMesage != "Server cannot set content type after HTTP headers have been sent.")
                {
                    QuotationDBFactory mFactory = new QuotationDBFactory();
                    mFactory.InsertErrorLog(HttpContext.Current.User.Identity.Name, Controller, Action, ERRMesage, trace);
                    SendEmail(filterContext);
                }

                if (ERRMesage == "The input is not a valid Base-64 string as it contains a non-base 64 character, more than two padding characters, or an illegal character among the padding characters. ")
                {
                    var redirectTarget = new RouteValueDictionary { { "action", "Index" }, { "controller", "Quotation" } };
                    filterContext.Result = new RedirectToRouteResult(redirectTarget);
                }
                else
                {
                    filterContext.Result = new ViewResult()
                    {
                        ViewName = "NotFound",
                        ViewData = new ViewDataDictionary(model)
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
        }

        void SendEmail(ExceptionContext context)
        {
            var Content = string.Empty;
            if (context.HttpContext.Request.RequestType.ToString() == "POST")
            {
                var d = new StreamReader(context.HttpContext.Request.InputStream);
                d.BaseStream.Seek(0, SeekOrigin.Begin);
                Content = d.ReadToEnd();
            }
            var exdata = new FOCiS.SSP.Models.UserManagement.ExceptionEmailDto()
            {
                ActionMethod = context.HttpContext.Request.RequestType.ToString(),
                AppUrl = System.Configuration.ConfigurationManager.AppSettings["APPURL"],
                Error = context.Exception.Message.ToString(),
                Headers = context.HttpContext.Request.Headers.ToString(),
                ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"],
                StackTrace = context.Exception.StackTrace.ToString(),
                Url = context.HttpContext.Request.Url.AbsoluteUri.ToString(),
                UserAgent = context.HttpContext.Request.UserAgent.ToString(),
                UserHostAddress = context.HttpContext.Request.UserHostAddress.ToString(),
                UserHostName = context.HttpContext.Request.UserHostName.ToString(),
                UTC = DateTime.UtcNow.ToString(),
                Content = Content,
                UserEmail = context.HttpContext.User.Identity.Name.ToString(),
                ServerVariables = context.HttpContext.Request.ServerVariables.ToString()
            };
            FOCiS.SSP.Web.UI.Controllers.DynamicClass.SendWebExceptionEmailV2(
                exdata
            );
        }
    }
}

//---------------Get Header Image when session is Empty------------------ Anil G
//if (Session["BaseIm"] == null && HttpContext.Current.User.Identity.Name.ToString() != "")
//{
//    UpdateImageSession(HttpContext.Current.User.Identity.Name.ToString());
//}
//----------------------