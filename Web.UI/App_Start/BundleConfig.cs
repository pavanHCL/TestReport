﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FOCiS.SSP.Web.UI.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {        
            bundles.Add(new StyleBundle("~/Create.css").Include(
                "~/Content/animate.css",
                "~/Content/iCheck.css",
                "~/Content/owl.carousel.css",// learnmore page
                "~/Content/owl.theme.default.css",// learnmore page
                "~/Content/intlTelInput.css"
                ));

            bundles.Add(new StyleBundle("~/Videos.css").Include(
               "~/Content/animate.css",
               "~/Content/owl.carousel.css"
               ));

            bundles.Add(new StyleBundle("~/Layout.css").Include(
                "~/Content/bootstrap.css",
                "~/Content/themes/base/hamburgers.css",
                "~/Content/selectize.css",
                "~/Content/selectize-bootstrap.css",
                "~/Content/toastr.min.css",
                "~/Content/tooltipster.bundle.min.css",
                "~/Content/tooltipster-sideTip-noir.min.css",
                "~/Content/bootstrap-tour.min.css",
                 "~/Content/ThinkDesign.css"
                ));

            bundles.Add(new StyleBundle("~/bundle/Home/About.css").Include(
               "~/Content/animate.css",
               "~/Content/owl.theme.default.css",
               "~/Content/bootstrap.css",
               "~/Content/font-awesome.css",
               "~/Content/owl.carousel.css",
               "~/Content/themes/base/jquery-ui.css",
               "~/Content/themes/base/hamburgers.css"
               ));

            bundles.Add(new StyleBundle("~/Home/Invoice.css").Include(
               "~/Content/owl.carousel.css",
               "~/Content/owl.theme.default.css",
                //"~/Content/select2.min.css",
               "~/Content/ThinkDesign.css"
               ));

            bundles.Add(new StyleBundle("~/Home/Preview.css").Include(
               "~/Content/animate.css",
               "~/Content/owl.carousel.css",
               "~/Content/owl.theme.default.css",
               "~/Content/iCheck.css",
               "~/Content/select2.min.css"
               ));

            bundles.Add(new StyleBundle("~/Home/Login.css").Include(
               "~/Content/animate.css",
               "~/Content/owl.carousel.css",
               "~/Content/owl.theme.default.css",
               "~/Content/iCheck.css",
                "~/Content/intlTelInput.css"
               ));

            bundles.Add(new StyleBundle("~/Home/TrackingList.css").Include(
               "~/Content/animate.css",
               "~/Content/owl.carousel.css",
               "~/Content/owl.theme.default.css",
                "~/Content/iCheck.css",
               "~/Content/select2.min.css",
               "~/Content/bootstrap-datepicker.min.css",
               "~/Content/tooltipster.bundle.min.css",
                "~/Content/tooltipster-sideTip-noir.min.css"
               ));

            bundles.Add(new StyleBundle("~/Home/Charge.css").Include(
              "~/Content/animate.css",
              "~/Content/owl.carousel.css",
              "~/Content/owl.theme.default.css",
               "~/Content/iCheck.css",
              "~/Content/bootstrap-datepicker.min.css",
              "~/Content/bootstrap-tour.min.css",
              "~/Content/themes/base/minified/jquery-ui.min.css"
              ));

            bundles.Add(new StyleBundle("~/Home/BookingSummary.css").Include(
               "~/Content/select2.min.css",
               "~/Content/iCheck.css",
               "~/Content/owl.carousel.css",
               "~/Content/owl.theme.default.css",
               "~/Content/bootstrap-datepicker.min.css",
               "~/Content/themes/base/jquery-ui.css"
               ));

            bundles.Add(new StyleBundle("~/Home/Quote2Job.css").Include(
               "~/Content/select2.min.css",
               "~/Content/selectize.css",
               "~/Content/selectize-bootstrap.css",
               "~/Content/iCheck.css",
               "~/Content/owl.carousel.css",
               "~/Content/owl.theme.default.css",
               "~/Content/bootstrap-datepicker.min.css",
               "~/Content/themes/base/jquery-ui.css",
               "~/Content/bootstrap-tour.min.css",
               "~/Content/intlTelInput.css"
               ));

            bundles.Add(new StyleBundle("~/Home/ProfilePage.css").Include(
             "~/Content/select2.min.css",
             "~/Content/selectize.css",
             "~/Content/selectize-bootstrap.css",
             "~/Content/iCheck.css",
             "~/Content/owl.carousel.css",
             "~/Content/owl.theme.default.css",
             "~/Content/jqueryui.css",
             "~/Content/bootstrap-datepicker.min.css",
             "~/Content/animate.css",
              "~/Content/intlTelInput.css"
             ));

            bundles.Add(new StyleBundle("~/Home/CustomerSupport.css").Include(
             "~/Content/select2.min.css",
             "~/Content/selectize.css",
             "~/Content/selectize-bootstrap.css",
             "~/Content/iCheck.css",
             "~/Content/jqueryui.css",
             "~/Content/animate.css"
             ));

            bundles.Add(new StyleBundle("~/Home/CustomerFeedback.css").Include(
               "~/Content/themes/circle.css",
               "~/Content/font-awesome.css"
               ));

            bundles.Add(new StyleBundle("~/KnowledgeSeries.css").Include(
             "~/Content/animate.css",
              "~/Content/owl.carousel.css"
             ));

            bundles.Add(new StyleBundle("~/Home/QuoteBooklist.css").Include(
                 "~/Content/select2.min.css",
                 "~/Content/animate.css",
                 "~/Content/iCheck.css"
               ));
            bundles.Add(new StyleBundle("~/Learnmore.css").Include(
                "~/Content/select2.min.css",
                "~/Content/animate.css",
                "~/Content/iCheck.css",
                "~/Content/owl.carousel.css",// learnmore page
                "~/Content/owl.theme.default.css",// learnmore page
                 "~/Content/intlTelInput.css",
                  "~/Content/selectize.css"
                ));
            // Scripts

            bundles.Add(new ScriptBundle("~/bundles/Layout.js").Include(
                 "~/Scripts/jquery.i18n.js",
               "~/Scripts/jquery-3.1.0.js",
                "~/Scripts/jquery-migrate-3.0.0.js",
                "~/Scripts/jquery-ui-1.9.0.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery.validate.js",//d
                "~/Scripts/jquery.validate.unobtrusive.js",//d
                "~/Scripts/jquery.validate.unobtrusive.bootstrap.js",//d
                "~/Scripts/selectize.js",
                "~/Scripts/toastr.min.js",
                "~/Scripts/tooltipster.bundle.min.js",
                "~/Scripts/site.common.js",// animation for controls
                 "~/Scripts/modernizr-2.8.3.js",//old browser support
                 "~/Scripts/UserManagement/UserLogin.js",
                  "~/Scripts/UserManagement/aes.js",//dont remove -unable to login
                 "~/Scripts/loader-structure.min.js",//loader
                 "~/Scripts/loader-animation.min.js",//loader
                  "~/Scripts/bootstrap-tour.min.js",//tour
                  "~/Scripts/MultiLanguage/LanguageGlobal.js"//multi language texts
                ));

            bundles.Add(new ScriptBundle("~/Create.js").Include(              
                 "~/Scripts/jquery.iCheck.min.js",
                 "~/Scripts/knockout-3.4.0.js",
                 "~/Scripts/currencyFormatter.min.js",//currency format
                 "~/Scripts/accounting.js",
                 "~/Scripts/owl.carousel.js",//testimonals
                 "~/Scripts/wow.min.js",
                 "~/Scripts/Quotation/QuotationModule.js",
                  "~/Scripts/Quotation/Quotationcreate.js",
                //"~/Scripts/intlTelInput.js",//phone number
                  "~/Scripts/Quotation/Learnmore.js",
                  "~/Scripts/MultiLanguage/MultiLanguageQuotationModule.js"
                 ));

            bundles.Add(new ScriptBundle("~/AmazonCreate.js").Include(
                 "~/Scripts/jquery.iCheck.min.js",
                 "~/Scripts/knockout-3.4.0.js",
                 "~/Scripts/currencyFormatter.min.js",//currency format
                 "~/Scripts/accounting.js",
                 "~/Scripts/owl.carousel.js",//testimonals
                 "~/Scripts/wow.min.js",
                 "~/Scripts/Quotation/QuotationAmazon.js",
                  "~/Scripts/Quotation/QuotationAmazoncreate.js",
                //"~/Scripts/intlTelInput.js",//phone number
                  "~/Scripts/Quotation/Learnmore.js"
                 ));

            bundles.Add(new ScriptBundle("~/Learnmore.js").Include(
                 "~/Scripts/jquery.iCheck.min.js",
                 "~/Scripts/owl.carousel.js",//testimonals
                 "~/Scripts/wow.min.js",
                 "~/Scripts/intlTelInput.js",
                  "~/Scripts/selectize.js",
                  "~/Scripts/Quotation/Learnmore.js"
                 ));
            bundles.Add(new ScriptBundle("~/bundles/About.js").Include(
                "~/Scripts/wow.min.js",
                 "~/Scripts/owl.carousel.js",
                 "~/Scripts/modernizr-2.8.3.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/Co2.js").Include(
                "~/Scripts/owl.carousel.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/Videos.js").Include(
                 "~/Scripts/wow.min.js",
                "~/Scripts/owl.carousel.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/KnowledgeSeries.js").Include(
               "~/Scripts/wow.min.js",
               "~/Scripts/owl.carousel.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Preview.js").Include(
                 "~/Scripts/owl.carousel.js",
                 "~/Scripts/jquery.iCheck.min.js",
                 "~/Scripts/wow.min.js",
                "~/Scripts/select2.js",//check box functionality
                 "~/Scripts/Quotation/QuotePreviewModule.js",
                 "~/Scripts/Quotation/QuoteListModule.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/TrackingSummary.js").Include(
                "~/Scripts/wow.min.js",
                "~/Scripts/select2.js",
                 "~/Scripts/selectize.js",
                "~/Scripts/select2-tab-fix.min.js",
                "~/Scripts/i18n/en.js",
                "~/Scripts/jquery.iCheck.min.js",
                "~/Scripts/owl.carousel.js",
                //"~/Scripts/Quotation/QuotationModule.js",
                "~/Scripts/tooltipster.bundle.min.js",
                "~/Scripts/bootstrap-datepicker.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/TrackingList.js").Include(
                "~/Scripts/wow.min.js",
                "~/Scripts/jquery.iCheck.min.js",
                "~/Scripts/owl.carousel.js",
                "~/Scripts/bootstrap-datepicker.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/QuoteBookList.js").Include(
                "~/Scripts/wow.min.js",
                "~/Scripts/jquery.iCheck.min.js",
                "~/Scripts/owl.carousel.js",
                "~/Scripts/Quotation/QuoteListModule.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/BookingSummary.js").Include(
                 "~/Scripts/select2.js",
                 "~/Scripts/jquery.iCheck.min.js",
                 "~/Scripts/bootstrap-datepicker.min.js",
                 "~/Scripts/ajaxfileupload.js",
                 "~/Scripts/tooltipster.bundle.min.js",
                 "~/Scripts/knockout-3.4.0.js",
                 "~/Scripts/wow.min.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/Quote2Job.js").Include(
                 "~/Scripts/selectize.js",
                  "~/Scripts/select2.js",
                 "~/Scripts/select2-tab-fix.min.js",
                "~/Scripts/jquery.iCheck.min.js",
                "~/Scripts/owl.carousel.js",
                "~/Scripts/JobCompliance/JobCompliance.js",
               "~/Scripts/bootstrap-datepicker.min.js",
                "~/Scripts/ajaxfileupload.js",
                "~/Scripts/tooltipster.bundle.min.js",
                "~/Scripts/knockout-3.4.0.js",
                "~/Scripts/wow.min.js",
                "~/Scripts/bootstrap-tour.min.js",
                "~/Scripts/intlTelInput.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/Charge.js").Include(
                "~/Scripts/jquery.iCheck.min.js",
                "~/Scripts/owl.carousel.js",
                "~/Scripts/wow.min.js",
                "~/Scripts/bootstrap-datepicker.min.js",
                "~/Scripts/bootstrap-tour.min.js",
                "~/Scripts/Stripe/stripev3.js",
                "~/Scripts/Payment/PaymentModule.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/Sessiontimeout.js").Include(
              "~/Scripts/owl.carousel.js",
              "~/Scripts/wow.min.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Login.js").Include(
               "~/Scripts/select2.js",
               "~/Scripts/select2-tab-fix.min.js",
               "~/Scripts/jquery.iCheck.min.js",
               "~/Scripts/owl.carousel.js",
               "~/Scripts/wow.min.js",
                "~/Scripts/UserManagement/aes.js",//dont remove unable to login
               "~/Scripts/intlTelInput.js"
               ));

            bundles.Add(new ScriptBundle("~/ProfilePage.js").Include(
               "~/Scripts/select2.js",
               "~/Scripts/select2-tab-fix.min.js",
               "~/Scripts/selectize.js",
               "~/Scripts/jquery.iCheck.min.js",
               "~/Scripts/owl.carousel.js",
               "~/Scripts/ajaxfileupload.js",
               "~/Scripts/intlTelInput.js",
               "~/Scripts/currencyFormatter.min.js",
                "~/Scripts/accounting.js",
                "~/Scripts/bootstrap-progressbar.min.js",
                 "~/Scripts/UserManagement/UserProfilePage.js"
               ));


            bundles.Add(new ScriptBundle("~/CustomerSupport.js").Include(
              "~/Scripts/select2.js",
              "~/Scripts/select2-tab-fix.min.js",
              "~/Scripts/selectize.js",
              "~/Scripts/jquery.iCheck.min.js",
              "~/Scripts/ajaxfileupload.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Notifications.js").Include(
                 "~/Scripts/knockout-3.4.0.js"
                ));
            BundleTable.EnableOptimizations = true;
        }
    }
}
