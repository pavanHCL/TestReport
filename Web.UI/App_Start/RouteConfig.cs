﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FOCiS.SSP.Web.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("*.ico");

            routes.MapRoute(
                 name: "termsandconditions",
                 url: "terms-and-conditions",
                 defaults: new { controller = "LearnMore", action = "terms-and-conditions", id = UrlParameter.Optional }
           );
            routes.MapRoute(
           name: "PageNavigationsIndex",
           url: "PageNavigations/Index",
           defaults: new { controller = "LearnMore", action = "Redirect", actionname = "learn-more" }
           );
            routes.MapRoute(
          name: "PageNavigations",
          url: "PageNavigations",
          defaults: new { controller = "LearnMore", action = "Redirect", actionname = "learn-more" }
          );
            routes.MapRoute(
            name: "PageNavigationsLearnmore",
            url: "PageNavigations/Learnmore",
            defaults: new { controller = "LearnMore", action = "Redirect", actionname = "learn-more" }
            );

            routes.MapRoute(
              name: "Co2Facts",
              url: "PageNavigations/Co2Facts",
              defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "co2-facts" }
              );
            routes.MapRoute(
              name: "PageNavigationsCo2Facts",
              url: "Co2Facts",
              defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "co2-facts" }
            );
            routes.MapRoute(
               name: "DocumentsList",
               url: "PageNavigations/DocumentsList",
               defaults: new { controller = "LearnMore", action = "Redirect", actionname = "documents-list" }
              );
            routes.MapRoute(
           name: "PageNavigationsDocumentsList",
           url: "DocumentsList",
           defaults: new { controller = "LearnMore", action = "Redirect", actionname = "documents-list" }
             );
            routes.MapRoute(
              name: "TransitTime",
              url: "PageNavigations/TransitTime",
              defaults: new { controller = "LearnMore", action = "Redirect", actionname = "transit-time" }
             );
            routes.MapRoute(
             name: "PageNavigationsTransitTime",
             url: "TransitTime",
             defaults: new { controller = "LearnMore", action = "Redirect", actionname = "transit-time" }
               );

            routes.MapRoute(
              name: "HowitWorks",
              url: "PageNavigations/HowitWorks",
              defaults: new { controller = "LearnMore", action = "Redirect", actionname = "how-it-works" }
               );
            routes.MapRoute(
            name: "PageNavigationsHowitWorks",
            url: "HowitWorks",
            defaults: new { controller = "LearnMore", action = "Redirect", actionname = "how-it-works" }
              );
            routes.MapRoute(
            name: "KnowledgeSeriesShipaFreightKnowledgeSeries",
            url: "knowledge-series/ShipaFreightKnowledgeSeries",
            defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "introduction" }
             );
            routes.MapRoute(
            name: "ShipaFreightKnowledgeSeries",
            url: "ShipaFreightKnowledgeSeries",
            defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "introduction" }
            );
            routes.MapRoute(
                 name: "KnowledgeSeries",
                 url: "knowledge-series/{action}",
                 defaults: new { controller = "KnowledgeSeries", action = "knowledge-series", id = UrlParameter.Optional }
           );
            routes.MapRoute(
              name: "LearnMoreCo2Facts",
              url: "learn-more/co2-facts",
              defaults: new { controller = "KnowledgeSeries", action = "co2-facts", id = UrlParameter.Optional }
          );
            routes.MapRoute(
                 name: "LearnMore",
                 url: "learn-more/{action}",
                 defaults: new { controller = "LearnMore", action = "learn-more", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "PageNavigationKnowledgeSeries",
                url: "KnowledgeSeries",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "knowledge-series" }
          );
            routes.MapRoute(
               name: "shipforsuccess",
               url: "ship-success/{action}",
               defaults: new { controller = "KnowledgeSeries", action = "ship-success", id = UrlParameter.Optional }
         );

            routes.MapRoute(
              name: "DownloadReportPDF",
              url: "DownloadReportPDF/{action}",
              defaults: new { controller = "KnowledgeSeries", action = "DownloadReportPDF", id = UrlParameter.Optional }
              );
            routes.MapRoute(
              name: "DownloadReport",
              url: "DownloadReport",
              defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "DownloadReportPDF" }
              );
            routes.MapRoute(
                 name: "TheSMEKnowledgeGap",
                 url: "TheSMEKnowledgeGap",
                 defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "sme-knowledge-gap" }
           );
            routes.MapRoute(
                name: "ExportFinance",
                url: "ExportFinance",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "export-finance" }
          );
            routes.MapRoute(
                name: "FirstShipment",
                url: "FirstShipment",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "first-shipment" }
          );
            routes.MapRoute(
                name: "FindingPartner",
                url: "FindingPartner",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "finding-partner" }
          );
            routes.MapRoute(
               name: "KeepingCompliant",
               url: "KeepingCompliant",
               defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "keeping-compliant" }
            );
            routes.MapRoute(
                name: "MarketingIntel",
                url: "MarketingIntel",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "marketing-intel" }
          );
            routes.MapRoute(
                name: "ExportClassification",
                url: "PageNavigations/ExportClassification",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "export-classification" }
          );
            routes.MapRoute(
                name: "InternationalEcommerce",
                url: "PageNavigations/InternationalEcommerce",
                defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "international-eCommerce" }
          );
            routes.MapRoute(
              name: "ExportingEurope",
              url: "PageNavigations/ExportingEurope",
              defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "exporting-europe" }
           );
            routes.MapRoute(
              name: "EmergingMarkets",
              url: "PageNavigations/EmergingMarkets",
              defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "emerging-markets" }
              );
            routes.MapRoute(
             name: "ProductStandards",
             url: "PageNavigations/ProductStandards",
             defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "product-standards" }
             );
            routes.MapRoute(
            name: "CustomsProcessing",
            url: "PageNavigations/CustomsProcessing",
            defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "customs-processing" }
            );
            routes.MapRoute(
            name: "ProtectingIP",
            url: "ProtectingIP",
            defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "protecting-ip" }
            );
            routes.MapRoute(
             name: "ChineseExporters",
             url: "ChineseExporters",
             defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "chinese-exporters" }
           );
            routes.MapRoute(
             name: "InsightsforAmazonFBA",
             url: "insights-for-amazon-fba",
             defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "6-Insights-for-Amazon-FBA" }
           );
            routes.MapRoute(
                name: "Videos",
                url: "videos/{action}",
                defaults: new { controller = "Videos", action = "videos", id = UrlParameter.Optional }
          );
            routes.MapRoute(
           name: "ShipaVideos",
           url: "ShipaVideos",
           defaults: new { controller = "Videos", action = "Redirect", videoname = "videos" }
           );
            routes.MapRoute(
              name: "PageNavigationsVideos",
              url: "Video/{videoname}",
              defaults: new { controller = "Videos", action = "Redirect", videoname = UrlParameter.Optional }
              );
            routes.MapRoute(
              name: "PageNavigationsExclusiveOffers",
              url: "PageNavigations/ExclusiveOffers",
              defaults: new { controller = "Offers", action = "Redirect", videoname = "offers" }
           );
            routes.MapRoute(
              name: "Offers",
              url: "offers/{action}",
              defaults: new { controller = "Offers", action = "offers", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "PageNavigationsOffers",
               url: "Offers",
               defaults: new { controller = "Offers", action = "offers", id = UrlParameter.Optional }
              );
            routes.MapRoute(
                name: "ContactUs",
                url: "contact-us",
                defaults: new { controller = "Quotation", action = "Contactus", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "SwissLanding",
             url: "swiss-landing",
             defaults: new { controller = "Quotation", action = "SwissLanding", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "optin",
               url: "knowledge-series/opt-in",
               defaults: new { controller = "KnowledgeSeries", action = "Redirect", actionname = "opt-in" }
         );



            routes.MapRoute(
           name: "PrivacyPolicy",
           url: "privacy-policy",
           defaults: new { controller = "LearnMore", action = "PrivacyPolicy", id = UrlParameter.Optional }
          );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Quotation", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}