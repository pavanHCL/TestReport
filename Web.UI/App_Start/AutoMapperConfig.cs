﻿using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Models.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FOCiS.SSP.Models.Tracking;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.Models.DB;

namespace FOCiS.SSP.Web.UI
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DBFactory.Entities.QuotationEntity, QuotationModel>();
                cfg.CreateMap<DBFactory.Entities.QuotationTemplateEntity, QuotationTemplateModel>();
                cfg.CreateMap<QuotationModel, DBFactory.Entities.QuotationEntity>()
                    .ForMember(d => d.ISHAZARDOUSCARGO, s => s.MapFrom(s1 => s1.IsHazardousCargo ? 1L : 0L))
                    .ForMember(d => d.ISINSURANCEREQUIRED, s => s.MapFrom(s1 => s1.IsInsuranceRequired ? 1L : 0L))
                    .ForMember(d => d.ISOFFERAPPLICABLE, s => s.MapFrom(s1 => s1.IsOfferApplicable ? 1L : 0L))
                    .ForMember(d => d.ISEVENTCARGO, s => s.MapFrom(s1 => s1.IsEventCargo ? 1L : 0L));

                cfg.CreateMap<DBFactory.Entities.QuotationShipmentEntity, QuotationShipmentModel>();
                cfg.CreateMap<QuotationShipmentModel, DBFactory.Entities.QuotationShipmentEntity>();
                cfg.CreateMap<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>();
                cfg.CreateMap<DBFactory.Entities.SSPDocumentsEntity, SSPDocumentsModel>();
                cfg.CreateMap<DBFactory.Entities.SSPJobDetailsEntity, SSPJobDetailsModel>();
                cfg.CreateMap<DBFactory.Entities.JobBookingListEntity, JobBookingListModel>();
                cfg.CreateMap<DBFactory.Entities.SSPPartyDetailsEntity, SSPPartyDetails>();
                cfg.CreateMap<DBFactory.Entities.PaymentChargesEntity, PaymentCharges>();
                cfg.CreateMap<DBFactory.Entities.ReceiptHeaderEntity, ReceiptHeader>();

                cfg.CreateMap<DBFactory.Entities.HsCodeInstructionEntity, HsCodeInstructionModel>();

                cfg.CreateMap<DBFactory.Entities.JobBookingEntity, JobBookingModel>();               
                cfg.CreateMap<JobBookingModel, DBFactory.Entities.JobBookingEntity>()
                     .ForMember(d => d.LIVEUPLOADS, s => s.MapFrom(s1 => s1.LIVEUPLOADS ? 1 : 0))
                     .ForMember(d => d.LOADINGDOCKAVAILABLE, s => s.MapFrom(s1 => s1.LOADINGDOCKAVAILABLE ? 1 : 0))
                     .ForMember(d => d.CARGOPALLETIZED, s => s.MapFrom(s1 => s1.CARGOPALLETIZED ? 1 : 0))
                     .ForMember(d => d.ORIGINALDOCUMENTSREQUIRED, s => s.MapFrom(s1 => s1.ORIGINALDOCUMENTSREQUIRED ? 1 : 0))
                     .ForMember(d => d.TEMPERATURECONTROLREQUIRED, s => s.MapFrom(s1 => s1.TEMPERATURECONTROLREQUIRED ? 1 : 0))
                     .ForMember(d => d.COMMERCIALPICKUPLOCATION, s => s.MapFrom(s1 => s1.COMMERCIALPICKUPLOCATION ? 1 : 0));

                cfg.CreateMap<DBFactory.Entities.JobBookingDemoEntity, JobBookingDemoModel>();
                cfg.CreateMap<JobBookingDemoModel, DBFactory.Entities.JobBookingDemoEntity>();
                cfg.CreateMap<SSPClientMasterModel, DBFactory.Entities.SSPClientMasterEntity>();

                cfg.CreateMap<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>();

                cfg.CreateMap<DBFactory.Entities.QuotationShipmentEntity, QuotationShipmentPreviewModel>();
                cfg.CreateMap<DBFactory.Entities.QuotationChargeEntity, QuotationChargePreviewModel>();
                cfg.CreateMap<DBFactory.Entities.PaymentEntity, PaymentModel>();
                cfg.CreateMap<DBFactory.Entities.PaymentTransactionDetailsEntity, PaymentTransactionDetailsModel>();
                cfg.CreateMap<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>();

                cfg.CreateMap<DBFactory.Entities.Track, TrackModel>();
                cfg.CreateMap<DBFactory.Entities.TrackingEntity, TrackingModel>();
                cfg.CreateMap<DBFactory.Entities.UserEntity, UserModel>();
                cfg.CreateMap<DBFactory.Entities.UserProfileEntity, UserProfileModel>();
                cfg.CreateMap<UserProfileModel, DBFactory.Entities.UserProfileEntity>();

                cfg.CreateMap<DBFactory.Entities.Requests, RequestsModel>();
                cfg.CreateMap<DBFactory.Entities.Chat, ChatModel>();
                cfg.CreateMap<DBFactory.Entities.SupportEntity, SupportRequestsEnv>();
                cfg.CreateMap<UserProfileModel, DBFactory.Entities.UserProfileEntity>()
                .ForMember(d => d.EXISTINGCUSTOMER, s => s.MapFrom(s1 => s1.EXISTINGCUSTOMER ? 1 : 0))
                .ForMember(d => d.NOTIFICATIONSUBSCRIPTION, s => s.MapFrom(s1 => s1.NOTIFICATIONSUBSCRIPTION ? 1 : 0))
                .ForMember(d => d.ISFAPIAO, s => s.MapFrom(s1 => s1.ISFAPIAO ? 1 : 0))
                 .ForMember(d => d.ISINDIVIDUAL, s => s.MapFrom(s1 => s1.ISINDIVIDUAL ? 1 : 0))
                .ForMember(d => d.USEMYCREIDT, s => s.MapFrom(s1 => s1.USEMYCREIDT ? 1 : 0));
                
                cfg.CreateMap<DBFactory.Entities.UMUserAddressDetailsEntity, UserAddressModel>();
                cfg.CreateMap<DBFactory.Entities.PartyDetails, TrackingPartyDetailsModel>();
                cfg.CreateMap<DBFactory.Entities.PackageDetailsEntity, PackageDetailsModel>();
                cfg.CreateMap<DBFactory.Entities.ShpimentEventComments, ShpimentEventCommentsModel>();
                cfg.CreateMap<DBFactory.Entities.QuotationTermsConditionEntity, QuotationTermsAndConditionsModel>();
                cfg.CreateMap<DBFactory.Entities.CreditEntity, CreditModel>();
                cfg.CreateMap<DBFactory.Entities.NotificationEntity, NotificationModel>();
                cfg.CreateMap<DBFactory.Entities.SSPBranchListEntity, SSPBranchListModel>();
                cfg.CreateMap<DBFactory.Entities.SSPEntityListEntity, SSPEntityListModel>();
                cfg.CreateMap<DBFactory.Entities.DashBoardEntity, DBModel>();
                cfg.CreateMap<DBFactory.Entities.JobBookingShipmentEntity, JobBookingShipmentModel>();
                cfg.CreateMap<DBFactory.Entities.QuoteJobListEntity, QuoteJobListModel>();
                cfg.CreateMap<DBFactory.Entities.ScheduleEntity, ScheduleModel>();
                cfg.CreateMap<DBFactory.Entities.SSPPaymentOptionList, SSPPaymentOptionListModel>();
                cfg.CreateMap<DBFactory.Entities.QuoteOffersEntity, QuoteOffersModel>();

                cfg.CreateMap<DBFactory.Entities.QuotationChargeEntity, QOrgChargeEntity>();
                cfg.CreateMap<DBFactory.Entities.QuotationChargeEntity, QDesChargeEntity>();
                cfg.CreateMap<DBFactory.Entities.QuotationChargeEntity, QInterChargeEntity>();
                cfg.CreateMap<DBFactory.Entities.QuotationChargeEntity, QAdditionalChargeEntity>();
                cfg.CreateMap<DBFactory.Entities.QuotationChargeEntity, QOptionalChargeEntity>();
                cfg.CreateMap<DBFactory.Entities.TrackStatusCount, TrackStatusModelCount>();
                cfg.CreateMap<DBFactory.Entities.UserPartyMaster, UserPartyMasterModel>();
                cfg.CreateMap<DBFactory.Entities.CargoAvabilityEntity, CargoAvabilityModel>();
                cfg.CreateMap<DBFactory.Entities.IncoTermsEntity, IncotermsDetailsModel>();
                cfg.CreateMap<DBFactory.Entities.Compliance_Fields, Compliance_Fields_Model>();
                cfg.CreateMap<Compliance_Fields_Model, Compliance_Fields>();
                cfg.CreateMap<DBFactory.Entities.CreditDocEntity, CreditDoc>();
                cfg.CreateMap<DBFactory.Entities.CreditCommentEntity, CrediComment>();
                cfg.CreateMap<DBFactory.Entities.AddCreditCommentEntity, AddCrediComment>();
                cfg.CreateMap<DBFactory.Entities.UserProfileEntity, SupportRequestsEnv>();
                cfg.CreateMap<DBFactory.Entities.RefundPaymentEntity, RefundPaymentModel>();
                cfg.CreateMap<DBFactory.Entities.AdditionalCreditEntity, AdditionalCredit>();
                cfg.CreateMap<DBFactory.Entities.AddCreditDocEntity, AddCreditDoc>();
            });
        }
    }
}
