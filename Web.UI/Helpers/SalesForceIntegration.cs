﻿using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Models.QM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace FOCiS.SSP.Web.UI.Helpers
{
    public static class SalesForceIntegration
    {
        public  static async Task<HeaderSection> HeaderDetailsMapping(string transcationType,string eventType) 
        {
            HeaderSection HeaderSectionObject = new HeaderSection();
            try
            {
                HeaderSectionObject.TransactionID = GenerateTranscationNo(4);
                HeaderSectionObject.SenderID = "ShipaFreight";
                HeaderSectionObject.ReceiverID = "EIL";
                HeaderSectionObject.UserID = "client@agility.com";
                HeaderSectionObject.CreatedDateTime = System.DateTime.Now;
                HeaderSectionObject.CreatedDateTimeZone = "UTC";
                HeaderSectionObject.VersionNumber = "0.1";
                HeaderSectionObject.TransactionType = transcationType;
                HeaderSectionObject.PartnerID = eventType;
                HeaderSectionObject.Criteria = 0;
               
            }
            catch (Exception ex) 
            {
                throw ex;
            }
            return await Task.Run(() => HeaderSectionObject).ConfigureAwait(false);
        }

        public static async Task<UserDetails> UserSignUpDetailsMapping(UserEntity mDBEntity)
        {
            UserDetails UserMappingObject = new UserDetails();
            try
            {
                UserMappingObject.UserId = mDBEntity.USERID;
                UserMappingObject.AccountStatus = mDBEntity.ACCOUNTSTATUS;
                UserMappingObject.FirstName = mDBEntity.FIRSTNAME;
                UserMappingObject.LastName = mDBEntity.LASTNAME;
                UserMappingObject.CompanyName = mDBEntity.COMPANYNAME;
                UserMappingObject.MobileNumber = mDBEntity.MOBILENUMBER;
                UserMappingObject.WorkPhone = mDBEntity.MOBILENUMBER;
                UserMappingObject.ISTermsAgreed = mDBEntity.ISTERMSAGREED;
                UserMappingObject.CountryName = mDBEntity.COUNTRYNAME;
                UserMappingObject.CountryCode = mDBEntity.COUNTRYCODE;
                UserMappingObject.CountryId = mDBEntity.COUNTRYID;
                UserMappingObject.ISDCode = mDBEntity.ISDCODE;
                UserMappingObject.SubscriptionStatus = mDBEntity.NOTIFICATIONSUBSCRIPTION;
                UserMappingObject.ShipmentProcess = mDBEntity.SHIPMENTPROCESS;
                UserMappingObject.ShippingExperience = mDBEntity.SHIPPINGEXPERIENCE;  
            }
            catch (Exception ex) {
                throw ex;
            }
            return await Task.Run(() => UserMappingObject).ConfigureAwait(false);
        }

        public static async Task<UserDetails> UserProfileDetailsMapping(UserProfileEntity ProfileData, UserProfileEntity CreditData)
        {
            UserDetails UserMappingObject = new UserDetails();
            try
            {
                UserMappingObject.UserId = ProfileData.USERID;
                UserMappingObject.AccountStatus = ProfileData.ACCOUNTSTATUS;
                UserMappingObject.FirstName = ProfileData.FIRSTNAME;
                UserMappingObject.LastName = ProfileData.LASTNAME;
                UserMappingObject.CompanyName = ProfileData.COMPANYNAME;
                UserMappingObject.MobileNumber = ProfileData.MOBILENUMBER;
                UserMappingObject.WorkPhone = ProfileData.MOBILENUMBER;
                UserMappingObject.ISTermsAgreed = ProfileData.ISTERMSAGREED;
                UserMappingObject.CountryName = ProfileData.COUNTRYNAME;
                UserMappingObject.CountryCode = ProfileData.COUNTRYCODE;
                UserMappingObject.CountryId = ProfileData.COUNTRYID;
                UserMappingObject.ISDCode = ProfileData.ISDCODE;
                UserMappingObject.SubscriptionStatus = Convert.ToBoolean(ProfileData.NOTIFICATIONSUBSCRIPTION);
                UserMappingObject.ShipmentProcess = ProfileData.SHIPMENTPROCESS;
                UserMappingObject.ShippingExperience = ProfileData.SHIPPINGEXPERIENCE;
                UserMappingObject.FullAddress = ProfileData.UAFULLADDRESS;
                UserMappingObject.VatRegisterNumber = ProfileData.VATREGNUMBER;
                UserMappingObject.ExistingFreightForward = ProfileData.EXISTINGFREIGHTFORWARDER;
                UserMappingObject.PersonalAssistance = ProfileData.PERSONALASSISTANCE;
                UserMappingObject.ShipmentProcess = ProfileData.SHIPMENTPROCESS;
                UserMappingObject.ShippingExperience = ProfileData.SHIPPINGEXPERIENCE;
                UserMappingObject.LengthUnit = ProfileData.LENGTHUNIT;
                UserMappingObject.WeightUnit = ProfileData.WEIGHTUNIT;
                UserMappingObject.CurrentPaymentTerms = ProfileData.CURRENTPAYTERMSNAME;
                UserMappingObject.RegisteredDate = ProfileData.DATECREATED;
                UserMappingObject.JobTitle = ProfileData.OTHERJOBTITLE;
                UserMappingObject.PreferredCurrencyCode = ProfileData.PREFERREDCURRENCYCODE;
                UserMappingObject.UseMyCredit = Convert.ToBoolean(ProfileData.USEMYCREIDT);
                UserMappingObject.CurrencyId = ProfileData.CURRENCYID;
                UserMappingObject.LastLoginDate = ProfileData.LASTLOGONTIME;
                UserMappingObject.ExistingCustomer = Convert.ToBoolean(ProfileData.EXISTINGCUSTOMER);
                UserMappingObject.CityName = ProfileData.UACITYNAME;
                UserMappingObject.ApprovedCreditlimit = CreditData.APPROVEDCREDITLIMIT;
                UserMappingObject.RequestedCreditlimit = CreditData.REQUESTEDCREDITLIMIT;
                UserMappingObject.CreditBalance = CreditData.CURRENTOSBALANCE;
              
            }
            catch (Exception ex) 
            {
                throw ex;
            }
            return await Task.Run(() => UserMappingObject).ConfigureAwait(false);
        }

        public static async Task<QuotationDetails> QuotationDetailsMapping(QuotationEntity mDBEntity, string UserHostAddress)
        {
            QuotationDetails QuotationObject = new QuotationDetails();
            try
            {
                if (mDBEntity.CREATEDBY != "guest")
                {
                    QuotationObject.UserId = mDBEntity.CREATEDBY;
                }
                else
                {
                    QuotationObject.UserId = mDBEntity.GUESTEMAIL;
                }
                QuotationObject.QuotationId = mDBEntity.QUOTATIONID.ToString();
                QuotationObject.QuotationUrl = UserHostAddress;
                QuotationObject.QuotationNumber = mDBEntity.QUOTATIONNUMBER;
                QuotationObject.DateCreated = mDBEntity.DATEOFENQUIRY;
                QuotationObject.State = mDBEntity.STATEID;
                QuotationObject.QuotationDescription = mDBEntity.SHIPMENTDESCRIPTION;
                QuotationObject.MovementType = mDBEntity.MOVEMENTTYPENAME;
                QuotationObject.Product = mDBEntity.PRODUCTNAME;
                QuotationObject.ProductType = mDBEntity.PRODUCTTYPENAME;
                QuotationObject.OriginCountry = mDBEntity.OCOUNTRYNAME;
                QuotationObject.DestinationCountry = mDBEntity.DCOUNTRYNAME;
                //QuotationObject.UserCountry = mDBEntity.;
                QuotationObject.OriginPortName = mDBEntity.ORIGINPORTNAME;
                QuotationObject.DestinationPortName = mDBEntity.DESTINATIONPORTNAME;
                QuotationObject.PreferredCurrency = mDBEntity.PREFERREDCURRENCYID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => QuotationObject).ConfigureAwait(false);
        }

        public static async Task<UserDetails> GusetUserMapping(QuotationEntity mDBEntity)
        {
            UserDetails UserMappingObject = new UserDetails();
            try
            {
                UserMappingObject.UserId = mDBEntity.GUESTEMAIL;
                UserMappingObject.FirstName = mDBEntity.GUESTNAME;
                UserMappingObject.LastName = mDBEntity.GUESTLASTNAME;
                UserMappingObject.CompanyName = mDBEntity.GUESTCOMPANY;
                UserMappingObject.CountryCode = mDBEntity.GUESTCOUNTRYCODE;
                //UserMappingObject.PersonalAssistance = mDBEntity.PERSONALASSISTANCE;
                UserMappingObject.UseofQuote = mDBEntity.USEOFQUOTE;
                UserMappingObject.ShipmentProcess = mDBEntity.SHIPMENTPROCESS;
                UserMappingObject.SubscriptionStatus = Convert.ToBoolean(mDBEntity.NOTIFICATIONSUBSCRIPTION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => UserMappingObject).ConfigureAwait(false);
        }

        public static async Task<BookingDetails> BookingDetailsMapping(JobBookingModel mUIModel, string UserHostAddress)
        {
            BookingDetails BookingObject = new BookingDetails();
            try
            {
                BookingObject.BookingId = mUIModel.JOPERATIONALJOBNUMBER;
                BookingObject.ConsignmentId = null;
                BookingObject.Shipper = mUIModel.SHCLIENTNAME;
                BookingObject.Consignee = mUIModel.CONCLIENTNAME;
                BookingObject.MovementType = mUIModel.MOVEMENTTYPENAME;
                BookingObject.Product = mUIModel.PRODUCTNAME;
                BookingObject.ProductType = mUIModel.PRODUCTTYPENAME;
                BookingObject.OriginCountry = null;
                BookingObject.DestinationCountry = null;
                BookingObject.UserCountry = mUIModel.PartiesModel.Where(x => x.UPERSONAL == 1).Select(n => n.COUNTRYNAME).FirstOrDefault();
                BookingObject.OriginPortName = mUIModel.ORIGINPORTNAME;
                BookingObject.DestinationPortName = mUIModel.DESTINATIONPORTNAME;
                BookingObject.DestinationPortName = mUIModel.DESTINATIONPORTNAME;
                BookingObject.UnitofMeasure = null;
                BookingObject.TotalGrossWeight = mUIModel.TOTALGROSSWEIGHT;
                BookingObject.ShipperClientId = mUIModel.SHCLIENTID;
                BookingObject.ConsigneeClientId = mUIModel.CONCLIENTID;
                BookingObject.NotifyClientId = null;
                BookingObject.GLNumber = null;
                BookingObject.PaymentType = null;
                BookingObject.PreferredCurrency = mUIModel.PREFERREDCURRENCYID;
                BookingObject.BookingAmount = 0;
                BookingObject.AdditionalAmount = 0;
                BookingObject.GrossRevenue = 0;
                BookingObject.GrossRevenue_USD = 0;
                BookingObject.GrandTotal = mUIModel.GRANDTOTAL;
                BookingObject.DateCreated = System.DateTime.Now;
                BookingObject.UserId = mUIModel.CREATEDBY;
                BookingObject.QuotationId = mUIModel.QUOTATIONID.ToString();
                BookingObject.BookingUrl = UserHostAddress;
                BookingObject.State = mUIModel.STATEID;
                BookingObject.DiscountCode = null;
                BookingObject.DiscountAmount = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => BookingObject).ConfigureAwait(false);
        
        }


        public static async Task<BookingDetails> PayementDetailsMapping(JobBookingEntity mUIModel, string UserHostAddress)
        {
            BookingDetails BookingObject = new BookingDetails();
            try
            {
                BookingObject.BookingId = mUIModel.JOPERATIONALJOBNUMBER;
                BookingObject.ConsignmentId = null;
                BookingObject.Shipper = mUIModel.SHCLIENTNAME;
                BookingObject.Consignee = mUIModel.CONCLIENTNAME;
                BookingObject.MovementType = mUIModel.MOVEMENTTYPENAME;
                BookingObject.Product = mUIModel.PRODUCTNAME;
                BookingObject.ProductType = mUIModel.PRODUCTTYPENAME;
                BookingObject.OriginCountry = null;
                BookingObject.DestinationCountry = null;
                BookingObject.UserCountry = null;
                BookingObject.OriginPortName = mUIModel.ORIGINPORTNAME;
                BookingObject.DestinationPortName = mUIModel.DESTINATIONPORTNAME;
                BookingObject.DestinationPortName = mUIModel.DESTINATIONPORTNAME;
                BookingObject.UnitofMeasure = null;
                BookingObject.TotalGrossWeight = mUIModel.TOTALGROSSWEIGHT;
                BookingObject.ShipperClientId = mUIModel.SHCLIENTID;
                BookingObject.ConsigneeClientId = mUIModel.CONCLIENTID;
                BookingObject.NotifyClientId = null;
                BookingObject.GLNumber = null;
                BookingObject.PaymentType = null;
                BookingObject.PreferredCurrency = mUIModel.PREFERREDCURRENCYID;
                BookingObject.BookingAmount = 0;
                BookingObject.AdditionalAmount = 0;
                BookingObject.GrossRevenue = 0;
                BookingObject.GrossRevenue_USD = 0;
                BookingObject.GrandTotal = mUIModel.GRANDTOTAL;
                BookingObject.DateCreated = System.DateTime.Now;
                BookingObject.UserId = mUIModel.CREATEDBY;
                BookingObject.QuotationId = mUIModel.QUOTATIONID.ToString();
                BookingObject.BookingUrl = UserHostAddress;
                BookingObject.State = mUIModel.STATEID;
                BookingObject.DiscountCode = null;
                BookingObject.DiscountAmount = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => BookingObject).ConfigureAwait(false);

        }

        public static async Task<BookingDetails> PayementTranscationOnlineMapping(PaymentModel mUIModel,QuotationPreviewModel QModel,string TranscationType, string PaymentMode, double PayAmount, string UserHostAddress)
        {
            BookingDetails BookingObject = new BookingDetails();
            try
            {
                BookingObject.BookingId = mUIModel.OPjobnumber;
                BookingObject.ConsignmentId = null;
                BookingObject.Shipper = mUIModel.ShipperName;
                BookingObject.Consignee = mUIModel.ConsigneeName;
                BookingObject.MovementType = mUIModel.MovementType;
                BookingObject.Product = QModel.ProductName;
                BookingObject.ProductType = QModel.ProductTypeName;
                BookingObject.OriginCountry = null;
                BookingObject.DestinationCountry = null;
                BookingObject.UserCountry = mUIModel.USERCOUNTRY;
                BookingObject.OriginPortName = QModel.OriginPortName;
                BookingObject.DestinationPortName = QModel.DestinationPortName;
                BookingObject.UnitofMeasure = null;
                BookingObject.TotalGrossWeight = QModel.TotalGrossWeight;
                BookingObject.ShipperClientId = null;
                BookingObject.ConsigneeClientId = null;
                BookingObject.NotifyClientId = null;
                BookingObject.GLNumber = null;
                BookingObject.PaymentType = PaymentMode;
                BookingObject.PreferredCurrency = mUIModel.Currency;
                BookingObject.BookingAmount = mUIModel.ReceiptNetAmount;
                BookingObject.AdditionalAmount = 0;
                BookingObject.GrossRevenue = 0;
                BookingObject.GrossRevenue_USD = mUIModel.NOTSUPPAMOUNTUSD;
                BookingObject.GrandTotal = Convert.ToDecimal(PayAmount);
                BookingObject.DateCreated = System.DateTime.Now;
                BookingObject.UserId = QModel.CreatedBy;
                BookingObject.QuotationId = mUIModel.QuotationId.ToString();
                BookingObject.BookingUrl = UserHostAddress;
                BookingObject.State = TranscationType;
                BookingObject.DiscountCode = mUIModel.DiscountCode;
                BookingObject.DiscountAmount = mUIModel.DiscountAmount;
                BookingObject.DiscountExpiryDate = mUIModel.DiscountExpiryDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => BookingObject).ConfigureAwait(false);

        }

        public static async Task<BookingDetails> PayementTranscationOfflineMapping(PaymentTransactionDetailsModel TranscationalModel, PaymentModel mUIModel, string TranscationType, string PaymentMode, double PayAmount, string UserHostAddress)
        {
            BookingDetails BookingObject = new BookingDetails();
            try
            {
                BookingObject.BookingId = mUIModel.OPjobnumber;
                BookingObject.ConsignmentId = null;
                BookingObject.Shipper = mUIModel.ShipperName;
                BookingObject.Consignee = mUIModel.ConsigneeName;
                BookingObject.MovementType = mUIModel.MovementType;
                BookingObject.Product = null;
                BookingObject.ProductType = null;
                BookingObject.OriginCountry = null;
                BookingObject.DestinationCountry = null;
                BookingObject.UserCountry = mUIModel.USERCOUNTRY;
                BookingObject.OriginPortName = null;
                BookingObject.DestinationPortName = null;
                BookingObject.DestinationPortName = null;
                BookingObject.UnitofMeasure = null;
                BookingObject.TotalGrossWeight = 0;
                BookingObject.ShipperClientId = null;
                BookingObject.ConsigneeClientId = null;
                BookingObject.NotifyClientId = null;
                BookingObject.GLNumber = null;
                BookingObject.PaymentType = TranscationalModel.PAYMENTTYPE;
                BookingObject.PreferredCurrency = mUIModel.Currency;
                BookingObject.BookingAmount = Convert.ToDecimal(TranscationalModel.TRANSAMOUNT);
                BookingObject.AdditionalAmount = TranscationalModel.ADDITIONALCHARGEAMOUNT;
                BookingObject.GrossRevenue = 0;
                BookingObject.GrossRevenue_USD = mUIModel.NOTSUPPAMOUNTUSD;
                BookingObject.GrandTotal = Convert.ToDecimal(TranscationalModel.TRANSAMOUNT);
                BookingObject.DateCreated = System.DateTime.Now;
                BookingObject.UserId = mUIModel.UserId;
                BookingObject.QuotationId = mUIModel.QuotationId.ToString();
                BookingObject.BookingUrl = UserHostAddress;
                BookingObject.State = TranscationType;
                BookingObject.DiscountCode = mUIModel.DiscountCode;
                BookingObject.DiscountAmount = mUIModel.DiscountAmount;
                BookingObject.DiscountExpiryDate = mUIModel.DiscountExpiryDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => BookingObject).ConfigureAwait(false);

        }

        public static string GenerateTranscationNo(int length)
        {
            const string chars = "0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}