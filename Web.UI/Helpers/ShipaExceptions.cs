﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOCiS.SSP.Web.UI.Helpers
{
    public class ShipaExceptions : Exception
    {
        public ShipaExceptions(string message)
            : base(message)
        {
        }
    }
}