﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory;
using System.Data;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace FOCiS.SSP.Web.UI.Helpers
{
    public static class LocalizedConverter
    {
        public static T GetEnumConversionByLanguage<T>(this Enum enumValue)
        {
            string message = string.Empty;
            try
            {
                string conversionValue = enumValue.GetType().GetMember(enumValue.ToString()).First().GetCustomAttribute<DisplayAttribute>().Name;
                var resourceManager = new ResourceManager(typeof(FOCiS.Dictionary.App_GlobalResources.Resource));
                string uiCulture = CultureInfo.CurrentUICulture.Name.ToString();
                string trimValue = Regex.Replace(conversionValue, @"\s", "");
                message = resourceManager.GetString(trimValue, CultureInfo.CreateSpecificCulture(uiCulture));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return (T)Convert.ChangeType(message, typeof(T));
        }

        public static async Task<T> GetConversionDataByLanguage<T>(string languageCode, T dynamiConversionObject)
        {
            try
            {
                var resourceManager = new ResourceManager(typeof(FOCiS.Dictionary.App_GlobalResources.Resource));
                var stringProps = dynamiConversionObject
                                 .GetType()
                                 .GetProperties()
                                 .Where(p => p.PropertyType == typeof(string));
                foreach (var prop in stringProps)
                {
                    string value = (string)prop.GetValue(dynamiConversionObject);
                    if (!string.IsNullOrEmpty(value))
                    {
                        string trimValue = Regex.Replace(value, @"\s", "");
                        string ConvertValue = resourceManager.GetString(trimValue, CultureInfo.CreateSpecificCulture(languageCode));
                        if (ConvertValue != null)
                        {
                            prop.SetValue(dynamiConversionObject, ConvertValue);
                        }
                    }

                }

                Type t = dynamiConversionObject.GetType();

                PropertyInfo property = t.GetProperty("ShipmentItems");

                object packagelist = property.GetValue(dynamiConversionObject);

                IList objpackage = (IList)packagelist;
                for (int i = 0; i < objpackage.Count; i++)
                {
                    var PropertyPackage = objpackage[i]
                                     .GetType()
                                     .GetProperties().Where(p => p.PropertyType == typeof(string));
                    foreach (var prop in PropertyPackage)
                    {
                        string value = (string)prop.GetValue(objpackage[i]);
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (value.Trim() == "20' General Purpose" || value.Trim() == "40' General Purpose" || value.Trim() == "40' High Cube")
                            {
                                value = Regex.Replace(value, @"[^0-9a-zA-Z]+", "");
                                var array = Regex.Matches(value, @"\D+|\d+")
                                              .Cast<Match>()
                                             .Select(m => m.Value)
                                             .ToArray();
                                value = array[1] + array[0];
                            }
                            string trimValue = Regex.Replace(value.Trim(), @"\s", "");
                            string my_String = Regex.Replace(trimValue, @"[^0-9a-zA-Z]+", "");
                            string ConvertValue = resourceManager.GetString(my_String, CultureInfo.CreateSpecificCulture(languageCode));
                            if (ConvertValue != null)
                            {
                                prop.SetValue(objpackage[i], ConvertValue);
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return await Task.Run(() => (T)dynamiConversionObject).ConfigureAwait(false);

        }

        public static async Task<List<T>> GetConversionQuotationListByLanguage<T>(string languageCode, List<T> dynamiConversionObject)
        {
            try
            {
                var resourceManager = new ResourceManager(typeof(FOCiS.Dictionary.App_GlobalResources.Resource));
                for (int i = 0; i < dynamiConversionObject.Count; i++)
                {
                    var stringProps = dynamiConversionObject[i]
                                     .GetType()
                                     .GetProperties()
                                     .Where(p => p.PropertyType == typeof(string));
                    foreach (var prop in stringProps)
                    {
                        string value = (string)prop.GetValue(dynamiConversionObject[i]);
                        if (!string.IsNullOrEmpty(value))
                        {
                            string trimValue = Regex.Replace(value, @"\s", "");
                            string ConvertValue = resourceManager.GetString(trimValue, CultureInfo.CreateSpecificCulture(languageCode));
                            if (ConvertValue != null)
                            {
                                prop.SetValue(dynamiConversionObject[i], ConvertValue);
                            }
                        }

                    }

                }

                for (int j = 0; j < dynamiConversionObject.Count; j++)
                {
                    Type dynamicObject = dynamiConversionObject[j].GetType();
                    PropertyInfo property = dynamicObject.GetProperty("ShipmentItems");
                    object packagelist = property.GetValue(dynamiConversionObject[j]);
                    IList objpackage = (IList)packagelist;
                    for (int i = 0; i < objpackage.Count; i++)
                    {
                        var PropertyPackage = objpackage[i]
                                         .GetType()
                                         .GetProperties().Where(p => p.PropertyType == typeof(string));
                        foreach (var prop in PropertyPackage)
                        {
                            string value = (string)prop.GetValue(objpackage[i]);
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (value.Trim() == "20' General Purpose" || value.Trim() == "40' General Purpose" || value.Trim() == "40' High Cube")
                                {
                                    value = Regex.Replace(value, @"[^0-9a-zA-Z]+", "");
                                    var array = Regex.Matches(value, @"\D+|\d+")
                                                  .Cast<Match>()
                                                 .Select(m => m.Value)
                                                 .ToArray();
                                    value = array[1] + array[0];
                                }
                                string trimValue = Regex.Replace(value.Trim(), @"\s", "");
                                string my_String = Regex.Replace(trimValue, @"[^0-9a-zA-Z]+", "");
                                string ConvertValue = resourceManager.GetString(my_String, CultureInfo.CreateSpecificCulture(languageCode));
                                if (ConvertValue != null)
                                {
                                    prop.SetValue(objpackage[i], ConvertValue);
                                }
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return await Task.Run(() => dynamiConversionObject as List<T>).ConfigureAwait(false);

        }

        public static async Task<List<T>> ChargesQuotelanguageconversion<T>(string languageCode, QuotationEntity dynamiConversionObject)
        {
            try
            {
                QuotationDBFactory mQuotationFactory = new QuotationDBFactory();
                DataTable dt = mQuotationFactory.ChargeNamebyLanguagetable(languageCode);
                for (int i = 0; i < dynamiConversionObject.QuotationCharges.Count; i++)
                {
                    var stringProps = dynamiConversionObject.QuotationCharges[i]
                                        .GetType()
                                        .GetProperties()
                                        .Where(p => p.PropertyType == typeof(string));
                    foreach (var prop in stringProps)
                    {
                        string ChargeName = (string)prop.GetValue(dynamiConversionObject.QuotationCharges[i]);
                        if (!string.IsNullOrEmpty(ChargeName) && ChargeName != "0" && dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string originalrequirement = dr["ORIGINALREQUIREMENTDATA"].ToString();
                                string LanguageChargeName = string.Empty;
                                if (originalrequirement == ChargeName)
                                {
                                    LanguageChargeName = dr["MODIFIEDREQUIREMENTDATA"].ToString();
                                    if (string.IsNullOrEmpty(LanguageChargeName))
                                    {
                                        prop.SetValue(dynamiConversionObject.QuotationCharges[i], ChargeName);
                                    }
                                    else
                                    {
                                        prop.SetValue(dynamiConversionObject.QuotationCharges[i], LanguageChargeName);
                                    }
                                }

                            }
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return await Task.Run(() => dynamiConversionObject as List<T>).ConfigureAwait(false);
        }

        public static async Task<List<T>> ChargesBooklanguageconversion<T>(string languageCode, JobBookingEntity dynamiConversionObject)
        {
            try
            {
                QuotationDBFactory mQuotationFactory = new QuotationDBFactory();
                DataTable dt = mQuotationFactory.ChargeNamebyLanguagetable(languageCode);
                for (int i = 0; i < dynamiConversionObject.PaymentCharges.Count; i++)
                {
                    var stringProps = dynamiConversionObject.PaymentCharges[i]
                                        .GetType()
                                        .GetProperties()
                                        .Where(p => p.PropertyType == typeof(string));
                    foreach (var prop in stringProps)
                    {
                        string ChargeName = (string)prop.GetValue(dynamiConversionObject.PaymentCharges[i]);
                        if (!string.IsNullOrEmpty(ChargeName) && ChargeName != "0" && dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string originalrequirement = dr["ORIGINALREQUIREMENTDATA"].ToString();
                                string LanguageChargeName = string.Empty;
                                if (originalrequirement == ChargeName)
                                {
                                    LanguageChargeName = dr["MODIFIEDREQUIREMENTDATA"].ToString();
                                    if (string.IsNullOrEmpty(LanguageChargeName))
                                    {
                                        prop.SetValue(dynamiConversionObject.PaymentCharges[i], ChargeName);
                                    }
                                    else
                                    {
                                        prop.SetValue(dynamiConversionObject.PaymentCharges[i], LanguageChargeName);
                                    }
                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return await Task.Run(() => dynamiConversionObject as List<T>).ConfigureAwait(false);
        }

        public static async Task<string[]> GetConversionDataBySingleObject<T>(string languageCode, string ConversionObjectName, string ConversionObjectType)
        {
            string[] ConversionObject;
            try
            {
                QuotationDBFactory mQuotationFactory = new QuotationDBFactory();
                ConversionObject = mQuotationFactory.GetConversionSupportDataBySingleObject(languageCode, ConversionObjectName, ConversionObjectType);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return await Task.Run(() => ConversionObject).ConfigureAwait(false);
        }

        public static async Task<SupportEntity> GetConversionSupportDataByMultiObject<T>(string languageCode, SupportEntity SupportRequests)
        {
            SupportEntity ConversionObject = new SupportEntity();
            QuotationDBFactory mQuotationFactory = new QuotationDBFactory();
            try
            {
                ConversionObject = mQuotationFactory.GetConversionSupportDataByMultiObject(languageCode, SupportRequests);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return await Task.Run(() => ConversionObject).ConfigureAwait(false);
        }

        public static async Task<List<T>> GetConversionObjectListByLanguage<T>(string languageCode, List<T> dynamiConversionObject)
        {
            try
            {
                var resourceManager = new ResourceManager(typeof(FOCiS.Dictionary.App_GlobalResources.Resource));
                for (int i = 0; i < dynamiConversionObject.Count; i++)
                {
                    var stringProps = dynamiConversionObject[i]
                                     .GetType()
                                     .GetProperties()
                                     .Where(p => p.PropertyType == typeof(string));
                    foreach (var prop in stringProps)
                    {
                        string value = (string)prop.GetValue(dynamiConversionObject[i]);
                        if (!string.IsNullOrEmpty(value))
                        {
                            string trimValue = Regex.Replace(value, @"\s|[().]", "");
                            string ConvertValue = resourceManager.GetString(trimValue, CultureInfo.CreateSpecificCulture(languageCode));
                            if (ConvertValue != null)
                            {
                                prop.SetValue(dynamiConversionObject[i], ConvertValue);
                            }
                        }

                    }

                }

              
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return await Task.Run(() => dynamiConversionObject as List<T>).ConfigureAwait(false);

        }
        public static string GetNotificationConversionDataByLanguage(string notificationcode)
        {
            ResourceManager rm = new ResourceManager(typeof(FOCiS.Dictionary.App_GlobalResources.Resource));
            return rm.GetString(notificationcode);
        }
    }
}