﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Web.Routing;
using System.Security.Cryptography;
using System.IO;
using FOCiS.SSP.Web.UI.Controllers;

namespace Chikoti.Helpers
{
    public static class MyExtensions
    {
        public static MvcHtmlString EncodedActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues, object htmlAttributes, string cssclass, int? span)
        {
            string queryString = string.Empty;
            string htmlAttributesString = string.Empty;
            if (routeValues != null)
            {
                RouteValueDictionary d = new RouteValueDictionary(routeValues);
                for (int i = 0; i < d.Keys.Count; i++)
                {
                    if (i > 0)
                    {
                        queryString += "?";
                    }
                    queryString += d.Keys.ElementAt(i) + "=" + d.Values.ElementAt(i);
                }
            }
            if (htmlAttributes != null)
            {
                RouteValueDictionary d = new RouteValueDictionary(htmlAttributes);
                for (int i = 0; i < d.Keys.Count; i++)
                {
                    htmlAttributesString += " " + d.Keys.ElementAt(i) + "=" + d.Values.ElementAt(i);
                }
            }

            StringBuilder ancor = new StringBuilder();
            ancor.Append("<a ");
            if (htmlAttributesString != string.Empty)
            {
                ancor.Append(htmlAttributesString);
            }
            ancor.Append(" href='");
            if (controllerName != string.Empty)
            {
                ancor.Append("/" + controllerName);
            }
            if (actionName != "Index")
            {
                ancor.Append("/" + actionName);
            }
            if (queryString != string.Empty)
            {
                char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

                string[] mul = queryString.Split('?');
                for (int jk = 0; jk < mul.Length; jk++)
                {
                    if (jk == 0)
                        ancor.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                    else
                        ancor.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                }
            }
            ancor.Append("'");
            if (cssclass == "_blank")
            {
                ancor.Append("target='");
                ancor.Append(cssclass);
                ancor.Append("'");
            }
            else
            {
                ancor.Append("class='");
                ancor.Append(cssclass);
                ancor.Append("'");
            }
            if (linkText == "Regenerate")
            {
                ancor.Append("onclick='Loaderwithtext()'");
            }
            if (linkText == "Use Template")
            {
                ancor.Append("onclick='UseTemplate()'");
            }
            if (actionName != "Download" && actionName != "DownloadAttchment" && linkText != "Regenerate")
            {
                ancor.Append("onclick='Loader()'");
            }
            ancor.Append(">");
            if (span == 9)
            {
                ancor.Append("<i class='icon-dload-inactive mr-r10'></i><span>");
                ancor.Append(linkText);
                ancor.Append("</span>");
            }
            else
            {
                ancor.Append(linkText);
            }
            ancor.Append("</a>");
            ancor.Append("");
            return new MvcHtmlString(ancor.ToString());
        }

        public static string EncodedDownloadLink(string linkText, string actionName, string controllerName, object routeValues, object htmlAttributes, string cssclass, int? span)
        {
            string queryString = string.Empty;
            string htmlAttributesString = string.Empty;
            if (routeValues != null)
            {
                RouteValueDictionary d = new RouteValueDictionary(routeValues);
                for (int i = 0; i < d.Keys.Count; i++)
                {
                    if (i > 0)
                    {
                        queryString += "?";
                    }
                    queryString += d.Keys.ElementAt(i) + "=" + d.Values.ElementAt(i);
                }
            }
            if (htmlAttributes != null)
            {
                RouteValueDictionary d = new RouteValueDictionary(htmlAttributes);
                for (int i = 0; i < d.Keys.Count; i++)
                {
                    htmlAttributesString += " " + d.Keys.ElementAt(i) + "=" + d.Values.ElementAt(i);
                }
            }

            StringBuilder ancor = new StringBuilder();
            ancor.Append("<a ");
            if (htmlAttributesString != string.Empty)
            {
                ancor.Append(htmlAttributesString);
            }
            ancor.Append(" href='");
            if (controllerName != string.Empty)
            {
                ancor.Append("/" + controllerName);
            }
            if (actionName != "Index")
            {
                ancor.Append("/" + actionName);
            }
            if (queryString != string.Empty)
            {
                char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

                string[] mul = queryString.Split('?');
                for (int jk = 0; jk < mul.Length; jk++)
                {
                    if (jk == 0)
                        ancor.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                    else
                        ancor.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                }
            }
            ancor.Append("'");
            if (cssclass == "_blank")
            {
                ancor.Append("target='");
                ancor.Append(cssclass);
                ancor.Append("'");
            }
            else
            {
                ancor.Append("class='");
                ancor.Append(cssclass);
                ancor.Append("'");
            }
            if (linkText == "Regenerate")
            {
                ancor.Append("onclick='Loaderwithtext()'");
            }
            if (linkText == "Use Template")
            {
                ancor.Append("onclick='UseTemplate()'");
            }
            // if (linkText != "Download" && actionName != "DownloadAttchment" && linkText != "Regenerate")
            //{
            //    ancor.Append("onclick='Loader()'");
            //}
            if (actionName != "Download" && actionName != "DownloadAttchment" && linkText != "Regenerate")
            {
                ancor.Append("onclick='Loader()'");
            }
            ancor.Append(">");
            if (span == 9)
            {
                ancor.Append("<i class='icon-dload-inactive mr-r10'></i><span>");
                ancor.Append(linkText);
                ancor.Append("</span>");
            }
            else
            {
                ancor.Append(linkText);
            }
            ancor.Append("</a>");
            ancor.Append("");
            return ancor.ToString();
        }
    }
}