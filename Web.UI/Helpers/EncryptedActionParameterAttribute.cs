﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.IO;

namespace FOCiS.SSP.Web.UI.Helpers
{

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class EncryptedActionParameterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Dictionary<string, object> decryptedParameters = new Dictionary<string, object>();
            if (HttpContext.Current.Request.QueryString.AllKeys.Count() > 0)
            {
                for (int kk = 0; kk < HttpContext.Current.Request.QueryString.AllKeys.Count(); kk++)
                {
                    if (HttpContext.Current.Request.QueryString.AllKeys[0] != "gclid"
                        && HttpContext.Current.Request.QueryString.AllKeys[kk] != null)
                    {
                        string encryptedQueryString = HttpContext.Current.Request.QueryString.Get(HttpContext.Current.Request.QueryString.AllKeys[kk]);
                        string decrptedString = FOCiS.SSP.Web.UI.Controllers.Encryption.Decrypt(encryptedQueryString.ToString());
                        string[] paramsArrs = decrptedString.Split('?');
                        for (int i = 0; i < paramsArrs.Length; i++)
                        {
                            string[] paramArr = paramsArrs[i].Split('=');
                            if (paramArr[1] != "")
                            {
                                int n;
                                if (int.TryParse(paramArr[1], out n))
                                {
                                    decryptedParameters.Add(paramArr[0], Convert.ToInt32(paramArr[1]));
                                }
                                else
                                {
                                    decryptedParameters.Add(paramArr[0], Convert.ToString(paramArr[1]));
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < decryptedParameters.Count; i++)
                {
                    filterContext.ActionParameters[decryptedParameters.Keys.ElementAt(i)] = decryptedParameters.Values.ElementAt(i);
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }
}