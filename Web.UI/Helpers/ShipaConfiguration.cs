﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FOCiS.SSP.Web.UI.Helpers
{
    public class ShipaConfiguration
    {
        public static class InternalUrls
        {
            public static string ShipaApiUrl { get { return ConfigurationManager.AppSettings["APPURL"]; } }
            public static string OneSignalKey { get {return ConfigurationManager.AppSettings["OneSignalKey"];}}
            public static string OneSignalId { get { return ConfigurationManager.AppSettings["OneSignalId"]; } }
            public static string ListGuestUser { get { return ConfigurationManager.AppSettings["ListGuestUser"]; } }
            public static string ListRegisteredUser { get { return ConfigurationManager.AppSettings["ListRegisteredUser"]; } }
            public static string SFSubScriberUser { get { return ConfigurationManager.AppSettings["SFSubScriberUser"]; } }
            public static string SFUnSubScriberUser { get { return ConfigurationManager.AppSettings["SFUnSubScriberUser"]; } }
            public static string ListUserAddress { get { return ConfigurationManager.AppSettings["sender_addr1"]; } }
            public static string UserCountry { get { return ConfigurationManager.AppSettings["sender_country"]; } }
            public static string SenderZipCode { get { return ConfigurationManager.AppSettings["sender_zip"]; } }
            public static string SenderUrl { get { return ConfigurationManager.AppSettings["sender_url"]; } }
            public static string SenderName { get { return ConfigurationManager.AppSettings["sender_name"]; } }
            public static string SenderCity { get { return ConfigurationManager.AppSettings["sender_city"]; } }
            public static string SenderReminder { get { return ConfigurationManager.AppSettings["sender_reminder"]; } }
        }
    }
}