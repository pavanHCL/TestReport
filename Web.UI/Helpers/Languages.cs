﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace FOCiS.SSP.Web.UI.Helpers
{
    public class Languages
    {

        public Languages()
        {
            _AvialableLanguages = GetAllLanguages().ToList();
        }

        private static List<Language> languagesItems = null;
        private static List<Language> _AvialableLanguages;
        public static List<Language> AvailableLanguages
        {
             get {
                    if (_AvialableLanguages != null)
                    {
                        return _AvialableLanguages;
                    }
                    else
                    {
                       _AvialableLanguages= GetAllLanguages().ToList();
                       return _AvialableLanguages;
                    }
             }
        }
        public static bool IsLanguageAvailable(string lang)
        {
            return AvailableLanguages.FirstOrDefault(a => a.LanguageCulture.Equals(lang)) != null ? true : false;
        }
        public static string GetDefaultLanguage()
        {
            return AvailableLanguages[0].LanguageCulture;
        }
        public void SetLanguage(string lang)
        {
            try
            {
                if (!IsLanguageAvailable(lang))
                {
                    lang = GetDefaultLanguage();
                }
                var cultureInfo = new CultureInfo(lang);
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                HttpCookie langCookie = new HttpCookie("shipaculture", lang);
                langCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(langCookie);
            }
            catch (Exception ex) 
            { 
                //
            }
        }

         public static IEnumerable<Language> GetAllLanguages()
         {
             try
             {
                 List<Language> list = null;
                 var objlanguages = System.Web.HttpRuntime.Cache["languages"];
                 if (objlanguages == null)
                 {
                     string path = HttpRuntime.AppDomainAppPath;
                     using (StreamReader r = new StreamReader(path + "/JsonData/LanguagesList.json"))
                     {
                         string json = r.ReadToEnd();
                         languagesItems = JsonConvert.DeserializeObject<List<Language>>(json);
                         System.Web.HttpRuntime.Cache["languages"] = languagesItems;

                         System.Web.HttpRuntime.Cache.Insert("languages", languagesItems, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                         list = languagesItems;
                     }
                 }
                 else
                 {
                     List<Language> objLanguageList = objlanguages as List<Language>;
                     languagesItems = objLanguageList;
                     list = languagesItems.ToList();
                 }
                 return list;
             }
             catch (Exception ex)
             {
                 return null;
             }
        }
         
         
    
    }
    public class Language
    {
        public string LanguageCulture { get; set; }

        public string LanguageName { get; set; }
    }
}