﻿using FOCiS.SSP.Models.CacheManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FOCiS.SSP.Web.UI.CacheMechanisim
{
    public class MyCacheSource
    {
        private List<CountryInformation> SourceItems = null;
        private List<CurrencyInformation> CurrencyItems = null;

        public IEnumerable<CountryInformation> GetAllDefaultCountryList(string SearchTerm)
        {
            List<CountryInformation> list = null;
            if (SearchTerm == string.Empty)
            {
                if (System.Web.HttpRuntime.Cache["countries"] == null)
                {
                    string path = HttpRuntime.AppDomainAppPath;
                    using (StreamReader r = new StreamReader(path + "/JsonData/CountriesList.json"))
                    {
                        string json = r.ReadToEnd();
                        SourceItems = JsonConvert.DeserializeObject<List<CountryInformation>>(json);
                        System.Web.HttpRuntime.Cache["countries"] = SourceItems;

                        System.Web.HttpRuntime.Cache.Insert("countries", SourceItems, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                        list = SourceItems;
                    }

                }
                else {
                    list = HttpRuntime.Cache["countries"] as List<CountryInformation>;
                }
            }
            else
            {
                var objcountry = System.Web.HttpRuntime.Cache["countries"];
                if (objcountry == null)
                {
                    string path = HttpRuntime.AppDomainAppPath;
                    using (StreamReader r = new StreamReader(path + "/JsonData/CountriesList.json"))
                    {
                        string json = r.ReadToEnd();
                        SourceItems = JsonConvert.DeserializeObject<List<CountryInformation>>(json);
                        System.Web.HttpRuntime.Cache["countries"] = SourceItems;

                        System.Web.HttpRuntime.Cache.Insert("countries", SourceItems, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                        list = SourceItems;
                    }
                    if (SearchTerm != string.Empty)
                    {
                        SourceItems = SourceItems.Where(x => x.Value != Convert.ToInt32(SearchTerm)).ToList();

                    }
                }
                else
                {
                    List<CountryInformation> objectCountryList = objcountry as List<CountryInformation>;
                    SourceItems = objectCountryList;
                    if (SearchTerm != string.Empty)
                    {
                        list = SourceItems.Where(x => x.Value != Convert.ToInt32(SearchTerm)).ToList();

                    }
                }
            }


            return list;

        }

        public IEnumerable<CountryInformation> SearchCountriesFromCache(string SearchTerm, string CountryId)
        {
            List<CountryInformation> list = new List<CountryInformation>();
            if (SearchTerm != string.Empty)
            {
                var objcountry = System.Web.HttpRuntime.Cache["countries"];
                if (objcountry == null)
                {
                    string path = HttpRuntime.AppDomainAppPath;
                    using (StreamReader r = new StreamReader(path + "/JsonData/CountriesList.json"))
                    {
                        string json = r.ReadToEnd();
                        SourceItems = JsonConvert.DeserializeObject<List<CountryInformation>>(json);
                        System.Web.HttpRuntime.Cache["countries"] = SourceItems;

                        System.Web.HttpRuntime.Cache.Insert("countries", SourceItems, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                        list = SourceItems;
                    }

                }
                else
                {
                    SourceItems = objcountry as List<CountryInformation>; ;
                }
                
                if (SearchTerm != string.Empty)
                {
                    list = (from fr in SourceItems
                            where (fr.Text.ToLower().StartsWith(SearchTerm.Trim().ToLower()) || (fr.COUNTRYCODE.StartsWith(SearchTerm.Trim().ToUpper())))
                            orderby fr.Text ascending
                            select fr).ToList();
                    if (CountryId != "")
                    {
                        list = list.Where(x => x.Value != Convert.ToInt32(CountryId)).ToList();
                    }

                }

            }


            return list;

        }

        public IEnumerable<CurrencyInformation> GetAllCurrencyList()
        {
            List<CurrencyInformation> list = null;

            if (System.Web.HttpRuntime.Cache["currencies"] == null)
            {
                string path = HttpRuntime.AppDomainAppPath;
                using (StreamReader r = new StreamReader(path + "/JsonData/CurrenciesList.json"))
                {
                    string json = r.ReadToEnd();
                    CurrencyItems = JsonConvert.DeserializeObject<List<CurrencyInformation>>(json);
                    System.Web.HttpRuntime.Cache["currencies"] = CurrencyItems;

                    System.Web.HttpRuntime.Cache.Insert("currencies", CurrencyItems, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                    list = CurrencyItems;
                }

            }
            else {
                list = HttpRuntime.Cache["currencies"] as List<CurrencyInformation>;
            }
            return list;
        }
    }
}