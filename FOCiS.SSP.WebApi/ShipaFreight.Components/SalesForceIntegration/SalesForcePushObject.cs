﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.WebApi.InterfaceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FOCiS.SSP.WebApi.ShipaFreight.Components.SalesForceIntegration
{
    public class SalesForcePushObject:ISalesForceManager
    {
        public async Task<SalesForceEntity> PushDatatoMessageQueue(SalesForceEntity SalesForceEntity)
        {
            try
            {
                MQIntegrationReader PushDataToMessageQueue = new MQIntegrationReader();
                SalesForceEntity = await Task.Run(() => PushDataToMessageQueue.MQIntegrationUserCall(SalesForceEntity)).ConfigureAwait(false);
            }
            catch (Exception ex) { 
            
            }
            return SalesForceEntity;
        }


        public int GetQuotationDataById(long id, string type)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(id);
            return 0;
        }
    }
}