﻿using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.WebApi.Controllers;
using FOCiS.SSP.WebApi.Helpers;
using IBM.WMQ;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace FOCiS.SSP.WebApi.ShipaFreight.Components.SalesForceIntegration
{
    public class MQIntegrationReader : BaseController
    {
        public SalesForceEntity MQIntegrationUserCall(SalesForceEntity objSalesForceEntity)
        {
            try
            {
                string ConversionObject = MQIntegrationReader.Serialize(objSalesForceEntity);
                string HostNameOfDb = string.Empty;
                string DbNameForDb = string.Empty;
                string StoredProcedureName = string.Empty;
                string UsernameOfDB = string.Empty;
                string PasswordOfDB = string.Empty;
                string MessageOfQueue = string.Empty;
                string QueueLastError = string.Empty;
                string queueName = ShipaConfiguration.MQQueueName();
                string queueManagerName = ShipaConfiguration.MQQueueManagerName();
                string HOST_NAME = ShipaConfiguration.HostName();
                string CHANNEL_PROPERTY = ShipaConfiguration.ChannelProperty();
                MQQueueManager mqQMgr = null;
                MQQueue mqQueue = null;
                MQPutMessageOptions mqPutMsgOpts;
                Hashtable properties;
                int reconnectOption = 2;
                int msgLen;
                String message;
                try
                {
                    properties = new Hashtable();
                    properties.Add(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT);
                    properties.Add(MQC.HOST_NAME_PROPERTY, HOST_NAME);
                    properties.Add(MQC.CHANNEL_PROPERTY, CHANNEL_PROPERTY);

                    switch (reconnectOption)
                    {
                        case 0: properties.Add(MQC.CONNECT_OPTIONS_PROPERTY, MQC.MQCNO_RECONNECT_DISABLED); break;
                        case 1: properties.Add(MQC.CONNECT_OPTIONS_PROPERTY, MQC.MQCNO_RECONNECT); break;
                        case 2: properties.Add(MQC.CONNECT_OPTIONS_PROPERTY, MQC.MQCNO_RECONNECT_Q_MGR); break;
                        case 3: properties.Add(MQC.CONNECT_OPTIONS_PROPERTY, MQC.MQCNO_RECONNECT_AS_DEF); break;
                    }
                    int openOptions = MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_OUTPUT;
                    mqQMgr = new MQQueueManager(queueManagerName, properties);
                    mqQueue = mqQMgr.AccessQueue(queueName, openOptions);
                }
                catch (MQException mqe)
                {
                    // stop if failed
                    LogError("MQIntegrationReader", "MQIntegrationUserCall", mqe.Message.ToString(), mqe.StackTrace.ToString());
                    throw new ShipaApiException(mqe.Message.ToString());
                }

                bool isContinue = true;
                while (isContinue)
                {
                    message = ConversionObject;
                    msgLen = message.Length;

                    if (msgLen > 0)
                    {
                        MQMessage mqMsg;
                        MQGetMessageOptions mqGetMsgOpts;
                        mqMsg = new MQMessage();
                        mqGetMsgOpts = new MQGetMessageOptions();
                        mqGetMsgOpts.Options = MQC.MQGMO_FAIL_IF_QUIESCING | MQC.MQGMO_WAIT;
                        mqGetMsgOpts.WaitInterval = MQC.MQWI_UNLIMITED;
                        mqMsg.CharacterSet = 1208;
                        mqMsg.WriteString(message);
                        mqMsg.Format = MQC.MQFMT_STRING;
                        mqPutMsgOpts = new MQPutMessageOptions();
                        try
                        {
                            mqQueue.Put(mqMsg, mqPutMsgOpts);
                            isContinue = false;
                        }
                        catch (MQException mqe)
                        {
                            if (mqe.Reason == MQC.MQRC_NO_MSG_AVAILABLE)
                            {
                                LogError("MQIntegrationReader", "No Message available.", mqe.Message.ToString(), mqe.StackTrace.ToString());
                                //log.Error("3. No Message available. ", mqe);
                                isContinue = false;
                            }
                            else
                            {
                                if (mqe.Reason == MQC.MQRC_TRUNCATED_MSG_FAILED)
                                {
                                    LogError("MQIntegrationReader", "Truncated Message failed.", mqe.Message.ToString(), mqe.StackTrace.ToString());
                                    //log.Error("4. Truncated Message failed. ", mqe);
                                    isContinue = false;
                                }
                                else
                                {
                                    LogError("MQIntegrationReader", "Truncated Message failed.", mqe.Message.ToString(), mqe.StackTrace.ToString());
                                    //log.Error("5. - ", mqe);
                                }
                            }
                        }
                    }
                    else
                    {
                        isContinue = false;
                    }

                }
                try
                {
                    //Close the Queue
                    mqQueue.Close();
                    //Close the Queue Manager
                    mqQMgr.Disconnect();
                }
                catch (MQException mqe)
                {
                    LogError("MQIntegrationReader", "MQIntegrationUserCall", mqe.Message.ToString(), mqe.StackTrace.ToString());
                }
            }
            catch (Exception ex)
            {

                LogError("MQIntegrationReader", "MQIntegrationUserCall", ex.Message.ToString(), ex.StackTrace.ToString());
            }

            return objSalesForceEntity;
        }

        public static string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch (Exception ex)
            {
                throw new ShipaApiException(ex.Message.ToString());
            }
        }

    }
}