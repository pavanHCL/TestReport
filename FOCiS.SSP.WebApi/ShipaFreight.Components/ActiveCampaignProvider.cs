﻿//using ActiveCampaign.Responses;
//using ActiveCampaign.Struct;
//using FOCiS.SSP.DBFactory;
//using FOCiS.SSP.DBFactory.Entities;
//using FOCiS.SSP.DBFactory.Factory;
//using FOCiS.SSP.WebApi.Helpers;
//using FOCIS.SSP.ActiveCampaign;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Web;
//using System.Web.Http;

//namespace FOCiS.SSP.WebApi.ShipaFreight.Components
//{
//    public class ActiveCampaignBusiness
//    {
//        private string ActiveCampaignApiUrl = ShipaConfiguration.GetActiveCampaignApiUrl();
//        private string ActiveCampaignKey = ShipaConfiguration.GetActiveCampaignApiKey();
//        private string ApiUrl = ShipaConfiguration.ShipaApiUrl();

//        public async Task<UserEntity> ShipaFreightSignUpData(UserEntity UserObject)
//        {
//            try
//            {
//                string ListId;
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                ActiveCampaignContactEntity objUserEntity = new ActiveCampaignContactEntity();
//                UserDBFactory userDBFactory = new UserDBFactory();
//                var ActiveUserObject = userDBFactory.GetUserDetails(UserObject.USERID);
//                if (ActiveUserObject.ACCOUNTSTATUS == 0 && UserObject.NOTIFICATIONSUBSCRIPTION == false)
//                {
//                    var listTypeObj = activeCampaign.GetListType(ShipaConfiguration.SFUnSubScriberUser());
//                    if (listTypeObj.ListType != ShipaConfiguration.SFUnSubScriberUser())
//                    {
//                        int AccountStatus = 2;
//                        var objinsertList = InsertListIntoActiveCampaign(AccountStatus);
//                        UserObject.ListId = objinsertList.Id;
//                        ListId = objinsertList.Id.ToString();
//                    }
//                    else
//                    {
//                        UserObject.ListId = listTypeObj.ListId;
//                        ListId = listTypeObj.ListId.ToString();
//                    }
//                    UserObject.SubscriptionStatus = 2;
//                    var objInsertGuest = await Task.Run(() => InsertSignUpUsersIntoActiveCampaign(UserObject)).ConfigureAwait(false);
//                }
//                if (ActiveUserObject.ACCOUNTSTATUS == 1 && UserObject.NOTIFICATIONSUBSCRIPTION == true)
//                {
//                    objUserEntity = await Task.Run(() => activeCampaign.GetActiveContactsByEmailId(UserObject.USERID)).ConfigureAwait(false);
//                    var listTypeObj = activeCampaign.GetListType(ShipaConfiguration.SFSubScriberUser());
//                    if (listTypeObj.ListType != ShipaConfiguration.SFSubScriberUser())
//                    {
//                        int AccountStatus = 3;
//                        var objinsertList = InsertListIntoActiveCampaign(AccountStatus);
//                        UserObject.ListId = objinsertList.Id;
//                        ListId = objinsertList.Id.ToString();
//                    }
//                    else
//                    {
//                        UserObject.ListId = listTypeObj.ListId;
//                        ListId = listTypeObj.ListId.ToString();
//                    }
//                    UserObject.SubscriptionStatus = 1;
//                    if (objUserEntity.SubscriberId != 0)
//                    {
//                        //await Task.Run(() => DeleteActiveContact(objUserEntity));
//                        UserObject.SubscriberId = Convert.ToInt32(objUserEntity.SubscriberId);
//                        var objInsertGuest = await Task.Run(() => UpdateSignUpUsersIntoActiveCampaign(UserObject)).ConfigureAwait(false);
//                    }
//                    else
//                    {
//                        var objInsertGuest = await Task.Run(() => InsertSignUpUsersIntoActiveCampaign(UserObject)).ConfigureAwait(false);
//                    }

//                }
//                if (ActiveUserObject.ACCOUNTSTATUS == 1 && UserObject.NOTIFICATIONSUBSCRIPTION == false)
//                {
//                    objUserEntity = await Task.Run(() => activeCampaign.GetActiveContactsByEmailId(UserObject.USERID)).ConfigureAwait(false);
//                    var listTypeObj = activeCampaign.GetListType(ShipaConfiguration.SFUnSubScriberUser());
//                    if (listTypeObj.ListType != ShipaConfiguration.SFUnSubScriberUser())
//                    {
//                        int AccountStatus = 2;
//                        var objinsertList = InsertListIntoActiveCampaign(AccountStatus);
//                        UserObject.ListId = objinsertList.Id;
//                        ListId = objinsertList.Id.ToString();
//                    }
//                    else
//                    {
//                        UserObject.ListId = listTypeObj.ListId;
//                        ListId = listTypeObj.ListId.ToString();
//                    }
//                    UserObject.SubscriptionStatus = 2;
//                    if (objUserEntity.SubscriberId != 0)
//                    {
//                        //await Task.Run(() => DeleteActiveContact(objUserEntity));
//                        UserObject.SubscriberId = Convert.ToInt32(objUserEntity.SubscriberId);
//                        var objInsertGuest = await Task.Run(() => UpdateSignUpUsersIntoActiveCampaign(UserObject)).ConfigureAwait(false);
//                    }
//                    else
//                    {
//                        var objInsertGuest = await Task.Run(() => InsertSignUpUsersIntoActiveCampaign(UserObject)).ConfigureAwait(false);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }

//            return UserObject;
//        }

//        public async Task<SubscriberInsertResponse> InsertQuoteDataIntoActiveCampaign(List<ActCampaignStatusEntity> ActCampaignObject, UserEntity Usermodel, string UserType)
//        {
//            SubscriberInsertResponse InsertResponse = new SubscriberInsertResponse();
//            try
//            {
//                for (int i = 0; i < ActCampaignObject.Count; i++)
//                {
//                    FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                    InsertResponse = await Task.Run(() => activeCampaign.Api.contact.Add(new Contact()
//                    {                        
//                        email = ActCampaignObject[i].CreatedBy,
//                        firstName = ActCampaignObject[i].FirstName,
//                        lastName = ActCampaignObject[i].LastName,
//                        orgName = ActCampaignObject[i].CompanyName,
//                        phone = ActCampaignObject[i].MobileNumber,
//                        quotationNumber = ActCampaignObject[i].QuotationNumber,
//                        quoteRequestDate = ActCampaignObject[i].QuoteRequestDate,
//                        quoteGivenByGSSCDate = ActCampaignObject[i].QuoteGivenByGSSCDate,
//                        operationalJobNumber = ActCampaignObject[i].OperationalJobNumber,
//                        dCountryName = ActCampaignObject[i].DCountryName,
//                        oCountryName = ActCampaignObject[i].OCountryName,
//                        productName = ActCampaignObject[i].ProductName,
//                        movementTypeName = ActCampaignObject[i].MovementTypeName,
//                        customerStatus = ActCampaignObject[i].CustomerStatus,
//                        stateId = ActCampaignObject[i].StateId,
//                        currency = ActCampaignObject[i].Currency,
//                        quoteGrandTotal = ActCampaignObject[i].QuoteGrandTotal,
//                        jobGrandTotal = ActCampaignObject[i].JobGrandTotal,
//                        list = Usermodel.ListId.ToString(),
//                    })).ConfigureAwait(false);
//                    int status = SaveContactData(InsertResponse, ActCampaignObject[i].CreatedBy, UserType, "Insert");
//                }
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return InsertResponse;
//        }

//        public async Task<SubscriberUpdatePost> EditContactsIntoActiveCampaign(ActCampaignEditEntity ActCampaignObject, int SubscriberId, string ListId, string UserType)
//        {
//            SubscriberUpdatePost InsertResponse = new SubscriberUpdatePost();
//            try
//            {
//                FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                InsertResponse = await Task.Run(() => activeCampaign.Api.contact.Edit(new Contact()
//                {

//                    id = SubscriberId,
//                    email = ActCampaignObject.CreatedBy,
//                    firstName = ActCampaignObject.FirstName,
//                    lastName = ActCampaignObject.LastName,
//                    orgName = ActCampaignObject.CompanyName,
//                    phone = ActCampaignObject.MobileNumber,
//                    quotationNumber = ActCampaignObject.QuotationNumber,
//                    quoteRequestDate = ActCampaignObject.QuoteRequestDate,
//                    quoteGivenByGSSCDate = ActCampaignObject.QuoteGivenByGSSCDate,
//                    operationalJobNumber = ActCampaignObject.OperationalJobNumber,
//                    dCountryName = ActCampaignObject.DCountryName,
//                    oCountryName = ActCampaignObject.OCountryName,
//                    productName = ActCampaignObject.ProductName,
//                    movementTypeName = ActCampaignObject.MovementTypeName,
//                    customerStatus = ActCampaignObject.CustomerStatus,
//                    stateId = ActCampaignObject.StateId,
//                    currency = ActCampaignObject.Currency,
//                    quoteGrandTotal = ActCampaignObject.QuoteGrandTotal,
//                    jobGrandTotal = ActCampaignObject.JobGrandTotal,
//                    list = ListId.ToString()

//                })).ConfigureAwait(false);
//                int status = UpdateContactData(InsertResponse, ActCampaignObject.CreatedBy, UserType, "Update");
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return InsertResponse;
//        }

//        public async Task<int> DeleteActiveContact(ActiveCampaignContactEntity UserObject)
//        {
//            int status = 0;
//            try
//            {
//                FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                await Task.Run(() => activeCampaign.Api.contact.Delete(Convert.ToInt32(UserObject.SubscriberId))).ConfigureAwait(false);
//                DeleteActiveContact(Convert.ToInt32(UserObject.SubscriberId));
//                status = 1;
//            }
//            catch (Exception ex)
//            {
//                status = 0;
//                throw ex.InnerException;
//            }

//            return status;
//        }

//        public async Task<int> DeleteListInActiveContact()
//        {
//            int status = 0;
//            ActiveCampaignDBFactory ActCampaignObj = new ActiveCampaignDBFactory();
//            ListDeleteMulti listDeleteResponse = new ListDeleteMulti();
//            try
//            {
//                var guestUsersObj = ActCampaignObj.GetListType(ShipaConfiguration.ListGuestUser());
//                var registerUsersObj = ActCampaignObj.GetListType(ShipaConfiguration.ListRegisteredUser());
//                int GuestId = guestUsersObj.ListId;
//                int RegisterId = registerUsersObj.ListId;
//                FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                listDeleteResponse = await Task.Run(() => activeCampaign.Api.list.Delete(GuestId, RegisterId)).ConfigureAwait(false);
//                ActCampaignObj.DeleteActiveList(GuestId);
//                ActCampaignObj.DeleteActiveList(RegisterId);
//                status = 1;
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return status;
//        }

//        public ListInsertResult InsertListIntoActiveCampaign(int AccountStatus)
//        {
//            string listName = string.Empty;
//            ListInsertResult addUserList = new ListInsertResult();
//            try
//            {
//                if (AccountStatus == 0)
//                {
//                    listName = ShipaConfiguration.ListGuestUser();
//                }
//                if (AccountStatus == 1)
//                {
//                    listName = ShipaConfiguration.ListRegisteredUser();
//                }
//                if (AccountStatus == 2)
//                {
//                    listName = ShipaConfiguration.SFUnSubScriberUser();
//                }
//                if (AccountStatus == 3)
//                {
//                    listName = ShipaConfiguration.SFSubScriberUser();
//                }
//                string userAddress = ShipaConfiguration.ListUserAddress();
//                string userCountry = ShipaConfiguration.UserCountry();
//                string senderZipCode = ShipaConfiguration.SenderZipCode();
//                string senderUrl = ShipaConfiguration.SenderUrl();
//                string senderName = ShipaConfiguration.SenderName();
//                string senderCity = ShipaConfiguration.SenderCity();
//                string senderRemainder = ShipaConfiguration.SenderReminder();

//                FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                addUserList = activeCampaign.Api.list.Add(new ListAddOptions()
//                {
//                    Name = listName,
//                    sender_name = senderName,
//                    sender_addr1 = userAddress,
//                    sender_country = userCountry,
//                    sender_zip = senderUrl,
//                    sender_city = senderCity,
//                    sender_url = senderUrl,
//                    sender_reminder = senderRemainder,
//                });
//                int status = SaveListADD(addUserList, listName);

//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return addUserList;
//        }

//        public async Task<SubscriberInsertResponse> InsertSignUpUsersIntoActiveCampaign(UserEntity Usermodel)
//        {
//            SubscriberInsertResponse InsertResponse = new SubscriberInsertResponse();
//            try
//            {
//                FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                InsertResponse = await Task.Run(() => activeCampaign.Api.contact.Add(new Contact()
//                {
//                    email = Usermodel.USERID,
//                    firstName = Usermodel.FIRSTNAME,
//                    lastName = Usermodel.LASTNAME,
//                    orgName = Usermodel.COMPANYNAME,
//                    phone = Usermodel.MOBILENUMBER,
//                    status = Usermodel.SubscriptionStatus,
//                    notificationSubscription = Usermodel.NOTIFICATIONSUBSCRIPTION.ToString(),
//                    list = Usermodel.ListId.ToString(),
//                })).ConfigureAwait(false);
//                int status = SaveContactData(InsertResponse, Usermodel.USERID, "SignUpUser", "Insert");
//            }

//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return InsertResponse;

//        }

//        public async Task<SubscriberUpdatePost> UpdateSignUpUsersIntoActiveCampaign(UserEntity Usermodel)
//        {
//            SubscriberUpdatePost InsertResponse = new SubscriberUpdatePost();
//            try
//            {
//                FOCIS.SSP.ActiveCampaign.ActiveCampaign activeCampaign = new FOCIS.SSP.ActiveCampaign.ActiveCampaign(new Api(ActiveCampaignApiUrl, ActiveCampaignKey, true));
//                InsertResponse = await Task.Run(() => activeCampaign.Api.contact.Edit(new Contact()
//                {
//                    id = Usermodel.SubscriberId,
//                    email = Usermodel.USERID,
//                    firstName = Usermodel.FIRSTNAME,
//                    lastName = Usermodel.LASTNAME,
//                    orgName = Usermodel.COMPANYNAME,
//                    phone = Usermodel.MOBILENUMBER,
//                    status = Usermodel.SubscriptionStatus,
//                    notificationSubscription = Usermodel.NOTIFICATIONSUBSCRIPTION.ToString(),
//                    list = Usermodel.ListId.ToString(),
//                })).ConfigureAwait(false);
//                int status = UpdateContactData(InsertResponse, Usermodel.USERID, "SignUpUser", "Update");
//            }

//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return InsertResponse;

//        }

//        public async Task<List<ActCampaignStatusEntity>> GuestUsersData()
//        {
//            string UserType = "GuestUser";
//            UserEntity objdata = new UserEntity();
//            string ListId;
//            List<ActCampaignStatusEntity> objActCampaignEntity = new List<ActCampaignStatusEntity>();
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                List<ActiveCampaignContactEntity> objContactEntity = new List<ActiveCampaignContactEntity>();
//                List<ActiveCampaignContactEntity> objDeleteEntity = new List<ActiveCampaignContactEntity>();
//                ActiveCampaignContactEntity objUserEntity = new ActiveCampaignContactEntity();
//                ActCampaignEditEntity objEditEntity = new ActCampaignEditEntity();
//                objContactEntity = activeCampaign.GetUsersData("RegisterUser");
//                var GuestUsersObjCheck = activeCampaign.GetUsersData("GuestUser");
//                var listTypeObj = activeCampaign.GetListType(ShipaConfiguration.ListGuestUser());
//                if (listTypeObj.ListType != ShipaConfiguration.ListGuestUser())
//                {
//                    int AccountStatus = 0;
//                    var objinsertList = InsertListIntoActiveCampaign(AccountStatus);
//                    objdata.ListId = objinsertList.Id;
//                    ListId = objinsertList.Id.ToString();
//                }
//                else
//                {
//                    objdata.ListId = listTypeObj.ListId;
//                    ListId = listTypeObj.ListId.ToString();
//                }
//                if (GuestUsersObjCheck.Count > 0)
//                {
//                    objActCampaignEntity = activeCampaign.GetGuestUsersStatusforActCampaign("guest");
//                    for (int i = 0; i < objActCampaignEntity.Count; i++)
//                    {
//                        objUserEntity = await Task.Run(() => activeCampaign.GetActiveContactsByEmailId(objActCampaignEntity[i].EmailId)).ConfigureAwait(false);
//                        if (objUserEntity != null)
//                        {
//                            objEditEntity.QuotationNumber = objActCampaignEntity[i].QuotationNumber;
//                            objEditEntity.FirstName = objActCampaignEntity[i].FirstName;
//                            objEditEntity.CompanyName = objActCampaignEntity[i].CompanyName;
//                            objEditEntity.EmailId = objActCampaignEntity[i].EmailId;
//                            objEditEntity.QuoteRequestDate = objActCampaignEntity[i].QuoteRequestDate;
//                            objEditEntity.QuoteGivenByGSSCDate = objActCampaignEntity[i].QuoteGivenByGSSCDate;
//                            objEditEntity.OperationalJobNumber = objActCampaignEntity[i].OperationalJobNumber;
//                            objEditEntity.OCountryName = objActCampaignEntity[i].OCountryName;
//                            objEditEntity.DCountryName = objActCampaignEntity[i].DCountryName;
//                            objEditEntity.ProductName = objActCampaignEntity[i].ProductName;
//                            objEditEntity.MovementTypeName = objActCampaignEntity[i].MovementTypeName;
//                            objEditEntity.OriginPortName = objActCampaignEntity[i].OriginPortName;
//                            objEditEntity.DestinationPortName = objActCampaignEntity[i].DestinationPortName;
//                            objEditEntity.CreatedBy = objActCampaignEntity[i].CreatedBy;
//                            objEditEntity.CustomerStatus = objActCampaignEntity[i].CustomerStatus;
//                            objEditEntity.StateId = objActCampaignEntity[i].StateId;
//                            objEditEntity.Currency = objActCampaignEntity[i].Currency;
//                            objEditEntity.QuoteGrandTotal = objActCampaignEntity[i].QuoteGrandTotal;
//                            objEditEntity.JobGrandTotal = objActCampaignEntity[i].JobGrandTotal;
//                            var objInsertGuest = await Task.Run(() => EditContactsIntoActiveCampaign(objEditEntity, Convert.ToInt32(objUserEntity.SubscriberId), ListId, UserType)).ConfigureAwait(false);
//                        }
//                        else
//                        {
//                            var objInsertGuest = await Task.Run(() => InsertQuoteDataIntoActiveCampaign(objActCampaignEntity, objdata, UserType)).ConfigureAwait(false);
//                        }
//                    }

//                }
//                else
//                {
//                    objActCampaignEntity = activeCampaign.GetGuestUsersStatusforActCampaign("guest");
//                    var objCheck = objActCampaignEntity.Where(b => objContactEntity.Any(a => a.ContactEmailId == b.EmailId)).ToList();
//                    if (objCheck.Any())
//                    {
//                        for (int i = 0; i < objCheck.Count; i++)
//                        {
//                            ActiveCampaignContactEntity mEntity = new ActiveCampaignContactEntity();
//                            mEntity = activeCampaign.GetContactEmailId(objCheck[i].EmailId, "GuestUser");
//                            objDeleteEntity.Add(mEntity);
//                        }
//                    }
//                    if (objDeleteEntity.Count > 0)
//                    {
//                        //await Task.Run(() => DeleteActiveContact(objDeleteEntity));
//                    }
//                    var objInsertGuest = await Task.Run(() => InsertQuoteDataIntoActiveCampaign(objActCampaignEntity, objdata, UserType)).ConfigureAwait(false);
//                }

//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return objActCampaignEntity;
//        }

//        public async Task<List<ActCampaignStatusEntity>> RegisterUsersData()
//        {
//            string UserType = "RegisterUser";
//            string ListId;
//            List<ActCampaignStatusEntity> objActCampaignEntity = new List<ActCampaignStatusEntity>();
//            UserEntity objdata = new UserEntity();
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                ActiveCampaignContactEntity objUserEntity = new ActiveCampaignContactEntity();
//                List<ActiveCampaignContactEntity> objContactEntity = new List<ActiveCampaignContactEntity>();
//                ActCampaignEditEntity objEditEntity = new ActCampaignEditEntity();
//                objContactEntity = activeCampaign.GetUsersData("RegisterUser");
//                var GuestUsersObjCheck = activeCampaign.GetUsersData("GuestUser");
//                var listTypeObj = activeCampaign.GetListType(ShipaConfiguration.ListRegisteredUser());
//                if (listTypeObj.ListType != ShipaConfiguration.ListRegisteredUser())
//                {
//                    int AccountStatus = 1;
//                    var objinsertList = InsertListIntoActiveCampaign(AccountStatus);
//                    objdata.ListId = objinsertList.Id;
//                    ListId = objinsertList.Id.ToString();
//                }
//                else
//                {
//                    objdata.ListId = listTypeObj.ListId;
//                    ListId = listTypeObj.ListId.ToString();
//                }
//                if (GuestUsersObjCheck.Count > 0)
//                {
//                    objActCampaignEntity = activeCampaign.GetRegisterUsersStatusforActCampaign();
//                    for (int i = 0; i < objActCampaignEntity.Count; i++)
//                    {
//                        objUserEntity = await Task.Run(() => activeCampaign.GetActiveContactsByEmailId(objActCampaignEntity[i].EmailId)).ConfigureAwait(false);
//                        if (objUserEntity.ContactEmailId != null)
//                        {
//                            objEditEntity.QuotationNumber = objActCampaignEntity[i].QuotationNumber;
//                            objEditEntity.FirstName = objActCampaignEntity[i].FirstName;
//                            objEditEntity.CompanyName = objActCampaignEntity[i].CompanyName;
//                            objEditEntity.EmailId = objActCampaignEntity[i].EmailId;
//                            objEditEntity.QuoteRequestDate = objActCampaignEntity[i].QuoteRequestDate;
//                            objEditEntity.QuoteGivenByGSSCDate = objActCampaignEntity[i].QuoteGivenByGSSCDate;
//                            objEditEntity.OperationalJobNumber = objActCampaignEntity[i].OperationalJobNumber;
//                            objEditEntity.OCountryName = objActCampaignEntity[i].OCountryName;
//                            objEditEntity.DCountryName = objActCampaignEntity[i].DCountryName;
//                            objEditEntity.ProductName = objActCampaignEntity[i].ProductName;
//                            objEditEntity.MovementTypeName = objActCampaignEntity[i].MovementTypeName;
//                            objEditEntity.OriginPortName = objActCampaignEntity[i].OriginPortName;
//                            objEditEntity.DestinationPortName = objActCampaignEntity[i].DestinationPortName;
//                            objEditEntity.CreatedBy = objActCampaignEntity[i].CreatedBy;
//                            objEditEntity.CustomerStatus = objActCampaignEntity[i].CustomerStatus;
//                            objEditEntity.StateId = objActCampaignEntity[i].StateId;
//                            objEditEntity.Currency = objActCampaignEntity[i].Currency;
//                            objEditEntity.QuoteGrandTotal = objActCampaignEntity[i].QuoteGrandTotal;
//                            objEditEntity.JobGrandTotal = objActCampaignEntity[i].JobGrandTotal;
//                            var objInsertGuest = await Task.Run(() => EditContactsIntoActiveCampaign(objEditEntity, Convert.ToInt32(objUserEntity.SubscriberId), ListId, UserType)).ConfigureAwait(false);
//                        }
//                        else
//                        {
//                            var objInsertGuest = await Task.Run(() => InsertQuoteDataIntoActiveCampaign(objActCampaignEntity, objdata, UserType)).ConfigureAwait(false);
//                        }
//                    }

//                }

//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return objActCampaignEntity;
//        }

//        public int SaveListADD(ListInsertResult AddListData, string listName)
//        {
//            int status = 0;
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                ActiveCampaignListEntity activeListEntity = new ActiveCampaignListEntity();
//                activeListEntity.ListId = AddListData.Id;
//                activeListEntity.ListType = listName;
//                activeListEntity.ResultCode = AddListData.ResultCode;
//                activeListEntity.ResultMessage = AddListData.ResultMessage;
//                activeListEntity.ResultOutPut = AddListData.ResultOutput;
//                status = activeCampaign.SaveList(activeListEntity);
//                status = 1;
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return status;
//        }

//        public int UpdateContactData(SubscriberUpdatePost Subscriber, string ContactEmailId, string UserType, string QueryType)
//        {
//            int status = 0;
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                ActiveCampaignContactEntity activeContactEntity = new ActiveCampaignContactEntity();
//                activeContactEntity.SubscriberId = Subscriber.SubscriberId;
//                activeContactEntity.SendLastdid = Subscriber.SendlastDid;
//                activeContactEntity.ResultCode = Subscriber.ResultCode;
//                activeContactEntity.ResultMessage = Subscriber.ResultMessage;
//                activeContactEntity.ResultOutPut = Subscriber.ResultOutput;
//                activeContactEntity.ContactEmailId = ContactEmailId;
//                activeContactEntity.UserType = UserType;
//                status = activeCampaign.SaveContactInList(activeContactEntity, QueryType);
//                status = 1;
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return status;
//        }

//        public int SaveContactData(SubscriberInsertResponse Subscriber, string ContactEmailId, string UserType, string QueryType)
//        {
//            int status = 0;
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                ActiveCampaignContactEntity activeContactEntity = new ActiveCampaignContactEntity();
//                activeContactEntity.SubscriberId = Subscriber.SubscriberId;
//                activeContactEntity.SendLastdid = Subscriber.SendlastDid;
//                activeContactEntity.ResultCode = Subscriber.ResultCode;
//                activeContactEntity.ResultMessage = Subscriber.ResultMessage;
//                activeContactEntity.ResultOutPut = Subscriber.ResultOutput;
//                activeContactEntity.ContactEmailId = ContactEmailId;
//                activeContactEntity.UserType = UserType;
//                status = activeCampaign.SaveContactInList(activeContactEntity, QueryType);
//                status = 1;
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return status;
//        }

//        public int DeleteActiveContact(int SubscribeId)
//        {
//            int ContactId = 0;
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                activeCampaign.DeleteActiveContact(SubscribeId);
//                ContactId = 1;
//            }
//            catch (Exception ex)
//            {
//                ContactId = 0;
//                throw ex.InnerException;

//            }

//            return ContactId;
//        }

//        public int SaveWebHookResponse(string Response)
//        {
//            int status = 0;
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                status = activeCampaign.InsertWebHookResponse(Response);
//                status = 1;
//            }
//            catch (Exception ex)
//            {
//                throw ex.InnerException;
//            }
//            return status;
//        }
//    }
//}