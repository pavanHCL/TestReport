﻿using Jose;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace FOCiS.SSP.WebApi.Helpers
{
    public static class HsbcEncryption
    {
        public static String signAndEncrypt(string payload)
        {
            try
            {
                //Signed by merchant private key.
                string merchantKeyID = ConfigurationManager.AppSettings["merchantKeyID"].ToString();
                string merchantKeyPath = ConfigurationManager.AppSettings["merchant_" + merchantKeyID].ToString();
                string merchantKeyPass = ConfigurationManager.AppSettings["merchantKeyPass"].ToString();
                var privateKey = new X509Certificate2(merchantKeyPath, merchantKeyPass, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet).PrivateKey as RSACryptoServiceProvider;

                //re-import RSAParameters to fix "Invalid algorithm specified" exception
                RSACryptoServiceProvider newKey = new RSACryptoServiceProvider();
                newKey.ImportParameters(privateKey.ExportParameters(true));

                var jwsHeader = new Dictionary<string, object>()
                    {
                        { "kid", merchantKeyID}
                    };
                string signToken = Jose.JWT.Encode(payload, newKey, JwsAlgorithm.RS256, extraHeaders: jwsHeader);

                //Encrypt by hsbc public key.
                string hsbcKeyID = ConfigurationManager.AppSettings["hsbcKeyID"].ToString();
                string hsbcKeyPath = ConfigurationManager.AppSettings["hsbc_" + hsbcKeyID].ToString();
                var publicKey = new X509Certificate2(hsbcKeyPath).PublicKey.Key as RSACryptoServiceProvider;

                var jweHeader = new Dictionary<string, object>()
                    {
                        { "kid", hsbcKeyID}
                    };
                string encToken = Jose.JWT.Encode(signToken, publicKey, JweAlgorithm.RSA_OAEP_256, JweEncryption.A128GCM, extraHeaders: jweHeader);

                return encToken;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string decryptAndVerify(string token)
        {
            //Decrypt by merchant private key
            var jweHeaders = JWT.Headers(token);
            string merchantKeyID = (string)jweHeaders["kid"];
            string merchantKeyPath = ConfigurationManager.AppSettings["merchant_" + merchantKeyID].ToString();
            string merchantKeyPass = ConfigurationManager.AppSettings["merchantKeyPass"].ToString();
            var privateKey = new X509Certificate2(merchantKeyPath, merchantKeyPass, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet).PrivateKey as RSACryptoServiceProvider;
            string decryptToken = JWT.Decode(token, privateKey);

            //Verify signature by hsbc public key
            var jwsHeaders = JWT.Headers(decryptToken);
            string hsbcKeyID = (string)jwsHeaders["kid"];
            string hsbcKeyPath = ConfigurationManager.AppSettings["hsbc_" + hsbcKeyID].ToString();
            var publicKey = new X509Certificate2(hsbcKeyPath).PublicKey.Key as RSACryptoServiceProvider;

            string payload = JWT.Decode(decryptToken, publicKey);

            return payload;
        }
    }
}