﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Models.UserManagement;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace FOCiS.SSP.Web.UI.Controllers
{
    public class APIDynamicClass
    {
        private static CultureInfo culture = (CultureInfo)CultureInfo.CreateSpecificCulture("en-US");
        private static string format = "{0:N2}";
        private static Decimal mQuoteChargesTotal = 0;
        private static string PreferredCurrencyId = string.Empty;
        private static QuotationPreviewModel DUIModels;
        private static UserProfileEntity DUserModel;
        private static int PDFpage = 0;
        private static byte[] bytes;
        private static TrackingDBFactory tbf = new TrackingDBFactory();

        #region Quote PDF
        public static byte[] GeneratePDF(MemoryStream memoryStream, QuotationPreviewModel mUIModels)
        {
            PDFpage = 0;
            DUIModels = mUIModels;
            PreferredCurrencyId = mUIModels.PreferredCurrencyId;
            UserDBFactory mFactory = new UserDBFactory();
            DUserModel = mFactory.GetUserProfileDetails(mUIModels.CreatedBy, 1);
            mQuoteChargesTotal = 0;
            Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();
            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = (mUIModels.ProductId == 2) ? "Ocean Freight Quote" : "Air Freight Quote";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);
            doc.Add(mFormattingpdf.LineSeparator());
            doc.Add(QuoteInfoTableModel(mUIModels));
            doc.Add(ShipInfoTableModel(mUIModels));
            PdfPTable mitemdescriptiontable = new PdfPTable(1);
            mitemdescriptiontable.WidthPercentage = 100;
            mitemdescriptiontable.SpacingBefore = 10;
            mitemdescriptiontable.DefaultCell.Border = 0;
            mitemdescriptiontable.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Item Description", Element.ALIGN_LEFT, false)));

            PdfPTable mItemDescription = new PdfPTable(1);
            mItemDescription.SetWidths(new float[] { 100 });
            mItemDescription.WidthPercentage = 100;
            mItemDescription.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mUIModels.ShipmentDescription == null ? "" : mUIModels.ShipmentDescription, Element.ALIGN_LEFT)));

            PdfPCell mRoutecell = new PdfPCell(mItemDescription);
            mRoutecell.Colspan = 2;
            mRoutecell.BorderColor = mFormattingpdf.TableBorderColor;
            mitemdescriptiontable.AddCell(mRoutecell);
            doc.Add(mitemdescriptiontable);

            if (mUIModels.QuotationCharges.Count() > 0)
            {
                var mOrigincharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mDestinationcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mINTcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mAddcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                if (mOrigincharges.Count() > 0)
                    doc.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mUIModels.PreferredCurrencyId));

                if (mINTcharges.Count() > 0)
                    doc.Add(ChargesTableModel(mINTcharges, "International Charges", mUIModels.PreferredCurrencyId));

                if (mDestinationcharges.Count() > 0)
                    doc.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mUIModels.PreferredCurrencyId));

                if (mAddcharges.Count() > 0)
                    doc.Add(ChargesTableModel(mAddcharges, "Additional Charges", mUIModels.PreferredCurrencyId));
            }
            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotalBorder.WidthPercentage = 100;
            mQuoteChargeTotalBorder.SpacingBefore = 10;
            mQuoteChargeTotalBorder.DefaultCell.Border = 0;
            mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

            PdfPTable mQuoteChargeTotal = new PdfPTable(2);
            mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotal.WidthPercentage = 100;
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PreferredCurrencyId + " " + Double.Parse(mQuoteChargesTotal.ToString()), Element.ALIGN_RIGHT)));

            PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
            mQuoteChargeTotalBorder.AddCell(mTotalcells);
            doc.Add(mQuoteChargeTotalBorder);
            doc.NewPage();
            mHeadingtable.AddCell(quotationheading);
            doc.Add(TermsandConditionsTableModel(mUIModels));

            doc.Close();

            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        #region PDF Methods
        #region PDF Formatting

        /// <summary>
        /// Summary description for Formatting
        /// </summary>
        public class Formattingpdf
        {
            public Font Heading4 { get; private set; }
            public Font Heading5 { get; private set; }
            public Font Heading6 { get; private set; }
            public Font Heading7 { get; private set; }
            public Font TableCaption7 { get; private set; }
            public Font Heading8 { get; private set; }
            public Font Heading9 { get; private set; }
            public Font Normal { get; private set; }
            public Font NormalRedColor { get; private set; }
            public Font Normal6 { get; private set; }
            public Font NormalItalic6 { get; private set; }
            public Font NormalItalic7 { get; private set; }
            public Font NormalBold { get; private set; }
            public Font BoldForQuoteTotal { get; private set; }

            public BaseColor TableHeadingBgColor { get; private set; }
            public BaseColor TableFooterBgColor { get; private set; }
            public BaseColor TableBorderColor { get; private set; }
            public BaseFont chinesebasefont { get; private set; }
            public BaseFont koreanbasefont { get; private set; }

            public FontSelector selectorTableCaption { get; private set; }
            public FontSelector selectorTableHeading { get; private set; }
            public FontSelector selectorTableContent { get; private set; }

            public Font mChineseFontTabCaption { get; private set; }
            public Font mChineseFontTabContent { get; private set; }
            public Font mChineseFontTabContentItalic { get; private set; }
            public Font mKoreanFontTabCaption { get; private set; }
            public Font mKoreanFontTabContent { get; private set; }
            public Font mKoreanFontTabContentItalic { get; private set; }

            public BaseColor TableCaptionFontColour { get; private set; }
            public BaseColor FontColour { get; private set; }

            public string mQuotePdfOutputLangId { get; private set; }

            public Formattingpdf(string QuoteOutPutLanguage = "")
            {
                TableCaptionFontColour = new BaseColor(175, 40, 46);

                TableCaption7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, TableCaptionFontColour);
                Heading4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, Font.NORMAL, BaseColor.BLACK);
                Heading5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, Font.NORMAL, BaseColor.BLACK);
                Heading7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                Heading6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, Font.NORMAL, BaseColor.BLACK);
                Heading8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, Font.NORMAL, BaseColor.BLACK);
                Heading9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.NORMAL, BaseColor.BLACK);
                Normal = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
                NormalRedColor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.RED);
                Normal6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
                NormalItalic6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
                NormalItalic7 = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.ITALIC, BaseColor.BLACK);
                NormalBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                FontColour = new BaseColor(176, 41, 46);
                BoldForQuoteTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, Font.NORMAL, FontColour);
                TableHeadingBgColor = new BaseColor(215, 215, 215);
                TableFooterBgColor = new BaseColor(253, 233, 217);
                TableBorderColor = BaseColor.GRAY;
                mQuotePdfOutputLangId = QuoteOutPutLanguage;

                //For Chinese characters display
                mChineseFontTabCaption = new Font(chinesebasefont, 7, Font.BOLD, TableCaptionFontColour);
                mChineseFontTabContent = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mChineseFontTabContentItalic = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);

                //For Korean characters display
                mKoreanFontTabCaption = new Font(koreanbasefont, 7, Font.BOLD, TableCaptionFontColour);
                mKoreanFontTabContent = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mKoreanFontTabContentItalic = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);

                selectorTableCaption = new FontSelector();
                selectorTableCaption.AddFont(TableCaption7);
                selectorTableCaption.AddFont(mChineseFontTabCaption);
                selectorTableCaption.AddFont(mKoreanFontTabCaption);

                selectorTableContent = new FontSelector();
                selectorTableContent.AddFont(NormalItalic7);
                selectorTableContent.AddFont(mChineseFontTabContentItalic);
                selectorTableContent.AddFont(mKoreanFontTabContentItalic);

                selectorTableHeading = new FontSelector();
            }

            /// <summary>
            /// Document Heading
            /// </summary>
            /// <param name="headName">Tha Document Name</param>
            /// <param name="hAlign">The h Align</param>
            /// <param name="underline"></param>
            /// <returns></returns>
            ///
            public PdfPCell DocumentHeadingIncrease(string headName, int hAlign = Element.ALIGN_RIGHT, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading = new FontSelector();
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 13, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 13, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_LEFT, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            public PdfPCell DocumentHeading(string headName, int hAlign = Element.ALIGN_CENTER, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading8);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading.AddFont(Heading8);
                Font docChiHeading = new Font(chinesebasefont, 10, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            /// <summary>
            /// Subs the heading cell.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <returns></returns>
            public PdfPCell TableCaption(string caption, int vAlign = Element.ALIGN_MIDDLE, int hAlign = Element.ALIGN_LEFT, bool isbold = true, bool IsColor = false, bool isBorder = false)
            {
                if (isbold)
                {
                    FontSelector selectorTableCaptionbold = new FontSelector();
                    Font TableChiHead7;
                    Font TableKorHead7;
                    if (IsColor == false)
                    {
                        selectorTableCaptionbold.AddFont(Heading7);
                        TableChiHead7 = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                        TableKorHead7 = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                        selectorTableCaptionbold.AddFont(TableChiHead7);
                        selectorTableCaptionbold.AddFont(TableKorHead7);
                    }
                    else
                    {
                        selectorTableCaptionbold.AddFont(TableCaption7);
                        Font TableChiCap7 = new Font(chinesebasefont, 9, Font.BOLD, TableCaptionFontColour);
                        Font TableKorCap7 = new Font(koreanbasefont, 9, Font.BOLD, TableCaptionFontColour);
                        selectorTableCaptionbold.AddFont(TableChiCap7);
                        selectorTableCaptionbold.AddFont(TableKorCap7);
                    }
                    return new PdfPCell(selectorTableCaptionbold.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
                else
                {
                    FontSelector selectorTabCaption = new FontSelector();
                    selectorTabCaption.AddFont(Normal6);
                    Font ChiTableNormal6 = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    Font KorTableNormal6 = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    selectorTabCaption.AddFont(ChiTableNormal6);
                    selectorTabCaption.AddFont(KorTableNormal6);
                    return new PdfPCell(selectorTabCaption.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true, bool isLowFont = false)
            {
                if (isLowFont)
                    return TableHeading(caption, Heading5, hAlign, underline);
                else
                    return TableHeading(caption, Heading6, hAlign, underline);
            }

            /// <summary>
            /// Tables the footer.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <param name="underline">if set to <c>true</c> [underline].</param>
            /// <returns></returns>
            public PdfPCell TableFooter(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
                mPdfCell.BackgroundColor = TableFooterBgColor;
                return mPdfCell;
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="headingFont">The heading font.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, Font headingFont, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                int padding = 3;
                var mChunk = new Chunk(caption, headingFont);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);

                FontSelector selectorTabHeading = new FontSelector();
                selectorTabHeading.AddFont(headingFont);
                Font ChiTabHeading = new Font(chinesebasefont, 8, Font.BOLD, BaseColor.BLACK);
                Font KorTabHeading = new Font(koreanbasefont, 8, Font.BOLD, BaseColor.BLACK);
                selectorTabHeading.AddFont(ChiTabHeading);
                selectorTabHeading.AddFont(KorTabHeading);
                return new PdfPCell(selectorTabHeading.Process(mChunk.ToString())) { BorderColor = TableBorderColor, BackgroundColor = TableHeadingBgColor, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = 2, PaddingRight = 2 };//padding - 2
            }

            /// <summary>
            /// Tables Content
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCell(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabContentCell = new FontSelector();
                selectorTabContentCell.AddFont(Normal);
                Font ChiTabContentcell = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabContentcell = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabContentCell.AddFont(ChiTabContentcell);
                selectorTabContentCell.AddFont(KorTabContentcell);
                return new PdfPCell(selectorTabContentCell.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            public PdfPCell TableContentCellNormalandItalic(string cellData, string cellItalicData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                Paragraph mPgh = new Paragraph();
                FontSelector selectorNormal = new FontSelector();
                selectorNormal.AddFont(Normal);
                Font fntChi1 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor1 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormal.AddFont(fntChi1);
                selectorNormal.AddFont(fntKor1);

                Phrase ph1 = selectorNormal.Process(cellData);
                FontSelector selectorNormItalic = new FontSelector();
                selectorNormItalic.AddFont(Normal);
                Font fntChi2 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor2 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormItalic.AddFont(fntChi2);
                selectorNormItalic.AddFont(fntKor2);
                Phrase ph2 = selectorNormItalic.Process(cellItalicData);
                mPgh.Add(ph1);
                if (!string.IsNullOrEmpty(cellItalicData))
                    mPgh.Add(ph2);
                return new PdfPCell(mPgh) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content Italic
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellItalic(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellItalic = new FontSelector();
                selectorTabConentcellItalic.AddFont(NormalItalic7);
                Font ChiContentcell = new Font(chinesebasefont, 9, Font.ITALIC, BaseColor.BLACK);
                Font KorContentcell = new Font(koreanbasefont, 9, Font.ITALIC, BaseColor.BLACK);
                selectorTabConentcellItalic.AddFont(ChiContentcell);
                selectorTabConentcellItalic.AddFont(KorContentcell);
                return new PdfPCell(selectorTabConentcellItalic.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content with red color
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellRedColor(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellRedColor = new FontSelector();
                selectorTabConentcellRedColor.AddFont(NormalRedColor);
                Font fntChiRed = new Font(chinesebasefont, 10, Font.NORMAL, BaseColor.RED);
                Font fntKorRed = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.RED);
                selectorTabConentcellRedColor.AddFont(fntChiRed);
                selectorTabConentcellRedColor.AddFont(fntKorRed);
                return new PdfPCell(selectorTabConentcellRedColor.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBold(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBold = new FontSelector();
                selectorTabConentcellBold.AddFont(NormalBold);
                Font fntChiBold = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                Font fntKorBold = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                selectorTabConentcellBold.AddFont(fntChiBold);
                selectorTabConentcellBold.AddFont(fntKorBold);
                return new PdfPCell(selectorTabConentcellBold.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold for Quotation Grand Total
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldForQuoteTotal(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBoldQT = new FontSelector();
                selectorTabConentcellBoldQT.AddFont(BoldForQuoteTotal);
                Font fntChiBorderQT = new Font(chinesebasefont, 10, Font.BOLD, FontColour);
                Font fntKorBorderQT = new Font(koreanbasefont, 10, Font.BOLD, FontColour);
                selectorTabConentcellBoldQT.AddFont(fntChiBorderQT);
                selectorTabConentcellBoldQT.AddFont(fntKorBorderQT);

                return new PdfPCell(selectorTabConentcellBoldQT.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding };
            }

            /// <summary>
            /// Table Content Cell Data Bold,Underline
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="underline"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldUnderline(string cellData, int hAlign = Element.ALIGN_LEFT, bool underline = false)
            {
                int padding = 2;
                var mChunk = new Chunk(cellData, Heading6);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                FontSelector selectorTabConentcellBoldline = new FontSelector();
                selectorTabConentcellBoldline.AddFont(Heading6);
                Font fntChiBoldline = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                Font fntKorBoldline = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                selectorTabConentcellBoldline.AddFont(fntChiBoldline);
                selectorTabConentcellBoldline.AddFont(fntKorBoldline);

                return new PdfPCell(selectorTabConentcellBoldline.Process(mChunk.ToString())) { Border = 0, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            ///// <summary>
            ///// Lines the separator.
            ///// </summary>
            ///// <returns></returns>
            public Chunk LineSeparator()
            {
                return new Chunk(new LineSeparator(4f, 100f, TableHeadingBgColor, Element.ALIGN_CENTER, -1));
            }

            public Chunk ChunkText(string caption, bool isColor = false)
            {
                if (isColor)
                    return new Chunk(caption, TableCaption7);
                else
                    return new Chunk(caption, Heading7);
            }
        }

        #endregion PDF Formatting

        public static PdfPTable QuoteInfoTableModel(QuotationPreviewModel model)
        {
            try
            {
                var MName = string.Empty;
                var Ploading = string.Empty;
                var Pdischarge = string.Empty;
                if (model.ProductId == 3)
                {
                    if (model.MovementTypeName == "Port to port") { MName = "Airport to airport"; }
                    if (model.MovementTypeName == "Door to port") { MName = "Door to airport"; }
                    if (model.MovementTypeName == "Port to door") { MName = "Airport to door"; }
                    if (model.MovementTypeName == "Door to door") { MName = "Door to door"; }
                    Ploading = "Airport of loading";
                    Pdischarge = "Airport of discharge";
                }
                else
                {
                    MName = model.MovementTypeName;
                    Ploading = "Port of loading";
                    Pdischarge = "Port of discharge";
                }
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mQuotebordertable = new PdfPTable(3);
                mQuotebordertable.WidthPercentage = 100;
                mQuotebordertable.SpacingBefore = 10;
                mQuotebordertable.DefaultCell.Border = 0;
                int haz = 0;

                PdfPTable mQuoteTab = new PdfPTable(9);
                mQuoteTab.SetWidths(new float[] { 10, 1, 13, 17, 1, 20, 18, 1, 20 });
                mQuoteTab.WidthPercentage = 100;
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Mode of transport")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.ProductName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.OriginPlaceName == null ? "" : " Origin City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : model.OriginPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.DestinationPlaceName == null ? "" : "Destination City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : model.DestinationPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Movement type")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(MName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(Ploading)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPortName == null ? "" : model.OriginPortName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(Pdischarge)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPortName == null ? "" : model.DestinationPortName)));
                if (model.co2emission != 0)
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("CO2 Emission")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.co2emission == 0 ? "" : Convert.ToString(model.co2emission) + " KG")));
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
                if (model.OriginZipCode != null || model.DestinationZipCode != null)
                {
                    if (model.OriginZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Zip/Postal Code")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginZipCode)));
                    }
                    else
                    {
                        if (model.IsHazardousCargo != 0)
                        {
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Hazardous Cargo")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.HazardousGoodsType)));
                            haz = 1;
                        }
                        else
                        {
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        }
                    }
                    if (model.DestinationZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Zip/Postal Code")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationZipCode)));
                    }
                    else
                    {
                        if (model.IsHazardousCargo != 0)
                        {
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Hazardous Cargo")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.HazardousGoodsType)));
                            haz = 1;
                        }
                        else
                        {
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                            mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        }
                    }
                }
                else
                {
                    if (model.IsHazardousCargo != 0)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Hazardous Cargo")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.HazardousGoodsType)));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        haz = 1;
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                }
                if (model.IsHazardousCargo != 0 && haz == 0)
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Hazardous Cargo")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.HazardousGoodsType)));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }

                PdfPCell mRoutecell = new PdfPCell(mQuoteTab);
                mRoutecell.Colspan = 3;
                mQuotebordertable.AddCell(mRoutecell);

                return mQuotebordertable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static PdfPTable ShipInfoTableModel(QuotationPreviewModel model)
        {
            int mProductId = model.ProductId;
            int mProductTypeId = model.ProductTypeId;
            string mCargoDescription = model.ShipmentDescription;
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mShipbordertable = new PdfPTable(1);
                mShipbordertable.WidthPercentage = 100;
                mShipbordertable.SpacingBefore = 10;
                mShipbordertable.DefaultCell.Border = 0;

                List<PdfPCell> mHeaderLabels = new List<PdfPCell>();
                PdfPTable mShipTab = null;

                mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Quantity", Element.ALIGN_LEFT, false)));

                if (mProductTypeId == 5)
                {
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Container Size", Element.ALIGN_LEFT, false)));
                    mShipTab = new PdfPTable(2);
                    mShipTab.SetWidths(new float[] { 20, 80 });
                }
                else
                {
                    mShipTab = new PdfPTable(5);
                    mShipTab.SetWidths(new float[] { 10, 10, 10, 10, 10 });
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Package Type", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Dimension(L*W*H)", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Per Piece", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Gross Weight", Element.ALIGN_LEFT, false)));
                }
                mShipTab.WidthPercentage = 100;

                mShipbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Cargo Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                foreach (var mLabel in mHeaderLabels)
                {
                    mShipTab.AddCell(mLabel);
                }
                foreach (var mShipment in model.ShipmentItems)
                {
                    mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.Quantity.ToString(), Element.ALIGN_LEFT)));
                    if (mProductTypeId == 5)
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ContainerName, Element.ALIGN_LEFT)));
                    else
                    {
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.PackageTypeName, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ItemLength + " x " + mShipment.ItemWidth + " x " + mShipment.ItemHeight + " " + mShipment.LengthUOMCode, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTPERPIECE) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTPERPIECE) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTTOTAL) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTTOTAL) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                    }
                }
                PdfPCell mShipcell = new PdfPCell(mShipTab);
                mShipcell.BorderColor = mFormattingpdf.TableBorderColor;
                mShipbordertable.AddCell(mShipcell);

                if (mProductTypeId != 5)
                {
                    string mTotalCharegebleVolume = string.Empty;
                    if (mProductId == 2)
                        mTotalCharegebleVolume = "Revenue Ton" + " : " + (IsNullOrDefault(model.ChargeableVolume) ? "0.00" : (model.ChargeableVolume) <= 1 ? String.Format(culture, format, model.ChargeableVolume) + " " + "(minimum)" : String.Format(culture, format, model.ChargeableVolume));
                    else
                        mTotalCharegebleVolume = "Volumetric Weight (Kgs)" + " : " + (IsNullOrDefault(model.VolumetricWeight) ? "0.0" : String.Format(culture, format, model.VolumetricWeight));

                    string mTotalGW = "Total Gross Weight (Kgs)" + " : " + (IsNullOrDefault(model.TotalGrossWeight) ? "0.0" : String.Format(culture, format, model.TotalGrossWeight));
                    string mTotalCBM = "Total Cubic Meters (m3)" + " : " + (IsNullOrDefault(model.TotalCBM) ? "0.000" : String.Format(culture, "{0:N4}", model.TotalCBM));
                    string mTotalCharegebleWeight = "Chargeable Weight (Kgs)" + " : " + (IsNullOrDefault(model.ChargeableWeight) ? "0.00" : String.Format(culture, format, model.ChargeableWeight));

                    PdfPTable mShipmentTotalWeights;

                    if (mProductId == 2)
                    {
                        mShipmentTotalWeights = new PdfPTable(3);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.SetWidths(new float[] { 53, 56, 77 });
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    else
                    {
                        mShipmentTotalWeights = new PdfPTable(4);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleWeight, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    PdfPCell mTableFooterCell = mFormattingpdf.TableHeading("ShipmentDetails", Element.ALIGN_LEFT, false);
                    mTableFooterCell.Colspan = mShipTab.NumberOfColumns;
                    mTableFooterCell.Padding = 0;
                    mTableFooterCell.BackgroundColor = BaseColor.WHITE;
                    mTableFooterCell.AddElement(mShipmentTotalWeights);
                    mShipbordertable.AddCell(mTableFooterCell);
                }
                return mShipbordertable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static PdfPTable ChargesTableModel(IEnumerable<QuotationChargePreviewModel> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                //if (PreferredCurrencyId == null || PreferredCurrencyId == "")
                //{
                PreferredCurrencyId = CurrencyId;
                //}
                //var ChargesTotal = Double.Parse(model.Where(x => x.ISCHARGEINCLUDED == true).Sum(sum => sum.TotalPrice).ToString("N2"));

                var ChargesTotal = model.Where(x => x.ISCHARGEINCLUDED == true).Sum(sum => sum.TotalPrice).ToString();
                QuotationDBFactory mFactory = new QuotationDBFactory();
                string returnvalue = mFactory.RoundedValue(ChargesTotal, CurrencyId);

                if (returnvalue != "0.00" && returnvalue != null)
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + returnvalue, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        if (QC.ISCHARGEINCLUDED == true)
                        {
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.ChargeName, Element.ALIGN_LEFT)));
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.RoundedTotalPrice.ToString(), Element.ALIGN_RIGHT)));
                        }
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static PdfPTable TermsandConditionsTableModel(QuotationPreviewModel model)
        {
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mTACtable = new PdfPTable(1);
                mTACtable.WidthPercentage = 100;
                mTACtable.HeaderRows = 0;
                mTACtable.DefaultCell.Border = 0;
                mTACtable.SetWidths(new float[] { 100 });
                mTACtable.KeepTogether = true;

                PdfPCell quotationheading = mFormattingpdf.DocumentHeading("Terms and Conditions");
                quotationheading.PaddingBottom = -7;
                quotationheading.Border = 0;
                mTACtable.AddCell(quotationheading);

                Phrase mQuoteCondLinephrase = new Phrase();
                mQuoteCondLinephrase.Add(mFormattingpdf.LineSeparator());
                mTACtable.AddCell(mQuoteCondLinephrase);
                mTACtable.AddCell(new PdfPCell() { FixedHeight = 5, Border = 0 });
                int i = 0;
                foreach (var QT in model.TermAndConditions)
                {
                    i = i + 1;
                    PdfPCell mQuoteDesciption = new PdfPCell(mFormattingpdf.TableContentCell(i + "." + QT.DESCRIPTION)) { Padding = 2 };
                    mTACtable.AddCell(mQuoteDesciption);
                }
                return mTACtable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        static bool IsNullOrDefault<T>(T value)
        {
            return object.Equals(value, default(T));
        }

        public static PdfPCell ImageCell(string path, float scale, int align)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath(path));
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 0f;
            cell.PaddingTop = 0f;
            return cell;
        }

        public class ITextEvents : PdfPageEventHelper
        {
            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                string format = "MMMM d, yyyy";
                CultureInfo ci = new CultureInfo("en-US");
                string TaxNum = "";
                if (PDFpage == 0)
                {
                    string FullName = (DUserModel.FIRSTNAME != null) ? DUserModel.FIRSTNAME + " " + DUserModel.LASTNAME : DUIModels.GuestName;
                    if (string.IsNullOrWhiteSpace(FullName))
                        FullName = DUserModel.USERID;
                    string Company = (DUserModel.COMPANYNAME != null) ? DUserModel.COMPANYNAME : DUIModels.GuestCompany;
                    string Address = (DUserModel.UAFULLADDRESS != null) ? DUserModel.UAFULLADDRESS : "";
                    string Phone = (DUserModel.WORKPHONE != null) ? (DUserModel.ISDCODE + " - " + DUserModel.WORKPHONE) : "";
                    string EMail = (DUserModel.USERID != null) ? DUserModel.USERID : DUIModels.GuestEmail;
                    if (!string.IsNullOrWhiteSpace(DUserModel.COUNTRYNAME))
                    {
                        if (DUserModel.COUNTRYID == Convert.ToDouble(Country.INDIA))
                        {
                            if (!string.IsNullOrEmpty(DUserModel.VATREGNUMBER))
                                TaxNum = "GST number : " + DUserModel.VATREGNUMBER;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(DUserModel.VATREGNUMBER))
                                TaxNum = "Indirect Tax number : " + DUserModel.VATREGNUMBER;
                        }
                    }
                    PdfPTable mUserTab = new PdfPTable(2);
                    mUserTab.WidthPercentage = 100;
                    mUserTab.SpacingAfter = 0;
                    mUserTab.DefaultCell.Padding = 0;
                    mUserTab.DefaultCell.Border = 0;
                    mUserTab.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                    float[] tblUserwidths = new float[] { 50, 50 };
                    mUserTab.SetWidths(tblUserwidths);

                    int mUserInfoAlign = Element.ALIGN_LEFT;
                    PdfPTable mHeaderUserTable = new PdfPTable(1);
                    mHeaderUserTable.WidthPercentage = 100;
                    mHeaderUserTable.TotalWidth = 230;
                    mHeaderUserTable.DefaultCell.Padding = 0;
                    mHeaderUserTable.HorizontalAlignment = 0;
                    mUserTab.AddCell(mHeaderUserTable);

                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell("For,", mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(FullName, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Company, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Address))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Address, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Phone))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Phone, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(EMail, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(TaxNum, mUserInfoAlign));
                    mUserTab.AddCell(mHeaderUserTable);
                    mHeaderUserTable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 35), writer.DirectContent);
                }
                PDFpage++;
                string mQuotationNumber = DUIModels.QuotationNumber;
                string mDateCreated = DUIModels.DateofEnquiry.ToString();
                string mDateofValidity = DUIModels.DateOfValidity.ToString();
                string mIssueDate = "Issue Date" + " : " + ((!String.IsNullOrEmpty(mDateCreated)) ? Convert.ToDateTime(mDateCreated).ToString(format, ci) : string.Empty);
                string mValidTillDate = "Quote valid until" + " : " + ((!String.IsNullOrEmpty(mDateofValidity)) ? Convert.ToDateTime(mDateofValidity).ToString(format, ci) : string.Empty);
                string mtransittime = "Transit time" + " : " + (((!String.IsNullOrEmpty(DUIModels.Transittime)) && (DUIModels.Transittime != "No Transmit time available")) ? (DUIModels.Transittime) : "Not available");
                string mShipmentDate = string.Empty;
                string DateOfShipment = DUIModels.DateofShipment.ToString();
                if (String.IsNullOrEmpty(DateOfShipment))
                    mShipmentDate = "DateOfShipment" + " : " + ((!String.IsNullOrEmpty(DateOfShipment)) ? Convert.ToDateTime(DateOfShipment).ToString(format, ci) : string.Empty);

                PdfPTable mHeadermaintable = new PdfPTable(3);
                mHeadermaintable.WidthPercentage = 100;
                mHeadermaintable.SpacingAfter = 0;
                mHeadermaintable.DefaultCell.Padding = 0;
                mHeadermaintable.DefaultCell.Border = 0;
                mHeadermaintable.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                float[] tblwidths = new float[] { 50, 29, 36 };
                mHeadermaintable.SetWidths(tblwidths);

                int mQuoteInfoAlign = Element.ALIGN_LEFT;

                PdfPTable mHeaderLeft = new PdfPTable(1);
                mHeaderLeft.WidthPercentage = 100;
                mHeaderLeft.DefaultCell.Padding = 0;
                mHeaderLeft.HorizontalAlignment = 0;

                //Company Logo
                PdfPTable mHeaderImageTable = new PdfPTable(1);
                mHeaderImageTable.WidthPercentage = 100;
                mHeadermaintable.TotalWidth = 500;
                mHeaderImageTable.DefaultCell.Padding = 0;
                mHeaderImageTable.DefaultCell.Border = 0;
                PdfPCell imgcel = ImageCell("~/Images/AgilityPrint.png", 48f, mQuoteInfoAlign);
                imgcel.Border = 0;
                imgcel.PaddingBottom = 3;
                mHeaderImageTable.AddCell(imgcel);
                mHeaderImageTable.AddCell(mFormattingpdf.TableContentCellBold(mQuotationNumber, mQuoteInfoAlign));
                mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mIssueDate, mQuoteInfoAlign));
                mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mValidTillDate, mQuoteInfoAlign));
                if (DUIModels.ProductId == 3 && mtransittime != "Transit time : Not available")
                    mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mtransittime, mQuoteInfoAlign));
                if (!string.IsNullOrWhiteSpace(mShipmentDate))
                    mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mShipmentDate, mQuoteInfoAlign));
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell(mHeaderImageTable);
                mHeadermaintable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 10), writer.DirectContent);
            }
        }
        #endregion PDF Methods
        #endregion


        public static void SendMailAndStoreMailCommunication(string from, string To, string cc, string BCC, string Module, string SubModule, string REFERENCEID,
           string CreatedBy, string HTMLBODY, string AttachementIDs, bool IsStoreReQuired, string SUBJECT)
        {
            try
            {
                if (IsStoreReQuired)
                {
                    string base64 = EncryptStringAES(HTMLBODY);
                    byte[] bytes = System.Convert.FromBase64String(Convert.ToString(base64));
                    tbf.StoreMailCommunication(Module, SubModule, REFERENCEID, from, To, cc, BCC, CreatedBy, bytes, AttachementIDs, SUBJECT);
                }
                // Sendmail(subject, from, To, cc, BCC, Attachmentbytes, AttachmentName, HTMLBODY);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public static string EncryptStringAES(string plainText)
        {

            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var EncriptedFromJavascript = EncryptStringToBytes(plainText, keybytes, iv);
            return Convert.ToBase64String(EncriptedFromJavascript);
        }
        public static string DecryptStringAES(string cipherText)
        {

            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                try
                {
                    // Create the streams used for decryption.
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            // Create a RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }


        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string GetEnvironmentName()
        {
            string environment = ConfigurationManager.AppSettings["EnvironmentName"];
            return environment;
        }

        public static string GetEnvironmentNameDocs()
        {
            string environment = ConfigurationManager.AppSettings["EnvironmentName"];
            if (environment.ToUpper().Contains("AGILE"))
            {
                environment = "AGL";
            }
            else if (environment.ToUpper().Contains("SIT"))
            {
                environment = "SIT";
            }
            else if (environment.ToUpper().Contains("DEMO"))
            {
                environment = "DEM";
            }
            else if (environment.ToUpper().Contains("DEV"))
            {
                environment = "DEV";
            }
            else if (environment.ToUpper() == string.Empty)
            {
                environment = "PRD";
            }
            return environment;
        }
        
        public static void SendExceptionEmail(string url, string ex, string st)
        {
            string Emailids = string.Empty;
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            Emailids = System.Configuration.ConfigurationManager.AppSettings["ExceptionNotifier"];
            string[] ids = Emailids.Split(',');
            foreach (var item in ids)
            {
                message.To.Add(new MailAddress(item));
            }
            string EnvironmentName = GetEnvironmentName();
            message.Subject = "Exception notification" + EnvironmentName;
            message.IsBodyHtml = true;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"
                <table style='height: 74px;' width='284';border='1'>
                <tbody>
                <tr style='height: 17px;'>
                <td style='width: 134px; height: 17px;'>URL</td>"
            );
            sb.AppendLine("<td style='width: 134px; height: 17px;'>"); sb.Append(url); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 17px;'>
                <td style='width: 134px; height: 17px;'>EXCEPTION</td>");
            sb.AppendLine("<td style='width: 134px; height: 17px;'>"); sb.Append(ex); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 18px;'>
                <td style='width: 134px; height: 18px;'>STACKTRACE</td> ");
            sb.AppendLine("<td style='width: 134px; height: 18px;'>"); sb.Append(st); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 18px;'>
                <td style='width: 134px; height: 18px;'>TIME (UTC) </td> ");
            sb.AppendLine("<td style='width: 134px; height: 18px;'>");
            sb.Append(DateTime.UtcNow); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                </tbody>
                </table>");

            message.Body = sb.ToString();
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }

        public static void SendExceptionEmailV2(ExceptionEmailDto data)
        {
            string Emailids = string.Empty;
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            Emailids = System.Configuration.ConfigurationManager.AppSettings["ExceptionNotifier"];
            string[] ids = Emailids.Split(',');
            foreach (var item in ids)
            {
                message.To.Add(new MailAddress(item));
            }
            string EnvironmentName = GetEnvironmentName();
            message.Subject = "Exception notification" + EnvironmentName;
            message.IsBodyHtml = true;

            string newMail = @"
                 <!DOCTYPE html>
<html lang='en' style='margin: 0; outline: none; padding: 0;'>
<head>
    <!--[if !mso]><meta http-equiv='X-UA-Compatible' content='IE=edge'><!--<![endif]-->
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta http-equiv='Content-Language' content='en-us'>
</head>
<body style='margin: 0; outline: none; padding: 0; font-family: arial, sans-serif; font-size:12px;'>
<div style='width: 950px;'>
    <table style='width: 850px; border-collapse: collapse; position: relative; table-layout: fixed;' cellpadding='10'
           cellspacing='0' border='1'>
        <tbody>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                URL (${HttpMethod})
            </td>
            <td style='word-break: break-all;'>
                ${URL}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                EXCEPTION
            </td>
            <td style='word-break: break-all;'>
                ${Message}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                STACK TRACE
            </td>
            <td style='word-break: break-all;'>
                ${stack}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                USER
            </td>
            <td style='word-break: break-all;'>&nbsp;${UserEmail}</td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                TIME (UTC)
            </td>
            <td style='word-break: break-all;'>
                ${UTC}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                HEADERS
            </td>
            <td style='word-break: break-all;'>
                ${httpHeaders}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                REQUEST BODY
            </td>
            <td style='word-break: break-all;'>
                ${FromBody}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                USER AGENT
            </td>
            <td style='word-break: break-all;'>
                ${UserAgent}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                SERVER NAME
            </td>
            <td style='word-break: break-all;'>
                ${ServerName}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                HOST ADDRESS
            </td>
            <td style='word-break: break-all;'>
                ${UserHostAddress}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                HOST&nbsp;NAME
            </td>
            <td style='word-break: break-all;'>
                ${UserHostName}
            </td>
        </tr>
        <tr>
            <td style='width: 200px; font-weight:bold;'>
                SERVER VARIABLES
            </td>
            <td style='word-break: break-all;'>
                ${ServerVariables}
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>

                ";

            newMail = newMail.Replace("${absoluteUrl}", data.AppUrl);
            newMail = newMail.Replace("${HttpMethod}", data.ActionMethod);
            newMail = newMail.Replace("${URL}", data.Url);
            newMail = newMail.Replace("${httpHeaders}", data.Headers);
            newMail = newMail.Replace("${FromBody}", data.Content == string.Empty ? "undefined" : data.Content);
            newMail = newMail.Replace("${Message}", data.Error);
            newMail = newMail.Replace("${stack}", data.StackTrace);
            newMail = newMail.Replace("${ServerName}", System.Configuration.ConfigurationManager.AppSettings["ServerName"]);
            newMail = newMail.Replace("${UTC}", data.UTC);
            newMail = newMail.Replace("${UserAgent}", data.UserAgent);
            newMail = newMail.Replace("${UserHostAddress}", data.UserHostAddress);
            newMail = newMail.Replace("${UserHostName}", data.UserHostName);
            newMail = newMail.Replace("${ServerVariables}", HttpUtility.UrlDecode(data.ServerVariables).
                    Replace("&",System.Environment.NewLine));
            newMail = newMail.Replace("${UserEmail}", "undefined");

            message.Body = newMail;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }
    }
}

namespace FOCiS.SSP.WebApi
{
    public static class Encryption
    {
        public static string Encrypt(string plainText)
        {
            string key = "CHIKOTI1213#";
            byte[] EncryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            EncryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(plainText);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            string base64 = Convert.ToBase64String(mStream.ToArray());
            return base64 = base64.Replace("+", "%2B");
        }

        public static string Decrypt(string encryptedText)
        {
            string key = "CHIKOTI1213#";
            byte[] DecryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] inputByte = new byte[encryptedText.Length];

            DecryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByte = Convert.FromBase64String(encryptedText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByte, 0, inputByte.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }
    }
}