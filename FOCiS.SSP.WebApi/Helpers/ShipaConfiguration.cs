﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FOCiS.SSP.WebApi.Helpers
{
    public class ShipaConfiguration
    {
        #region Accessing Keys from config file 
        internal static string GetActiveCampaignApiKey()
        {
            return GetValueforKey("ActiveCampaignApiKey");
        }
        internal static string GetActiveCampaignApiUrl()
        {
            return GetValueforKey("ActiveCampaignApiUrl");
        }
        internal static string ListGuestUser()
        {
            return GetValueforKey("ListGuestUser");
        }
        internal static string ListRegisteredUser()
        {
            return GetValueforKey("ListRegisteredUser");
        }
        internal static string SFSubScriberUser()
        {
            return GetValueforKey("SFSubScriberUser");
        }
        internal static string SFUnSubScriberUser()
        {
            return GetValueforKey("SFUnSubScriberUser");
        }
        internal static string ListUserAddress()
        {
            return GetValueforKey("sender_addr1");
        }
        internal static string UserCountry()
        {
            return GetValueforKey("sender_country");
        }
        internal static string SenderZipCode()
        {
            return GetValueforKey("sender_zip");
        }
        internal static string SenderUrl()
        {
            return GetValueforKey("sender_url");
        }
        internal static string SenderName()
        {
            return GetValueforKey("sender_name");
        }
        internal static string SenderCity()
        {
            return GetValueforKey("sender_city");
        }
        internal static string SenderReminder()
        {
            return GetValueforKey("sender_reminder");
        }
        internal static string ShipaApiUrl()
        {
            return GetValueforKey("APPURL");
        }

        internal static string MQQueueName()
        {
            return GetValueforKey("QUEUE_NAME");
        }
        internal static string MQQueueManagerName()
        {
            return GetValueforKey("QUEUE_MANAGER_NAME");
        }
        internal static string HostName()
        {
            return GetValueforKey("HOST_NAME");
        }
        internal static string ChannelProperty()
        {
            return GetValueforKey("CHANNEL_PROPERTY");
        }
        #endregion
        private static string GetValueforKey(string Key)
        {
            return ConfigurationManager.AppSettings[Key].ToString();

        }

    }

    public class ShipaApiException : Exception
    {
        public ShipaApiException(string message)
            : base(message)
        {
        }
    }
}