﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.Web.UI.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace FOCiS.SSP.WebApi
{
    public class ApiExceptionHandler : IExceptionHandler
    {
        public virtual Task HandleAsync(ExceptionHandlerContext context,
                                        CancellationToken cancellationToken)
        {
            LogError(context);
            const string errorMessage = "An error has occurred.";
            var response = context.Request.CreateResponse(
                    HttpStatusCode.InternalServerError,
                    new
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Message = errorMessage
                    });
            response.Headers.Add("X-Error", errorMessage);
            context.Result = new ResponseMessageResult(response);
            return Task.FromResult(0);
        }

        public virtual void LogError(ExceptionHandlerContext context)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            var requri = context.Request.RequestUri.ToString();
            mFactory.InsertErrorLog(
                "",
                "api",
                requri.Substring(requri.IndexOf("/api/")),
                context.Exception.Message,
                context.Exception.StackTrace.ToString()
             );

            var Content = string.Empty;
            if (context.Request.Method.ToString() == "POST")
            {
                var d = new StreamReader(context.Request.Content.ReadAsStreamAsync().Result);
                d.BaseStream.Seek(0, SeekOrigin.Begin);
                Content = d.ReadToEnd();
            }
            var hostdata = ((System.Web.HttpRequestWrapper)context.RequestContext.GetType().Assembly.GetType("System.Web.Http.WebHost.WebHostHttpRequestContext").GetProperty("WebRequest").GetMethod.Invoke(context.RequestContext, null));
            var exdata = new ExceptionEmailDto()
            {
                ActionMethod = context.Request.Method.ToString(),
                AppUrl = System.Configuration.ConfigurationManager.AppSettings["APPURL"],
                Error = context.Exception.Message.ToString(),
                Headers = context.Request.Headers.ToString(),
                ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"],
                StackTrace = context.Exception.StackTrace.ToString(),
                Url = context.Request.RequestUri.ToString(),
                UserAgent = hostdata == null ? string.Empty : hostdata.UserAgent.ToString(),
                UserHostAddress = hostdata == null ? string.Empty : hostdata.UserHostAddress.ToString(),
                UserHostName = hostdata == null ? string.Empty : hostdata.UserHostName.ToString(),
                UTC = DateTime.UtcNow.ToString(),
                Content = Content,
                ServerVariables = hostdata == null ? "undefined" : hostdata.ServerVariables.ToString()
            };
            FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendExceptionEmailV2(
                exdata
            );
        }
    }
}