﻿using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.WebApi.InterfaceManager
{
    interface ISalesForceManager
    {
        Task<SalesForceEntity> PushDatatoMessageQueue(SalesForceEntity SalesForceEntity);

        int GetQuotationDataById(long id,string type);
    }
}
