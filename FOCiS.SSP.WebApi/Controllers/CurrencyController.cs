﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models;
using FOCiS.SSP.DBFactory.Entities;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using FOCiS.SSP.Models.CacheManagement;
using Newtonsoft.Json;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class CurrencyController : BaseController
    {
        private readonly IJobBookingDbFactory _JFactory;
        private readonly IMDMDataFactory _MDMFactory;

        public CurrencyController()
        {
            _JFactory = new JobBookingDbFactory();
            _MDMFactory = new MDMDataFactory();
        }

        public CurrencyController(IJobBookingDbFactory jf, IMDMDataFactory mf)
        {
            _JFactory = jf;
            _MDMFactory = mf;
        }

        public IHttpActionResult Get()
        {
            return BadRequest();
        }

        [Route("api/mdm/currency/GetCurrency")]
        [HttpGet]
        public IHttpActionResult GetCurrency(string s, int page = 1, int pageSize = 10)
        {
            // MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select2Entity<string>> mFactoryEntity = _MDMFactory.FindCurrency((page <= 0 ? 1 : page), (pageSize <= 0 ? 10 : pageSize), s);
            return Ok(mFactoryEntity);
        }

        [Route("api/mdm/currency/GetCountry")]
        [HttpGet]
        public IHttpActionResult GetCountry(string s, string CountryId)
        {
            if (CountryId == null) { CountryId = "0"; }
            //MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select3Entity<Int64>> mFactoryEntity = _MDMFactory.FindCountry(s, CountryId);
            return Ok(mFactoryEntity);
        }

        [Route("api/mdm/currency/ValidateZipcode")]
        [HttpGet]
        public async Task<IHttpActionResult> ValidateZipcode(string cityid, string zip, string countrycode)
        {

            // MDMDataFactory mFactory = new MDMDataFactory();
            if (cityid != "" && cityid != null)
            {

                var mFactoryEntity = await Task.Run(() => _MDMFactory.GetZipcodesByCity(cityid, countrycode)).ConfigureAwait(false);
                return Ok(mFactoryEntity);
            }
            else
            {
                var mFactoryEntity = await Task.Run(() => _MDMFactory.GetCitiesByZipCode(zip, countrycode)).ConfigureAwait(false);
                return Ok(mFactoryEntity);
            }
        }

        [Route("api/mdm/currency/IsZipcodesAvailable")]
        [HttpGet]
        public IHttpActionResult IsZipcodesAvailable(string countrycode)
        {
            //MDMDataFactory mFactory = new MDMDataFactory();
            var mFactoryEntity = _MDMFactory.IsZipcodesAvailable(countrycode);
            return Ok(mFactoryEntity);
        }

        [Route("api/mdm/currency/GetStateIsapplicable")]
        [HttpPost]
        public IHttpActionResult GetStateIsapplicable(GetStateIsapplicableDto Req)
        {
            //JobBookingDbFactory mFactory = new JobBookingDbFactory();
            var dt = _JFactory.GetStateConfigurationFlag(Req.CountryID);
            return Ok(dt);
        }

        [Route("api/mdm/currency/FillCityByCountryID")]
        [HttpPost]
        public dynamic FillCityByCountryID(GetStateIsapplicableDto Req)
        {
            return SafeRun<dynamic>(() =>
            {
                //MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CityEntity> cities = _MDMFactory.GetCitiesByCountry(Convert.ToInt32(Req.CountryID));
                if (cities != null)
                {
                    var CityList = (
                                from items in cities
                                select new
                                {
                                    Text = items.NAME,
                                    Value = items.UNLOCATIONID,
                                    CityCode = items.CODE
                                }).Distinct().OrderBy(x => x.Text).ToList();
                    return Ok(CityList);
                }
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = "No Cities Available"
                    })
                };
            });
        }

        [Route("api/mdm/currency/GetCurrencyRounded")]
        [HttpPost]
        public HttpResponseMessage GetCurrencyRounded([FromBody]GetCurrencyRoundedDto Req)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                //var db = new JobBookingDbFactory();
                var value = _JFactory.GetCurrencyRounded(Req.AMOUNT, Req.CURRENCY.ToUpper());
                return BaseResponse(new
                {
                    AMOUNT = value.Rows[0]["currency"],
                    CURRENCY = Req.CURRENCY.ToUpper(),
                    DECIMALCOUNT = value.Rows[0]["DecimalCount"]
                }, HttpStatusCode.OK);
            });
        }

        [Route("api/mdm/currency/IsValidZipcode")]
        [HttpPost]
        public HttpResponseMessage IsValidZipcode([FromBody]ValidatePostcalCodeModel req)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                string json = string.Empty;
                StringBuilder url = new StringBuilder();
                url.Append("https://secure.geonames.org/postalCodeLookupJSON?username=agility");
                url.Append("&postalcode="); url.Append(req.postalcode);
                url.Append("&country="); url.Append(req.countrycode);
                url.Append("&placename="); url.Append(req.text);
                url.Append("&maxRows=1");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.ToString());
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    json = reader.ReadToEnd();
                    reader.Dispose();
                    stream.Dispose();
                    response.Dispose();
                }

                GeoNamesModel po = Newtonsoft.Json.JsonConvert.DeserializeObject<GeoNamesModel>(json);

                if (po.postalcodes.Any())
                {
                    return CountrySpecificGeoNameValidations(req, po.postalcodes[0]);
                }
                else
                {
                    return BaseResponse("INVALID", HttpStatusCode.OK);
                }
            });
        }

        private HttpResponseMessage CountrySpecificGeoNameValidations(ValidatePostcalCodeModel req, PostalCodes po)
        {
            switch (req.countrycode.ToUpperInvariant())
            {
                case "IN":
                    var state = po.adminName1;
                    if (state.ToLowerInvariant() == req.subcode.ToLowerInvariant()) return BaseResponse("VALID", HttpStatusCode.OK);
                    return BaseResponse("INVALID", HttpStatusCode.OK);
                case "ES":
                    var st = FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.RemoveDiacritics(po.adminName2);
                    if (st.ToLowerInvariant() == req.subcode.Split(new char[] { ' ' })[2].ToLowerInvariant()) return BaseResponse("VALID", HttpStatusCode.OK);
                    return BaseResponse("INVALID", HttpStatusCode.OK);
                case "US":
                    var stcode = po.adminCode1;
                    if (stcode.ToLowerInvariant() == req.subcode.ToLowerInvariant()) return BaseResponse("VALID", HttpStatusCode.OK);
                    return BaseResponse("INVALID", HttpStatusCode.OK);
                default:
                    return BaseResponse("VALID", HttpStatusCode.OK);
            }
        }

        [Route("api/mdm/currency/conversion")]
        [HttpPost]
        public HttpResponseMessage CurrencyConversion(CurrencyConversionModel req)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                var db = new JobBookingDbFactory();
                var value = db.GetCurrencyConversion(
                                req.amount,
                                req.currency.ToUpper(),
                                req.tocurrency.ToUpper()
                             );
                return BaseResponse(value.Rows[0]["currency"], HttpStatusCode.OK);
            });
        }

        [Route("api/mdm/currency/GetCurrencyList")]
        [HttpGet]
        public HttpResponseMessage GetCurrencyList(string s = "")
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                List<CurrencyInformation> list = null;
                string path = HttpRuntime.AppDomainAppPath;
                using (StreamReader r = new StreamReader(path + "/JsonData/CurrenciesList.json"))
                {
                    string json = r.ReadToEnd();
                    list = JsonConvert.DeserializeObject<List<CurrencyInformation>>(json);
                }

                if (!string.IsNullOrEmpty(s))
                {
                    list = list.Where(o =>
                        o.ID.ToLowerInvariant().Contains(s.ToLowerInvariant())
                        || o.TEXT.ToLowerInvariant().Contains(s.ToLowerInvariant()))
                    .ToList();
                }
                return BaseResponse(list, HttpStatusCode.OK);
            });
        }

        private HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }
    }

    public class GeoNamesModel
    {
        public PostalCodes[] postalcodes { get; set; }
    }

    public class PostalCodes
    {
        public string postalcode { get; set; }
        public string placeName { get; set; }
        public string adminName1 { get; set; }
        public string adminCode1 { get; set; }
        public string adminName2 { get; set; }
        public string adminCode2 { get; set; }
        public string adminName3 { get; set; }
        public string adminCode3 { get; set; }
    }

    public class ValidatePostcalCodeModel
    {
        public string countrycode { get; set; }
        public string text { get; set; }
        public string subcode { get; set; }
        public string postalcode { get; set; }
    }

    public class CurrencyConversionModel
    {
        public string amount { get; set; }
        public string currency { get; set; }
        public string tocurrency { get; set; }
    }

    public class GetStateIsapplicableDto
    {
        [Required]
        public string CountryID { get; set; }
    }

    public class GetCurrencyRoundedDto
    {
        public string AMOUNT { get; set; }
        public string CURRENCY { get; set; }
    }
}