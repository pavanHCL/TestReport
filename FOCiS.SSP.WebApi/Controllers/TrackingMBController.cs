﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using System.Data;
using Newtonsoft.Json;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using FOCiS.SSP.Models.Tracking;
using FOCiS.SSP.Models.JP;
using System.Web.Mail;
using System.Net.Mail;
using System.Web;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using FOCiS.SSP.Web.UI.Controllers;
using System.Text.RegularExpressions;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class TrackingMBController : BaseController
    {
        [Route("api/TrackingMB/TrackList")]
        [HttpPost]
        public dynamic TrackList([FromBody] dynamic model)
        {
            try
            {
                string Status = model.status;
                if (string.IsNullOrEmpty(Status))
                {
                    model.status = "ACTIVE";
                }
                //DbFactory object initiation and collect data from DB
                TrackingDBFactory mFactory = new TrackingDBFactory();
                ListTrackingEntity trackentity = mFactory.ReturnTrackEntityList(Convert.ToString(model.Email), "", 0,
                    Convert.ToString(model.status), Convert.ToInt32(model.PageNo), Convert.ToString(model.SearchString).ToUpper());

                //Main Data(Quote,Job Booking)
                List<DBFactory.Entities.TrackingEntity> mDBEntity = trackentity.TrackingItems.ToList();
                List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);

                //Package Details Lsit
                List<DBFactory.Entities.PackageDetailsEntity> mDBEntityLinq = (from w1 in trackentity.PackageDetailsItems where trackentity.TrackingItems.Any(w2 => w1.JobNumber == w2.JOBNUMBER) select w1).ToList();
                List<PackageDetailsModel> mDPackageModel = Mapper.Map<List<DBFactory.Entities.PackageDetailsEntity>, List<PackageDetailsModel>>(mDBEntityLinq);

                //Track Service
                List<DBFactory.Entities.Track> mDBEntityTrackLinq = (from w1 in trackentity.TrackItems where trackentity.TrackingItems.Any(w2 => w1.ConsignmentId == w2.CONSIGNMENTID) select w1).OrderByDescending(x => x.CURRENTEVENTORDER).ToList();
                List<TrackModel> mDTrackModel = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(mDBEntityTrackLinq);


                //Track Service
                List<DBFactory.Entities.Track> mDBEntityTrackOrder = (from w1 in trackentity.TrackItems where trackentity.TrackingItems.Any(w2 => w1.ConsignmentId == w2.CONSIGNMENTID) select w1).ToList();
                List<TrackModel> mDTrackSErviceDesOrder = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(mDBEntityTrackOrder);


                //Count
                List<DBFactory.Entities.TrackStatusCount> EntityCount = mFactory.GetTrackStatusCount(Convert.ToString(model.Email), Convert.ToString(model.SearchString).ToUpper());
                List<TrackStatusModelCountMB> TrackStatusModelCount = new List<TrackStatusModelCountMB>();
                TrackStatusModelCountMB ts = new TrackStatusModelCountMB();

                foreach (TrackStatusCount va in EntityCount)
                {
                    if (va.Trackstatus == "ACTIVE")
                    {
                        ts.ACTIVETRACK = va.TrackCount;
                    }
                    else
                    {
                        ts.COMPLETEDTRACK = va.TrackCount;
                    }

                }
                ts.ALLTrack = ts.ACTIVETRACK + ts.COMPLETEDTRACK;
                TrackStatusModelCount.Add(ts);

                foreach (var item in mUIModel)
                {
                    item.PackageDetailsItems = mDPackageModel.FindAll(o => o.JobNumber == item.JOBNUMBER);
                }

                foreach (var item in mUIModel)
                {
                    item.TrackItems = mDTrackModel.FindAll(qsItems => qsItems.ConsignmentId == item.CONSIGNMENTID);
                }

                foreach (var item in mUIModel)
                {
                    item.TrackOrderByItems = mDTrackSErviceDesOrder.FindAll(qsItems => qsItems.ConsignmentId == item.CONSIGNMENTID);
                }

                ListTrackingModelForMB lt = new ListTrackingModelForMB();
                lt.TrackingItems = mUIModel;
                lt.TrackStatusModelCount = TrackStatusModelCount;

                return Ok(lt);
            }
            catch (Exception ex)
            {
                LogError("api/TrackingMB", "TrackList", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/TrackingMB/TrackDetails")]
        [HttpPost]
        public dynamic TrackDetails([FromBody] dynamic model)
        {
            try
            {
                TrackingDBFactory mFactory = new TrackingDBFactory();

                //DbFactory object intiation

                ListTrackingEntity trackentity = mFactory.ReturnTrackEntityList(Convert.ToString(model.Email),
                    Convert.ToString(model.ConsignmentId), Convert.ToInt32(model.Jobnumber), "", 0, "");

                //Main Data(Quote,Job Booking)
                List<DBFactory.Entities.TrackingEntity> mDBEntity = trackentity.TrackingItems.ToList();
                List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);

                //Package Details Lsit
                List<DBFactory.Entities.PackageDetailsEntity> mDPackageEntity = trackentity.PackageDetailsItems.ToList();
                List<PackageDetailsModel> mDPackageModel = Mapper.Map<List<DBFactory.Entities.PackageDetailsEntity>, List<PackageDetailsModel>>(mDPackageEntity);

                //Service
                List<DBFactory.Entities.Track> EntityTrack = trackentity.TrackItems.OrderByDescending(x => x.CURRENTEVENTORDER).ToList();
                List<TrackModel> mDTrackModel = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(EntityTrack);


                //Service
                List<DBFactory.Entities.Track> EntityTrackDesOrder = trackentity.TrackItems.ToList();
                List<TrackModel> mDTrackSErviceDesOrder = Mapper.Map<List<DBFactory.Entities.Track>, List<TrackModel>>(EntityTrackDesOrder);

                // Party Details
                List<DBFactory.Entities.PartyDetails> mDPartyBEntity = mFactory.GetPartyDetails(Convert.ToString(model.Email), Convert.ToInt64(model.Jobnumber));
                List<TrackingPartyDetailsModel> mUIPartyModel = Mapper.Map<List<DBFactory.Entities.PartyDetails>, List<TrackingPartyDetailsModel>>(mDPartyBEntity);

                //Comments
                List<DBFactory.Entities.ShpimentEventComments> MShpEvntCmnts = mFactory.GetShipmentEventComments(Convert.ToString(model.Email), Convert.ToInt32(model.Jobnumber), Convert.ToString(model.ConsignmentId));
                List<ShpimentEventCommentsModel> MUIShpcmnts = Mapper.Map<List<DBFactory.Entities.ShpimentEventComments>, List<ShpimentEventCommentsModel>>(MShpEvntCmnts);

                foreach (var item in mUIModel)
                {
                    item.PackageDetailsItems = mDPackageModel.FindAll(o => o.JobNumber == item.JOBNUMBER);
                }

                foreach (var item in mUIModel)
                {
                    item.TrackItems = mDTrackModel.FindAll(qsItems => qsItems.ConsignmentId == item.CONSIGNMENTID);
                }

                foreach (var item in mUIModel)
                {
                    item.TrackOrderByItems = mDTrackSErviceDesOrder.FindAll(qsItems => qsItems.ConsignmentId == item.CONSIGNMENTID);
                }

                ListTrackingModelForMB tm = new ListTrackingModelForMB();
                tm.TrackingItems = mUIModel;
                tm.TrackingPartyItems = mUIPartyModel;
                tm.shipmentCommentsItems = MUIShpcmnts;

                return Ok(tm);
            }
            catch (Exception ex)
            {
                LogError("api/TrackingMB", "TrackDetails", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }


        [Route("api/TrackingMB/ShareTrack")]
        [HttpPost]
        public dynamic ShareLink([FromBody] dynamic model)
        {
            try
            {
                TrackingDBFactory mFactory = new TrackingDBFactory();

                //DbFactory object intiation

                ListTrackingEntity trackentity = mFactory.ReturnTrackEntityList(Convert.ToString(model.LoginEmail),
                    Convert.ToString(model.ConsignmentId), Convert.ToInt32(model.Jobnumber), "", 0, "");

                //Main Data(Quote,Job Booking)
                List<DBFactory.Entities.TrackingEntity> mDBEntity = trackentity.TrackingItems.ToList();
                List<TrackingModel> mUIModel = Mapper.Map<List<DBFactory.Entities.TrackingEntity>, List<TrackingModel>>(mDBEntity);

                //Package Details Lsit
                List<DBFactory.Entities.PackageDetailsEntity> mDPackageEntity = trackentity.PackageDetailsItems.ToList();
                List<PackageDetailsModel> mDPackageModel = Mapper.Map<List<DBFactory.Entities.PackageDetailsEntity>, List<PackageDetailsModel>>(mDPackageEntity);

                // Party Details
                List<DBFactory.Entities.PartyDetails> mDPartyBEntity = mFactory.GetPartyDetails(Convert.ToString(model.LoginEmail), Convert.ToInt64(model.Jobnumber));
                List<TrackingPartyDetailsModel> mUIPartyModel = Mapper.Map<List<DBFactory.Entities.PartyDetails>, List<TrackingPartyDetailsModel>>(mDPartyBEntity);

                ListTrackingModel tm = new ListTrackingModel();
                tm.TrackingItems = mUIModel;
                tm.PackageDetailsItems = mDPackageModel;
                tm.TrackingPartyItems = mUIPartyModel;

                if (!tm.TrackingItems.Any())
                {
                    return ErrorResponse("Track details not found.");
                }

                Int64 jobnumber = tm.TrackingItems.FirstOrDefault().JOBNUMBER;

                var ids = "";

                if (tm.TrackingItems.FirstOrDefault().CONSIGNMENTID != "" && tm.TrackingItems.FirstOrDefault().CONSIGNMENTID != null)
                {
                    ids = "ConsignmentId=" + tm.TrackingItems.FirstOrDefault().CONSIGNMENTID + "?Jobnumber=" + tm.TrackingItems.FirstOrDefault().JOBNUMBER;
                }
                else
                {
                    ids = "Jobnumber=" + tm.TrackingItems.FirstOrDefault().JOBNUMBER;
                }

                string str = GetEncodedID(ids.ToString());


                string link = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "Tracking/TrackSummary" + str;

                SendMail(tm, Convert.ToString(model.LoginEmail), link, jobnumber, Convert.ToString(model.LastEventStatus), Convert.ToString(model.CONSIGNMENTID),
                    Convert.ToString(model.Name), Convert.ToString(model.Notes), Convert.ToString(model.Email), model);


                QuotationDBFactory qmmFactory = new QuotationDBFactory();
                //Insert new emails into Address Book -- Anil G

                if (Convert.ToString(model.Email) != "")
                {
                    qmmFactory.InsertNewEmailsIntoAddressBook(Convert.ToString(model.Email), model.LoginEmail.ToString());
                }

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Track has been shared."
                    })
                };

            }
            catch (Exception ex)
            {
                LogError("api/TrackingMB", "ShareTrack", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/TrackingMB/InsertComments")]
        [HttpPost]
        public HttpResponseMessage InsertComments([FromBody]InsertCommentModel request)
        {
            if (!ModelState.IsValid) return ErrorResponse(ModelState);
            return SafeRun<HttpResponseMessage>(() =>
            {
                TrackingDBFactory mFactory = new TrackingDBFactory();
                mFactory.InsertShareComments(request.EventName, request.Comments, request.UserId, request.Jobnumber, 0);
                return BaseResponse("Success", HttpStatusCode.OK); ;
            });
        }

        [Route("api/TrackingMB/FeedBack")]
        [HttpPost]
        public HttpResponseMessage InsertFeedBackComments([FromBody]FeedBackModel req)
        {
            if (!ModelState.IsValid) return ErrorResponse(ModelState);
            TrackingDBFactory mFactory = new TrackingDBFactory();
            mFactory.InsertFeedBackComments(
                req.business, req.confirmGoods, req.schedule,
                req.teamSuppport, req.material, req.payment,
                req.businessRatings, req.confirmGoodsRatings, req.scheduleRatings,
                req.teamSuppportRatings, req.materialRatings, req.paymentRatings,
                req.jobnumber, req.npsRating);
            return BaseResponse("OK", HttpStatusCode.OK);
        }

        private string GetEncodedID(string Id)
        {
            StringBuilder Result = new StringBuilder();

            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

            string[] mul = Id.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            return Result.ToString();
        }
        private void SendMail(ListTrackingModel tm, string Email, string link, long jobnumber,
            string status, string consigneementID, string name, string notes, string AddresBook, dynamic model)
        {
            try
            {
                string RateLink = string.Empty;
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.From = new MailAddress("no-reply@agility.com");


                if (AddresBook != "")
                {
                    if (AddresBook.Contains(','))
                    {
                        foreach (string s in AddresBook.Split(','))
                        {
                            message.Bcc.Add(new MailAddress(s));
                        }
                    }
                    else
                    {
                        message.CC.Add(new MailAddress(AddresBook));
                    }
                }
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight- Tracking" + EnvironmentName;
                message.IsBodyHtml = true;

                string body = string.Empty;

                string mode = tm.TrackingItems.FirstOrDefault().PRODUCTNAME.ToString().ToUpper() == "AIR FREIGHT" || tm.TrackingItems.FirstOrDefault().PRODUCTNAME.ToString().ToUpper() == "AIR" ? "Air" : "Ocean";

                string mPlaceOfReceipt = ""; string mPlaceOfDelivery = ""; string mPRLableName = string.Empty;
                string mPDLableName = string.Empty;


                if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Door to Door" || tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Door to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPLACECODE, tm.TrackingItems.FirstOrDefault().ORIGINPLACENAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPLACECODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPLACENAME);

                    mPRLableName = "Origin City"; mPDLableName = "Destination City";
                }
                else if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Door to port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPLACECODE, tm.TrackingItems.FirstOrDefault().ORIGINPLACENAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPORTCODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPORTNAME);

                    if (tm.TrackingItems.FirstOrDefault().PRODUCTID != 3)
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Port";
                    }
                    else
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Airport";
                    }
                }
                else if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Port to Port" || tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Port to port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPORTCODE, tm.TrackingItems.FirstOrDefault().ORIGINPORTNAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPORTCODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPORTNAME);

                    if (tm.TrackingItems.FirstOrDefault().PRODUCTID != 3)
                    {
                        mPRLableName = "Origin Port "; mPDLableName = "Destination Port";
                    }
                    else
                    {
                        mPRLableName = "Origin Airport "; mPDLableName = "Destination Airport";
                    }
                }
                else if (tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME == "Port to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().ORIGINPORTCODE, tm.TrackingItems.FirstOrDefault().ORIGINPORTNAME);
                    mPlaceOfDelivery = string.Format("{0}, {1}", tm.TrackingItems.FirstOrDefault().DESTINATIONPLACECODE, tm.TrackingItems.FirstOrDefault().DESTINATIONPLACENAME);
                    if (tm.TrackingItems.FirstOrDefault().PRODUCTID != 3)
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination City";
                    }
                    else
                    {
                        mPRLableName = "Origin Airport"; mPDLableName = "Destination City";
                    }
                }

                string cargodetails = string.Empty;
                foreach (var mShipment in tm.PackageDetailsItems)
                {
                    cargodetails = cargodetails +
                                    "<tr>" +
                                    "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                    "<span class='inner-detail-head'" + ">Package Type</span><br>" +
                                    "<span class='inner-detail-cont'" + ">" + mShipment.PACKAGETYPENAME + "</span></td>" +
                                    "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                    "<span class='inner-detail-head'" + ">Quantity in Pieces</span><br>" +
                                    "<span class='inner-detail-cont'" + "> " + mShipment.QUANTITY.ToString() + "</span></td> </tr>";
                }

                string mapPath = "";
                if (status != "Delivered")
                {
                    if (tm.TrackingItems.FirstOrDefault().CONSIGNMENTID == null)
                    {
                        mapPath = HttpContext.Current.Server.MapPath("~/App_Data/tracking-email-WC.html");
                    }
                    else
                    {
                        mapPath = HttpContext.Current.Server.MapPath("~/App_Data/tracking-email-Think.html");
                    }
                }
                else
                {
                    string linkParam = link.Split('?')[1];
                    RateLink = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "CustomerFeedback?" + linkParam;
                    mapPath = HttpContext.Current.Server.MapPath("~/App_Data/tracking-email-rating.html");
                }

                string loginuser = string.Empty;
                if (!ReferenceEquals(Convert.ToString(model.LoginEmail), null))
                {
                    loginuser = Convert.ToString(model.LoginEmail);
                }

                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{user_name}", ((string.IsNullOrEmpty(name)) ? Email : name));
                body = body.Replace("{notes}", notes);
                body = body.Replace("{consignment_id}", tm.TrackingItems.FirstOrDefault().CONSIGNMENTID);
                body = body.Replace("{booking_date}", tm.TrackingItems.FirstOrDefault().DATECREATED.ToString("dd-MMM-yyyy"));
                body = body.Replace("{transport_mode}", mode.ToString());
                body = body.Replace("{Shipper_name}", tm.TrackingPartyItems.FirstOrDefault(i => i.PARTYTYPE == "91").ClientName.ToString());
                body = body.Replace("{consignee_name}", tm.TrackingPartyItems.FirstOrDefault(i => i.PARTYTYPE == "92").ClientName.ToString());
                body = body.Replace("{receipt_place}", mPlaceOfReceipt.ToString());
                body = body.Replace("{del_place}", mPlaceOfDelivery.ToString());

                body = body.Replace("{lblNameOrigin}", mPRLableName);
                body = body.Replace("{lblNameDest}", mPDLableName);

                body = body.Replace("{qty}", tm.TrackingItems.FirstOrDefault().TOTALQUANTITY.ToString());
                body = body.Replace("{weight}", tm.TrackingItems.FirstOrDefault().TOTALGROSSWEIGHT.ToString());
                body = body.Replace("{cbm}", tm.TrackingItems.FirstOrDefault().TOTALCBM.ToString());
                body = body.Replace("{booking_id}", tm.TrackingItems.FirstOrDefault().OPERATIONALJOBNUMBER.ToString());
                body = body.Replace("{imag}", mode == "Air" ? "air" : "ship");
                body = body.Replace("{dep_place}", mPlaceOfDelivery.ToString());
                body = body.Replace("{Mvnt_type}", tm.TrackingItems.FirstOrDefault().MOVEMENTTYPENAME);
                body = body.Replace("{pckage_type}", cargodetails.ToString());
                body = body.Replace("{Status}", status);
                body = body.Replace("{url}", link.ToString());
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (status == "Delivered")
                {
                    body = body.Replace("{feddbackmess}", "Delivery completed successfully !!!");
                    body = body.Replace("{a1}", "Please");
                    body = body.Replace("{a2}", "provide us feedback");
                    body = body.Replace("{a3}", " so that we can improve your Shipa Freight experience.");
                    body = body.Replace("{feedback}", link);
                }

                message.Body = body;

                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();

                message.Dispose();
            }
            catch (Exception ex)
            {
                LogError("api/TrackingMB", "SendMail", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ArgumentException(ex.ToString());
            }

        }

        private HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }

        private HttpResponseMessage ErrorResponse()
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = "Something Went Wrong"
                })
            };
        }

        private HttpResponseMessage ErrorResponse(dynamic error)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.ExpectationFailed,
                    Error = error
                })
            };
        }

    }

    public class InsertCommentModel
    {
        [Required(ErrorMessage = "UserId value is required.")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "Comments value is required.")]
        public string Comments { get; set; }
        [Required(ErrorMessage = "Jobnumber value is required.")]
        public long Jobnumber { get; set; }
        [Required(ErrorMessage = "EventName value is required.")]
        public string EventName { get; set; }
    }

    public class FeedBackModel
    {
        public string business { get; set; }
        public string businessRatings { get; set; }
        public string confirmGoods { get; set; }
        public string confirmGoodsRatings { get; set; }
        public string schedule { get; set; }
        public string scheduleRatings { get; set; }
        public string teamSuppport { get; set; }
        public string teamSuppportRatings { get; set; }
        public string material { get; set; }
        public string materialRatings { get; set; }
        public string payment { get; set; }
        public string paymentRatings { get; set; }
        [Required(ErrorMessage = "jobnumber value is required.")]
        public string jobnumber { get; set; }
        public string npsRating { get; set; }
    }
}
