﻿using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models;
using FOCiS.SSP.DBFactory.Entities;
using System.IO;
using Newtonsoft.Json;
using FOCiS.SSP.Models.CacheManagement;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class UNLocationController : ApiController
    {
        public IHttpActionResult Get()
        {
            return BadRequest();
        }

        //[Route("api/mdm/unlocation")]
        //[HttpGet]
        //public IHttpActionResult unlocation(string s, string f, string countryid, string xcountryid, int page = 1, int pageSize = 10)
        //{
        //    if (countryid == null || countryid=="") { countryid = "0"; }
        //    PagedResultsEntity<dynamic> mFactoryEntity;
        //    MDMDataFactory mFactory = new MDMDataFactory();
        //    if (f == "9")
        //    {
        //        mFactoryEntity = mFactory.FindUnlocationWithoutCode((page <= 0 ? 1 : page), (pageSize <= 0 ? 10 : pageSize), s, f, countryid, xcountryid);
        //    }
        //    else
        //    {
        //        mFactoryEntity = mFactory.FindUnlocation((page <= 0 ? 1 : page), (pageSize <= 0 ? 10 : pageSize), s, f, countryid, xcountryid);
        //    }      
        //    mFactoryEntity = mFactory.FindUnlocDetails((page <= 0 ? 1 : page), (pageSize <= 0 ? 10 : pageSize), s, f, countryid, xcountryid);
        //    return Ok(mFactoryEntity);
        //}

        [Route("api/mdm/dynamicunlocation")]
        [HttpGet]
        public IHttpActionResult dynamicunlocation(string search, string f, string countryid, string xcountryid, string transportType, string packageType,string direction, int page = 1, int pageSize = 10)
        {
            if (countryid == null || countryid == "") { countryid = "0"; }
            PagedResultsEntity<dynamic> mFactoryEntity;

            MDMDataFactory mFactory = new MDMDataFactory();
            mFactoryEntity = mFactory.FindDynamicUnlocDetails((page <= 0 ? 1 : page), (pageSize <= 0 ? 10 : pageSize), search, f, countryid, xcountryid, transportType, packageType, direction);
            return Ok(mFactoryEntity);
        }
        [Route("api/mdm/GetAllPortsbyCountry")]
        [HttpGet]
        public IHttpActionResult GetAllPortsbyCountry(string f, string countryid)
        {
            if (countryid == null || countryid == "") { countryid = "0"; }
            PagedResultsEntity<dynamic> mFactoryEntity;
            MDMDataFactory mFactory = new MDMDataFactory();
            mFactoryEntity = mFactory.GetAllPortsbyCountry(f, countryid);

            return Ok(mFactoryEntity);
        }

        [Route("api/mdm/currency/GetAllCountries")]
        [HttpPost]
        public dynamic GetAllCountries([FromBody]dynamic Req)
        {
            var SourceItems = new List<CountryInformation>();
            string path = HttpRuntime.AppDomainAppPath;
           // return await Task.Run<dynamic>(() => {
                using (StreamReader r = new StreamReader(path + "/JsonData/CountriesList.json"))
                {
                    string json = r.ReadToEnd();
                    SourceItems = JsonConvert.DeserializeObject<List<CountryInformation>>(json);
                    r.Close();
                }
                int PRODUCTID = Convert.ToInt32(Req.PRODUCTID);
                int MID = Convert.ToInt32(Req.MOVEMENTTYPEID);
                string SEARCH = Convert.ToString(Req.SEARCH);
                int EXCLUDE = Convert.ToInt32(Req.EXCLUDE.Value);

                switch (MID)
                {
                    case 1: // Door to Door 
                        SourceItems = SourceItems.Where(o => o.Doortodoor == "Yes").ToList();
                        break;
                    case 2: //Port to Port
                        if (PRODUCTID == 2) SourceItems = SourceItems.Where(o => o.OceanPort == null).ToList();
                        if (PRODUCTID == 3) SourceItems = SourceItems.Where(o => o.AirPort == null).ToList();
                        break;
                    case 3: //Door to Port
                        if (Req.ORIGIN == 1) SourceItems = SourceItems.ToList();
                        if (PRODUCTID == 2 && Req.ORIGIN != 1) SourceItems = SourceItems.Where(o => o.OceanPort == null).ToList();
                        if (PRODUCTID == 3 && Req.ORIGIN != 1) SourceItems = SourceItems.Where(o => o.AirPort == null).ToList();
                        break;
                    case 4: //port to Door
                        if (Req.ORIGIN != 1) SourceItems = SourceItems.ToList();
                        if (PRODUCTID == 2 && Req.ORIGIN == 1) SourceItems = SourceItems.Where(o => o.OceanPort == null).ToList();
                        if (PRODUCTID == 3 && Req.ORIGIN == 1) SourceItems = SourceItems.Where(o => o.AirPort == null).ToList();
                        break;
                    default:
                        break;
                }
                if (SEARCH != "")
                    SourceItems = SourceItems
                                .Where(o => o.COUNTRYCODE.ToLowerInvariant().StartsWith(SEARCH.ToLowerInvariant()) || o.Text.ToLowerInvariant().StartsWith(SEARCH.ToLowerInvariant())).ToList();
                if (EXCLUDE != 0)
                    SourceItems = SourceItems.Where(o => o.Value != EXCLUDE).ToList();
                return SourceItems;            
           // });
        }
    }
}