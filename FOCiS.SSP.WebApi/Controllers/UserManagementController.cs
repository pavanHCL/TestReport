﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using System.Data;
using Newtonsoft.Json;
using FOCiS.SSP.DBFactory.Entities;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Http.Routing;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.DB;
using System.Text.RegularExpressions;
using AutoMapper;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.WebApi.Helpers;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Web.UI.Controllers;
using System.ComponentModel.DataAnnotations;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class UserManagementController : BaseController
    {
        private readonly IJobBookingDbFactory _JFactory;
        private readonly UserDBFactory _UFactory;
        private readonly QuotationDBFactory _QFactory;
        private string ApiUrl = ShipaConfiguration.ShipaApiUrl();
        private string IPAddress;
        public UserManagementController()
        {
            _JFactory = new JobBookingDbFactory();
            _UFactory = new UserDBFactory();
            _QFactory = new QuotationDBFactory();

        }

        [Route("api/UserManagement/PostSignUp")]
        [HttpPost]
        public HttpResponseMessage SignUp([FromBody]SignupModel request)
        {
            try
            {
                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];

                UserDBFactory UMF = new UserDBFactory();
                var oneSignalService = new OneSignal();

                string email = request.Email;
                UserEntity mDBEntity = UMF.GetUserDetails(email.ToString());
                if (mDBEntity.USERID == null)
                {
                    string EncryptedEmail = EncryptStringAES(request.Email);
                    request.Password = EncryptStringAES(request.Password);

                    TokenDetails token = GenerateToken("USER_ACTIVATION_TOKEN_EXPIRY");

                    mDBEntity.USERID = request.Email;
                    mDBEntity.FIRSTNAME = request.FirstName;
                    mDBEntity.LASTNAME = request.LastName;
                    mDBEntity.EMAILID = EncryptedEmail;
                    mDBEntity.PASSWORD = request.Password;
                    mDBEntity.ISDCODE = request.ISDCode;
                    mDBEntity.MOBILENUMBER = request.ContactNo;
                    mDBEntity.COMPANYNAME = request.CompanyName;
                    mDBEntity.TOKEN = token.Token;
                    mDBEntity.TOKENEXPIRY = token.TokenExpiry;
                    mDBEntity.NOTIFICATIONSUBSCRIPTION = request.NotificationSubscription;
                    mDBEntity.SHIPMENTPROCESS = request.ShipmentProcess;
                    mDBEntity.SHIPPINGEXPERIENCE = request.ShippingExperience;

                    mDBEntity.USERTYPE = 2;
                    mDBEntity.ISTEMPPASSWORD = 0;
                    mDBEntity.ACCOUNTSTATUS = 0;
                    mDBEntity.ISTERMSAGREED = 1;

                    mDBEntity.COUNTRYCODE = request.COUNTRYCODE;
                    mDBEntity.COUNTRYNAME = request.CountryName;
                    mDBEntity.COUNTRYID = request.UACountryId;
                    try
                    {
                        long Result = UMF.Save(mDBEntity);
                        #region code enable once Active Campaign API Enabled from Firewall
                        //if (Result == 1)
                        //{
                        //    var targetUrl = ApiUrl + "api/activecampaign/ShipaFreightSignUpData";
                        //    var obj = GetDataFromApi<UserEntity>(targetUrl, "POST", mDBEntity);
                        //}
                        #endregion
                        //Sending Push Notification 
                        var notData = new { NOTIFICATIONTYPEID = 5111, NOTIFICATIONCODE = "Profile Incomplete" };
                        oneSignalService.SendPushMessageByTag("Email", mDBEntity.USERID, "Profile Incomplete", notData);

                        // Generate the html link sent via email
                        string strUserId = EncryptStringAES(request.Email);
                        string strEncryptURL = EncryptStringAES("/");
                        string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                        string subject = "";

                        subject = "Account Activation for user " + request.Email + EnvironmentName;


                        //if (this.Url.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
                        //{
                        //    var secureUrlBuilder = new UriBuilder(this.Url.Request.RequestUri);
                        //    secureUrlBuilder.Scheme = Uri.UriSchemeHttps;
                        //    this.Url.Request.RequestUri = new Uri(secureUrlBuilder.ToString());
                        //}

                        // should now return https
                        //string resetLink = this.Url.Link("Default", new
                        //{
                        //    Controller = "UserManagement",
                        //    Action = "ActivateUser",
                        //    rt = token.Token,
                        //    UserId = strUserId,
                        //    ReturnUrl = strEncryptURL
                        //});

                        string appurl = System.Configuration.ConfigurationManager.AppSettings["APPURL"];

                        //string resetLink1 = appurl + "UserManagement/ActivateUser?rt=" + token.Token + "&UserId=" + strUserId + "&ReturnUrl =" + strEncryptURL;

                        var url = this.Url.Link("DefaultApi", new
                        {
                            Controller = "UserManagement",
                            Action = "ActivateUser",
                            rt = token.Token,
                            UserId = strUserId,
                            ReturnUrl = strEncryptURL
                        });

                        string urlparams = url.Split('?')[1];
                        string resetLink1 = appurl + "UserManagement/ActivateUser?" + urlparams.Split('&')[1] + "&" + urlparams.Split('&')[2] + "&" + urlparams.Split('&')[3];

                        string userName = mDBEntity.FIRSTNAME;
                        string body = createEmailBody(userName, resetLink1);

                        MailMessage message = new MailMessage();
                        message.To.Add(request.Email);
                        message.Subject = subject;
                        message.Body = body;
                        message.IsBodyHtml = true;
                        bool IsMailSent = SendMailNotification(message);

                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.OK,
                                Message = "Registration successful. Please activate your account by clicking on the activation link sent to your registered email." //return Success Message
                            })
                        };
                    }
                    catch (Exception ex)
                    {

                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.ExpectationFailed,
                                Message = ex.Message.ToString() //return exception
                            })
                        };

                    }
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.NotAcceptable,
                            Message = "User Already Exist!." //return exception
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "PostSignUp", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message
                    })
                };
            }
        }

        public bool SendMailNotification(MailMessage message)
        {
            bool IsMailSent = false;
            SmtpClient mySmtpClient = new SmtpClient();
            if (mySmtpClient.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory && mySmtpClient.PickupDirectoryLocation.StartsWith("~"))
            {
                string root = AppDomain.CurrentDomain.BaseDirectory;
                string pickupRoot = mySmtpClient.PickupDirectoryLocation.Replace("~/", root);
                pickupRoot = pickupRoot.Replace("/", @"\");
                mySmtpClient.PickupDirectoryLocation = pickupRoot;
            }
            try
            {
                mySmtpClient.Send(message);
                IsMailSent = true;
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "SendMailNotification", ex.Message.ToString(), ex.StackTrace.ToString());
                ModelState.AddModelError("", "Issue sending email: " + ex.Message);
            }
            return IsMailSent;
        }

        [Route("api/UserManagement/Login")]
        [HttpPost]
        public HttpResponseMessage Login([FromBody]dynamic request)
        {
            try
            {
                bool IsLoginSuccess = false;

                if (request.Email.Value != "" && request.Password.Value != "")
                {
                    ReturnValues Obj = CheckUserValidatoins(Convert.ToString(request.Email.Value), Convert.ToString(request.Password.Value));

                    IsLoginSuccess = Obj.IsLoginSuccess;

                    if (IsLoginSuccess == true)
                    {
                        IPAddress = GetIPaddress();
                        _QFactory.InsertUserTrackDetails(request.Email.Value, IPAddress, System.Configuration.ConfigurationManager.AppSettings["ServerName"]);

                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.OK,
                                Message = "Logged In Successfully!."
                            })
                        };
                    }
                    else
                    {
                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.ExpectationFailed,
                                Message = Obj.Message
                            })
                        };
                    }
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.NotAcceptable,
                            Message = "UserName and Password should not be empty!."
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "Login", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message
                    })
                };
            }
        }

        private ReturnValues CheckUserValidatoins(string UserID, string Password)
        {
            ReturnValues response = new ReturnValues();
            UserEntity mDBEntity = _UFactory.GetUserLoginInfo(UserID);

            if (string.IsNullOrEmpty(mDBEntity.USERID))
            {
                response.Message = "Couldn't find your Shipa Freight account, Please signup.";
                return response;
            }
            if (mDBEntity.ACCOUNTSTATUS == 0)
            {
                response.Message = "Account is Not Activated, Please click on activation link sent in mail.";
                return response;
            }

            if (mDBEntity.FAILEDPASSWORDATTEMPT >= 5)
            {
                response.Message = @"Your account has been locked out because of 5 or more unsuccessfull login attempts. 
                                     Please Reset your Password or contact service desk";
                return response;
            }
            if (mDBEntity.ACCOUNTSTATUS == 2)
            {
                response.Message = "Your account has been deactivated, Please contact Shipa Freight help desk.";
                return response;
            }

            return VerifyUserIDandPasswordareCorrect(mDBEntity, Password, response);

        }

        private ReturnValues VerifyUserIDandPasswordareCorrect(UserEntity mDBEntity, string Password, ReturnValues response)
        {
            string OriginalPassword = DecryptStringAES(mDBEntity.PASSWORD);
            string InstantPassword = string.Empty;

            bool timecheck = InstantPasswordTimeExpiryCalculation(mDBEntity.ServerDate.ToString(), mDBEntity.InstTime.ToString());

            if (!string.IsNullOrEmpty(mDBEntity.InstantPASSWORD))
                InstantPassword = DecryptStringAES(mDBEntity.InstantPASSWORD);

            if (OriginalPassword == Password)
            {
                response.IsLoginSuccess = true;
                _UFactory.InsertFailedPasswordAttemtsCount(mDBEntity.USERID, "True");
            }
            else if (InstantPassword == Password && timecheck == true)
            {
                response.IsLoginSuccess = true;
                _UFactory.InsertFailedPasswordAttemtsCount(mDBEntity.USERID, "True");
            }
            else
            {
                response.Message = "Invalid Email Id or Password";
                _UFactory.InsertFailedPasswordAttemtsCount(mDBEntity.USERID, "False");
            }

            return response;
        }

        private bool InstantPasswordTimeExpiryCalculation(string ServerDate, string InstTime)
        {
            var start = DateTime.Parse(ServerDate.ToString());
            var oldDate = DateTime.Parse(InstTime.ToString());
            if (start.Subtract(oldDate) >= TimeSpan.FromMinutes(30)) //30 minutes were passed from start
                return false;
            else
                return true;
        }
        private ReturnValues ValidateUser(string Email, string Password)
        {
            //string strUserId = AESEncrytDecry.DecryptStringAES(Email);

            UserDBFactory men = new UserDBFactory();

            UserEntity mDBEntity = men.GetUserDetails(Email);
            ReturnValues obj = new ReturnValues();

            if (mDBEntity.USERID == null) { obj.Message = "Invalid Email Id or Password."; return obj; }

            string strPassword = DecryptStringAES(mDBEntity.PASSWORD);

            if ((strPassword != Password) || (mDBEntity.ACCOUNTSTATUS == 0) || (mDBEntity.FAILEDPASSWORDATTEMPT >= 5))
            {
                obj.IsLoginSuccess = false;
                if (mDBEntity.FAILEDPASSWORDATTEMPT >= 5)
                {
                    obj.Message = "Your account has been locked out because of 5 or more unsuccessfull login attempts. Please Reset your Password or contact service desk";
                }
                else if (strPassword != Password)
                {
                    men.InsertFailedPasswordAttemtsCount(Email, "False");
                    obj.Message = "Invalid Email Id or Password.";
                    // TempData["TosterMsg"] = "Invalid Email Id or Password.";
                }
                else
                    obj.Message = "Account is Not Activated, Please click on activation link sent in mail.";
            }
            else
            {
                if (mDBEntity.FAILEDPASSWORDATTEMPT > 0) { men.InsertFailedPasswordAttemtsCount(Email, "True"); }

                obj.IsLoginSuccess = true;
                System.Web.Security.FormsAuthentication.SetAuthCookie(Email, true);
            }
            return obj;
        }
        private string GetIPaddress()
        {

            if (Dns.GetHostAddresses(Dns.GetHostName()).Length > 0)
                return Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();

            return "";
        }

        [Route("api/UserManagement/TestAuthorized")]
        [HttpPost]
        [Authorize]
        public dynamic TestAuthorized()
        {
            return "Logged in as " + HttpContext.Current.User.Identity.Name;
        }

        [Route("api/UserManagement/ForgotPassword")]
        [HttpPost]
        public HttpResponseMessage ForgotPassword([FromBody]dynamic request)
        {
            try
            {

                if (request.Email.Value == "")
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Email should not be empty."
                        })
                    };
                }
                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];

                bool IsMailSent = false; string ResultMsg = string.Empty;
                TokenDetails T_obj = GenerateToken("PASSWORD_RESET_TOKEN_EXPIRY");

                UserDBFactory mFactory = new UserDBFactory();
                UserEntity mDBEntity = mFactory.GetUserDetails(request.Email.Value);

                string strUserId = EncryptStringAES(request.Email.Value);

                if (mDBEntity.USERID == null || mDBEntity.ACCOUNTSTATUS == 0)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Please provide valid credentials."
                        })
                    };
                }
                //else if (mDBEntity.ACCOUNTSTATUS == 0)
                //{
                //    ResultMsg = "User is not activated, Please activate using activation link sent in mail.";

                //    return new HttpResponseMessage()
                //    {
                //        Content = new JsonContent(new
                //        {
                //            StatusCode = HttpStatusCode.ExpectationFailed,
                //            Message = "User is not activated, Please activate using activation link sent in mail."
                //        })
                //    };
                //}
                else
                {
                    mDBEntity.TOKEN = T_obj.Token;
                    mDBEntity.TOKENEXPIRY = T_obj.TokenExpiry;
                    mFactory.UpdateUserToken(mDBEntity);
                    string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                    string subject = "";
                    subject = "Reset your password for " + request.Email.Value + EnvironmentName;

                    string userName = mDBEntity.FIRSTNAME;// +" " + mDBEntity.LASTNAME;
                    string resetLink = this.Url.Link("Default", new { Controller = "UserManagement", Action = "ResetPassword", rt = T_obj.Token, UserId = strUserId });
                    resetLink = Actionlink + "UserManagement/ResetPassword?" + resetLink.Split('?')[1];

                    string body = string.Empty;

                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/Reset-Password-template.html")))
                    { body = reader.ReadToEnd(); }

                    body = body.Replace("{user}", ((string.IsNullOrEmpty(userName) != true) ? userName : string.Empty));
                    body = body.Replace("{url}", resetLink);
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                    MailMessage message = new MailMessage();
                    message.To.Add(request.Email.Value);
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;
                    IsMailSent = SendMailNotification(message);

                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Reset password email has been sent to " + userName + "."
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "ForgotPassword", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        /// <summary>
        /// This is for new front end, which depends on referer url instead of server url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("api/UserManagement/ForgotPassword/v2")]
        [HttpPost]
        public HttpResponseMessage ForgotPasswordV2([FromBody]UserModel request)
        {
            try
            {

                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                if (HttpContext.Current.Request.UrlReferrer == null) return ErrorResponse("Referer is required");
                string Actionlink = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                TokenDetails T_obj = GenerateToken("PASSWORD_RESET_TOKEN_EXPIRY");
                UserDBFactory mFactory = new UserDBFactory();
                UserEntity mDBEntity = mFactory.GetUserDetails(request.UserId);

                if (mDBEntity.USERID == null)
                {
                    return BaseResponse("User Does not exist, Please register", HttpStatusCode.ExpectationFailed);
                }
                else if (mDBEntity.ACCOUNTSTATUS == 0)
                {
                    return BaseResponse("Account Disabled, Please contact support", HttpStatusCode.ExpectationFailed);
                }
                else
                {
                    mDBEntity.TOKEN = T_obj.Token;
                    mDBEntity.TOKENEXPIRY = T_obj.TokenExpiry;
                    mFactory.UpdateUserToken(mDBEntity);
                    string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                    string subject = "";
                    subject = "Reset your password for " + request.UserId + EnvironmentName;
                    string strUserId = EncryptStringAES(request.UserId);
                    string userName = mDBEntity.FIRSTNAME;
                    var rlink = Actionlink + "/reset-password?token=" + T_obj.Token + "&user=" + request.UserId;
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/Reset-Password-template.html")))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{user}", ((string.IsNullOrEmpty(userName) != true) ? userName : string.Empty));
                    body = body.Replace("{url}", rlink);
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                    MailMessage message = new MailMessage();
                    message.To.Add(request.UserId);
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;
                    SendMailNotification(message);
                    return BaseResponse("Reset password email has been sent to " + userName + ".", HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "ForgotPasswordV2", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }


        [Route("api/UserManagement/ValidateToken")]
        [HttpPost]
        public HttpResponseMessage ValidateTokenForgot([FromBody]ResetPasswordModel request)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                UserDBFactory mFactory = new UserDBFactory();
                byte[] data = Convert.FromBase64String(request.Token);
                UserEntity mDBEntity = mFactory.GetUserDetails(request.UserId);
                if (DateTime.UtcNow < mDBEntity.TOKENEXPIRY && request.Token == mDBEntity.TOKEN)
                {
                    if (request.Password == null) return BaseResponse("Valid Token, Please set your new password", HttpStatusCode.OK);
                    string strPassword = EncryptStringAES(request.Password);
                    mFactory.ResetPassword(request.UserId, strPassword);
                    return BaseResponse("Password changed successfully", HttpStatusCode.OK);
                }
                else
                {
                    return ErrorResponse("Token Expired");
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "ValidateTokenForgot", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/UserManagement/ChangePassword")]
        [HttpPost]
        public HttpResponseMessage ChangePassword([FromBody]ChangePasswordModel request)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                if (!isValidPassword(request.Password))
                {
                    return ErrorResponse(@"Enter password that must be At least 8 characters long, 
                                        Must contain a number, 
                                        Must contain one capital letter and one small letter, 
                                        Special characters are not allowed");
                }
                UserDBFactory mFactory = new UserDBFactory();
                UserEntity mDBEntity = mFactory.GetUserDetails(request.UserId);
                if (mDBEntity.ACCOUNTSTATUS == 0) return ErrorResponse("Account is inactive, please contact customer care.");
                string oldPassword = DecryptStringAES(mDBEntity.PASSWORD);
                if (oldPassword != request.OldPassword)
                {
                    return ErrorResponse("Old Password is incorrect");
                }
                string strPassword = EncryptStringAES(request.Password);
                mFactory.ResetPassword(request.UserId, strPassword);
                return BaseResponse("Password changed successfully", HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "ChangePassword", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/UserManagement/ActivateUser")]
        [HttpGet]
        public HttpResponseMessage ActivateUser(string rt, string UserId, string ReturnUrl)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                bool resetResponse = false; byte[] data = null; string strUserId = string.Empty; string strDecryptURL = string.Empty;
                try
                {
                    data = Convert.FromBase64String(rt);
                }
                catch (Exception ex)
                {
                    LogError("UserManagement", "ActivateUser", ex.Message.ToString(), ex.StackTrace.ToString());
                    return ErrorResponse("Error in activation url, Please contact Shipa freight help desk.");
                }

                DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
                UserDBFactory mFactory = new UserDBFactory();
                try
                {
                    strUserId = APIDynamicClass.DecryptStringAES(UserId);
                    strDecryptURL = string.IsNullOrEmpty(ReturnUrl) ? null : APIDynamicClass.DecryptStringAES(ReturnUrl);
                }
                catch (Exception ex)
                {
                    LogError("UserManagement", "ActivateUser", ex.Message.ToString(), ex.StackTrace.ToString());
                    return ErrorResponse("Error in activation url, Please contact Shipa freight help desk.");
                }

                UserEntity mDBEntity = mFactory.GetUserDetails(strUserId);
                if (mDBEntity.ACCOUNTSTATUS == 0)
                {
                    if (mDBEntity.TOKEN == rt)
                    {
                        if (when < mDBEntity.TOKENEXPIRY && rt == mDBEntity.TOKEN)
                        {
                            resetResponse = true;
                        }
                        if (resetResponse)
                        {
                            mFactory.ActivateUserAccountStatus(strUserId);
                            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                            string subject = "";
                            subject = "Your Shipa Freight account is now active" + EnvironmentName;
                            string body = string.Empty;
                            string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/account-active-temp.html");
                            using (StreamReader reader = new StreamReader(mapPath))
                            { body = reader.ReadToEnd(); }
                            body = body.Replace("{user}", mDBEntity.FIRSTNAME.ToString());
                            body = body.Replace("{appurl}", Actionlink);
                            body = body.Replace("{profilelink}", Actionlink + "/UserManagement/MyProfile");
                            body = body.Replace("{creditlink}", Actionlink + "/UserManagement/Credit");
                            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                            body = body.Replace("{videolink}", Actionlink + "/videos");
                            SendMail(mDBEntity.USERID, body, subject, "ActiveUser", UserId, true);
                            IPAddress = GetIPaddress();
                            _QFactory.InsertUserTrackDetails(mDBEntity.USERID, IPAddress, System.Configuration.ConfigurationManager.AppSettings["ServerName"]);
                        }
                        else
                        {
                            return ErrorResponse("Error While activating the user. Token is Expired");
                        }
                        return BaseResponse(new { status = "activated", redirecturl = strDecryptURL }, HttpStatusCode.OK);
                    }
                    return ErrorResponse("Error While activating the user. Token is Invalid");
                }
                return ErrorResponse("This user ID is already activated. Please log in.");
            });
        }

        public void SendMail(string Email, string body, string subject, string submodule, string referenceno, bool IsRequired)
        {
            MailMessage message = new MailMessage();
            string to = string.Empty;
            if (Email.Contains(';'))
            {
                for (int i = 0; i <= (Email.Split(';').Length - 1); i++)
                {
                    message.To.Add(new MailAddress(Email.Split(';')[i]));
                    to = Email.Split(';')[i] + ",";
                }
            }
            else
            {
                message.To.Add(new MailAddress(Email));
                to = Email;
            }

            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", to, "", "",
                "UserManagement", submodule, referenceno, Email, body, "", IsRequired, message.Subject.ToString());
        }

        #region Support

        [Route("api/UserManagement/GetSupportRequests")]
        [HttpPost]
        public dynamic GetSupportRequests([FromBody]SupportRequestsEnv Model)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Model.TabStatus)))
                {
                    switch (Convert.ToString(Model.TabStatus).ToUpper())
                    {
                        case "CLOSED":
                            Model.TabStatus = "Closed";
                            break;
                        case "PENDING REVIEW":
                            Model.TabStatus = "Pending Review";
                            break;
                        case "SUBMITTED":
                            Model.TabStatus = "Submitted";
                            break;
                    }
                }

                UserDBFactory mFactory = new UserDBFactory();
                UserProfileEntity mDBEntity = mFactory.GetSupportRequests(Convert.ToString(Model.Email), Convert.ToString(Model.TabStatus), Convert.ToString(Model.SearchString), Convert.ToInt32(Model.PageNo));
                Model = Mapper.Map<SupportRequestsEnv>(mDBEntity);
                Model.All = Model.PREVIEW + Model.Submitted + Model.Closed;
                return Model;
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportRequests", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/SaveSupportRequests")]
        [HttpPost]
        public dynamic SaveSupportRequests([FromBody]SaveTicketModel Model)
        {
            try
            {
                UserDBFactory dbfac = new UserDBFactory();
                string reqSeqNo = dbfac.SaveTicket(Model.Module, Model.Type, Model.ReferenceID, Model.Query, Model.Description, Model.Email, Model.ModuleCode);
                // Upload files
                if (!string.IsNullOrEmpty(Model.stringInBase64))
                {
                    byte[] fileBytes = System.Convert.FromBase64String(Convert.ToString(Model.stringInBase64));
                    dbfac.SaveTicketDocuemnts(Model.ReferenceID, Model.Email, reqSeqNo, fileBytes, Model.FileName, "", "Request");
                }
                // Send Email to Customer Support 
                SendMailFocisCustSupport("GSSC Team", System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"], "ShipAfreight - Customer Support Request (" + reqSeqNo + ")", Convert.ToString(Model.Query), reqSeqNo);

                string Message = "Customer support Request has been done by the user : " + Model.Email;

                SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + reqSeqNo + ")", Message, reqSeqNo, "", "CUSTOMERSUPPORT", Model.Email); //string User,string Email,string Subject,string Description,string RefNumber

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Support Request Created Successfully",
                        REQID = reqSeqNo
                    })
                };

            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "SaveSupportRequests", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/GetSupportRequestChat")]
        [HttpPost]
        public dynamic GetSupportRequestChat([FromBody]dynamic Req)
        {
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                SupportEntity mDBEntity = mFactory.GetSupportChat(Convert.ToString(Req.REQID));
                var res = mDBEntity.Chat;

                var d = res.Where(o => o.REQID != 1234)
                        .Where(o => o.MESSAGE != null && o.MESSAGE != "" || o.UPLOADEDFROM == "Communication")
                        .OrderBy(o => o.DATECREATED).ToList();

                return d;
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportRequestChat", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }


        [Route("api/UserManagement/GetSupportRequestChatNew")]
        [HttpPost]
        public dynamic GetSupportRequestChatNew([FromBody]dynamic Req)
        {
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                SupportEntity mDBEntity = mFactory.GetSupportChat(Convert.ToString(Req.REQID));
                var res = mDBEntity.Chat;

                var d = res.Where(o => o.REQID != 1234)
                        .Where(o => o.MESSAGE != null && o.MESSAGE != "" || o.UPLOADEDFROM == "Communication")
                        .OrderBy(o => o.DATECREATED).ToList();

                var d2 = res.Where(o => o.REQID != 1234)
                            .Where(o => (o.MESSAGE == null || o.MESSAGE == "") && (o.FILENAME != null && o.FILENAME != "") && o.UPLOADEDFROM == "Request")
                            .OrderBy(o => o.DATECREATED).FirstOrDefault();
                var ress = new ChatEnvelope()
                {
                    Chat = d,
                    FILENAME = d2 == null ? "" : d2.FILENAME,
                    DOCREQID = d2 == null ? "" : d2.DOCREQID.ToString()
                };

                return ress;
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportRequestChatNew", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        public class ChatEnvelope
        {
            public IList<Chat> Chat { get; set; }
            public string FILENAME { get; set; }
            public string DOCREQID { get; set; }
        }

        [Route("api/UserManagement/SaveChat")]
        [HttpPost]
        public dynamic SaveChat([FromBody]dynamic Req)
        {
            try
            {
                UserDBFactory DB = new UserDBFactory();
                if (Req.stringInBase64 != null && Req.stringInBase64.Value != "")
                {
                    byte[] fileBytes = System.Convert.FromBase64String(Convert.ToString(Req.stringInBase64));
                    DB.SaveTicketDocuemnts("", Convert.ToString(Req.Email), Req.SEQNO.Value, fileBytes, Convert.ToString(Req.FileName), Convert.ToString(Req.ChatMes), "Communication");
                }
                else
                {
                    DB.SaveOrCloseChat(Convert.ToString(Req.ChatMes), Convert.ToString(Req.REQID), Convert.ToString(Req.Email), 0);
                }
                // send Mails
                SendMailForGSSCQuotes(Convert.ToString(Req.SEQNO), Convert.ToString(Req.Email));
                SendMailFocisCustSupport("GSSC Team", "SSPCustomerSupport,SSPCountryCustomerSupport", "ShipAfreight - Customer Support Request (" + Convert.ToString(Req.SEQNO) + ")", Convert.ToString(Req.ChatMes), Convert.ToString(Req.SEQNO), "", "CUSTOMERSUPPORT", Convert.ToString(Req.Email));
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Success"
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "SaveChat", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/CloseChat")]
        [HttpPost]
        public dynamic CloseChat([FromBody]dynamic Req)
        {
            try
            {
                UserDBFactory DB = new UserDBFactory();
                DB.SaveOrCloseChat("", Convert.ToString(Req.REQID), Convert.ToString(Req.Email), 1);
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Success"
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "CloseChat", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/DownloadSupportDocs")]
        [HttpPost]
        public dynamic DownloadSupportDocs([FromBody]dynamic Req)
        {
            try
            {
                if (Req.DOCREQID == null || Req.DOCREQID == "") return BaseResponse(new { Error = "DOCREQID IS REQUIRED" }, HttpStatusCode.ExpectationFailed);
                UserDBFactory mFactory = new UserDBFactory();
                DataSet ds = mFactory.GetbytesDownloadAttchment(Convert.ToInt32(Req.DOCREQID));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var fd = new
                    {
                        FILENAME = ds.Tables[0].Rows[0]["FILENAME"],
                        FILEEXTENSION = ds.Tables[0].Rows[0]["FILENAME"].ToString().Split('.').LastOrDefault(),
                        FILECONTENT = ds.Tables[0].Rows[0]["FILECONTENT"]
                    };
                    var res = BaseResponse(fd, HttpStatusCode.OK);
                    return res;
                }
                else
                {
                    return BaseResponse(new { Error = "Not Found" }, HttpStatusCode.ExpectationFailed);
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "DownloadSupportDocs", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse(new { Error = "Something Went wrong" }, HttpStatusCode.ExpectationFailed);
            }
        }

        [Route("api/UserManagement/GetQueryTypeForContact")]
        [HttpGet]
        public dynamic GetQueryTypeForContact()
        {
            var res = new
            {
                StatusCode = 200,
                Data = new object[]{
                    new { QueryType = "Air, ocean or road freight" },
                    new { QueryType = "Warehousing" },
                    new { QueryType = "Customs clearance" },
                    new { QueryType = "Supply chain solutions" },
                    new { QueryType = "Chemical logistics" },
                    new { QueryType = "Fairs & events logistics" },
                    new { QueryType = "Project/heavy-lift logistics" },
                    new { QueryType = "Other" }
                }
            };
            return res;
        }

        [Route("api/UserManagement/QueryInformation")]
        [HttpPost]
        public dynamic QueryInformation([FromBody]QueryInformationModel QueryInformation)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            try
            {
                int QueryId = mFactory.SaveQueryInformation(QueryInformation);
                SendMailForContactUs(QueryInformation);
                return BaseResponse(new { Message = "Your request has been received and we will respond to you shortly." }, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "QueryInformation", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse(new { Error = "Something Went wrong" }, HttpStatusCode.ExpectationFailed);
            }
        }

        [Route("api/UserManagement/GetSupportCategories_MultiLanguage")]
        [HttpGet]
        public dynamic GetSupportCategories_MultiLanguage(int CategoryID, string uiCulture)
        {
            try
            {
                UserDBFactory UD = new UserDBFactory();

                ResultsEntity<CustomerSupportEntity> mFactoryEntity = UD.GetSupportCategories(CategoryID, uiCulture.ToLower());

                return Ok(mFactoryEntity);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportCategories_MultiLanguage", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/UserManagement/GetSupportSubCategories_MultiLanguage")]
        [HttpGet]
        public dynamic GetSupportSubCategories_MultiLanguage(int CategoryID, string uiCulture)
        {
            try
            {
                UserDBFactory UD = new UserDBFactory();

                ResultsEntity<CustomerSupportEntity> mFactoryEntity = UD.GetSupportCategories(Convert.ToInt32(CategoryID), uiCulture.ToLower());

                return Ok(mFactoryEntity);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportSubCategories_MultiLanguage", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/GetSupportCategories")]
        [HttpGet]
        public dynamic GetSupportCategories()
        {
            try
            {
                UserDBFactory UD = new UserDBFactory();

                ResultsEntity<CustomerSupportEntity> mFactoryEntity = UD.GetSupportCategories(0);

                return Ok(mFactoryEntity);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportCategories", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/UserManagement/GetSupportSubCategories")]
        [HttpGet]
        public dynamic GetSupportCategories(string CategoryID)
        {
            try
            {
                UserDBFactory UD = new UserDBFactory();

                ResultsEntity<CustomerSupportEntity> mFactoryEntity = UD.GetSupportCategories(Convert.ToInt32(CategoryID));

                return Ok(mFactoryEntity);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetSupportCategories", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }
        #endregion

        #region Notifications

        [Route("api/UserManagement/GetNotifications")]
        [HttpPost]
        public dynamic GetNotifications([FromBody]dynamic Req)
        {
            try
            {
                if (Req.Email == null) return Request.CreateResponse(HttpStatusCode.NoContent);
                UserDBFactory mFactory = new UserDBFactory();
                List<DBFactory.Entities.NotificationEntity> mDBEntity = mFactory.GetNotificationDetails(Convert.ToString(Req.Email));
                var Count = mFactory.ShowNotificationCount(Convert.ToString(Req.Email));
                var response = new
                {
                    StatusCode = HttpStatusCode.OK,
                    Count = Count,
                    Notifications = mDBEntity
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetNotifications", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/DeleteNotification")]
        [HttpPost]
        public dynamic DeleteNotification([FromBody]dynamic Req)
        {
            try
            {
                if (Req.Id == null) return Request.CreateResponse(HttpStatusCode.NoContent);
                UserDBFactory mFactory = new UserDBFactory();
                string Result = mFactory.DeleteNotification(Convert.ToInt32(Req.Id));
                var res = new { Message = "Notification Deleted." };
                return BaseResponse(res, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "DeleteNotification", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/NotificationsMark")]
        [HttpPost]
        public dynamic NotificationsMark([FromBody]dynamic Req)
        {
            try
            {
                if (Req.Id == null) return Request.CreateResponse(HttpStatusCode.NoContent);
                UserDBFactory mFactory = new UserDBFactory();
                string Result = mFactory.NotificationsMark(Convert.ToInt32(Req.Id));
                var res = new { Message = "Notification Marked" };
                return BaseResponse(res, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "NotificationsMark", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/NotificationsCount")]
        [HttpPost]
        public dynamic NotificationsCount([FromBody]dynamic Req)
        {
            try
            {
                if (Req.Email == null) return Request.CreateResponse(HttpStatusCode.NoContent);
                UserDBFactory mFactory = new UserDBFactory();
                var Result = mFactory.ShowNotificationCount(Convert.ToString(Req.Email));
                return BaseResponse(Result, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "NotificationsCount", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/SendPushByEmail")]
        [HttpPost]
        public dynamic SendPushByEmail([FromBody]dynamic Req)
        {
            try
            {
                var OneSignalService = new OneSignal();
                OneSignalService.SendPushMessageByTag("Email", Convert.ToString(Req.USERID.Value), Convert.ToString(Req.NOTIFICATIONCODE.Value), Req);
                return BaseResponse(new { Message = "Success" }, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "SendPushByEmail", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse(new { Error = "Something Went Wrong" }, HttpStatusCode.ExpectationFailed);
            }
        }

        #endregion

        #region Additional credit

        [Route("api/UserManagement/GetCredit")]
        [HttpPost]
        public dynamic GetCredit([FromBody]UserModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    UserDBFactory mFactory = new UserDBFactory();
                    CreditEnvelope Response = new CreditEnvelope();
                    Response.CREDITAPPLICATION = mFactory.GetMBCreditProfileNew(Req.UserId, 2);
                    if (Response.CREDITAPPLICATION.CREDITAPPID != 0)
                    {
                        Response.ADDITIONALCREDITAPPLICATION = mFactory.GetAddCreditLimit(Req.UserId, Convert.ToInt32(Response.CREDITAPPLICATION.CREDITAPPID));
                        if (Response.ADDITIONALCREDITAPPLICATION.ADDITIONALCREDITID != 0)
                        {
                            Response.ADDITIONALCREDITAPPLICATION.CreditComments = Response.ADDITIONALCREDITAPPLICATION
                                                        .CreditComments
                                                        .Where(o => o.ADDITIONALCREDITID == Response.ADDITIONALCREDITAPPLICATION.ADDITIONALCREDITID)
                                                        .ToList();
                        }
                        else
                        {
                            Response.ADDITIONALCREDITAPPLICATION = null;
                        }
                    }
                    return BaseResponse(Response, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetCredit", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/RequestAdditionalCreditLimit")]
        [HttpPost]
        public dynamic AdditionalCreditLimit([FromBody]AdditionalCreditModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    UserDBFactory mFactory = new UserDBFactory();
                    DBFactory.Entities.UserProfileEntity mDBEntity = mFactory.GetUserProfileDetails(Req.UserId, 2);
                    UserProfileModel mUIModel = Mapper.Map<UserProfileEntity, UserProfileModel>(mDBEntity);
                    mUIModel.CreditDoc = null;
                    mUIModel.CreditComments = null;
                    mUIModel.INDUSTRYID = Req.INDUSTRYID;
                    mUIModel.NOOFEMP = Convert.ToInt32(Req.NOOFEMP);
                    mUIModel.PARENTCOMPANY = Req.PARENTCOMPANY;
                    mUIModel.ANNUALTURNOVER = Req.ANNUALTURNOVER;
                    mUIModel.DBNUMBER = Req.DBNUMBER;
                    mUIModel.HFMENTRYCODE = Req.HFMENTRYCODE;
                    mUIModel.CURRENCYID = Req.CURRENCYID;
                    mUIModel.COMMENTS = Req.COMMENTS;
                    mUIModel.REQUESTEDCREDITLIMIT = Req.REQUESTEDCREDITLIMIT;
                    mUIModel.CURRENTPAYTERMSNAME = Req.CURRENTPAYTERMSNAME;
                    mUIModel.EXISTINGCUSTOMER = Req.EXISTINGCUSTOMER;
                    mUIModel.USEMYCREIDT = Req.USEMYCREIDT;
                    UserProfileEntity Userobj = Mapper.Map<UserProfileModel, UserProfileEntity>(mUIModel);
                    long? addcreditID = mFactory.UpdateAdditionalCredit(Userobj);

                    List<CreditCommentEntity> creditcommentList = mDBEntity.CreditComments.Where(x => x.ADDITIONALCREDITID == addcreditID).ToList();
                    if (creditcommentList.Count == 0)
                    {
                        if (addcreditID != null)
                        {
                            CreditCommentEntity objCreditComment = new CreditCommentEntity();
                            objCreditComment.ADDITIONALCREDITID = addcreditID;
                            objCreditComment.CREDITAPPID = mUIModel.CREDITAPPID;
                            objCreditComment.COMMENTS = mUIModel.COMMENTS;
                            objCreditComment.COMMENTSFROM = "SHIPA";
                            objCreditComment.CREATEDBY = Req.UserId;
                            mFactory.SaveCreditComments(objCreditComment);
                        }
                    }
                    if (mUIModel.CountryId != 0)
                    {
                        SendMailtoCountryFinance(mUIModel.CountryId, "Additional", Req.UserId);
                    }
                    if (mUIModel.EXISTINGCUSTOMER == true && mUIModel.USEMYCREIDT == true)
                    {
                        SendMailToGSSCforExistingCustomer(mUIModel, Req.UserId);
                    }
                    return BaseResponse(new { Message = "Your business credit request has been received. We will notify you when your credit is approved." }, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "AdditionalCreditLimit", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/AddCreditUploadDocuments")]
        [HttpPost]
        public dynamic AddCreditUploadDocuments([FromBody]UploadDocModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    long docid = 0;
                    byte[] fileBytes = System.Convert.FromBase64String(Req.stringInBase64);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                    sspdoc.CREATEDBY = Req.UserId;
                    sspdoc.FILENAME = Req.FileName;
                    sspdoc.FILEEXTENSION = Req.ContentType;
                    sspdoc.FILESIZE = Req.ContentLength;
                    sspdoc.FILECONTENT = fileBytes;
                    sspdoc.DocumentName = Req.FileName;
                    sspdoc.DOCUMNETTYPE = Req.FileName;
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    UserDBFactory creditFactory = new UserDBFactory();
                    docid = creditFactory.AddCreditSaveDocumentsWithParams(docentity, Req.UserId, "AdditionalCreditLimit", "NO");
                    return BaseResponse(new { DOCID = docid }, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "AddCreditUploadDocuments", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/DeleteAddcreditdoc")]
        [HttpPost]
        public dynamic DeleteAddcreditdoc([FromBody]DeleteDocModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    UserDBFactory mFactory = new UserDBFactory();
                    mFactory.DeleteAddCreditDocument(Req.UserId, Req.DOCID);
                    return BaseResponse(new { Message = "Success" }, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "DeleteAddcreditdoc", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/UserManagement/AddCreditChat")]
        [HttpPost]
        public dynamic _AddCreditChat([FromBody]AddCreditChatModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    UserDBFactory mFactory = new UserDBFactory();
                    CreditCommentEntity objCreditComment = new CreditCommentEntity();
                    objCreditComment.CREDITAPPID = Req.CREDITAPPID;
                    objCreditComment.ADDITIONALCREDITID = Req.ADDITIONALCREDITID;
                    objCreditComment.COMMENTS = Req.COMMENTS;
                    objCreditComment.COMMENTSFROM = "SHIPA";
                    objCreditComment.CREATEDBY = Req.UserId;
                    mFactory.SaveCreditComments(objCreditComment);
                    return BaseResponse(new { Message = "Success" }, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "AddCreditChat", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }
        public void SendMailToGSSCforExistingCustomer(UserProfileModel model, string user)
        {
            SafeRun<dynamic>(() =>
            {
                string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
                string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
                string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(Email));
                message.To.Add(new MailAddress(help));
                message.Subject = "Shipa Freight - Request for known customer";
                message.IsBodyHtml = true;
                string body = string.Empty;

                string Bodytext = "<b>Dear GSSC team</b>, <br/> <br/>           A user (" + model.UserId + ") has updated the profile mentioning as existing customer and has authorized to use the present credit limit setup for his account. Please check with country finance (" + "<b>" + model.CountryName + "</b>" + ") and update their credit limit from the user section.";
                body = Bodytext;

                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
                "UserManagement", "Existing customer", model.UserId, user, body, "", false, message.Subject.ToString());
                return null;
            });
        }

        #endregion

        [Route("api/checkappversion")]
        [HttpGet]
        public dynamic GETAPPVERSIONS()
        {
            try
            {
                return SafeRun<dynamic>(() =>
                {
                    UserDBFactory mFactory = new UserDBFactory();
                    List<SSPMOBILEVERSIONSENTITY> data = mFactory.GETAPPVERSIONS();
                    return BaseResponse(data, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GETAPPVERSIONS", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/GetExceptions")]
        [HttpGet]
        public HttpResponseMessage GetExceptions(int count = 5)
        {
            try
            {
                return SafeRun<HttpResponseMessage>(() =>
                {
                    IEnumerable<string> values = new List<string>();
                    this.Request.Headers.TryGetValues("x_source", out values);
                    if (values == null || values.ToArray()[0] != "agility") return ErrorResponse();
                    QuotationDBFactory mFactory = new QuotationDBFactory();
                    var d = mFactory.GetErrorLog(count);
                    return BaseResponse(d, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetExceptions", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        #region Profile Credit DashBoard

        [Route("api/UserManagement/GetProfileCreidtData")]
        [HttpPost]
        public dynamic GetProfileCreidtData([FromBody]dynamic request)
        {
            try
            {
                UserDBFactory mFactory = new UserDBFactory();

                if (Convert.ToInt32(request.TabStatus) == 1)
                {
                    MBPROFILE mDBEntity = mFactory.GetMBUserProfile(Convert.ToString(request.CreatedBy), Convert.ToInt32(request.TabStatus));
                    if (mDBEntity.BOOKINGCOUNT == 0)
                    {
                        mDBEntity.QUOTECOUNT = mFactory.GetQuotationCount(Convert.ToString(request.CreatedBy));
                    }
                    return Ok(mDBEntity);
                }
                else
                {
                    DBFactory.Entities.MBCREDIT mDBEntity = new DBFactory.Entities.MBCREDIT();
                    mDBEntity = mFactory.GetMBCreditProfile(Convert.ToString(request.CreatedBy), Convert.ToInt32(request.TabStatus));
                    return Ok(mDBEntity);
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetProfileCreidtData", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/UserManagement/GetDashboardData")]
        [HttpPost]
        public dynamic GetDashboardData([FromBody]dynamic request)
        {
            try
            {
                if (request.CreatedBy == null)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Please provide your emailid."
                        })
                    };
                }

                TrackingDBFactory mFactory = new TrackingDBFactory();
                List<DashBoardEntity> trackentity = mFactory.GetDashBoardData(request.CreatedBy.ToString());
                DBMBModel mUIModel = new DBMBModel();

                foreach (var val in trackentity.Where(i => i.Status != "Credit"))
                {
                    mUIModel.BOOKINGSDONE = val.BOOKINGSDONE;
                    mUIModel.TOTALVOLUME = val.TOTALVOLUME;
                    mUIModel.TotalSpent = val.TotalSpent;
                }

                if (trackentity.Where(i => i.Status == "Credit").Count() > 0)
                {
                    mUIModel.CREDITLIMIT = trackentity.Where(i => i.Status == "Credit").FirstOrDefault().TotalSpent;
                }
                else
                {
                    mUIModel.CREDITLIMIT = 0;
                }

                UserDBFactory UDF = new UserDBFactory();
                ProfileInfo PInfo = UDF.GetProfileInfo(Convert.ToString(request.CreatedBy));
                // PInfo.ImgBase64 = Convert.ToBase64String(PInfo.IMGCONTENT);
                DashBoardList DBL = new DashBoardList();
                DBL.DBData = mUIModel;
                DBL.PINFO = PInfo;
                return Ok(DBL);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetDashboardData", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/UserManagement/GetOrSaveImage")]
        [HttpPost]
        public dynamic GetOrSaveImage([FromBody]dynamic request)
        {
            try
            {
                UserDBFactory ud = new UserDBFactory();

                if (request.Status == 1)
                {
                    ProfileImage PI = ud.GetOrSaveImage(Convert.ToString(request.CreatedBy), Convert.ToInt32(request.Status), null);
                    return Ok(PI);
                }
                else
                {
                    //Convert base64 to bytes..
                    byte[] bytes = System.Convert.FromBase64String(Convert.ToString(request.stringInBase64));

                    ud.UpdateImage(Convert.ToString(request.CreatedBy), bytes);
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Your changes has been updated successfully." //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetOrSaveImage", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/SaveProfileOrCredit")]
        [HttpPost]
        public dynamic SaveProfileOrCredit(UserProfileEntity model)
        {
            try
            {
                UserDBFactory mFactory = new UserDBFactory();
                UserEntity UE = mFactory.GetUserFirstAndLastName(model.USERID);
                if (UE != null)
                    if (string.IsNullOrEmpty(UE.USERID))
                        return BaseResponse("Couldn't find your Shipa Freight account, Please signup.", HttpStatusCode.NotFound);

                var oneSignalService = new OneSignal();
                if (model.TabStatus == 1)
                {
                    model.MOBILENUMBER = Regex.Replace(model.MOBILENUMBER, @"[-+() ]", "");
                    string UAHouseNo = (model.UAHOUSENO != null) ? model.UAHOUSENO + "," : null;
                    string UABuildingName = (model.UABUILDINGNAME != null) ? model.UABUILDINGNAME + "," : null;
                    string UAAddressline2 = (model.UAADDRESSLINE2 != null) ? model.UAADDRESSLINE2 + "," : null;
                    // model.UAFULLADDRESS = UAHouseNo + UABuildingName + model.UAADDRESSLINE1 + ", " + UAAddressline2 + model.UACOUNTRYNAME + ", " + model.UASTATENAME + ", " + model.UACITYNAME + ", " + model.UAPOSTCODE;
                    model.UAFULLADDRESS = model.UAADDRESSLINE1 + ", " + UAAddressline2 + model.UACOUNTRYNAME + ", " + model.UASTATENAME + ", " + model.UACITYNAME + ", " + model.UAPOSTCODE;
                    model.COUNTRYID = model.UACOUNTRYID;
                    model.COUNTRYCODE = model.UACOUNTRYCODE;
                    model.COUNTRYNAME = model.UACOUNTRYNAME;
                    string cs = "0";
                    if (model.ResetQuotes) cs = "1";
                    mFactory.UpdateUserProfile(model, "2", "Profile Updated", cs);
                    #region code enable once Active Campaign API Enabled from Firewall
                    //var targetUrl = ApiUrl + "api/activecampaign/ShipaFreightSignUpData";
                    //var obj = GetDataFromApi<UserEntity>(targetUrl, "POST", model);
                    #endregion
                    //Sending Push Notification 
                    var notData = new { NOTIFICATIONTYPEID = 5115, NOTIFICATIONCODE = "Profile Updated" };
                    oneSignalService.SendPushMessageByTag("Email", model.USERID, "Profile Updated", notData);

                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Your profile has been updated successfully."
                        })
                    };
                }
                else
                {
                    if (model.ANNUALTURNOVERCURRENCY != null)
                    {
                        string modifiedAnnualCurrencyValue = model.ANNUALTURNOVERCURRENCY.Replace(",", "");
                        decimal CurrencyValue = Convert.ToDecimal(string.Join("", modifiedAnnualCurrencyValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                        model.ANNUALTURNOVER = CurrencyValue;
                    }
                    if (model.REQUESTEDCREDITLIMITCURRENCY != null)
                    {
                        string modifiedRequestCurrencyValue = model.REQUESTEDCREDITLIMITCURRENCY.Replace(",", "");
                        decimal CurrencyValue = Convert.ToDecimal(string.Join("", modifiedRequestCurrencyValue.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries)));
                        model.REQUESTEDCREDITLIMIT = CurrencyValue;
                    }

                    mFactory.UpdateUserProfile(model, "1", "Credit Requested", "0");

                    //SendMailtoGSSC(model.USERID);
                    UserEntity mDBEntity = mFactory.GetUserDetails(model.USERID);
                    if (mDBEntity.COUNTRYID != 0)
                    {
                        SendMailtoCountryFinance(mDBEntity.COUNTRYID, "Credit", model.USERID);
                    }
                    //Sending Push Notification 
                    var notData = new { NOTIFICATIONTYPEID = 5115, NOTIFICATIONCODE = "Credit Requested" };
                    oneSignalService.SendPushMessageByTag("Email", model.USERID, "Credit Requested", notData);

                    if (model.EXISTINGCUSTOMER == 1 && model.USEMYCREIDT == 1)
                    {

                        SendMailToGSSCforExistingCustomer(model);
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Your Credit request has been submitted successfully."
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "SaveProfileOrCredit", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }


        [Route("api/UserManagement/FillCountry")]
        [HttpGet]
        public dynamic FillCountry()
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CountryEntity> country = mdmFactory.GetCountries(string.Empty, string.Empty);
                if (country != null)
                {
                    return Ok(country.OrderBy(x => x.NAME));
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Error." //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "FillCountry", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/FillStateByCountryId")]
        [HttpPost]
        public dynamic FillStateByCountryId([FromBody]dynamic request)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<StateEntity> States = mdmFactory.GetStatesByCountry(Convert.ToInt32(request.CountryId), string.Empty, string.Empty);
                if (States != null)
                {
                    return Ok(States.OrderBy(x => x.NAME));
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Error." //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "FillStateByCountryId", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/FillCityByStateSubDivisionId")]
        [HttpPost]
        public dynamic FillCityByStateSubDivisionId([FromBody]dynamic request)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CityEntity> cities = mdmFactory.GetCitiesByState(Convert.ToInt32(request.SubDivisionId), string.Empty, string.Empty);
                if (cities != null)
                {

                    return Ok(cities.OrderBy(x => x.NAME));
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "No Data" //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "FillCityByStateSubDivisionId", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                   {
                       Content = new JsonContent(new
                       {
                           StatusCode = HttpStatusCode.ExpectationFailed,
                           Message = ex.Message.ToString() //return Success Message
                       })
                   };
            }
        }

        [Route("api/UserManagement/UpdateStateByCityId")]
        [HttpPost]
        public dynamic UpdateStateByCityId([FromBody]dynamic request)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<StateEntity> States = mdmFactory.GetStateByCityId(Convert.ToString(request.SubdivisionCode));
                if (States != null)
                {

                    return Ok(States.OrderBy(x => x.NAME));
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "No Data" //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "UpdateStateByCityId", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/FillCityByCountryID")]
        [HttpPost]
        public dynamic FillCityByCountryID([FromBody]dynamic request)
        {
            try
            {
                MDMDataFactory mdmFactory = new MDMDataFactory();
                IList<CityEntity> cities = mdmFactory.GetCitiesByCountry(Convert.ToInt32(request.CountryID));
                if (cities != null)
                {

                    return Ok(cities.OrderBy(x => x.NAME));
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "No data" //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "FillCityByCountryID", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/FillIndustry")]
        [HttpGet]
        public dynamic FillIndustry()
        {
            try
            {

                IList<IndustryList> IndustryList = FillIndusList();
                if (IndustryList != null)
                {

                    return Ok(IndustryList.OrderBy(x => x.Text));
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "No data" //return Success Message
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "FillIndustry", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        public void SendMailToGSSCforExistingCustomer(UserProfileEntity model)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];

            MailMessage message = new MailMessage();
            message.From = new MailAddress("shipa@agility.com");
            message.To.Add(new MailAddress(Email));
            message.To.Add(new MailAddress(help));
            message.Subject = "Shipa Freight - Request for known customer";
            message.IsBodyHtml = true;
            string body = string.Empty;

            string Bodytext = "<b>Dear GSSC team</b>, <br/> <br/>           A user (" + model.USERID + ") has updated the profile mentioning as existing customer and has authorized to use the present credit limit setup for his account. Please check with country finance (" + "<b>" + model.COUNTRYNAME + "</b>" + ") and update their credit limit from the user section.";
            body = Bodytext;

            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }
        public void SendMailtoGSSC(string User)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];

            MailMessage message = new MailMessage();
            message.From = new MailAddress("shipa@agility.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = "Shipa Freight - Request for Business Credit" + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/GSSC-CreditRequest.html")))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{User}", User);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }

        [Route("api/UserManagement/CreditUploadDocuments")]
        [HttpPost]
        public dynamic CreditUploadDocuments([FromBody]UploadDocModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    long docid = 0;
                    byte[] fileBytes = System.Convert.FromBase64String(Req.stringInBase64);
                    SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                    sspdoc.CREATEDBY = Req.UserId;
                    sspdoc.FILENAME = Req.FileName;
                    sspdoc.FILEEXTENSION = Req.ContentType;
                    sspdoc.FILESIZE = Req.ContentLength;
                    sspdoc.FILECONTENT = fileBytes;
                    sspdoc.DocumentName = Req.FileName;
                    sspdoc.DOCUMNETTYPE = Req.FileName;
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                    UserDBFactory creditFactory = new UserDBFactory();
                    docid = creditFactory.SaveDocumentsWithParams(docentity, Req.UserId, "", "NO");
                    return BaseResponse(new { DOCID = docid }, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "CreditUploadDocuments", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/Deletecreditdoc")]
        [HttpPost]
        public dynamic Deletecreditdoc([FromBody]DeleteDocModel Req)
        {
            try
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                return SafeRun<dynamic>(() =>
                {
                    UserDBFactory mFactory = new UserDBFactory();
                    mFactory.DeleteCreditDocument(Req.UserId, Req.DOCID);
                    return BaseResponse(new { Message = "Success" }, HttpStatusCode.OK);
                });
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "Deletecreditdoc", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        #endregion

        #region CREDIT Setup
        [Route("api/UserManagement/ProcessCredit")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ProcessCredit(decimal ApprovedCreditLimit, string AprroveStatus, string USERID)
        {
            try
            {
                UserDBFactory UD = new UserDBFactory();

                UD.ProcessCredit(Convert.ToDecimal(ApprovedCreditLimit), Convert.ToString(AprroveStatus), Convert.ToString(USERID));

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Data updated successfully."
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "ProcessCredit", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/UserManagement/GetTaxdetails")]
        [HttpPost]
        public dynamic GetTaxdetails([FromBody]dynamic Req)
        {
            try
            {
                TaxDetails tax = new TaxDetails();
                UserDBFactory mFactory = new UserDBFactory();
                DataSet Result = mFactory.GetCountryTaxDetails(Convert.ToInt32(Req.countryid.Value));
                if (!ReferenceEquals(Result, null) && Result.Tables[0].Rows.Count > 0)
                {
                    tax.IndirectTaxapplicable = Convert.ToString(Result.Tables[0].Rows[0]["INDIRECTTAXAPPLICABILITY"]);
                    tax.Taxlabel = Convert.ToString(Result.Tables[0].Rows[0]["INDIRECTTAXLABEL"]);
                    tax.Ispostcode = Convert.ToString(Result.Tables[0].Rows[0]["ISPOSTCODE"]);
                    tax.Indtaxmaxlength = Convert.ToString(Result.Tables[0].Rows[0]["INDTAXMAXIMUMLENGTH"]);
                }
                return BaseResponse(tax, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/UserManagement", "GetTaxdetails", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        #endregion

        #region Marketing Banner
        [Route("api/UserManagement/MarketingBanner")]
        [HttpGet]
        public HttpResponseMessage MarketingBanner()
        {
            UserDBFactory Ufactory = new UserDBFactory();
            var data = Ufactory.GetMarketingBanner();
            return BaseResponse(data, HttpStatusCode.OK);
        }
        #endregion

        #region Refrence fn's

        public static string DecryptStringAES(string cipherText)
        {

            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }

        public static string EncryptStringAES(string plainText)
        {

            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var EncriptedFromJavascript = EncryptStringToBytes(plainText, keybytes, iv);
            return Convert.ToBase64String(EncriptedFromJavascript);
        }

        public TokenDetails GenerateToken(string ConfigName)
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            UserDBFactory UMF = new UserDBFactory();

            string ConfigValue = UMF.GetUMConfigValue(ConfigName);
            TokenDetails tobj = new TokenDetails();
            tobj.Token = token;
            tobj.TokenExpiry = DateTime.UtcNow.AddHours(int.Parse(ConfigValue));
            return tobj;

        }

        public void SendMailFocisCustSupport(string User, string Roles, string Subject, string Description, string RefNumber, string JobNumber = "", string NotifyType = "", string CreatedBy = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            if (Roles.Contains("@"))
            {
                message.To.Add(Roles);
                message.To.Add(new MailAddress("help@shipafreight.com"));
            }
            else
            {
                recipientsList = mFactory.GetEmailList(Roles, JobNumber, NotifyType, "", CreatedBy);
                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); message.To.Add(new MailAddress("help@shipafreight.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
            }
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = Subject + EnvironmentName;

            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/CustomerSupport-Focis.html");

            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{User}", User);
            body = body.Replace("{Message}", Description);
            body = body.Replace("{Reference_No}", RefNumber);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            if (Roles.Contains("@"))
            {

                FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Roles, "", "",
            "UserManagement", "Customer support", RefNumber, CreatedBy, body, "", true, message.Subject.ToString());
            }
        }

        public void SendMailForGSSCQuotes(string reqSeqNo, string UEmail)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            Email = Email + ";" + UEmail + ";" + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            string subject = "";
            subject = "Shipa Freight- Customer support Request" + EnvironmentName;
            string body = string.Empty;

            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/CustomerSupport.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{user_name}", "User");
            body = body.Replace("{Message}", "Customer support Request has been done by the user : " + UEmail + " and Customer support Request Number :");
            body = body.Replace("{Reference_No}", reqSeqNo);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            SendMail(Email, body, subject);
        }

        public void SendMail(string Email, string body, string subject)
        {
            MailMessage message = new MailMessage();

            if (Email.Contains(';'))
            {
                for (int i = 0; i <= (Email.Split(';').Length - 1); i++)
                {
                    message.To.Add(new MailAddress(Email.Split(';')[i]));
                }
            }
            else
            {
                message.To.Add(new MailAddress(Email));
            }

            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }

        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            Dictionary<string, long> dic = new Dictionary<string, long>();
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    temp_row.Add(col.ColumnName, dr[col]);
                }
                trows.Add(temp_row);
            }
            return trows;
        }

        public void SendMailForContactUs(QueryInformationModel QueryInformation)
        {
            string Email = string.Empty;
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            if (string.IsNullOrEmpty(EnvironmentName))
            {
                Email = System.Configuration.ConfigurationManager.AppSettings["ContactusEmailForProduction"];
            }
            else
            {
                Email = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            }
            string subject = "Contact us request" + EnvironmentName;
            string body = string.Empty;
            string data = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/ContactUs.html")))
            { body = reader.ReadToEnd(); }
            data = "Dear Toby,";
            data = data + "<br/> <br/>" + "A Contact us request for Shipa Freight with the following details below:";
            data = data + "<br/> <br/>" + "CompanyName : " + QueryInformation.CompanyName;
            data = data + "<br/> <br/>" + "ContactNumber : " + QueryInformation.ContactNumber;
            data = data + "<br/> <br/>" + "Description : " + QueryInformation.Discription;
            data = data + "<br/> <br/>" + "EmailId : " + QueryInformation.EmailId;
            data = data + "<br/> <br/>" + "PreferredContactMethod : " + QueryInformation.PreferredContactMethod;
            data = data + "<br/> <br/>" + "Requester Name : " + QueryInformation.QueryName;
            data = data + "<br/> <br/>" + "QueryType : " + QueryInformation.QueryType;
            data = data + "<br/> <br/>" + "How to know : " + QueryInformation.Howtoknow;
            data = data + "<br/> <br/>" + "Regards";
            data = data + "<br/>" + "Shipa Freight Team";
            body = body.Replace("{body}", data);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMailC(Email, body, subject, "Contact us request", false, null, QueryInformation.EmailId);
        }

        public void SendMailC(string Email, string body, string subject, string submodule, bool MailStore, byte[] bytes = null, string QuotationNumber = "", long docid = 0)
        {
            string BCC = string.Empty;
            string CC = string.Empty;
            string attachment = string.Empty;
            MailMessage message = new MailMessage();
            if (Email.Contains(';'))
            {
                foreach (string s in Email.Split(';'))
                {
                    message.Bcc.Add(new MailAddress(s));
                    BCC = s + ",";
                }
            }
            else
            {
                message.CC.Add(new MailAddress(Email));
                CC = Email;
            }
            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            if (bytes != null)
                message.Attachments.Add(new Attachment(new MemoryStream(bytes), QuotationNumber + ".pdf"));

            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", "", CC, BCC,
             "Quotation", submodule, QuotationNumber.ToString(), "", body, docid.ToString(), MailStore, message.Subject.ToString());
        }


        public HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }

        public HttpResponseMessage ErrorResponse()
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = "Something Went Wrong"
                })
            };
        }

        public HttpResponseMessage ErrorResponse(dynamic error)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.ExpectationFailed,
                    Error = error
                })
            };
        }

        #endregion

        #region Private Methods

        private string createEmailBody(string userName, string url)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/account-activation-temp.html")))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{user}", ((string.IsNullOrEmpty(userName) != true) ? userName : string.Empty));
            body = body.Replace("{url}", url);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            return body;
        }
        private bool isValidPassword(String pass)
        {
            int check = 0;
            if (Regex.Match(pass, ".*[A-Z].*").Success) check++;
            if (Regex.Match(pass, ".*[a-z].*").Success) check++;
            if (Regex.Match(pass, ".*[0-9].*").Success) check++;
            if (!Regex.Match(pass, ".*[~!@#$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*").Success) check++;
            if (pass.Length >= 8) check++;
            if (check < 5) return false;
            return true;
        }
        private void SendMailtoCountryFinance(long CountryId, string request, string userid)
        {
            SafeRun<dynamic>(() =>
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                string body = string.Empty;
                string team = string.Empty;
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                string recipientsList = "";
                recipientsList = mFactory.GetEmailList("SSPCOUNTRYFINANCE", "", "CREDITAPPLICATION", CountryId.ToString());
                string[] bccid = recipientsList.Split(',');
                string User = userid;
                foreach (string bccEmailId in bccid)
                {
                    message.To.Add(new MailAddress(bccEmailId));
                }
                message.IsBodyHtml = true;
                string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/CountryFinance-AddCreditRequest.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                string ServerEnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();
                if (request == "Credit")
                {
                    message.Subject = "Shipa Freight - Request for Business Credit" + EnvironmentName;
                }
                else
                {
                    message.Subject = "Shipa Freight - Request for Additional Business Credit" + EnvironmentName;
                }

                if (ServerEnvironmentName == "PRD")
                {
                    body = body.Replace("{focisurl}", "https://focis.agility.com");
                }
                else if (ServerEnvironmentName == "AGL")
                {
                    body = body.Replace("{focisurl}", "https://focisagile.agility.com/login.aspx");
                }
                else if (ServerEnvironmentName == "SIT")
                {
                    body = body.Replace("{focisurl}", "https://focissit.agility.com/login.aspx");
                }
                else if (ServerEnvironmentName == "DEM")
                {
                    body = body.Replace("{focisurl}", "https://focisdemo.agility.com/login.aspx");
                }
                else
                {
                    body = body.Replace("{focisurl}", "https://focisagile.agility.com/login.aspx");
                }
                if (request == "Credit")
                {
                    body = body.Replace("{Additional}", "");
                }
                else
                {
                    body = body.Replace("{Additional}", "Additional");
                }
                body = body.Replace("{User}", User);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                if (request == "Credit")
                {
                    FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                     "UserManagement", "Request for Business Credit", "", userid, body, "", false, message.Subject.ToString());
                }
                else
                {
                    FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                 "UserManagement", "Request for Additional Business Credit", "", userid, body, "", false, message.Subject.ToString());
                }
                return null;
            });
        }
        private IList<IndustryList> FillIndusList()
        {
            IList<IndustryList> Il = new List<IndustryList>{
                    new IndustryList{ Text="Automotive", Value = "Automotive"},              
                    new IndustryList{ Text="Chemicals", Value = "Chemicals" },
                    new IndustryList{ Text="Consumer goods", Value = "Consumer goods" },
                    new IndustryList{ Text="Defense and government", Value = "Defense and government" },
                    new IndustryList{ Text="Energy and Petrochemicals", Value = "Energy and Petrochemicals" },
                    new IndustryList{ Text="Fashion & Retail", Value = "Fashion & Retail" },
                    new IndustryList{ Text="Industrial", Value = "Industrial" },
                    new IndustryList{ Text="Media", Value = "Media" },
                    new IndustryList{ Text="Meetings & Events(MICE)", Value = "Meetings & Events(MICE)" },
                    new IndustryList{ Text="Pharma & life sciences", Value = "Pharma & life sciences" },
                    new IndustryList{ Text="Technology", Value = "Technology" },
                    new IndustryList{ Text="Transport & logistics", Value = "Transport & logistics" },                   
                    new IndustryList{ Text="Other", Value = "Other" }};
            return Il;
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                try
                {
                    // Create the streams used for decryption.
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }
        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            // Create a RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        #endregion

    }

    #region Models

    public class TokenDetails
    {
        public string Token { get; set; }
        public DateTime TokenExpiry { get; set; }
    }
    public class ReturnValues
    {
        public bool IsLoginSuccess { get; set; }
        public string Message { get; set; }
    }
    public class IndustryList
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
    public class TaxDetails
    {
        public string IndirectTaxapplicable { get; set; }
        public string Taxlabel { get; set; }
        public string Ispostcode { get; set; }
        public string Indtaxmaxlength { get; set; }
    }

    public class CreditEnvelope
    {
        public DBFactory.Entities.MBCREDIT CREDITAPPLICATION { get; set; }
        public AdditionalCreditEntity ADDITIONALCREDITAPPLICATION { get; set; }
    }

    public class UserModel
    {
        [Required(ErrorMessage = "UserId value is required.")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Not a valid email")]
        public string UserId { get; set; }
    }

    public class ResetPasswordModel : UserModel
    {
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "New password and confirmation does not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Token value is required.")]
        public string Token { get; set; }
    }

    public class ChangePasswordModel : UserModel
    {
        [Required(ErrorMessage = "OldPassword value is required.")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Password value is required.")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "New password and confirmation does not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class UploadDocModel : UserModel
    {
        [Required(ErrorMessage = "stringInBase64 value is required.")]
        public string stringInBase64 { get; set; }

        public string ContentType { get; set; }

        public long ContentLength { get; set; }

        [Required(ErrorMessage = "FileName value is required.")]
        public string FileName { get; set; }
    }

    public class DeleteDocModel : UserModel
    {
        [Required(ErrorMessage = "DOCID value is required.")]
        public int DOCID { get; set; }
    }

    public class AddCreditChatModel : UserModel
    {
        [Required(ErrorMessage = "CREDITAPPID value is required.")]
        public decimal CREDITAPPID { get; set; }

        public decimal? ADDITIONALCREDITID { get; set; }

        [Required(ErrorMessage = "COMMENTS value is required.")]
        public string COMMENTS { get; set; }
    }

    public class AdditionalCreditModel : UserModel
    {

        public long CREDITAPPID { get; set; }
        public long ADDITIONALCREDITID { get; set; }

        [Required(ErrorMessage = "INDUSTRYID value is required.")]
        public string INDUSTRYID { get; set; }

        [Required(ErrorMessage = "NOOFEMP value is required.")]
        public Int64 NOOFEMP { get; set; }

        public string PARENTCOMPANY { get; set; }

        [Required(ErrorMessage = "ANNUALTURNOVER value is required.")]
        public decimal ANNUALTURNOVER { get; set; }

        public string DBNUMBER { get; set; }
        public string HFMENTRYCODE { get; set; }

        [Required(ErrorMessage = "CURRENCYID value is required.")]
        public string CURRENCYID { get; set; }

        [Required(ErrorMessage = "REQUESTEDCREDITLIMIT value is required.")]
        public decimal REQUESTEDCREDITLIMIT { get; set; }

        public string CURRENTPAYTERMSNAME { get; set; }

        public string COMMENTS { get; set; }

        public string CreditStatus { get; set; }
        public bool EXISTINGCUSTOMER { get; set; }
        public bool USEMYCREIDT { get; set; }
        public IList<AddCreditDoc> CreditDoc { get; set; }
        public List<CrediComment> CreditComments { get; set; }
    }

    public class SaveTicketModel
    {
        public string Module { get; set; }
        public string Type { get; set; }
        public string ReferenceID { get; set; }
        public string Query { get; set; }
        public string Description { get; set; }
        public string ModuleCode { get; set; }
        public string FileName { get; set; }
        public string stringInBase64 { get; set; }
        public string Email { get; set; }
    }

    public class MarketingBanner
    {
        public string Title { get; set; }
        public string CardType { get; set; }
        public string SubTtitle { get; set; }
        public string ImgUrl { get; set; }
        public string WebUrl { get; set; }
    }

    public class SignupModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ISDCode { get; set; }
        public string ContactNo { get; set; }
        public string CompanyName { get; set; }
        public string COUNTRYCODE { get; set; }
        public bool NotificationSubscription { get; set; }
        public string ShipmentProcess { get; set; }
        public string ShippingExperience { get; set; }
        public string CountryName { get; set; }
        public long UACountryId { get; set; }
    }

    #endregion
}
