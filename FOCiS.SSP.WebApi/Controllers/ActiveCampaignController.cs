﻿//using ActiveCampaign.Responses;
//using ActiveCampaign.Struct;
//using FOCiS.SSP.DBFactory;
//using FOCiS.SSP.DBFactory.Entities;
//using FOCiS.SSP.DBFactory.Factory;
//using FOCiS.SSP.Models.UserManagement;
//using FOCiS.SSP.WebApi.Helpers;
//using FOCIS.SSP.ActiveCampaign;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Xml.Serialization;
//using FOCiS.SSP.WebApi.ShipaFreight.Components;

//namespace FOCiS.SSP.WebApi.Controllers
//{
//    public class ActiveCampaignController : BaseController
//    {
//        ActiveCampaignBusiness ActiveBusinessObj = new ActiveCampaignBusiness();
//        [Route("api/activecampaign/InsertUsersIntoActiveCampaign")]
//        [HttpPost]
//        public async Task<SubscriberInsertResponse> InsertQuoteDataIntoActiveCampaign(List<ActCampaignStatusEntity> ActCampaignObject, UserEntity Usermodel, string UserType)
//        {
//            SubscriberInsertResponse InsertResponse = new SubscriberInsertResponse();
//            try
//            {
//                InsertResponse = await Task.Run(() => ActiveBusinessObj.InsertQuoteDataIntoActiveCampaign(ActCampaignObject, Usermodel, UserType));
//            }
//            catch (Exception)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return InsertResponse;
//        }

//        [Route("api/activecampaign/EditContactsIntoActiveCampaign")]
//        [HttpPost]
//        public async Task<SubscriberUpdatePost> EditContactsIntoActiveCampaign(ActCampaignEditEntity ActCampaignObject, int SubscriberId, string ListId, string UserType)
//        {
//            SubscriberUpdatePost InsertResponse = new SubscriberUpdatePost();
//            try
//            {
//                InsertResponse = await Task.Run(() => ActiveBusinessObj.EditContactsIntoActiveCampaign(ActCampaignObject, SubscriberId, ListId, UserType));
//            }
//            catch (Exception)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return InsertResponse;
//        }

//        [Route("api/activecampaign/DeleteActiveContact")]
//        [HttpPost]
//        public async Task<int> DeleteActiveContact(ActiveCampaignContactEntity UserObject)
//        {
//            int status = 0;
//            try
//            {
//                status = await Task.Run(() => ActiveBusinessObj.DeleteActiveContact(UserObject));

//            }
//            catch (Exception)
//            {
//                status = 0;
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }

//            return status;
//        }

//        [Route("api/activecampaign/DeleteListInActiveContact")]
//        [HttpGet]
//        public async Task<int> DeleteListInActiveContact()
//        {
//            int status = 0;
         
//            try
//            {
//               status= await Task.Run(()=> ActiveBusinessObj.DeleteListInActiveContact());

//            }
//            catch (Exception)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return status;
//        }

//        [Route("api/activecampaign/GuestUsersData")]
//        [HttpGet]
//        public async Task<List<ActCampaignStatusEntity>> GuestUsersData()
//        {
//            List<ActCampaignStatusEntity> objActCampaignEntity = new List<ActCampaignStatusEntity>();
//            try
//            {

//                objActCampaignEntity = await Task.Run(() => ActiveBusinessObj.GuestUsersData());
//            }
//            catch (Exception)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return objActCampaignEntity;

//        }

//        [Route("api/activecampaign/RegisterUsersData")]
//        [HttpGet]
//        public async Task<List<ActCampaignStatusEntity>> RegisterUsersData()
//        {
           
//            List<ActCampaignStatusEntity> objActCampaignEntity = new List<ActCampaignStatusEntity>();
//            try
//            {
//               objActCampaignEntity= await Task.Run(()=> ActiveBusinessObj.RegisterUsersData());
//            }
//            catch (Exception ex)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return objActCampaignEntity;

//        }

//        [Route("api/activecampaign/DeleteUsersData")]
//        [HttpGet]
//        public async Task<List<ActiveCampaignContactEntity>> DeleteUsersData()
//        {
//            List<ActiveCampaignContactEntity> objUserEntity = new List<ActiveCampaignContactEntity>();
//            try
//            {
//                ActiveCampaignDBFactory activeCampaign = new ActiveCampaignDBFactory();
//                objUserEntity = await Task.Run(() => activeCampaign.GetActiveContacts()).ConfigureAwait(false);
//                //await Task.Run(() => DeleteActiveContact(objUserEntity)).ConfigureAwait(false);

//            }
//            catch (Exception)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return objUserEntity;

//        }

//        [Route("api/activecampaign/ShipaFreightSignUpData")]
//        [HttpPost]
//        public async Task<UserEntity> ShipaFreightSignUpData(UserEntity UserObject)
//        {
//            try
//            {
//                await Task.Run(() => ActiveBusinessObj.ShipaFreightSignUpData(UserObject));
//            }
//            catch (Exception) {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return UserObject;
//        }

//        [Route("api/activecampaign/WebHookUnSubscribeResponse")]
//        [HttpPost]
//        public string WebHookUnSubscribeResponse([FromBody]dynamic request)
//        {
//            WebHookUnSubScribeResult GetUnSubScribeResponse = new WebHookUnSubScribeResult();
//            string response;
//            try
//            {
//                response = JsonConvert.SerializeObject(request);
//                //var targetUrl = ApiUrl + "api/activecampaign/WebHookUnSubscribeResponse";
//                //response = await Task.Run(() =>ReadStream(targetUrl)).ConfigureAwait(false);
//                int status = ActiveBusinessObj.SaveWebHookResponse(response);
//            }
//            catch (Exception)
//            {
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            }
//            return response;
//        }

//    }
//}
