﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using System.Data;
using Newtonsoft.Json;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.draw;
using System.Globalization;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.Web.UI.Controllers;
using FOCiS.SSP.Models.Payment;
using FOCiS.SSP.WebApi.Helpers;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class JoBookMBController : BaseController
    {
        private static CultureInfo culture = (CultureInfo)CultureInfo.CreateSpecificCulture("en-US");
        private static string format = "{0:N2}";
        private static Decimal mQuoteChargesTotal = 0;
        private static string PreferredCurrencyId = string.Empty;
        private static QuotationPreviewModel DUIModels;
        private static UserProfileEntity DUserModel;
        private static byte[] bytes;
        private static int PDFpage = 0;
        string body = string.Empty;
        string subject = string.Empty;
        string mPlaceOfReceipt = string.Empty;
        string mPlaceOfDelivery = string.Empty;
        string mPRLableName = string.Empty;
        string mPDLableName = string.Empty;
        string cargodetails = string.Empty;
        string productname = string.Empty;

        public IEnumerable<QuotationModel> Get()
        {
            return null;
        }

        public IHttpActionResult GetById(int id)
        {
            return null;
        }

        #region Get API'S

        [Route("api/JoBookMB/QuoteToJob")]
        [HttpPost]
        public dynamic QuoteToJob([FromBody]dynamic request)
        {
            DBFactory.Entities.QuoteToJobDetails mDBEntity = new QuoteToJobDetails();

            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            UserDBFactory UserFactory = new UserDBFactory();
            IList<Shipper> sh = new List<Shipper>();
            IList<Consignee> CON = new List<Consignee>();

            JobBookingEntity JBE = new JobBookingEntity();

            DataSet qds = new DataSet();
            qds = UserFactory.IsProfileUpdated(Convert.ToString(request.CreatedBy));
            if (qds.Tables[0].Rows[0]["NOTIFICATIONCOUNT"].ToString() == "0")
            {
                return ErrorResponse("Profile Incomplete");
            }

            int OriginCountryID = 0;
            if (Convert.ToInt64(request.JobNumber) == 0)
            {
                mDBEntity = mFactory.GetByQuotationID(Convert.ToInt32(request.QuotationID), Convert.ToString(request.CreatedBy));

                Generatequotepdf(mDBEntity.QuoteDetails.QUOTATIONID, mDBEntity.JobItems.JOBNUMBER, Convert.ToString(request.CreatedBy));

                mFactory.InsertCountryBasedDocsFIELDS(Convert.ToInt64(mDBEntity.QuoteDetails.OCOUNTRYID),
                     Convert.ToInt64(mDBEntity.QuoteDetails.DCOUNTRYID), mDBEntity.JobItems.JOBNUMBER.ToString(), mDBEntity.QuoteDetails.QUOTATIONID.ToString(),
                     Convert.ToString(request.CreatedBy), JBE);

                DBFactory.Entities.UserPartyMaster PartyEntity = UserFactory.GetUserPersonalParty(Convert.ToString(request.CreatedBy));

                if (PartyEntity != null)
                {
                    if (PartyEntity.COUNTRYID == mDBEntity.QuoteDetails.OCOUNTRYID)
                    {
                        Shipper Shitem = new Shipper();
                        OriginCountryID = Convert.ToInt32(PartyEntity.COUNTRYID);
                        Shitem.SHCLIENTID = Convert.ToString(PartyEntity.SSPCLIENTID);
                        Shitem.SHCLIENTNAME = Convert.ToString(PartyEntity.Clientname);
                        Shitem.SHHOUSENO = Convert.ToString(PartyEntity.HOUSENO);
                        Shitem.SHBUILDINGNAME = Convert.ToString(PartyEntity.BUILDINGNAME);
                        Shitem.SHSTREET1 = Convert.ToString(PartyEntity.ADDRESSLINE1);
                        Shitem.SHSTREET2 = Convert.ToString(PartyEntity.ADDRESSLINE2);
                        Shitem.SHSTATENAME = Convert.ToString(PartyEntity.STATENAME);
                        Shitem.SHCOUNTRYNAME = Convert.ToString(PartyEntity.COUNTRYNAME);
                        Shitem.SHCITYNAME = Convert.ToString(PartyEntity.CITYNAME);
                        Shitem.SHPINCODE = Convert.ToString(PartyEntity.Pincode);
                        Shitem.SHFIRSTNAME = Convert.ToString(PartyEntity.FIRSTNAME);
                        Shitem.SHLASTNAME = Convert.ToString(PartyEntity.LASTNAME);
                        Shitem.SHEMAILID = Convert.ToString(PartyEntity.EMAILID);
                        Shitem.SHPHONE = Convert.ToString(PartyEntity.PHONE);
                        Shitem.SHCOUNTRYID = PartyEntity.COUNTRYID.ToString();
                        sh.Add(Shitem);
                        mDBEntity.Shipper = sh.ToList();
                    }
                    else
                    {

                        mDBEntity.Shipper = sh;
                    }
                    if (PartyEntity.COUNTRYID == mDBEntity.QuoteDetails.DCOUNTRYID)
                    {
                        Consignee CONitem = new Consignee();

                        CONitem.CONCLIENTID = Convert.ToString(PartyEntity.SSPCLIENTID);
                        CONitem.CONCLIENTNAME = Convert.ToString(PartyEntity.Clientname);
                        CONitem.CONHOUSENO = Convert.ToString(PartyEntity.HOUSENO);
                        CONitem.CONBUILDINGNAME = Convert.ToString(PartyEntity.BUILDINGNAME);
                        CONitem.CONSTREET1 = Convert.ToString(PartyEntity.ADDRESSLINE1);
                        CONitem.CONSTREET2 = Convert.ToString(PartyEntity.ADDRESSLINE2);
                        CONitem.CONSTATENAME = Convert.ToString(PartyEntity.STATENAME);
                        CONitem.CONCOUNTRYNAME = Convert.ToString(PartyEntity.COUNTRYNAME);
                        CONitem.CONCITYNAME = Convert.ToString(PartyEntity.CITYNAME);
                        CONitem.CONPINCODE = Convert.ToString(PartyEntity.Pincode);
                        CONitem.CONFIRSTNAME = Convert.ToString(PartyEntity.FIRSTNAME);
                        CONitem.CONLASTNAME = Convert.ToString(PartyEntity.LASTNAME);
                        CONitem.CONEMAILID = Convert.ToString(PartyEntity.EMAILID);
                        CONitem.CONPHONE = Convert.ToString(PartyEntity.PHONE);
                        CONitem.CONCOUNTRYID = PartyEntity.COUNTRYID.ToString();
                        CON.Add(CONitem);
                        mDBEntity.Consignee = CON.ToList();
                    }
                    else
                    {
                        mDBEntity.Consignee = CON;
                    }
                }
                else
                {
                    OriginCountryID = Convert.ToInt32(mDBEntity.QuoteDetails.OCOUNTRYID);
                    mDBEntity.Consignee = CON;
                    mDBEntity.Shipper = sh;
                }
            }
            else
            {
                mDBEntity = mFactory.GetQuotedJobDetails_MB(Convert.ToInt32(request.QuotationID), Convert.ToInt64(request.JobNumber));

                if (mDBEntity.PartyItems.Count() > 0)
                {
                    foreach (var items in mDBEntity.PartyItems)
                    {
                        if (items.PARTYTYPE == "91")
                        {
                            OriginCountryID = Convert.ToInt32(items.COUNTRYID);
                            Shipper Shitem = new Shipper();

                            Shitem.SHCLIENTID = Convert.ToString(items.SSPCLIENTID);
                            Shitem.SHCLIENTNAME = Convert.ToString(items.CLIENTNAME);
                            Shitem.SHHOUSENO = Convert.ToString(items.HOUSENO);
                            Shitem.SHBUILDINGNAME = Convert.ToString(items.BUILDINGNAME);
                            Shitem.SHSTREET1 = Convert.ToString(items.STREET1);
                            Shitem.SHSTREET2 = Convert.ToString(items.STREET2);
                            Shitem.SHSTATENAME = Convert.ToString(items.STATENAME);
                            Shitem.SHCOUNTRYNAME = Convert.ToString(items.COUNTRYNAME);
                            Shitem.SHCITYNAME = Convert.ToString(items.CITYNAME);
                            Shitem.SHPINCODE = Convert.ToString(items.PINCODE);
                            Shitem.SHFIRSTNAME = Convert.ToString(items.FIRSTNAME);
                            Shitem.SHLASTNAME = Convert.ToString(items.LASTNAME);
                            Shitem.SHEMAILID = Convert.ToString(items.EMAILID);
                            Shitem.SHPHONE = Convert.ToString(items.PHONE);
                            Shitem.SHPARTYTYPE = items.PARTYTYPE;
                            Shitem.SHCOUNTRYID = items.COUNTRYID.ToString();
                            sh.Add(Shitem);
                            mDBEntity.Shipper = sh.ToList();
                        }
                        else
                        {
                            Consignee CONitem = new Consignee();

                            CONitem.CONCLIENTID = Convert.ToString(items.SSPCLIENTID);
                            CONitem.CONCLIENTNAME = Convert.ToString(items.CLIENTNAME);
                            CONitem.CONHOUSENO = Convert.ToString(items.HOUSENO);
                            CONitem.CONBUILDINGNAME = Convert.ToString(items.BUILDINGNAME);
                            CONitem.CONSTREET1 = Convert.ToString(items.STREET1);
                            CONitem.CONSTREET2 = Convert.ToString(items.STREET2);
                            CONitem.CONSTATENAME = Convert.ToString(items.STATENAME);
                            CONitem.CONCOUNTRYNAME = Convert.ToString(items.COUNTRYNAME);
                            CONitem.CONCITYNAME = Convert.ToString(items.CITYNAME);
                            CONitem.CONPINCODE = Convert.ToString(items.PINCODE);
                            CONitem.CONFIRSTNAME = Convert.ToString(items.FIRSTNAME);
                            CONitem.CONLASTNAME = Convert.ToString(items.LASTNAME);
                            CONitem.CONEMAILID = Convert.ToString(items.EMAILID);
                            CONitem.CONPHONE = Convert.ToString(items.PHONE);
                            CONitem.CONPARTYTYPE = items.PARTYTYPE;
                            CONitem.CONCOUNTRYID = items.COUNTRYID.ToString();
                            CON.Add(CONitem);
                            mDBEntity.Consignee = CON.ToList();
                        }
                    }
                }
                else
                {
                    OriginCountryID = Convert.ToInt32(mDBEntity.QuoteDetails.OCOUNTRYID);
                    //Get User Personal Party Details and fill based on countryid
                    FillShipperConsignee(Convert.ToString(request.CreatedBy), mDBEntity);
                }
            }

            DBFactory.Entities.CargoAvabilityEntity CargoDate = mFactory.GetPickUpTransitDates(Convert.ToInt64(OriginCountryID));
            mDBEntity.CargoAvabilityEntity = CargoDate;

            MDMDataFactory mdmFactory = new MDMDataFactory();
            IList<IncoTermsEntity> incoterms = mdmFactory.GetIncoTerms(string.Empty, string.Empty);
            mDBEntity.IncoTerms = incoterms;
            //-------Remove Party items from list Before return...
            mDBEntity.PartyItems = null;
            return mDBEntity;
        }

        private void FillShipperConsignee(string CreatedBy, QuoteToJobDetails mDBEntity)
        {
            IList<Shipper> sh = new List<Shipper>();
            IList<Consignee> CON = new List<Consignee>();
            UserDBFactory UserFactory = new UserDBFactory();
            DBFactory.Entities.UserPartyMaster PartyEntity = UserFactory.GetUserPersonalParty(CreatedBy);

            if (PartyEntity != null)
            {
                if (PartyEntity.COUNTRYID == mDBEntity.QuoteDetails.OCOUNTRYID)
                {
                    Shipper Shitem = new Shipper();
                    Shitem.SHCLIENTID = Convert.ToString(PartyEntity.SSPCLIENTID);
                    Shitem.SHCLIENTNAME = Convert.ToString(PartyEntity.Clientname);
                    Shitem.SHHOUSENO = Convert.ToString(PartyEntity.HOUSENO);
                    Shitem.SHBUILDINGNAME = Convert.ToString(PartyEntity.BUILDINGNAME);
                    Shitem.SHSTREET1 = Convert.ToString(PartyEntity.ADDRESSLINE1);
                    Shitem.SHSTREET2 = Convert.ToString(PartyEntity.ADDRESSLINE2);
                    Shitem.SHSTATENAME = Convert.ToString(PartyEntity.STATENAME);
                    Shitem.SHCOUNTRYNAME = Convert.ToString(PartyEntity.COUNTRYNAME);
                    Shitem.SHCITYNAME = Convert.ToString(PartyEntity.CITYNAME);
                    Shitem.SHPINCODE = Convert.ToString(PartyEntity.Pincode);
                    Shitem.SHFIRSTNAME = Convert.ToString(PartyEntity.FIRSTNAME);
                    Shitem.SHLASTNAME = Convert.ToString(PartyEntity.LASTNAME);
                    Shitem.SHEMAILID = Convert.ToString(PartyEntity.EMAILID);
                    Shitem.SHPHONE = Convert.ToString(PartyEntity.PHONE);
                    Shitem.SHCOUNTRYID = PartyEntity.COUNTRYID.ToString();
                    sh.Add(Shitem);
                    mDBEntity.Shipper = sh.ToList();
                }
                else
                {
                    mDBEntity.Shipper = sh;
                }
                if (PartyEntity.COUNTRYID == mDBEntity.QuoteDetails.DCOUNTRYID)
                {
                    Consignee CONitem = new Consignee();

                    CONitem.CONCLIENTID = Convert.ToString(PartyEntity.SSPCLIENTID);
                    CONitem.CONCLIENTNAME = Convert.ToString(PartyEntity.Clientname);
                    CONitem.CONHOUSENO = Convert.ToString(PartyEntity.HOUSENO);
                    CONitem.CONBUILDINGNAME = Convert.ToString(PartyEntity.BUILDINGNAME);
                    CONitem.CONSTREET1 = Convert.ToString(PartyEntity.ADDRESSLINE1);
                    CONitem.CONSTREET2 = Convert.ToString(PartyEntity.ADDRESSLINE2);
                    CONitem.CONSTATENAME = Convert.ToString(PartyEntity.STATENAME);
                    CONitem.CONCOUNTRYNAME = Convert.ToString(PartyEntity.COUNTRYNAME);
                    CONitem.CONCITYNAME = Convert.ToString(PartyEntity.CITYNAME);
                    CONitem.CONPINCODE = Convert.ToString(PartyEntity.Pincode);
                    CONitem.CONFIRSTNAME = Convert.ToString(PartyEntity.FIRSTNAME);
                    CONitem.CONLASTNAME = Convert.ToString(PartyEntity.LASTNAME);
                    CONitem.CONEMAILID = Convert.ToString(PartyEntity.EMAILID);
                    CONitem.CONPHONE = Convert.ToString(PartyEntity.PHONE);
                    CONitem.CONCOUNTRYID = PartyEntity.COUNTRYID.ToString();
                    CON.Add(CONitem);
                    mDBEntity.Consignee = CON.ToList();
                }
                else
                {
                    mDBEntity.Consignee = CON;
                }
            }
            else
            {
                mDBEntity.Consignee = CON;
                mDBEntity.Shipper = sh;
            }
        }

        [Route("api/JoBookMB/BookingSummary")]
        [HttpPost]
        public dynamic BookingSummary([FromBody]dynamic request)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            UserDBFactory UserFactory = new UserDBFactory();
            IList<Shipper> sh = new List<Shipper>();
            IList<Consignee> CON = new List<Consignee>();
            int OriginCountryID = 0;

            BookingSummary mDBEntity = mFactory.GetBookingSummary_MB(Convert.ToInt32(request.QuotationID), Convert.ToInt64(request.JobNumber));
            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mDBEntity.QuoteDetails.PREFERREDCURRENCYID);

            if (mDBEntity.PartyItems.Any())
            {
                foreach (var items in mDBEntity.PartyItems)
                {
                    if (items.PARTYTYPE == "91")
                    {
                        OriginCountryID = Convert.ToInt32(items.COUNTRYID);
                        Shipper Shitem = new Shipper();

                        Shitem.SHCLIENTID = Convert.ToString(items.SSPCLIENTID);
                        Shitem.SHCLIENTNAME = Convert.ToString(items.CLIENTNAME);
                        Shitem.SHHOUSENO = Convert.ToString(items.HOUSENO);
                        Shitem.SHBUILDINGNAME = Convert.ToString(items.BUILDINGNAME);
                        Shitem.SHSTREET1 = Convert.ToString(items.STREET1);
                        Shitem.SHSTREET2 = Convert.ToString(items.STREET2);
                        Shitem.SHSTATENAME = Convert.ToString(items.STATENAME);
                        Shitem.SHCOUNTRYNAME = Convert.ToString(items.COUNTRYNAME);
                        Shitem.SHCITYNAME = Convert.ToString(items.CITYNAME);
                        Shitem.SHPINCODE = Convert.ToString(items.PINCODE);
                        Shitem.SHFIRSTNAME = Convert.ToString(items.FIRSTNAME);
                        Shitem.SHLASTNAME = Convert.ToString(items.LASTNAME);
                        Shitem.SHEMAILID = Convert.ToString(items.EMAILID);
                        Shitem.SHPHONE = Convert.ToString(items.PHONE);
                        Shitem.SHPARTYTYPE = items.PARTYTYPE;
                        Shitem.SHCOUNTRYID = items.COUNTRYID.ToString();
                        sh.Add(Shitem);
                        mDBEntity.Shipper = sh.ToList();
                    }
                    else
                    {
                        Consignee CONitem = new Consignee();

                        CONitem.CONCLIENTID = Convert.ToString(items.SSPCLIENTID);
                        CONitem.CONCLIENTNAME = Convert.ToString(items.CLIENTNAME);
                        CONitem.CONHOUSENO = Convert.ToString(items.HOUSENO);
                        CONitem.CONBUILDINGNAME = Convert.ToString(items.BUILDINGNAME);
                        CONitem.CONSTREET1 = Convert.ToString(items.STREET1);
                        CONitem.CONSTREET2 = Convert.ToString(items.STREET2);
                        CONitem.CONSTATENAME = Convert.ToString(items.STATENAME);
                        CONitem.CONCOUNTRYNAME = Convert.ToString(items.COUNTRYNAME);
                        CONitem.CONCITYNAME = Convert.ToString(items.CITYNAME);
                        CONitem.CONPINCODE = Convert.ToString(items.PINCODE);
                        CONitem.CONFIRSTNAME = Convert.ToString(items.FIRSTNAME);
                        CONitem.CONLASTNAME = Convert.ToString(items.LASTNAME);
                        CONitem.CONEMAILID = Convert.ToString(items.EMAILID);
                        CONitem.CONPHONE = Convert.ToString(items.PHONE);
                        CONitem.CONPARTYTYPE = items.PARTYTYPE;
                        CONitem.CONCOUNTRYID = items.COUNTRYID.ToString();
                        CON.Add(CONitem);
                        mDBEntity.Consignee = CON.ToList();
                    }
                }
            }
            else
            {
                mDBEntity.Shipper = sh;
                mDBEntity.Consignee = CON;
            }
            DBFactory.Entities.CargoAvabilityEntity CargoDate = mFactory.GetPickUpTransitDates(Convert.ToInt64(OriginCountryID));
            mDBEntity.CargoAvabilityEntity = CargoDate;

            MDMDataFactory mdmFactory = new MDMDataFactory();
            IList<IncoTermsEntity> incoterms = mdmFactory.GetIncoTerms(string.Empty, string.Empty);
            mDBEntity.IncoTerms = incoterms;

            //-------Remove Party items from list Before return...
            mDBEntity = Map_QuotationCharges(mDBEntity);
            mDBEntity.PartyItems = null;
            mDBEntity.QuotationChargeEntity = null;
            mDBEntity.DecimalCount = decimalCount;
            return mDBEntity;
        }

        private BookingSummary Map_QuotationCharges(BookingSummary mDBEntity)
        {
            List<QuotationChargeEntity> mUIOrg = mDBEntity.QuotationChargeEntity.AsEnumerable().Where(x => x.RouteTypeId == 1118 && x.CHARGESOURCE.ToLower() != "f").ToList();
            List<QuotationChargeEntity> mUIDes = mDBEntity.QuotationChargeEntity.AsEnumerable().Where(x => x.RouteTypeId == 1117 && x.CHARGESOURCE.ToLower() != "f").ToList();
            List<QuotationChargeEntity> mUIInter = mDBEntity.QuotationChargeEntity.AsEnumerable().Where(x => x.RouteTypeId == 1116 && x.CHARGESOURCE.ToLower() != "f").ToList();
            List<QuotationChargeEntity> mUIOptional = mDBEntity.QuotationChargeEntity.Where(x => x.RouteTypeId == 250001 && x.CHARGESOURCE.ToLower() != "f" && x.ISCHARGEINCLUDED == 1).ToList();
            List<QuotationChargeEntity> mUIAdditional = mDBEntity.QuotationChargeEntity.Where(x => x.CHARGESOURCE.ToLower() == "f" && (x.CHARGESTATUS != null && (x.CHARGESTATUS.ToLower() == "paid" || x.CHARGESTATUS.ToLower() == "accepted by customer"))).OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ToList();

            List<QOrgChargeEntity> mUIModel = Mapper.Map<List<QuotationChargeEntity>, List<QOrgChargeEntity>>(mUIOrg);
            List<QDesChargeEntity> mUIModelDes = Mapper.Map<List<QuotationChargeEntity>, List<QDesChargeEntity>>(mUIDes);
            List<QInterChargeEntity> mUIModelInter = Mapper.Map<List<QuotationChargeEntity>, List<QInterChargeEntity>>(mUIInter);
            List<QOptionalChargeEntity> mUIModelOptional = Mapper.Map<List<QuotationChargeEntity>, List<QOptionalChargeEntity>>(mUIOptional);
            List<QAdditionalChargeEntity> mUIModelAdditional = Mapper.Map<List<QuotationChargeEntity>, List<QAdditionalChargeEntity>>(mUIAdditional);

            mDBEntity.QOrgCharges = mUIModel.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QDesCharges = mUIModelDes.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QInterCharges = mUIModelInter.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QOptionalCharges = mUIModelOptional.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QAdditionalCharges = mUIModelAdditional.Where(o => o.CHARGEAPPLICABILITY == "Current" || o.CHARGEAPPLICABILITY == null || o.CHARGEAPPLICABILITY == "?").OrderBy(o => o.CHARGENAME).ToList();

            return mDBEntity;
        }

        [Route("api/JoBookMB/_SearchParties")]
        [HttpGet]
        public dynamic _SearchParties(string Name, string Searchstring = "", string TabName = "", int CountryId = 0)
        {
            try
            {
                if (string.IsNullOrEmpty(Name))
                {
                    return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = "Logged in user name should not be empty."
                    })
                };
                }

                UserDBFactory UserFactory = new UserDBFactory();
                List<DBFactory.Entities.UserPartyMaster> PartyEntity = new List<UserPartyMaster>();


                if (Searchstring == "\"\"" || Searchstring == null)
                    Searchstring = string.Empty;

                if (CountryId == 0 && (TabName.ToUpper() == "PROFILE" || TabName == ""))
                {
                    PartyEntity = UserFactory.GetPartyMasterList_MB(Name, Searchstring, 0, TabName);
                }
                else
                {
                    PartyEntity = UserFactory.GetPartyMasterList_MB(Name, Searchstring, CountryId, TabName);
                }

                List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);
                return Ok(PartyModel);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.ToString()
                    })
                };
            }

        }


        [Route("api/JoBookMB/CheckExistingJob")]
        [HttpGet]
        public dynamic CheckExistingJob(int quotationId, string Email)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataSet ds = mFactory.CheckExistingJob(Convert.ToInt32(quotationId), Email);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                var JsonToReturn = new
                {
                    rows = trows,
                };


                return Ok(trows);
            }
            else
            {
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = "No Data"
                    })
                };
            }
        }

        [Route("api/JoBookMB/CheckExistingJob/v2")]
        [HttpGet]
        public dynamic VerifyJob(int quotationId, string Email)
        {
            UserDBFactory mQFactory = new UserDBFactory();
            DataSet qds = new DataSet();
            qds = mQFactory.IsProfileUpdated(Email);
            if (qds.Tables[0].Rows[0]["NOTIFICATIONCOUNT"].ToString() == "0")
            {
                return ErrorResponse("Profile Incomplete");
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();

                DataSet ds = mFactory.CheckExistingJob(Convert.ToInt32(quotationId), Email);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return BaseResponse(new
                    {
                        JobStatus = "exists",
                        JobNumber = dt.Rows[0]["JOBNUMBER"].ToString(),
                        JobGrandTotal = dt.Rows[0]["JOBGRANDTOTAL"].ToString(),
                        GrandTotal = dt.Rows[0]["GRANDTOTAL"].ToString()
                    }, HttpStatusCode.OK);
                }
                else
                {
                    return BaseResponse(new { JobStatus = "notexists", JobNumber = 0 }, HttpStatusCode.OK);
                }
            }
        }

        [Route("api/JoBookMB/getCountryWeekends")]
        [HttpGet]
        public dynamic getCountryWeekends(Int64 originCountryID, Int64 destinationCountryID)
        {
            try
            {
                JobBookingDbFactory JBFactory = new JobBookingDbFactory();
                IList<GetCountryWeekends> weekOff = JBFactory.GetCountryWeekends(originCountryID, destinationCountryID);
                if (weekOff != null)
                {
                    var weekOfflist = (
                             from items in weekOff
                             select new
                             {
                                 Text = items.COUNTRYID,
                                 Value = items.WEEKOFF
                             }).Distinct().OrderBy(x => x.Text).ToList();

                    GetCountryWeekendsList GWL = new GetCountryWeekendsList();
                    GWL.GetCountryWeekends = weekOff;
                    return Ok(GWL);
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {

                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "getCountryWeekends", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException(ex.ToString());
            }
        }

        [Route("api/JoBookMB/getCountryHolidays")]
        [HttpGet]
        public dynamic getCountryHolidays(Int64 originCountryID, Int64 destinationCountryID)
        {
            UserDBFactory mFactory = new UserDBFactory();
            JobBookingDbFactory JBFactory = new JobBookingDbFactory();
            DataSet ds = JBFactory.GetCountryHolidays(originCountryID, destinationCountryID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Dictionary<string, object>> trows = ReturnJsonResult(ds.Tables[0]);
                var JsonToReturn = new
                {
                    rows = trows,
                };


                return Ok(JsonToReturn);
            }
            else
            {
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = "No Data"
                    })
                };
            }
        }
        #endregion


        #region POST API'S
        /// <summary>
        /// JoBooking List 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// 
        [Route("api/JoBookMB/GetSavedBookList")]
        [HttpPost]
        public IHttpActionResult GetSavedQuoteList([FromBody]dynamic request)
        {
            string list = ""; string status = ""; int pageno = 1;

            if (request.list != null) list = request.List.Value; if (request.Status != null) status = request.Status.Value;

            if (list == null || list == "") list = "JobList"; if (status == null || status == "") status = "ACTIVE";

            if (request.PageNo.Value != null) pageno = Convert.ToInt32(request.PageNo.Value);

            QuotationDBFactory mFactory = new QuotationDBFactory();


            DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetJobStatusCount(request.Email.Value, Convert.ToString(request.SearchString).ToUpper());
            List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetSavedQuotes(request.Email.Value, status, list, pageno, Convert.ToString(request.SearchString).ToUpper(), "DESC");


            ListSavedJobsEntity SQ = new ListSavedJobsEntity();
            SQ.JobsTabCount = mQuoteStatusEntity;
            SQ.SavedJobs = mDBEntity;

            return Ok(SQ);
        }

        [Route("api/JoBookMB/GetSavedBookListNew")]
        [HttpPost]
        public dynamic GetSavedBookListNew([FromBody]dynamic request)
        {
            try
            {
                if (Convert.ToString(request.Status) == null || Convert.ToString(request.Status) == "")
                    request.Status = "INITIATED";
                if (request.PageNo.Value == null) request.PageNo.Value = Convert.ToInt32(1);

                JobBookingDbFactory mFactory = new JobBookingDbFactory();

                List<DBFactory.Entities.QuoteJobListEntity> mDBEntity = mFactory.GetSavedJobs_MB(Convert.ToString(request.Email.Value),
                    Convert.ToString(request.Status), "JobList", Convert.ToInt32(request.PageNo),
                    Convert.ToString(request.SearchString).ToUpper(), Convert.ToString("DESC"));

                DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetJobStatusCount(Convert.ToString(request.Email.Value), Convert.ToString(request.SearchString).ToUpper());

                ListSavedJobsEntity SQ = new ListSavedJobsEntity();
                SQ.JobsTabCount = mQuoteStatusEntity;
                SQ.SavedJobs = mDBEntity;
                return Ok(SQ);
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "GetSavedBookListNew", ex.Message.ToString(), ex.StackTrace.ToString(), Convert.ToString(request.Email.Value));
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/GetSavedDocuments")]
        [HttpPost]
        public dynamic GetSavedDocuments([FromBody]dynamic request)
        {
            try
            {
                JobBookingDbFactory JDF = new JobBookingDbFactory();
                IList<MBDocs> Doc = JDF.GetDocumentDetailsForMobile(0, Convert.ToInt64(request.JobNumber));

                return Ok(Doc);
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "GetSavedDocuments", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/DownloadDocByID")]
        [HttpPost]
        public dynamic DownloadDocByID([FromBody]dynamic request)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                IList<MBDocs> mDBEntity = mFactory.DownloadDocByID(Convert.ToInt64(request.DOCID), "");

                return Ok(mDBEntity);
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "DownloadDocByID", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/UploadDocs")]
        [HttpPost]
        public dynamic UploadDocs([FromBody]dynamic request)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();

                byte[] fbytes = System.Convert.FromBase64String(Convert.ToString(request.stringInBase64));

                SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                sspdoc.DOCID = Convert.ToInt64(request.Docid);
                sspdoc.QUOTATIONID = Convert.ToInt64(request.QuotationId);
                sspdoc.DOCUMNETTYPE = Convert.ToString(request.DocumentType);
                sspdoc.FILENAME = Convert.ToString(request.FileName);
                sspdoc.FILEEXTENSION = Convert.ToString(request.FILEEXTENSION);
                sspdoc.FILESIZE = Convert.ToInt64(request.ContentLength);
                sspdoc.FILECONTENT = fbytes;
                sspdoc.JOBNUMBER = Convert.ToInt64(request.JobNumber);


                if (Convert.ToString(request.DocumentName) == "")
                {
                    sspdoc.DocumentName = Convert.ToString(request.UploadedFileName).ToString();
                }
                else
                    sspdoc.DocumentName = Convert.ToString(request.DocumentName);

                Guid obj = Guid.NewGuid();
                string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();

                if (Convert.ToInt64(request.Docid) == 0)
                {
                    sspdoc.CONDMAND = Convert.ToString(request.CONDMAND);
                    sspdoc.DOCUMENTTEMPLATE = Convert.ToString(request.DocTemplate);
                    sspdoc.DIRECTION = Convert.ToString(request.Direction);
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);

                    mFactory.SaveDocumentsWithParams(docentity, Convert.ToString(request.CreatedBy), "Uploaded_Shipa", obj.ToString(), EnvironmentName, "YES");
                }
                else
                {
                    DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);

                    mFactory.UpdateDocumentBytesCollabration(docentity, Convert.ToString(request.CreatedBy), obj.ToString(), EnvironmentName, Convert.ToString(request.CreatedBy));
                }

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Document Uploaded Successfully."
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "UploadDocs", ex.Message.ToString(), ex.StackTrace.ToString(), Convert.ToString(request.CreatedBy));
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/UpdateSaveforlater")]
        [HttpPost]
        public dynamic UpdateSaveforlater([FromBody]dynamic request)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.UpdateSaveforlaterfordocument(Convert.ToString(request.DOCID), Convert.ToString(request.DocupDate),
                    Convert.ToString(request.ProvidedBy), Convert.ToString(request.Status));

                //--------Send Mail notification if mandatory documents are empty... Anil G------------
                //if (Convert.ToString(request.Status) == "Upload later all")
                //    SendMailForMandatoryDocs(Convert.ToString(request.CreatedBy));
                //-------------------------------------------------------------------------------------

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Updated successfully."
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "UpdateSaveforlater", ex.Message.ToString(), ex.StackTrace.ToString(), Convert.ToString(request.ProvidedBy));
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/ClearUploadLater")]
        [HttpPost]
        public dynamic ClearUploadLater([FromBody]dynamic request)
        {
            try
            {
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.ClearUploadLater(Convert.ToString(request.DOCID));

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Cleared Successfully.."
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "ClearUploadLater", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/DeleteDocsbyDocId")]
        [HttpPost]
        public dynamic DeleteDocsbyDocId([FromBody]dynamic request)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                mFactory.DeleteDocContentbyDocId(Convert.ToInt64(request.DOCID), Convert.ToString(request.CreatedBy));

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "File content has been Deleted successfully."
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "DeleteDocsbyDocId", ex.Message.ToString(), ex.StackTrace.ToString(), Convert.ToString(request.CreatedBy));
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/SaveJob")]
        [HttpPost]
        public dynamic SaveJob(JobBookingModel model)
        {
            string oPNumber = model.JOPERATIONALJOBNUMBER;
            long mQuotationId;
            long id = model.QUOTATIONID;

            //Oversize cargo start ************ Oversize cargo start//
            if (!ReferenceEquals(model.ShipmentItems, null) && model.ShipmentItems.Count > 0)
            {
                foreach (var item in model.ShipmentItems)
                {
                    if (!string.IsNullOrEmpty(item.PackageTypeName) && (item.ItemLength != 0) && (item.ItemWidth != 0))
                    {
                        if (item.ItemHeight == 0)
                            item.ItemHeight = 227;
                    }
                    if (item.ItemHeight > 0)
                    {
                        if (string.IsNullOrEmpty(item.PackageTypeName) || (item.ItemLength == 0) || (item.ItemWidth == 0))
                            item.ItemHeight = 0;
                    }
                }
            }
            //Oversize cargo end ************ Oversize cargo end//

            string HSCode = "";
            string DestHSCode = "";

            DBFactory.Entities.JobBookingEntity mUIModel = Mapper.Map<JobBookingModel, DBFactory.Entities.JobBookingEntity>(model);
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            mUIModel.QUOTATIONID = id;

            if (mUIModel.SHPARTYDETAILSID > 0 && mUIModel.CONPARTYDETAILSID > 0)
            {
                if (!string.IsNullOrEmpty(model.JCARGODELIVERYBY.ToString()))
                    mUIModel.JCARGODELIVERYBY = model.JCARGODELIVERYBY;
                if (!string.IsNullOrEmpty(model.JCARGOAVAILABLEFROM.ToString()))
                    mUIModel.JCARGOAVAILABLEFROM = model.JCARGOAVAILABLEFROM;

                mFactory.UpdateJob(mUIModel, model.CREATEDBY, model.STATEID, HSCode, DestHSCode, "", "");
            }
            else
            {
                if (!string.IsNullOrEmpty(model.JCARGODELIVERYBY.ToString()))
                    mUIModel.JCARGODELIVERYBY = model.JCARGODELIVERYBY;
                if (!string.IsNullOrEmpty(model.JCARGOAVAILABLEFROM.ToString()))
                    mUIModel.JCARGOAVAILABLEFROM = model.JCARGOAVAILABLEFROM;
                mFactory.SaveJob(mUIModel, model.CREATEDBY, model.STATEID, HSCode, DestHSCode, "", "");

            }

            if (model.STATEID == "ContPayment")
            {

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        StateID = "BookingSummary",
                        QuotationID = mUIModel.QUOTATIONID,
                        JobNumber = mUIModel.JJOBNUMBER
                    })
                };
            }
            else
            {

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        StateID = "GetSavedBookList",
                        QuotationID = mUIModel.QUOTATIONID,
                        JobNumber = mUIModel.JJOBNUMBER
                    })
                };
            }
        }

        [Route("api/JoBookMB/AddNewParty")]
        [HttpPost]
        public dynamic AddNewParty([FromBody]dynamic Model)  //string SSPClientID, string CompanyName, string HouseNO, string BulName, string PAddrLine1, string PAddrLine2, string PostalCode,        //string FirstName, string LastName, string Email, string MobNumber, string CountryID, string CountryName, string StateID, string StateName, string CityID, string CityName, string Title, string CountryCode, string StateCode, string CityCode)
        {
            try
            {
                UserDBFactory DBF = new UserDBFactory();

                var ISDCODE = Model.ISDCODE;

                if (ISDCODE == null)
                    ISDCODE = "";
                else
                    ISDCODE = Convert.ToString(Model.ISDCODE);

                if (Convert.ToString(Model.SSPClientID.Value) == "0")
                {
                    // Save 
                    DataSet ds = DBF.AddeNewPartyAddress(Convert.ToString(Model.SSPClientID.Value), Convert.ToString(Model.CompanyName.Value), Convert.ToString(Model.HouseNO.Value),
                                Convert.ToString(Model.BulName.Value), Convert.ToString(Model.PAddrLine1.Value), Convert.ToString(Model.PAddrLine2.Value), Convert.ToString(Model.PostalCode.Value),
                               Convert.ToString(Model.FirstName.Value), Convert.ToString(Model.LastName.Value), Convert.ToString(Model.Email.Value), Convert.ToString(Model.MobNumber.Value),
                               Convert.ToString(Model.CountryName.Value), Convert.ToString(Model.StateName.Value), Convert.ToString(Model.CityName.Value), Convert.ToString(Model.Name.Value),
                               Convert.ToString(Model.CountryID.Value), Convert.ToString(Model.StateID.Value), Convert.ToString(Model.CityID.Value), Convert.ToString(Model.Title.Value),
                               Convert.ToString(Model.CountryCode.Value), Convert.ToString(Model.StateCode.Value), Convert.ToString(Model.CityCode.Value), Convert.ToString(Model.NiCkName.Value),
                               Convert.ToString(Model.TaxID.Value), string.Empty, ISDCODE);
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                        var JsonToReturn = new
                        {
                            rows = trows,
                        };
                        return BaseResponse(trows, HttpStatusCode.OK);
                    }
                    else
                    {
                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.ExpectationFailed,
                                Message = "No Data"
                            })
                        };
                    }
                }
                else // To Update
                {
                    var c = DBF.UpdtePartyAddress(Convert.ToString(Model.SSPClientID.Value), Convert.ToString(Model.CompanyName.Value), Convert.ToString(Model.HouseNO.Value),
                                 Convert.ToString(Model.BulName.Value), Convert.ToString(Model.PAddrLine1.Value), Convert.ToString(Model.PAddrLine2.Value), Convert.ToString(Model.PostalCode.Value),
                                Convert.ToString(Model.FirstName.Value), Convert.ToString(Model.LastName.Value), Convert.ToString(Model.Email.Value), Convert.ToString(Model.MobNumber.Value),
                                Convert.ToString(Model.CountryName.Value), Convert.ToString(Model.StateName.Value), Convert.ToString(Model.CityName.Value), Convert.ToString(Model.Name.Value),
                                Convert.ToString(Model.CountryID.Value), Convert.ToString(Model.StateID.Value), Convert.ToString(Model.CityID.Value), Convert.ToString(Model.Title.Value),
                                Convert.ToString(Model.CountryCode.Value), Convert.ToString(Model.StateCode.Value), Convert.ToString(Model.CityCode.Value), Convert.ToString(Model.NiCkName.Value), Convert.ToString(Model.TaxID.Value), string.Empty,
                                ISDCODE);
                    var alert = string.Empty;
                    if (c > 0)
                    {
                        alert = "Dear user, your request to edit the party details is sent to our helpdesk team to be changed in all our applications. Once approved, you will be informed and the new address can be used in jobs booked thereafter, until then you will be able to use the old address of the party only";
                    }
                    else
                    {
                        alert = "Success";
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = alert
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "AddNewParty", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/JoBookMB/DeleteParty")]
        [HttpPost]
        public dynamic DeleteParty([FromBody]dynamic Model)
        {
            return SafeRun<HttpResponseMessage>(() =>
           {

               if (Model.SSPClientID == null || Model.Email == null || Model.Email == "")
                   return BaseResponse("SSPClientID and Email parameters are cannot be empty.", HttpStatusCode.ExpectationFailed);

               UserDBFactory DBF = new UserDBFactory();
               decimal status = DBF.DeleteParty(Convert.ToInt32(Model.SSPClientID.Value), Model.Email.Value);
               if (status == 0)
                   return BaseResponse("Something went wrong with provided input details.", HttpStatusCode.ExpectationFailed);

               return BaseResponse("Party has been deleted successfully.", HttpStatusCode.OK);
           });
        }

        #endregion

        #region Additional Payments
        [Route("api/JoBookMB/AdditionalChargesSummary")]
        [HttpGet]
        public dynamic AdditionalChargesList(int JobNumber, int Id, int ChargeSetid, string SourceType = "")
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                string AddChargeSrc = string.Empty;

                DataTable AdditionalCharges = mFactory.GetAdditionalCharges(JobNumber, Id, SourceType);
                if (AdditionalCharges.Rows.Count > 0)
                {
                    AddChargeSrc = AdditionalCharges.Rows[0]["ADDITIONALCHARGESTATUS"].ToString();
                }

                if (AddChargeSrc != null && (AddChargeSrc.ToLower() == "pay now" || AddChargeSrc.ToLower() == "pay later"))
                {
                    DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetAdditionalChargeDetails(Convert.ToInt32(Id), Convert.ToInt64(JobNumber), AddChargeSrc, ChargeSetid);
                    int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mDBEntity.PREFERREDCURRENCYID);
                    IList<SSPJobDetailsEntity> jobdet = mDBEntity.JobItems;
                    IList<SSPPartyDetailsEntity> partydet = mDBEntity.PartyItems;
                    var shipperdet = (from u in partydet where u.PARTYTYPE == "91" select u).ToArray();
                    var consigneedet = (from u in partydet where u.PARTYTYPE == "92" select u).ToArray();
                    if (jobdet != null)
                    {
                        mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
                        mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
                        mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
                        mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
                        mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
                        mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
                        mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
                        mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
                        mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
                        mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
                        mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
                        mDBEntity.JSTATEID = jobdet[0].STATEID;

                    }
                    if (shipperdet != null && shipperdet.Length > 0)
                    {
                        mDBEntity.SHCLIENTNAME = shipperdet[0].CLIENTNAME;
                    }
                    if (consigneedet != null && consigneedet.Length > 0)
                    {
                        mDBEntity.CONCLIENTNAME = consigneedet[0].CLIENTNAME;
                    }
                    mDBEntity.CHARGESETID = ChargeSetid;
                    JobBookingModel mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                    mUIModel.DecimalCount = decimalCount;
                    mUIModel.PaymentCharges.ToList().ForEach(o => o.RNETAMOUNT = o.NETAMOUNT.ToString("N" + decimalCount));
                    return BaseResponse(mUIModel, HttpStatusCode.OK);
                }
                else
                    return BaseResponse(new { Error = "No Additional Charges Found." }, HttpStatusCode.ExpectationFailed);
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "AdditionalChargesSummary", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse(new { Error = "Something Went Wrong." }, HttpStatusCode.ExpectationFailed);
            }
        }

        [Route("api/JoBookMB/AdditionalChargeAutoApprove")]
        [HttpGet]
        public dynamic AdditionalChargeAutoApprove(int JobNumber, int ChargeSetId)
        {
            string user = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable UserDtls = mFactory.GetUserDtls(JobNumber);
            if (UserDtls.Rows.Count > 0)
            {
                user = UserDtls.Rows[0]["USERID"].ToString();
            }
            DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(JobNumber, user);
            PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);

            miUIModel.UserId = user;
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(Convert.ToInt32(miUIModel.QuotationId));
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
            try
            {
                GenerateApprovalpdf(miUIModel.QuotationId, miUIModel.JobNumber, ChargeSetId, user);
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "AdditionalChargeAutoApprove(GenerateApprovalpdf)", ex.Message.ToString(), ex.StackTrace.ToString());
                string tes = ex.ToString();
            }

            if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay later")
            {
                PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
                try
                {
                    BusinessCreditmodel.RECEIPTHDRID = miUIModel.RECEIPTHDRID;
                    BusinessCreditmodel.JOBNUMBER = miUIModel.JobNumber;
                    BusinessCreditmodel.QUOTATIONID = miUIModel.QuotationId;
                    BusinessCreditmodel.PAYMENTOPTION = "Pay Later";
                    BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                    BusinessCreditmodel.PAYMENTREFNUMBER = "Pay Later";
                    BusinessCreditmodel.PAYMENTREFMESSAGE = "approved_Additional";
                    BusinessCreditmodel.CREATEDBY = user;
                    BusinessCreditmodel.MODIFIEDBY = user;
                    BusinessCreditmodel.PAYMENTTYPE = "Approved by customer";
                    BusinessCreditmodel.TRANSMODE = "Approved by customer";
                    BusinessCreditmodel.TRANSAMOUNT = Convert.ToDouble(miUIModel.ReceiptNetAmount);
                    BusinessCreditmodel.TRANSCURRENCY = miUIModel.Currency;
                    BusinessCreditmodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                    BusinessCreditmodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                    BusinessCreditmodel.ADDITIONALCHARGESTATUS = "Approved by customer";

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);
                    string result = mFactory.AdditionalChargeAutoApprove(mUIModel, user);
                    SendMailForAdditionalChargesApproved(Convert.ToInt32(miUIModel.JobNumber), miUIModel.OPjobnumber, Convert.ToDecimal(miUIModel.ADDITIONALCHARGEAMOUNT), miUIModel.Currency);

                    return BaseResponse(new { Message = "Success" }, HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    LogError("api/JoBookMB", "AdditionalChargeAutoApprove", ex.Message.ToString(), ex.StackTrace.ToString());
                    return BaseResponse(new { Error = "Failed" }, HttpStatusCode.ExpectationFailed);
                }
            }
            else
            {
                return BaseResponse(new { Error = "Something Went Wrong." }, HttpStatusCode.ExpectationFailed);
            }
        }

        public void GenerateApprovalpdf(long id, long JJOBNUMBER, int ChargeSetId, string user)
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string TCount = string.Empty;
            DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetAdditionalChargeDetails(Convert.ToInt32(id), Convert.ToInt64(JJOBNUMBER), "Pay Later", ChargeSetId);
            JobBookingModel mUIModels = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
            PreferredCurrencyId = mUIModels.PREFERREDCURRENCYID;
            DataTable AdvReceiptChargeDtls = null;
            DBFactory.Entities.AdvanceReceiptEntity AdvReceiptDtls = mFactory.GetAdvanceReceiptDtls(Convert.ToInt32(JJOBNUMBER));
            AdvReceiptChargeDtls = mFactory.GetAdditionalChargeSum(Convert.ToInt32(JJOBNUMBER), ChargeSetId);
            if (AdvReceiptChargeDtls.Rows.Count > 0)
            {
                TCount = AdvReceiptChargeDtls.Rows[0]["TCOUNT"].ToString();
            }
            else
            {
                TCount = "1";
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bytes = GenerateAdditionalChargeApprovePDF(memoryStream, mUIModels, mUIModels.JobItems[0].OPERATIONALJOBNUMBER, mUIModels.JobItems[0].ADDITIONALCHARGEAMOUNT);
                MemoryStream stream = new MemoryStream(bytes);
                //Insert same data into db 
                SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                sspdoc.QUOTATIONID = Convert.ToInt64(id);
                sspdoc.DOCUMNETTYPE = "ShipAFreightDoc";
                DateTime today = DateTime.Today;
                sspdoc.FILENAME = "AddChargeApproval_" + TCount + ".pdf";
                sspdoc.FILEEXTENSION = "application/pdf";
                sspdoc.FILESIZE = bytes.Length;
                sspdoc.FILECONTENT = bytes;
                sspdoc.JOBNUMBER = JJOBNUMBER;
                sspdoc.DocumentName = "Approval Letter";
                DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

                Guid obj = Guid.NewGuid();
                string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();

                long mQuotationId = mjobFactory.SaveDocuments(docentity, user, "System Generated", obj.ToString(), EnvironmentName);
            }
        }

        public static byte[] GenerateAdditionalChargeApprovePDF(MemoryStream memoryStream, JobBookingModel mUIModels, string bookingId, decimal amount)
        {
            PDFpage = 0;
            UserDBFactory mFactory = new UserDBFactory();
            DUserModel = mFactory.GetUserProfileDetails(mUIModels.CREATEDBY, 1);

            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mUIModels.PREFERREDCURRENCYID);

            mQuoteChargesTotal = 0;
            Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();
            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = "Approval Letter";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);
            doc.Add(mFormattingpdf.LineSeparator());

            Formattingpdf mSubpdf = new Formattingpdf();
            PdfPTable mSubtable = new PdfPTable(1);
            mSubtable.WidthPercentage = 100;
            mSubtable.SpacingBefore = 25;
            mSubtable.DefaultCell.Border = 0;

            mSubtable.AddCell((mSubpdf.TableContentCell("Sub: Approval of Additional charges to be billed during final invoice for booking " + bookingId + ".")));
            doc.Add(mSubtable);

            Formattingpdf mSubpdf1 = new Formattingpdf();
            PdfPTable mSubtable1 = new PdfPTable(1);
            mSubtable1.WidthPercentage = 100;
            mSubtable1.SpacingBefore = 25;
            mSubtable1.SpacingAfter = 20;
            mSubtable1.DefaultCell.Border = 0;

            mSubtable1.AddCell((mSubpdf1.TableContentCell("There is an additional charges of " + mUIModels.PREFERREDCURRENCYID + " " + amount.ToString("N" + decimalCount) + " for the booking " + bookingId + ". This is an approval letter for consent to be billed later for this particular booking and for reference by the origin and destination branches. ")));
            doc.Add(mSubtable1);

            if (mUIModels.PaymentCharges.Count() > 0)
            {
                var mAddcharges = mUIModels.PaymentCharges.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase);
                if (mAddcharges.Count() > 0)
                    doc.Add(ChargesTableAutoApprovalModel(mAddcharges, "Additional Charges", mUIModels.PREFERREDCURRENCYID, decimalCount));
            }

            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotalBorder.WidthPercentage = 100;
            mQuoteChargeTotalBorder.SpacingBefore = 10;
            mQuoteChargeTotalBorder.DefaultCell.Border = 0;
            mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

            PdfPTable mQuoteChargeTotal = new PdfPTable(2);
            mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotal.WidthPercentage = 100;
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PREFERREDCURRENCYID + " " + mQuoteChargesTotal.ToString("N" + decimalCount), Element.ALIGN_RIGHT)));

            PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
            mQuoteChargeTotalBorder.AddCell(mTotalcells);
            doc.Add(mQuoteChargeTotalBorder);

            Formattingpdf mFrompdf = new Formattingpdf();
            PdfPTable mFromtable = new PdfPTable(1);
            mFromtable.WidthPercentage = 100;
            mFromtable.SpacingBefore = 10;
            mFromtable.DefaultCell.Border = 0;

            mFromtable.AddCell((mFrompdf.TableContentCell("Approved By,")));
            mFromtable.AddCell((mFrompdf.TableContentCell(DUserModel.FIRSTNAME + " " + DUserModel.LASTNAME)));
            DateTime today = DateTime.Today;
            mFromtable.AddCell(mFrompdf.TableContentCell(today.ToString("dd/MM/yyyy")));
            doc.Add(mFromtable);

            doc.Close();

            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        public static PdfPTable ChargesTableAutoApprovalModel(IEnumerable<PaymentCharges> model, string mHeaderText, string CurrencyId, int decimalCount)
        {
            try
            {
                if (PreferredCurrencyId == null || PreferredCurrencyId == "")
                {
                    PreferredCurrencyId = CurrencyId;
                }
                var ChargesTotal = Double.Parse(model.Sum(sum => sum.NETAMOUNT).ToString());
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(PreferredCurrencyId + " " + ChargesTotal.ToString("N" + decimalCount), Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.CHARGENAME, Element.ALIGN_LEFT)));
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(Double.Parse(QC.NETAMOUNT.ToString()).ToString("N" + decimalCount).ToString(), Element.ALIGN_RIGHT)));
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;

                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SendMailForAdditionalChargesApproved(int JobNumber, string BookingId, decimal AdditionalChargeAmount, string Currency)
        {
            try
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                string FirstName = string.Empty;
                string UserID = string.Empty;
                string Message = string.Empty;
                DataTable UserDtls = mFactory.GetUserDtls(JobNumber);
                if (UserDtls.Rows.Count > 0)
                {
                    FirstName = UserDtls.Rows[0]["FIRSTNAME"].ToString();
                    UserID = UserDtls.Rows[0]["USERID"].ToString();
                }
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(UserID));
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - Additional charges accepted by customer." + EnvironmentName;

                Message = "Additional charge accepted by customer.";
                string amount = mFactoryRound.RoundedValue(AdditionalChargeAmount.ToString("N2"), Currency) + " " + Currency;
                message.IsBodyHtml = true;
                string mbody = string.Empty;
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/AddtionalChargeApprovedNotification.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { mbody = reader.ReadToEnd(); }
                mbody = mbody.Replace("{statusMessage}", Message);
                mbody = mbody.Replace("{user_name}", FirstName);
                mbody = mbody.Replace("{Amount}", amount);
                mbody = mbody.Replace("{BookingID}", BookingId);
                mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = mbody;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", UserID, "", "",
            "JobBooking", "Additional charges accepted", BookingId, UserID, mbody, "", true, message.Subject.ToString());
                SendMailtoGroup(BookingId, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE", "Paymentstatus", JobNumber, UserID, "Additional Charges", "Additional Charges");
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "SendMailForAdditionalChargesApproved", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ArgumentException(ex.ToString());
            }
        }

        private void SendMailtoGroup(string BookingId, string roles, string partyType, int JobNumber, string user, string Paymentmethod, string PaymentType)
        {
            string AttachmentIds = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            try
            {
                recipientsList = mFactory.GetEmailList(roles, BookingId, partyType);

                DataSet documents = mFactory.GetDocuments(JobNumber);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");

                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - Payment Process" + EnvironmentName;
                message.IsBodyHtml = true;
                string mbody = string.Empty;
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/GSSC-PaymentProcess.html");
                if (PaymentType == string.Empty)
                    PaymentType = Paymentmethod;

                using (StreamReader reader = new StreamReader(mapPath))
                { mbody = reader.ReadToEnd(); }
                mbody = mbody.Replace("{PaymentProcess}", Paymentmethod);
                mbody = mbody.Replace("{Reference_No}", BookingId);
                mbody = mbody.Replace("{PaymentType}", PaymentType);
                mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.Body = mbody;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
           "JobBooking", "Payment Process", BookingId, user, mbody, AttachmentIds, false, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "SendMailtoGroup", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException(ex.ToString());
            }
        }


        #endregion

        #region CancelJob

        [Route("api/JoBookMB/CancelJob")]
        [HttpPost]
        public dynamic CancelJob([FromBody]dynamic req)
        {
            try
            {
                string result = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                result = mFactory.RequestCancelJob(Convert.ToInt32(req.JOBNUMBER), Convert.ToString(req.OPERATIONALJOBNUMBER), Convert.ToString(req.REASONDESCRIPTION), Convert.ToString(req.REASON), Convert.ToString(req.USER));
                if (result.ToLower() == "success")
                {
                    string recipientsList = string.Empty;
                    string User = string.Empty;
                    try
                    {
                        SendEmailUpdateJobStatus(Convert.ToInt32(req.JOBNUMBER), Convert.ToString(req.OPERATIONALJOBNUMBER), "Cancellation Pending", Convert.ToString(req.USER), "User");
                    }
                    catch
                    {
                        //
                    }
                    try
                    {
                        SendEmailUpdateJobStatus(Convert.ToInt32(req.JOBNUMBER), Convert.ToString(req.OPERATIONALJOBNUMBER), "Cancellation Pending", Convert.ToString(req.USER));
                    }
                    catch
                    {
                        //
                    }
                    return BaseResponse(new { Message = "Cancellation request submitted to operation team." }, HttpStatusCode.OK);
                }
                else
                {
                    return BaseResponse(new { Message = "Something went wrong. Please try again." }, HttpStatusCode.ExpectationFailed);
                }
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "CancelJob", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse(new { Message = "Something went wrong. Please try again." }, HttpStatusCode.InternalServerError);
            }
        }

        private void SendEmailUpdateJobStatus(int JobNumber, string BookingId, string Status, string User, string type = "")
        {
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            string recipientsList = "";
            try
            {
                MailMessage message = new MailMessage();
                string mbody = string.Empty;
                string team = string.Empty;
                message.From = new MailAddress("noreply@shipafreight.com");
                if (string.IsNullOrEmpty(type))
                {
                    recipientsList = mFactory.GetEmailList("SSPGSSC", BookingId, "");
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));

                    }
                }
                else
                {
                    message.To.Add(new MailAddress(User));
                }
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();

                if (string.IsNullOrEmpty(type))
                {
                    message.Subject = "ShipA Freight - Cancellation Approval Pending  of BookingId " + BookingId + EnvironmentName;
                }
                else
                {
                    message.Subject = "ShipA Freight - Cancellation Requested of BookingId " + BookingId + EnvironmentName;
                }
                message.IsBodyHtml = true;

                string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/UpdateJobStatusCancellation.html");

                using (StreamReader reader = new StreamReader(mapPath))
                { mbody = reader.ReadToEnd(); }

                if (string.IsNullOrEmpty(type))
                {
                    string l1 = "User (" + User + ") has made a request to cancel  job booking " + BookingId + ".";
                    string l2 = "Please review the booking details and facilitate the cancellation.";
                    mbody = mbody.Replace("{Message1}", l1);
                    mbody = mbody.Replace("{Message2}", l2);
                    mbody = mbody.Replace("{Message3}", string.Empty);
                    mbody = mbody.Replace("{CnlButton}", string.Empty);
                }
                else
                {
                    string resetLink = string.Empty;
                    int id = mFactory.getQuotationId(JobNumber);
                    string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                    string url = "JobNumber=" + JobNumber + "?id=" + id;
                    dynamic t1 = EncryptId(url);
                    string rs = t1.ToString();
                    resetLink = Actionlink + "JobBooking/BookingSummary" + rs;
                    string CnlButton = "<td style=font-family: 'Open Sans', sans-serif; background-color: #f29221; padding: 10px; text-align: center; color: #FFF;><a href=" + resetLink + " style=height: 50px; width: 100px; text-align: center; color: #FFF; background-color: #f29221; text-decoration: none; text-transform: uppercase>" + Status.ToUpper() + "</a></td>";

                    string l1 = "We’ve received your request to cancel job booking " + BookingId + " . Our team is reviewing the current status of your shipment and assessing the necessary arrangements to cancel the job.";
                    string l2 = "We’ll send you a confirmation once we have cancelled the job and issued a refund for the quote amount.";
                    string l3 = "Click on the link below to monitor the status of your cancellation through the ShipA Freight portal.";
                    mbody = mbody.Replace("{Message1}", l1);
                    mbody = mbody.Replace("{Message2}", l2);
                    mbody = mbody.Replace("{Message3}", l3);
                    mbody = mbody.Replace("{CnlButton}", CnlButton);
                }
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(User);
                team = (string.IsNullOrEmpty(type)) ? "Team" : UE.FIRSTNAME;
                mbody = mbody.Replace("{Team}", team);
                mbody = mbody.Replace("{Reference_No}", BookingId);
                mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                message.Body = mbody;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                if (string.IsNullOrEmpty(type))
                {
                    APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
               "JobBooking", "Job Cancellation", BookingId, User, mbody, "", false, message.Subject.ToString());
                }
                else
                {
                    APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                  "JobBooking", "Job Cancellation", BookingId, User, mbody, "", true, message.Subject.ToString());
                }
            }
            catch (Exception ex)
            {
                LogError("api/JoBookMB", "SendEmailUpdateJobStatus", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException(ex.ToString());
            }
        }

        public dynamic EncryptId(string plainText)
        {
            StringBuilder Result = new StringBuilder();

            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

            string[] mul = plainText.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            string JResult = Result.ToString();
            return JResult;
        }

        #endregion

        #region Collabaration

        [Route("api/JoBookMB/Collabrate")]
        [HttpPost]
        public dynamic CollabrateSave([FromBody]CollabarateModel Req)
        {
            if (!ModelState.IsValid) return ErrorResponse(ModelState);

            return SafeRun<dynamic>(() =>
            {
                string Name = "";
                string Con = "";
                string docnames = string.Empty;
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(Req.UserId);
                if (!string.IsNullOrEmpty(Req.ShipperName))
                {
                    string[] ShName = Req.ShipperName.Split(' ');
                    Name = APIDynamicClass.EncryptStringAES(ShName[0]);
                }
                if (!string.IsNullOrEmpty(Req.ConsigneeName))
                {
                    string[] ConnName = Req.ConsigneeName.Split(' ');
                    Con = APIDynamicClass.EncryptStringAES(ConnName[0]);
                }
                if (!string.IsNullOrEmpty(Req.DocNames))
                {

                    if (Req.DocNames.Length > 1)
                    {
                        var names = Req.DocNames.Split(',');
                        foreach (var item in names)
                        {
                            docnames = docnames + "<ul><li>" + item + "</li></ul>";
                        }
                    }
                    else
                    {
                        docnames = Req.DocNames;
                    }
                }
                JobBookingDbFactory JBDF = new JobBookingDbFactory();
                JBDF.UpdateCollabratedocument(Req.DOCID, Req.DocupDate, Req.ProvidedBy, Req.ProviderEmailId);
                string Jobnumber = APIDynamicClass.EncryptStringAES(Req.Jobnumber);
                string Quoteid = APIDynamicClass.EncryptStringAES(Req.QuoatationNo);
                string Providermailid = APIDynamicClass.EncryptStringAES(Req.ProviderEmailId);
                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                string resetLink = Actionlink + "/Documents/Opendocuments?jobnumber=" + HttpUtility.UrlEncode(Jobnumber)
                        + "&quoteno=" + HttpUtility.UrlEncode(Quoteid)
                        + "&Provideremailid=" + HttpUtility.UrlEncode(Providermailid)
                        + "&CargoAvailable=" + HttpUtility.UrlEncode(Req.CargoAvailable)
                        + "&Con=" + HttpUtility.UrlEncode(Con)
                        + "&Name=" + HttpUtility.UrlEncode(Name);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(Req.ProviderEmailId));
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Action Requested: Upload Documents for Upcoming Shipment" + EnvironmentName;
                message.IsBodyHtml = true;
                string mbody = string.Empty;
                string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/Collabate.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { mbody = reader.ReadToEnd(); }
                mbody = mbody.Replace("{user}", Req.ShipperName);
                mbody = mbody.Replace("{user_created}", UE.FIRSTNAME + " " + UE.LASTNAME + " (" + Req.UserId + ") ");
                mbody = mbody.Replace("{url}", resetLink);
                mbody = mbody.Replace("{docnames}", docnames);
                mbody = mbody.Replace("{Identity}", Req.NoteToShipper);
                mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = mbody;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                return BaseResponse(new { Message = "Success" }, HttpStatusCode.OK);
            });
        }

        [Route("api/JoBookMB/Getdocumentslist")]
        [HttpPost]
        public dynamic Getdocumentslist([FromBody]CollabarateDocModel Req)
        {
            if (!ModelState.IsValid) return ErrorResponse(ModelState.Values);
            return SafeRun<dynamic>(() =>
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                DataTable dt = mFactory.Getdocumentnames(Req.Jobnumber.ToString(), Req.QuoatationNo.ToString(), Req.UserId);
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> trows = ReturnJsonResult(dt);
                    return BaseResponse(trows, HttpStatusCode.OK);
                }
                return ErrorResponse("No Documents found");
            });
        }

        [Route("api/JoBookMB/CollabrateCargo")]
        [HttpPost]
        public dynamic CollabrateCargo([FromBody]CollabrateCargoModel Req)
        {
            if (!ModelState.IsValid) return ErrorResponse(ModelState.Values);
            return SafeRun<dynamic>(() =>
            {
                JobBookingDbFactory UD = new JobBookingDbFactory();
                string OwnerFN = UD.GetUserFirstNInsertSupplier(Req.OperationalJobnumber, Req.ShipperName, Req.UserId, Req.NoteToShipper);

                MailMessage message = new MailMessage();
                string body = string.Empty;
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(Req.ShipperEmailId));

                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "ShipA Freight - Action Requested: Provide Details for Upcoming Shipment" + EnvironmentName;

                message.IsBodyHtml = true;
                string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/CollaborationInitiate.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                string l1 = "Shipa Freight user " + OwnerFN + " has requested that you provide exact weights, dimensions, pickup location and/or other details for an upcoming shipment.";
                string l2 = "To ensure timely pickup of your cargo, please provide shipment details as soon as possible by clicking the link below and following the instructions on the page.";
                string l3 = "Shipa Freight User Notes:";

                string resetLink = string.Empty;
                string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
                string url = "id=" + Req.QuoatationNo + "?col=" + Req.ShipperEmailId;
                string rs = EncryptId(url);
                resetLink = Actionlink + "Quotation/CargoCheck" + rs;
                string ColButton = "<td style=font-family: 'Open Sans', sans-serif; background-color: #f29221; padding: 10px; text-align: center; color: #FFF;><a href=" + resetLink + " style=height: 50px; width: 100px; text-align: center; color: #FFF; background-color: #f29221; text-decoration: none; text-transform: uppercase>Update Shipment Details</a></td>";

                body = body.Replace("{Team}", Req.ShipperName);
                body = body.Replace("{Message1}", l1);
                body = body.Replace("{Message2}", l2);
                body = body.Replace("{Message3}", l3);
                body = body.Replace("{Message4}", Req.NoteToShipper);
                body = body.Replace("{ColButton}", ColButton);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();

                APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Req.ShipperEmailId, "", "",
              "JobBooking", "Collaboration Initiate", Req.OperationalJobnumber, Req.ShipperEmailId, body, "", true, message.Subject.ToString());
                var result = "A request to update shipment details has been sent to the shipper. You will be notified and can continue booking once shipment details are updated.";
                return BaseResponse(result, HttpStatusCode.OK);
            });
        }

        [Route("api/JoBookMB/CollabrateUploadDoc")]
        [HttpPost]
        public dynamic UploadDocuments_NewCollabration([FromBody]CollabrateUploadDocModel request)
        {
            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            byte[] fbytes = System.Convert.FromBase64String(Convert.ToString(request.stringInBase64));
            sspdoc.FILENAME = request.FileName;
            sspdoc.FILEEXTENSION = request.ContentType;
            sspdoc.FILESIZE = request.ContentLength;
            sspdoc.FILECONTENT = fbytes;
            sspdoc.JOBNUMBER = request.JobNumber;

            Guid obj = Guid.NewGuid();
            string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();

            if (request.IsAdditionalDoc)
            {
                sspdoc.QUOTATIONID = Convert.ToInt64(request.QuoteId);
                sspdoc.CONDMAND = request.COMAND;
                sspdoc.DOCUMENTTEMPLATE = request.DOCUMENTTEMPLATE;
                sspdoc.DocumentName = request.DocNames;
                sspdoc.DOCUMNETTYPE = request.DOCUMENTTYPE;
                sspdoc.DIRECTION = request.DIRECTION;
                DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                request.DOCID= mFactory.SaveDocumentsWithParams(docentity, request.UserId, "Uploaded_Shipa", 
                    obj.ToString(), EnvironmentName, request.ModifiedBy, "YES");

            }
            else
            {
                sspdoc.DOCID = request.DOCID;
                DBFactory.Entities.SSPDocumentsEntity docentit = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                mFactory.UpdateDocumentBytesCollabration(docentit, request.UserId, obj.ToString(), EnvironmentName, request.ModifiedBy);
            }

            SafeRun<bool>(() =>
            {
                // Send Email
                SendUploadStatusEmail(
                    request.DocNames,
                    request.ModifiedBy,
                    request.OperationalJobNumber,
                    Convert.ToInt32(request.JobNumber),
                    request.QuoteId,
                    request.QuoatationNo,
                    request.UserId
                );
                return true;
            });

            return BaseResponse(new { Message= "File uploaded Successfully!!!", DOCID = request.DOCID }, HttpStatusCode.OK);
        }

        private void SendUploadStatusEmail(string DOCID, string ShipperMailid,
            string operationaljobnumber, int Jobnumber, int QuoteId, string QuotationNumber, string createdby)
        {
            UserDBFactory UserFactory = new UserDBFactory();

            string[] names;
            string docnames = string.Empty;

            if (!string.IsNullOrEmpty(DOCID))
            {

                if (DOCID.Length > 1)
                {
                    names = DOCID.Split(',');
                    foreach (var item in names)
                    {
                        docnames = docnames + "<ul><li>" + item + "</li></ul>";
                    }
                }
                else
                {
                    docnames = DOCID;
                }
            }
            UserDBFactory UD = new UserDBFactory();
            UserEntity UE = UD.GetUserFirstAndLastName(createdby);
            string user = createdby;
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(user));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = "Notice: Shipper" + " " + ShipperMailid + " " + "Uploaded" + " " + DOCID + " "
                + "for Booking" + " " + operationaljobnumber + EnvironmentName;
            message.IsBodyHtml = true;
            string body = string.Empty;
            string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/UploadComplte.html");
            string bodymsg = @"<p>Shipper" + " " + ShipperMailid + " " + "uploaded" + " " + DOCID + " " + "for booking " +
                " " + operationaljobnumber + ".</p>" + @"<p>Visit Shipa Freight to view this and other 
                documents relating to this booking.</p>";
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{user}", UE.FIRSTNAME);
            body = body.Replace("{body}", bodymsg);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            UserFactory.NewNotification(5120, "Shipper Uploaded Documents", operationaljobnumber, Jobnumber,
                QuoteId, QuotationNumber, user);

        }

        #endregion


        #region Download Quote Reference Fn's
        //public static byte[] GeneratePDF(MemoryStream memoryStream, QuotationPreviewModel mUIModels)
        //{
        //    PDFpage = 0;
        //    UserDBFactory mFactory = new UserDBFactory();
        //    DUserModel = mFactory.GetUserProfileDetails(mUIModels.CreatedBy, 1);
        //    mQuoteChargesTotal = 0;
        //    Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
        //    PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
        //    doc.Open();
        //    writer.PageEvent = new ITextEvents();

        //    PdfPTable mHeadingtable = new PdfPTable(1);
        //    mHeadingtable.WidthPercentage = 100;
        //    mHeadingtable.DefaultCell.Border = 0;
        //    mHeadingtable.SpacingAfter = 5;
        //    string mDocumentHeading = (mUIModels.ProductId == 2) ? "Ocean Freight Quote" : "Air Freight Quote";
        //    Formattingpdf mFormattingpdf = new Formattingpdf();
        //    PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
        //    mHeadingtable.AddCell(quotationheading);
        //    doc.Add(mHeadingtable);
        //    doc.Add(mFormattingpdf.LineSeparator());

        //    doc.Add(QuoteInfoTableModel(mUIModels));
        //    doc.Add(ShipInfoTableModel(mUIModels));

        //    PdfPTable mitemdescriptiontable = new PdfPTable(1);
        //    mitemdescriptiontable.WidthPercentage = 100;
        //    mitemdescriptiontable.SpacingBefore = 10;
        //    mitemdescriptiontable.DefaultCell.Border = 0;
        //    mitemdescriptiontable.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Item Description", Element.ALIGN_LEFT, false)));

        //    PdfPTable mItemDescription = new PdfPTable(1);
        //    mItemDescription.SetWidths(new float[] { 100 });
        //    mItemDescription.WidthPercentage = 100;
        //    mItemDescription.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mUIModels.ShipmentDescription == null ? "" : mUIModels.ShipmentDescription, Element.ALIGN_LEFT)));

        //    PdfPCell mRoutecell = new PdfPCell(mItemDescription);
        //    mRoutecell.Colspan = 2;
        //    mRoutecell.BorderColor = mFormattingpdf.TableBorderColor;
        //    mitemdescriptiontable.AddCell(mRoutecell);
        //    doc.Add(mitemdescriptiontable);

        //    if (mUIModels.QuotationCharges.Count() > 0)
        //    {
        //        var mOrigincharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        var mDestinationcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        var mINTcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        var mAddcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
        //        if (mOrigincharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mUIModels.PreferredCurrencyId));

        //        if (mINTcharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mINTcharges, "International Charges", mUIModels.PreferredCurrencyId));

        //        if (mDestinationcharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mUIModels.PreferredCurrencyId));

        //        if (mAddcharges.Count() > 0)
        //            doc.Add(ChargesTableModel(mAddcharges, "Additional Charges", mUIModels.PreferredCurrencyId));
        //    }
        //    PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
        //    mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
        //    mQuoteChargeTotalBorder.WidthPercentage = 100;
        //    mQuoteChargeTotalBorder.SpacingBefore = 10;
        //    mQuoteChargeTotalBorder.DefaultCell.Border = 0;
        //    mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

        //    PdfPTable mQuoteChargeTotal = new PdfPTable(2);
        //    mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
        //    mQuoteChargeTotal.WidthPercentage = 100;
        //    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
        //    //mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PreferredCurrencyId + " " + mQuoteChargesTotal.ToString(), Element.ALIGN_RIGHT)));
        //    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PreferredCurrencyId + " " + Double.Parse(mQuoteChargesTotal.ToString()), Element.ALIGN_RIGHT)));

        //    PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
        //    mQuoteChargeTotalBorder.AddCell(mTotalcells);
        //    doc.Add(mQuoteChargeTotalBorder);
        //    doc.NewPage();
        //    mHeadingtable.AddCell(quotationheading);
        //    doc.Add(TermsandConditionsTableModel(mUIModels));

        //    doc.Close();

        //    bytes = memoryStream.ToArray(); ;
        //    Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        PdfReader reader = new PdfReader(bytes);
        //        using (PdfStamper stamper = new PdfStamper(reader, stream))
        //        {
        //            int pages = reader.NumberOfPages;
        //            for (int i = 1; i <= pages; i++)
        //            {
        //                ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
        //            }
        //        }
        //        bytes = stream.ToArray();
        //    }
        //    return bytes;
        //}


        public static PdfPTable ShipInfoTableModel(QuotationPreviewModel model)
        {
            int mProductId = model.ProductId;
            int mProductTypeId = model.ProductTypeId;
            string mCargoDescription = model.ShipmentDescription;
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mShipbordertable = new PdfPTable(1);
                mShipbordertable.WidthPercentage = 100;
                mShipbordertable.SpacingBefore = 10;
                mShipbordertable.DefaultCell.Border = 0;

                List<PdfPCell> mHeaderLabels = new List<PdfPCell>();
                PdfPTable mShipTab = null;

                mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Quantity", Element.ALIGN_LEFT, false)));

                if (mProductTypeId == 5)
                {
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Container Size", Element.ALIGN_LEFT, false)));
                    mShipTab = new PdfPTable(2);
                    mShipTab.SetWidths(new float[] { 20, 80 });
                }
                else
                {
                    mShipTab = new PdfPTable(5);
                    mShipTab.SetWidths(new float[] { 10, 10, 10, 10, 10 });
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Package Type", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Dimension(L*W*H)", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Per Piece", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Gross Weight", Element.ALIGN_LEFT, false)));
                }
                mShipTab.WidthPercentage = 100;

                mShipbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Cargo Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                foreach (var mLabel in mHeaderLabels)
                {
                    mShipTab.AddCell(mLabel);
                }
                foreach (var mShipment in model.ShipmentItems)
                {
                    mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.Quantity.ToString(), Element.ALIGN_LEFT)));
                    if (mProductTypeId == 5)
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ContainerName, Element.ALIGN_LEFT)));
                    else
                    {
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.PackageTypeName, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ItemLength + " x " + mShipment.ItemWidth + " x " + mShipment.ItemHeight, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTPERPIECE) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTPERPIECE) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTTOTAL) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTTOTAL) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                    }
                }
                PdfPCell mShipcell = new PdfPCell(mShipTab);
                mShipcell.BorderColor = mFormattingpdf.TableBorderColor;
                mShipbordertable.AddCell(mShipcell);

                if (mProductTypeId != 5)
                {
                    string mTotalCharegebleVolume = string.Empty;
                    if (mProductId == 2)
                        mTotalCharegebleVolume = "Revenue Ton" + " : " + (IsNullOrDefault(model.ChargeableVolume) ? "0.00" : (model.ChargeableVolume) <= 1 ? String.Format(culture, format, model.ChargeableVolume) + " " + "(minimum)" : String.Format(culture, format, model.ChargeableVolume));
                    else
                        mTotalCharegebleVolume = "Volumetric Weight (Kgs)" + " : " + (IsNullOrDefault(model.VolumetricWeight) ? "0.0" : String.Format(culture, format, model.VolumetricWeight));

                    string mTotalGW = "Total Gross Weight (Kgs)" + " : " + (IsNullOrDefault(model.TotalGrossWeight) ? "0.0" : String.Format(culture, format, model.TotalGrossWeight));
                    string mTotalCBM = "Total Cubic Meters" + " : " + (IsNullOrDefault(model.TotalCBM) ? "0.000" : String.Format(culture, format, model.TotalCBM));
                    string mTotalCharegebleWeight = "Chargeable Weight" + " : " + (IsNullOrDefault(model.ChargeableWeight) ? "0.00" : String.Format(culture, format, model.ChargeableWeight));

                    PdfPTable mShipmentTotalWeights;

                    if (mProductId == 2)
                    {
                        mShipmentTotalWeights = new PdfPTable(3);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.SetWidths(new float[] { 53, 56, 77 });
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    else
                    {
                        mShipmentTotalWeights = new PdfPTable(4);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleWeight, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    PdfPCell mTableFooterCell = mFormattingpdf.TableHeading("ShipmentDetails", Element.ALIGN_LEFT, false);
                    mTableFooterCell.Colspan = mShipTab.NumberOfColumns;
                    mTableFooterCell.Padding = 0;
                    mTableFooterCell.BackgroundColor = BaseColor.WHITE;
                    mTableFooterCell.AddElement(mShipmentTotalWeights);
                    mShipbordertable.AddCell(mTableFooterCell);
                }
                return mShipbordertable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        static bool IsNullOrDefault<T>(T value)
        {
            return object.Equals(value, default(T));
        }
        public static PdfPCell ImageCell(string path, float scale, int align)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath(path));
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 0f;
            cell.PaddingTop = 0f;
            return cell;
        }
        public static PdfPTable QuoteInfoTableModel(QuotationPreviewModel model)
        {
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mQuotebordertable = new PdfPTable(3);
                mQuotebordertable.WidthPercentage = 100;
                mQuotebordertable.SpacingBefore = 10;
                mQuotebordertable.DefaultCell.Border = 0;

                PdfPTable mQuoteTab = new PdfPTable(9);
                mQuoteTab.SetWidths(new float[] { 15, 1, 15, 13, 1, 20, 15, 1, 20 });
                mQuoteTab.WidthPercentage = 100;
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Mode of transport")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.ProductName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.OriginPlaceName == null ? "" : " Origin City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : model.OriginPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.DestinationPlaceName == null ? "" : "Destination City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : model.DestinationPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Movement type")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.MovementTypeName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Port of loading")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPortName == null ? "" : model.OriginPortName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Port of discharge")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPortName == null ? "" : model.DestinationPortName)));
                if (model.co2emission != 0)
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("CO2 Emission")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.co2emission == 0 ? "" : Convert.ToString(model.co2emission) + " KG")));
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
                if (model.OriginZipCode != null || model.DestinationZipCode != null)
                {
                    if (model.OriginZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("ZipCode")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginZipCode)));
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                    if (model.DestinationZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("ZipCode")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationZipCode)));
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }

                PdfPCell mRoutecell = new PdfPCell(mQuoteTab);
                mRoutecell.Colspan = 3;
                mQuotebordertable.AddCell(mRoutecell);

                return mQuotebordertable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public class ITextEvents : PdfPageEventHelper
        {
            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                string format = "MMMM d, yyyy";
                CultureInfo ci = new CultureInfo("en-US");
                string TaxNum = "";
                if (PDFpage == 0)
                {
                    string FullName = (DUserModel.FIRSTNAME != null) ? DUserModel.FIRSTNAME + " " + DUserModel.LASTNAME : DUIModels.GuestName;
                    if (string.IsNullOrWhiteSpace(FullName))
                        FullName = DUserModel.USERID;
                    string Company = (DUserModel.COMPANYNAME != null) ? DUserModel.COMPANYNAME : DUIModels.GuestCompany;
                    string Address = (DUserModel.UAFULLADDRESS != null) ? DUserModel.UAFULLADDRESS : "";
                    string Phone = (DUserModel.WORKPHONE != null) ? (DUserModel.ISDCODE + " - " + DUserModel.WORKPHONE) : "";
                    string EMail = (DUserModel.USERID != null) ? DUserModel.USERID : DUIModels.GuestEmail;
                    if (!string.IsNullOrWhiteSpace(DUserModel.COUNTRYNAME))
                    {
                        if (DUserModel.COUNTRYID == Convert.ToDouble(Country.INDIA))
                        {
                            if (!string.IsNullOrEmpty(DUserModel.VATREGNUMBER))
                                TaxNum = "GST number : " + DUserModel.VATREGNUMBER;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(DUserModel.VATREGNUMBER))
                                TaxNum = "Indirect Tax number : " + DUserModel.VATREGNUMBER;
                        }
                    }
                    PdfPTable mUserTab = new PdfPTable(2);
                    mUserTab.WidthPercentage = 100;
                    mUserTab.SpacingAfter = 0;
                    mUserTab.DefaultCell.Padding = 0;
                    mUserTab.DefaultCell.Border = 0;
                    mUserTab.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                    float[] tblUserwidths = new float[] { 50, 50 };
                    mUserTab.SetWidths(tblUserwidths);

                    int mUserInfoAlign = Element.ALIGN_LEFT;
                    PdfPTable mHeaderUserTable = new PdfPTable(1);
                    mHeaderUserTable.WidthPercentage = 100;
                    mHeaderUserTable.TotalWidth = 230;
                    mHeaderUserTable.DefaultCell.Padding = 0;
                    mHeaderUserTable.HorizontalAlignment = 0;
                    mUserTab.AddCell(mHeaderUserTable);

                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell("For,", mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(FullName, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Company, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Address))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Address, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Phone))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Phone, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(EMail, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(TaxNum, mUserInfoAlign));
                    mUserTab.AddCell(mHeaderUserTable);
                    mHeaderUserTable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 35), writer.DirectContent);
                }
                PDFpage++;
                string mQuotationNumber = DUIModels.QuotationNumber;
                string mDateCreated = DUIModels.DateofEnquiry.ToString();
                string mDateofValidity = DUIModels.DateOfValidity.ToString();
                string mIssueDate = "Issue Date" + " : " + ((!String.IsNullOrEmpty(mDateCreated)) ? Convert.ToDateTime(mDateCreated).ToString(format, ci) : string.Empty);
                string mValidTillDate = "Quote valid until" + " : " + ((!String.IsNullOrEmpty(mDateofValidity)) ? Convert.ToDateTime(mDateofValidity).ToString(format, ci) : string.Empty);
                string mShipmentDate = string.Empty;
                string DateOfShipment = DUIModels.DateofShipment.ToString();
                if (String.IsNullOrEmpty(DateOfShipment))
                    mShipmentDate = "DateOfShipment" + " : " + ((!String.IsNullOrEmpty(DateOfShipment)) ? Convert.ToDateTime(DateOfShipment).ToString(format, ci) : string.Empty);

                PdfPTable mHeadermaintable = new PdfPTable(3);
                mHeadermaintable.WidthPercentage = 100;
                mHeadermaintable.SpacingAfter = 0;
                mHeadermaintable.DefaultCell.Padding = 0;
                mHeadermaintable.DefaultCell.Border = 0;
                mHeadermaintable.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                float[] tblwidths = new float[] { 50, 29, 36 };
                mHeadermaintable.SetWidths(tblwidths);

                int mQuoteInfoAlign = Element.ALIGN_LEFT;

                PdfPTable mHeaderLeft = new PdfPTable(1);
                mHeaderLeft.WidthPercentage = 100;
                mHeaderLeft.DefaultCell.Padding = 0;
                mHeaderLeft.HorizontalAlignment = 0;

                //Company Logo
                PdfPTable mHeaderImageTable = new PdfPTable(1);
                mHeaderImageTable.WidthPercentage = 100;
                mHeadermaintable.TotalWidth = 500;
                mHeaderImageTable.DefaultCell.Padding = 0;
                mHeaderImageTable.DefaultCell.Border = 0;
                PdfPCell imgcel = ImageCell("~/Images/AgilityPrint.png", 48f, mQuoteInfoAlign);
                imgcel.Border = 0;
                imgcel.PaddingBottom = 3;
                mHeaderImageTable.AddCell(imgcel);
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell(mHeaderImageTable);
                mHeadermaintable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 10), writer.DirectContent);
            }
        }
        public static PdfPTable ChargesTableModel(IEnumerable<QuotationChargePreviewModel> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Where(x => x.ISCHARGEINCLUDED == true).Sum(sum => sum.TotalPrice).ToString());
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + ChargesTotal, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        if (QC.ISCHARGEINCLUDED == true)
                        {
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.ChargeName, Element.ALIGN_LEFT)));
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(Double.Parse(QC.TotalPrice.ToString()).ToString(), Element.ALIGN_RIGHT)));
                        }
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static PdfPTable TermsandConditionsTableModel(QuotationPreviewModel model)
        {
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mTACtable = new PdfPTable(1);
                mTACtable.WidthPercentage = 100;
                mTACtable.HeaderRows = 0;
                mTACtable.DefaultCell.Border = 0;
                mTACtable.SetWidths(new float[] { 100 });
                mTACtable.KeepTogether = true;

                PdfPCell quotationheading = mFormattingpdf.DocumentHeading("Terms and Conditions");
                quotationheading.PaddingBottom = -7;
                quotationheading.Border = 0;
                mTACtable.AddCell(quotationheading);

                Phrase mQuoteCondLinephrase = new Phrase();
                mQuoteCondLinephrase.Add(mFormattingpdf.LineSeparator());
                mTACtable.AddCell(mQuoteCondLinephrase);
                mTACtable.AddCell(new PdfPCell() { FixedHeight = 5, Border = 0 });
                int i = 0;
                foreach (var QT in model.TermAndConditions)
                {
                    i = i + 1;
                    PdfPCell mQuoteDesciption = new PdfPCell(mFormattingpdf.TableContentCell(i + "." + QT.DESCRIPTION)) { Padding = 2 };
                    mTACtable.AddCell(mQuoteDesciption);
                }
                return mTACtable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            Dictionary<string, long> dic = new Dictionary<string, long>();
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    temp_row.Add(col.ColumnName, dr[col]);
                }
                trows.Add(temp_row);
            }
            return trows;
        }
        #endregion

        #region PDF Formatting

        /// <summary>
        /// Summary description for Formatting
        /// </summary>
        public class Formattingpdf
        {
            public Font Heading4 { get; private set; }
            public Font Heading5 { get; private set; }
            public Font Heading6 { get; private set; }
            public Font Heading7 { get; private set; }
            public Font TableCaption7 { get; private set; }
            public Font Heading8 { get; private set; }
            public Font Heading9 { get; private set; }
            public Font Normal { get; private set; }
            public Font NormalRedColor { get; private set; }
            public Font Normal6 { get; private set; }
            public Font NormalItalic6 { get; private set; }
            public Font NormalItalic7 { get; private set; }
            public Font NormalBold { get; private set; }
            public Font BoldForQuoteTotal { get; private set; }

            public BaseColor TableHeadingBgColor { get; private set; }
            public BaseColor TableFooterBgColor { get; private set; }
            public BaseColor TableBorderColor { get; private set; }
            public BaseFont chinesebasefont { get; private set; }
            public BaseFont koreanbasefont { get; private set; }

            public FontSelector selectorTableCaption { get; private set; }
            public FontSelector selectorTableHeading { get; private set; }
            public FontSelector selectorTableContent { get; private set; }

            public Font mChineseFontTabCaption { get; private set; }
            public Font mChineseFontTabContent { get; private set; }
            public Font mChineseFontTabContentItalic { get; private set; }
            public Font mKoreanFontTabCaption { get; private set; }
            public Font mKoreanFontTabContent { get; private set; }
            public Font mKoreanFontTabContentItalic { get; private set; }

            public BaseColor TableCaptionFontColour { get; private set; }
            public BaseColor FontColour { get; private set; }

            public string mQuotePdfOutputLangId { get; private set; }

            public Formattingpdf(string QuoteOutPutLanguage = "")
            {
                TableCaptionFontColour = new BaseColor(175, 40, 46);

                TableCaption7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, TableCaptionFontColour);
                Heading4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, Font.NORMAL, BaseColor.BLACK);
                Heading5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, Font.NORMAL, BaseColor.BLACK);
                Heading7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                Heading6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, Font.NORMAL, BaseColor.BLACK);
                Heading8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, Font.NORMAL, BaseColor.BLACK);
                Heading9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.NORMAL, BaseColor.BLACK);
                Normal = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
                NormalRedColor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.RED);
                Normal6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
                NormalItalic6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
                NormalItalic7 = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.ITALIC, BaseColor.BLACK);
                NormalBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                FontColour = new BaseColor(176, 41, 46);
                BoldForQuoteTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, Font.NORMAL, FontColour);
                TableHeadingBgColor = new BaseColor(215, 215, 215);
                TableFooterBgColor = new BaseColor(253, 233, 217);
                TableBorderColor = BaseColor.GRAY;
                mQuotePdfOutputLangId = QuoteOutPutLanguage;

                //For Chinese characters display
                mChineseFontTabCaption = new Font(chinesebasefont, 7, Font.BOLD, TableCaptionFontColour);
                mChineseFontTabContent = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mChineseFontTabContentItalic = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);

                //For Korean characters display
                mKoreanFontTabCaption = new Font(koreanbasefont, 7, Font.BOLD, TableCaptionFontColour);
                mKoreanFontTabContent = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mKoreanFontTabContentItalic = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);

                selectorTableCaption = new FontSelector();
                selectorTableCaption.AddFont(TableCaption7);
                selectorTableCaption.AddFont(mChineseFontTabCaption);
                selectorTableCaption.AddFont(mKoreanFontTabCaption);

                selectorTableContent = new FontSelector();
                selectorTableContent.AddFont(NormalItalic7);
                selectorTableContent.AddFont(mChineseFontTabContentItalic);
                selectorTableContent.AddFont(mKoreanFontTabContentItalic);

                selectorTableHeading = new FontSelector();
            }

            /// <summary>
            /// Document Heading
            /// </summary>
            /// <param name="headName">Tha Document Name</param>
            /// <param name="hAlign">The h Align</param>
            /// <param name="underline"></param>
            /// <returns></returns>
            ///
            public PdfPCell DocumentHeadingIncrease(string headName, int hAlign = Element.ALIGN_RIGHT, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading = new FontSelector();
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 13, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 13, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_LEFT, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            public PdfPCell DocumentHeading(string headName, int hAlign = Element.ALIGN_CENTER, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading8);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading.AddFont(Heading8);
                Font docChiHeading = new Font(chinesebasefont, 10, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            /// <summary>
            /// Subs the heading cell.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <returns></returns>
            public PdfPCell TableCaption(string caption, int vAlign = Element.ALIGN_MIDDLE, int hAlign = Element.ALIGN_LEFT, bool isbold = true, bool IsColor = false, bool isBorder = false)
            {
                if (isbold)
                {
                    FontSelector selectorTableCaptionbold = new FontSelector();
                    Font TableChiHead7;
                    Font TableKorHead7;
                    if (IsColor == false)
                    {
                        selectorTableCaptionbold.AddFont(Heading7);
                        TableChiHead7 = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                        TableKorHead7 = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                        selectorTableCaptionbold.AddFont(TableChiHead7);
                        selectorTableCaptionbold.AddFont(TableKorHead7);
                    }
                    else
                    {
                        selectorTableCaptionbold.AddFont(TableCaption7);
                        Font TableChiCap7 = new Font(chinesebasefont, 9, Font.BOLD, TableCaptionFontColour);
                        Font TableKorCap7 = new Font(koreanbasefont, 9, Font.BOLD, TableCaptionFontColour);
                        selectorTableCaptionbold.AddFont(TableChiCap7);
                        selectorTableCaptionbold.AddFont(TableKorCap7);
                    }
                    return new PdfPCell(selectorTableCaptionbold.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
                else
                {
                    FontSelector selectorTabCaption = new FontSelector();
                    selectorTabCaption.AddFont(Normal6);
                    Font ChiTableNormal6 = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    Font KorTableNormal6 = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    selectorTabCaption.AddFont(ChiTableNormal6);
                    selectorTabCaption.AddFont(KorTableNormal6);
                    return new PdfPCell(selectorTabCaption.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true, bool isLowFont = false)
            {
                if (isLowFont)
                    return TableHeading(caption, Heading5, hAlign, underline);
                else
                    return TableHeading(caption, Heading6, hAlign, underline);
            }

            /// <summary>
            /// Tables the footer.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <param name="underline">if set to <c>true</c> [underline].</param>
            /// <returns></returns>
            public PdfPCell TableFooter(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
                mPdfCell.BackgroundColor = TableFooterBgColor;
                return mPdfCell;
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="headingFont">The heading font.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, Font headingFont, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                int padding = 3;
                var mChunk = new Chunk(caption, headingFont);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);

                FontSelector selectorTabHeading = new FontSelector();
                selectorTabHeading.AddFont(headingFont);
                Font ChiTabHeading = new Font(chinesebasefont, 8, Font.BOLD, BaseColor.BLACK);
                Font KorTabHeading = new Font(koreanbasefont, 8, Font.BOLD, BaseColor.BLACK);
                selectorTabHeading.AddFont(ChiTabHeading);
                selectorTabHeading.AddFont(KorTabHeading);
                return new PdfPCell(selectorTabHeading.Process(mChunk.ToString())) { BorderColor = TableBorderColor, BackgroundColor = TableHeadingBgColor, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = 2, PaddingRight = 2 };//padding - 2
            }

            /// <summary>
            /// Tables Content
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCell(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabContentCell = new FontSelector();
                selectorTabContentCell.AddFont(Normal);
                Font ChiTabContentcell = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabContentcell = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabContentCell.AddFont(ChiTabContentcell);
                selectorTabContentCell.AddFont(KorTabContentcell);
                return new PdfPCell(selectorTabContentCell.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            public PdfPCell TableContentCellNormalandItalic(string cellData, string cellItalicData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                Paragraph mPgh = new Paragraph();
                FontSelector selectorNormal = new FontSelector();
                selectorNormal.AddFont(Normal);
                Font fntChi1 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor1 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormal.AddFont(fntChi1);
                selectorNormal.AddFont(fntKor1);

                Phrase ph1 = selectorNormal.Process(cellData);
                FontSelector selectorNormItalic = new FontSelector();
                selectorNormItalic.AddFont(Normal);
                Font fntChi2 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor2 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormItalic.AddFont(fntChi2);
                selectorNormItalic.AddFont(fntKor2);
                Phrase ph2 = selectorNormItalic.Process(cellItalicData);
                mPgh.Add(ph1);
                if (!string.IsNullOrEmpty(cellItalicData))
                    mPgh.Add(ph2);
                return new PdfPCell(mPgh) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content Italic
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellItalic(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellItalic = new FontSelector();
                selectorTabConentcellItalic.AddFont(NormalItalic7);
                Font ChiContentcell = new Font(chinesebasefont, 9, Font.ITALIC, BaseColor.BLACK);
                Font KorContentcell = new Font(koreanbasefont, 9, Font.ITALIC, BaseColor.BLACK);
                selectorTabConentcellItalic.AddFont(ChiContentcell);
                selectorTabConentcellItalic.AddFont(KorContentcell);
                return new PdfPCell(selectorTabConentcellItalic.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content with red color
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellRedColor(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellRedColor = new FontSelector();
                selectorTabConentcellRedColor.AddFont(NormalRedColor);
                Font fntChiRed = new Font(chinesebasefont, 10, Font.NORMAL, BaseColor.RED);
                Font fntKorRed = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.RED);
                selectorTabConentcellRedColor.AddFont(fntChiRed);
                selectorTabConentcellRedColor.AddFont(fntKorRed);
                return new PdfPCell(selectorTabConentcellRedColor.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBold(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBold = new FontSelector();
                selectorTabConentcellBold.AddFont(NormalBold);
                Font fntChiBold = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                Font fntKorBold = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                selectorTabConentcellBold.AddFont(fntChiBold);
                selectorTabConentcellBold.AddFont(fntKorBold);
                return new PdfPCell(selectorTabConentcellBold.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold for Quotation Grand Total
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldForQuoteTotal(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBoldQT = new FontSelector();
                selectorTabConentcellBoldQT.AddFont(BoldForQuoteTotal);
                Font fntChiBorderQT = new Font(chinesebasefont, 10, Font.BOLD, FontColour);
                Font fntKorBorderQT = new Font(koreanbasefont, 10, Font.BOLD, FontColour);
                selectorTabConentcellBoldQT.AddFont(fntChiBorderQT);
                selectorTabConentcellBoldQT.AddFont(fntKorBorderQT);

                return new PdfPCell(selectorTabConentcellBoldQT.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding };
            }

            /// <summary>
            /// Table Content Cell Data Bold,Underline
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="underline"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldUnderline(string cellData, int hAlign = Element.ALIGN_LEFT, bool underline = false)
            {
                int padding = 2;
                var mChunk = new Chunk(cellData, Heading6);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                FontSelector selectorTabConentcellBoldline = new FontSelector();
                selectorTabConentcellBoldline.AddFont(Heading6);
                Font fntChiBoldline = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                Font fntKorBoldline = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                selectorTabConentcellBoldline.AddFont(fntChiBoldline);
                selectorTabConentcellBoldline.AddFont(fntKorBoldline);

                return new PdfPCell(selectorTabConentcellBoldline.Process(mChunk.ToString())) { Border = 0, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            ///// <summary>
            ///// Lines the separator.
            ///// </summary>
            ///// <returns></returns>
            public Chunk LineSeparator()
            {
                return new Chunk(new LineSeparator(4f, 100f, TableHeadingBgColor, Element.ALIGN_CENTER, -1));
            }

            public Chunk ChunkText(string caption, bool isColor = false)
            {
                if (isColor)
                    return new Chunk(caption, TableCaption7);
                else
                    return new Chunk(caption, Heading7);
            }
        }

        #endregion PDF Formatting

        #region ReferenceFunctions

        private void Generatequotepdf(long QUOTATIONID, long JOBNUMBER, string CreatedBy)
        {
            QuotationDBFactory mQuoFactory = new QuotationDBFactory();
            DBFactory.Entities.QuotationEntity mDBEntity = mQuoFactory.GetById(QUOTATIONID);
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            PreferredCurrencyId = DUIModels.PreferredCurrencyId;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bytes = APIDynamicClass.GeneratePDF(memoryStream, DUIModels);
                MemoryStream stream = new MemoryStream(bytes);
                //Insert same data into db 
                SSPDocumentsModel sspdoc = new SSPDocumentsModel();
                sspdoc.QUOTATIONID = Convert.ToInt64(QUOTATIONID);
                sspdoc.DOCUMNETTYPE = "Quotation";
                sspdoc.FILENAME = DUIModels.QuotationNumber + ".pdf";
                sspdoc.FILEEXTENSION = "application/pdf";
                sspdoc.FILESIZE = bytes.Length;
                sspdoc.FILECONTENT = bytes;
                sspdoc.JOBNUMBER = JOBNUMBER;
                sspdoc.DocumentName = "Quotation";
                DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
                JobBookingDbFactory mjobFactory = new JobBookingDbFactory();
                Guid obj = Guid.NewGuid();
                string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();

                long mQuotationId = mjobFactory.SaveDocuments(docentity, Convert.ToString(CreatedBy), "System Generated", obj.ToString(), EnvironmentName);

            }
        }


        public void SendmailtoNPC(QuotationEntity mDBEntity)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            string msubject = "Overweight/CBM threshold quote request - " + DUIModels.QuotationNumber;
            string NPCEmail = System.Configuration.ConfigurationManager.AppSettings["NPCEmail"];
            string mbody = "Dear NPC User,";
            mbody = mbody + "<br/> <br/>" + "A quote is generated to a Shipa Freight user, which is above the Max criteria for Weight/CBM configured for " + mDBEntity.OCOUNTRYNAME;
            mbody = mbody + "<br/> <br/>" + "Regards";
            mbody = mbody + "<br/>" + "Shipa Freight Team";

            if (mDBEntity.PRODUCTNAME == "Ocean")
            {
                try
                {
                    DataTable dt = mFactory.GetNPCdetails(Convert.ToInt64(mDBEntity.OCOUNTRYID));
                    if (dt.Rows.Count > 0 && (mDBEntity.TOTALGROSSWEIGHT > Convert.ToDecimal(dt.Rows[0]["OCEANMAXWEIGHT"].ToString()) || mDBEntity.TOTALCBM > Convert.ToDecimal(dt.Rows[0]["OCEANCBM"].ToString())))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            bytes = APIDynamicClass.GeneratePDF(memoryStream, DUIModels);
                            memoryStream.Close();
                        }
                        string Emailid = (dt.Rows[0]["NPCMAILID"].ToString() != null && dt.Rows[0]["NPCMAILID"].ToString().Trim() != "") ? dt.Rows[0]["NPCMAILID"].ToString() : NPCEmail;
                        SendMail(Emailid, mbody, msubject, bytes, DUIModels.QuotationNumber);
                    }
                }
                catch
                {
                    //
                }
            }
            else
            {
                try
                {
                    DataTable dt = mFactory.GetNPCdetails(Convert.ToInt64(mDBEntity.OCOUNTRYID));
                    if (dt.Rows.Count > 0 && (mDBEntity.TOTALGROSSWEIGHT > Convert.ToDecimal(dt.Rows[0]["AIRMAXWEIGHT"].ToString()) || mDBEntity.TOTALCBM > Convert.ToDecimal(dt.Rows[0]["AIRCBM"].ToString())))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            bytes = APIDynamicClass.GeneratePDF(memoryStream, DUIModels);
                            memoryStream.Close();
                        }
                        string Emailid = (dt.Rows[0]["NPCMAILID"].ToString() != null && dt.Rows[0]["NPCMAILID"].ToString().Trim() != "") ? dt.Rows[0]["NPCMAILID"].ToString() : NPCEmail;
                        SendMail(Emailid, mbody, msubject, bytes, DUIModels.QuotationNumber);
                    }
                }
                catch (Exception ex)
                {
                    LogError("api/JoBookMB", "SendmailtoNPC", ex.Message.ToString(), ex.StackTrace.ToString());
                }
            }
        }
        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }
            return dist;
        }

        public double Co2EmissionCalculation(QuotationEntity model)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            double objText = 0;
            double OCo2 = 0; double PCo2 = 0; double DCo2 = 0;
            double oplacelat = 0; double oplacelon = 0;
            double oportlat = 0; double oportlon = 0;
            double dplacelat = 0; double dplacelon = 0;
            double dportlat = 0; double dportlon = 0;

            DataSet ds = new DataSet();
            if (model.MOVEMENTTYPENAME == "Port to port")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPORTID, model.DESTINATIONPORTID);
                    DataRow[] orows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (orows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] drows = ds.Tables[0].Select("UNLOCATIONID = '" + model.DESTINATIONPORTID + "'");
                    if (drows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Door to door")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.ORIGINPLACEID != 0) && (model.ORIGINPLACEID != null)) && ((model.DESTINATIONPLACEID != 0) && (model.DESTINATIONPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPLACEID, model.ORIGINPORTID, model.DESTINATIONPLACEID, model.DESTINATIONPORTID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID);
                    if (oprows.Length > 0)
                    {
                        oplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[1]);
                        oplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] orows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (orows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] drows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID);
                    if (drows.Length > 0)
                    {
                        dplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[1]);
                        dplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (dprows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Door to port")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.ORIGINPLACEID != 0) && (model.ORIGINPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPLACEID, model.ORIGINPORTID, model.DESTINATIONPORTID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = '" + model.ORIGINPLACEID + "'");
                    if (oprows.Length > 0)
                    {
                        oplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[1]);
                        oplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (dprows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] prows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (prows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Port to door")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.DESTINATIONPLACEID != 0) && (model.DESTINATIONPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPORTID, model.DESTINATIONPORTID, model.DESTINATIONPLACEID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (oprows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] prows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (prows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID);
                    if (dprows.Length > 0)
                    {
                        dplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[1]);
                        dplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[2]);
                    }
                }
            }

            if (oplacelat != 0 && oplacelon != 0 && oportlat != 0 && oportlon != 0)
            {
                double Origindistance = DistanceTo(oplacelat, oplacelon, oportlat, oportlon, 'K');
                double OADP = 0.2;
                double Adjusteddistance = Origindistance + (Origindistance * OADP);
                double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                double OCWMT = 0.001;
                double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                double TonneKM = Adjusteddistance * OcargoweightMT;
                double KGCo2 = 0.13096;
                double KGCo2emitted = KGCo2 * TonneKM;
                OCo2 = KGCo2emitted;//* 0.001                
            }
            if (oportlat != 0 && oportlon != 0 && dportlat != 0 && dportlon != 0)
            {
                double Origindistance = DistanceTo(oportlat, oportlon, dportlat, dportlon, 'K');
                double OADP = 0;
                if (model.PRODUCTNAME.ToUpperInvariant() == "AIR")
                {
                    OADP = 0.08;
                }
                else
                {
                    OADP = 0.15;
                }
                double Adjusteddistance = Origindistance + (Origindistance * OADP);
                double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                double OCWMT = 0.001;
                double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                double TonneKM = Adjusteddistance * OcargoweightMT;
                double KGCo2 = 0;
                if (model.PRODUCTNAME.ToUpperInvariant() == "OCEAN")
                {
                    KGCo2 = 0.019065;
                }
                else { KGCo2 = 0.7129; }

                double KGCo2emitted = KGCo2 * TonneKM;
                PCo2 = KGCo2emitted;//* 0.001               
            }
            if (dportlat != 0 && dportlon != 0 && dplacelat != 0 && dplacelon != 0)
            {
                double Origindistance = DistanceTo(dportlat, dportlon, dplacelat, dplacelon, 'K');
                double OADP = 0.2;
                double Adjusteddistance = Origindistance + (Origindistance * OADP);
                double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                double OCWMT = 0.001;
                double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                double TonneKM = Adjusteddistance * OcargoweightMT;
                double KGCo2 = 0.13096;
                double KGCo2emitted = KGCo2 * TonneKM;
            }
            objText = OCo2 + PCo2 + DCo2;
            mFactory.UpdateCo2(model.QUOTATIONID, objText);

            return objText;
        }



        public IHttpActionResult Post([FromBody]QuotationModel value)
        {
            value.QuotationNumber = DateTime.Now.ToShortDateString();
            return Ok(value);
        }

        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void SendMailForGSSCQuotes(string number, int threshold, int hazordous, int notavailable)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            Email = Email + "," + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];
            subject = "Request for Quote submitted in GSSC Flow : " + number;
            string body = "Dear User,";
            body = body + "<br/> <br/>" + "A Request for Quote is  set to GSSC and available because of";

            if (notavailable > 0)
                body = body + "<br/> " + "\u2022 " + " Rates are not available";
            if (threshold == 1)
                body = body + "<br/> " + "\u2022 " + " Threshold value met";
            if (hazordous == 1)
                body = body + "<br/> " + "\u2022 " + " Hazordous Goods available";

            body = body + "<br/> <br/>" + " Please use the below URL to access the application";
            body = body + "<br/>" + URL;

            body = body + "<br/> <br/>" + "Regards";
            body = body + "<br/>" + "Shipa Freight Team";

            SendMail(Email, body, subject);
        }

        public void SendMail(string Email, string body, string subject, byte[] bytes = null, string QuotationNumber = "")
        {
            MailMessage message = new MailMessage();

            if (Email.Contains(','))
            {
                foreach (string s in Email.Split(','))
                {
                    message.Bcc.Add(new MailAddress(s));
                }
            }
            else
            {
                message.CC.Add(new MailAddress(Email));
            }
            // message.To.Add(new MailAddress("shipafreight@agility.com"));
            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            if (bytes != null)
                message.Attachments.Add(new Attachment(new MemoryStream(bytes), QuotationNumber + ".pdf"));
            message.IsBodyHtml = true;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }

        public void Assignvalues(QuotationPreviewModel mUIModelPreview)
        {
            if (!ReferenceEquals(mUIModelPreview, null))
            {
                foreach (var mShipment in mUIModelPreview.ShipmentItems)
                {
                    cargodetails = cargodetails +
                                      "<tr>" +
                                      "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                      "<span class='inner-detail-head'" + ">Package Type</span><br>" +
                                      "<span class='inner-detail-cont'" + ">" + mShipment.PackageTypeName + "</span></td>" +
                                      "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                      "<span class='inner-detail-head'" + ">Quantity in Pieces</span><br>" +
                                      "<span class='inner-detail-cont'" + "> " + mShipment.Quantity.ToString() + "</span></td> </tr>";
                }

                if (mUIModelPreview.MovementTypeName == "Door to door" || mUIModelPreview.MovementTypeName == "Door to Door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1} {2}", mUIModelPreview.OriginPlaceCode, mUIModelPreview.OriginPlaceName, mUIModelPreview.OriginSubDivisionCode != null ? "," + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0}, {1} {2}", mUIModelPreview.DestinationPlaceCode, mUIModelPreview.DestinationPlaceName, mUIModelPreview.DestinationSubDivisionCode != null ? "," + mUIModelPreview.DestinationSubDivisionCode : "");
                    mPRLableName = "Origin City"; mPDLableName = "Destination City";
                }
                else if (mUIModelPreview.MovementTypeName == "Door to port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1} {2}", mUIModelPreview.OriginPlaceCode, mUIModelPreview.OriginPlaceName, mUIModelPreview.OriginSubDivisionCode != null ? "," + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0}, {1} {2}", mUIModelPreview.DestinationPortCode, mUIModelPreview.DestinationPortName, mUIModelPreview.DestinationSubDivisionCode != null ? "," + mUIModelPreview.DestinationSubDivisionCode : "");
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination AirPort";
                    }
                    else
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Port";
                    }
                }
                else if (mUIModelPreview.MovementTypeName == "Port to port" || mUIModelPreview.MovementTypeName == "Port to Port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1} {2}", mUIModelPreview.OriginPortCode, mUIModelPreview.OriginPortName, mUIModelPreview.OriginSubDivisionCode != null ? "," + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0}, {1} {2}", mUIModelPreview.DestinationPortCode, mUIModelPreview.DestinationPortName, mUIModelPreview.DestinationSubDivisionCode != null ? "," + mUIModelPreview.DestinationSubDivisionCode : "");
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin AirPort"; mPDLableName = "Destination AirPort";
                    }
                    else
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination Port";
                    }
                }
                else if (mUIModelPreview.MovementTypeName == "Port to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1} {2}", mUIModelPreview.OriginPortCode, mUIModelPreview.OriginPortName, mUIModelPreview.OriginSubDivisionCode != null ? "," + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0}, {1} {2}", mUIModelPreview.DestinationPlaceCode, mUIModelPreview.DestinationPlaceName, mUIModelPreview.DestinationSubDivisionCode != null ? "," + mUIModelPreview.DestinationSubDivisionCode : "");
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin AirPort"; mPDLableName = "Destination City";
                    }
                    else
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination City";
                    }
                }
            }
        }

        public HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }

        public HttpResponseMessage ErrorResponse()
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = "Something Went Wrong"
                })
            };
        }

        public HttpResponseMessage ErrorResponse(dynamic error)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.ExpectationFailed,
                    Error = error
                })
            };
        }

        #endregion

        #region Models

        public class UserModel
        {
            [Required(ErrorMessage = "UserId value is required.")]
            [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Not a valid email")]
            public string UserId { get; set; }
        }

        public class CollabarateModel : UserModel
        {
            [Required(ErrorMessage = "DOCID value is required.")]
            public string DOCID { get; set; }

            [Required(ErrorMessage = "comma separated DocNames required")]
            public string DocNames { get; set; }

            public string DocupDate { get; set; }

            public string ProvidedBy { get; set; }

            [Required(ErrorMessage = "ProviderEmailId value is required.")]
            [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Not a valid email")]
            public string ProviderEmailId { get; set; }

            [Required(ErrorMessage = "Jobnumber value is required.")]
            public string Jobnumber { get; set; }

            [Required(ErrorMessage = "QuoatationNo value is required.")]
            public string QuoatationNo { get; set; }

            public string CargoAvailable { get; set; }

            public string ShipperName { get; set; }

            public string ConsigneeName { get; set; }

            public string NoteToShipper { get; set; }
        }

        public class CollabarateDocModel : UserModel
        {
            [Required(ErrorMessage = "Jobnumber value is required.")]
            public int Jobnumber { get; set; }

            [Required(ErrorMessage = "QuoatationNo value is required.")]
            public int QuoatationNo { get; set; }

        }


        public class CollabrateCargoModel : UserModel
        {
            [Required(ErrorMessage = "OperationalJobnumber value is required.")]
            public string OperationalJobnumber { get; set; } // This is operational Jobnumer

            [Required(ErrorMessage = "QuoatationNo value is required.")]
            public int QuoatationNo { get; set; }

            [Required(ErrorMessage = "ShipperEmailId value is required.")]
            [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Not a valid email")]
            public string ShipperEmailId { get; set; }

            public string ShipperName { get; set; }

            public string NoteToShipper { get; set; }

        }

        public class CollabrateUploadDocModel : UserModel
        {
            [Required(ErrorMessage = "stringInBase64 value is required.")]
            public string stringInBase64 { get; set; }

            public string ContentType { get; set; }

            public long ContentLength { get; set; }

            [Required(ErrorMessage = "FileName value is required.")]
            public string FileName { get; set; }

            public long DOCID { get; set; }

            [Required(ErrorMessage = "DocNames required")]
            public string DocNames { get; set; }

            [Required(ErrorMessage = "JobNumber value is required.")]
            public long JobNumber { get; set; }

            [Required(ErrorMessage = "OperationalJobNumber value is required.")]
            public string OperationalJobNumber { get; set; }

            [Required(ErrorMessage = "ModifiedBy value is required.")]
            [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Not a valid email")]
            public string ModifiedBy { get; set; }

            [Required(ErrorMessage = "QuoatationNo value is required.")]
            public string QuoatationNo { get; set; }

            [Required(ErrorMessage = "QuoatationNo value is required.")]
            public int QuoteId { get; set; }

            public string COMAND { get; set; }
            public string DOCUMENTTEMPLATE { get; set; }
            public string DOCUMENTTYPE { get; set; }
            public string DIRECTION { get; set; }

            public bool IsAdditionalDoc { get; set; }
        }

        #endregion

        //[Route("api/JoBookMB/BookNowIndex")]
        //[HttpPost]
        //public IHttpActionResult BookNowIndex([FromBody]dynamic request) //int id, int? JobNumber, 
        //{
        //    JobBookingDbFactory mFactory = new JobBookingDbFactory();
        //    JobBookingModel mUIModel = new JobBookingModel();
        //    UserDBFactory UserFactory = new UserDBFactory();
        //    decimal CountryId = 0;
        //    long jobNum = 0;
        //    if (Convert.ToInt64(request.JobNumber.Value) == 0)
        //    {

        //        jobNum = 0;
        //        DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetById(Convert.ToInt32(request.id.Value), request.UserName.Value);
        //        mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);

        //        List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(request.UserName.Value, "", 0);
        //        List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);


        //        UserPartyMasterModel user = PartyModel.Where(x => x.UPERSONAL == 1).FirstOrDefault();
        //        PartyModel.Clear();
        //        PartyModel.Add(user);
        //        mUIModel.PartiesModel = PartyModel;
        //        CountryId = user.COUNTRYID;
        //        mDBEntity.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
        //        mUIModel.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
        //        mUIModel.SHCLIENTID = Convert.ToString(user.SSPCLIENTID);
        //    }
        //    else
        //    {
        //        jobNum = Convert.ToInt64(request.JobNumber.Value);
        //        DBFactory.Entities.JobBookingEntity mDBEntity = mFactory.GetQuotedJobDetails(Convert.ToInt32(request.id.Value), Convert.ToInt64(request.JobNumber.Value));
        //        // mDBEntity.DocumentDetails.
        //        IList<SSPJobDetailsEntity> jobdet = new List<SSPJobDetailsEntity>();
        //        IList<SSPPartyDetailsEntity> partydet = new List<SSPPartyDetailsEntity>();
        //        jobdet = mDBEntity.JobItems;
        //        partydet = mDBEntity.PartyItems;
        //        if (jobdet != null && jobdet.Count > 0)
        //        {
        //            mDBEntity.JJOBNUMBER = jobdet[0].JOBNUMBER;
        //            mDBEntity.JOPERATIONALJOBNUMBER = jobdet[0].OPERATIONALJOBNUMBER;
        //            mDBEntity.JQUOTATIONID = jobdet[0].QUOTATIONID;
        //            mDBEntity.JQUOTATIONNUMBER = jobdet[0].QUOTATIONNUMBER;
        //            mDBEntity.JINCOTERMCODE = jobdet[0].INCOTERMCODE;
        //            mDBEntity.JINCOTERMDESCRIPTION = jobdet[0].INCOTERMDESCRIPTION;
        //            mDBEntity.JCARGOAVAILABLEFROM = jobdet[0].CARGOAVAILABLEFROM;
        //            mDBEntity.JCARGODELIVERYBY = jobdet[0].CARGODELIVERYBY;
        //            mDBEntity.JDOCUMENTSREADY = jobdet[0].DOCUMENTSREADY;
        //            mDBEntity.JSLINUMBER = jobdet[0].SLINUMBER;
        //            mDBEntity.JCONSIGNMENTID = jobdet[0].CONSIGNMENTID;
        //            mDBEntity.JSTATEID = jobdet[0].STATEID;
        //            mDBEntity.SPECIALINSTRUCTION = jobdet[0].SPECIALINSTRUCTIONS == null ? "" : jobdet[0].SPECIALINSTRUCTIONS;
        //            if (partydet.Count() > 0)
        //            {
        //                foreach (var items in partydet)
        //                {
        //                    if (items.PARTYTYPE == "91")
        //                    {
        //                        mDBEntity.SHCLIENTID = Convert.ToString(items.SSPCLIENTID);
        //                        mDBEntity.SHPARTYDETAILSID = items.PARTYDETAILSID;
        //                        CountryId = items.COUNTRYID;
        //                        mDBEntity.SHCOUNTRYID = Convert.ToString(items.COUNTRYID);
        //                    }
        //                    else
        //                    {
        //                        mDBEntity.CONCLIENTID = Convert.ToString(items.SSPCLIENTID);
        //                        mDBEntity.CONPARTYDETAILSID = items.PARTYDETAILSID;
        //                        mDBEntity.CONCOUNTRYID = Convert.ToString(items.COUNTRYID);
        //                    }
        //                }
        //            }
        //        }

        //        mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
        //        List<DBFactory.Entities.UserPartyMaster> PartyEntity = UserFactory.GetUserPartyMaster(request.UserName.Value, "", 0);
        //        List<UserPartyMasterModel> PartyModel = Mapper.Map<List<UserPartyMaster>, List<UserPartyMasterModel>>(PartyEntity);
        //        mUIModel.PartiesModel = PartyModel;
        //        if (partydet.Count == 0)
        //        {
        //            UserPartyMasterModel user = PartyModel.Where(x => x.UPERSONAL == 1).FirstOrDefault();
        //            mDBEntity.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
        //            mUIModel.SHCOUNTRYID = Convert.ToString(user.COUNTRYID);
        //            mUIModel.SHCLIENTID = Convert.ToString(user.SSPCLIENTID);
        //        }
        //        else
        //        {
        //            //if shipper or consignee deleted from parties.
        //            int index = PartyModel.FindIndex(item => item.SSPCLIENTID == Convert.ToInt64(mDBEntity.SHCLIENTID));
        //            if (index < 0)
        //            {
        //                foreach (var item in PartyModel.Where(x => x.UPERSONAL == 1))
        //                {
        //                    mUIModel.UPERSONALID = Convert.ToString(item.SSPCLIENTID);
        //                    mUIModel.UPERSONALCOUNTRYID = Convert.ToString(item.COUNTRYID);
        //                }
        //            }
        //            int index1 = PartyModel.FindIndex(item => item.SSPCLIENTID == Convert.ToInt64(mDBEntity.CONCLIENTID));
        //            if (index1 < 0)
        //            {
        //                foreach (var item in PartyModel.Where(x => x.UPERSONAL == 1))
        //                {
        //                    mUIModel.CONUPERSONALID = Convert.ToString(item.SSPCLIENTID);
        //                    mUIModel.CONUPERSONALCOUNTRYID = Convert.ToString(item.COUNTRYID);
        //                }
        //            }
        //        }
        //    }

        //    DBFactory.Entities.CargoAvabilityEntity CargoDate = mFactory.GetPickUpTransitDates(Convert.ToInt64(CountryId));
        //    mUIModel.CargoAvabilityModel = Mapper.Map<CargoAvabilityEntity, CargoAvabilityModel>(CargoDate);
        //    MDMDataFactory mdmFactory = new MDMDataFactory();
        //    List<IncoTermsEntity> incotermsEntity = mdmFactory.GetIncoTerms(string.Empty, string.Empty);
        //    List<IncotermsDetailsModel> incoterms = Mapper.Map<List<IncoTermsEntity>, List<IncotermsDetailsModel>>(incotermsEntity);

        //    mUIModel.IncotermDetails = incoterms;
        //    byte[] emptyDoc = new byte[0];
        //    for (int i = 0; i < mUIModel.DocumentDetails.Count; i++)
        //    {
        //        mUIModel.DocumentDetails[i].FILECONTENT = emptyDoc;
        //    }

        //    return Ok(mUIModel);
        //}

    }
}