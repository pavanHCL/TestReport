﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using System.Data;
using Newtonsoft.Json;
using FOCiS.SSP.DBFactory.Entities;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Http.Routing;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.DB;
using System.Text.RegularExpressions;
using AutoMapper;
using FOCiS.SSP.Models.UserManagement;
using FOCiS.SSP.WebApi.Helpers;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Web.UI.Controllers;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Reflection;

namespace FOCiS.SSP.WebApi.Controllers
{
    [Route("api/Encryption/{action}")]
    public class EncryptionController : BaseController
    {
        public IHttpActionResult Get()
        {
            return BadRequest();
        }

        [HttpPost]
        public dynamic GetDecodedValues([FromBody]EncryptedParams request)
        {

            try
            {
                return CheckSecrectKeyValidation(request);
            }
            catch (Exception ex)
            {
                return BaseResponse("Something went worng", HttpStatusCode.ExpectationFailed);
            }
        }

        private dynamic CheckSecrectKeyValidation(EncryptedParams request)
        {
            if (request.Parameters == null)
                return BaseResponse("Parameter 'parameters' must have at least one element", HttpStatusCode.ExpectationFailed);

            if (System.Configuration.ConfigurationManager.AppSettings["Secretkey"] == null)
                return BaseResponse("Missed secret key in config file.", HttpStatusCode.BadRequest);

            if (!Request.Headers.Contains("X-API-KEY"))
                return BaseResponse("Required header 'X-API-KEY' is missing.", HttpStatusCode.BadRequest);

            string Secretkey = System.Configuration.ConfigurationManager.AppSettings["Secretkey"];
            string API_KEY = Request.Headers.GetValues("X-API-KEY").First();

            if (API_KEY != Secretkey)
                return BaseResponse("Incorrect key passed in 'X-API-KEY' header", HttpStatusCode.BadRequest);

            return GetEncodedDic(request);

        }

        private dynamic GetEncodedDic(EncryptedParams request)
        {

            if (request.Parameters.Substring(0, 1) != "q")
            {
                return UserMangementDecryption(request.Parameters);
            }
            else
            {
                return URLDecryption(request.Parameters);
            }

        }

        private dynamic UserMangementDecryption(string request)
        {
            string[] parmasList = request.Split('&');
            var DecodedParams = new Dictionary<string, string>();

            for (int i = 0; i < parmasList.Length; i++)
            {
                string DecodedValue = string.Empty;
                try
                {
                    int RightIndex = parmasList[i].IndexOf('=');

                    if (parmasList[i].Split('=')[0] == "rt" || parmasList[i].Split('=')[0] == "CargoAvailable")
                    {
                        DecodedParams.Add(parmasList[i].Substring(0, RightIndex), parmasList[i].Substring(RightIndex + 1));
                    }
                    else
                    {

                        string ParamerterName = parmasList[i].Substring(0, RightIndex);
                        DecodedValue = APIDynamicClass.DecryptStringAES(parmasList[i].Substring((RightIndex + 1)));
                        DecodedParams.Add(ParamerterName, DecodedValue);
                    }

                }
                catch (Exception ex)
                {
                    return BaseResponse("One or more parameters are invalid and cannot be decrypted", HttpStatusCode.BadRequest);
                }
            }
            return new { Parameters = DecodedParams };
        }

        private dynamic URLDecryption(string request)
        {
            string[] parmasList = request.Split('&');
            var DecodedParams = new Dictionary<string, string>();

            for (int i = 0; i < parmasList.Length; i++)
            {
                string DecodedValue = string.Empty;
                try
                {
                    DecodedValue = Encryption.Decrypt(parmasList[i].Substring(2));
                    DecodedParams.Add(DecodedValue.Split('=')[0], DecodedValue.Split('=')[1]);
                }
                catch (Exception ex)
                {
                    return BaseResponse("One or more parameters are invalid and cannot be decrypted", HttpStatusCode.BadRequest);
                }
            }
            return new { Parameters = DecodedParams };
        }

        public HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }

    }

    public class EncryptedParams
    {
        public string Parameters { get; set; }
    }


}