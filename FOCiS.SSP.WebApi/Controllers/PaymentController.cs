﻿using AutoMapper;
using FOCiS.SSP.DBFactory;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using FOCiS.SSP.Models.JP;
using FOCiS.SSP.Models.Payment;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.Web.UI.Controllers;
using FOCiS.SSP.WebApi.Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace FOCiS.SSP.WebApi.Controllers
{
    [Route("api/Payment/{action}")]
    public class PaymentController : BaseController
    {

        private readonly IJobBookingDbFactory _JFactory;
        private readonly UserDBFactory _UFactory;
        private readonly QuotationDBFactory _QFactory;
        private readonly OneSignal _OneSignal;
        private static string ChargeSetTotal = string.Empty;
        private static int z = 0;
        private static byte[] bytes;
        private static Decimal mQuoteChargesTotal = 0;

        public PaymentController()
        {
            _JFactory = new JobBookingDbFactory();
            _UFactory = new UserDBFactory();
            _OneSignal = new OneSignal();
            _QFactory = new QuotationDBFactory();
        }

        public PaymentController(
            IJobBookingDbFactory jFactory,
            UserDBFactory uFactory,
            QuotationDBFactory qFactory,
            OneSignal oneSignal
            )
        {
            _JFactory = jFactory;
            _UFactory = uFactory;
            _QFactory = qFactory;
            _OneSignal = oneSignal;
        }

        [HttpPost]
        public HttpResponseMessage StripePayment([FromBody]StripeModel request)
        {
            if (CheckPaymentStatus(request.JobNumber).ToLowerInvariant() == "notexist")
            {
                return OnlineMainPayment(request);
            }
            return OnlineAdditionalPayment(request);
        }

        [HttpPost]
        public HttpResponseMessage SaveBusinessCredit([FromBody] PaymentModel Apimodel)
        {
            //PaymentModel model = new PaymentModel();
            PaymentTransactionDetailsModel BusinessCreditmodel = new PaymentTransactionDetailsModel();
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(Apimodel.JobNumber));

            MPaymentResult e = new MPaymentResult();
            if (PaymentStatus.ToLower() == "notexist")
            {

                UserDBFactory UserFactory = new UserDBFactory();
                string str = string.Empty;
                string DiscountCode = Apimodel.DiscountCode;
                try
                {
                    int id = Convert.ToInt32(Apimodel.JobNumber);

                    DBFactory.Entities.PaymentEntity mDBEntity = _JFactory.GetPaymentDetailsByJobNumber(id, Convert.ToString(Apimodel.UserId));
                    CouponCode result = new CouponCode();
                    if (!string.IsNullOrEmpty(Apimodel.DiscountCode))
                    {
                        result = _JFactory.GetAgilityDiscountPrice(Apimodel.DiscountCode, Convert.ToString(Apimodel.JobNumber), Convert.ToString(Apimodel.UserId));

                        if (result.CouponStatus.ToUpper() == "INVALID COUPON")
                        {
                            return new HttpResponseMessage()
                            {
                                Content = new JsonContent(new
                                {
                                    StatusCode = HttpStatusCode.ExpectationFailed,
                                    Message = "Invalid Coupon code."
                                })
                            };
                        }

                    }

                    BusinessCreditmodel.TRANSAMOUNT = Convert.ToInt64(mDBEntity.RECEIPTNETAMOUNT);
                    e.RECEIPTNETAMOUNT = Convert.ToInt64(mDBEntity.RECEIPTNETAMOUNT);
                    Apimodel.TransactionMode = "Credit";
                    BusinessCreditmodel.RECEIPTHDRID = mDBEntity.RECEIPTHDRID;
                    BusinessCreditmodel.JOBNUMBER = mDBEntity.JOBNUMBER;
                    BusinessCreditmodel.QUOTATIONID = mDBEntity.QUOTATIONID;
                    BusinessCreditmodel.PAYMENTOPTION = "Business Credit";
                    BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                    BusinessCreditmodel.PAYMENTREFNUMBER = "Credit";
                    BusinessCreditmodel.PAYMENTREFMESSAGE = "approved";
                    BusinessCreditmodel.CREATEDBY = Convert.ToString(Apimodel.UserId);
                    BusinessCreditmodel.MODIFIEDBY = Convert.ToString(Apimodel.UserId);
                    BusinessCreditmodel.PAYMENTTYPE = "Business credit";
                    BusinessCreditmodel.TRANSMODE = "Credit";
                    BusinessCreditmodel.TRANSDESCRIPTION = "Business Credit";
                    BusinessCreditmodel.TRANSCURRENCY = mDBEntity.CURRENCY;
                    e.CURRENCY = mDBEntity.CURRENCY;

                    DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);
                    _JFactory.SaveBusinessCreditTransactionDetails(mUIModel, Convert.ToString(Apimodel.UserId), DiscountCode, result.DiscountAmount);
                    CheckIsMandatoryDocsUploaded(mUIModel.JOBNUMBER.ToString(), Convert.ToString(Apimodel.UserId), mUIModel.QUOTATIONID, mDBEntity.OPJOBNUMBER);

                    PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                    miUIModel.UserId = Convert.ToString(Apimodel.UserId);
                    byte[] bytes = null;

                    UserFactory.NewNotification(5113, "Payment Completed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), Convert.ToString(Apimodel.UserId));
                    var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = Convert.ToString(Apimodel.UserId.ToString()) };
                    SafeRun<bool>(() => { return _OneSignal.SendPushMessageByTag("Email", Convert.ToString(Apimodel.UserId), "Payment Completed", pushdata); }); //
                    DBFactory.Entities.QuotationEntity quotationentity = new QuotationEntity();
                    UserDBFactory UD = new UserDBFactory();
                    UserEntity UE = UD.GetUserFirstAndLastName(miUIModel.UserId);
                    //------------------------------------------------------
                    QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                    quotationentity = mQuoFactory.GetById(Convert.ToInt32(mDBEntity.QUOTATIONID));
                    QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);


                    UserProfileEntity mUserModel = _UFactory.GetUserProfileDetails(Apimodel.UserId, 1);
                    //-----------------------------------------------------

                    GenerateBookingConfirmationPDF(
                            Convert.ToInt32(mDBEntity.JOBNUMBER),
                            "Booking Confirmation",
                            Convert.ToInt64(mDBEntity.QUOTATIONID),
                            mDBEntity.OPJOBNUMBER,
                            Apimodel.UserId,
                            mUserModel,
                            mUIModels
                         );

                    GenerateAdvanceReceiptPDF(
                         Convert.ToInt32(Apimodel.JobNumber),
                        "Advance Receipt",
                        Convert.ToInt64(mDBEntity.QUOTATIONID),
                        mDBEntity.OPJOBNUMBER,
                        Apimodel.UserId,
                        0,
                        mUserModel,
                        mUIModels
                    );

                    //-------------Mail----------------------------------------------------------------
                    try
                    {
                        SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ReceiptNetAmount),
                            null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId,
                             mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment",
                            mUserModel.FIRSTNAME, "Business credit", "Business credit");


                        SendMailtoGroup(mDBEntity.OPJOBNUMBER, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC", "Paymentstatus", Convert.ToInt32(Apimodel.JobNumber),
                            miUIModel.UserId, "Business Credit", "Payment", Convert.ToString(miUIModel.ReceiptNetAmount), null, null, miUIModel.Currency,
                            "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, mUIModels, miUIModel);

                        SendThankingMailtoCustomer(miUIModel.UserId, miUIModel.OPjobnumber, mUserModel.FIRSTNAME);
                    }
                    catch (Exception ex)
                    {
                        LogError("api/Payment", "SaveBusinessCredit", ex.Message.ToString(), ex.StackTrace.ToString());
                    }
                    //-------------------------------------------------------------------
                }
                catch (Exception ex)
                {
                    LogError("api/Payment", "SaveBusinessCredit", ex.Message.ToString(), ex.StackTrace.ToString());
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.InternalServerError,
                            Message = "Something went wrong. please try later"
                        }),
                        StatusCode = HttpStatusCode.InternalServerError
                    };
                }

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Thank you for your payment, your shipment is confirmed. A confirmation email has been sent to " + Convert.ToString(Apimodel.UserId)
                    })
                };
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();

                int id = Convert.ToInt32(Apimodel.JobNumber);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(id, Apimodel.UserId);
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = Apimodel.UserId;
                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    UserDBFactory UserFactory = new UserDBFactory();
                    string str = string.Empty;
                    string DiscountCode = Apimodel.DiscountCode;
                    decimal DiscountAmount = Apimodel.DiscountAmount;

                    Apimodel.TransactionMode = "Credit";
                    CouponCode result = new CouponCode();

                    if (!string.IsNullOrEmpty(Apimodel.DiscountCode))
                    {
                        result = _JFactory.GetAgilityDiscountPrice(Apimodel.DiscountCode, Convert.ToString(Apimodel.JobNumber), Convert.ToString(Apimodel.UserId));

                        if (result.CouponStatus.ToUpper() == "INVALID COUPON")
                        {
                            return new HttpResponseMessage()
                            {
                                Content = new JsonContent(new
                                {
                                    StatusCode = HttpStatusCode.ExpectationFailed,
                                    Message = "Invalid Coupon code." //return Success Message
                                })
                            };
                        }

                    }
                    BusinessCreditmodel.TRANSAMOUNT = Convert.ToInt64(mDBEntity.RECEIPTNETAMOUNT);
                    e.RECEIPTNETAMOUNT = Convert.ToInt64(mDBEntity.RECEIPTNETAMOUNT);

                    try
                    {
                        BusinessCreditmodel.RECEIPTHDRID = mDBEntity.RECEIPTHDRID;
                        BusinessCreditmodel.JOBNUMBER = Apimodel.JobNumber;
                        BusinessCreditmodel.QUOTATIONID = mDBEntity.QUOTATIONID;
                        BusinessCreditmodel.PAYMENTOPTION = "Business Credit";
                        BusinessCreditmodel.ISPAYMENTSUCESS = 1;
                        BusinessCreditmodel.PAYMENTREFNUMBER = "Credit";
                        BusinessCreditmodel.PAYMENTREFMESSAGE = "approved";
                        BusinessCreditmodel.CREATEDBY = Convert.ToString(Apimodel.UserId);
                        BusinessCreditmodel.MODIFIEDBY = Convert.ToString(Apimodel.UserId);
                        BusinessCreditmodel.PAYMENTTYPE = "Business credit";
                        BusinessCreditmodel.TRANSMODE = "Credit";
                        BusinessCreditmodel.TRANSDESCRIPTION = "Business Credit";
                        BusinessCreditmodel.TRANSCURRENCY = mDBEntity.CURRENCY;
                        BusinessCreditmodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                        BusinessCreditmodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                        BusinessCreditmodel.ADDITIONALCHARGESTATUS = (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now") ? (BusinessCreditmodel.ISPAYMENTSUCESS == 1) ? "Paid" : "Rejected" : "";
                        e.CURRENCY = mDBEntity.CURRENCY;
                        e.JOBNUMBER = Apimodel.JobNumber;
                        DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(BusinessCreditmodel);

                        mFactory.SaveBusinessCreditTransactionDetails(mUIModel, Apimodel.UserId, DiscountCode, DiscountAmount);

                        byte[] bytes = null;

                        UserFactory.NewNotification(5113, "Payment Completed", mDBEntity.OPJOBNUMBER.ToString(), Convert.ToInt32(mDBEntity.JOBNUMBER), Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER.ToString(), Apimodel.UserId.ToString());

                        var pushdata = new { NOTIFICATIONTYPEID = 5113, NOTIFICATIONCODE = "Payment Completed", JOBNUMBER = mDBEntity.JOBNUMBER, BOOKINGID = mDBEntity.OPJOBNUMBER, QUOTATIONID = mDBEntity.QUOTATIONID, QUOTATIONNUMBER = mDBEntity.QUOTATIONNUMBER, USERID = Apimodel.UserId.ToString() };
                        SafeRun<bool>(() => { return _OneSignal.SendPushMessageByTag("Email", Apimodel.UserId.ToString(), "Payment Completed", pushdata); });

                        //------------------------------------------------------
                        QuotationDBFactory mQuoFactory = new QuotationDBFactory();
                        DBFactory.Entities.QuotationEntity quotationentity = mQuoFactory.GetById(mDBEntity.QUOTATIONID);
                        QuotationPreviewModel mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);
                        UserDBFactory UD = new UserDBFactory();
                        UserEntity UE = UD.GetUserFirstAndLastName(miUIModel.UserId);

                        UserProfileEntity mUserModel = _UFactory.GetUserProfileDetails(Apimodel.UserId, 1);
                        //-----------------------------------------------------

                        GenerateAdvanceReceiptPDF(
                             Convert.ToInt32(Apimodel.JobNumber),
                            "Advance Receipt",
                            mDBEntity.QUOTATIONID,
                            mDBEntity.OPJOBNUMBER,
                            Apimodel.UserId,
                           Convert.ToInt32(miUIModel.CHARGESETID),
                            mUserModel,
                            mUIModels
                        );

                        SendMailWithCard(miUIModel.UserId, "", "", Convert.ToString(miUIModel.ReceiptNetAmount),
                           null, null, miUIModel.Currency, "Job Booked", miUIModel.OPjobnumber, miUIModel.JobNumber, miUIModel.QuotationId,
                            mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, "Payment",
                           mUserModel.FIRSTNAME, "Additional Charge Payment", "Business credit");
                        SendMailtoGroup(miUIModel.OPjobnumber, "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE", "Paymentstatus", Convert.ToInt32(miUIModel.JobNumber), miUIModel.UserId, "Business Credit", "Additional Charge Payment", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), null, null, miUIModel.Currency, "Job Booked", mUIModels.ProductName, mUIModels.MovementTypeName, miUIModel.ShipperName, miUIModel.ConsigneeName, UE.COUNTRYNAME, mUIModels, miUIModel);
                    }
                    catch (Exception ex)
                    {
                        LogError("api/Payment", "SaveBusinessCredit(Additional)", ex.Message.ToString(), ex.StackTrace.ToString());
                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.InternalServerError,
                                Message = ex.ToString()
                            }),
                            StatusCode = HttpStatusCode.InternalServerError
                        };
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Thank you for your payment, your shipment is confirmed. A confirmation email has been sent to " + Convert.ToString(Apimodel.UserId)
                        })
                    };
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Payment Already Done" //return Success Message
                        })
                    };
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetPaymentMethods([FromBody]dynamic request)
        {
            DBFactory.Entities.PayementMenthods mDBEntity = _JFactory.GetPaymentDetailsByUserCountry(Convert.ToInt32(request.JobNumber), Convert.ToString(request.UserID));
            return Ok(mDBEntity);
        }

        [HttpPost]
        public HttpResponseMessage ApplyCouponCode([FromBody]CouponModel request)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                if (!ModelState.IsValid) return ErrorResponse(ModelState);
                CouponCode result = _JFactory.GetAgilityDiscountPrice(request.CouponCode, request.JobNumber, request.UserId);
                return BaseResponse(result, HttpStatusCode.OK);
            });
        }

        [HttpPost]
        public HttpResponseMessage HsbcPayment([FromBody]StripeModel request)
        {
            PaymentEntity mDBEntity = _JFactory.GetPaymentDetailsByJobNumber(
                request.JobNumber, request.UserId, request.CouponCode, request.RequestedAmount);
            HsbcOrder order = HsbcCreateOrder(mDBEntity, request.CouponCode);
            if (string.IsNullOrEmpty(order.orderNo))
            {
                return BaseResponse(
                    new { Data = new HSBCResponseDto { StatusCode = "False", proCode = "999998", proMsg = "Payment Done Already" } },
                    HttpStatusCode.ExpectationFailed);
            }
            string strOrder = JsonConvert.SerializeObject(order);
            string postData = HsbcEncryption.signAndEncrypt(strOrder);
            HSBCResponseDto res = sendRequest(postData, order.payType, request.HsbcMode);
            res.orderid = order.orderNo;
            return BaseResponse(res, HttpStatusCode.OK);
        }

        [HttpPost]
        public IHttpActionResult HsbcWebhook()
        {
            throw new DivideByZeroException();
            var responseContent = string.Empty;
            using (var reader = new StreamReader(Request.Content.ReadAsStreamAsync().Result))
            {
                responseContent = reader.ReadToEnd();
            }
            string json = HsbcEncryption.decryptAndVerify(responseContent);
            HSBCEnquiryStatus data = (HSBCEnquiryStatus)JsonConvert.DeserializeObject(json);

            SendEmailNotificationForExceptions("HsbcWebhook", "", json);

            if (data.proCode != null)
            {
                ThreadPool.QueueUserWorkItem(o =>
                {
                    // Save
                    // PDF
                    // Emails
                });
            }

            return Ok("SUCCESS");
        }

        [HttpPost]
        public IHttpActionResult HsbcResponse([FromBody]HsbcFrontResponse req)
        {
            var details = _JFactory.GetHsbcJobNumber(req.order_id);

            if (req.order_pay_code == "1111")
            {
                // Failed payment
                var routeData = new MPaymentResult()
                {
                    JOBNUMBER = details.JOBNUMBER,
                    QUOTATIONID = details.QUOTATIONID,
                    USERID = details.CREATEDBY,
                    DATA = req.order_pay_msg
                };
                return Ok(routeData);
            }
            else
            {
                var routeData = new MPaymentResult()
                {
                    JOBNUMBER = details.JOBNUMBER,
                    QUOTATIONID = details.QUOTATIONID,
                    USERID = details.CREATEDBY,
                    TOKEN = req.order_id
                };
                return Ok(routeData);
            }
        }

        [HttpGet]
        public HttpResponseMessage HsbcOrderStatusEnquiry(string order)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                string url = ConfigurationManager.AppSettings["hsbcurl"].ToString() + "merchant/payEnq";
                string clientID = ConfigurationManager.AppSettings["clientID"].ToString();
                string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
                var dd = DateTime.Now.ToString("yymmdd") + DateTime.Now.ToString("hhmmss");
                HSBCStatusDto data = new HSBCStatusDto()
                {
                    orderNo = ConfigurationManager.AppSettings["MerchantID"].ToString() + dd,
                    merId = ConfigurationManager.AppSettings["MerchantID"].ToString(),
                    merIp = ConfigurationManager.AppSettings["MerchantIP"].ToString(),
                    transType = "03",
                    orgOrderNo = order
                };
                var reqdata = HsbcEncryption.signAndEncrypt(JsonConvert.SerializeObject(data));
                HttpContent httpContent = new StringContent(reqdata);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("x-hsbc-client-id", clientID);
                httpClient.DefaultRequestHeaders.Add("x-hsbc-client-secret", clientSecret);
                HttpResponseMessage response = httpClient.PostAsync(url, httpContent).Result;

                HSBCResponseDto resp = new HSBCResponseDto()
                {
                    StatusCode = response.IsSuccessStatusCode.ToString(),
                    orderid = order
                };

                if (response.IsSuccessStatusCode)
                {
                    string json = HsbcEncryption.decryptAndVerify(response.Content.ReadAsStringAsync().Result);
                    var jo = (JObject)JsonConvert.DeserializeObject(json);
                    var res = (JObject)jo["response"];
                    resp.proCode = jo["response"]["proCode"].ToString();
                    resp.proMsg = jo["response"]["proMsg"] == null ? "" : jo["response"]["proMsg"].ToString();
                }
                return BaseResponse(resp, HttpStatusCode.OK);
            });
        }

        [HttpPost]
        public IHttpActionResult HsbcRefundResponse()
        {
            var responseContent = string.Empty;
            using (var reader = new StreamReader(Request.Content.ReadAsStreamAsync().Result))
            {
                responseContent = reader.ReadToEnd();
            }
            string json = HsbcEncryption.decryptAndVerify(responseContent);
            HsbcRefundResponse resp = (HsbcRefundResponse)JsonConvert.DeserializeObject(json);

            if (resp.common.returnCode == "000" && resp.response.proCode == "111111")
            {
                _JFactory.UpdateHsbcRefundStatus(resp.response.orderNo);
            }
            return Ok("SUCCESS");
        }

        # region Offline Wire Transfer
        [HttpPost]
        public IHttpActionResult GetWireTransferDetails([FromBody]dynamic request)
        {
            BENEFECIARYINFO BENEFECIARYDETAILS = new Controllers.BENEFECIARYINFO();
            List<SSPEntityListEntity> WtInfo = _JFactory.GetWireTransferDetails(Convert.ToString(request.UserID));

            List<CURRENCYLIST> cl = new List<CURRENCYLIST>();

            foreach (var i in WtInfo.GroupBy(c => c.CURRENCYCODE).Select(x => x.FirstOrDefault()))
            {
                CURRENCYLIST c = new CURRENCYLIST();
                c.Currency = i.CURRENCYCODE;
                cl.Add(c);
            }

            BENEFECIARYDETAILS.BENEFECIARYDETAILS = WtInfo;
            BENEFECIARYDETAILS.CURRENCYLIST = cl;

            return Ok(BENEFECIARYDETAILS);
        }

        [HttpPost]
        public HttpResponseMessage SaveOfflineWireTransfer([FromBody] PaymentTransactionDetailsModel Offmodel)
        {
            MPaymentResult e = new MPaymentResult();
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(Offmodel.JOBNUMBER));
            if (PaymentStatus.ToLower() == "notexist")
            {
                DataTable dtjobInfo = _JFactory.GetJobinfoBasedonBookingID(Convert.ToInt32(Offmodel.JOBNUMBER));

                DBFactory.Entities.QuotationEntity mDBEntity = _QFactory.GetById(Convert.ToInt64(dtjobInfo.Rows[0]["QuotationId"]));
                QuotationPreviewModel DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);

                CouponCode result = new CouponCode();
                if (!string.IsNullOrEmpty(Offmodel.DiscountCode))
                {
                    result = _JFactory.GetAgilityDiscountPrice(Offmodel.DiscountCode, Convert.ToString(Offmodel.JOBNUMBER), Convert.ToString(Offmodel.CREATEDBY));

                    if (result.CouponStatus.ToUpper() == "INVALID COUPON")
                    {
                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.ExpectationFailed,
                                Message = "Invalid Coupon code."
                            })
                        };
                    }
                    else
                    {
                        Offmodel.DiscountAmount = result.DiscountAmount;
                    }

                }
                OfflineWireTransfer(Offmodel, dtjobInfo, DUIModels);
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                int id = Convert.ToInt32(Offmodel.JOBNUMBER);
                DBFactory.Entities.PaymentEntity PaymentDBEntity = mFactory.GetPaymentDetailsByJobNumber(Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToString(Offmodel.CREATEDBY));
                PaymentModel PaymentModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(PaymentDBEntity);

                // To Customer
                SendMailWithCard(Offmodel.CREATEDBY, "", "Wire transfer", Convert.ToString(Offmodel.TRANSAMOUNT), null, null,
                    Offmodel.PAIDCURRENCY, "Job Booked", dtjobInfo.Rows[0]["OPERATIONALJOBNUMBER"].ToString(), Offmodel.JOBNUMBER, DUIModels.QuotationId,
                     DUIModels.ProductName, DUIModels.MovementTypeName, dtjobInfo.Rows[0]["Shipper"].ToString(), dtjobInfo.Rows[0]["Consignee"].ToString(),
                    "Payment", "", "Offline");
                Offmodel.OPJOBNUMBER = Offmodel.OPERATIONALJOBNUMBER; // temp fix 
                SendMailForCountryFinanceCashier("finance", Offmodel, "", "Payment", "", "", DUIModels,PaymentModel);

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Thank you for your payment, your Booking is under process. A confirmation email will be sent to " + Convert.ToString(Offmodel.CREATEDBY) + " on successful payment processing." //return Success Message
                    })
                };
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                int id = Convert.ToInt32(Offmodel.JOBNUMBER);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToString(Offmodel.CREATEDBY));
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = Offmodel.CREATEDBY;

                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    DataTable dtjobInfo = _JFactory.GetJobinfoBasedonBookingID(Convert.ToInt32(Offmodel.JOBNUMBER));
                    DBFactory.Entities.QuotationEntity mDBqEntity = _QFactory.GetById(Convert.ToInt64(dtjobInfo.Rows[0]["QuotationId"]));
                    QuotationPreviewModel DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBqEntity);
                    OfflineWireTransfer(Offmodel, dtjobInfo, DUIModels);


                    // To Customer
                    SendMailWithCard(Offmodel.CREATEDBY, "", "Wire transfer", Convert.ToString(Offmodel.TRANSAMOUNT), null, null,
                        Offmodel.PAIDCURRENCY, "Job Booked", dtjobInfo.Rows[0]["OPERATIONALJOBNUMBER"].ToString(), Offmodel.JOBNUMBER, DUIModels.QuotationId,
                         DUIModels.ProductName, DUIModels.MovementTypeName, dtjobInfo.Rows[0]["Shipper"].ToString(), dtjobInfo.Rows[0]["Consignee"].ToString(),
                        "Additional Charge Payment", "", "Offline");
                    Offmodel.OPJOBNUMBER = Offmodel.OPERATIONALJOBNUMBER; // temp fix
                    SendMailForCountryFinanceCashier("finance", Offmodel, "", "Additional Charge Payment", "", Convert.ToString(miUIModel.ADDITIONALCHARGEAMOUNT), DUIModels, miUIModel);
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Thank you for your payment, your Booking is under process. A confirmation email will be sent to " + Convert.ToString(Offmodel.CREATEDBY) + " on successful payment processing." //return Success Message
                        })
                    };
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Payment alredy done!" //return Success Message
                        }),

                    };
                }


            }

        }

        private void OfflineWireTransfer(PaymentTransactionDetailsModel Offmodel, DataTable dtjobInfo, QuotationPreviewModel DUIModels)
        {
            Offmodel.RECEIPTHDRID = Convert.ToInt64(dtjobInfo.Rows[0]["RECEIPTHDRID"]);
            Offmodel.QUOTATIONID = Convert.ToInt64(dtjobInfo.Rows[0]["QuotationId"]);
            Offmodel.PAYMENTOPTION = "Offline";
            Offmodel.ISPAYMENTSUCESS = 1;
            Offmodel.PAYMENTREFMESSAGE = "Pending";
            Offmodel.MODIFIEDBY = Offmodel.CREATEDBY;
            Offmodel.PAYMENTTYPE = "Wire transfer";
            Offmodel.PAYMENTREFNUMBER = Offmodel.TRANSCHEQUENO;
            Offmodel.TRANSMODE = "Bank";

            if (Offmodel.TRANSAMOUNT != 0)
            {
                double WTTransAmount = Convert.ToDouble(Offmodel.TRANSAMOUNT);
                Offmodel.TRANSAMOUNT = WTTransAmount;
            }

            DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel =
                Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(Offmodel);

            _JFactory.SaveOfflinePaymentTransactionDetails(mUIModel, Offmodel.DiscountCode, Offmodel.DiscountAmount);

            CheckIsMandatoryDocs(Offmodel.JOBNUMBER.ToString(), Offmodel.CREATEDBY, Convert.ToString(mUIModel.QUOTATIONID), dtjobInfo.Rows[0]["OPERATIONALJOBNUMBER"].ToString());

            byte[] paymentpdfdata = PaymentTransaction(Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToInt32(Offmodel.QUOTATIONID), Convert.ToString(Offmodel.CREATEDBY), mUIModel, DUIModels, null);


            Guid obj = Guid.NewGuid();
            string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();
            _JFactory.SaveOfflinePaymentBookingSummaryDoc(paymentpdfdata, Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToInt32(mUIModel.QUOTATIONID), Offmodel.CREATEDBY, dtjobInfo.Rows[0]["OPERATIONALJOBNUMBER"].ToString(), obj.ToString(), EnvironmentName);

        }
        #endregion

        # region Offline Pay At Branch
        [HttpPost]
        public IHttpActionResult GetPayAtBranchDetails([FromBody]dynamic request)
        {
            PAYATBRANCHINFO PAYATBRANCHINFO = new Controllers.PAYATBRANCHINFO();
            List<SSPBranchListEntity> WtInfo = _JFactory.GetPayAtBrachDetails(Convert.ToString(request.UserID));

            List<CURRENCYLIST> cl = new List<CURRENCYLIST>();

            foreach (var i in WtInfo.GroupBy(c => c.CURRENCYCODE).Select(x => x.FirstOrDefault()))
            {
                CURRENCYLIST c = new CURRENCYLIST();
                c.Currency = i.CURRENCYCODE;
                cl.Add(c);
            }

            PAYATBRANCHINFO.BRANCHADDRESS = WtInfo;
            PAYATBRANCHINFO.CURRENCYLIST = cl;

            return Ok(PAYATBRANCHINFO);
        }

        [HttpPost]
        public HttpResponseMessage SaveOffinePayAtBranch([FromBody] PaymentTransactionDetailsModel Offmodel)
        {
            MPaymentResult e = new MPaymentResult();
            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(Offmodel.JOBNUMBER));
            if (PaymentStatus.ToLower() == "notexist")
            {
                DataTable dtjobInfo = _JFactory.GetJobinfoBasedonBookingID(Convert.ToInt32(Offmodel.JOBNUMBER));

                CouponCode result = new CouponCode();
                if (!string.IsNullOrEmpty(Offmodel.DiscountCode))
                {
                    result = _JFactory.GetAgilityDiscountPrice(Offmodel.DiscountCode, Convert.ToString(Offmodel.JOBNUMBER), Convert.ToString(Offmodel.CREATEDBY));

                    if (result.CouponStatus.ToUpper() == "INVALID COUPON")
                    {
                        return new HttpResponseMessage()
                        {
                            Content = new JsonContent(new
                            {
                                StatusCode = HttpStatusCode.ExpectationFailed,
                                Message = "Invalid Coupon code."
                            })
                        };
                    }
                    else
                    {
                        Offmodel.DiscountAmount = result.DiscountAmount;
                    }
                }
                DBFactory.Entities.QuotationEntity mDBEntity = _QFactory.GetById(Convert.ToInt64(dtjobInfo.Rows[0]["QuotationId"]));
                QuotationPreviewModel DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);

                OfflinePayAtBranch(Offmodel, dtjobInfo, DUIModels);
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                int id = Convert.ToInt32(Offmodel.JOBNUMBER);
                DBFactory.Entities.PaymentEntity PaymentEntity = mFactory.GetPaymentDetailsByJobNumber(Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToString(Offmodel.CREATEDBY));
                PaymentModel PaymentModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(PaymentEntity);
                if (Offmodel.PAYMENTTYPE.ToLower() == "pay at branch")
                {
                    SendMailForSuccessPayAtBranch(Offmodel, "Pay at branch", DUIModels,
                        Offmodel.SITECITY + ", " + Offmodel.SITEADDRESS, Convert.ToInt32(Offmodel.JOBNUMBER), Offmodel.OPJOBNUMBER, "Payment", "", "");
                    Offmodel.OPJOBNUMBER = Offmodel.OPERATIONALJOBNUMBER; // temp fix
                    SendMailForCountryFinanceCashier("cashier", Offmodel, Offmodel.SITECITY + ", " + Offmodel.SITEADDRESS, "Payment","","",DUIModels,PaymentModel);
                }

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Thank you for your payment, your Booking is under process. A confirmation email will be sent to " + Convert.ToString(Offmodel.CREATEDBY) + " on successful payment processing." //return Success Message
                    })
                };
            }
            else
            {
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                int id = Convert.ToInt32(Offmodel.JOBNUMBER);
                DBFactory.Entities.PaymentEntity mDBEntity = mFactory.GetPaymentDetailsByJobNumber(Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToString(Offmodel.CREATEDBY));
                PaymentModel miUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                miUIModel.UserId = Offmodel.CREATEDBY;

                if (miUIModel.ADDITIONALCHARGESTATUS != null && miUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    DataTable dtjobInfo = _JFactory.GetJobinfoBasedonBookingID(Convert.ToInt32(Offmodel.JOBNUMBER));
                    DBFactory.Entities.QuotationEntity mDBqEntity = _QFactory.GetById(Convert.ToInt64(dtjobInfo.Rows[0]["QuotationId"]));
                    QuotationPreviewModel DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBqEntity);
                    Offmodel.ADDITIONALCHARGEAMOUNT = miUIModel.ADDITIONALCHARGEAMOUNT;
                    Offmodel.ADDITIONALCHARGESTATUS = miUIModel.ADDITIONALCHARGESTATUS;
                    Offmodel.ADDITIONALCHARGEAMOUNTINUSD = miUIModel.ADDITIONALCHARGEAMOUNTINUSD;
                    OfflinePayAtBranch(Offmodel, dtjobInfo, DUIModels);

                    if (Offmodel.PAYMENTTYPE.ToLower() == "pay at branch")
                    {
                        SendMailForSuccessPayAtBranch(Offmodel, "Pay at branch", DUIModels,
                            Offmodel.SITECITY + ", " + Offmodel.SITEADDRESS, Convert.ToInt32(Offmodel.JOBNUMBER), Offmodel.OPJOBNUMBER, "Additional Charge Payment", "", Convert.ToString(Offmodel.ADDITIONALCHARGEAMOUNT));
                        Offmodel.OPJOBNUMBER = Offmodel.OPERATIONALJOBNUMBER; // temp fix
                        SendMailForCountryFinanceCashier("cashier", Offmodel, Offmodel.SITECITY + ", " + Offmodel.SITEADDRESS, "Additional Charge Payment", "", Convert.ToString(Offmodel.ADDITIONALCHARGEAMOUNT), DUIModels, miUIModel);
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Thank you for your payment, your Booking is under process. A confirmation email will be sent to " + Convert.ToString(Offmodel.CREATEDBY) + " on successful payment processing." //return Success Message
                        })
                    };
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Payment alredy done!" //return Success Message
                        })
                    };
                }
            }
        }

        private void OfflinePayAtBranch(PaymentTransactionDetailsModel Offmodel, DataTable dtjobInfo, QuotationPreviewModel DUIModels)
        {
            Offmodel.RECEIPTHDRID = Convert.ToInt64(dtjobInfo.Rows[0]["RECEIPTHDRID"]);
            Offmodel.QUOTATIONID = Convert.ToInt64(dtjobInfo.Rows[0]["QuotationId"]);
            Offmodel.PAYMENTOPTION = "Offline";
            Offmodel.ISPAYMENTSUCESS = 1;
            Offmodel.PAYMENTREFMESSAGE = "Pending";
            Offmodel.MODIFIEDBY = Offmodel.CREATEDBY;
            Offmodel.PAYMENTTYPE = "Pay at branch";
            Offmodel.PAYMENTREFNUMBER = Offmodel.TRANSCHEQUENO;


            if (Offmodel.TRANSAMOUNT != 0)
            {
                double WTTransAmount = Convert.ToDouble(Offmodel.TRANSAMOUNT);
                Offmodel.TRANSAMOUNT = WTTransAmount;
            }

            DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel =
                Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(Offmodel);
            //Save Call
            _JFactory.SaveOfflinePaymentTransactionDetails(mUIModel, Offmodel.DiscountCode, Offmodel.DiscountAmount);

            CheckIsMandatoryDocs(Offmodel.JOBNUMBER.ToString(), Offmodel.CREATEDBY, Convert.ToString(mUIModel.QUOTATIONID), dtjobInfo.Rows[0]["OPERATIONALJOBNUMBER"].ToString());

            byte[] paymentpdfdata = PaymentTransaction(Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToInt32(Offmodel.QUOTATIONID), Convert.ToString(Offmodel.CREATEDBY), mUIModel, DUIModels, Offmodel.SITEADDRESS);

            Guid obj = Guid.NewGuid();
            string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();
            _JFactory.SaveOfflinePaymentBookingSummaryDoc(paymentpdfdata, Convert.ToInt32(Offmodel.JOBNUMBER), Convert.ToInt32(mUIModel.QUOTATIONID), Offmodel.CREATEDBY, dtjobInfo.Rows[0]["OPERATIONALJOBNUMBER"].ToString(), obj.ToString(), EnvironmentName);
        }

        private void SendMailForSuccessPayAtBranch(PaymentTransactionDetailsModel Offmodel, string paymentType, QuotationPreviewModel DUIModels,
            string branchAddress, int jobNumber, string OPjobnumber, string flowType, string FirstName = "", string additionalChargeAmount = "")
        {
            try
            {
                string AttachmentIds = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                DataSet documents = mFactory.GetDocuments(jobNumber);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                message.To.Add(new MailAddress(Offmodel.CREATEDBY));
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - " + flowType + " in progress" + "( Booking ID #" + OPjobnumber + " )" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                if (Offmodel.TRANSMODE.ToLower() == "cash")
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/payment-PayAtBranch-success.html");
                }
                else
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/payment-PayAtBranch-success-cheque.html");
                }
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                if (FirstName == "")
                {
                    UserDBFactory UD = new UserDBFactory();
                    UserEntity UE = UD.GetUserFirstAndLastName(Offmodel.CREATEDBY);
                    body = body.Replace("{user_name}", UE.FIRSTNAME);
                }
                else
                {
                    body = body.Replace("{user_name}", FirstName);
                }
                if (additionalChargeAmount == "")
                {
                    body = body.Replace("{Amount}", Offmodel.TRANSCURRENCY.ToString() + " " +
                        mFactoryRound.RoundedValue(Convert.ToString(Offmodel.TRANSAMOUNT), Offmodel.TRANSCURRENCY.ToString()));
                }
                else
                {
                    body = body.Replace("{Amount}", Offmodel.TRANSCURRENCY.ToString() + " " + mFactoryRound.RoundedValue(additionalChargeAmount, Offmodel.TRANSCURRENCY.ToString()));
                }

                body = body.Replace("{PaymentMode}", Offmodel.PAYMENTTYPE);
                body = body.Replace("{TransactionMode}", Offmodel.TRANSMODE);
                string branchname = branchAddress;
                body = body.Replace("{Branch}", branchname);
                body = body.Replace("{TransDate}", Convert.ToString(Offmodel.TRANSCHEQUEDATE.Value.ToShortDateString()));
                body = body.Replace("{OpJobNumber}", OPjobnumber);


                if (Offmodel.TRANSMODE.ToLower() == "cash")
                {
                    body = body.Replace("{Comment}", Offmodel.TRANSDESCRIPTION);
                }
                else
                {

                    body = body.Replace("{TransDate}", Convert.ToString(Offmodel.TRANSCHEQUEDATE.Value.ToShortDateString()));
                    body = body.Replace("{TransBankName}", Offmodel.TRANSBANKNAME);
                    body = body.Replace("{TransBranchName}", Offmodel.TRANSBRANCHNAME);
                    body = body.Replace("{TransChequeNo}", Offmodel.TRANSCHEQUENO);
                    body = body.Replace("{TransChequeDate}", Convert.ToString(Offmodel.TRANSCHEQUEDATE.Value.ToShortDateString()));
                    body = body.Replace("{TransCDDescription}", Offmodel.TRANSDESCRIPTION);
                }

                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Offmodel.CREATEDBY, "", "",
      "JobBooking", paymentType, OPjobnumber, Offmodel.CREATEDBY, body, AttachmentIds, true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "SendMailForSuccessPayAtBranch", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ArgumentException("Something Went Wrong.");
            }
        }

        #endregion

        #region Offline Private methods
        private void SendMailForCountryFinanceCashier(string role, PaymentTransactionDetailsModel model, string branchAddress, string flowType, string FirstName = "", string additionalChargeAmount = "", QuotationPreviewModel quotation = null, PaymentModel payment = null)
        {
            try
            {
                string AttachmentIds = string.Empty;
                JobBookingDbFactory mFactory = new JobBookingDbFactory();
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                DataSet documents = mFactory.GetDocuments(Convert.ToInt32(model.JOBNUMBER));
                string recipientsList = "";
                string roles = string.Empty;
                if (role.ToLower() == "cashier")
                {
                    roles = "SSPCOUNTRYCASHIER";
                }
                else if (role.ToLower() == "finance")
                {
                    roles = "SSPCOUNTRYFINANCE";
                }
                recipientsList = mFactory.GetEmailList(roles, model.OPJOBNUMBER, "Paymentstatus");
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");
                if (recipientsList == "") { message.To.Add(new MailAddress("vsivaram@agility.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId)); //Adding Multiple BCC email Id
                    }
                }
                string DevEmailids = System.Configuration.ConfigurationManager.AppSettings["GlobalPaymentNotifier"];
                string[] ids = DevEmailids.Split(',');
                foreach (var item in ids)
                {
                    message.Bcc.Add(new MailAddress(item));
                }
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - offline " + flowType.ToLower() + " notification" + "(Booking ID #" + model.OPJOBNUMBER + ")" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                if (model.PAYMENTTYPE.ToLower() == "wire transfer")
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/WireTransferNotification.html");
                }
                else if (model.TRANSMODE.ToLower() == "cash")
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/CashNotification.html");
                }
                else
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/ChequeNotification.html");
                }
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(model.CREATEDBY);
                //using streamreader for reading my email template 
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                if (FirstName == "")
                {
                    body = body.Replace("{user_name}", UE.FIRSTNAME);
                }
                else
                {
                    body = body.Replace("{user_name}", FirstName);
                }
                body = body.Replace("{country}", UE.COUNTRYNAME);
                if (additionalChargeAmount == "")
                {
                    body = body.Replace("{Amount}", model.TRANSCURRENCY.ToString() + " " + mFactoryRound.RoundedValue(Convert.ToString(model.TRANSAMOUNT),
                        model.TRANSCURRENCY.ToString()));
                }
                else
                {
                    body = body.Replace("{Amount}", model.TRANSCURRENCY.ToString() + " " + mFactoryRound.RoundedValue(additionalChargeAmount, model.TRANSCURRENCY.ToString()));
                }

                body = body.Replace("{PaymentMode}", model.PAYMENTTYPE);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
                if (model.PAYMENTTYPE.ToLower() == "wire transfer")
                {

                    SSPEntityListEntity EntityList = _JFactory.GetEntitydetailsBasedonEntity(Convert.ToInt32(model.BRANCHENTITYKEY));
                    string branchname = EntityList.BANKNAME + " " + EntityList.BRANCH;

                    body = body.Replace("{User}", model.CREATEDBY);
                    body = body.Replace("{BeneficiaryDetails}", "Benefeciary details:");
                    body = body.Replace("{AccHolderName}", EntityList.BENEFICIARYNAME);
                    body = body.Replace("{BankBranchName}", branchname);
                    body = body.Replace("{AccountNo}", EntityList.BANKACCOUNTNUMBER);
                    body = body.Replace("{SwiftCode}", EntityList.SWIFTCODE);
                    body = body.Replace("{IFSCIBANCode}", EntityList.IFSCCODE);

                    body = body.Replace("{TransactionDetails}", "Transaction details:");
                    body = body.Replace("{TransBank}", model.TRANSBANKNAME);
                    body = body.Replace("{TransBranchName}", model.TRANSBRANCHNAME);
                    body = body.Replace("{TransReffNo}", model.TRANSCHEQUENO);
                    body = body.Replace("{TransDate}", Convert.ToString(model.TRANSCHEQUEDATE.Value.ToShortDateString()));
                    body = body.Replace("{TransDesc}", model.TRANSDESCRIPTION);
                }
                else if (model.PAYMENTTYPE.ToLower() == "pay at branch")
                {
                    body = body.Replace("{TransactionMode}", model.TRANSMODE);
                    body = body.Replace("{Branch}", branchAddress);
                    body = body.Replace("{TransDate}", Convert.ToString(model.TRANSCHEQUEDATE.Value.ToShortDateString()));
                    if (model.TRANSMODE.ToLower() == "cash")
                    {
                        body = body.Replace("{Comment}", model.TRANSDESCRIPTION);
                    }
                    else
                    {

                        body = body.Replace("{TransDate}", Convert.ToString(model.TRANSCHEQUEDATE.Value.ToShortDateString()));
                        body = body.Replace("{TransBankName}", model.TRANSBANKNAME);
                        body = body.Replace("{TransBranchName}", model.TRANSBRANCHNAME);
                        body = body.Replace("{TransChequeNo}", model.TRANSCHEQUENO);
                        body = body.Replace("{TransChequeDate}", Convert.ToString(model.TRANSCHEQUEDATE.Value.ToShortDateString()));
                        body = body.Replace("{TransCDDescription}", model.TRANSDESCRIPTION);
                    }
                }
                body = body.Replace("{OpJobNumber}", model.OPJOBNUMBER);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                body = body.Replace("{Mode}", quotation.ProductName);
                body = body.Replace("{MovementType}", quotation.MovementTypeName);
                body = body.Replace("{OriginCountry}", quotation.OCOUNTRYNAME);
                body = body.Replace("{DestinationCountry}", quotation.DCOUNTRYNAME);
                body = body.Replace("{PayingCountry}", UE.COUNTRYNAME);
                string placeportconent = string.Empty;
                string specilaHandlingRequirements = string.Empty;
                placeportconent = PrapareCityPortData(quotation);
                body = body.Replace("{placeportconent}", placeportconent);
                specilaHandlingRequirements = PrepareSpecialHandlingInstructions(payment);
                body = body.Replace("{specilaHandlingRequirements}", specilaHandlingRequirements);
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                //          DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                //"JobBooking", "Offline payment", model.OPjobnumber, model.UserId, body, AttachmentIds, false, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "SendMailForCountryFinanceCashier", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ArgumentException("Something Went Wrong.");
            }
        }

        private void CheckIsMandatoryDocs(string jobNumber, string user, string quotationid, string BookigID)
        {
            try
            {
                int Count = _JFactory.CheckIsMandatoryDocsUploaded(jobNumber);
                if (Count > 0)
                    SendMailForMandatoryDocs(user, jobNumber, quotationid, BookigID);
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "CheckIsMandatoryDocs", ex.Message.ToString(), ex.StackTrace.ToString());
            }
        }
        private void SendMailForMandatoryDocs(string Email, string jobNumber, string quotationid, string BookingID)
        {
            string resetLink = string.Empty;
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = "Mandatory documents are needed before goods availability to Agility" + EnvironmentName;
            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/MandatoryDocs.html");
            string body = string.Empty;
            string url = "JobNumber=" + jobNumber + "?id=" + quotationid;
            string t1 = EncryptId(url);
            string reqdocument = t1.ToString();
            resetLink = Actionlink + "JobBooking/BookingSummary?" + reqdocument;
            UserDBFactory UD = new UserDBFactory();
            UserEntity UE = UD.GetUserFirstAndLastName(Email);
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }

            body = body.Replace("{Message}", "<p>You have not uploaded the mandatory documents needed to process your shipment.");
            body = body.Replace("{Message1}", "<p>You need to upload these before the pickup date, or this might result in delay of pickup and the shipment.");
            body = body.Replace("{user}", UE.FIRSTNAME);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{btnName}", "Upload document Now");
            body = body.Replace("{documentreq}", resetLink);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            //  DynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
            //"JobBooking", "Mandatory Documents", BookingID, Email, body, "", true, message.Subject.ToString());
        }
        public byte[] PaymentTransaction(int JobNumber, int QuotationId, string UserID, PaymentTransactionDetailsEntity mUIModel, QuotationPreviewModel mQuotaionpreview, string SITEADDRESS, string ChargeSetId = "")
        {
            byte[] bytes = null;


            JobBookingModel mbookingModel = new JobBookingModel();
            PaymentTransactionDetailsEntity paymentTransactEntity = (PaymentTransactionDetailsEntity)mUIModel;
            PaymentTransactionDetailsModel paymentTrans = Mapper.Map<PaymentTransactionDetailsEntity, PaymentTransactionDetailsModel>(paymentTransactEntity);
            DBFactory.Entities.PaymentEntity paymentEntity = _JFactory.GetPaymentDetailsByJobNumber(JobNumber, UserID); // HttpContext.User.Identity.Name
            PaymentModel mpayment = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(paymentEntity);
            DBFactory.Entities.JobBookingEntity bookingEntity = _JFactory.GetQuotedJobDetails(Convert.ToInt32(QuotationId), Convert.ToInt64(JobNumber));
            mbookingModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(bookingEntity);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bytes = GeneratePaymentTransactionPdf(memoryStream, paymentTrans, mpayment, mbookingModel, mQuotaionpreview, JobNumber, QuotationId, SITEADDRESS, ChargeSetId);
                memoryStream.Close();
            }
            return bytes;
        }
        public byte[] GeneratePaymentTransactionPdf(MemoryStream memoryStream, PaymentTransactionDetailsModel transaction, PaymentModel payment, JobBookingModel booking, QuotationPreviewModel mQuotaionpreview, int JobNumber, int QuotationId, string SITEADDRESS, string ChargeSetId = "")
        {
            z = 0;
            UserDBFactory mFactory = new UserDBFactory();
            JobBookingDbFactory JFactory = new JobBookingDbFactory();
            JobBookingModel mUIModel = new JobBookingModel();
            var mUserModel = mFactory.GetUserProfileDetails(mQuotaionpreview.CreatedBy, 1);


            int DecimalCount = mFactory.GetDecimalPointByCurrencyCode(mQuotaionpreview.PreferredCurrencyId);
            QuotationPreviewModel mUIModels = mQuotaionpreview;
            Document document = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            document.Open();
            var ev = new ITextEvents();
            ev.mUserModel = mUserModel;
            ev.mUIModels = mUIModels;
            writer.PageEvent = ev;

            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 1;
            string mDocumentHeading = "Booking Request";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            document.Add(mHeadingtable);

            document.Add(mFormattingpdf.LineSeparator());
            document.Add(bookingInfoTableModel(booking));
            if (mQuotaionpreview.ProductTypeId == 5)
            {
                document.Add(containerTableModel(booking));
                if (!ReferenceEquals(booking.JobShipmentItems, null) && (booking.JobShipmentItems.Count > 0))
                {
                    document.Add(cargoTableModel(booking));
                }
            }
            else if (mQuotaionpreview.ProductTypeId == 6)
            {
                document.Add(cargoTableModel(booking));
            }
            if (string.IsNullOrEmpty(ChargeSetId))
            {
                if (mQuotaionpreview.QuotationCharges.Any())
                {
                    var mOrigincharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    var mDestinationcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    var mINTcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    var mAddcharges = mQuotaionpreview.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                    if (mOrigincharges.Any())
                        document.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mQuotaionpreview.PreferredCurrencyId));

                    if (mINTcharges.Any())
                        document.Add(ChargesTableModel(mINTcharges, "International Charges", mQuotaionpreview.PreferredCurrencyId));

                    if (mDestinationcharges.Any())
                        document.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mQuotaionpreview.PreferredCurrencyId));

                    if (mAddcharges.Any())
                        document.Add(ChargesTableModel(mAddcharges, "Optional Charges", mQuotaionpreview.PreferredCurrencyId));
                }
            }
            else
            {
                try
                {
                    DBFactory.Entities.JobBookingEntity mDBEntity = JFactory.GetQuotedJobDetails(QuotationId, JobNumber);
                    mUIModel = Mapper.Map<DBFactory.Entities.JobBookingEntity, JobBookingModel>(mDBEntity);
                    var mAddcharges1 = mUIModel.PaymentCharges.Where(x => x.CHARGESOURCE.ToLower() == "f" && (x.CHARGESTATUS != null && (x.CHARGESTATUS.ToLower() == "pay now" || x.CHARGESTATUS.ToLower() == "pay later")));
                    if (mAddcharges1.Any())
                        document.Add(ChargesTableAddModel(mAddcharges1, "Additional Charges", mQuotaionpreview.PreferredCurrencyId));
                }
                catch (Exception ex)
                {
                    LogError("api/Payment", "GeneratePaymentTransactionPdf", ex.Message.ToString(), ex.StackTrace.ToString());
                }
            }
            PdfPTable mgrossTotalBorder = new PdfPTable(2);
            PdfPTable mdiscountTotalBorder = new PdfPTable(2);
            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            QuotationDBFactory mFactoryRound = new QuotationDBFactory();
            if (booking.ReceiptHeaderDetails[0].DISCOUNTAMOUNT != 0)
            {

                mgrossTotalBorder.SetWidths(new float[] { 50, 50 });
                mgrossTotalBorder.WidthPercentage = 100;
                mgrossTotalBorder.SpacingBefore = 10;
                mgrossTotalBorder.DefaultCell.Border = 0;
                mgrossTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mgrossChargeTotal = new PdfPTable(2);
                mgrossChargeTotal.SetWidths(new float[] { 50, 50 });
                mgrossChargeTotal.WidthPercentage = 100;
                mgrossChargeTotal.AddCell((mFormattingpdf.TableHeading("Gross Total", Element.ALIGN_LEFT)));
                mgrossChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].RECEIPTAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));

                PdfPCell mgrosscells = new PdfPCell(mgrossChargeTotal);
                mgrossTotalBorder.AddCell(mgrosscells);

                mdiscountTotalBorder.SetWidths(new float[] { 50, 50 });
                mdiscountTotalBorder.WidthPercentage = 100;
                mdiscountTotalBorder.SpacingBefore = 10;
                mdiscountTotalBorder.DefaultCell.Border = 0;
                mdiscountTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mdiscountChargeTotal = new PdfPTable(2);
                mdiscountChargeTotal.SetWidths(new float[] { 50, 50 });
                mdiscountChargeTotal.WidthPercentage = 100;
                mdiscountChargeTotal.AddCell((mFormattingpdf.TableHeading("Discount Amount", Element.ALIGN_LEFT)));
                mdiscountChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].DISCOUNTAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));

                PdfPCell mdiscountcells = new PdfPCell(mdiscountChargeTotal);
                mdiscountTotalBorder.AddCell(mdiscountcells);
                mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotalBorder.WidthPercentage = 100;
                mQuoteChargeTotalBorder.SpacingBefore = 10;
                mQuoteChargeTotalBorder.DefaultCell.Border = 0;
                mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mQuoteChargeTotal = new PdfPTable(2);
                mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotal.WidthPercentage = 100;
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(booking.ReceiptHeaderDetails[0].RECEIPTNETAMOUNT), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                //////////////////////////////////////////////////////////////////////////////
                if (transaction.PAIDCURRENCY != mQuotaionpreview.PreferredCurrencyId)
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Payment Amount", Element.ALIGN_LEFT)));
                    var ct = JFactory.GetCurrencyConversion(booking.ReceiptHeaderDetails[0].RECEIPTNETAMOUNT.ToString(), mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                }

                PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
                mQuoteChargeTotalBorder.AddCell(mTotalcells);
                document.Add(mgrossTotalBorder);
                document.Add(mdiscountTotalBorder);
                document.Add(mQuoteChargeTotalBorder);

            }
            else
            {

                mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotalBorder.WidthPercentage = 100;
                mQuoteChargeTotalBorder.SpacingBefore = 10;
                mQuoteChargeTotalBorder.DefaultCell.Border = 0;
                mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

                PdfPTable mQuoteChargeTotal = new PdfPTable(2);
                mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
                mQuoteChargeTotal.WidthPercentage = 100;
                mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
                if (string.IsNullOrEmpty(ChargeSetId))
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(mQuotaionpreview.GrandTotal), mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                }
                else
                {
                    if (!string.IsNullOrEmpty(ChargeSetTotal))
                        mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mQuotaionpreview.PreferredCurrencyId + " " + mFactoryRound.RoundedValue(ChargeSetTotal, mQuotaionpreview.PreferredCurrencyId), Element.ALIGN_RIGHT)));
                }
                /////////////////////////////////////////////////////////////////////////////////////////
                if (transaction.PAIDCURRENCY != mQuotaionpreview.PreferredCurrencyId)
                {
                    mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Payment Amount", Element.ALIGN_LEFT)));
                    if (string.IsNullOrEmpty(ChargeSetId))
                    {
                        var ct = JFactory.GetCurrencyConversion(mQuotaionpreview.GrandTotal.ToString(), mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                        mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ChargeSetTotal))
                        {
                            var ct = JFactory.GetCurrencyConversion(ChargeSetTotal, mQuotaionpreview.PreferredCurrencyId.ToString(), transaction.PAIDCURRENCY.ToString());
                            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(transaction.PAIDCURRENCY + " " + ct.Rows[0]["currency"], Element.ALIGN_RIGHT)));
                        }
                    }
                }

                PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
                mQuoteChargeTotalBorder.AddCell(mTotalcells);
                document.Add(mQuoteChargeTotalBorder);
            }

            document.Add(ShipperTableModel(booking));
            if (transaction.PAYMENTTYPE.ToLower() == "wire transfer")
            {
                document.Add(PaymenTabletModel(payment));
                document.Add(TransactionTabletModel(transaction));
            }
            else if (transaction.PAYMENTTYPE.ToLower() == "pay at branch")
            {
                document.Add(payatbranchModel(transaction, SITEADDRESS));
            }
            document.Close();
            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        public static PdfPTable bookingInfoTableModel(JobBookingModel model)
        {
            SSPJobDetailsModel jobdetails = new SSPJobDetailsModel();
            if (model.JobItems.Count > 0)
            {
                foreach (var item in model.JobItems)
                {
                    jobdetails = item;
                }
            }

            var MName = string.Empty;
            var Ploading = string.Empty;
            var Pdischarge = string.Empty;
            if (model.PRODUCTID == 3)
            {
                if (model.MOVEMENTTYPENAME == "Port to port") { MName = "Airport to airport"; }
                if (model.MOVEMENTTYPENAME == "Door to port") { MName = "Door to airport"; }
                if (model.MOVEMENTTYPENAME == "Port to door") { MName = "Airport to door"; }
                if (model.MOVEMENTTYPENAME == "Door to door") { MName = "Door to door"; }
                Ploading = "Airport of loading";
                Pdischarge = "Airport of discharge";
            }
            else
            {
                MName = model.MOVEMENTTYPENAME;
                Ploading = "Port of loading";
                Pdischarge = "Port of discharge";
            }

            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mbookingbordertable = new PdfPTable(3);
            mbookingbordertable.WidthPercentage = 100;
            mbookingbordertable.SpacingBefore = 10;
            mbookingbordertable.DefaultCell.Border = 0;
            PdfPTable mbookingTab = new PdfPTable(9);
            mbookingTab.SetWidths(new float[] { 10, 1, 13, 17, 1, 20, 18, 1, 20 });
            mbookingTab.WidthPercentage = 100;
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Booking ID")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(jobdetails.OPERATIONALJOBNUMBER == null ? "" : jobdetails.OPERATIONALJOBNUMBER)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Origin City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.ORIGINPLACENAME == null ? "" : model.ORIGINPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold(model.DESTINATIONPLACENAME == null ? "" : "Destination City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPLACENAME == null ? "" : ":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPLACENAME == null ? "" : model.DESTINATIONPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Mode of transport")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.PRODUCTNAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Movement type")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(MName)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Enquiry Date")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DATEOFENQUIRY == null ? "" : Convert.ToString(model.DATEOFENQUIRY.ToShortDateString()))));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Quote valid until")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DATEOFVALIDITY == null ? "" : Convert.ToString(model.DATEOFVALIDITY.ToShortDateString()))));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Pickup City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.ORIGINPLACENAME == null ? "" : model.ORIGINPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold(Ploading)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.ORIGINPORTNAME == null ? "" : model.ORIGINPORTNAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Deliver City")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPLACENAME == null ? "" : model.DESTINATIONPLACENAME)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCellBold(Pdischarge)));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
            mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.DESTINATIONPORTNAME == null ? "" : model.DESTINATIONPORTNAME)));
            if (model.ISHAZARDOUSCARGO != 0)
            {
                mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("Hazardous Cargo")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell(model.HAZARDOUSGOODSTYPE == null ? "" : model.HAZARDOUSGOODSTYPE)));
            }
            else
            {
                mbookingTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell("")));
                mbookingTab.AddCell((mFormattingpdf.TableContentCell("")));
            }
            PdfPCell mRoutecell = new PdfPCell(mbookingTab);
            mRoutecell.Colspan = 3;
            mbookingbordertable.AddCell(mRoutecell);

            return mbookingbordertable;
        }
        public static PdfPTable cargoTableModel(JobBookingModel model)
        {
            PdfPTable mcargoTab = new PdfPTable(10);
            Formattingpdf mformatcargopdf = new Formattingpdf();
            PdfPTable mCargobordertable = new PdfPTable(1);
            mCargobordertable.WidthPercentage = 100;
            mCargobordertable.SpacingBefore = 10;
            mCargobordertable.DefaultCell.Border = 0;
            if (model.PRODUCTTYPEID == 6)
                mcargoTab = new PdfPTable(9);
            mCargobordertable.AddCell(new PdfPCell(mformatcargopdf.TableCaption("Cargo Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Package Type", 0, false)));
            if (model.PRODUCTTYPEID == 5)
                mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Container", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Quantity", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Unit", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Length", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Width", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Height", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("PPW", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("TW", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Unit", 0, false)));
            if (model.PRODUCTTYPEID == 5)
                mcargoTab.SetWidths(new float[] { 20, 10, 10, 5, 10, 10, 10, 5, 5, 5 });
            else if (model.PRODUCTTYPEID == 6)
                mcargoTab.SetWidths(new float[] { 20, 10, 10, 10, 10, 10, 10, 10, 10 });

            mcargoTab.WidthPercentage = 100;
            if (model.PRODUCTTYPEID == 5)
            {
                List<JobBookingShipmentModel> SortedList = model.JobShipmentItems.OrderBy(o => o.ContainerCode).ToList();

                foreach (var item in SortedList)
                {

                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.PackageTypeName == null ? "" : item.PackageTypeName)));
                    if (item.ContainerCode == "22G0")
                        item.ContainerCode = "20'GP";
                    else if (item.ContainerCode == "42G0")
                        item.ContainerCode = "40'GP";
                    else if (item.ContainerCode == "45G0")
                        item.ContainerCode = "45'HC";
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ContainerCode == null ? "" : Convert.ToString(item.ContainerCode))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.Quantity == 0 ? "" : Convert.ToString(item.Quantity))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.LengthUOMCode == null ? "" : item.LengthUOMCode)));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemLength == 0 ? "" : Convert.ToString(item.ItemLength))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemWidth == 0 ? "" : Convert.ToString(item.ItemWidth))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemHeight == 0 ? "" : Convert.ToString(item.ItemHeight))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightPerPiece == 0 ? "" : Convert.ToString(item.WeightPerPiece))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightTotal == 0 ? "" : Convert.ToString(item.WeightTotal))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightUOMName == null ? "" : item.WeightUOMName)));
                }

            }
            else if (model.PRODUCTTYPEID == 6)
            {
                foreach (var item in model.ShipmentItems)
                {
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.PackageTypeName == null ? "" : item.PackageTypeName)));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.Quantity == 0 ? "" : Convert.ToString(item.Quantity))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.LengthUOMCode == null ? "" : item.LengthUOMCode)));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemLength == 0 ? "" : Convert.ToString(item.ItemLength))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemWidth == 0 ? "" : Convert.ToString(item.ItemWidth))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ItemHeight == 0 ? "" : Convert.ToString(item.ItemHeight))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightPerPiece == 0 ? "" : Convert.ToString(item.WeightPerPiece))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightTotal == 0 ? "" : Convert.ToString(item.WeightTotal))));
                    mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.WeightUOMName == null ? "" : item.WeightUOMName)));

                }
            }
            PdfPCell mRoutecell = new PdfPCell(mcargoTab);
            mCargobordertable.AddCell(mRoutecell);
            return mCargobordertable;
        }
        public static PdfPTable containerTableModel(JobBookingModel model)
        {
            Formattingpdf mformatcargopdf = new Formattingpdf();
            PdfPTable mCargobordertable = new PdfPTable(1);
            mCargobordertable.WidthPercentage = 100;
            mCargobordertable.SpacingBefore = 10;
            mCargobordertable.DefaultCell.Border = 0;
            PdfPTable mcargoTab = new PdfPTable(3);
            mCargobordertable.AddCell(new PdfPCell(mformatcargopdf.TableCaption("Container Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Package Type", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Quantity", 0, false)));
            mcargoTab.AddCell(new PdfPCell(mformatcargopdf.TableHeading("Container", 0, false)));
            mcargoTab.SetWidths(new float[] { 40, 10, 60 });
            mcargoTab.WidthPercentage = 100;
            foreach (var item in model.ShipmentItems)
            {
                mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.PackageTypeName == null ? "" : item.PackageTypeName)));
                mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.Quantity == 0 ? "" : Convert.ToString(item.Quantity))));
                mcargoTab.AddCell((mformatcargopdf.TableContentCell(item.ContainerName == null ? "" : item.ContainerName.Trim())));
            }
            PdfPCell mRoutecell = new PdfPCell(mcargoTab);
            mCargobordertable.AddCell(mRoutecell);
            return mCargobordertable;
        }
        public static PdfPTable ShipperTableModel(JobBookingModel model)
        {
            if (model.PartyItems.Count > 0)
            {
                foreach (var item in model.PartyItems)
                {
                    if (item.PARTYTYPE == "91")
                    {
                        model.SHCLIENTNAME = item.CLIENTNAME;
                    }
                    else if (item.PARTYTYPE == "92")
                    {
                        model.CONCLIENTNAME = item.CLIENTNAME;
                    }
                }
            }

            if (model.JobItems.Count > 0)
            {
                foreach (var item in model.JobItems)
                {
                    model.JCARGOAVAILABLEFROM = item.CARGOAVAILABLEFROM;
                    model.JCARGODELIVERYBY = item.CARGODELIVERYBY;
                }
            }
            Formattingpdf mformatshippdf = new Formattingpdf();
            PdfPTable mshipperbordertable = new PdfPTable(1);
            mshipperbordertable.WidthPercentage = 100;
            mshipperbordertable.SpacingBefore = 10;
            mshipperbordertable.DefaultCell.Border = 0;
            PdfPTable mshipperTab = new PdfPTable(2);
            mshipperTab.SetWidths(new float[] { 25, 75 });
            mshipperTab.WidthPercentage = 100;
            mshipperbordertable.AddCell(new PdfPCell(mformatshippdf.TableCaption("Shipper&Consignee Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Shipper Name")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.SHCLIENTNAME == null ? "" : model.SHCLIENTNAME)));
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Consignee Name")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.SHCLIENTNAME == null ? "" : model.CONCLIENTNAME)));
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Expected Cargo Available Date")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.JCARGOAVAILABLEFROM == DateTime.MinValue ? "" : Convert.ToString(model.JCARGOAVAILABLEFROM.ToShortDateString()))));
            mshipperTab.AddCell((mformatshippdf.TableContentCellBold("Estimated Cargo Delivery Date")));
            mshipperTab.AddCell((mformatshippdf.TableContentCell(model.JCARGODELIVERYBY == DateTime.MinValue ? "" : Convert.ToString(model.JCARGODELIVERYBY.ToShortDateString()))));
            PdfPCell mRoutecell = new PdfPCell(mshipperTab);
            mRoutecell.Colspan = 3;
            mshipperbordertable.AddCell(mRoutecell);
            return mshipperbordertable;
        }
        public static PdfPTable PaymenTabletModel(PaymentModel model)
        {
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mshipperbordertable = new PdfPTable(1);
            mshipperbordertable.WidthPercentage = 100;
            mshipperbordertable.SpacingBefore = 10;
            mshipperbordertable.DefaultCell.Border = 0;
            PdfPTable mQuoteTab = new PdfPTable(2);
            mQuoteTab.SetWidths(new float[] { 25, 75 });
            mQuoteTab.WidthPercentage = 100;
            mshipperbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Benefeciary Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Acc. Holder Name")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.BeneficiaryName)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Account No")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.BankAccountNumber)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Swift Code")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.SwiftCode == null ? "" : model.EntityList.SwiftCode)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("IFSC/IBAN Code")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.IFSCCode)));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Bank/Branch Name")));
            mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.EntityList.BankName + "/" + model.EntityList.Branch)));
            PdfPCell mRoutecell = new PdfPCell(mQuoteTab);
            mshipperbordertable.AddCell(mRoutecell);
            return mshipperbordertable;
        }

        public static PdfPTable TransactionTabletModel(PaymentTransactionDetailsModel transaction)
        {
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mPaymentbordertable = new PdfPTable(1);
            mPaymentbordertable.WidthPercentage = 100;
            mPaymentbordertable.SpacingBefore = 10;
            mPaymentbordertable.DefaultCell.Border = 0;
            PdfPTable mpaymentTab = new PdfPTable(2);
            mpaymentTab.SetWidths(new float[] { 25, 75 });
            mpaymentTab.WidthPercentage = 100;
            mPaymentbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Transaction Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Bank")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBANKNAME)));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBRANCHNAME)));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Transaction Ref No.")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.PAYMENTREFNUMBER)));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Date")));
            mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(transaction.TRANSCHEQUEDATE.Value.ToShortDateString()))));
            if (!string.IsNullOrEmpty(transaction.TRANSDESCRIPTION))
            {
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Description")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSDESCRIPTION)));
            }
            PdfPCell mpRoutecell = new PdfPCell(mpaymentTab);
            mPaymentbordertable.AddCell(mpRoutecell);
            return mPaymentbordertable;
        }

        public PdfPTable payatbranchModel(PaymentTransactionDetailsModel transaction, string SITEADDRESS)
        {

            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPTable mPaymentbordertable = new PdfPTable(1);
            mPaymentbordertable.WidthPercentage = 100;
            mPaymentbordertable.SpacingBefore = 10;
            mPaymentbordertable.DefaultCell.Border = 0;
            PdfPTable mpaymentTab = new PdfPTable(2);
            mpaymentTab.SetWidths(new float[] { 25, 75 });
            mpaymentTab.WidthPercentage = 100;
            mPaymentbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Transaction Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
            if (transaction.TRANSMODE.ToLower() == "cash")
            {
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Transaction Mode")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSMODE)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Comments")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSDESCRIPTION)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Date")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(transaction.TRANSCHEQUEDATE.Value.ToShortDateString()))));
                if (!ReferenceEquals(SITEADDRESS, null))
                {
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch Address")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(SITEADDRESS))));
                }
            }
            else if (transaction.TRANSMODE.ToLower() == "cheque" || transaction.TRANSMODE.ToLower() == "demand draft")
            {
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Bank")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBANKNAME)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSBRANCHNAME)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Cheque/DD Number")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSCHEQUENO)));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Date")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(transaction.TRANSCHEQUEDATE.Value.ToShortDateString()))));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Comments")));
                mpaymentTab.AddCell((mFormattingpdf.TableContentCell(transaction.TRANSDESCRIPTION)));
                if (!ReferenceEquals(SITEADDRESS, null))
                {
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("Branch Address")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell(Convert.ToString(SITEADDRESS))));
                }
                else
                {
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mpaymentTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
            }
            PdfPCell mpRoutecell = new PdfPCell(mpaymentTab);
            mPaymentbordertable.AddCell(mpRoutecell);
            return mPaymentbordertable;
        }

        #endregion

        #region private methods

        public string CheckPaymentStatus(int jobnumber)
        {

            string result = string.Empty;
            var ds = _JFactory.CheckPaymentStatus(jobnumber);
            if (ds.Tables[0].Rows.Count > 0)
            {
                result = ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                result = "NOTEXIST";
            }
            return result;
        }

        private StripeData GetStripeData(string currency)
        {
            StripeData stripeData = new StripeData();
            switch (currency.ToLowerInvariant())
            {
                case "eur":
                case "gbp":
                    stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_NL"];
                    stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_NL"];
                    stripeData.account = "stripeNetherlands";
                    break;
                case "chf":
                    stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_CH"];
                    stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_CH"];
                    stripeData.account = "stripeSwiss";
                    break;
                case "sgd":
                    stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_SG"];
                    stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_SG"];
                    stripeData.account = "stripeSingapore";
                    break;
                default:
                    stripeData.secretKey = System.Configuration.ConfigurationManager.AppSettings["StripeSK_US"];
                    stripeData.publicKey = System.Configuration.ConfigurationManager.AppSettings["StripePK_US"];
                    stripeData.account = "stripeUSA";
                    break;
            }
            return stripeData;
        }

        private HttpResponseMessage StripeCharge(StripeChargeCreateOptions charge, PaymentModel mUIModel)
        {
            StripeData stripeData = GetStripeData(charge.Currency);
            var chargeService = new StripeChargeService(stripeData.secretKey)
            {
                ExpandBalanceTransaction = true
            };
            try
            {
                StripeCharge stripeCharge = chargeService.Create(charge);
                ThreadPool.QueueUserWorkItem(o =>
                {
                    if (stripeCharge.Status == "succeeded")
                    {
                        try
                        {
                            SafeRun<bool>(() =>
                            {
                                var Baltran = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                _JFactory.SaveStripeTransactionDetails(
                                    charge.Metadata["id"].ToString(),
                                    stripeCharge.Status,
                                    (Convert.ToDouble(stripeCharge.Amount) / 100).ToString(),
                                    charge.Currency,
                                    "Credit/Debit",
                                    charge.SourceTokenOrExistingSourceId,
                                    stripeCharge.Id,
                                    stripeCharge.BalanceTransactionId,
                                    stripeCharge.BalanceTransaction.Currency.ToUpperInvariant(),
                                    (Convert.ToDouble(stripeCharge.BalanceTransaction.Amount) / 100).ToString(),
                                    (Convert.ToDouble(stripeCharge.BalanceTransaction.Net) / 100).ToString(),
                                    (Convert.ToDouble(stripeCharge.BalanceTransaction.Fee) / 100).ToString(),
                                    stripeCharge.BalanceTransaction.Status,
                                    Baltran.balance_transaction.exchange_rate == null ? "1" : Baltran.balance_transaction.exchange_rate,
                                    stripeData.account
                                );
                                return true;
                            });
                            string brand = string.Empty;
                            string last4 = string.Empty;
                            try
                            {
                                StripeCardModel carddata = FetchCardData(stripeCharge);
                                brand = carddata.Brand;
                                last4 = carddata.Last4;
                            }
                            catch (Exception ex)
                            {
                                LogError("api/Payment", "StripeCharge", ex.Message.ToString(), ex.StackTrace.ToString());
                            }
                            var data = new StripeSaveModel()
                            {
                                StripeSourceId = charge.SourceTokenOrExistingSourceId,
                                ReceiptHeaderId = mUIModel.RECEIPTHDRID.ToString(),
                                JobNumber = Convert.ToInt32(mUIModel.JobNumber),
                                QuotationId = mUIModel.QuotationId.ToString(),
                                CardType = brand,
                                CardNumber = last4,
                                ResultStatus = "approved",
                                CouponCode = charge.Metadata["discountcode"].ToString(),
                                UserId = charge.Metadata["email"].ToString(),
                                Reason = "Success",
                                PayAmount = (Convert.ToDouble(stripeCharge.BalanceTransaction.Amount))
                            };
                            SavePayment(data);
                        }
                        catch (Exception ex)
                        {
                            LogError("api/Payment", "StripeCharge", ex.Message.ToString(), ex.StackTrace.ToString());
                            SendEmailNotificationForExceptions("api/payment/StripePayment", ex.Message.ToString(), ex.StackTrace.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            SafeRun<bool>(() =>
                            {
                                var Baltran = JsonConvert.DeserializeObject<StripeChargeResponseModel>(stripeCharge.StripeResponse.ResponseJson);
                                _JFactory.SaveStripeTransactionDetails(
                                    charge.Metadata["id"].ToString(),
                                    stripeCharge.Status,
                                    (Convert.ToDouble(stripeCharge.Amount) / 100).ToString(),
                                    charge.Currency,
                                    "Credit/Debit",
                                    charge.SourceTokenOrExistingSourceId,
                                    stripeCharge.Id,
                                    stripeCharge.FailureMessage,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    Baltran.balance_transaction.exchange_rate == null ? "1" : Baltran.balance_transaction.exchange_rate,
                                    stripeData.account
                                );
                                return true;
                            });
                            string brand = string.Empty;
                            string last4 = string.Empty;
                            try
                            {
                                StripeCardModel carddata = FetchCardData(stripeCharge);
                                brand = carddata.Brand;
                                last4 = carddata.Last4;
                            }
                            catch (Exception ex)
                            {
                                LogError("api/Payment", "StripeCharge", ex.Message.ToString(), ex.StackTrace.ToString());
                            }
                            var data = new StripeSaveModel()
                            {
                                StripeSourceId = charge.SourceTokenOrExistingSourceId,
                                ReceiptHeaderId = mUIModel.RECEIPTHDRID.ToString(),
                                JobNumber = Convert.ToInt32(mUIModel.JobNumber),
                                QuotationId = mUIModel.QuotationId.ToString(),
                                CardType = brand,
                                CardNumber = last4,
                                ResultStatus = "failed",
                                CouponCode = charge.Metadata["discountcode"].ToString(),
                                UserId = charge.Metadata["email"].ToString(),
                                Reason = stripeCharge.FailureMessage,
                                PayAmount = (Convert.ToDouble(stripeCharge.BalanceTransaction.Amount))
                            };
                            SavePayment(data);
                        }
                        catch (Exception ex)
                        {
                            LogError("api/Payment", "StripeCharge", ex.Message.ToString(), ex.StackTrace.ToString());
                        }
                    }
                });
                var message = string.Empty;
                if (stripeCharge.Status == "succeeded")
                {
                    message = "Thank you for your payment, your shipment is confirmed. A confirmation email has been sent to " + charge.ReceiptEmail;
                }
                else
                {
                    message = "Something went wrong";
                }
                var response = new
                {
                    Status = stripeCharge.Status,
                    FailureMessage = stripeCharge.FailureMessage,
                    Message = message
                };
                return BaseResponse(response, HttpStatusCode.OK);
            }
            catch (Exception stpEx)
            {
                SafeRun<bool>(() =>
                {
                    _JFactory.SaveStripeTransactionDetails(
                       charge.Metadata["id"].ToString(),
                       "failed",
                       Convert.ToDecimal(charge.Amount / 100).ToString(),
                       charge.Currency,
                       "Credit/Debit",
                       charge.SourceTokenOrExistingSourceId,
                       "",
                       stpEx.Message.ToString(),
                       "",
                       "",
                       "",
                       "",
                       "",
                       "",
                       stripeData.account
                    );
                    return true;
                });
                LogError("api/Payment", "StripeCharge", stpEx.Message.ToString(), stpEx.StackTrace.ToString());
                return ErrorResponse(stpEx.Message.ToString());
            }
        }

        private HttpResponseMessage OnlineMainPayment(StripeModel request)
        {
            try
            {
                double PayAmount = 0;
                DBFactory.Entities.PaymentEntity mDBEntity = _JFactory.GetPaymentDetailsByJobNumber(request.JobNumber, request.UserId, request.CouponCode, request.RequestedAmount);
                PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                if (mUIModel.NOTSUPPAMOUNTUSD != 0)
                {
                    PayAmount = Convert.ToDouble(mUIModel.NOTSUPPAMOUNTUSD) * 100;
                    request.Currency = "USD";
                }
                else
                {
                    PayAmount = Convert.ToDouble(mUIModel.ReceiptNetAmount) * 100;
                }
                var dic = new Dictionary<string, string>();
                dic.Add("id", request.JobNumber.ToString());
                dic.Add("email", request.UserId);
                dic.Add("discountcode", request.CouponCode);
                var myCharge = new StripeChargeCreateOptions()
                {
                    Amount = Convert.ToInt32(PayAmount),
                    Currency = request.Currency,
                    Description = request.Description,
                    SourceTokenOrExistingSourceId = request.StripeSourceId,
                    Metadata = dic,
                    ReceiptEmail = request.UserId
                };
                return StripeCharge(myCharge, mUIModel);
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "OnlineMainPayment", ex.Message.ToString(), ex.StackTrace.ToString());
                return ErrorResponse(ex.Message.ToString());
            }
        }

        private HttpResponseMessage OnlineAdditionalPayment(StripeModel request)
        {
            try
            {
                double PayAmount = 0;
                DBFactory.Entities.PaymentEntity mDBEntity = _JFactory.GetPaymentDetailsByJobNumber(request.JobNumber, request.UserId);
                PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
                if (mUIModel.ADDITIONALCHARGESTATUS != null && mUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
                {
                    if (mUIModel.NOTSUPPAMOUNTUSD != 0)
                    {
                        PayAmount = Convert.ToDouble(mUIModel.ADDITIONALCHARGEAMOUNTINUSD) * 100;
                        request.Currency = "USD";
                    }
                    else
                    {
                        PayAmount = Convert.ToDouble(mUIModel.ADDITIONALCHARGEAMOUNT) * 100;
                    }
                    var dic = new Dictionary<string, string>();
                    dic.Add("id", request.JobNumber.ToString());
                    dic.Add("email", request.UserId);
                    dic.Add("discountcode", request.CouponCode);
                    dic.Add("additional", "true");

                    var myCharge = new StripeChargeCreateOptions()
                    {
                        Amount = Convert.ToInt32(PayAmount),
                        Currency = request.Currency,
                        Description = request.Description,
                        SourceTokenOrExistingSourceId = request.StripeSourceId,
                        Metadata = dic,
                        ReceiptEmail = request.UserId
                    };

                    return StripeCharge(myCharge, mUIModel);
                }
                else
                {
                    return ErrorResponse("Payment done already");
                }
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "OnlineAdditionalPayment", ex.Message.ToString(), ex.StackTrace.ToString());
                return ErrorResponse(ex.Message.ToString());
            }
        }

        private void SavePayment(StripeSaveModel data)
        {
            DBFactory.Entities.PaymentEntity mDBEntity = _JFactory.GetPaymentDetailsByJobNumber(Convert.ToInt32(data.JobNumber), data.UserId, data.CouponCode, data.RequestedAmount);
            PaymentModel mUIModel = Mapper.Map<DBFactory.Entities.PaymentEntity, PaymentModel>(mDBEntity);
            data.AdditionalChargeStatus = (mUIModel.ADDITIONALCHARGESTATUS != null && mUIModel.ADDITIONALCHARGESTATUS.ToLower() == "pay now") ? mUIModel.ADDITIONALCHARGESTATUS : "";
            data.AdditionalChargeAmount = (mUIModel.ADDITIONALCHARGEAMOUNT > 0) ? mUIModel.ADDITIONALCHARGEAMOUNT : 0;
            decimal AdditionalChargeAmountINUSD = (mUIModel.ADDITIONALCHARGEAMOUNTINUSD > 0) ? mUIModel.ADDITIONALCHARGEAMOUNTINUSD : 0;

            try
            {
                TransResult(data.StripeSourceId, data.ReceiptHeaderId, data.JobNumber.ToString(), data.QuotationId, data.ResultStatus, "Online",
                    data.CouponCode, mUIModel.DiscountAmount, data.UserId, data.PayAmount, data.AdditionalChargeStatus, data.AdditionalChargeAmount,
                    AdditionalChargeAmountINUSD, data.Reason);
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "SavePayment", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException("Something Went Wrong.");
            }

            CheckIsMandatoryDocsUploaded(data.JobNumber.ToString(), data.UserId, Convert.ToInt32(data.QuotationId), mDBEntity.OPJOBNUMBER);

            DBFactory.Entities.QuotationEntity quotationentity = _QFactory.GetById(Convert.ToInt32(data.QuotationId));

            data.mUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(quotationentity);

            UserEntity UE = _UFactory.GetUserFirstAndLastName(data.UserId);
            data.FirstName = UE.FIRSTNAME;
            data.CRMEmail = UE.CRMEmail;
            if (data.ResultStatus.ToLower() == "approved")
            {
                // generate Pdf's
                SafeRun<bool>(() =>
                {
                    var mUserModel = _UFactory.GetUserProfileDetails(data.UserId, 1);

                    if (mUIModel.CHARGESETID > 0)
                    {
                        //For Additional Charges, will not generate Booking Confirmation.
                        GenerateAdvanceReceiptPDF(
                            Convert.ToInt32(data.JobNumber),
                            "Advance Receipt",
                            Convert.ToInt64(data.QuotationId),
                            mUIModel.OPjobnumber,
                            data.UserId,
                            Convert.ToInt32(mUIModel.CHARGESETID),
                            mUserModel,
                            data.mUIModels
                        );
                    }
                    else
                    {
                        GenerateBookingConfirmationPDF(
                            Convert.ToInt32(data.JobNumber),
                            "Booking Confirmation",
                            Convert.ToInt64(data.QuotationId),
                            mUIModel.OPjobnumber,
                            data.UserId,
                            mUserModel,
                            data.mUIModels
                         );

                        GenerateAdvanceReceiptPDF(
                            Convert.ToInt32(data.JobNumber),
                            "Advance Receipt",
                            Convert.ToInt64(data.QuotationId),
                            mUIModel.OPjobnumber,
                            data.UserId,
                            0,
                            mUserModel,
                            data.mUIModels
                        );
                    }
                    return true;
                });

                // Notify User
                OnlinePaymentSuccessNotifyUser(mUIModel, data, UE.COUNTRYNAME);
            }
            else
            {
                // Result status failed.
                OnlinePaymentFailedNotifyUser(mUIModel, data);
            }
        }

        private bool TransResult(string refNumber, string receiptHeaderId, string jobNumber, string quotationId,
            string state, string paymentOption, string CouponCode, decimal DiscountAmount, string user, double PayAmount,
            string AdditionalChargeStatus = "", decimal AdditionalChargeAmount = 0, decimal AdditionalChargeAmountINUSD = 0,
            string reason = "", string transmode = "", string paymenttype = "", string paidcurrency = "")
        {
            PaymentTransactionDetailsModel model = new PaymentTransactionDetailsModel();
            try
            {
                model.RECEIPTHDRID = Convert.ToInt32(receiptHeaderId);
                model.JOBNUMBER = Convert.ToInt32(jobNumber);
                model.QUOTATIONID = Convert.ToInt32(quotationId);
                model.PAYMENTOPTION = paymentOption;
                model.ISPAYMENTSUCESS = ((state == "approved") ? 1 : 0);
                model.PAYMENTREFNUMBER = refNumber;
                model.PAYMENTREFMESSAGE = state;
                model.CREATEDBY = user;
                model.MODIFIEDBY = user;
                model.TRANSMODE = transmode == "" ? "Card" : transmode;
                model.PAYMENTTYPE = paymenttype == "" ? "Credit/Debit" : paymenttype;
                model.ADDITIONALCHARGEAMOUNT = AdditionalChargeAmount;
                model.ADDITIONALCHARGEAMOUNTINUSD = AdditionalChargeAmountINUSD;
                model.ADDITIONALCHARGESTATUS = (AdditionalChargeStatus != null && AdditionalChargeStatus.ToLower() == "pay now") ? (model.ISPAYMENTSUCESS == 1) ? "Paid" : "Rejected" : "";

                DBFactory.Entities.PaymentTransactionDetailsEntity mUIModel = Mapper.Map<PaymentTransactionDetailsModel, DBFactory.Entities.PaymentTransactionDetailsEntity>(model);
                _JFactory.SavePaymentTransactionDetails(mUIModel, CouponCode, DiscountAmount, PayAmount, reason, paidcurrency);
                return true;
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "TransResult", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException("Something Went Wrong.");
            }
        }

        private void OnlinePaymentFailedNotifyUser(PaymentModel mUIModel, StripeSaveModel data)
        {
            #region Mail to User
            SafeRun<string>(() =>
            {
                string amount = string.Empty;
                string flowType = string.Empty;
                if (data.AdditionalChargeStatus == string.Empty)
                {
                    amount = Convert.ToString(mUIModel.ReceiptNetAmount); flowType = "Payment";
                }
                else
                {
                    amount = Convert.ToString(data.AdditionalChargeAmount); flowType = "Additional Charge Payment";
                }
                try
                {
                    SendMailWithCard(data.UserId, data.StripeSourceId, "Online", amount, data.CardNumber, data.CardType,
                        mUIModel.Currency, "Payment Failed", mUIModel.OPjobnumber, mUIModel.JobNumber,
                        mUIModel.QuotationId, data.mUIModels.ProductName, data.mUIModels.MovementTypeName,
                        mUIModel.ShipperName, mUIModel.ConsigneeName, flowType, data.FirstName, "Online", data.Reason);
                }
                catch (Exception ex)
                {
                    LogError("api/Payment", "OnlinePaymentFailedNotifyUser", ex.Message.ToString(), ex.StackTrace.ToString());
                    throw new ShipaApiException("Exception while sending Payment Failed mail Online " + "Something Went Wrong.");
                }
                return string.Empty;
            });
            #endregion Mail to User

            #region push to mobile User

            SafeRun<bool>(() =>
            {
                _UFactory.NewNotification(
                    5113,
                    "Payment failed",
                    mUIModel.OPjobnumber,
                    Convert.ToInt32(mUIModel.JobNumber),
                    Convert.ToInt32(mUIModel.QuotationId),
                    mUIModel.QuotationNumber,
                    data.UserId
                );

                var pushdata = new
                {
                    NOTIFICATIONTYPEID = 5113,
                    NOTIFICATIONCODE = "Payment failed",
                    JOBNUMBER = mUIModel.JobNumber,
                    BOOKINGID = mUIModel.OPjobnumber,
                    QUOTATIONID = mUIModel.QuotationId,
                    USERID = data.UserId
                };

                return _OneSignal.SendPushMessageByTag("Email", data.UserId.ToLowerInvariant(), "Payment failed", pushdata);
            });

            #endregion Mail to User
        }

        private void OnlinePaymentSuccessNotifyUser(PaymentModel mUIModel, StripeSaveModel data,string Country)
        {
            #region Mail to User
            SafeRun<bool>(() =>
            {
                if (data.AdditionalChargeStatus == null || data.AdditionalChargeStatus == "")
                {

                    SendMailWithCard(data.UserId, data.StripeSourceId, "", Double.Parse(mUIModel.ReceiptNetAmount.ToString()).ToString(),
                        data.CardNumber, data.CardType, mUIModel.Currency, "Job Booked", mUIModel.OPjobnumber, mUIModel.JobNumber,
                        mUIModel.QuotationId, data.mUIModels.ProductName, data.mUIModels.MovementTypeName, mUIModel.ShipperName,
                        mUIModel.ConsigneeName, "Payment", data.FirstName, "Online", data.Reason);
                }
                else
                {
                    SendMailWithCard(data.UserId, data.StripeSourceId, "", Convert.ToString(data.AdditionalChargeAmount), data.CardNumber, data.CardType,
                        mUIModel.Currency, "Job Booked", mUIModel.OPjobnumber, mUIModel.JobNumber, mUIModel.QuotationId,
                        data.mUIModels.ProductName, data.mUIModels.MovementTypeName, mUIModel.ShipperName, mUIModel.ConsigneeName,
                        "Additional Charge Payment", data.FirstName, "Online", data.Reason);
                }
                return true;
            });
            #endregion Mail to User

            #region Mail to Focis users
            SafeRun<bool>(() =>
            {
                string roles = string.Empty;
                string amount = string.Empty;
                string flowType = string.Empty;
                // Exclude GSSC while additional charges applied.
                if (data.AdditionalChargeStatus == null || data.AdditionalChargeStatus == "")
                {
                    roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC"; amount = Convert.ToString(mUIModel.ReceiptNetAmount);
                    flowType = "Payment";
                }
                else { roles = "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE"; amount = Convert.ToString(data.AdditionalChargeAmount); flowType = "Additional Charge Payment"; }

                SendMailtoGroup(mUIModel.OPjobnumber, roles, "Paymentstatus", Convert.ToInt32(mUIModel.JobNumber), data.UserId, "Online", flowType, amount,
                    data.CardNumber, data.CardType, mUIModel.Currency, "Job Booked", data.mUIModels.ProductName, data.mUIModels.MovementTypeName,
                    mUIModel.ShipperName,mUIModel.ConsigneeName,Country, data.mUIModels,mUIModel);

                SendThankingMailtoCustomer(data.UserId, mUIModel.OPjobnumber, data.FirstName);

                return true;
            });
            #endregion Mail to Focis users

            #region push message to mobile users
            SafeRun<bool>(() =>
            {
                _UFactory.NewNotification(
                   5113,
                   "Payment Completed",
                   mUIModel.OPjobnumber,
                   Convert.ToInt32(mUIModel.JobNumber),
                   Convert.ToInt32(mUIModel.QuotationId),
                   mUIModel.QuotationNumber,
                   data.UserId
                );
                var pushdata = new
                {
                    NOTIFICATIONTYPEID = 5113,
                    NOTIFICATIONCODE = "Payment Completed",
                    JOBNUMBER = mUIModel.JobNumber,
                    BOOKINGID = mUIModel.OPjobnumber,
                    QUOTATIONID = mUIModel.QuotationId,
                    USERID = data.UserId
                };
                _OneSignal.SendPushMessageByTag("Email", data.UserId.ToLowerInvariant(), "Payment Completed", pushdata);
                return true;
            });
            #endregion push message to mobile users

        }

        private void SendMailForMandatoryDocs(string Email, string jobNumber, long quotationid, string BookingID)
        {
            string resetLink = string.Empty;
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = "Mandatory documents are needed before goods availability to Agility" + EnvironmentName;
            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/MandatoryDocs.html");
            string body = string.Empty;
            string url = "JobNumber=" + jobNumber + "?id=" + quotationid;
            string reqdocument = EncryptId(url);
            resetLink = Actionlink + "JobBooking/BookingSummary?" + reqdocument;
            UserEntity UE = _UFactory.GetUserFirstAndLastName(Email);
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{Message}", "<p>You have not uploaded the mandatory documents needed to process your shipment.");
            body = body.Replace("{Message1}", "<p>You need to upload these before the pickup date, or this might result in delay of pickup and the shipment.");
            body = body.Replace("{user}", UE.FIRSTNAME);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{btnName}", "Upload document Now");
            body = body.Replace("{documentreq}", resetLink);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
            "JobBooking", "Mandatory Documents", BookingID, Email, body, "", true, message.Subject.ToString());
        }

        private void CheckIsMandatoryDocsUploaded(string jobNumber, string user, long quotationid, string BookigID)
        {
            SafeRun<bool>(() =>
            {
                int Count = _JFactory.CheckIsMandatoryDocsUploaded(jobNumber);
                if (Count > 0)
                    SendMailForMandatoryDocs(user, jobNumber, quotationid, BookigID);
                return true;
            });
        }

        private void SendMailWithCard(string Email, string refNumber, string paymentType, string Total, string cardno, string cardtype,
            string currency, string status, string BookingID, long Jobnumber, long QuotationID,
            string mode, string movementType, string shipperName, string consigneeName, string flowType, string FirstName = "",
            string paymentMethod = "", string reason = "")
        {
            string AttachmentIds = string.Empty;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            QuotationDBFactory mFactoryRound = new QuotationDBFactory();
            DataSet documents = mFactory.GetDocuments(Convert.ToInt32(Jobnumber));

            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress(Email));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();

            if (paymentMethod.ToUpperInvariant() == "ONLINE" || paymentMethod.ToUpperInvariant() == "OFFLINE" || paymentType.ToUpperInvariant() == "OFFLINE")
            {
                message.Subject = "Shipa Freight- " + flowType + "( Booking ID #" + BookingID + " )" + EnvironmentName;
            }
            else if (paymentMethod.ToUpperInvariant() == "BUSINESS CREDIT")
            {
                message.Subject = "Shipa Freight - " + flowType + " through Business credit" + "( Booking ID #" + BookingID + " )" + EnvironmentName;
            }
            else
            {
                message.Subject = "Shipa Freight - " + flowType + " in progress." + "( Booking ID #" + BookingID + " )" + EnvironmentName;
            }
            string mapPath = string.Empty;
            if (status.ToUpperInvariant() == "JOB BOOKED")
            {
                if (paymentType.ToUpperInvariant() == "WIRE TRANSFER" && paymentMethod.ToUpperInvariant() == "OFFLINE")
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/payment-wiretransfer.html");
                }
                else
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/payment-success.html");

                }
            }
            else
            {
                mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Payment-fail.html");
            }
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }

            if (FirstName == "")
            {
                UserDBFactory UD = new UserDBFactory();
                UserEntity UE = UD.GetUserFirstAndLastName(Email);
                body = body.Replace("{user_name}", UE.FIRSTNAME);
            }
            else
            {
                body = body.Replace("{user_name}", FirstName);
            }
            body = body.Replace("{currency}", currency);
            body = body.Replace("{Amount}", mFactoryRound.RoundedValue(Total, currency));
            if (paymentMethod.ToUpperInvariant() == "ONLINE")
            {
                if (cardtype.ToLowerInvariant() != "alipay")
                {
                    body = body.Replace("{PaymentMethod}", cardtype + "<br><span style=text-transform: lowercase>xxxxxxxxxxxx" + cardno + "<span>");
                }
                else
                {
                    body = body.Replace("{PaymentMethod}", cardtype);
                }
            }
            else if (paymentMethod.ToUpperInvariant() == "OFFLINE")
            {
                body = body.Replace("{PaymentMethod}", paymentType);
            }
            else
            {
                body = body.Replace("{PaymentMethod}", paymentMethod);
            }
            body = body.Replace("{Amount}", mFactoryRound.RoundedValue(Total, currency));
            body = body.Replace("{Status}", status);
            body = body.Replace("{BookingID}", BookingID);
            body = body.Replace("{Mode}", mode);
            body = body.Replace("{MovementType}", movementType);
            body = body.Replace("{ShipperOrConsignee}", shipperName + " / " + consigneeName);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            string Tbody = "";
            if (movementType.ToUpper() == "DOOR TO PORT" || movementType.ToUpper() == "PORT TO PORT")
            {
                Tbody = PrepareShipmentTrack("P");
                body = body.Replace("{shpnttracking}", Tbody);
            }
            else
            {
                Tbody = PrepareShipmentTrack("D");
                body = body.Replace("{shpnttracking}", Tbody);
            }


            if (status.ToUpperInvariant() != "JOB BOOKED" && paymentMethod.ToUpperInvariant() == "ONLINE")
            {
                body = body.Replace("{Reason}", "(" + reason + ")");
            }
            else
            {
                body = body.Replace("{Reason}", "");
            }

            body = body.Replace("{Trackingurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "/Tracking?SearchString=" + BookingID + "&status=ACTIVE");

            if (documents != null)
            {
                foreach (DataRow dr in documents.Tables[0].Rows)
                {
                    string fileName = dr["FILENAME"].ToString();
                    byte[] fileContent = (byte[])dr["FILECONTENT"];
                    message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                    if (AttachmentIds == "")
                        AttachmentIds = dr["DOCID"].ToString();
                    else
                        AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                }
            }
            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", Email, "", "",
            "JobBooking", "Credit/Debit/Payment Confirmation", BookingID, Email, body, AttachmentIds, true, message.Subject.ToString());
        }

        private string PrepareShipmentTrack(string p)
        {
            string Tbody = "";
            if (p == "D")
            {
                Tbody = @"<table width='600px'><tbody><tr><td style='color: #333;font-size: 18px; font-weight: normal; padding-bottom: 5px'>Shipment Tracking</td></tr>
                                <tr><td style='color: #333;background-color: #FFFFFF; font-size: 16px; padding: 10px 10px 10px 10px; border: 1px solid #DDDDDD;'><table style='width: 100%; text-align: center; color: #333333; font-size: 12px;'>
                    <tbody>
                    <tr>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/job-b.png'></td>
                        <td width='16%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/receipt-b.png'></td>
                        <td width='16%'><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/departed-b.png'></td>
                        <td width='15%'><img style='width: 100%; height: 4px;'  src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/arrival-b.png'></td>
                        <td width='15%'><img style='width: 100%; height: 4px;'  src='https://apps.agility.com/AgilityDelivers/AGFX/border.png'></td>
                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/delivered-b.png'></td>
                    </tr>
                    <tr>
                        <td style='vertical-align: top;'>
                            Job Confirmed
                        </td>
                        <td width='16%'></td>
                        <td style='vertical-align: top;'>
                            Receipt of Goods
                        </td>
                        <td width='17%'></td>
                        <td style='vertical-align: top;'>
                            Departed
                        </td>
                        <td width='16%'></td>
                        <td style='vertical-align: top;'>
                            Arrival
                        </td>
                        <td width='15%'></td>
                        <td style='vertical-align: top;'>
                            Delivered
                        </td>
                    </tr>
                    </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>";
            }
            else
            {
                Tbody = @"<table width='600' style='background-color: #f0f0f0; width: 600px; margin: auto; padding: 10px 20px;'>
                    <tbody>
                        <tr>
                            <td style='color: #333;font-size: 18px; font-weight: normal; padding-bottom: 5px'>
                                Shipment Tracking
                            </td>
                        </tr>
                        <tr>
                            <td style='color: #333;background-color: #FFFFFF; font-size: 16px; padding: 10px 10px 10px 10px; border: 1px solid #DDDDDD;'>
                                <table style=' width: 100%; text-align: center; color: #333333; font-size: 12px;'>
                                    <tbody>
                                    <tr>
                                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/location-1.png'></td>
                                        <td><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>
                            
                                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/receipt-b.png'></td>
                                        <td><img style='width: 100%; height: 4px;' src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>
                                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/departed-b.png'></td>
                                        <td><img style='width: 100%; height: 3px;'  src='https://apps.agility.com/AgilityDelivers/AGFX/border-big.png'></td>
                                        <td><img src='https://apps.agility.com/AgilityDelivers/AGFX/arrival-b.png'></td>
                                    </tr>
                                    <tr>
                                        <td style='vertical-align: top;'>
                                            Job Confirmed
                                        </td>
                                        <td></td>
                                        <td style='vertical-align: top;'>
                                            Receipt of Goods
                                        </td>
                                        <td></td>
                                        <td style='vertical-align: top;'>
                                            Departure
                                        </td>
                                        <td></td>
                                        <td style='vertical-align: top;'>
                                            Arrival
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>";
            }
            return Tbody;
        }

        private void SendMailtoGroup(string BookingId, string roles, string partyType, int JobNumber, string user, string Paymentmethod, string flowType, string amount = "", string cardno = "", string cardtype = "", string currency = "", string status = "", string mode = "", string movementType = "", string shipperName = "", string consigneeName = "",string Country="", QuotationPreviewModel qModel=null, PaymentModel mUIModel=null)
        {
            string AttachmentIds = string.Empty;
            string recipientsList = "";
            try
            {
                recipientsList = _JFactory.GetEmailList(roles, BookingId, partyType);
                DataSet documents = _JFactory.GetDocuments(JobNumber);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("noreply@shipafreight.com");

                if (string.IsNullOrEmpty(recipientsList)) { message.To.Add(new MailAddress("vsivaram@agility.com")); }
                else
                {
                    string[] bccid = recipientsList.Split(',');
                    foreach (string bccEmailId in bccid)
                    {
                        message.To.Add(new MailAddress(bccEmailId));
                    }
                }
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight - " + flowType + " Process" + "( Booking ID #" + BookingId + " )" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                if (roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC" || roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE" && Paymentmethod != "Additional Charges")
                {
                    string DevEmailids = System.Configuration.ConfigurationManager.AppSettings["GlobalPaymentNotifier"];
                    string[] ids = DevEmailids.Split(',');
                    foreach (var item in ids)
                    {
                        message.Bcc.Add(new MailAddress(item));
                    }
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/PaymentProcess-Operator-Finance-GSSC.html");
                }
                else
                {
                    mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/GSSC-PaymentProcess.html");
                }

                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }
                body = body.Replace("{Reference_No}", BookingId);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                if (roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE,SSPGSSC" || roles == "SSPCOUNTRYJOBOPS,SSPCOUNTRYFINANCE")
                {
                    body = body.Replace("{currency}", currency);
                    body = body.Replace("{Amount}", _QFactory.RoundedValue(amount, currency));

                    if (Paymentmethod == "Online")
                    {
                        if (cardtype.ToLowerInvariant() != "alipay")
                        {
                            body = body.Replace("{PaymentProcess}", cardtype + "<br><span style=text-transform: lowercase>xxxxxxxxxxxx" + cardno + "<span>");
                        }
                        else
                        {
                            body = body.Replace("{PaymentProcess}", cardtype);
                        }
                    }
                    else
                    {
                        body = body.Replace("{PaymentProcess}", Paymentmethod);
                        body = body.Replace("{PaymentType}", Paymentmethod);
                    }
                    body = body.Replace("{Status}", status);
                    body = body.Replace("{Mode}", mode);
                    body = body.Replace("{MovementType}", movementType);
                    body = body.Replace("{ShipperOrConsignee}", shipperName + " / " + consigneeName);
                    body = body.Replace("{OriginCountry}", qModel.OCOUNTRYNAME);
                    body = body.Replace("{DestinationCountry}", qModel.DCOUNTRYNAME);
                    body = body.Replace("{PayingCountry}", Country);
                    string placeportconent = string.Empty;
                    string specilaHandlingRequirements = string.Empty;
                    placeportconent = PrapareCityPortData(qModel);
                    body = body.Replace("{placeportconent}", placeportconent);
                    specilaHandlingRequirements = PrepareSpecialHandlingInstructions(mUIModel);
                    body = body.Replace("{specilaHandlingRequirements}", specilaHandlingRequirements);
                }
                if (documents != null)
                {
                    foreach (DataRow dr in documents.Tables[0].Rows)
                    {
                        string fileName = dr["FILENAME"].ToString();
                        byte[] fileContent = (byte[])dr["FILECONTENT"];
                        message.Attachments.Add(new Attachment(new MemoryStream(fileContent), fileName));
                        if (AttachmentIds == "")
                            AttachmentIds = dr["DOCID"].ToString();
                        else
                            AttachmentIds = AttachmentIds + "," + dr["DOCID"].ToString();
                    }
                }
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", recipientsList, "", "",
                    "JobBooking", "Payment Process", BookingId, user, body, AttachmentIds, false, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "SendMailtoGroup", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException("Something Went Wrong.");
            }
        }

        public string PrapareCityPortData(QuotationPreviewModel qModel)
        {
            string placeportconent = string.Empty;
            if (qModel.OriginPlaceName != null && qModel.DestinationPlaceName != null)
            {
                placeportconent = placeportconent + "<tr style=\"vertical-align: top;\"><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Origin City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.OriginPlaceName + "</div></td><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Destination City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.DestinationPlaceName + "</div></td>";
            }
            else
            {
                if (qModel.OriginPlaceName != null)
                {
                    placeportconent = placeportconent + "<tr style=\"vertical-align: top;\"><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Origin City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.OriginPlaceName + "</div></td>";
                }
                if (qModel.DestinationPlaceName != null)
                {
                    placeportconent = placeportconent + "<tr><td> <div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.OriginPlaceName + "</div></td><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Destination City" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.DestinationPlaceName + "</div></td>";
                }
            }
            if (qModel.OriginPortName != null && qModel.DestinationPortName != null)
            {
                placeportconent = placeportconent + "<tr style=\"vertical-align: top;\"><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Origin Port" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.OriginPortName + "</div></td><td style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Destination Port" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + qModel.DestinationPortName + "</div></td>";
            }
            return placeportconent;
        }

        public string PrepareSpecialHandlingInstructions(PaymentModel pModel)
        {
            string specilaHandlingRequirements = string.Empty;
            if (pModel.LIVEUPLOADS > 0)
            {
                specilaHandlingRequirements = "Live load/unload";
            }
            if (pModel.LOADINGDOCKAVAILABLE > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Lift gate required";
            }
            if (pModel.CARGOPALLETIZED > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Non-palletized cargo";
            }
            if (pModel.ORIGINALDOCUMENTSREQUIRED > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Original documents required";
            }
            if (pModel.TEMPERATURECONTROLREQUIRED > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Temperature control required";
            }
            if (pModel.COMMERCIALPICKUPLOCATION > 0)
            {
                specilaHandlingRequirements = specilaHandlingRequirements + ", Residential location(s)";
            }
            if (string.IsNullOrEmpty(specilaHandlingRequirements))
            {
                specilaHandlingRequirements = (string.IsNullOrEmpty(pModel.SPECIALINSTRUCTIONS) ? specilaHandlingRequirements : pModel.SPECIALINSTRUCTIONS);
            }
            else
            {
                specilaHandlingRequirements = (string.IsNullOrEmpty(pModel.SPECIALINSTRUCTIONS) ? specilaHandlingRequirements : specilaHandlingRequirements + ", " + pModel.SPECIALINSTRUCTIONS);
            }
            if (!string.IsNullOrEmpty(specilaHandlingRequirements))
            {
                specilaHandlingRequirements = specilaHandlingRequirements.TrimStart(',');
                specilaHandlingRequirements = "<tr style=\"vertical-align: top;\"><td colspan=2 style=\"font-family: 'Open Sans', sans-serif; text-transform: uppercase; padding: 00px 10px; color: #abadad; font-family: 'Open Sans', sans-serif; text-transform: capitalize\">" + "Special handling requirements" + "<div style=\"color: #333; font-family: 'Open Sans', sans-serif; font-size: 16px;\">" + specilaHandlingRequirements + "</div></td>";
            }
            return specilaHandlingRequirements;
        }
        private void SendThankingMailtoCustomer(string toEmail, string bookingId, string firstName)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress("cet@shipafreight.com"); //CE 
                message.To.Add(toEmail);
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                message.Subject = "Shipa Freight_Thanks For Payment" + EnvironmentName;
                message.IsBodyHtml = true;
                string body = string.Empty;
                string mapPath = string.Empty;
                mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Customer-Thanking.html");
                using (StreamReader reader = new StreamReader(mapPath))
                { body = reader.ReadToEnd(); }

                body = body.Replace("{user_name}", firstName);
                body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                message.Body = body;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                client.Dispose();
                message.Dispose();
                APIDynamicClass.SendMailAndStoreMailCommunication("cet@shipafreight.com", toEmail, "", "",
           "JobBooking", "Payment Process-Thanking mail to customer", bookingId, toEmail, body, "", true, message.Subject.ToString());
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "SendThankingMailtoCustomer", ex.Message.ToString(), ex.StackTrace.ToString());
                throw new ShipaApiException("Something Went Wrong.");
            }
        }

        StripeCardModel FetchCardData(StripeCharge data)
        {
            StripeCardModel card = new StripeCardModel();

            try
            {
                StripeData stripeData = GetStripeData(data.Currency);
                var sourceService = new StripeSourceService(stripeData.secretKey);
                StripeSource SourceObject = sourceService.Get(data.Source.Id);
                if (SourceObject.Type == "three_d_secure")
                {
                    var carddata = JsonConvert.DeserializeObject<StripeChargeResponseModel>(data.StripeResponse.ResponseJson);
                    card.Brand = carddata.Source.three_d_secure.Brand;
                    card.Last4 = carddata.Source.three_d_secure.Last4;
                }
                else
                {
                    card.Brand = SourceObject.Card.Brand;
                    card.Last4 = SourceObject.Card.Last4;
                }
            }
            catch (Exception ex)
            {
                LogError("api/Payment", "FetchCardData", ex.Message.ToString(), ex.StackTrace.ToString());
                card.Brand = string.Empty;
                card.Last4 = string.Empty;
            }
            return card;
        }

        public void SendEmailNotificationForExceptions(string url, string ex, string st)
        {
            string Actionlink = System.Configuration.ConfigurationManager.AppSettings["APPURL"];
            MailMessage message = new MailMessage();
            message.From = new MailAddress("noreply@shipafreight.com");
            message.To.Add(new MailAddress("pannepu@agility.com"));
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            message.Subject = "Exception notification" + EnvironmentName;
            message.IsBodyHtml = true;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"
                <table style='height: 74px;' width='284'>
                <tbody>
                <tr style='height: 17px;'>
                <td style='width: 134px; height: 17px;'>URL</td>"
            );
            sb.AppendLine("<td style='width: 134px; height: 17px;'>"); sb.Append(url); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 17px;'>
                <td style='width: 134px; height: 17px;'>EXCEPTION</td>");
            sb.AppendLine("<td style='width: 134px; height: 17px;'>"); sb.Append(ex); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                <tr style='height: 18px;'>
                <td style='width: 134px; height: 18px;'>STACKTRACE</td> ");
            sb.AppendLine("<td style='width: 134px; height: 18px;'>"); sb.Append(st); sb.Append("</td>");
            sb.AppendLine(@"
                </tr>
                </tbody>
                </table>");

            message.Body = sb.ToString();
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }

        private HsbcOrder HsbcCreateOrder(PaymentEntity model, string coupon)
        {
            long rand = 999999999999999;
            HsbcOrder order = new HsbcOrder()
            {
                limitPay = string.Empty,
                goodsInfo = string.Empty,
                remarks = string.Empty,
                orderTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                transType = "01",
                payType = "QCARDPAY",
                orderCurr = "CNY",
                merId = ConfigurationManager.AppSettings["MerchantID"].ToString(),
                merIp = ConfigurationManager.AppSettings["MerchantIP"].ToString(),
                userId = ConfigurationManager.AppSettings["UserID"].ToString(),
                notifyUrl = ConfigurationManager.AppSettings["APPURL"] + "api/Payment/HsbcWebhook",
                frontUrl = ConfigurationManager.AppSettings["APPURL"] + "api/Payment/HsbcResponse",
                goodsDes = model.OPJOBNUMBER,
                expireTime = "15"
            };

            string PaymentStatus = CheckPaymentStatus(Convert.ToInt32(model.JOBNUMBER));
            decimal PayAmount;
            if (PaymentStatus.ToLower() == "notexist")
            {
                PayAmount = model.RECEIPTNETAMOUNT;
            }
            else if (model.ADDITIONALCHARGESTATUS != null && model.ADDITIONALCHARGESTATUS.ToLower() == "pay now")
            {
                PayAmount = model.ADDITIONALCHARGEAMOUNT;
            }
            else
            {
                order.orderNo = string.Empty;
                return order;
            }

            var value = _JFactory.GetCurrencyAndOrder(PayAmount.ToString(), model.CURRENCY.ToUpper(), "CNY");
            string[] val = value.Split(',');

            order.orderAmt = Convert.ToInt32(Convert.ToDecimal(val[0]) * 100).ToString();
            order.orderNo = ConfigurationManager.AppSettings["MerchantID"].ToString()
                            + (rand - Convert.ToDouble(val[1])).ToString();

            _JFactory.SaveStripeTransactionDetails(
                model.JOBNUMBER.ToString(),
                "pending",
                PayAmount.ToString(),
                model.CURRENCY.ToUpper(),
                "Credit/Debit",
                order.orderNo,
                string.Empty,
                coupon,
                "CNY",
                val[0],
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                "HSBC"
            );

            return order;
        }

        private HSBCResponseDto sendRequest(string postData, string PaymentType, string HsbcMode)
        {
            try
            {
                HSBCResponseDto resp = new HSBCResponseDto();
                var link = string.Empty;
                if (HsbcMode.ToLowerInvariant() == "web")
                {
                    link = "merchant/pcPay";
                }
                else
                {
                    link = "merchant/mobilePay";
                }
                string url = ConfigurationManager.AppSettings["hsbcurl"].ToString() + link;
                string clientID = ConfigurationManager.AppSettings["clientID"].ToString();
                string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
                HttpContent httpContent = new StringContent(postData);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("x-hsbc-client-id", clientID);
                httpClient.DefaultRequestHeaders.Add("x-hsbc-client-secret", clientSecret);
                HttpResponseMessage response = httpClient.PostAsync(url, httpContent).Result;
                resp.StatusCode = response.IsSuccessStatusCode.ToString();
                if (response.IsSuccessStatusCode)
                {
                    string json = HsbcEncryption.decryptAndVerify(
                            response.Content.ReadAsStringAsync().Result);
                    var jo = (JObject)JsonConvert.DeserializeObject(json);
                    var res = (JObject)jo["response"];
                    if (jo["response"]["proCode"].ToString() != "999999")
                    {
                        if (PaymentType.ToLowerInvariant() == "qcardpay"
                            || PaymentType.ToLowerInvariant() == "card"
                            || PaymentType.ToLowerInvariant() == "pcpay")
                        {
                            var pageContent = HttpUtility.UrlDecode(
                                jo["response"]["htmlPage"].ToString());
                            resp.HtmlCode = Convert.ToBase64String(Encoding.UTF8.GetBytes(pageContent));
                        }
                    }
                    resp.proCode = jo["response"]["proCode"].ToString();
                    return resp;
                }
                else return resp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion private methods

        #region PDF's

        public static byte[] GenerateAdvanceReceiptPDF(
            int jobnumber, string ReportName, Int64 QuotaionID, string OperationJobNumber, string CreatedBy, int ChargeSetId, UserProfileEntity mUserModel, QuotationPreviewModel mUIModels)
        {
            MemoryStream memoryStream = new MemoryStream();
            string discountvalue = string.Empty;
            string SumAdditionalCharges = string.Empty;
            string TCount = string.Empty;
            DataTable AdvReceiptChargeDtls = null;
            JobBookingDbFactory mFactory = new JobBookingDbFactory();

            DBFactory.Entities.AdvanceReceiptEntity AdvReceiptDtls = mFactory.GetAdvanceReceiptDtls(Convert.ToInt32(jobnumber));
            DataTable ShipmentDtls = mFactory.GetShipmentDtls(jobnumber);
            if (ChargeSetId <= 0)
            {
                discountvalue = mFactory.GETDiscounts(Convert.ToInt32(jobnumber));
                AdvReceiptChargeDtls = mFactory.GetChargeDtls(jobnumber);
            }
            else
            {
                AdvReceiptChargeDtls = mFactory.GetAdditionalChargeSum(jobnumber, ChargeSetId);
            }
            DataTable UserDtls = mFactory.GetUserDtls(jobnumber);


            var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.CurrencySymbol = "";


            DataTable VATDtls = mFactory.GetVATDtls(jobnumber);
            string origintotalprice = string.Empty;
            string desttotalprice = string.Empty;
            string addtotalprice = string.Empty;
            string distotalprice = string.Empty;
            string intfrgttotalprice = string.Empty;

            int decimalCount = mFactory.GetDecimalPointByCurrencyCode(AdvReceiptDtls.PREFERREDCURRENCYID);

            double totalamount = 0;
            string Total = string.Empty;

            double decorigintotalprice = 0, decintfrgttotalprice = 0, decdesttotalprice = 0, decaddtotalprice = 0, decdiscount = 0;

            if (ChargeSetId > 0)
            {
                if (AdvReceiptChargeDtls.Rows.Count > 0)
                {
                    SumAdditionalCharges = AdvReceiptChargeDtls.Rows[0]["TOTALPRICE"].ToString();
                    TCount = AdvReceiptChargeDtls.Rows[0]["TCOUNT"].ToString();
                }
            }
            else
            {
                foreach (DataRow row in AdvReceiptChargeDtls.Rows)
                {
                    string ROUTETYPEID = row["ROUTETYPEID"].ToString();
                    string TOTALPRICE = row["TOTALPRICE"].ToString();

                    if (ROUTETYPEID == "1118")//"Origin"
                    {
                        origintotalprice = TOTALPRICE;//
                    }
                    if (ROUTETYPEID == "1116")//"international freight"
                    {
                        intfrgttotalprice = TOTALPRICE;
                    }
                    if (ROUTETYPEID == "1117")//"Destination"
                    {
                        desttotalprice = TOTALPRICE;
                    }
                    if (ROUTETYPEID == "250001")//"Additional"
                    {
                        addtotalprice = TOTALPRICE;
                    }
                    distotalprice = discountvalue;
                }
            }

            Document doc = new Document(PageSize.A4, 50, 50, 100, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            var ev = new ITextEvents();
            ev.mUIModels = mUIModels;
            ev.mUserModel = mUserModel;
            ev.z = 0;
            writer.PageEvent = ev;


            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = "Advance Receipt";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);

            string VATRegNo = string.Empty;
            if (VATDtls.Rows.Count > 0)
            {
                VATRegNo = VATDtls.Rows[0]["VATREGNUMBER"].ToString();
            }



            string ReceivedFrom = string.Empty;
            if (UserDtls.Rows.Count > 0)
            {
                ReceivedFrom = UserDtls.Rows[0]["FIRSTNAME"].ToString() + "," + UserDtls.Rows[0]["FULLADDRESS"].ToString();
            }

            //////////////////////

            Font boldFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
            Font normalFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new float[] { 5f, 47.5f, 47.5f });
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 100;
            table.SpacingBefore = 10;

            PdfPCell verticalcell = new PdfPCell(new Phrase("", new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL)));//if needed replace empty string with desired string to show vertical message in document.
            verticalcell.Rotation = 90;
            verticalcell.HorizontalAlignment = Element.ALIGN_CENTER;
            verticalcell.VerticalAlignment = Element.ALIGN_TOP;
            verticalcell.Border = 0;
            verticalcell.Rowspan = 6;
            table.AddCell(verticalcell);

            Phrase phrase = new Phrase();
            phrase.Add(
                new Chunk("Received From :", boldFont)
            );
            phrase.Add(Chunk.NEWLINE);
            if (ReceivedFrom != string.Empty)
            {
                for (int j = 0; j < ReceivedFrom.Split(',').Length; j++)
                {
                    if (ReceivedFrom.Split(',')[j].Trim() != string.Empty)
                    {
                        phrase.Add(new Chunk(ReceivedFrom.Split(',')[j].Trim(), normalFont));
                        phrase.Add(Chunk.NEWLINE);
                    }
                }
            }
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(
                new Chunk("Customer VAT Registered No :", boldFont)
            );
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk(VATRegNo, normalFont));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(Chunk.NEWLINE);

            if (ShipmentDtls.Rows[0]["ISHAZARDOUSCARGO"].ToString() != "0")
            {
                phrase.Add(
                    new Chunk("Hazardous Cargo", boldFont)
                );
                phrase.Add(new Chunk(" : " + ShipmentDtls.Rows[0]["HAZARDOUSGOODSTYPE"].ToString(), normalFont));
                phrase.Add(Chunk.NEWLINE);
                phrase.Add(Chunk.NEWLINE);
            }


            PdfPCell cell1 = new PdfPCell();
            cell1.PaddingLeft = 10;
            cell1.PaddingTop = 10;
            cell1.Colspan = 1;
            cell1.AddElement(phrase);


            table.AddCell(cell1);

            Phrase phrase1 = new Phrase();
            phrase1.Add(
                new Chunk("Receipt No", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            if (ChargeSetId <= 0)
            {
                phrase1.Add(new Chunk(": " + AdvReceiptDtls.RECEIPTNUMBER, normalFont));
            }
            else
            {
                phrase1.Add(new Chunk(": " + AdvReceiptDtls.RECEIPTNUMBER + "AR" + ChargeSetId, normalFont));
            }



            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Date of Issue", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            phrase1.Add(new Chunk(": " + AdvReceiptDtls.RECEIPTDATE, normalFont));

            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Payment Type", boldFont)
            );
            phrase1.Add(Chunk.SPACETABBING);
            phrase1.Add(new Chunk(": " + AdvReceiptDtls.PAYMENTOPTION + " - " + AdvReceiptDtls.PAYMENTTYPE, normalFont));

            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(
                new Chunk("Transaction Reference ID :", boldFont)
            );
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(new Chunk(AdvReceiptDtls.PAYMENTREFNUMBER, normalFont));
            phrase1.Add(Chunk.NEWLINE);
            phrase1.Add(Chunk.NEWLINE);

            PdfPCell cell2 = new PdfPCell();
            cell2.PaddingLeft = 10;
            cell2.PaddingTop = 10;
            cell2.Colspan = 1;
            cell2.AddElement(phrase1);
            table.AddCell(cell2);


            Phrase bookingphrase = new Phrase();
            bookingphrase.Add(
                new Chunk("Booking ID :", boldFont)
            );
            bookingphrase.Add(Chunk.SPACETABBING);
            bookingphrase.Add(new Chunk(AdvReceiptDtls.OPERATIONALJOBNUMBER, normalFont));
            bookingphrase.Add(Chunk.NEWLINE);
            bookingphrase.Add(Chunk.NEWLINE);


            PdfPCell bookingcell = new PdfPCell();
            bookingcell.PaddingLeft = 10;
            bookingcell.PaddingTop = 10;
            bookingcell.AddElement(bookingphrase);
            bookingcell.Colspan = 2;
            table.AddCell(bookingcell);


            Phrase chargedescphrase = new Phrase();
            chargedescphrase.Add(
                new Chunk("Charges Description", boldFont)
            );
            chargedescphrase.Add(Chunk.NEWLINE);
            chargedescphrase.Add(Chunk.NEWLINE);

            PdfPCell chargedesccell = new PdfPCell();
            chargedesccell.PaddingLeft = 10;
            chargedesccell.PaddingTop = 10;
            chargedesccell.Colspan = 1;
            chargedesccell.AddElement(chargedescphrase);
            table.AddCell(chargedesccell);


            Phrase amountphrase = new Phrase();
            amountphrase.Add(new Chunk(new VerticalPositionMark()));
            amountphrase.Add(
                new Chunk("Amount", boldFont)
                );
            amountphrase.Add(Chunk.NEWLINE);
            amountphrase.Add(Chunk.NEWLINE);

            PdfPCell amountcell = new PdfPCell();
            amountcell.PaddingRight = 10;
            amountcell.PaddingTop = 10;
            amountcell.Colspan = 1;
            amountcell.AddElement(amountphrase);
            table.AddCell(amountcell);
            Phrase chargesphrase = new Phrase();
            if (ChargeSetId <= 0)
            {
                chargesphrase.Add(
                    new Chunk("Origin Charges", normalFont)
                    );

                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                chargesphrase.Add(
                    new Chunk("International Freight Charges", normalFont)
                    );

                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                chargesphrase.Add(
                    new Chunk("Destination Charges", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                chargesphrase.Add(
                       new Chunk("Optional Charges", normalFont)
                    );
                chargesphrase.Add(Chunk.NEWLINE);
                chargesphrase.Add(Chunk.NEWLINE);

                if (distotalprice != "0")
                {
                    chargesphrase.Add(
                        new Chunk("Discount", normalFont)
                        );
                    chargesphrase.Add(Chunk.NEWLINE);
                    chargesphrase.Add(Chunk.NEWLINE);
                }
            }
            else
            {
                if (SumAdditionalCharges != "0" && SumAdditionalCharges != "")
                {
                    chargesphrase.Add(
                       new Chunk("Additional Charges", normalFont)
                       );
                    chargesphrase.Add(Chunk.NEWLINE);
                    chargesphrase.Add(Chunk.NEWLINE);
                }
            }

            if (origintotalprice != string.Empty)
                decorigintotalprice = Double.Parse(origintotalprice);
            if (intfrgttotalprice != string.Empty)
                decintfrgttotalprice = Double.Parse(intfrgttotalprice);
            if (desttotalprice != string.Empty)
                decdesttotalprice = Double.Parse(desttotalprice);
            if (addtotalprice != string.Empty)
                decaddtotalprice = Double.Parse(addtotalprice);
            if (distotalprice != string.Empty)
                decdiscount = Double.Parse(distotalprice);

            totalamount = decorigintotalprice + decintfrgttotalprice + decdesttotalprice + decaddtotalprice - decdiscount;
            if (ChargeSetId <= 0)
            {
                Total = Double.Parse(totalamount.ToString()).ToString("N" + decimalCount);
            }
            else
            {
                Total = Double.Parse(SumAdditionalCharges.ToString()).ToString("N" + decimalCount);
            }

            PdfPCell chargescell = new PdfPCell();
            chargescell.PaddingLeft = 10;
            chargescell.PaddingTop = 10;
            chargescell.Colspan = 1;
            chargescell.AddElement(chargesphrase);
            table.AddCell(chargescell);

            Phrase amountsphrase = new Phrase();
            if (ChargeSetId <= 0)
            {
                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                             new Chunk(decorigintotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                        );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);

                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                             new Chunk(decintfrgttotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                        );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);


                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                              new Chunk(decdesttotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                            );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);

                amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                amountsphrase.Add(
                              new Chunk(decaddtotalprice.ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                             );
                amountsphrase.Add(Chunk.NEWLINE);
                amountsphrase.Add(Chunk.NEWLINE);
                if (distotalprice != "0")
                {
                    amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                    amountsphrase.Add(
                                       new Chunk(distotalprice + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                                 );
                    amountsphrase.Add(Chunk.NEWLINE);
                    amountsphrase.Add(Chunk.NEWLINE);
                }
            }
            else
            {
                if (SumAdditionalCharges != "0" && SumAdditionalCharges != "")
                {
                    amountsphrase.Add(new Chunk(new VerticalPositionMark()));
                    amountsphrase.Add(
                                  new Chunk(Double.Parse(SumAdditionalCharges).ToString("N" + decimalCount) + " " + AdvReceiptDtls.PREFERREDCURRENCYID, normalFont)
                                 );
                    amountsphrase.Add(Chunk.NEWLINE);
                    amountsphrase.Add(Chunk.NEWLINE);
                }
            }

            PdfPCell amountscell = new PdfPCell();
            amountscell.PaddingRight = 10;
            amountscell.PaddingTop = 10;
            amountcell.Colspan = 1;
            amountscell.AddElement(amountsphrase);
            table.AddCell(amountscell);


            Phrase totalphrase = new Phrase();
            totalphrase.Add(
                new Chunk("Total", boldFont)
            );
            totalphrase.Add(Chunk.NEWLINE);
            totalphrase.Add(Chunk.NEWLINE);

            PdfPCell totalcell = new PdfPCell();
            totalcell.PaddingLeft = 10;
            totalcell.PaddingTop = 10;
            totalcell.Colspan = 1;
            totalcell.AddElement(totalphrase);
            table.AddCell(totalcell);



            Phrase totalamountphrase = new Phrase();
            totalamountphrase.Add(new Chunk(new VerticalPositionMark()));
            totalamountphrase.Add(
                new Chunk(Total + " " + AdvReceiptDtls.PREFERREDCURRENCYID, boldFont)
                );
            totalamountphrase.Add(Chunk.NEWLINE);
            totalamountphrase.Add(Chunk.NEWLINE);
            PdfPCell totalamountcell = new PdfPCell();
            totalamountcell.PaddingRight = 10;
            totalamountcell.PaddingTop = 10;
            totalamountcell.Colspan = 1;
            totalamountcell.AddElement(totalamountphrase);
            table.AddCell(totalamountcell);

            try
            {
                DataTable BillingInfo = mFactory.GetBillingInfo(jobnumber);
                var count = BillingInfo.Rows.Count - 1;
                var paidcurrency = BillingInfo.Rows[count]["PAIDCURRENCY"].ToString();
                var jobtotal = BillingInfo.Rows[count]["JOBTOTAL"].ToString();
                if (AdvReceiptDtls.PREFERREDCURRENCYID.ToLowerInvariant() != paidcurrency.ToLowerInvariant())
                {
                    int NewdecimalCount = mFactory.GetDecimalPointByCurrencyCode(paidcurrency);
                    Phrase currencyphrase = new Phrase();
                    currencyphrase.Add(
                        new Chunk("Payment Amount", boldFont)
                    );
                    currencyphrase.Add(Chunk.NEWLINE);
                    currencyphrase.Add(Chunk.NEWLINE);

                    PdfPCell currencycell = new PdfPCell();
                    currencycell.PaddingLeft = 10;
                    currencycell.PaddingTop = 10;
                    currencycell.Colspan = 1;
                    currencycell.AddElement(currencyphrase);
                    table.AddCell(currencycell);


                    Phrase totalcurrencyphrase = new Phrase();
                    totalcurrencyphrase.Add(new Chunk(new VerticalPositionMark()));
                    totalcurrencyphrase.Add(
                        new Chunk(Double.Parse(jobtotal).ToString("N" + NewdecimalCount) + " " + paidcurrency, boldFont)
                        );
                    totalcurrencyphrase.Add(Chunk.NEWLINE);
                    totalcurrencyphrase.Add(Chunk.NEWLINE);
                    PdfPCell totalcurrencycell = new PdfPCell();
                    totalcurrencycell.PaddingRight = 10;
                    totalcurrencycell.PaddingTop = 10;
                    totalcurrencycell.Colspan = 1;
                    totalcurrencycell.AddElement(totalcurrencyphrase);
                    table.AddCell(totalcurrencycell);
                }
            }
            catch
            {
                //
            }

            doc.Add(table);

            PdfPTable note = new PdfPTable(2);
            note.SetWidths(new float[] { 5f, 95f });
            note.WidthPercentage = 100;
            note.SpacingBefore = 10;
            note.DefaultCell.Border = 0;
            note.AddCell(" ");
            note.AddCell(new PdfPCell(mFormattingpdf.TableContentCell("IMPORTANT: All business handled by Agility is subject to Agility's trading terms and conditions, which contain limitations of liability." +
            "Copies of these applicable terms and conditions are available upon request.", Element.ALIGN_LEFT)));

            doc.Add(note);

            ///////////

            doc.Close();

            byte[] bytes = memoryStream.ToArray();
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }

            //Insert same data into db : Anil G
            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
            sspdoc.QUOTATIONID = Convert.ToInt64(QuotaionID);
            sspdoc.DOCUMNETTYPE = "ShipAFreightDoc";
            if (ChargeSetId > 0)
            {
                sspdoc.FILENAME = "AdvanceCashReceipt_AdditionalCharges_" + TCount + ".pdf";
            }
            else
            {
                sspdoc.FILENAME = "AdvanceCashReceipt_" + OperationJobNumber.ToString() + ".pdf";
            }
            sspdoc.FILEEXTENSION = "application/pdf";
            sspdoc.FILESIZE = bytes.Length;
            sspdoc.FILECONTENT = bytes;
            sspdoc.JOBNUMBER = Convert.ToInt64(jobnumber);
            sspdoc.DocumentName = ReportName;
            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
            JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

            Guid obj = Guid.NewGuid();
            string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();

            mjobFactory.SaveDocuments(docentity, Convert.ToString(CreatedBy), "System Generated", obj.ToString(), EnvironmentName);

            return bytes;
        }

        public static byte[] GenerateBookingConfirmationPDF(int jobnumber, string ReportName, Int64 QuotaionID,
            string OperationJobNumber, string CreatedBy, UserProfileEntity mUserModel, QuotationPreviewModel mUIModels
            )
        {
            MemoryStream memoryStream = new MemoryStream();

            if (mUserModel == null)
            {
                UserDBFactory mFactory1 = new UserDBFactory();
                mUserModel = mFactory1.GetUserProfileDetails(CreatedBy, 1);
            }
            JobBookingDbFactory mFactory = new JobBookingDbFactory();
            DataTable QuoteDtls = mFactory.GetQuoteDtls(jobnumber);
            DataTable PartyDtls = mFactory.GetPartyDtls(jobnumber);
            DataTable BillingInfo = mFactory.GetBillingInfo(jobnumber);
            DataTable ShipmentDtls = mFactory.GetShipmentDtls(jobnumber);
            DataTable VATDtls = mFactory.GetVATDtls(jobnumber);

            DataTable CargoDtls = mFactory.GetCargoDtls(Convert.ToInt32(jobnumber));

            Document doc = new Document(PageSize.A4, 50, 50, 125, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            var ev = new ITextEvents();
            ev.mUserModel = mUserModel;
            ev.mUIModels = mUIModels;
            writer.PageEvent = ev;

            Font boldFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
            Font normalFont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);

            PdfPTable table = new PdfPTable(9);
            table.SetWidths(new float[] { 5f, 5f, 14.5f, 9f, 12.5f, 15.5f, 15.5f, 12.5f, 10.5f });
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 100;
            table.SpacingBefore = 10;

            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 2;
            string mDocumentHeading = "Booking Confirmation";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);


            PdfPCell verticalcell = new PdfPCell(new Phrase("IMPORTANT: All business handled by Agility is subject to Agility's trading terms and conditions, which contain limitations of liability." + Environment.NewLine + " Copies of these applicable terms and conditions are available upon request.", new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL)));
            verticalcell.Rotation = 90;
            verticalcell.HorizontalAlignment = Element.ALIGN_CENTER;
            verticalcell.VerticalAlignment = Element.ALIGN_TOP;
            verticalcell.Rowspan = 12;

            table.AddCell(verticalcell);

            Phrase phrase = new Phrase();
            phrase.Add(
                new Chunk("Quote No", normalFont)
            );
            PdfPCell quotenolblCell = new PdfPCell();
            quotenolblCell.AddElement(phrase);
            quotenolblCell.PaddingBottom = 6;
            quotenolblCell.Colspan = 2;
            table.AddCell(quotenolblCell);

            Phrase quotenophrase = new Phrase();
            quotenophrase.Add(
                new Chunk(QuoteDtls.Rows[0]["QUOTATIONNUMBER"].ToString(), boldFont)
            );
            PdfPCell quotenocell = new PdfPCell();
            quotenocell.AddElement(quotenophrase);
            quotenocell.PaddingBottom = 6;
            quotenocell.Colspan = 2;
            table.AddCell(quotenocell);


            Phrase bookingIdPhrase = new Phrase();
            bookingIdPhrase.Add(
                new Chunk("Booking Id", normalFont)
                );
            bookingIdPhrase.Add(Chunk.SPACETABBING);
            bookingIdPhrase.Add(new Chunk(QuoteDtls.Rows[0]["OPERATIONALJOBNUMBER"].ToString(), boldFont));


            bookingIdPhrase.Add(Chunk.SPACETABBING);
            bookingIdPhrase.Add(new Chunk("Date", normalFont));
            bookingIdPhrase.Add(Chunk.SPACETABBING);
            bookingIdPhrase.Add(new Chunk(System.DateTime.Now.ToString("dd-MMM-yyyy"), boldFont));


            PdfPCell bookingIdCell = new PdfPCell();
            bookingIdCell.AddElement(bookingIdPhrase);
            bookingIdCell.PaddingBottom = 6;
            bookingIdCell.Colspan = 4;
            table.AddCell(bookingIdCell);

            PdfPCell shipperlblcell = new PdfPCell(new Phrase(new Chunk("Shipper", boldFont)));
            shipperlblcell.PaddingBottom = 5;
            shipperlblcell.Colspan = 2;
            table.AddCell(shipperlblcell);

            PdfPCell shippercntlblcell = new PdfPCell(new Phrase(new Chunk("Contact", boldFont)));
            shippercntlblcell.PaddingBottom = 5;
            shippercntlblcell.Colspan = 2;
            table.AddCell(shippercntlblcell);

            PdfPCell consigneelblcell = new PdfPCell(new Phrase(new Chunk("Consignee", boldFont)));
            consigneelblcell.PaddingBottom = 5;
            consigneelblcell.Colspan = 2;
            table.AddCell(consigneelblcell);

            PdfPCell consigneecntlblcell = new PdfPCell(new Phrase(new Chunk("Contact", boldFont)));
            consigneecntlblcell.PaddingBottom = 5;
            consigneecntlblcell.Colspan = 2;
            table.AddCell(consigneecntlblcell);

            DataTable dtShipper = new DataTable();
            DataTable dtConsignee = new DataTable();
            if (PartyDtls.Rows.Count > 0)
            {
                dtShipper = PartyDtls.AsEnumerable().Where(i => i.Field<string>("PARTYTYPE") == "91").CopyToDataTable();
                dtConsignee = PartyDtls.AsEnumerable().Where(i => i.Field<string>("PARTYTYPE") == "92").CopyToDataTable();
            }

            string ShipperContact = string.Empty;
            string ConsigneeContact = string.Empty;
            string ShipperAddress = string.Empty;
            string ConsigneeAddress = string.Empty;

            string VATRegNo = string.Empty;
            if (VATDtls.Rows.Count > 0)
            {
                VATRegNo = "VAT Reg No: " + VATDtls.Rows[0]["VATREGNUMBER"].ToString();
            }

            if (dtShipper.Rows.Count > 0)
            {
                ShipperContact = dtShipper.Rows[0]["SALUTATION"].ToString() + " " + dtShipper.Rows[0]["FIRSTNAME"].ToString() + " " + dtShipper.Rows[0]["LASTNAME"].ToString() + "\n" + dtShipper.Rows[0]["EMAILID"].ToString() + "\n" + dtShipper.Rows[0]["PHONE"].ToString();
                ShipperAddress = dtShipper.Rows[0]["CLIENTNAME"].ToString() + Environment.NewLine + dtShipper.Rows[0]["FULLADDRESS"].ToString() + Environment.NewLine + VATRegNo;
            }
            if (dtConsignee.Rows.Count > 0)
            {
                ConsigneeContact = dtConsignee.Rows[0]["SALUTATION"].ToString() + " " + dtConsignee.Rows[0]["FIRSTNAME"].ToString() + " " + dtConsignee.Rows[0]["LASTNAME"].ToString() + "\n" + dtConsignee.Rows[0]["EMAILID"].ToString() + "\n" + dtConsignee.Rows[0]["PHONE"].ToString();
                ConsigneeAddress = dtConsignee.Rows[0]["CLIENTNAME"].ToString() + Environment.NewLine + dtConsignee.Rows[0]["FULLADDRESS"].ToString();
            }

            Phrase shipperaddphrase = new Phrase();
            shipperaddphrase.Add(new Chunk(ShipperAddress, normalFont));
            shipperaddphrase.Add(Chunk.NEWLINE);
            shipperaddphrase.Add(Chunk.NEWLINE);
            shipperaddphrase.Add(Chunk.NEWLINE);

            PdfPCell shippercell = new PdfPCell(shipperaddphrase);
            shippercell.PaddingBottom = 10;
            shippercell.PaddingRight = 10;
            shippercell.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            shippercell.Colspan = 2;
            shippercell.SetLeading(0, 1.5f);
            table.AddCell(shippercell);

            Phrase shippercntphrase = new Phrase();
            shippercntphrase.Add(new Chunk(ShipperContact, normalFont));
            shippercntphrase.Add(Chunk.NEWLINE);
            shippercntphrase.Add(Chunk.NEWLINE);
            shippercntphrase.Add(Chunk.NEWLINE);


            PdfPCell shippercntcell = new PdfPCell(shippercntphrase);
            shippercntcell.PaddingBottom = 10;
            shippercntcell.PaddingRight = 10;
            shippercntcell.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            shippercntcell.Colspan = 2;
            shippercntcell.SetLeading(0, 1.5f);
            table.AddCell(shippercntcell);

            Phrase consigneeaddphrase = new Phrase();
            consigneeaddphrase.Add(new Chunk(ConsigneeAddress, normalFont));
            consigneeaddphrase.Add(Chunk.NEWLINE);
            consigneeaddphrase.Add(Chunk.NEWLINE);
            consigneeaddphrase.Add(Chunk.NEWLINE);


            PdfPCell consigneecell = new PdfPCell(consigneeaddphrase);
            consigneecell.PaddingBottom = 10;
            consigneecell.PaddingRight = 10;
            consigneecell.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            consigneecell.Colspan = 2;
            consigneecell.SetLeading(0, 1.5f);
            table.AddCell(consigneecell);

            Phrase consigneecntphrase = new Phrase();
            consigneecntphrase.Add(new Chunk(ConsigneeContact, normalFont));
            consigneecntphrase.Add(Chunk.NEWLINE);
            consigneecntphrase.Add(Chunk.NEWLINE);
            consigneecntphrase.Add(Chunk.NEWLINE);

            PdfPCell consigneecntcell = new PdfPCell(consigneecntphrase);
            consigneecntcell.PaddingBottom = 10;
            consigneecntcell.PaddingRight = 10;
            consigneecntcell.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            consigneecntcell.Colspan = 2;
            consigneecntcell.SetLeading(0, 1.5f);
            table.AddCell(consigneecntcell);


            PdfPCell paymentdtlslblcell = new PdfPCell(new Phrase(new Chunk("Payment Details", boldFont)));
            paymentdtlslblcell.Colspan = 4;
            paymentdtlslblcell.PaddingBottom = 5;
            table.AddCell(paymentdtlslblcell);

            PdfPCell billinginfolblcell = new PdfPCell(new Phrase(new Chunk("Billing Information", boldFont)));
            billinginfolblcell.Colspan = 4;
            billinginfolblcell.PaddingBottom = 5;
            table.AddCell(billinginfolblcell);


            string PAYMENTREFNUMBER = string.Empty;
            string PAYMENTTYPE = string.Empty;
            string RECEIPTNETAMOUNT = string.Empty;
            string RECEIPTNUMBER = string.Empty;
            string RECEIPTDATE = string.Empty;
            string CURRENCY = string.Empty;

            var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.CurrencySymbol = "";

            if (BillingInfo.Rows.Count > 0)
            {
                PAYMENTREFNUMBER = BillingInfo.Rows[0]["PAYMENTREFNUMBER"].ToString();
                PAYMENTTYPE = BillingInfo.Rows[0]["PAYMENTOPTION"].ToString() + " - " + BillingInfo.Rows[0]["PAYMENTTYPE"].ToString();
                RECEIPTNETAMOUNT = BillingInfo.Rows[0]["RECEIPTNETAMOUNT"].ToString();
                RECEIPTNUMBER = BillingInfo.Rows[0]["RECEIPTNUMBER"].ToString();
                RECEIPTDATE = Convert.ToDateTime(BillingInfo.Rows[0]["RECEIPTDATE"]).ToString("dd-MMM-yyyy");
                CURRENCY = BillingInfo.Rows[0]["CURRENCY"].ToString();
                var decimalCount = mFactory.GetDecimalPointByCurrencyCode(CURRENCY);

                if (RECEIPTNETAMOUNT != "")
                    RECEIPTNETAMOUNT = Double.Parse(RECEIPTNETAMOUNT.ToString()).ToString("N" + decimalCount);
            }

            Phrase paymentdtlsphrase = new Phrase();
            paymentdtlsphrase.Add(new Chunk("Transaction Reference ID :", normalFont));
            paymentdtlsphrase.Add(Chunk.SPACETABBING);
            paymentdtlsphrase.Add(new Chunk(PAYMENTREFNUMBER, boldFont));
            paymentdtlsphrase.Add(Chunk.NEWLINE);
            paymentdtlsphrase.Add(Chunk.NEWLINE);


            PdfPCell paymentdtlscell = new PdfPCell();
            paymentdtlscell.AddElement(paymentdtlsphrase);
            paymentdtlscell.Colspan = 4;
            paymentdtlscell.PaddingBottom = 10;
            paymentdtlscell.PaddingRight = 10;
            table.AddCell(paymentdtlscell);


            Phrase billinginfophrase = new Phrase();
            billinginfophrase.Add(new Chunk("Payment Type", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + PAYMENTTYPE, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);

            billinginfophrase.Add(new Chunk("Net Amount", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + RECEIPTNETAMOUNT + " " + CURRENCY, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);

            billinginfophrase.Add(new Chunk("Receipt Number", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + RECEIPTNUMBER, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);

            billinginfophrase.Add(new Chunk("Date of Receipt", normalFont));
            billinginfophrase.Add(Chunk.SPACETABBING);
            billinginfophrase.Add(new Chunk(" :  " + RECEIPTDATE, boldFont));
            billinginfophrase.Add(Chunk.NEWLINE);
            billinginfophrase.Add(Chunk.NEWLINE);

            PdfPCell billinginfocell = new PdfPCell();
            billinginfocell.AddElement(billinginfophrase);
            billinginfocell.Colspan = 4;
            billinginfocell.PaddingBottom = 10;
            billinginfocell.PaddingRight = 10;
            table.AddCell(billinginfocell);

            PdfPCell shipmentdtlslblcell = new PdfPCell(new Phrase(new Chunk("Shipment Details", boldFont)));
            shipmentdtlslblcell.Colspan = 8;
            shipmentdtlslblcell.PaddingBottom = 5;
            table.AddCell(shipmentdtlslblcell);


            string Product = string.Empty;
            string MovementType = string.Empty;
            string PlaceOfReceipt = string.Empty;
            string PlaceOfDelivery = string.Empty;
            var MName = string.Empty;

            if (ShipmentDtls.Rows.Count > 0)
            {
                Product = ShipmentDtls.Rows[0]["PRODUCTNAME"].ToString();
                MovementType = ShipmentDtls.Rows[0]["MOVEMENTTYPENAME"].ToString();

                if (MovementType == "Door to Door" || MovementType == "Door to door")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPLACENAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPLACENAME"].ToString();
                }
                else if (MovementType == "Door to port")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPLACENAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPORTNAME"].ToString();
                }
                else if (MovementType == "Port to Port" || MovementType == "Port to port")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPORTNAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPORTNAME"].ToString();
                }
                else if (MovementType == "Port to door")
                {
                    PlaceOfReceipt = ShipmentDtls.Rows[0]["ORIGINPORTNAME"].ToString();
                    PlaceOfDelivery = ShipmentDtls.Rows[0]["DESTINATIONPLACENAME"].ToString();
                }
            }

            if (ShipmentDtls.Rows[0]["PRODUCTID"].ToString() == "3")
            {
                if (MovementType == "Port to port") { MName = "Airport to airport"; }
                if (MovementType == "Door to port") { MName = "Door to airport"; }
                if (MovementType == "Port to door") { MName = "Airport to door"; }
                if (MovementType == "Door to door") { MName = "Door to door"; }
            }
            else
            {
                MName = MovementType;
            }

            Phrase shipmentdtlsphrase1 = new Phrase();
            shipmentdtlsphrase1.Add(new Chunk("Product", normalFont));
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            shipmentdtlsphrase1.Add(new Chunk(Product, boldFont));
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            shipmentdtlsphrase1.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell1 = new PdfPCell(shipmentdtlsphrase1);
            shipmentdtlscell1.Colspan = 2;
            shipmentdtlscell1.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell1.PaddingBottom = 5;


            Phrase shipmentdtlsphrase2 = new Phrase();
            shipmentdtlsphrase2.Add(new Chunk("Movement Type", normalFont));
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            shipmentdtlsphrase2.Add(new Chunk(MName, boldFont));
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            shipmentdtlsphrase2.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell2 = new PdfPCell(shipmentdtlsphrase2);
            shipmentdtlscell2.Colspan = 2;
            shipmentdtlscell2.Border = PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell2.PaddingBottom = 5;

            Phrase shipmentdtlsphrase3 = new Phrase();
            shipmentdtlsphrase3.Add(new Chunk("Place of Receipt", normalFont));
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            shipmentdtlsphrase3.Add(new Chunk(PlaceOfReceipt, boldFont));
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            shipmentdtlsphrase3.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell3 = new PdfPCell(shipmentdtlsphrase3);
            shipmentdtlscell3.Colspan = 2;
            shipmentdtlscell3.Border = PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell3.PaddingBottom = 5;

            Phrase shipmentdtlsphrase4 = new Phrase();
            shipmentdtlsphrase4.Add(new Chunk("Place of Delivery", normalFont));
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            shipmentdtlsphrase4.Add(new Chunk(PlaceOfDelivery, boldFont));
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            shipmentdtlsphrase4.Add(Chunk.NEWLINE);
            PdfPCell shipmentdtlscell4 = new PdfPCell(shipmentdtlsphrase4);
            shipmentdtlscell4.Colspan = 2;
            shipmentdtlscell4.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            shipmentdtlscell4.PaddingBottom = 5;

            table.AddCell(shipmentdtlscell1);
            table.AddCell(shipmentdtlscell2);
            table.AddCell(shipmentdtlscell3);
            table.AddCell(shipmentdtlscell4);

            if (Convert.ToString(ShipmentDtls.Rows[0]["ISHAZARDOUSCARGO"]) != "0")
            {
                Phrase hazardous = new Phrase();
                hazardous.Add(new Chunk("Hazardous Cargo", normalFont));
                hazardous.Add(Chunk.NEWLINE);
                hazardous.Add(Chunk.NEWLINE);
                hazardous.Add(new Chunk(Convert.ToString(ShipmentDtls.Rows[0]["HAZARDOUSGOODSTYPE"]), boldFont));
                hazardous.Add(Chunk.NEWLINE);
                PdfPCell hazardouspdf = new PdfPCell(hazardous);
                hazardouspdf.Colspan = 8;
                hazardouspdf.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                hazardouspdf.PaddingBottom = 5;
                table.AddCell(hazardouspdf);
            }
            PdfPCell cargodtlslblcell = new PdfPCell(new Phrase(new Chunk("Cargo Details", boldFont)));
            cargodtlslblcell.Colspan = 8;
            cargodtlslblcell.PaddingBottom = 5;
            table.AddCell(cargodtlslblcell);

            Phrase cargodtlsphrase1 = new Phrase();
            cargodtlsphrase1.Add(new Chunk("Sno", normalFont));
            cargodtlsphrase1.Add(Chunk.NEWLINE);
            cargodtlsphrase1.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell1 = new PdfPCell(cargodtlsphrase1);
            cargodtlscell1.PaddingBottom = 5;
            cargodtlscell1.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase2 = new Phrase();
            cargodtlsphrase2.Add(new Chunk("Package Type", normalFont));
            cargodtlsphrase2.Add(Chunk.NEWLINE);
            cargodtlsphrase2.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell2 = new PdfPCell(cargodtlsphrase2);
            cargodtlscell2.PaddingBottom = 5;
            cargodtlscell2.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase3 = new Phrase();
            cargodtlsphrase3.Add(new Chunk("Quantity", normalFont));
            cargodtlsphrase3.Add(Chunk.NEWLINE);
            cargodtlsphrase3.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell3 = new PdfPCell(cargodtlsphrase3);
            cargodtlscell3.PaddingBottom = 5;
            cargodtlscell3.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase4 = new Phrase();
            cargodtlsphrase4.Add(new Chunk("LWH", normalFont));
            cargodtlsphrase4.Add(Chunk.NEWLINE);
            cargodtlsphrase4.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell4 = new PdfPCell(cargodtlsphrase4);
            cargodtlscell4.PaddingBottom = 5;
            cargodtlscell4.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase5 = new Phrase();
            cargodtlsphrase5.Add(new Chunk("UOM of LWH", normalFont));
            cargodtlsphrase5.Add(Chunk.NEWLINE);
            cargodtlsphrase5.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell5 = new PdfPCell(cargodtlsphrase5);
            cargodtlscell5.PaddingBottom = 5;
            cargodtlscell5.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase6 = new Phrase();
            cargodtlsphrase6.Add(new Chunk("Per Piece Weight", normalFont));
            cargodtlsphrase6.Add(Chunk.NEWLINE);
            cargodtlsphrase6.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell6 = new PdfPCell(cargodtlsphrase6);
            cargodtlscell6.PaddingBottom = 5;
            cargodtlscell6.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase7 = new Phrase();
            cargodtlsphrase7.Add(new Chunk("Total Weight", normalFont));
            cargodtlsphrase7.Add(Chunk.NEWLINE);
            cargodtlsphrase7.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell7 = new PdfPCell(cargodtlsphrase7);
            cargodtlscell7.PaddingBottom = 5;
            cargodtlscell7.HorizontalAlignment = Element.ALIGN_CENTER;

            Phrase cargodtlsphrase8 = new Phrase();
            cargodtlsphrase8.Add(new Chunk("Total CBM", normalFont));
            cargodtlsphrase8.Add(Chunk.NEWLINE);
            cargodtlsphrase8.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell8 = new PdfPCell(cargodtlsphrase8);
            cargodtlscell8.PaddingBottom = 5;
            cargodtlscell8.HorizontalAlignment = Element.ALIGN_CENTER;

            string PERPIECEWEIGHT = string.Empty;
            string TOTALWEIGHT = string.Empty;

            for (int i = 0; i < CargoDtls.Rows.Count; i++)
            {
                if (CargoDtls.Rows[i]["WEIGHTPERPIECE"].ToString() != "")
                    PERPIECEWEIGHT = string.Format(culture, "{0:c}", Convert.ToDecimal(CargoDtls.Rows[i]["WEIGHTPERPIECE"].ToString()));
                if (CargoDtls.Rows[i]["WEIGHTTOTAL"].ToString() != "")
                    TOTALWEIGHT = string.Format(culture, "{0:c}", Convert.ToDecimal(CargoDtls.Rows[i]["WEIGHTTOTAL"].ToString()));

                cargodtlsphrase1.Add(new Chunk(CargoDtls.Rows[i]["SEQNO"].ToString(), boldFont));
                cargodtlsphrase1.Add(Chunk.NEWLINE);
                cargodtlsphrase1.Add(Chunk.NEWLINE);

                cargodtlsphrase2.Add(new Chunk(CargoDtls.Rows[i]["PACKAGETYPENAME"].ToString(), boldFont));
                cargodtlsphrase2.Add(Chunk.NEWLINE);
                cargodtlsphrase2.Add(Chunk.NEWLINE);

                cargodtlsphrase3.Add(new Chunk(CargoDtls.Rows[i]["QUANTITY"].ToString(), boldFont));
                cargodtlsphrase3.Add(Chunk.NEWLINE);
                cargodtlsphrase3.Add(Chunk.NEWLINE);

                cargodtlsphrase4.Add(new Chunk(CargoDtls.Rows[i]["ITEMLENGTH"].ToString() + "," + CargoDtls.Rows[i]["ITEMWIDTH"].ToString() + "," + CargoDtls.Rows[i]["ITEMHEIGHT"].ToString(), boldFont));
                cargodtlsphrase4.Add(Chunk.NEWLINE);
                cargodtlsphrase4.Add(Chunk.NEWLINE);

                cargodtlsphrase5.Add(new Chunk(CargoDtls.Rows[i]["LENGTHUOMNAME"].ToString(), boldFont));
                cargodtlsphrase5.Add(Chunk.NEWLINE);
                cargodtlsphrase5.Add(Chunk.NEWLINE);

                cargodtlsphrase6.Add(new Chunk(PERPIECEWEIGHT, boldFont));
                cargodtlsphrase6.Add(Chunk.NEWLINE);
                cargodtlsphrase6.Add(Chunk.NEWLINE);

                cargodtlsphrase7.Add(new Chunk(TOTALWEIGHT, boldFont));
                cargodtlsphrase7.Add(Chunk.NEWLINE);
                cargodtlsphrase7.Add(Chunk.NEWLINE);

                cargodtlsphrase8.Add(new Chunk(CargoDtls.Rows[i]["TOTALCBM"].ToString(), boldFont));
                cargodtlsphrase8.Add(Chunk.NEWLINE);
                cargodtlsphrase8.Add(Chunk.NEWLINE);
            }

            table.AddCell(cargodtlscell1);
            table.AddCell(cargodtlscell2);
            table.AddCell(cargodtlscell3);
            table.AddCell(cargodtlscell4);
            table.AddCell(cargodtlscell5);
            table.AddCell(cargodtlscell6);
            table.AddCell(cargodtlscell7);
            table.AddCell(cargodtlscell8);

            Phrase cargodtlsphrase9 = new Phrase();
            cargodtlsphrase9.Add(new Chunk("Cargo Available from", normalFont));
            cargodtlsphrase9.Add(Chunk.NEWLINE);
            cargodtlsphrase9.Add(Chunk.NEWLINE);
            cargodtlsphrase9.Add(new Chunk(Convert.ToDateTime(QuoteDtls.Rows[0]["CARGOAVAILABLEFROM"]).ToString("dd-MMM-yyyy"), boldFont));
            cargodtlsphrase9.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell9 = new PdfPCell(cargodtlsphrase9);
            cargodtlscell9.Colspan = 2;
            cargodtlscell9.Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            cargodtlscell9.PaddingBottom = 5;


            Phrase cargodtlsphrase10 = new Phrase();
            cargodtlsphrase10.Add(new Chunk("Origin Customs Clearance by", normalFont));
            cargodtlsphrase10.Add(Chunk.NEWLINE);
            cargodtlsphrase10.Add(Chunk.NEWLINE);
            cargodtlsphrase10.Add(new Chunk(QuoteDtls.Rows[0]["ORIGINCUSTOMSCLEARANCEBY"].ToString(), boldFont));
            cargodtlsphrase10.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell10 = new PdfPCell(cargodtlsphrase10);
            cargodtlscell10.Colspan = 2;
            cargodtlscell10.Border = PdfPCell.BOTTOM_BORDER;
            cargodtlscell10.PaddingBottom = 5;

            Phrase cargodtlsphrase11 = new Phrase();
            cargodtlsphrase11.Add(new Chunk("Cargo Deliver to", normalFont));
            cargodtlsphrase11.Add(Chunk.NEWLINE);
            cargodtlsphrase11.Add(Chunk.NEWLINE);
            //cargodtlsphrase11.Add(new Chunk(Convert.ToDateTime(QuoteDtls.Rows[0]["CARGODELIVERYBY"]).ToString("dd-MMM-yyyy"), boldFont));
            cargodtlsphrase11.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell11 = new PdfPCell(cargodtlsphrase11);
            cargodtlscell11.Colspan = 2;
            cargodtlscell11.Border = PdfPCell.BOTTOM_BORDER;
            cargodtlscell11.PaddingBottom = 5;

            Phrase cargodtlsphrase12 = new Phrase();
            cargodtlsphrase12.Add(new Chunk("Destination Customs Clearance by", normalFont));
            cargodtlsphrase12.Add(Chunk.NEWLINE);
            cargodtlsphrase12.Add(Chunk.NEWLINE);
            cargodtlsphrase12.Add(new Chunk(QuoteDtls.Rows[0]["DESTINATIONCUSTOMSCLEARANCEBY"].ToString(), boldFont));
            cargodtlsphrase12.Add(Chunk.NEWLINE);
            PdfPCell cargodtlscell12 = new PdfPCell(cargodtlsphrase12);
            cargodtlscell12.Colspan = 2;
            cargodtlscell12.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            cargodtlscell12.PaddingBottom = 5;

            table.AddCell(cargodtlscell9);
            table.AddCell(cargodtlscell10);
            table.AddCell(cargodtlscell11);
            table.AddCell(cargodtlscell12);


            PdfPCell specialinstrlblcell = new PdfPCell(new Phrase(new Chunk("Special Instruction", boldFont)));
            specialinstrlblcell.Colspan = 8;
            specialinstrlblcell.PaddingBottom = 5;
            table.AddCell(specialinstrlblcell);

            PdfPCell specialinstrcell = new PdfPCell(new Phrase(new Chunk(QuoteDtls.Rows[0]["SPECIALINSTRUCTIONS"].ToString(), normalFont)));
            specialinstrcell.Colspan = 8;
            specialinstrcell.PaddingBottom = 5;
            specialinstrcell.FixedHeight = 80f;
            table.AddCell(specialinstrcell);

            doc.Add(table);
            doc.Close();

            byte[] bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }

            //Insert same data into db : Anil G
            SSPDocumentsModel sspdoc = new SSPDocumentsModel();
            sspdoc.QUOTATIONID = Convert.ToInt64(QuotaionID);
            sspdoc.DOCUMNETTYPE = "BKGCONF";
            sspdoc.FILENAME = "BookingConfirmation_" + OperationJobNumber.ToString() + ".pdf";
            sspdoc.FILEEXTENSION = "application/pdf";
            sspdoc.FILESIZE = bytes.Length;
            sspdoc.FILECONTENT = bytes;
            sspdoc.JOBNUMBER = Convert.ToInt64(jobnumber);
            sspdoc.DocumentName = ReportName;
            DBFactory.Entities.SSPDocumentsEntity docentity = Mapper.Map<SSPDocumentsModel, DBFactory.Entities.SSPDocumentsEntity>(sspdoc);
            JobBookingDbFactory mjobFactory = new JobBookingDbFactory();

            Guid obj = Guid.NewGuid();
            string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();

            mjobFactory.SaveDocuments(docentity, CreatedBy, "System Generated", obj.ToString(), EnvironmentName);

            return bytes;
        }

        public class Formattingpdf
        {
            public Font Heading4 { get; private set; }
            public Font Heading5 { get; private set; }
            public Font Heading6 { get; private set; }
            public Font Heading7 { get; private set; }
            public Font TableCaption7 { get; private set; }
            public Font Heading8 { get; private set; }
            public Font Heading9 { get; private set; }
            public Font Normal { get; private set; }
            public Font NormalRedColor { get; private set; }
            public Font Normal6 { get; private set; }
            public Font NormalItalic6 { get; private set; }
            public Font NormalItalic7 { get; private set; }
            public Font NormalBold { get; private set; }
            public Font BoldForQuoteTotal { get; private set; }

            public BaseColor TableHeadingBgColor { get; private set; }
            public BaseColor TableFooterBgColor { get; private set; }
            public BaseColor TableBorderColor { get; private set; }
            public BaseFont chinesebasefont { get; private set; }
            public BaseFont koreanbasefont { get; private set; }

            public FontSelector selectorTableCaption { get; private set; }
            public FontSelector selectorTableHeading { get; private set; }
            public FontSelector selectorTableContent { get; private set; }

            public Font mChineseFontTabCaption { get; private set; }
            public Font mChineseFontTabContent { get; private set; }
            public Font mChineseFontTabContentItalic { get; private set; }
            public Font mKoreanFontTabCaption { get; private set; }
            public Font mKoreanFontTabContent { get; private set; }
            public Font mKoreanFontTabContentItalic { get; private set; }

            public BaseColor TableCaptionFontColour { get; private set; }
            public BaseColor FontColour { get; private set; }

            public string mQuotePdfOutputLangId { get; private set; }

            public Formattingpdf(string QuoteOutPutLanguage = "")
            {
                TableCaptionFontColour = new BaseColor(175, 40, 46);//(151, 71, 6)

                TableCaption7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, TableCaptionFontColour);
                Heading4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, Font.NORMAL, BaseColor.BLACK);
                Heading5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, Font.NORMAL, BaseColor.BLACK);
                Heading7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                Heading6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, Font.NORMAL, BaseColor.BLACK);
                Heading8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, Font.NORMAL, BaseColor.BLACK);
                Heading9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.NORMAL, BaseColor.BLACK);
                Normal = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
                NormalRedColor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.RED);
                Normal6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
                NormalItalic6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
                NormalItalic7 = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.ITALIC, BaseColor.BLACK);
                NormalBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                FontColour = new BaseColor(176, 41, 46);
                BoldForQuoteTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, Font.NORMAL, FontColour);
                TableHeadingBgColor = new BaseColor(215, 215, 215);
                TableFooterBgColor = new BaseColor(253, 233, 217);
                TableBorderColor = BaseColor.GRAY;
                mQuotePdfOutputLangId = QuoteOutPutLanguage;

                //For Chinese characters display
                mChineseFontTabCaption = new Font(chinesebasefont, 7, Font.BOLD, TableCaptionFontColour);
                mChineseFontTabContent = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mChineseFontTabContentItalic = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);

                //For Korean characters display
                mKoreanFontTabCaption = new Font(koreanbasefont, 7, Font.BOLD, TableCaptionFontColour);
                mKoreanFontTabContent = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mKoreanFontTabContentItalic = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);

                selectorTableCaption = new FontSelector();
                selectorTableCaption.AddFont(TableCaption7);
                selectorTableCaption.AddFont(mChineseFontTabCaption);
                selectorTableCaption.AddFont(mKoreanFontTabCaption);

                selectorTableContent = new FontSelector();
                selectorTableContent.AddFont(NormalItalic7);
                selectorTableContent.AddFont(mChineseFontTabContentItalic);
                selectorTableContent.AddFont(mKoreanFontTabContentItalic);

                selectorTableHeading = new FontSelector();
            }

            /// <summary>
            /// Document Heading
            /// </summary>
            /// <param name="headName">Tha Document Name</param>
            /// <param name="hAlign">The h Align</param>
            /// <param name="underline"></param>
            /// <returns></returns>
            ///
            public PdfPCell DocumentHeadingIncrease(string headName, int hAlign = Element.ALIGN_RIGHT, bool underline = true)
            {

                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading = new FontSelector();
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 13, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 13, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_LEFT, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            public PdfPCell DocumentHeading(string headName, int hAlign = Element.ALIGN_CENTER, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 10, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            /// <summary>
            /// Subs the heading cell.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <returns></returns>
            public PdfPCell TableCaption(string caption, int vAlign = Element.ALIGN_MIDDLE, int hAlign = Element.ALIGN_LEFT, bool isbold = true, bool IsColor = false, bool isBorder = false)
            {
                if (isbold)
                {
                    FontSelector selectorTableCaptionbold = new FontSelector();
                    Font TableChiHead7;
                    Font TableKorHead7;
                    if (IsColor == false)
                    {
                        selectorTableCaptionbold.AddFont(Heading7);
                        TableChiHead7 = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                        TableKorHead7 = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                        selectorTableCaptionbold.AddFont(TableChiHead7);
                        selectorTableCaptionbold.AddFont(TableKorHead7);
                    }
                    else
                    {
                        selectorTableCaptionbold.AddFont(TableCaption7);
                        Font TableChiCap7 = new Font(chinesebasefont, 9, Font.BOLD, TableCaptionFontColour);
                        Font TableKorCap7 = new Font(koreanbasefont, 9, Font.BOLD, TableCaptionFontColour);
                        selectorTableCaptionbold.AddFont(TableChiCap7);
                        selectorTableCaptionbold.AddFont(TableKorCap7);
                    }

                    return new PdfPCell(selectorTableCaptionbold.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
                else
                {
                    FontSelector selectorTabCaption = new FontSelector();
                    selectorTabCaption.AddFont(Normal6);
                    Font ChiTableNormal6 = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    Font KorTableNormal6 = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    selectorTabCaption.AddFont(ChiTableNormal6);
                    selectorTabCaption.AddFont(KorTableNormal6);
                    return new PdfPCell(selectorTabCaption.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true, bool isLowFont = false)
            {
                if (isLowFont)
                    return TableHeading(caption, Heading5, hAlign, underline);
                else
                    return TableHeading(caption, Heading6, hAlign, underline);
            }

            /// <summary>
            /// Tables the footer.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <param name="underline">if set to <c>true</c> [underline].</param>
            /// <returns></returns>
            public PdfPCell TableFooter(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
                mPdfCell.BackgroundColor = TableFooterBgColor;
                return mPdfCell;
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="headingFont">The heading font.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, Font headingFont, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                int padding = 3;
                var mChunk = new Chunk(caption, headingFont);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);

                FontSelector selectorTabHeading = new FontSelector();
                selectorTabHeading.AddFont(headingFont);
                Font ChiTabHeading = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabHeading = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabHeading.AddFont(ChiTabHeading);
                selectorTabHeading.AddFont(KorTabHeading);
                return new PdfPCell(selectorTabHeading.Process(mChunk.ToString())) { BorderColor = TableBorderColor, BackgroundColor = TableHeadingBgColor, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = 2, PaddingRight = 2 };//padding - 2
            }

            /// <summary>
            /// Tables Content
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCell(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabContentCell = new FontSelector();
                selectorTabContentCell.AddFont(Normal);
                Font ChiTabContentcell = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabContentcell = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabContentCell.AddFont(ChiTabContentcell);
                selectorTabContentCell.AddFont(KorTabContentcell);
                return new PdfPCell(selectorTabContentCell.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BorderWidth = 5, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            public PdfPCell TableContentCellNormalandItalic(string cellData, string cellItalicData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                Paragraph mPgh = new Paragraph();
                FontSelector selectorNormal = new FontSelector();
                selectorNormal.AddFont(Normal);
                Font fntChi1 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor1 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormal.AddFont(fntChi1);
                selectorNormal.AddFont(fntKor1);

                Phrase ph1 = selectorNormal.Process(cellData);
                FontSelector selectorNormItalic = new FontSelector();
                selectorNormItalic.AddFont(Normal);
                Font fntChi2 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor2 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormItalic.AddFont(fntChi2);
                selectorNormItalic.AddFont(fntKor2);
                Phrase ph2 = selectorNormItalic.Process(cellItalicData);
                mPgh.Add(ph1);
                if (!string.IsNullOrEmpty(cellItalicData))
                    mPgh.Add(ph2);
                return new PdfPCell(mPgh) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content Italic
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellItalic(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellItalic = new FontSelector();
                selectorTabConentcellItalic.AddFont(NormalItalic7);
                Font ChiContentcell = new Font(chinesebasefont, 9, Font.ITALIC, BaseColor.BLACK);
                Font KorContentcell = new Font(koreanbasefont, 9, Font.ITALIC, BaseColor.BLACK);
                selectorTabConentcellItalic.AddFont(ChiContentcell);
                selectorTabConentcellItalic.AddFont(KorContentcell);
                return new PdfPCell(selectorTabConentcellItalic.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content with red color
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellRedColor(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellRedColor = new FontSelector();
                selectorTabConentcellRedColor.AddFont(NormalRedColor);
                Font fntChiRed = new Font(chinesebasefont, 10, Font.NORMAL, BaseColor.RED);
                Font fntKorRed = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.RED);
                selectorTabConentcellRedColor.AddFont(fntChiRed);
                selectorTabConentcellRedColor.AddFont(fntKorRed);
                return new PdfPCell(selectorTabConentcellRedColor.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBold(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBold = new FontSelector();
                selectorTabConentcellBold.AddFont(NormalBold);
                Font fntChiBold = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                Font fntKorBold = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                selectorTabConentcellBold.AddFont(fntChiBold);
                selectorTabConentcellBold.AddFont(fntKorBold);
                return new PdfPCell(selectorTabConentcellBold.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold for Quotation Grand Total
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldForQuoteTotal(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBoldQT = new FontSelector();
                selectorTabConentcellBoldQT.AddFont(BoldForQuoteTotal);
                Font fntChiBorderQT = new Font(chinesebasefont, 10, Font.BOLD, FontColour);
                Font fntKorBorderQT = new Font(koreanbasefont, 10, Font.BOLD, FontColour);
                selectorTabConentcellBoldQT.AddFont(fntChiBorderQT);
                selectorTabConentcellBoldQT.AddFont(fntKorBorderQT);
                return new PdfPCell(selectorTabConentcellBoldQT.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding };
            }

            /// <summary>
            /// Table Content Cell Data Bold,Underline
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="underline"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldUnderline(string cellData, int hAlign = Element.ALIGN_LEFT, bool underline = false)
            {
                int padding = 2;
                var mChunk = new Chunk(cellData, Heading6);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                FontSelector selectorTabConentcellBoldline = new FontSelector();
                selectorTabConentcellBoldline.AddFont(Heading6);
                Font fntChiBoldline = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                Font fntKorBoldline = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                selectorTabConentcellBoldline.AddFont(fntChiBoldline);
                selectorTabConentcellBoldline.AddFont(fntKorBoldline);
                return new PdfPCell(selectorTabConentcellBoldline.Process(mChunk.ToString())) { Border = 0, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }



            ///// <summary>
            ///// Tables Content with border.
            ///// </summary>
            ///// <param name="caption">The caption.</param>
            ///// <param name="hAlign">The h align.</param>
            ///// <param name="underline">if set to <c>true</c> [underline].</param>
            ///// <returns></returns>
            //public PdfPCell TableContentBorder(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            //{
            //    var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
            //    mPdfCell.BackgroundColor = BaseColor.WHITE;
            //    return mPdfCell;
            //}



            ///// <summary>
            ///// Lines the separator.
            ///// </summary>
            ///// <returns></returns>
            public Chunk LineSeparator()
            {
                return new Chunk(new LineSeparator(4f, 100f, TableHeadingBgColor, Element.ALIGN_CENTER, -1));
            }

            public Chunk ChunkText(string caption, bool isColor = false)
            {
                if (isColor)
                    return new Chunk(caption, TableCaption7);
                else
                    return new Chunk(caption, Heading7);
            }



            #region Impersonate Code
            /// <summary>
            /// Object to change the user authticated
            /// </summary>

            public class UserImpersonation : IDisposable
            {
                /// <summary>
                /// Logon method (check athetification) from advapi32.dll
                /// </summary>
                /// <param name="lpszUserName"></param>
                /// <param name="lpszDomain"></param>
                /// <param name="lpszPassword"></param>
                /// <param name="dwLogonType"></param>
                /// <param name="dwLogonProvider"></param>
                /// <param name="phToken"></param>
                /// <returns></returns>
                [DllImport("advapi32.dll")]
                private static extern bool LogonUser(String lpszUserName,
                    String lpszDomain,
                    String lpszPassword,
                    int dwLogonType,
                    int dwLogonProvider,
                    ref IntPtr phToken);

                /// <summary>
                /// Close
                /// </summary>
                /// <param name="handle"></param>
                /// <returns></returns>
                [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
                public static extern bool CloseHandle(IntPtr handle);

                private WindowsImpersonationContext _windowsImpersonationContext;
                private IntPtr _tokenHandle;
                private string _userName;
                private string _domain;
                private string _passWord;

                const int LOGON32_PROVIDER_DEFAULT = 0;
                const int LOGON32_LOGON_INTERACTIVE = 2;

                /// <summary>
                /// Initialize a UserImpersonation
                /// </summary>
                /// <param name="userName"></param>
                /// <param name="domain"></param>
                /// <param name="passWord"></param>
                public UserImpersonation(string userName, string domain, string passWord)
                {
                    _userName = userName;
                    _domain = domain;
                    _passWord = passWord;
                }

                /// <summary>
                /// Valiate the user inforamtion
                /// </summary>
                /// <returns></returns>
                public bool ImpersonateValidUser()
                {
                    bool returnValue = LogonUser(_userName, _domain, _passWord,
                            LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                            ref _tokenHandle);

                    if (false == returnValue)
                    {
                        return false;
                    }

                    WindowsIdentity newId = new WindowsIdentity(_tokenHandle);
                    _windowsImpersonationContext = newId.Impersonate();
                    return true;
                }

                #region IDisposable Members

                /// <summary>
                /// Dispose the UserImpersonation connection
                /// </summary>
                public void Dispose()
                {
                    if (_windowsImpersonationContext != null)
                        _windowsImpersonationContext.Undo();
                    if (_tokenHandle != IntPtr.Zero)
                        CloseHandle(_tokenHandle);
                }

                #endregion
            }
            #endregion


        }

        public class ITextEvents : PdfPageEventHelper
        {
            public UserProfileEntity mUserModel { get; set; }
            public QuotationPreviewModel mUIModels { get; set; }
            public int z { get; set; }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                string format = "MMMM d, yyyy";
                CultureInfo ci = new CultureInfo("en-US");
                string TaxNum = "";
                if (z == 0)
                {

                    string FullName = (mUserModel.FIRSTNAME != null) ? mUserModel.FIRSTNAME + " " + mUserModel.LASTNAME : mUIModels.GuestName;
                    if (string.IsNullOrWhiteSpace(FullName))
                        FullName = mUserModel.USERID;
                    string Company = (mUserModel.COMPANYNAME != null) ? mUserModel.COMPANYNAME : mUIModels.GuestCompany;
                    string Address = (mUserModel.UAFULLADDRESS != null) ? mUserModel.UAFULLADDRESS : "";
                    string Phone = (mUserModel.WORKPHONE != null) ? (mUserModel.ISDCODE + " - " + mUserModel.WORKPHONE) : "";
                    string EMail = (mUserModel.USERID != null) ? mUserModel.USERID : mUIModels.GuestEmail;
                    if (!string.IsNullOrWhiteSpace(mUserModel.COUNTRYNAME))
                    {
                        if (mUserModel.COUNTRYID == Convert.ToDouble(Country.INDIA))
                        {
                            if (!string.IsNullOrEmpty(mUserModel.VATREGNUMBER))
                                TaxNum = "GST number : " + mUserModel.VATREGNUMBER;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(mUserModel.VATREGNUMBER))
                                TaxNum = "Indirect Tax number : " + mUserModel.VATREGNUMBER;
                        }
                    }
                    PdfPTable mUserTab = new PdfPTable(2);
                    mUserTab.WidthPercentage = 100;
                    mUserTab.SpacingAfter = 0;
                    mUserTab.DefaultCell.Padding = 0;
                    mUserTab.DefaultCell.Border = 0;
                    mUserTab.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                    float[] tblUserwidths = new float[] { 50, 50 };
                    mUserTab.SetWidths(tblUserwidths);

                    int mUserInfoAlign = Element.ALIGN_LEFT;
                    PdfPTable mHeaderUserTable = new PdfPTable(1);
                    mHeaderUserTable.WidthPercentage = 100;
                    mHeaderUserTable.TotalWidth = 230;
                    mHeaderUserTable.DefaultCell.Padding = 0;
                    mHeaderUserTable.HorizontalAlignment = 0;
                    mUserTab.AddCell(mHeaderUserTable);

                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell("For,", mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(FullName, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Company, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Address))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Address, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Phone))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Phone, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(EMail, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(TaxNum, mUserInfoAlign));
                    mUserTab.AddCell(mHeaderUserTable);
                    mHeaderUserTable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 35), writer.DirectContent);
                }
                z++;
                string mQuotationNumber = mUIModels.QuotationNumber;
                string mDateCreated = mUIModels.DateofEnquiry.ToString();
                string mDateofValidity = mUIModels.DateOfValidity.ToString();
                string mIssueDate = "Issue Date" + " : " + ((!String.IsNullOrEmpty(mDateCreated)) ? Convert.ToDateTime(mDateCreated).ToString(format, ci) : string.Empty);
                string mValidTillDate = "Quote valid until" + " : " + ((!String.IsNullOrEmpty(mDateofValidity)) ? Convert.ToDateTime(mDateofValidity).ToString(format, ci) : string.Empty);
                string mShipmentDate = string.Empty;
                string DateOfShipment = mUIModels.DateofShipment.ToString();
                if (String.IsNullOrEmpty(DateOfShipment))
                    mShipmentDate = "DateOfShipment" + " : " + ((!String.IsNullOrEmpty(DateOfShipment)) ? Convert.ToDateTime(DateOfShipment).ToString(format, ci) : string.Empty);

                PdfPTable mHeadermaintable = new PdfPTable(3);
                mHeadermaintable.WidthPercentage = 100;
                mHeadermaintable.SpacingAfter = 0;
                mHeadermaintable.DefaultCell.Padding = 0;
                mHeadermaintable.DefaultCell.Border = 0;
                mHeadermaintable.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                float[] tblwidths = new float[] { 50, 29, 36 };
                mHeadermaintable.SetWidths(tblwidths);

                int mQuoteInfoAlign = Element.ALIGN_LEFT;

                //Company Logo
                PdfPTable mHeaderImageTable = new PdfPTable(1);
                mHeaderImageTable.WidthPercentage = 100;
                mHeadermaintable.TotalWidth = 500;
                mHeaderImageTable.DefaultCell.Padding = 0;
                mHeaderImageTable.DefaultCell.Border = 0;
                PdfPCell imgcel = ImageCell("~/Images/AgilityPrint.png", 48f, mQuoteInfoAlign);
                imgcel.Border = 0;
                imgcel.PaddingBottom = 3;
                mHeaderImageTable.AddCell(imgcel);
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell(mHeaderImageTable);
                mHeadermaintable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 10), writer.DirectContent);
            }

            public static PdfPCell ImageCell(string path, float scale, int align)
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(System.Web.Hosting.HostingEnvironment.MapPath(path));
                image.ScalePercent(scale);
                PdfPCell cell = new PdfPCell(image);
                cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
                cell.HorizontalAlignment = align;
                cell.PaddingBottom = 0f;
                cell.PaddingTop = 0f;
                return cell;
            }
        }
        public static PdfPTable ChargesTableModel(IEnumerable<QuotationChargePreviewModel> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Where(x => x.ISCHARGEINCLUDED == true).Sum(sum => Convert.ToDouble(sum.RoundedTotalPrice)).ToString()); //.ToString("N2");
                QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                var roundedChargesTotal = mFactoryRound.RoundedValue(Convert.ToString(ChargesTotal), CurrencyId);
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(roundedChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + roundedChargesTotal, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        if (QC.ISCHARGEINCLUDED)
                        {
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.ChargeName, Element.ALIGN_LEFT)));
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.RoundedTotalPrice, Element.ALIGN_RIGHT)));
                        }
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception ex)
            {
                PdfPTable mChargebordertable = new PdfPTable(1);
                return mChargebordertable;
            }
        }

        public static PdfPTable ChargesTableAddModel(IEnumerable<PaymentCharges> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Where(x => x.CHARGESOURCE.ToLower() == "f" && (x.CHARGESTATUS != null && (x.CHARGESTATUS.ToLower() == "pay now" || x.CHARGESTATUS.ToLower() == "pay later"))).Sum(sum => sum.NETAMOUNT).ToString());
                if (ChargesTotal.ToString("N2") != "0.00")
                {
                    QuotationDBFactory mFactoryRound = new QuotationDBFactory();
                    ChargeSetTotal = Convert.ToString(ChargesTotal);
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + mFactoryRound.RoundedValue(Convert.ToString(ChargesTotal), CurrencyId), Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.CHARGENAME, Element.ALIGN_LEFT)));
                        mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mFactoryRound.RoundedValue(Convert.ToString(QC.NETAMOUNT), CurrencyId), Element.ALIGN_RIGHT)));
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception ex)
            {
                PdfPTable mChargebordertable = new PdfPTable(1);
                return mChargebordertable;
            }
        }
        #endregion PDF's

        #region Helpers
        private HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }

        private HttpResponseMessage ErrorResponse()
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = "Something Went Wrong"
                })
            };
        }

        private HttpResponseMessage ErrorResponse(dynamic error)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = HttpStatusCode.ExpectationFailed,
                    Error = error
                })
            };
        }

        private string EncryptId(string plainText)
        {
            StringBuilder Result = new StringBuilder();

            char[] Letters = { 'q', 'a', 'm', 'w', 'v', 'd', 'x', 'n' };

            string[] mul = plainText.Split('?');
            for (int jk = 0; jk < mul.Length; jk++)
            {
                if (jk == 0)
                    Result.Append("?" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
                else
                    Result.Append("&" + Letters[jk] + "=" + Encryption.Encrypt(mul[jk]));
            }
            string JResult = Result.ToString();
            return JResult;
        }

        #endregion Helpers
    }

    #region Models

    public class StripeModel
    {
        public StripeModel()
        {
            RequestedAmount = 0;
        }
        public int JobNumber { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string StripeSourceId { get; set; }
        public string CouponCode { get; set; }
        public string UserId { get; set; }
        public string PaySource { get; set; }
        public double RequestedAmount { get; set; }
        public string HsbcMode { get; set; }
    }

    public class StripeSaveModel : StripeModel
    {
        public string QuotationId { get; set; }
        public string ReceiptHeaderId { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string ResultStatus { get; set; }
        public string Reason { get; set; }
        public string FirstName { get; set; }
        public string AdditionalChargeStatus { get; set; }
        public decimal AdditionalChargeAmount { get; set; }
        public QuotationPreviewModel mUIModels { get; set; }
        public string CRMEmail { get; set; }
        public double PayAmount { get; set; }
    }

    public class CURRENCYLIST
    {
        public string Currency { get; set; }
    }

    public class BENEFECIARYINFO
    {
        public List<SSPEntityListEntity> BENEFECIARYDETAILS { get; set; }
        public List<CURRENCYLIST> CURRENCYLIST { get; set; }
    }

    public class PAYATBRANCHINFO
    {
        public List<SSPBranchListEntity> BRANCHADDRESS { get; set; }
        public List<CURRENCYLIST> CURRENCYLIST { get; set; }
    }

    public class CouponModel
    {
        [Required(ErrorMessage = "UserId value is required.")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "JobNumber value is required.")]
        public string JobNumber { get; set; }
        [Required(ErrorMessage = "CouponCode value is required.")]
        public string CouponCode { get; set; }
    }

    public class HsbcFrontResponse
    {
        public string pay_type { get; set; }
        public string order_st { get; set; }
        public string fy_ssn { get; set; }
        public string order_pay_code { get; set; }
        public string mchnt_cd { get; set; }
        public string order_amt { get; set; }
        public string user_id { get; set; }
        public string card_no { get; set; }
        public string RSA { get; set; }
        public string order_date { get; set; }
        public string order_id { get; set; }
        public string order_pay_msg { get; set; }
    }

    #endregion Models
}
