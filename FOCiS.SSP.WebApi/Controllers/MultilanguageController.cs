﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using FOCiS.SSP.Models.MultiLanguage;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class MultilanguageController : ApiController
    {
        [Route("api/Multilanguage/GetDataByMultiLanguage")]
        [HttpGet]
        public async Task<RootObject> GetMovementTypesByLanguage(string languageType)
        {
            var SourceItems = new RootObject();
            string path = HttpRuntime.AppDomainAppPath;
            try
            {
                if (!string.IsNullOrEmpty(languageType))
                {
                    if (languageType == "zh-hk")
                    {
                        using (StreamReader r = new StreamReader(path + "/MultiLanguageJsonData/ChineseLanguageData.json"))
                        {
                            string json = r.ReadToEnd();
                            SourceItems = JsonConvert.DeserializeObject<RootObject>(json);
                            r.Close();
                        }
                        SourceItems = await Task.Run(() => SourceItems);
                    }
                    if (languageType == "de")
                    {
                        using (StreamReader r = new StreamReader(path + "/MultiLanguageJsonData/GermanLanguageData.json"))
                        {
                            string json = r.ReadToEnd();
                            SourceItems = JsonConvert.DeserializeObject<RootObject>(json);
                            r.Close();
                        }
                        SourceItems = await Task.Run(() => SourceItems);
                    }

                }
                else
                {
                    SourceItems = null;
                }
            }
            catch (Exception ex) { 
            
            }
            return SourceItems;
        }
    }
}
