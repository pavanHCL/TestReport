﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FOCiS.SSP.Models.QM;
using FOCiS.SSP.DBFactory;
using System.Data;
using Newtonsoft.Json;
using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Factory;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.draw;
using System.Globalization;
using FOCiS.SSP.Models.UserManagement;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using FOCiS.SSP.WebApi.Helpers;
using FOCiS.SSP.Models.CacheManagement;
using FOCiS.SSP.Web.UI.Controllers;
using Newtonsoft.Json.Linq;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class QuotationController : BaseController
    {
        private static CultureInfo culture = (CultureInfo)CultureInfo.CreateSpecificCulture("en-US");
        private static string format = "{0:N2}";
        private static Decimal mQuoteChargesTotal = 0;
        private static string PreferredCurrencyId = string.Empty;
        private static QuotationPreviewModel DUIModels;
        private static UserProfileEntity DUserModel;
        private static byte[] bytes;
        private static int PDFpage = 0;
        string body = string.Empty;
        string subject = string.Empty;
        string mPlaceOfReceipt = string.Empty;
        string mPlaceOfDelivery = string.Empty;
        string mPRLableName = string.Empty;
        string mPDLableName = string.Empty;
        string cargodetails = string.Empty;
        string productname = string.Empty;

        private readonly QuotationDBFactory _QFactory;

        public QuotationController()
        {
            _QFactory = new QuotationDBFactory();
        }
        public IEnumerable<QuotationModel> Get()
        {
            return null;
        }

        public IHttpActionResult GetById(int id)
        {
            return null;
        }

        #region Get API'S
        [Route("api/Quotation/CheckSanctionedCountry")]
        [HttpGet]
        public dynamic CheckSanctionedCountry(string FromCountryId, string ToCountryId)
        {
            try
            {
                if (string.IsNullOrEmpty(FromCountryId)) { FromCountryId = "0"; }
                if (string.IsNullOrEmpty(ToCountryId)) { ToCountryId = "0"; }
                MDMDataFactory mFactory = new MDMDataFactory();
                ResultsEntity<Status<Int64>> mFactoryEntity = mFactory.CheckSanctionedCountry(FromCountryId, ToCountryId);
                return Ok(mFactoryEntity);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "CheckSanctionedCountry", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/Quotation/GetMovementTypes")]
        [HttpGet]
        public IHttpActionResult GetMovementTypes()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("text", typeof(string));

            dt.Rows.Add(1, "Door to door");
            dt.Rows.Add(2, "Port to port");
            dt.Rows.Add(3, "Door to port");
            dt.Rows.Add(4, "Port to door");

            MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select2Entity<string>> mFactoryEntity = mFactory.GetResult(dt);
            return Ok(mFactoryEntity);
        }

        [Route("api/Quotation/GetPackageTypes")]
        [HttpGet]
        public IHttpActionResult GetPackageTypes()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("text", typeof(string));
            dt.Columns.Add("code", typeof(string));
            dt.Columns.Add("ISZipCode", typeof(string));

            dt.Rows.Add(1, "Container(s)", "CNT", "");
            dt.Rows.Add(2, "Pallet(s)", "BPAL", "");
            dt.Rows.Add(3, "Skid(s)", "SKID", "");
            dt.Rows.Add(4, "Crate(s)", "CRAT", "");
            dt.Rows.Add(5, "Box(es)", "BOX", "");
            dt.Rows.Add(6, "Carton(s)", "CART", "");
            dt.Rows.Add(7, "Case(s)", "CASE", "");
            dt.Rows.Add(8, "Package(s)", "PACK", "");
            dt.Rows.Add(9, "Drum(s)", "DRUM", "");
            dt.Rows.Add(10, "Barrel(s)", "BARR", "");
            dt.Rows.Add(11, "Bale(s)", "BALN", "");
            dt.Rows.Add(12, "Bag(s)", "BAG", "");
            dt.Rows.Add(13, "Roll(s)", "ROLL", "");

            MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select3Entity<string>> mFactoryEntity = mFactory.GetResult3(dt);
            return Ok(mFactoryEntity);
        }


        [Route("api/Quotation/GetLengthUOMData")]
        [HttpGet]
        public IHttpActionResult GetLengthUOMData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("text", typeof(string));
            dt.Columns.Add("code", typeof(string));
            dt.Columns.Add("cap", typeof(string));

            dt.Rows.Add(98, "CM", "CENTIMETER", "0.01");
            dt.Rows.Add(99, "IN", "INCH", "0.0254");
            dt.Rows.Add(100, "M", "METER", "1");
            dt.Rows.Add(101, "MM", "MILLIMETER", "0.001");

            MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select4Entity<string>> mFactoryEntity = mFactory.GetResult4(dt);
            return Ok(mFactoryEntity);
        }

        [Route("api/Quotation/GetMultiJson")]
        [HttpGet]
        public IHttpActionResult GetMultiJson()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("text", typeof(string));
            dt.Columns.Add("code", typeof(string));
            dt.Columns.Add("cap", typeof(string));

            dt.Rows.Add(98, "CM", "CENTIMETER", "0.01");
            dt.Rows.Add(99, "IN", "INCH", "0.0254");
            dt.Rows.Add(100, "M", "METER", "1");
            dt.Rows.Add(101, "MM", "MILLIMETER", "0.001");


            DataTable dt1 = new DataTable();
            dt1.Columns.Add("id", typeof(string));
            dt1.Columns.Add("text", typeof(string));
            dt1.Columns.Add("code", typeof(string));
            dt1.Columns.Add("cap", typeof(string));

            dt1.Rows.Add(95, "KG", "", "0");
            dt1.Rows.Add(96, "LB", "", "0.45359237");
            dt1.Rows.Add(97, "TON", "", "1000");


            DataTable dtPacka = new DataTable();
            dtPacka.Columns.Add("id", typeof(string));
            dtPacka.Columns.Add("text", typeof(string));
            dtPacka.Columns.Add("code", typeof(string));
            dtPacka.Columns.Add("cap", typeof(string));

            dtPacka.Rows.Add(1, "Container(s)", "CNT", "");
            dtPacka.Rows.Add(2, "Pallet(s)", "BPAL", "");
            dtPacka.Rows.Add(3, "Skid(s)", "SKID", "");
            dtPacka.Rows.Add(4, "Crate(s)", "CRAT", "");

            dtPacka.Rows.Add(5, "Case(s)", "CASE", "");
            dtPacka.Rows.Add(6, "Package(s)", "PACK", "");
            dtPacka.Rows.Add(7, "Drum(s)", "DRUM", "");
            dtPacka.Rows.Add(8, "Barrel(s)", "BARR", "");
            dtPacka.Rows.Add(9, "Bale(s)", "BALN", "");
            dtPacka.Rows.Add(10, "Bag(s)", "BAG", "");
            dtPacka.Rows.Add(11, "Roll(s)", "ROLL", "");
            dt.Rows.Add(12, "Carton(s)", "CART", "");

            MDMDataFactory mFactory = new MDMDataFactory();
            MultipleEntity<Select4Entity<string>> mFactoryEntity = mFactory.GetResultMul(dt, dt1, dtPacka);
            return Ok(mFactoryEntity);
        }

        [Route("api/Quotation/GetWeightUOMData")]
        [HttpGet]
        public IHttpActionResult GetWeightUOMData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("text", typeof(string));
            dt.Columns.Add("code", typeof(string));
            dt.Columns.Add("cap", typeof(string));

            dt.Rows.Add(95, "KG", "", "0");
            dt.Rows.Add(96, "LB", "", "0.45359237");
            dt.Rows.Add(97, "TON", "", "1000");

            MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select4Entity<string>> mFactoryEntity = mFactory.GetResult4(dt);
            return Ok(mFactoryEntity);
        }

        [Route("api/Quotation/GetContainerData")]
        [HttpGet]
        public IHttpActionResult GetContainerData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("text", typeof(string));
            dt.Columns.Add("code", typeof(string));
            dt.Columns.Add("cap", typeof(string));
            dt.Columns.Add("MaxVol", typeof(string));
            dt.Columns.Add("MaxWgt", typeof(string));
            dt.Columns.Add("AvgWgt", typeof(string));

            dt.Rows.Add(1, "20' General Purpose", "22G0",
                "Maximum volume : 33.0 cbm, Maximum weight of 25,000 kg and Average weight of 14,000 Kg  per Container.", "33.0", "25,000", "14,000");
            dt.Rows.Add(11, "40' General Purpose", "42G0",
                "Maximum volume : 67.3 cbm, Maximum weight of 27,600 Kg and Average weight of 16,000 Kg  per Container.", "67.3", "27,600", "16,000");
            dt.Rows.Add(20, "40' High Cube", "45G0",
                "Maximum volume : 76.4 cbm, Maximum weight of 28,560 Kg and Average weight of 17,000 Kg  per Container.", "76.4", "28,560", "17,000");

            MDMDataFactory mFactory = new MDMDataFactory();
            PagedResultsEntity<Select7Entity<string>> mFactoryEntity = mFactory.GetResult7(dt);
            return Ok(mFactoryEntity);
        }

        [Route("api/Quotation/GetAddressBook")]
        [HttpPost]
        public dynamic GetAddressBook([FromBody]dynamic modal)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();

                DataTable dt = mFactory.GetAddressBook(Convert.ToString(modal.CreatedBy));

                if (dt.Rows.Count > 0)
                {
                    MDMDataFactory mFactorys = new MDMDataFactory();
                    ResultsEntity<SelectEntity<string>> mFactoryEntity = mFactorys.GetR(dt);

                    return Ok(mFactoryEntity);


                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "No Data"
                        })
                    };
                }
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "GetAddressBook", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }
        public static List<Dictionary<string, object>> ReturnJsonResult(DataTable dt)
        {
            Dictionary<string, object> temp_row;
            List<Dictionary<string, object>> trows = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                temp_row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    temp_row.Add(col.ColumnName, dr[col]);
                }
                trows.Add(temp_row);
            }
            return trows;
        }

        [Route("api/Quotation/GetSurveyQuestions")]
        [HttpGet]
        public dynamic GetSurveyQuestions()
        {
            var res = new
            {
                StatusCode = 200,
                Data = new object[]{
                    new { ID = 1115, QUESTION = "I don't currently need to ship freight" },
                    new { ID = 1116, QUESTION = "Quote is not competitive" },
                    new { ID = 1117, QUESTION = "I am just comparison shopping" },
                    new { ID = 1118, QUESTION = "Not comfortable shipping this way" },
                    new { ID = 1119, QUESTION = "Other, please specify:" }
                }
            };
            return res;
        }

        [Route("api/Quotation/CheckQuoteBooked")]
        [HttpGet]
        public dynamic CheckQuoteBooked(int id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            int QuoteBooked = mFactory.CheckQuoteBooked(Convert.ToInt32(id));
            var res = new { NOOFBOOKINGS = QuoteBooked };
            return BaseResponse(res, HttpStatusCode.OK);
        }


        [Route("api/Quotation/GetAmazonFCStates")]
        [HttpGet]
        public dynamic GetAmazonFCStates()
        {
            QuotationDBFactory UD = new QuotationDBFactory();

            ResultsEntity<AmazonFCStates> mFactoryEntity = UD.GetAmazonFCStates();

            return Ok(mFactoryEntity);

        }
        [Route("api/Quotation/GetAmazonFBAWarehouses")]
        [HttpGet]
        public dynamic GetAmazonFBAWarehouses(string SUBDIVISIONID)
        {
            QuotationDBFactory UD = new QuotationDBFactory();

            ResultsEntity<AmazonFBAWAREHOUSES> mFactoryEntity = UD.GetAmazonFBAWAREHOUSES(SUBDIVISIONID);

            return Ok(mFactoryEntity);

        }

        [Route("api/Quotation/GetAllProductClassifications")]
        [HttpGet]
        public async Task<List<ProductClassification>> GetAllProductClassifications(string search)
        {

            var SourceItems = new List<ProductClassification>();
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    string path = HttpRuntime.AppDomainAppPath;
                    using (StreamReader r = new StreamReader(path + "/JsonData/ProductClassificationList.json"))
                    {
                        string json = r.ReadToEnd();
                        SourceItems = JsonConvert.DeserializeObject<List<ProductClassification>>(json);
                        r.Close();
                    }
                    if (search != "")
                        SourceItems = await Task.Run(() => SourceItems

                                    .Where(o => o.text.ToLowerInvariant().Contains(search.ToLowerInvariant())).ToList());


                }

            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "GetAllProductClassifications", ex.Message.ToString(), ex.StackTrace.ToString());
            }
            return SourceItems;
        }


        #endregion

        #region POST API'S
        /// <summary>
        /// Quote List 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("api/Quotation/GetSavedQuoteList")]
        [HttpPost]
        public dynamic GetSavedQuoteList([FromBody]dynamic request)
        {
            try
            {
                string list = ""; string status = ""; int pageno = 1;

                if (request.list != null) list = request.List.Value; if (request.Status != null) status = request.Status.Value;

                if (list == null || list == "") list = "QuoteList"; if (status == null || status == "") status = "ACTIVE";

                if (request.PageNo.Value != null) pageno = Convert.ToInt32(request.PageNo.Value);

                QuotationDBFactory mFactory = new QuotationDBFactory();
                var mQuoteList = new List<DBFactory.Entities.QuoteJobListEntity>();

                DBFactory.Entities.QuoteStatusCountEntity mDBStatusEntity = mFactory.GetQuoteStatusCount(request.Email.Value, Convert.ToString(request.SearchString).ToUpper(), status);

                if (status.ToUpperInvariant() == "NOMINATIONS")
                {
                    mQuoteList = mFactory.GetOnlySavedTemplates(request.Email.Value, status, list, pageno, Convert.ToString(request.SearchString).ToUpper(), "DESC");
                }
                else
                {
                    mQuoteList = mFactory.GetOnlySavedQuotes(request.Email.Value, status, list, pageno, Convert.ToString(request.SearchString).ToUpper(), "DESC");
                }
                ListSavedQuotesEntity SQ = new ListSavedQuotesEntity();
                SQ.QuotesTabCount = mDBStatusEntity;
                SQ.SavedQuotes = mQuoteList;

                return Ok(SQ);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "GetSavedQuoteList", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }


        [Route("api/Quotation/SaveQuoteV1")]
        [HttpPost]
        public dynamic SaveQuoteV1([FromBody] QuotationModel model)
        {
            return SafeRun<dynamic>(() =>
            {
                return IsValidState(model);
            });
        }

        private dynamic IsValidState(QuotationModel model)
        {
            if (model == null)
                return BaseResponse("This is not valid input, Something went wrong with your request.", HttpStatusCode.ExpectationFailed);
            if (model.OriginPlaceId == 0 || model.DestinationPlaceId == null || model.DestinationPlaceId == 0)
                return BaseResponse("This is not valid input, OriginPlaceID and DestinationPlaceID can't be empty.", HttpStatusCode.ExpectationFailed);
            if (model.OCountryId == model.DCountryId)
                return BaseResponse("Origin and destination countries should be different.", HttpStatusCode.ExpectationFailed);
            if (model.ShipmentItems == null || model.ShipmentItems.Count() < 1)
                return BaseResponse("ShipmentItems missing, Need atleat one shipmentitem for processing.", HttpStatusCode.ExpectationFailed);
            if (string.IsNullOrEmpty(model.ShipmentDescription))
                return BaseResponse("This is not valid input, Shipment description can't be empty.", HttpStatusCode.ExpectationFailed);
            if (model.CREATEDBY.ToUpper() == "GUEST")
            {
                Dictionary<string, string> DICStatus = CheckISUserExistsAndQuoteLimit(model.GuestEmail);

                if (DICStatus["UserExistStatus"] == "true")
                {
                    return BaseResponse("User already exists, please login with your credentials.", HttpStatusCode.ExpectationFailed);
                }
                else if (DICStatus["NoofQuotes"] == "5")
                {
                    return BaseResponse("Daily Quote Limit Reached !", HttpStatusCode.ExpectationFailed);
                }
            }

            if (model.CREATEDBY.ToUpper() != "GUEST")
                if (!string.IsNullOrEmpty(model.GuestEmail) || !string.IsNullOrEmpty(model.GuestName) || !string.IsNullOrEmpty(model.GuestLastName) || !string.IsNullOrEmpty(model.GuestCompany))
                    return BaseResponse("Guest fields should be empty for logged-in users.", HttpStatusCode.ExpectationFailed);

            if (!string.IsNullOrEmpty(model.QuoteSharedBy) && string.IsNullOrEmpty(model.CREATEDBY))
                if (string.IsNullOrEmpty(model.CollaboratedShipperMail))
                    return BaseResponse("Collaborated Shiper EmailId can't be empty!", HttpStatusCode.ExpectationFailed);

            if (string.IsNullOrEmpty(model.QuoteSource))
                return BaseResponse("QuoteSource can't be empty!", HttpStatusCode.ExpectationFailed);

            return QuoteLogic(model);
        }

        private dynamic QuoteLogic(QuotationModel model)
        {
            decimal TotalGrossWeight = 0;

            //-------------Dims Fun---------------
            if (!string.IsNullOrEmpty(model.QuoteSharedBy) && string.IsNullOrEmpty(model.CREATEDBY))
            {

                if (!model.IsDimsModified)
                {
                    Sendmail_NoCollaborationQuote(model.QuoteSharedBy, model.CollaboratedShipperMail);
                    return BaseResponse("You have made no changes to Weight/Dimensions of your shipment. So, No updations have been made to your quote. A confirmation email has been sent to the requester.", HttpStatusCode.OK);
                }

                //Update Below param in dims case
                model.CREATEDBY = model.QuoteSharedBy;
            }

            FillPortPairs(model);
            model.IsOversizeCargo = IsOversizeCargo(model);

            foreach (var ShipmentItems in model.ShipmentItems)
            {
                if (ShipmentItems.PackageTypeId == 1)
                {
                    ShipmentItems.ItemHeight = 0;
                    ShipmentItems.ItemLength = 0;
                    ShipmentItems.TotalCBM = 0;
                    ShipmentItems.ItemWidth = 0;
                    if (ShipmentItems.WeightPerPiece == 0)
                    {
                        switch (ShipmentItems.ContainerCode)
                        {
                            case "22G0":
                                ShipmentItems.WeightPerPiece = 14000;
                                ShipmentItems.WeightTotal = 14000 * ShipmentItems.Quantity;
                                model.TotalGrossWeight = 14000 * ShipmentItems.Quantity;
                                break;
                            case "42G0":
                                ShipmentItems.WeightPerPiece = 16000;
                                ShipmentItems.WeightTotal = 16000 * ShipmentItems.Quantity;
                                model.TotalGrossWeight = 16000 * ShipmentItems.Quantity;
                                break;
                            case "45G0":
                                ShipmentItems.WeightPerPiece = 17000;
                                ShipmentItems.WeightTotal = 17000 * ShipmentItems.Quantity;
                                model.TotalGrossWeight = 17000 * ShipmentItems.Quantity;
                                break;
                        }
                        ShipmentItems.WeightUOMCode = "KG";
                        ShipmentItems.WeightUOMName = "KG";
                        ShipmentItems.WeightUOM = 95;
                    }
                    else
                    {
                        if (ShipmentItems.WeightUOM == 95)
                            TotalGrossWeight = TotalGrossWeight + (ShipmentItems.WeightPerPiece * ShipmentItems.Quantity);
                        else if (ShipmentItems.WeightUOM == 96)
                            TotalGrossWeight = TotalGrossWeight + ((ShipmentItems.WeightPerPiece * ShipmentItems.Quantity) * Convert.ToDecimal(0.45359237));
                        else if (ShipmentItems.WeightUOM == 97)
                            TotalGrossWeight = TotalGrossWeight + ((ShipmentItems.WeightPerPiece * ShipmentItems.Quantity) * 1000);

                        model.TotalGrossWeight = TotalGrossWeight;
                    }

                }
                else
                {
                    ShipmentItems.ContainerCode = null;
                    ShipmentItems.ContainerId = 0;
                    ShipmentItems.ContainerName = null;
                }
            }

            if (model.ProductId == ProductEnum.OceanFreight)
            {
                model.ProductName = "Ocean";
                model.ProductTypeId = model.ShipmentItems.Count(x => x.ContainerId > 0) > 0 ? 5 : 6;
                model.ProductTypeName = model.ShipmentItems.Count(x => x.ContainerId > 0) > 0 ? "FCL (NVOCC)" : "LCL (NVOCC)";
            }
            else
            {
                model.ProductTypeId = 8;
                model.ProductTypeName = "Premier";
                model.ProductName = "Air";
                model.PackageTypes = 0;
            }

            model.MovementTypeName = model.MovementTypeId.Value.GetType().GetMember(model.MovementTypeId.Value.ToString()).First().GetCustomAttribute<DisplayAttribute>().Name;
            DBFactory.Entities.QuotationEntity mUIModel = Mapper.Map<QuotationModel, DBFactory.Entities.QuotationEntity>(model);
            switch (model.MovementTypeId.Value)
            {
                case MovementTypeEnum.PorttoPort:
                    mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 28 : 30;
                    if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 1;
                    break;

                case MovementTypeEnum.DoortoPort:
                    mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 24 : 26;
                    if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 37;
                    break;

                case MovementTypeEnum.PorttoDoor:
                    mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 31 : 33;
                    if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 44;
                    break;

                default:
                    mUIModel.MOVEMENTTYPEID = 4;
                    break;
            }

            if ((mUIModel.PRODUCTID == 2 && mUIModel.DENSITYRATIO != 1000) || (mUIModel.PRODUCTID == 3 && mUIModel.DENSITYRATIO != 166.667))
            {
                if (mUIModel.PRODUCTID == 2)
                {
                    mUIModel.DENSITYRATIO = 1000;
                    mUIModel.VOLUMETRICWEIGHT = Math.Truncate((mUIModel.TOTALCBM * Convert.ToDecimal(mUIModel.DENSITYRATIO)) * 1000) / 1000;
                    mUIModel.CHARGEABLEWEIGHT = Math.Truncate((Math.Max(mUIModel.TOTALGROSSWEIGHT, mUIModel.VOLUMETRICWEIGHT)) * 1000) / 1000;
                    mUIModel.CHARGEABLEVOLUME = Math.Truncate((Math.Max(mUIModel.TOTALCBM, (mUIModel.TOTALGROSSWEIGHT / Convert.ToDecimal(mUIModel.DENSITYRATIO)))) * 1000) / 1000;
                    if (mUIModel.CHARGEABLEVOLUME < 1)
                    {
                        mUIModel.CHARGEABLEVOLUME = 1;
                    }
                }
                else
                {
                    mUIModel.DENSITYRATIO = 166.667;
                    mUIModel.VOLUMETRICWEIGHT = Math.Truncate((mUIModel.TOTALCBM * Convert.ToDecimal(mUIModel.DENSITYRATIO)) * 1000) / 1000;
                    mUIModel.CHARGEABLEWEIGHT = Math.Truncate((Math.Max(mUIModel.TOTALGROSSWEIGHT, mUIModel.VOLUMETRICWEIGHT)) * 1000) / 1000;
                    mUIModel.CHARGEABLEVOLUME = Math.Truncate((Math.Max(mUIModel.TOTALCBM, (mUIModel.TOTALGROSSWEIGHT / Convert.ToDecimal(mUIModel.DENSITYRATIO)))) * 1000) / 1000;
                }
            }

            if (model.ShipmentItems.Count(x => x.ContainerId > 0) > 0)
                mUIModel.PackageTypes = 2;
            else
                mUIModel.PackageTypes = 1;

            //Customer Specific Pricing functionality
            if (!string.IsNullOrEmpty(model.CREATEDBY))
            {
                long stakeholderId = _QFactory.GetStakeholderIdByUserId(model.CREATEDBY);
                if (stakeholderId != 0)
                {
                    mUIModel.CUSTOMERID = stakeholderId;
                }
            }
            //----------------Save call begins---------------
            long mQuotationId = _QFactory.InsertSave(mUIModel, "");

            return Quotemails(model, mQuotationId);
        }

        private dynamic Quotemails(QuotationModel model, long mQuotationId)
        {
            DBFactory.Entities.QuotationEntity mDBEntity = _QFactory.GetById(Convert.ToInt32(mQuotationId));

            if (!string.IsNullOrEmpty(model.QuoteSharedBy))
            {
                Sendmail_CollaborationQuote(model.QuoteSharedBy, model.CollaboratedShipperMail);
                UserDBFactory UserFactory = new UserDBFactory();
                UserFactory.NewNotification(5121, "New Quote Available for Your Shipment", "", 0, Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER, model.QuoteSharedBy);
            }

            if (mDBEntity.co2emission == 0)
            {
                double co2 = Co2EmissionCalculation(mDBEntity);
                mDBEntity.co2emission = Convert.ToDecimal(co2);
            }

            if (mDBEntity.CREATEDBY.ToUpper() != "GUEST" && ((mDBEntity.HASMISSINGCHARGES != 0) || (mDBEntity.HASMISSINGCHARGES == 0 && mDBEntity.GRANDTOTAL == 0))) // RATES NOT AVAILABLE & Zero Quote
            {
                int quoteid = Convert.ToInt32(mDBEntity.QUOTATIONID);
                _QFactory.RequestQuote(quoteid);
                SendMailForGSSCQuotes(mDBEntity.QUOTATIONNUMBER.ToString(), Convert.ToInt32(mDBEntity.ThresholdQty), Convert.ToInt32(mDBEntity.ISHAZARDOUSCARGO), Convert.ToInt32(mDBEntity.HASMISSINGCHARGES));
            }

            if (!string.IsNullOrEmpty(mDBEntity.GSSCREASON) && (mDBEntity.HASMISSINGCHARGES == 0))
            {
                _QFactory.InsertGSSCREASON(mDBEntity.QUOTATIONID, mDBEntity.GSSCREASON);

                int usercount = _QFactory.CheckUserRestriction(mDBEntity.CREATEDBY.ToUpper() == "GUEST" ? mDBEntity.GUESTEMAIL.ToUpper() : mDBEntity.CREATEDBY.ToUpper());
                if (usercount == 0)
                    SendMailForMissingChargesGSSCQuotes(mDBEntity);
            }
            if (mDBEntity.HASMISSINGCHARGES <= 0)
                SendmailtoNPC(mDBEntity);

            return QuotePreview(mDBEntity);
        }

        private dynamic QuotePreview(QuotationEntity mDBEntity)
        {
            List<QuotationChargeEntity> mUIOrg = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1118).ToList();
            List<QuotationChargeEntity> mUIDes = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1117).ToList();
            List<QuotationChargeEntity> mUIInter = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1116).ToList();
            List<QuotationChargeEntity> mUIAdditional = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 250001).ToList();

            List<QOrgChargeEntity> mUIOrgModel = Mapper.Map<List<QuotationChargeEntity>, List<QOrgChargeEntity>>(mUIOrg);
            List<QDesChargeEntity> mUIModelDes = Mapper.Map<List<QuotationChargeEntity>, List<QDesChargeEntity>>(mUIDes);
            List<QInterChargeEntity> mUIModelInter = Mapper.Map<List<QuotationChargeEntity>, List<QInterChargeEntity>>(mUIInter);
            List<QAdditionalChargeEntity> mUIModelAdditional = Mapper.Map<List<QuotationChargeEntity>, List<QAdditionalChargeEntity>>(mUIAdditional);

            mDBEntity.QOrgCharges = mUIOrgModel;
            mDBEntity.QDesCharges = mUIModelDes;
            mDBEntity.QInterCharges = mUIModelInter;
            mDBEntity.QAdditionalCharges = mUIModelAdditional;

            //Remove unnecessry items
            mDBEntity.Users = null;
            mDBEntity.TermAndConditions = null;
            mDBEntity.QuotationCharges = null;
            int decimalCount = _QFactory.GetDecimalPointByCurrencyCode(mDBEntity.PREFERREDCURRENCYID);
            mDBEntity.DECIMALCOUNT = decimalCount;
            if (mDBEntity == null)
                return BaseResponse("Something went wrong with quote preview.", HttpStatusCode.ExpectationFailed);
            return Ok(mDBEntity);
        }


        [Route("api/Quotation/SaveQuote")]
        [HttpPost]
        public dynamic SaveQuote([FromBody] QuotationModel model)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                if (model.ShipmentItems.Count() < 1)
                {
                    return BaseResponse("ShipmentItems missing", HttpStatusCode.ExpectationFailed);
                }
                if (model.OCountryId == model.DCountryId) return BaseResponse("Origin and destination countries should be different", HttpStatusCode.ExpectationFailed);

                if (model.CREATEDBY.ToUpper() == "GUEST")
                {
                    Dictionary<string, string> DICStatus = CheckISUserExistsAndQuoteLimit(model.GuestEmail);

                    if (DICStatus["UserExistStatus"] == "true")
                    {
                        return BaseResponse("User already exists, please login with your credentials.", HttpStatusCode.ExpectationFailed);
                    }
                    else if (DICStatus["NoofQuotes"] == "5")
                    {
                        return BaseResponse("Daily Quote Limit Reached !", HttpStatusCode.ExpectationFailed);
                    }
                }

                //-------------Dims Fun---------------
                if (!string.IsNullOrEmpty(model.QuoteSharedBy) && string.IsNullOrEmpty(model.CREATEDBY))
                {
                    if (string.IsNullOrEmpty(model.CollaboratedShipperMail))
                        return BaseResponse("Collaborated Shiper EmailId can't be empty!", HttpStatusCode.ExpectationFailed);

                    if (!model.IsDimsModified)
                    {
                        Sendmail_NoCollaborationQuote(model.QuoteSharedBy, model.CollaboratedShipperMail);
                        return BaseResponse("You have made no changes to Weight/Dimensions of your shipment. So,No updations have been made to your quote. A confirmation email has been sent to the requester.", HttpStatusCode.OK);
                    }

                    //Update Below param in dims case
                    model.CREATEDBY = model.QuoteSharedBy;
                }
                //-----------------------------------


                if (model != null)
                {
                    FillPortPairs(model);
                }

                if (model.IsHazardousCargo == true)
                {
                    //TO-Do
                }
                else
                    model.HazardousGoodsType = null;


                model.IsOversizeCargo = IsOversizeCargo(model);
                decimal TotalGrossWeight = 0;
                foreach (var ShipmentItems in model.ShipmentItems)
                {
                    if (ShipmentItems.PackageTypeId == 1)
                    {
                        ShipmentItems.ItemHeight = 0;
                        ShipmentItems.ItemLength = 0;
                        ShipmentItems.TotalCBM = 0;
                        ShipmentItems.ItemWidth = 0;
                        if (ShipmentItems.WeightPerPiece == 0)
                        {
                            switch (ShipmentItems.ContainerCode)
                            {
                                case "22G0":
                                    ShipmentItems.WeightPerPiece = 14000;
                                    ShipmentItems.WeightTotal = 14000 * ShipmentItems.Quantity;
                                    model.TotalGrossWeight = 14000 * ShipmentItems.Quantity;
                                    break;
                                case "42G0":
                                    ShipmentItems.WeightPerPiece = 16000;
                                    ShipmentItems.WeightTotal = 16000 * ShipmentItems.Quantity;
                                    model.TotalGrossWeight = 16000 * ShipmentItems.Quantity;
                                    break;
                                case "45G0":
                                    ShipmentItems.WeightPerPiece = 17000;
                                    ShipmentItems.WeightTotal = 17000 * ShipmentItems.Quantity;
                                    model.TotalGrossWeight = 17000 * ShipmentItems.Quantity;
                                    break;
                            }
                            ShipmentItems.WeightUOMCode = "KG";
                            ShipmentItems.WeightUOMName = "KG";
                            ShipmentItems.WeightUOM = 95;
                        }
                        else
                        {
                            if (ShipmentItems.WeightUOM == 95)
                                TotalGrossWeight = TotalGrossWeight + (ShipmentItems.WeightPerPiece * ShipmentItems.Quantity);
                            else if (ShipmentItems.WeightUOM == 96)
                                TotalGrossWeight = TotalGrossWeight + ((ShipmentItems.WeightPerPiece * ShipmentItems.Quantity) * Convert.ToDecimal(0.45359237));
                            else if (ShipmentItems.WeightUOM == 97)
                                TotalGrossWeight = TotalGrossWeight + ((ShipmentItems.WeightPerPiece * ShipmentItems.Quantity) * 1000);

                            model.TotalGrossWeight = TotalGrossWeight;
                        }

                    }
                    else
                    {
                        ShipmentItems.ContainerCode = null;
                        ShipmentItems.ContainerId = 0;
                        ShipmentItems.ContainerName = null;
                    }
                }


                if (model.ProductId == ProductEnum.OceanFreight)
                {
                    model.ProductName = "Ocean";
                    model.ProductTypeId = model.ShipmentItems.Count(x => x.ContainerId > 0) > 0 ? 5 : 6;
                    model.ProductTypeName = model.ShipmentItems.Count(x => x.ContainerId > 0) > 0 ? "FCL (NVOCC)" : "LCL (NVOCC)";
                }
                else
                {
                    model.ProductTypeId = 8;
                    model.ProductTypeName = "Premier";
                    model.ProductName = "Air";
                    model.PackageTypes = 0;
                }

                model.MovementTypeName = model.MovementTypeId.Value.GetType().GetMember(model.MovementTypeId.Value.ToString()).First().GetCustomAttribute<DisplayAttribute>().Name;


                DBFactory.Entities.QuotationEntity mUIModel = Mapper.Map<QuotationModel, DBFactory.Entities.QuotationEntity>(model);
                switch (model.MovementTypeId.Value)
                {
                    case MovementTypeEnum.PorttoPort:
                        mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 28 : 30;
                        if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 1;
                        break;

                    case MovementTypeEnum.DoortoPort:
                        mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 24 : 26;
                        if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 37;
                        break;

                    case MovementTypeEnum.PorttoDoor:
                        mUIModel.MOVEMENTTYPEID = model.ProductTypeId == 5 ? 31 : 33;
                        if (model.ProductTypeId == 8) mUIModel.MOVEMENTTYPEID = 44;
                        break;

                    default:
                        mUIModel.MOVEMENTTYPEID = 4;
                        break;
                }

                if (model.QuoteSource == "" || model.QuoteSource == null)
                    mUIModel.QuoteSource = "MB";

                if ((mUIModel.PRODUCTID == 2 && mUIModel.DENSITYRATIO != 1000) || (mUIModel.PRODUCTID == 3 && mUIModel.DENSITYRATIO != 166.667))
                {
                    if (mUIModel.PRODUCTID == 2)
                    {
                        mUIModel.DENSITYRATIO = 1000;
                        mUIModel.VOLUMETRICWEIGHT = Math.Truncate((mUIModel.TOTALCBM * Convert.ToDecimal(mUIModel.DENSITYRATIO)) * 1000) / 1000;
                        mUIModel.CHARGEABLEWEIGHT = Math.Truncate((Math.Max(mUIModel.TOTALGROSSWEIGHT, mUIModel.VOLUMETRICWEIGHT)) * 1000) / 1000;
                        mUIModel.CHARGEABLEVOLUME = Math.Truncate((Math.Max(mUIModel.TOTALCBM, (mUIModel.TOTALGROSSWEIGHT / Convert.ToDecimal(mUIModel.DENSITYRATIO)))) * 1000) / 1000;
                        if (mUIModel.CHARGEABLEVOLUME < 1)
                        {
                            mUIModel.CHARGEABLEVOLUME = 1;
                        }
                    }
                    else
                    {
                        mUIModel.DENSITYRATIO = 166.667;
                        mUIModel.VOLUMETRICWEIGHT = Math.Truncate((mUIModel.TOTALCBM * Convert.ToDecimal(mUIModel.DENSITYRATIO)) * 1000) / 1000;
                        mUIModel.CHARGEABLEWEIGHT = Math.Truncate((Math.Max(mUIModel.TOTALGROSSWEIGHT, mUIModel.VOLUMETRICWEIGHT)) * 1000) / 1000;
                        mUIModel.CHARGEABLEVOLUME = Math.Truncate((Math.Max(mUIModel.TOTALCBM, (mUIModel.TOTALGROSSWEIGHT / Convert.ToDecimal(mUIModel.DENSITYRATIO)))) * 1000) / 1000;
                    }
                }

                if (model.ShipmentItems.Count(x => x.ContainerId > 0) > 0)
                {
                    mUIModel.PackageTypes = 2;
                }
                else
                {
                    mUIModel.PackageTypes = 1;
                }

                //Customer Specific Pricing functionality
                if (!string.IsNullOrEmpty(model.CREATEDBY))
                {
                    long stakeholderId = _QFactory.GetStakeholderIdByUserId(model.CREATEDBY);
                    if (stakeholderId != 0)
                    {
                        mUIModel.CUSTOMERID = stakeholderId;
                    }
                }

                //----------------Save call begins---------------
                long mQuotationId = mFactory.InsertSave(mUIModel, "");

                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(mQuotationId));

                if (!string.IsNullOrEmpty(model.QuoteSharedBy))
                {
                    Sendmail_CollaborationQuote(model.QuoteSharedBy, model.CollaboratedShipperMail);
                    UserDBFactory UserFactory = new UserDBFactory();
                    UserFactory.NewNotification(5121, "New Quote Available for Your Shipment", "", 0, Convert.ToInt32(mDBEntity.QUOTATIONID), mDBEntity.QUOTATIONNUMBER, model.QuoteSharedBy);
                }
                //---------------------------------------------------------------------------------------

                if (mDBEntity.co2emission == 0)
                {
                    double co2 = Co2EmissionCalculation(mDBEntity);
                    mDBEntity.co2emission = Convert.ToDecimal(co2);
                }

                if (mDBEntity.CREATEDBY.ToUpper() != "GUEST" && ((mDBEntity.HASMISSINGCHARGES != 0) || (mDBEntity.HASMISSINGCHARGES == 0 && mDBEntity.GRANDTOTAL == 0))) // RATES NOT AVAILABLE & Zero Quote
                {
                    int quoteid = Convert.ToInt32(mDBEntity.QUOTATIONID);
                    mFactory.RequestQuote(quoteid);
                    SendMailForGSSCQuotes(mDBEntity.QUOTATIONNUMBER.ToString(), Convert.ToInt32(mDBEntity.ThresholdQty), Convert.ToInt32(mDBEntity.ISHAZARDOUSCARGO), Convert.ToInt32(mDBEntity.HASMISSINGCHARGES));
                }

                if (!string.IsNullOrEmpty(mDBEntity.GSSCREASON) && (mDBEntity.HASMISSINGCHARGES == 0))
                {
                    mFactory.InsertGSSCREASON(mDBEntity.QUOTATIONID, mDBEntity.GSSCREASON);

                    int usercount = mFactory.CheckUserRestriction(mDBEntity.CREATEDBY.ToUpper() == "GUEST" ? mDBEntity.GUESTEMAIL.ToUpper() : mDBEntity.CREATEDBY.ToUpper());
                    if (usercount == 0)
                        SendMailForMissingChargesGSSCQuotes(mDBEntity);
                }
                if (mDBEntity.HASMISSINGCHARGES <= 0)
                    SendmailtoNPC(mDBEntity);

                //--------------------------------------------------------------------
                List<QuotationChargeEntity> mUIOrg = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1118).ToList();
                List<QuotationChargeEntity> mUIDes = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1117).ToList();
                List<QuotationChargeEntity> mUIInter = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1116).ToList();
                List<QuotationChargeEntity> mUIAdditional = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 250001).ToList();

                List<QOrgChargeEntity> mUIOrgModel = Mapper.Map<List<QuotationChargeEntity>, List<QOrgChargeEntity>>(mUIOrg);
                List<QDesChargeEntity> mUIModelDes = Mapper.Map<List<QuotationChargeEntity>, List<QDesChargeEntity>>(mUIDes);
                List<QInterChargeEntity> mUIModelInter = Mapper.Map<List<QuotationChargeEntity>, List<QInterChargeEntity>>(mUIInter);
                List<QAdditionalChargeEntity> mUIModelAdditional = Mapper.Map<List<QuotationChargeEntity>, List<QAdditionalChargeEntity>>(mUIAdditional);

                mDBEntity.QOrgCharges = mUIOrgModel;
                mDBEntity.QDesCharges = mUIModelDes;
                mDBEntity.QInterCharges = mUIModelInter;
                mDBEntity.QAdditionalCharges = mUIModelAdditional;

                //Remove unnecessry items
                mDBEntity.Users = null;
                mDBEntity.TermAndConditions = null;
                mDBEntity.QuotationCharges = null;
                int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mDBEntity.PREFERREDCURRENCYID);
                mDBEntity.DECIMALCOUNT = decimalCount;
                if (mDBEntity == null)
                    return BaseResponse("Something went wrong with quote preview.", HttpStatusCode.ExpectationFailed);
                return Ok(mDBEntity);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "SaveQuote", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        public Dictionary<string, string> CheckISUserExistsAndQuoteLimit(string Email)
        {
            UserDBFactory UDF = new UserDBFactory();
            ProfileInfo PInfo = UDF.GetProfileInfo(Email);

            long NoofQuotes = 0;
            var dic = new Dictionary<string, string>();


            if (PInfo.USERID != null)
            {

                dic.Add("UserExistStatus", "true");
                dic.Add("NoofQuotes", "");
            }
            else
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                NoofQuotes = mFactory.NoofQuotesForGuest(Email);

                dic.Add("UserExistStatus", "false");
                dic.Add("NoofQuotes", NoofQuotes.ToString());
            }
            return dic;
        }
        public string TransitTimeCalculationByMovementType(string movementType, string transitTime, Int64 quotationId)
        {
            string calculatedTransitTime = string.Empty;
            try
            {
                var transitTiming = transitTime.Split(' ');
                double time = Convert.ToDouble(transitTiming[0]);
                if (time > 0)
                {
                    string dayOrhour = transitTiming[1];
                    if (movementType.ToUpperInvariant() == "DOOR TO DOOR")
                    {
                        calculatedTransitTime = (dayOrhour.ToUpperInvariant() == "DAYS") ? (time + 3) + " " + dayOrhour : "3 Days" + " " + transitTime;
                    }
                    else if (movementType.ToUpperInvariant() == "DOOR TO PORT" || movementType.ToUpperInvariant() == "PORT TO DOOR")
                    {
                        calculatedTransitTime = (dayOrhour.ToUpperInvariant() == "DAYS") ? (time + 2) + " " + dayOrhour : "2 Days" + " " + transitTime;
                    }
                    else if (movementType.ToUpperInvariant() == "PORT TO PORT")
                    {
                        calculatedTransitTime = (dayOrhour.ToUpperInvariant() == "DAYS") ? (time + 1) + " " + dayOrhour : "1 Days" + " " + transitTime;
                    }
                    else
                    {
                        calculatedTransitTime = transitTime;
                    }
                }
                else
                {
                    calculatedTransitTime = "No Transmit time available";
                }
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "TransitTimeCalculationByMovementType", ex.Message.ToString(), ex.StackTrace.ToString());
                calculatedTransitTime = "No Transmit time available";
            }
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.UpdateTransitTime(quotationId, calculatedTransitTime);
            return calculatedTransitTime;
        }

        public int IsOversizeCargo(QuotationModel model)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DataSet dsOverSize = mFactory.GetShipmentItemsDetails(Convert.ToInt32(model.OriginCountryId));
            int i = 0;
            if (dsOverSize.Tables[0].Rows.Count > 0)
            {
                Double Length_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]); // CM
                Double Width_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]);
                Double Height_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]);
                Double Length_IN = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]) * 0.393701);// CMS to IN
                Double Width_IN = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]) * 0.393701);
                Double Height_IN = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]) * 0.393701);
                string airUOM = Convert.ToString(dsOverSize.Tables[0].Rows[0]["AIRUOM"]);
                if (!string.IsNullOrEmpty(airUOM))
                {
                    switch (airUOM.ToUpperInvariant())
                    {
                        case "MM":
                            Length_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]) / 10); // MM to CM
                            Width_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]) / 10);
                            Height_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]) / 10);
                            break;
                        case "M":
                            Length_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]) * 100); // M to CM
                            Width_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]) * 100);
                            Height_CM = (Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]) * 100);
                            break;

                        case "IN":
                            Length_IN = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]); // IN
                            Width_IN = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]);
                            Height_IN = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]);
                            break;
                        case "CM":
                            Length_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRLENGTH"]); // CM
                            Width_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRWIDTH"]);
                            Height_CM = Convert.ToDouble(dsOverSize.Tables[0].Rows[0]["AIRHEIGHT"]);
                            break;
                    }
                }
                foreach (var ShipmentItems in model.ShipmentItems)
                {
                    if (ShipmentItems.PackageTypeCode != "CNT" && i != 1)
                    {
                        long Unit = ShipmentItems.LengthUOM;
                        Double Length;
                        Double Width;
                        Double Height;
                        switch (Unit)
                        {
                            case 101: //MM
                                Length = Convert.ToDouble(ShipmentItems.ItemLength / 10); // Convert MM to CMS
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth / 10); // Convert MM to CMS
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight / 10); // Convert MM to CMS
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_CM || Width >= Width_CM || Height >= Height_CM) { i = 1; }
                                }
                                else { if (Height >= 259.08) { i = 1; } }
                                break;
                            case 100: //M
                                Length = Convert.ToDouble(ShipmentItems.ItemLength * 100); // Convert M to CMS
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth * 100); // Convert M to CMS
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight * 100); // Convert M to CMS
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_CM || Width >= Width_CM || Height >= Height_CM) { i = 1; }
                                }
                                else { if (Height >= 259.08) { i = 1; } }
                                break;
                            case 99: //IN
                                Length = Convert.ToDouble(ShipmentItems.ItemLength);
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth);
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight);
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_IN || Width >= Width_IN || Height >= Height_IN) { i = 1; }
                                }
                                else
                                {
                                    Height = Convert.ToDouble(ShipmentItems.ItemHeight / 12); //Convert IN to FEET
                                    if (Height >= 8.5) { i = 1; }
                                }
                                break;
                            case 98: //CMS
                                Length = Convert.ToDouble(ShipmentItems.ItemLength);
                                Width = Convert.ToDouble(ShipmentItems.ItemWidth);
                                Height = Convert.ToDouble(ShipmentItems.ItemHeight);
                                if (model.ProductId == ProductEnum.AirFreight)
                                {
                                    if (Length >= Length_CM || Width >= Width_CM || Height >= Height_CM) { i = 1; }
                                }
                                else { if (Height >= 259.08) { i = 1; } }
                                break;
                        }
                    }
                }
            }
            return i;
        }

        [Route("api/Quotation/Preview")]
        [HttpPost]
        public dynamic Preview([FromBody] dynamic model)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();

            try
            {
                if (model.QuotationId == null || model.QuotationId == 0)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "QuotaionID Should not be empty."
                        })
                    };
                }

                long mQuotationId = model.QuotationId;

                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(mQuotationId));

                Co2Emission(mDBEntity);
                mDBEntity = Map_Preview(mDBEntity);
                int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mDBEntity.PREFERREDCURRENCYID);
                mDBEntity.DECIMALCOUNT = decimalCount;
                return Ok(mDBEntity);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "Preview", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }

        }

        [Route("api/Quotation/ClearCustoms")]
        [HttpPost]
        public dynamic ClearCustoms([FromBody] dynamic model)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                mFactory.UpdateCustomCharges(Convert.ToInt32(model.QuotationId), Convert.ToInt32(model.ChargeID), Convert.ToBoolean(model.IsChecked));

                if (model.QuotationId == null || model.QuotationId == 0)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "QuotaionID Should not be empty."
                        })
                    };
                }

                long mQuotationId = model.QuotationId;

                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(mQuotationId));

                Co2Emission(mDBEntity);
                mDBEntity = Map_Preview(mDBEntity);
                return Ok(mDBEntity);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "ClearCustoms", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/RequestQuote")]
        [HttpPost]
        public dynamic RequestQuote([FromBody] dynamic Model)
        {
            try
            {
                if (Model.QuotationID == null)
                    return BaseResponse("QuotationID should not be null or empty", HttpStatusCode.ExpectationFailed);

                QuotationDBFactory mFactory = new QuotationDBFactory();
                mFactory.RequestQuote(Convert.ToInt32(Model.QuotationID));
                SendMailForGSSCQuotes(Convert.ToString(Model.QuotationNumber), Convert.ToInt32(Model.ThresholdQty), Convert.ToInt32(Model.IsHazardousCargo), Convert.ToInt32(Model.HasMissingCharges));

                return BaseResponse("Submitted to Shipa Freight Service Desk.", HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "RequestQuote", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message
                    })
                };
            }
        }

        [Route("api/Quotation/RegenerateQuote")]
        [HttpPost]
        public async Task<dynamic> RegenerateQuote([FromBody] dynamic Model)
        {
            try
            {

                QuotationDBFactory mFactory = new QuotationDBFactory();

                long mNewId = await mFactory.RegenerateQuote(Convert.ToString(Model.EmailID), Convert.ToInt32(Model.QuotationID), "");

                long mQuotationId = mNewId;

                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(mQuotationId));

                Co2Emission(mDBEntity);

                mDBEntity = Map_Preview(mDBEntity);

                return Ok(mDBEntity);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "RegenerateQuote", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/QuoteIndex")]
        [HttpPost]
        public dynamic QuoteIndex([FromBody] dynamic model)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(model.QuotationID));
                QuotationModel mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);

                if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
                    mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
                else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
                    mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
                else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
                    mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
                else
                    mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
                if (mUIModel.CargoValue != 0)
                {
                    mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
                }
                else
                {
                    mUIModel.CurrencyMode = string.Empty;
                }
                mUIModel.OriginCountryName = mUIModel.OCountryName;
                mUIModel.DestinationCountryName = mUIModel.DCountryName;
                mUIModel.OriginCountryId = mUIModel.OCountryId;
                mUIModel.DestinationCountryId = mUIModel.DCountryId;
                int decimalCount = mFactory.GetDecimalPointByCurrencyCode(mDBEntity.PREFERREDCURRENCYID);
                mUIModel.PreferredDecimal = decimalCount;

                return Ok(mUIModel);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "QuoteIndex", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/ShareQuote")]
        [HttpPost]
        public dynamic ShareQuote([FromBody] dynamic Model)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(Model.QuotationID));
                DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = GeneratePDF(memoryStream, DUIModels);
                    memoryStream.Close();
                }
                string EnvironmentName = APIDynamicClass.GetEnvironmentName();
                subject = "Quote from Shipa Freight" + EnvironmentName;
                if (!ReferenceEquals(DUIModels, null))
                {
                    string loginuser = string.Empty;
                    if (!ReferenceEquals(Convert.ToString(Model.LoginEmail), null))
                    {
                        UserDBFactory UDF = new UserDBFactory();
                        UserEntity UE = UDF.GetUserFirstAndLastName(Convert.ToString(Model.LoginEmail));

                        loginuser = Convert.ToString(UE.FIRSTNAME);
                    }
                    Assignvalues(DUIModels);
                    if (DUIModels.ProductName.ToUpperInvariant() == "AIR")
                        productname = "air";
                    else
                        productname = "ship";

                    string str = Encryption.Encrypt(("id=" + Convert.ToInt32(Model.QuotationID)).ToString());
                    str = "?q=" + str;
                    string links = System.Configuration.ConfigurationManager.AppSettings["APPURL"] + "Quotation/Preview" + str;

                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/quote-link-temp.html")))
                    { body = reader.ReadToEnd(); }
                    body = body.Replace("{link}", links);
                    body = body.Replace("{user}", ((string.IsNullOrEmpty(Convert.ToString(Model.Name))) ? Convert.ToString(Model.Email) : Convert.ToString(Model.Name)));
                    body = body.Replace("{loginuser}", (DUIModels.CreatedBy == null || DUIModels.CreatedBy == "guest" || DUIModels.CreatedBy == "") ? DUIModels.GuestName : loginuser);
                    body = body.Replace("{notes}", String.IsNullOrEmpty(Convert.ToString(Model.Notes)) ? "" : Convert.ToString(Model.Notes));
                    body = body.Replace("{DateofEnquiry}", DUIModels.DateofEnquiry.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{PlaceofReceipt}", mPRLableName);
                    body = body.Replace("{PlaceofDepature}", mPDLableName);
                    body = body.Replace("{OriginPlaceName}", mPlaceOfReceipt);
                    body = body.Replace("{DestinationPlaceName}", mPlaceOfDelivery);
                    body = body.Replace("{PreferredCurrencyId}", DUIModels.PreferredCurrencyId);
                    body = body.Replace("{GrandTotal}", Double.Parse(DUIModels.GrandTotal.ToString()).ToString());
                    body = body.Replace("{DateCreated}", DUIModels.DateofEnquiry.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{DateOfValidity}", DUIModels.DateOfValidity.ToString("dd-MMM-yyyy"));
                    body = body.Replace("{ProductName}", DUIModels.ProductName);
                    body = body.Replace("{MovementTypeName}", DUIModels.MovementTypeName);
                    body = body.Replace("{OriginPlaceName}", DUIModels.OriginPlaceName);
                    body = body.Replace("{DestinationPlaceName}", DUIModels.DestinationPlaceName);
                    body = body.Replace("{productname}", productname);
                    body = body.Replace("{CargoDetails}", cargodetails);
                    body = body.Replace("{howitworks}", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/LearnMore/how-it-works");
                    body = body.Replace("{faq}", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/LearnMore/Faq");
                    body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
                    body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
                }
                SendMail(Convert.ToString(Model.Email), body, subject, bytes, DUIModels.QuotationNumber);

                //Insert new emails into Address Book -- Anil G
                if (Convert.ToString(Model.Email) != "")
                {
                    mFactory.InsertNewEmailsIntoAddressBook(Convert.ToString(Model.Email), Model.LoginEmail.ToString());
                }

                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.OK,
                        Message = "Quote has been shared successfully." //return Success Message
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "ShareQuote", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString() //return Success Message
                    })
                };
            }
        }

        [Route("api/Quotation/DownloadQuote")]
        [HttpPost]
        public dynamic DownloadQuote([FromBody] dynamic Model)
        {
            string base64 = string.Empty;
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt32(Model.QuotationID));
                DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
                PreferredCurrencyId = DUIModels.PreferredCurrencyId;


                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bytes = GeneratePDF(memoryStream, DUIModels);

                    base64 = Convert.ToBase64String(bytes);
                }
                return base64;
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "DownloadQuote", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.Gone,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/DeleteQuote")]
        [HttpPost]
        public dynamic DeleteQuote([FromBody] dynamic Model)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                string Result = mFactory.DeleteQuote(Convert.ToInt32(Model.QuotationID), Convert.ToString(Model.CreatedBy));
                if (Result == "Deleted")
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Quote has been deleted successfully."
                        })
                    };
                else
                    return new HttpResponseMessage()
                    {
                        Content = new JsonContent(new
                        {
                            StatusCode = HttpStatusCode.ExpectationFailed,
                            Message = "Error, Something went worng."
                        })
                    };
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "DeleteQuote", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/ClearCustoms")]
        [HttpPost]
        public dynamic ClearCustoms(int id, int QuotationId, bool IsChecked)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                mFactory.UpdateCustomCharges(Convert.ToInt32(QuotationId), Convert.ToInt32(id), IsChecked);


                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = "Error, Something went worng."
                    })
                };
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "ClearCustoms", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/SaveTemplate")]
        [HttpPost]
        public dynamic SaveTemplate([FromBody]dynamic Req)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                int count = mFactory.TemplateNameCheck(Convert.ToString(Req.TemplateName), Convert.ToString(Req.Email));
                if (count == 0)
                {
                    if (Req.Id == null)
                    {
                        var errresp = new { Error = "Id is Required" };
                        return BaseResponse(errresp, HttpStatusCode.ExpectationFailed);
                    }
                    DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetById(Convert.ToInt64(Req.Id));
                    string Result = mFactory.SaveTemplate(mDBEntity, Convert.ToString(Req.TemplateName));
                    var response = new { Message = Result };
                    return BaseResponse(response, HttpStatusCode.OK);
                }
                else
                {
                    var errresp = new { Error = "Template name already exists." };
                    return BaseResponse(errresp, HttpStatusCode.ExpectationFailed);
                }
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "SaveTemplate", ex.Message.ToString(), ex.StackTrace.ToString());
                var errresp = new { Error = ex.Message.ToString() };
                return BaseResponse(errresp, HttpStatusCode.InternalServerError);
            }
        }

        [Route("api/Quotation/DeleteTempalate")]
        [HttpPost]
        public dynamic DeleteTempalate([FromBody]dynamic Req)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                mFactory.DeleteTemplate(Convert.ToInt32(Req.Id), Convert.ToString(Req.Email));
                var resp = new { Message = "Quote has been deleted successfully." };
                return BaseResponse(resp, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "DeleteTempalate", ex.Message.ToString(), ex.StackTrace.ToString());
                var errresp = new { Error = ex.Message.ToString() };
                return BaseResponse(errresp, HttpStatusCode.InternalServerError);
            }
        }

        [Route("api/Quotation/GetByTemplateId")]
        [HttpPost]
        public dynamic GetByTemplateId([FromBody]dynamic Req)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                DBFactory.Entities.QuotationEntity mDBEntity = mFactory.GetByTemplateId(Convert.ToInt64(Req.Id));

                QuotationModel mUIModel = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationModel>(mDBEntity);
                if (mDBEntity.MOVEMENTTYPEID == 1 || mDBEntity.MOVEMENTTYPEID == 28 || mDBEntity.MOVEMENTTYPEID == 30)
                    mUIModel.MovementTypeId = MovementTypeEnum.PorttoPort;
                else if (mDBEntity.MOVEMENTTYPEID == 37 || mDBEntity.MOVEMENTTYPEID == 24 || mDBEntity.MOVEMENTTYPEID == 26)
                    mUIModel.MovementTypeId = MovementTypeEnum.DoortoPort;
                else if (mDBEntity.MOVEMENTTYPEID == 44 || mDBEntity.MOVEMENTTYPEID == 31 || mDBEntity.MOVEMENTTYPEID == 33)
                    mUIModel.MovementTypeId = MovementTypeEnum.PorttoDoor;
                else
                    mUIModel.MovementTypeId = MovementTypeEnum.DoortoDoor;
                if (mUIModel.CargoValue != 0)
                {
                    mUIModel.CurrencyMode = mUIModel.CargoValue.ToString();
                }
                else
                {
                    mUIModel.CurrencyMode = string.Empty;
                }
                if (mUIModel.RequestType != "OFFER")
                {
                    mUIModel.IsOfferApplicable = false;
                    mUIModel.OfferCode = string.Empty;
                }
                DataSet ds = mFactory.GetCountryID((mUIModel.OCountryCode).Trim());
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    mUIModel.OriginCountryName = ds.Tables[0].Rows[0]["NAME"].ToString();
                    mUIModel.OriginCountryId = Convert.ToDecimal(ds.Tables[0].Rows[0]["COUNTRYID"].ToString());
                    mUIModel.OCountryId = mUIModel.OriginCountryId;
                    mUIModel.OCountryName = mUIModel.OriginCountryName;
                }

                DataSet dsd = mFactory.GetCountryID((mUIModel.DCountryCode).Trim());
                if (dsd.Tables.Count > 0 && dsd.Tables[0].Rows.Count > 0)
                {
                    mUIModel.DestinationCountryName = dsd.Tables[0].Rows[0]["NAME"].ToString();
                    mUIModel.DestinationCountryId = Convert.ToDecimal(dsd.Tables[0].Rows[0]["COUNTRYID"].ToString());
                    mUIModel.DCountryId = mUIModel.DestinationCountryId;
                    mUIModel.DCountryName = mUIModel.DestinationCountryName;
                }
                return BaseResponse(mUIModel, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "GetByTemplateId", ex.Message.ToString(), ex.StackTrace.ToString());
                var errresp = new { Error = ex.Message.ToString() };
                return BaseResponse(errresp, HttpStatusCode.InternalServerError);
            }
        }

        [Route("api/Quotation/SubmitReasonforUnbook")]
        [HttpPost]
        public dynamic SubmitReasonforUnbook([FromBody]dynamic Req)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                mFactory.SubmitReasonforUnbook(Convert.ToInt32(Req.QUOTATIONID.Value), 0, Convert.ToString(Req.REASONID.Value));
                var res = new { Message = "Success" };
                return BaseResponse(res, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "SubmitReasonforUnbook", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }


        [Route("api/Quotation/CouponVerification")]
        [HttpPost]
        public dynamic CouponVerification([FromBody]dynamic data)
        {
            try
            {
                return CheckInputValidations(data);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "CouponVerification", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        private dynamic CheckInputValidations(dynamic data)
        {
            if (data == null)
                return BaseResponse(new { Error = "Input data was not in a correct format." }, HttpStatusCode.ExpectationFailed);

            if (data.PRODUCTID == null)
                return BaseResponse(new { Error = "ProductID should not be empty" }, HttpStatusCode.ExpectationFailed);

            if (data.COUPON == null)
                return BaseResponse(new { Error = "COUPON code should not be empty" }, HttpStatusCode.ExpectationFailed);

            return CheckCoupon(data);
        }

        private dynamic CheckCoupon(dynamic data)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DataTable dtMessage = mFactory.CouponVerification(Convert.ToString(data.COUPON).ToUpper(), Convert.ToInt32(data.PRODUCTID.Value));
            if (dtMessage.Rows.Count > 0)
            {
                return BaseResponse(new { Message = "Valid coupon" }, HttpStatusCode.OK);
            }
            else
            {
                return BaseResponse(new { Error = "Not a valid coupon" }, HttpStatusCode.ExpectationFailed);
            }
        }


        [Route("api/Quotation/ExclusiveOffers")]
        [HttpGet]
        public dynamic ExclusiveOffers(string SearchString = "")
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                List<QuoteOffersModel> mUIModel;
                List<DBFactory.Entities.QuoteOffersEntity> mDBEntity = mFactory.GetSavedOffers(SearchString);
                mUIModel = Mapper.Map<List<DBFactory.Entities.QuoteOffersEntity>, List<QuoteOffersModel>>(mDBEntity);
                DBFactory.Entities.QuoteStatusCountEntity mQuoteStatusEntity = mFactory.GetSavedOffersCount(SearchString);
                var data = new ExclusiveOffersEnvelope
                {
                    OFFERS = mUIModel,
                    COUNT = mQuoteStatusEntity.NOMINATIONS
                };
                return BaseResponse(data, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "ExclusiveOffers", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse(new { Error = "Something Went Wrong." }, HttpStatusCode.ExpectationFailed);
            }
        }

        public class ExclusiveOffersEnvelope
        {
            public decimal COUNT { get; set; }
            public List<QuoteOffersModel> OFFERS { get; set; }
        }

        [Route("api/Quotation/NoofQuotes")]
        [HttpGet]
        public dynamic NoofQuotes(string UserName)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                long NoofQuotes = mFactory.NoofQuotes(UserName);

                if (NoofQuotes <= 10) { return BaseResponse(new { Message = "ok" }, HttpStatusCode.OK); }
                else
                {
                    return BaseResponse(
                        new
                        {
                            Error = "Max quote limit reached",
                            Alert = "We notice you have made numerous quote requests in the last 24 hours. To ensure that you get what you need, please contact our Support team with your specific requirements and we will get back to you promptly."
                        },
                            HttpStatusCode.ExpectationFailed
                   );
                }
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "NoofQuotes", ex.Message.ToString(), ex.StackTrace.ToString());
                return BaseResponse("Something Went Wrong", HttpStatusCode.InternalServerError);
            }
        }


        [Route("api/Quotation/InsuranceRequiredOrNot")]
        [HttpGet]
        public dynamic InsuranceRequiredOrNot(Int64? o, Int64? d)
        {
            try
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                if (o != null && d != null)
                {
                    string insuranceRequired = mFactory.InsuranceRequiredOrNot(o, d);
                    if (insuranceRequired != null)
                    {
                        string isInsurance = insuranceRequired.ToString();
                        return BaseResponse(isInsurance, HttpStatusCode.OK);
                    }
                    else
                    {
                        return BaseResponse("Something Went Wrong", HttpStatusCode.ExpectationFailed);
                    }
                }
                else
                {
                    return BaseResponse("Something Went Wrong", HttpStatusCode.InternalServerError);
                }
            }
            catch (Exception ex)
            {
                LogError("api/Quotation", "InsuranceRequiredOrNot", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message.ToString()
                    })
                };
            }
        }

        [Route("api/Quotation/Sanctioned")]
        [HttpGet]
        public async Task<dynamic> Sanctioned(string FromCountryId, string ToCountryId, string ProductId, string PackageTypes)
        {
            if (string.IsNullOrEmpty(FromCountryId))
            {
                FromCountryId = "0";
            }
            if (string.IsNullOrEmpty(ToCountryId))
            {
                ToCountryId = "0";
            }
            if (string.IsNullOrEmpty(ProductId))
            {
                ProductId = "0";
            }
            if (string.IsNullOrEmpty(PackageTypes) || ProductId == "3")
            {
                PackageTypes = "0";
            }
            QuotationDBFactory mFactory = new QuotationDBFactory();
            string Result = await Task.Run(() => mFactory.SanctionedCountrybyCountryId(FromCountryId, ToCountryId, ProductId, PackageTypes)).ConfigureAwait(false);
            if (string.IsNullOrEmpty(Result)) Result = "No";
            return BaseResponse(Result, HttpStatusCode.OK);
        }

        [Route("api/Quotation/UploadMSDSCertificate")]
        [HttpPost]
        public dynamic UploadMSDS([FromBody]UploadDocModel theFile)
        {
            return SafeRun<HttpResponseMessage>(() =>
            {
                DBFactory.Entities.SSPDocumentsEntity docentity = new DBFactory.Entities.SSPDocumentsEntity();
                docentity.FILENAME = theFile.FileName;
                docentity.FILEEXTENSION = theFile.ContentType;
                docentity.FILESIZE = theFile.ContentLength;
                docentity.FILECONTENT = System.Convert.FromBase64String(theFile.stringInBase64);

                QuotationDBFactory mFactory = new QuotationDBFactory();
                Guid obj = Guid.NewGuid();
                string EnvironmentName = APIDynamicClass.GetEnvironmentNameDocs();
                var returneddocid = mFactory.UploadMSDSCertificate(docentity, obj.ToString(), EnvironmentName);
                return BaseResponse(new { DOCID = returneddocid }, HttpStatusCode.OK);
            });
        }

        [Route("api/Quotation/GetMSDSDocument")]
        [HttpGet]
        public dynamic GetMSDSDocument(string id)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            var msdsdocdetails = mFactory.GetMsdsdocdetails(id);
            msdsdocdetails.FILECONTENT = null;
            return BaseResponse(msdsdocdetails, HttpStatusCode.OK);
        }

        #endregion

        #region Download Quote Reference Fn's
        public static byte[] GeneratePDF(MemoryStream memoryStream, QuotationPreviewModel mUIModels)
        {
            PDFpage = 0;
            UserDBFactory mFactory = new UserDBFactory();
            DUserModel = mFactory.GetUserProfileDetails(mUIModels.CreatedBy, 1);
            mQuoteChargesTotal = 0;
            Document doc = new Document(PageSize.A4, 50, 50, 125, 25);
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            writer.PageEvent = new ITextEvents();

            PdfPTable mHeadingtable = new PdfPTable(1);
            mHeadingtable.WidthPercentage = 100;
            mHeadingtable.DefaultCell.Border = 0;
            mHeadingtable.SpacingAfter = 5;
            string mDocumentHeading = (mUIModels.ProductId == 2) ? "Ocean Freight Quote" : "Air Freight Quote";
            Formattingpdf mFormattingpdf = new Formattingpdf();
            PdfPCell quotationheading = mFormattingpdf.DocumentHeading(mDocumentHeading);
            mHeadingtable.AddCell(quotationheading);
            doc.Add(mHeadingtable);
            doc.Add(mFormattingpdf.LineSeparator());

            doc.Add(QuoteInfoTableModel(mUIModels));
            doc.Add(ShipInfoTableModel(mUIModels));

            PdfPTable mitemdescriptiontable = new PdfPTable(1);
            mitemdescriptiontable.WidthPercentage = 100;
            mitemdescriptiontable.SpacingBefore = 10;
            mitemdescriptiontable.DefaultCell.Border = 0;
            mitemdescriptiontable.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Item Description", Element.ALIGN_LEFT, false)));

            PdfPTable mItemDescription = new PdfPTable(1);
            mItemDescription.SetWidths(new float[] { 100 });
            mItemDescription.WidthPercentage = 100;
            mItemDescription.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mUIModels.ShipmentDescription == null ? "" : mUIModels.ShipmentDescription, Element.ALIGN_LEFT)));

            PdfPCell mRoutecell = new PdfPCell(mItemDescription);
            mRoutecell.Colspan = 2;
            mRoutecell.BorderColor = mFormattingpdf.TableBorderColor;
            mitemdescriptiontable.AddCell(mRoutecell);
            doc.Add(mitemdescriptiontable);

            if (mUIModels.QuotationCharges.Count() > 0)
            {
                var mOrigincharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1118).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mDestinationcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1117).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mINTcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 1116).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                var mAddcharges = mUIModels.QuotationCharges.Where(x => x.RouteTypeId == 250001).OrderBy(x => x.ChargeName).OrderBy(x => x.ChargeName, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase);
                if (mOrigincharges.Count() > 0)
                    doc.Add(ChargesTableModel(mOrigincharges, "Origin Charges", mUIModels.PreferredCurrencyId));

                if (mINTcharges.Count() > 0)
                    doc.Add(ChargesTableModel(mINTcharges, "International Charges", mUIModels.PreferredCurrencyId));

                if (mDestinationcharges.Count() > 0)
                    doc.Add(ChargesTableModel(mDestinationcharges, "Destination Charges", mUIModels.PreferredCurrencyId));

                if (mAddcharges.Count() > 0)
                    doc.Add(ChargesTableModel(mAddcharges, "Additional Charges", mUIModels.PreferredCurrencyId));
            }
            PdfPTable mQuoteChargeTotalBorder = new PdfPTable(2);
            mQuoteChargeTotalBorder.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotalBorder.WidthPercentage = 100;
            mQuoteChargeTotalBorder.SpacingBefore = 10;
            mQuoteChargeTotalBorder.DefaultCell.Border = 0;
            mQuoteChargeTotalBorder.AddCell(new PdfPCell(mFormattingpdf.TableCaption("", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });

            PdfPTable mQuoteChargeTotal = new PdfPTable(2);
            mQuoteChargeTotal.SetWidths(new float[] { 50, 50 });
            mQuoteChargeTotal.WidthPercentage = 100;
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading("Grand Total", Element.ALIGN_LEFT)));
            mQuoteChargeTotal.AddCell((mFormattingpdf.TableHeading(mUIModels.PreferredCurrencyId + " " + mUIModels.RoundedGrandTotalPrice, Element.ALIGN_RIGHT)));

            PdfPCell mTotalcells = new PdfPCell(mQuoteChargeTotal);
            mQuoteChargeTotalBorder.AddCell(mTotalcells);
            doc.Add(mQuoteChargeTotalBorder);
            doc.NewPage();
            mHeadingtable.AddCell(quotationheading);
            doc.Add(TermsandConditionsTableModel(mUIModels));

            doc.Close();

            bytes = memoryStream.ToArray(); ;
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages, blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;

        }


        public static PdfPTable ShipInfoTableModel(QuotationPreviewModel model)
        {
            int mProductId = model.ProductId;
            int mProductTypeId = model.ProductTypeId;
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mShipbordertable = new PdfPTable(1);
                mShipbordertable.WidthPercentage = 100;
                mShipbordertable.SpacingBefore = 10;
                mShipbordertable.DefaultCell.Border = 0;

                List<PdfPCell> mHeaderLabels = new List<PdfPCell>();
                PdfPTable mShipTab = null;

                mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Quantity", Element.ALIGN_LEFT, false)));

                if (mProductTypeId == 5)
                {
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Container Size", Element.ALIGN_LEFT, false)));
                    mShipTab = new PdfPTable(2);
                    mShipTab.SetWidths(new float[] { 20, 80 });
                }
                else
                {
                    mShipTab = new PdfPTable(5);
                    mShipTab.SetWidths(new float[] { 10, 10, 10, 10, 10 });
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Package Type", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Dimension(L*W*H)", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Per Piece", Element.ALIGN_LEFT, false)));
                    mHeaderLabels.Add(new PdfPCell(mFormattingpdf.TableHeading("Gross Weight", Element.ALIGN_LEFT, false)));
                }
                mShipTab.WidthPercentage = 100;

                mShipbordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption("Cargo Details", Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                foreach (var mLabel in mHeaderLabels)
                {
                    mShipTab.AddCell(mLabel);
                }
                foreach (var mShipment in model.ShipmentItems)
                {
                    mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.Quantity.ToString(), Element.ALIGN_LEFT)));
                    if (mProductTypeId == 5)
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ContainerName, Element.ALIGN_LEFT)));
                    else
                    {
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.PackageTypeName, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(mShipment.ItemLength + " x " + mShipment.ItemWidth + " x " + mShipment.ItemHeight, Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTPERPIECE) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTPERPIECE) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                        mShipTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(IsNullOrDefault(mShipment.WEIGHTTOTAL) ? "--" : (String.Format(String.Format(culture, format, mShipment.WEIGHTTOTAL) + " " + mShipment.WEIGHTUOMNAME)), Element.ALIGN_LEFT)));
                    }
                }
                PdfPCell mShipcell = new PdfPCell(mShipTab);
                mShipcell.BorderColor = mFormattingpdf.TableBorderColor;
                mShipbordertable.AddCell(mShipcell);

                if (mProductTypeId != 5)
                {
                    string mTotalCharegebleVolume = string.Empty;
                    if (mProductId == 2)
                    {
                        mTotalCharegebleVolume = "Revenue Ton" + " : " + (IsNullOrDefault(model.ChargeableVolume) ? "0.00" : (model.ChargeableVolume) <= 1 ? String.Format(culture, format, model.ChargeableVolume) + " " + "(minimum)" : String.Format(culture, format, model.ChargeableVolume));
                    }
                    else
                    {
                        mTotalCharegebleVolume = "Volumetric Weight (Kgs)" + " : " + (IsNullOrDefault(model.VolumetricWeight) ? "0.0" : String.Format(culture, format, model.VolumetricWeight));
                    }
                    string mTotalGW = "Total Gross Weight (Kgs)" + " : " + (IsNullOrDefault(model.TotalGrossWeight) ? "0.0" : String.Format(culture, format, model.TotalGrossWeight));
                    string mTotalCBM = "Total Cubic Meters" + " : " + (IsNullOrDefault(model.TotalCBM) ? "0.000" : String.Format(culture, format, model.TotalCBM));
                    string mTotalCharegebleWeight = "Chargeable Weight (Kgs)" + " : " + (IsNullOrDefault(model.ChargeableWeight) ? "0.00" : String.Format(culture, format, model.ChargeableWeight));

                    PdfPTable mShipmentTotalWeights;

                    if (mProductId == 2)
                    {
                        mShipmentTotalWeights = new PdfPTable(3);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.SetWidths(new float[] { 53, 56, 77 });
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    else
                    {
                        mShipmentTotalWeights = new PdfPTable(4);
                        mShipmentTotalWeights.WidthPercentage = 100;
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalGW, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCBM, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleWeight, Element.ALIGN_LEFT, false));
                        mShipmentTotalWeights.AddCell(mFormattingpdf.TableFooter(mTotalCharegebleVolume, Element.ALIGN_LEFT, false));
                    }
                    PdfPCell mTableFooterCell = mFormattingpdf.TableHeading("ShipmentDetails", Element.ALIGN_LEFT, false);
                    mTableFooterCell.Colspan = mShipTab.NumberOfColumns;
                    mTableFooterCell.Padding = 0;
                    mTableFooterCell.BackgroundColor = BaseColor.WHITE;
                    mTableFooterCell.AddElement(mShipmentTotalWeights);
                    mShipbordertable.AddCell(mTableFooterCell);
                }
                return mShipbordertable;
            }
            catch (Exception ex)
            {
                throw new ShipaApiException(ex.ToString());
            }
        }

        static bool IsNullOrDefault<T>(T value)
        {
            return object.Equals(value, default(T));
        }
        public static PdfPCell ImageCell(string path, float scale, int align)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath(path));
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 0f;
            cell.PaddingTop = 0f;
            return cell;
        }
        public static PdfPTable QuoteInfoTableModel(QuotationPreviewModel model)
        {
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mQuotebordertable = new PdfPTable(3);
                mQuotebordertable.WidthPercentage = 100;
                mQuotebordertable.SpacingBefore = 10;
                mQuotebordertable.DefaultCell.Border = 0;

                PdfPTable mQuoteTab = new PdfPTable(9);
                mQuoteTab.SetWidths(new float[] { 15, 1, 15, 13, 1, 20, 15, 1, 20 });
                mQuoteTab.WidthPercentage = 100;
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Mode of transport")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.ProductName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.OriginPlaceName == null ? "" : " Origin City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPlaceName == null ? "" : model.OriginPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold(model.DestinationPlaceName == null ? "" : "Destination City")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : ":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPlaceName == null ? "" : model.DestinationPlaceName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Movement type")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.MovementTypeName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Port of loading")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginPortName == null ? "" : model.OriginPortName)));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("Port of discharge")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationPortName == null ? "" : model.DestinationPortName)));
                if (model.co2emission != 0)
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("CO2 Emission")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.co2emission == 0 ? "" : Convert.ToString(model.co2emission) + " KG")));
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }
                if (model.OriginZipCode != null || model.DestinationZipCode != null)
                {
                    if (model.OriginZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("ZipCode")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.OriginZipCode)));
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                    if (model.DestinationZipCode != null)
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("ZipCode")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(":")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell(model.DestinationZipCode)));
                    }
                    else
                    {
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                        mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    }
                }
                else
                {
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCellBold("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                    mQuoteTab.AddCell((mFormattingpdf.TableContentCell("")));
                }

                PdfPCell mRoutecell = new PdfPCell(mQuoteTab);
                mRoutecell.Colspan = 3;
                mQuotebordertable.AddCell(mRoutecell);

                return mQuotebordertable;
            }
            catch (Exception ex)
            {
                throw new ShipaApiException(ex.ToString());
            }
        }

        public class ITextEvents : PdfPageEventHelper
        {
            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                string format = "MMMM d, yyyy";
                CultureInfo ci = new CultureInfo("en-US");
                string TaxNum = "";
                if (PDFpage == 0)
                {
                    string FullName = (DUserModel.FIRSTNAME != null) ? DUserModel.FIRSTNAME + " " + DUserModel.LASTNAME : DUIModels.GuestName;
                    if (string.IsNullOrWhiteSpace(FullName))
                        FullName = DUserModel.USERID;
                    string Company = (DUserModel.COMPANYNAME != null) ? DUserModel.COMPANYNAME : DUIModels.GuestCompany;
                    string Address = (DUserModel.UAFULLADDRESS != null) ? DUserModel.UAFULLADDRESS : "";
                    string Phone = (DUserModel.WORKPHONE != null) ? (DUserModel.ISDCODE + " - " + DUserModel.WORKPHONE) : "";
                    string EMail = (DUserModel.USERID != null) ? DUserModel.USERID : DUIModels.GuestEmail;
                    if (!string.IsNullOrWhiteSpace(DUserModel.COUNTRYNAME))
                    {
                        if (DUserModel.COUNTRYID == Convert.ToDouble(Country.INDIA))
                        {
                            if (!string.IsNullOrEmpty(DUserModel.VATREGNUMBER))
                                TaxNum = "GST number : " + DUserModel.VATREGNUMBER;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(DUserModel.VATREGNUMBER))
                                TaxNum = "Indirect Tax number : " + DUserModel.VATREGNUMBER;
                        }
                    }
                    PdfPTable mUserTab = new PdfPTable(2);
                    mUserTab.WidthPercentage = 100;
                    mUserTab.SpacingAfter = 0;
                    mUserTab.DefaultCell.Padding = 0;
                    mUserTab.DefaultCell.Border = 0;
                    mUserTab.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                    float[] tblUserwidths = new float[] { 50, 50 };
                    mUserTab.SetWidths(tblUserwidths);

                    int mUserInfoAlign = Element.ALIGN_LEFT;
                    PdfPTable mHeaderUserTable = new PdfPTable(1);
                    mHeaderUserTable.WidthPercentage = 100;
                    mHeaderUserTable.TotalWidth = 230;
                    mHeaderUserTable.DefaultCell.Padding = 0;
                    mHeaderUserTable.HorizontalAlignment = 0;
                    mUserTab.AddCell(mHeaderUserTable);

                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell("For,", mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(FullName, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Company, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Address))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Address, mUserInfoAlign));
                    if (!string.IsNullOrWhiteSpace(Phone))
                        mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(Phone, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(EMail, mUserInfoAlign));
                    mHeaderUserTable.AddCell(mFormattingpdf.TableContentCell(TaxNum, mUserInfoAlign));
                    mUserTab.AddCell(mHeaderUserTable);
                    mHeaderUserTable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 35), writer.DirectContent);
                }
                PDFpage++;
                string mQuotationNumber = DUIModels.QuotationNumber;
                string mDateCreated = DUIModels.DateofEnquiry.ToString();
                string mDateofValidity = DUIModels.DateOfValidity.ToString();
                string mIssueDate = "Issue Date" + " : " + ((!String.IsNullOrEmpty(mDateCreated)) ? Convert.ToDateTime(mDateCreated).ToString(format, ci) : string.Empty);
                string mValidTillDate = "Quote valid until" + " : " + ((!String.IsNullOrEmpty(mDateofValidity)) ? Convert.ToDateTime(mDateofValidity).ToString(format, ci) : string.Empty);
                string mShipmentDate = string.Empty;
                string DateOfShipment = DUIModels.DateofShipment.ToString();
                if (String.IsNullOrEmpty(DateOfShipment))
                    mShipmentDate = "DateOfShipment" + " : " + ((!String.IsNullOrEmpty(DateOfShipment)) ? Convert.ToDateTime(DateOfShipment).ToString(format, ci) : string.Empty);

                PdfPTable mHeadermaintable = new PdfPTable(3);
                mHeadermaintable.WidthPercentage = 100;
                mHeadermaintable.SpacingAfter = 0;
                mHeadermaintable.DefaultCell.Padding = 0;
                mHeadermaintable.DefaultCell.Border = 0;
                mHeadermaintable.DefaultCell.BorderColor = mFormattingpdf.TableBorderColor;
                float[] tblwidths = new float[] { 50, 29, 36 };
                mHeadermaintable.SetWidths(tblwidths);

                int mQuoteInfoAlign = Element.ALIGN_LEFT;

                PdfPTable mHeaderLeft = new PdfPTable(1);
                mHeaderLeft.WidthPercentage = 100;
                mHeaderLeft.DefaultCell.Padding = 0;
                mHeaderLeft.HorizontalAlignment = 0;

                //Company Logo
                PdfPTable mHeaderImageTable = new PdfPTable(1);
                mHeaderImageTable.WidthPercentage = 100;
                mHeadermaintable.TotalWidth = 500;
                mHeaderImageTable.DefaultCell.Padding = 0;
                mHeaderImageTable.DefaultCell.Border = 0;
                PdfPCell imgcel = ImageCell("~/Images/AgilityPrint.png", 48f, mQuoteInfoAlign);
                imgcel.Border = 0;
                imgcel.PaddingBottom = 3;
                mHeaderImageTable.AddCell(imgcel);
                mHeaderImageTable.AddCell(mFormattingpdf.TableContentCellBold(mQuotationNumber, mQuoteInfoAlign));
                mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mIssueDate, mQuoteInfoAlign));
                mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mValidTillDate, mQuoteInfoAlign));
                if (!string.IsNullOrWhiteSpace(mShipmentDate))
                    mHeaderImageTable.AddCell(mFormattingpdf.TableContentCell(mShipmentDate, mQuoteInfoAlign));
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell("");
                mHeadermaintable.AddCell(mHeaderImageTable);
                mHeadermaintable.WriteSelectedRows(0, -1, 55, (document.PageSize.Height - 10), writer.DirectContent);
            }
        }
        public static PdfPTable ChargesTableModel(IEnumerable<QuotationChargePreviewModel> model, string mHeaderText, string CurrencyId)
        {
            try
            {
                var ChargesTotal = Double.Parse(model.Where(x => x.ISCHARGEINCLUDED == true).Sum(sum => sum.TotalPrice).ToString()).ToString();
                QuotationDBFactory mFactory = new QuotationDBFactory();
                string returnvalue = mFactory.RoundedValue(ChargesTotal, CurrencyId);

                if (returnvalue != "0.00" && returnvalue != null)
                {
                    mQuoteChargesTotal = mQuoteChargesTotal + Convert.ToDecimal(ChargesTotal);
                    Formattingpdf mFormattingpdf = new Formattingpdf();
                    PdfPTable mChargebordertable = new PdfPTable(2);
                    mChargebordertable.WidthPercentage = 100;
                    mChargebordertable.SpacingBefore = 10;
                    mChargebordertable.DefaultCell.Border = 0;
                    PdfPTable mChargeTab = new PdfPTable(2);
                    mChargeTab.SetWidths(new float[] { 50, 50 });
                    mChargeTab.WidthPercentage = 100;
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Charge Name", Element.ALIGN_LEFT, false)));
                    mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableHeading("Total Price", Element.ALIGN_RIGHT, false)));
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(mHeaderText, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, true, true)) { Border = 0 });
                    mChargebordertable.AddCell(new PdfPCell(mFormattingpdf.TableCaption(CurrencyId + " " + returnvalue, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, true, true)) { Border = 0 });
                    foreach (var QC in model)
                    {
                        if (QC.ISCHARGEINCLUDED)
                        {
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.ChargeName, Element.ALIGN_LEFT)));
                            mChargeTab.AddCell(new PdfPCell(mFormattingpdf.TableContentCell(QC.RoundedTotalPrice.ToString(), Element.ALIGN_RIGHT)));
                        }
                    }

                    PdfPCell mChargecell = new PdfPCell(mChargeTab);
                    mChargecell.BorderColor = mFormattingpdf.TableBorderColor;
                    mChargecell.Colspan = 2;

                    mChargebordertable.AddCell(mChargecell);
                    mChargebordertable.KeepTogether = true;
                    return mChargebordertable;
                }
                else
                {
                    PdfPTable mChargebordertable = new PdfPTable(1);
                    return mChargebordertable;
                }
            }
            catch (Exception ex)
            {
                throw new ShipaApiException(ex.ToString());
            }
        }

        public static PdfPTable TermsandConditionsTableModel(QuotationPreviewModel model)
        {
            try
            {
                Formattingpdf mFormattingpdf = new Formattingpdf();
                PdfPTable mTACtable = new PdfPTable(1);
                mTACtable.WidthPercentage = 100;
                mTACtable.HeaderRows = 0;
                mTACtable.DefaultCell.Border = 0;
                mTACtable.SetWidths(new float[] { 100 });
                mTACtable.KeepTogether = true;

                PdfPCell quotationheading = mFormattingpdf.DocumentHeading("Terms and Conditions");
                quotationheading.PaddingBottom = -7;
                quotationheading.Border = 0;
                mTACtable.AddCell(quotationheading);

                Phrase mQuoteCondLinephrase = new Phrase();
                mQuoteCondLinephrase.Add(mFormattingpdf.LineSeparator());
                mTACtable.AddCell(mQuoteCondLinephrase);
                mTACtable.AddCell(new PdfPCell() { FixedHeight = 5, Border = 0 });
                int i = 0;
                foreach (var QT in model.TermAndConditions)
                {
                    i = i + 1;
                    PdfPCell mQuoteDesciption = new PdfPCell(mFormattingpdf.TableContentCell(i + "." + QT.DESCRIPTION)) { Padding = 2 };
                    mTACtable.AddCell(mQuoteDesciption);
                }
                return mTACtable;
            }
            catch (Exception ex)
            {
                throw new ShipaApiException(ex.ToString());
            }
        }
        #endregion

        #region PDF Formatting

        /// <summary>
        /// Summary description for Formatting
        /// </summary>
        public class Formattingpdf
        {
            public Font Heading4 { get; private set; }
            public Font Heading5 { get; private set; }
            public Font Heading6 { get; private set; }
            public Font Heading7 { get; private set; }
            public Font TableCaption7 { get; private set; }
            public Font Heading8 { get; private set; }
            public Font Heading9 { get; private set; }
            public Font Normal { get; private set; }
            public Font NormalRedColor { get; private set; }
            public Font Normal6 { get; private set; }
            public Font NormalItalic6 { get; private set; }
            public Font NormalItalic7 { get; private set; }
            public Font NormalBold { get; private set; }
            public Font BoldForQuoteTotal { get; private set; }

            public BaseColor TableHeadingBgColor { get; private set; }
            public BaseColor TableFooterBgColor { get; private set; }
            public BaseColor TableBorderColor { get; private set; }
            public BaseFont chinesebasefont { get; private set; }
            public BaseFont koreanbasefont { get; private set; }

            public FontSelector selectorTableCaption { get; private set; }
            public FontSelector selectorTableHeading { get; private set; }
            public FontSelector selectorTableContent { get; private set; }

            public Font mChineseFontTabCaption { get; private set; }
            public Font mChineseFontTabContent { get; private set; }
            public Font mChineseFontTabContentItalic { get; private set; }
            public Font mKoreanFontTabCaption { get; private set; }
            public Font mKoreanFontTabContent { get; private set; }
            public Font mKoreanFontTabContentItalic { get; private set; }

            public BaseColor TableCaptionFontColour { get; private set; }
            public BaseColor FontColour { get; private set; }

            public string mQuotePdfOutputLangId { get; private set; }

            public Formattingpdf(string QuoteOutPutLanguage = "")
            {
                TableCaptionFontColour = new BaseColor(175, 40, 46);

                TableCaption7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, TableCaptionFontColour);
                Heading4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, Font.NORMAL, BaseColor.BLACK);
                Heading5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, Font.NORMAL, BaseColor.BLACK);
                Heading7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                Heading6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, Font.NORMAL, BaseColor.BLACK);
                Heading8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, Font.NORMAL, BaseColor.BLACK);
                Heading9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.NORMAL, BaseColor.BLACK);
                Normal = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
                NormalRedColor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.RED);
                Normal6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
                NormalItalic6 = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
                NormalItalic7 = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.ITALIC, BaseColor.BLACK);
                NormalBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, BaseColor.BLACK);
                FontColour = new BaseColor(176, 41, 46);
                BoldForQuoteTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, Font.NORMAL, FontColour);
                TableHeadingBgColor = new BaseColor(215, 215, 215);
                TableFooterBgColor = new BaseColor(253, 233, 217);
                TableBorderColor = BaseColor.GRAY;
                mQuotePdfOutputLangId = QuoteOutPutLanguage;

                //For Chinese characters display
                mChineseFontTabCaption = new Font(chinesebasefont, 7, Font.BOLD, TableCaptionFontColour);
                mChineseFontTabContent = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mChineseFontTabContentItalic = new Font(chinesebasefont, 7, Font.NORMAL, BaseColor.BLACK);

                //For Korean characters display
                mKoreanFontTabCaption = new Font(koreanbasefont, 7, Font.BOLD, TableCaptionFontColour);
                mKoreanFontTabContent = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);
                mKoreanFontTabContentItalic = new Font(koreanbasefont, 7, Font.NORMAL, BaseColor.BLACK);

                selectorTableCaption = new FontSelector();
                selectorTableCaption.AddFont(TableCaption7);
                selectorTableCaption.AddFont(mChineseFontTabCaption);
                selectorTableCaption.AddFont(mKoreanFontTabCaption);

                selectorTableContent = new FontSelector();
                selectorTableContent.AddFont(NormalItalic7);
                selectorTableContent.AddFont(mChineseFontTabContentItalic);
                selectorTableContent.AddFont(mKoreanFontTabContentItalic);

                selectorTableHeading = new FontSelector();
            }

            /// <summary>
            /// Document Heading
            /// </summary>
            /// <param name="headName">Tha Document Name</param>
            /// <param name="hAlign">The h Align</param>
            /// <param name="underline"></param>
            /// <returns></returns>
            ///
            public PdfPCell DocumentHeadingIncrease(string headName, int hAlign = Element.ALIGN_RIGHT, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading9);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading = new FontSelector();
                selectorTableHeading.AddFont(Heading9);
                Font docChiHeading = new Font(chinesebasefont, 13, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 13, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_LEFT, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            public PdfPCell DocumentHeading(string headName, int hAlign = Element.ALIGN_CENTER, bool underline = true)
            {
                var mChunk = new Chunk(headName, Heading8);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                selectorTableHeading.AddFont(Heading8);
                Font docChiHeading = new Font(chinesebasefont, 10, Font.BOLD, BaseColor.BLACK);
                Font docKorHeading = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.BLACK);
                selectorTableHeading.AddFont(docChiHeading);
                selectorTableHeading.AddFont(docKorHeading);
                return new PdfPCell(selectorTableHeading.Process(mChunk.ToString())) { VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = 0 };
            }

            /// <summary>
            /// Subs the heading cell.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <returns></returns>
            public PdfPCell TableCaption(string caption, int vAlign = Element.ALIGN_MIDDLE, int hAlign = Element.ALIGN_LEFT, bool isbold = true, bool IsColor = false, bool isBorder = false)
            {
                if (isbold)
                {
                    FontSelector selectorTableCaptionbold = new FontSelector();
                    Font TableChiHead7;
                    Font TableKorHead7;
                    if (IsColor == false)
                    {
                        selectorTableCaptionbold.AddFont(Heading7);
                        TableChiHead7 = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                        TableKorHead7 = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                        selectorTableCaptionbold.AddFont(TableChiHead7);
                        selectorTableCaptionbold.AddFont(TableKorHead7);
                    }
                    else
                    {
                        selectorTableCaptionbold.AddFont(TableCaption7);
                        Font TableChiCap7 = new Font(chinesebasefont, 9, Font.BOLD, TableCaptionFontColour);
                        Font TableKorCap7 = new Font(koreanbasefont, 9, Font.BOLD, TableCaptionFontColour);
                        selectorTableCaptionbold.AddFont(TableChiCap7);
                        selectorTableCaptionbold.AddFont(TableKorCap7);
                    }
                    return new PdfPCell(selectorTableCaptionbold.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
                else
                {
                    FontSelector selectorTabCaption = new FontSelector();
                    selectorTabCaption.AddFont(Normal6);
                    Font ChiTableNormal6 = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    Font KorTableNormal6 = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                    selectorTabCaption.AddFont(ChiTableNormal6);
                    selectorTabCaption.AddFont(KorTableNormal6);
                    return new PdfPCell(selectorTabCaption.Process(caption)) { VerticalAlignment = vAlign, HorizontalAlignment = hAlign, Padding = 0, PaddingBottom = 3f, Border = isBorder == false ? 0 : Rectangle.BOX, BorderColor = TableBorderColor };
                }
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true, bool isLowFont = false)
            {
                if (isLowFont)
                    return TableHeading(caption, Heading5, hAlign, underline);
                else
                    return TableHeading(caption, Heading6, hAlign, underline);
            }

            /// <summary>
            /// Tables the footer.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="hAlign">The h align.</param>
            /// <param name="underline">if set to <c>true</c> [underline].</param>
            /// <returns></returns>
            public PdfPCell TableFooter(string caption, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                var mPdfCell = TableHeading(caption, Heading6, hAlign, underline);
                mPdfCell.BackgroundColor = TableFooterBgColor;
                return mPdfCell;
            }

            /// <summary>
            /// Tables the heading.
            /// </summary>
            /// <param name="caption">The caption.</param>
            /// <param name="headingFont">The heading font.</param>
            /// <param name="hAlign">The h align.</param>
            /// <returns></returns>
            public PdfPCell TableHeading(string caption, Font headingFont, int hAlign = Element.ALIGN_LEFT, bool underline = true)
            {
                int padding = 3;
                var mChunk = new Chunk(caption, headingFont);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);

                FontSelector selectorTabHeading = new FontSelector();
                selectorTabHeading.AddFont(headingFont);
                Font ChiTabHeading = new Font(chinesebasefont, 8, Font.BOLD, BaseColor.BLACK);
                Font KorTabHeading = new Font(koreanbasefont, 8, Font.BOLD, BaseColor.BLACK);
                selectorTabHeading.AddFont(ChiTabHeading);
                selectorTabHeading.AddFont(KorTabHeading);
                return new PdfPCell(selectorTabHeading.Process(mChunk.ToString())) { BorderColor = TableBorderColor, BackgroundColor = TableHeadingBgColor, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = 2, PaddingRight = 2 };//padding - 2
            }

            /// <summary>
            /// Tables Content
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCell(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabContentCell = new FontSelector();
                selectorTabContentCell.AddFont(Normal);
                Font ChiTabContentcell = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font KorTabContentcell = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorTabContentCell.AddFont(ChiTabContentcell);
                selectorTabContentCell.AddFont(KorTabContentcell);
                return new PdfPCell(selectorTabContentCell.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            public PdfPCell TableContentCellNormalandItalic(string cellData, string cellItalicData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                Paragraph mPgh = new Paragraph();
                FontSelector selectorNormal = new FontSelector();
                selectorNormal.AddFont(Normal);
                Font fntChi1 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor1 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormal.AddFont(fntChi1);
                selectorNormal.AddFont(fntKor1);

                Phrase ph1 = selectorNormal.Process(cellData);
                FontSelector selectorNormItalic = new FontSelector();
                selectorNormItalic.AddFont(Normal);
                Font fntChi2 = new Font(chinesebasefont, 9, Font.NORMAL, BaseColor.BLACK);
                Font fntKor2 = new Font(koreanbasefont, 9, Font.NORMAL, BaseColor.BLACK);
                selectorNormItalic.AddFont(fntChi2);
                selectorNormItalic.AddFont(fntKor2);
                Phrase ph2 = selectorNormItalic.Process(cellItalicData);
                mPgh.Add(ph1);
                if (!string.IsNullOrEmpty(cellItalicData))
                    mPgh.Add(ph2);
                return new PdfPCell(mPgh) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content Italic
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellItalic(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellItalic = new FontSelector();
                selectorTabConentcellItalic.AddFont(NormalItalic7);
                Font ChiContentcell = new Font(chinesebasefont, 9, Font.ITALIC, BaseColor.BLACK);
                Font KorContentcell = new Font(koreanbasefont, 9, Font.ITALIC, BaseColor.BLACK);
                selectorTabConentcellItalic.AddFont(ChiContentcell);
                selectorTabConentcellItalic.AddFont(KorContentcell);
                return new PdfPCell(selectorTabConentcellItalic.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Tables Content with red color
            /// </summary>
            /// <param name="cellData"> The cell data</param>
            /// <param name="hAlign">The h Align</param>
            /// <returns></returns>
            public PdfPCell TableContentCellRedColor(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellRedColor = new FontSelector();
                selectorTabConentcellRedColor.AddFont(NormalRedColor);
                Font fntChiRed = new Font(chinesebasefont, 10, Font.NORMAL, BaseColor.RED);
                Font fntKorRed = new Font(koreanbasefont, 10, Font.NORMAL, BaseColor.RED);
                selectorTabConentcellRedColor.AddFont(fntChiRed);
                selectorTabConentcellRedColor.AddFont(fntKorRed);
                return new PdfPCell(selectorTabConentcellRedColor.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBold(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBold = new FontSelector();
                selectorTabConentcellBold.AddFont(NormalBold);
                Font fntChiBold = new Font(chinesebasefont, 9, Font.BOLD, BaseColor.BLACK);
                Font fntKorBold = new Font(koreanbasefont, 9, Font.BOLD, BaseColor.BLACK);
                selectorTabConentcellBold.AddFont(fntChiBold);
                selectorTabConentcellBold.AddFont(fntKorBold);
                return new PdfPCell(selectorTabConentcellBold.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            /// <summary>
            /// Table Content Celldata Bold for Quotation Grand Total
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="border"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldForQuoteTotal(string cellData, int hAlign = Element.ALIGN_LEFT, int border = Rectangle.NO_BORDER)
            {
                int padding = 2;
                FontSelector selectorTabConentcellBoldQT = new FontSelector();
                selectorTabConentcellBoldQT.AddFont(BoldForQuoteTotal);
                Font fntChiBorderQT = new Font(chinesebasefont, 10, Font.BOLD, FontColour);
                Font fntKorBorderQT = new Font(koreanbasefont, 10, Font.BOLD, FontColour);
                selectorTabConentcellBoldQT.AddFont(fntChiBorderQT);
                selectorTabConentcellBoldQT.AddFont(fntKorBorderQT);

                return new PdfPCell(selectorTabConentcellBoldQT.Process(cellData)) { Border = border, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding };
            }

            /// <summary>
            /// Table Content Cell Data Bold,Underline
            /// </summary>
            /// <param name="cellData"></param>
            /// <param name="hAlign"></param>
            /// <param name="underline"></param>
            /// <returns></returns>
            public PdfPCell TableContentCellBoldUnderline(string cellData, int hAlign = Element.ALIGN_LEFT, bool underline = false)
            {
                int padding = 2;
                var mChunk = new Chunk(cellData, Heading6);
                if (underline)
                    mChunk.SetUnderline(0.1f, -2f);
                FontSelector selectorTabConentcellBoldline = new FontSelector();
                selectorTabConentcellBoldline.AddFont(Heading6);
                Font fntChiBoldline = new Font(chinesebasefont, 8, Font.NORMAL, BaseColor.BLACK);
                Font fntKorBoldline = new Font(koreanbasefont, 8, Font.NORMAL, BaseColor.BLACK);
                selectorTabConentcellBoldline.AddFont(fntChiBoldline);
                selectorTabConentcellBoldline.AddFont(fntKorBoldline);

                return new PdfPCell(selectorTabConentcellBoldline.Process(mChunk.ToString())) { Border = 0, BorderColor = TableBorderColor, BackgroundColor = BaseColor.WHITE, VerticalAlignment = Element.ALIGN_TOP, HorizontalAlignment = hAlign, Padding = padding, PaddingTop = padding - 2 };
            }

            ///// <summary>
            ///// Lines the separator.
            ///// </summary>
            ///// <returns></returns>
            public Chunk LineSeparator()
            {
                return new Chunk(new LineSeparator(4f, 100f, TableHeadingBgColor, Element.ALIGN_CENTER, -1));
            }

            public Chunk ChunkText(string caption, bool isColor = false)
            {
                if (isColor)
                    return new Chunk(caption, TableCaption7);
                else
                    return new Chunk(caption, Heading7);
            }
        }

        #endregion PDF Formatting

        #region ReferenceFunctions

        private QuotationEntity Map_Preview(QuotationEntity mDBEntity)
        {
            List<QuotationChargeEntity> mUIOrg = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1118).ToList();
            List<QuotationChargeEntity> mUIDes = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1117).ToList();
            List<QuotationChargeEntity> mUIInter = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 1116).ToList();
            List<QuotationChargeEntity> mUIAdditional = mDBEntity.QuotationCharges.AsEnumerable().Where(x => x.RouteTypeId == 250001).ToList();


            List<QOrgChargeEntity> mUIModel = Mapper.Map<List<QuotationChargeEntity>, List<QOrgChargeEntity>>(mUIOrg);
            List<QDesChargeEntity> mUIModelDes = Mapper.Map<List<QuotationChargeEntity>, List<QDesChargeEntity>>(mUIDes);
            List<QInterChargeEntity> mUIModelInter = Mapper.Map<List<QuotationChargeEntity>, List<QInterChargeEntity>>(mUIInter);
            List<QAdditionalChargeEntity> mUIModelAdditional = Mapper.Map<List<QuotationChargeEntity>, List<QAdditionalChargeEntity>>(mUIAdditional);

            mDBEntity.QOrgCharges = mUIModel.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QDesCharges = mUIModelDes.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QInterCharges = mUIModelInter.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();
            mDBEntity.QAdditionalCharges = mUIModelAdditional.OrderBy(x => x.CHARGENAME, StringComparer.CurrentCultureIgnoreCase).ThenBy(x => x.UOM, StringComparer.CurrentCultureIgnoreCase).ToList();

            UserDBFactory ud = new UserDBFactory();
            UserEntity UE = ud.GetUserFirstAndLastName(mDBEntity.CREATEDBY);
            mDBEntity.FirstName = UE.FIRSTNAME;

            //Remove unnecessry items
            mDBEntity.Users = null;
            mDBEntity.TermAndConditions = null;
            mDBEntity.QuotationCharges = null;

            return mDBEntity;
        }
        private void Co2Emission(QuotationEntity mDBEntity)
        {
            //----------------------------Co2 Emission----------------------------
            if (mDBEntity.co2emission == 0)
            {
                double co2 = Co2EmissionCalculation(mDBEntity);
                mDBEntity.co2emission = Convert.ToDecimal(co2);
            }
            //-------------------------------------------------------------------------
        }

        public void SendmailtoNPC(QuotationEntity mDBEntity)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            string subject = "Overweight/CBM threshold quote request - " + DUIModels.QuotationNumber + EnvironmentName;
            string NPCEmail = System.Configuration.ConfigurationManager.AppSettings["NPCEmail"];
            string help = System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string body = string.Empty;
            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/OverweightCBM_threshold.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { body = reader.ReadToEnd(); }
            body = body.Replace("{Country}", mDBEntity.OCOUNTRYNAME);
            body = body.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
            body = body.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            body = body.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);

            if (mDBEntity.PRODUCTNAME == "Ocean")
            {
                try
                {
                    DataTable dt = mFactory.GetNPCdetails(Convert.ToInt64(mDBEntity.OCOUNTRYID));
                    if ((dt.Rows.Count > 0) && (mDBEntity.TOTALGROSSWEIGHT > Convert.ToDecimal(dt.Rows[0]["OCEANMAXWEIGHT"].ToString()) || mDBEntity.TOTALCBM > Convert.ToDecimal(dt.Rows[0]["OCEANCBM"].ToString())))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            bytes = GeneratePDF(memoryStream, DUIModels);
                            memoryStream.Close();
                        }
                        string Emailid = (dt.Rows[0]["OCEANNPCMAILID"].ToString() != null && dt.Rows[0]["OCEANNPCMAILID"].ToString().Trim() != "") ? dt.Rows[0]["OCEANNPCMAILID"].ToString() : NPCEmail;
                        Emailid = Emailid + "," + help;
                        SendMail(Emailid, body, subject, "Overweight/CBM threshold quote request", false, bytes, DUIModels.QuotationNumber);
                    }
                }
                catch (Exception ex)
                {
                    LogError("api/Quotation", "SendmailtoNPC", ex.Message.ToString(), ex.StackTrace.ToString());
                }
            }
            else
            {
                try
                {
                    DataTable dt = mFactory.GetNPCdetails(Convert.ToInt64(mDBEntity.OCOUNTRYID));

                    if ((dt.Rows.Count > 0) && (mDBEntity.TOTALGROSSWEIGHT > Convert.ToDecimal(dt.Rows[0]["AIRMAXWEIGHT"].ToString()) || mDBEntity.TOTALCBM > Convert.ToDecimal(dt.Rows[0]["AIRCBM"].ToString())))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            bytes = GeneratePDF(memoryStream, DUIModels);
                            memoryStream.Close();
                        }
                        string Emailid = (dt.Rows[0]["AIRNPCMAILID"].ToString() != null && dt.Rows[0]["AIRNPCMAILID"].ToString().Trim() != "") ? dt.Rows[0]["AIRNPCMAILID"].ToString() : NPCEmail;
                        Emailid = Emailid + "," + help;
                        SendMail(Emailid, body, subject, "Overweight/CBM threshold quote request", false, bytes, DUIModels.QuotationNumber);
                    }
                }
                catch (Exception ex)
                {
                    LogError("api/Quotation", "SendmailtoNPC", ex.Message.ToString(), ex.StackTrace.ToString());
                }
            }
        }
        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }
            return dist;
        }

        public double Co2EmissionCalculation(QuotationEntity model)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            double objText = 0;
            double OCo2 = 0; double PCo2 = 0; double DCo2 = 0;
            double oplacelat = 0; double oplacelon = 0;
            double oportlat = 0; double oportlon = 0;
            double dplacelat = 0; double dplacelon = 0;
            double dportlat = 0; double dportlon = 0;

            DataSet ds = new DataSet();
            if (model.MOVEMENTTYPENAME == "Port to port")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPORTID, model.DESTINATIONPORTID);
                    DataRow[] orows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (orows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] drows = ds.Tables[0].Select("UNLOCATIONID = '" + model.DESTINATIONPORTID + "'");
                    if (drows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Door to door")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.ORIGINPLACEID != 0) && (model.ORIGINPLACEID != null)) && ((model.DESTINATIONPLACEID != 0) && (model.DESTINATIONPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPLACEID, model.ORIGINPORTID, model.DESTINATIONPLACEID, model.DESTINATIONPORTID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID);
                    if (oprows.Length > 0)
                    {
                        oplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[1]);
                        oplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] orows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (orows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] drows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID);
                    if (drows.Length > 0)
                    {
                        dplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[1]);
                        dplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (dprows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Door to port")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.ORIGINPLACEID != 0) && (model.ORIGINPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPLACEID, model.ORIGINPORTID, model.DESTINATIONPORTID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = '" + model.ORIGINPLACEID + "'");
                    if (oprows.Length > 0)
                    {
                        oplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[1]);
                        oplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPLACEID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (dprows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] prows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (prows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                }
            }
            else if (model.MOVEMENTTYPENAME == "Port to door")
            {
                if (((model.ORIGINPORTID != 0) && (model.ORIGINPORTID != null)) && ((model.DESTINATIONPORTID != 0) && (model.DESTINATIONPORTID != null)) && ((model.DESTINATIONPLACEID != 0) && (model.DESTINATIONPLACEID != null)))
                {
                    ds = mFactory.GetLatLon(model.ORIGINPORTID, model.DESTINATIONPORTID, model.DESTINATIONPLACEID);
                    DataRow[] oprows = ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID);
                    if (oprows.Length > 0)
                    {
                        oportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[1]);
                        oportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.ORIGINPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] prows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID);
                    if (prows.Length > 0)
                    {
                        dportlat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[1]);
                        dportlon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPORTID)[0].ItemArray[2]);
                    }
                    DataRow[] dprows = ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID);
                    if (dprows.Length > 0)
                    {
                        dplacelat = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[1]);
                        dplacelon = Convert.ToDouble(ds.Tables[0].Select("UNLOCATIONID = " + model.DESTINATIONPLACEID)[0].ItemArray[2]);
                    }
                }
            }

            if (oplacelat != 0 && oplacelon != 0 && oportlat != 0 && oportlon != 0)
            {
                double Origindistance = DistanceTo(oplacelat, oplacelon, oportlat, oportlon, 'K');
                double OADP = 0.2;
                double Adjusteddistance = Origindistance + (Origindistance * OADP);
                double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                double OCWMT = 0.001;
                double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                double TonneKM = Adjusteddistance * OcargoweightMT;
                double KGCo2 = 0.13096;
                double KGCo2emitted = KGCo2 * TonneKM;
                OCo2 = KGCo2emitted;//* 0.001                
            }
            if (oportlat != 0 && oportlon != 0 && dportlat != 0 && dportlon != 0)
            {
                double Origindistance = DistanceTo(oportlat, oportlon, dportlat, dportlon, 'K');
                double OADP = 0;
                if (model.PRODUCTNAME.ToUpperInvariant() == "AIR")
                {
                    OADP = 0.08;
                }
                else
                {
                    OADP = 0.15;
                }
                double Adjusteddistance = Origindistance + (Origindistance * OADP);
                double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                double OCWMT = 0.001;
                double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                double TonneKM = Adjusteddistance * OcargoweightMT;
                double KGCo2 = 0;
                if (model.PRODUCTNAME.ToUpperInvariant() == "OCEAN")
                {
                    KGCo2 = 0.019065;
                }
                else { KGCo2 = 0.7129; }

                double KGCo2emitted = KGCo2 * TonneKM;
                PCo2 = KGCo2emitted;//* 0.001               
            }
            if (dportlat != 0 && dportlon != 0 && dplacelat != 0 && dplacelon != 0)
            {
                double Origindistance = DistanceTo(dportlat, dportlon, dplacelat, dplacelon, 'K');
                double OADP = 0.2;
                double Adjusteddistance = Origindistance + (Origindistance * OADP);
                double OcargoweightKG = Convert.ToDouble(model.TOTALGROSSWEIGHT);
                double OCWMT = 0.001;
                double OcargoweightMT = Convert.ToDouble(OcargoweightKG * OCWMT);
                double TonneKM = Adjusteddistance * OcargoweightMT;
                double KGCo2 = 0.13096;
                double KGCo2emitted = KGCo2 * TonneKM;
            }
            objText = OCo2 + PCo2 + DCo2;
            mFactory.UpdateCo2(model.QUOTATIONID, objText);

            return objText;
        }
        private void FillPortPairs(QuotationModel model)
        {
            switch (model.MovementTypeId.Value)
            {
                case MovementTypeEnum.PorttoPort:
                    model.OriginPortId = model.OriginPlaceId;
                    model.DestinationPortid = model.DestinationPlaceId;
                    UpdateODPortPairs(model.OriginPlaceId, model.DestinationPlaceId, model);
                    break;

                case MovementTypeEnum.DoortoPort:
                    model.DestinationPortid = model.DestinationPlaceId;
                    UpdateODPortPairs(model.OriginPlaceId, model.DestinationPlaceId, model);
                    break;

                case MovementTypeEnum.PorttoDoor:
                    model.OriginPortId = model.OriginPlaceId;
                    UpdateODPortPairs(model.OriginPlaceId, model.DestinationPlaceId, model);
                    break;

                default:
                    UpdateODPortPairs(model.OriginPlaceId, model.DestinationPlaceId, model);
                    break;
            }
        }

        private void UpdateODPortPairs(long? O, long? D, QuotationModel model)
        {
            QuotationDBFactory QDF = new QuotationDBFactory();

            PortPairsEntity PPMO = QDF.GETPORTPAIRSDATA(O);
            PortPairsEntity PPMD = QDF.GETPORTPAIRSDATA(D);

            switch (model.MovementTypeId.Value)
            {
                case MovementTypeEnum.PorttoPort:
                    //O Port
                    model.OriginPortName = PPMO.NAME;
                    model.OriginPortCode = PPMO.CODE;
                    model.OPortCountryId = Convert.ToInt64(PPMO.COUNTRYID);
                    model.OriginsubdivisionCode = PPMO.SUBDIVISIONNAME;
                    model.OCountryCode = PPMO.COUNTRYCODE;

                    //D Port
                    model.DestinationPortName = PPMD.NAME;
                    model.DestinationPortCode = PPMD.CODE;
                    model.DPortCountryId = Convert.ToInt64(PPMD.COUNTRYID);
                    model.DestinationsubdivisionCode = PPMD.SUBDIVISIONNAME;
                    model.DCountryCode = PPMD.COUNTRYCODE;

                    break;

                case MovementTypeEnum.DoortoPort:
                    //O Place
                    model.OriginPlaceName = PPMO.NAME;
                    model.OriginPlaceCode = PPMO.CODE;
                    model.OCountryId = Convert.ToInt64(PPMO.COUNTRYID);
                    model.OriginsubdivisionCode = PPMO.SUBDIVISIONNAME;
                    model.OCountryCode = PPMO.COUNTRYCODE;

                    //D Port
                    model.DestinationPortName = PPMD.NAME;
                    model.DestinationPortCode = PPMD.CODE;
                    model.DPortCountryId = Convert.ToInt64(PPMD.COUNTRYID);
                    model.DestinationsubdivisionCode = PPMD.SUBDIVISIONNAME;
                    model.DCountryCode = PPMD.COUNTRYCODE;
                    break;
                case MovementTypeEnum.PorttoDoor:
                    //O Port
                    model.OriginPortName = PPMO.NAME;
                    model.OriginPortCode = PPMO.CODE;
                    model.OPortCountryId = Convert.ToInt64(PPMO.COUNTRYID);
                    model.OriginsubdivisionCode = PPMO.SUBDIVISIONNAME;
                    model.OCountryCode = PPMO.COUNTRYCODE;

                    //D Place
                    model.DestinationPlaceName = PPMD.NAME;
                    model.DestinationPlaceCode = PPMD.CODE;
                    model.DCountryId = Convert.ToInt64(PPMD.COUNTRYID);
                    model.DestinationsubdivisionCode = PPMD.SUBDIVISIONNAME;
                    model.DCountryCode = PPMD.COUNTRYCODE;
                    break;

                default:
                    //O Place
                    model.OriginPlaceName = PPMO.NAME;
                    model.OriginPlaceCode = PPMO.CODE;
                    model.OCountryId = Convert.ToInt64(PPMO.COUNTRYID);
                    model.OriginsubdivisionCode = PPMO.SUBDIVISIONNAME;
                    model.OCountryCode = PPMO.COUNTRYCODE;

                    //D Place
                    model.DestinationPlaceName = PPMD.NAME;
                    model.DestinationPlaceCode = PPMD.CODE;
                    model.DCountryId = Convert.ToInt64(PPMD.COUNTRYID);
                    model.DestinationsubdivisionCode = PPMD.SUBDIVISIONNAME;
                    model.DCountryCode = PPMD.COUNTRYCODE;

                    break;
            }
        }

        public IHttpActionResult Post([FromBody]QuotationModel value)
        {
            value.QuotationNumber = DateTime.Now.ToShortDateString();
            return Ok(value);
        }

        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void SendMailForGSSCQuotes(string number, int threshold, int hazordous, int notavailable)
        {
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCEmail"];
            Email = Email + "," + System.Configuration.ConfigurationManager.AppSettings["ContactusEmail"];
            string URL = System.Configuration.ConfigurationManager.AppSettings["GSSCEUrl"];
            subject = "Request for Quote submitted in GSSC Flow : " + number;
            string mbody = "Dear User,";
            mbody = mbody + "<br/> <br/>" + "A Request for Quote is  set to GSSC and available because of";

            if (notavailable > 0)
                mbody = mbody + "<br/> " + "\u2022 " + " Rates are not available";
            if (threshold == 1)
                mbody = mbody + "<br/> " + "\u2022 " + " Threshold value met";
            if (hazordous == 1)
                mbody = mbody + "<br/> " + "\u2022 " + " Hazordous Goods available";

            mbody = mbody + "<br/> <br/>" + " Please use the below URL to access the application";
            mbody = mbody + "<br/>" + URL;

            mbody = mbody + "<br/> <br/>" + "Regards";
            mbody = mbody + "<br/>" + "Shipa Freight Team";

            SendMail(Email, mbody, subject);
        }

        public void SendMail(string Email, string body, string subject, byte[] bytes = null, string QuotationNumber = "")
        {
            MailMessage message = new MailMessage();

            if (Email.Contains(','))
            {
                foreach (string s in Email.Split(','))
                {
                    message.Bcc.Add(new MailAddress(s));
                }
            }
            else
            {
                message.CC.Add(new MailAddress(Email));
            }
            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            if (bytes != null)
                message.Attachments.Add(new Attachment(new MemoryStream(bytes), QuotationNumber + ".pdf"));
            message.IsBodyHtml = true;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
        }


        public void Assignvalues(QuotationPreviewModel mUIModelPreview)
        {
            if (!ReferenceEquals(mUIModelPreview, null))
            {
                foreach (var mShipment in mUIModelPreview.ShipmentItems)
                {
                    cargodetails = cargodetails +
                                      "<tr>" +
                                      "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                      "<span class='inner-detail-head'" + ">Package Type</span><br>" +
                                      "<span class='inner-detail-cont'" + ">" + mShipment.PackageTypeName + "</span></td>" +
                                      "<td width='300' style=" + "'padding-top: 10px; padding-bottom: 10px;'" + ">" +
                                      "<span class='inner-detail-head'" + ">Quantity in Pieces</span><br>" +
                                      "<span class='inner-detail-cont'" + "> " + mShipment.Quantity.ToString() + "</span></td> </tr>";
                }
                if (mUIModelPreview.MovementTypeName == "Door to door" || mUIModelPreview.MovementTypeName == "Door to Door")
                {
                    mPlaceOfReceipt = string.Format("{0} {1}", mUIModelPreview.OriginPlaceName, mUIModelPreview.OriginSubDivisionCode != null ? "," + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0} {1}", mUIModelPreview.DestinationPlaceName, mUIModelPreview.DestinationSubDivisionCode != null ? "," + mUIModelPreview.DestinationSubDivisionCode : "");
                    mPRLableName = "Origin City"; mPDLableName = "Destination City";
                }
                else if (mUIModelPreview.MovementTypeName == "Door to port")
                {
                    mPlaceOfReceipt = string.Format("{0} {1}", mUIModelPreview.OriginPlaceName, mUIModelPreview.OriginSubDivisionCode != null ? "," + mUIModelPreview.OriginSubDivisionCode : "");
                    mPlaceOfDelivery = string.Format("{0}, {1}", mUIModelPreview.DestinationPortCode, mUIModelPreview.DestinationPortName);
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination AirPort";
                    }
                    else
                    {
                        mPRLableName = "Origin City"; mPDLableName = "Destination Port";
                    }
                }
                else if (mUIModelPreview.MovementTypeName == "Port to port" || mUIModelPreview.MovementTypeName == "Port to Port")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", mUIModelPreview.OriginPortCode, mUIModelPreview.OriginPortName);
                    mPlaceOfDelivery = string.Format("{0}, {1}", mUIModelPreview.DestinationPortCode, mUIModelPreview.DestinationPortName);
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin AirPort"; mPDLableName = "Destination AirPort";
                    }
                    else
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination Port";
                    }
                }
                else if (mUIModelPreview.MovementTypeName == "Port to door")
                {
                    mPlaceOfReceipt = string.Format("{0}, {1}", mUIModelPreview.OriginPortCode, mUIModelPreview.OriginPortName);
                    mPlaceOfDelivery = string.Format("{0} {1}", mUIModelPreview.DestinationPlaceName, mUIModelPreview.DestinationSubDivisionCode != null ? "," + mUIModelPreview.DestinationSubDivisionCode : "");
                    if (mUIModelPreview.ProductName.ToUpperInvariant() == "AIR")
                    {
                        mPRLableName = "Origin AirPort"; mPDLableName = "Destination City";
                    }
                    else
                    {
                        mPRLableName = "Origin Port"; mPDLableName = "Destination City";
                    }
                }
            }
        }

        public HttpResponseMessage BaseResponse(dynamic data, HttpStatusCode code)
        {
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    StatusCode = code,
                    Data = data
                })
            };
        }

        #endregion

        #region mails
        private void SendMailForMissingChargesGSSCQuotes(QuotationEntity mDBEntity)
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            DUIModels = Mapper.Map<DBFactory.Entities.QuotationEntity, QuotationPreviewModel>(mDBEntity);
            string Email = System.Configuration.ConfigurationManager.AppSettings["GSSCMCEmail"];
            string PAEmail = System.Configuration.ConfigurationManager.AppSettings["GSSCMCPAEmail"];
            string CountryManager = System.Configuration.ConfigurationManager.AppSettings["GSSCManagerEmail"];
            string CountryNPC = System.Configuration.ConfigurationManager.AppSettings["GSSCNPCEmail"];
            string NPCAndManager = CountryNPC + "," + CountryManager;
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            subject = "Attention: Quote " + DUIModels.QuotationNumber + " with missing " + " " + mDBEntity.GSSCREASON.ToLower() + " " + "charges sent to customer" + EnvironmentName;
            string mbody = string.Empty;
            string quoteInfo = string.Empty;
            quoteInfo = "<table cellpadding=" + 5 + " cellspacing=" + 2 + " width = " + 600 + ">";
            quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Mode of Transport" + "</td><td>" + " : " + "</td><td>" + DUIModels.ProductName + "</td><td style=\"font-weight:bold;\"> " + "Movement Type " + "</td><td>" + " : " + "</td><td>" + DUIModels.MovementTypeName + "</td> </tr>";
            quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin country" + "</td><td>" + " : " + "</td><td>" + mDBEntity.OCOUNTRYNAME + "</td><td style=\"font-weight:bold;\"> " + "Destination country " + "</td><td>" + " : " + "</td><td>" + mDBEntity.DCOUNTRYNAME;
            if (DUIModels.OriginPlaceName != null && DUIModels.DestinationPlaceName != null)
            {
                quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin City" + "</td><td>" + " : " + "</td><td>" + DUIModels.OriginPlaceName + "</td><td style=\"font-weight:bold;\"> " + "Destination City" + "</td><td>" + " : " + "</td><td>" + DUIModels.DestinationPlaceName;
            }
            else
            {
                if (DUIModels.OriginPlaceName != null)
                {
                    quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin City" + "</td><td>" + " : " + "</td><td>" + DUIModels.OriginPlaceName;
                }
                if (DUIModels.DestinationPlaceName != null)
                {
                    quoteInfo = quoteInfo + "<tr><td>" + "</td><td>" + "</td><td>" + DUIModels.OriginPlaceName + "</td><td style=\"font-weight:bold;\"> " + "Destination City" + "</td><td>" + " : " + "</td><td>" + DUIModels.DestinationPlaceName;
                }
            }
            if (DUIModels.OriginPortName != null && DUIModels.DestinationPortName != null)
            {
                quoteInfo = quoteInfo + "<tr><td style=\"font-weight:bold;\">" + "Origin Port" + "</td><td>" + " : " + "</td><td>" + DUIModels.OriginPortName + "</td><td style=\"font-weight:bold;\"> " + "Destination Port" + "</td><td>" + " : " + "</td><td>" + DUIModels.DestinationPortName;
            }
            quoteInfo = quoteInfo + "</table>";

            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/MissingChargesQuote.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{QuotationNumber}", DUIModels.QuotationNumber);
            mbody = mbody.Replace("{GsscReason}", mDBEntity.GSSCREASON.ToLower());
            mbody = mbody.Replace("{QuoteInfo}", quoteInfo);
            mbody = mbody.Replace("{focisurl}", System.Configuration.ConfigurationManager.AppSettings["FOCISURL"]);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bytes = APIDynamicClass.GeneratePDF(memoryStream, DUIModels);
                memoryStream.Close();
            }
            string countryId = string.Empty;
            if (string.IsNullOrEmpty(EnvironmentName))
            {
                string reason = mDBEntity.GSSCREASON; ;
                if (!string.IsNullOrEmpty(reason))
                {
                    string[] gsscReason = reason.Split(',');
                    int gsscReasonCount = gsscReason.Count();
                    if (gsscReasonCount > 1)
                    {
                        countryId = mDBEntity.OCOUNTRYID + "," + mDBEntity.DCOUNTRYID;
                    }
                    else
                    {
                        countryId = gsscReason[0].ToUpper() == "ORIGIN" ? Convert.ToString(mDBEntity.OCOUNTRYID) : Convert.ToString(mDBEntity.DCOUNTRYID);
                    }
                }
                string emails = mFactory.GetNPCAndCountryManagerEmailsForNPC(countryId);
                NPCAndManager = string.IsNullOrEmpty(emails) ? NPCAndManager : emails;
            }
            Email = Email + "," + PAEmail + "," + NPCAndManager;
            SendMail(Email, mbody, subject, "Quote with missing Origin/Destination charges", false, bytes, DUIModels.QuotationNumber, DUIModels.CreatedBy);
        }
        public void SendMail(string Email, string body, string subject, string submodule, bool MailStore, byte[] bytes = null,
            string QuotationNumber = "", string CreatedBy = "", long docid = 0)
        {
            string TO = string.Empty;
            string BCC = string.Empty;
            string CC = string.Empty;
            string attachment = string.Empty;
            MailMessage message = new MailMessage();
            if (Email.Contains(';'))
            {
                if (submodule.ToUpper() == "QUOTE WITH MISSING ORIGIN/DESTINATION CHARGES")
                {

                    foreach (string s in Email.Split(';'))
                    {
                        message.To.Add(new MailAddress(s));
                        TO = s + ",";
                    }
                }
                else
                {
                    foreach (string s in Email.Split(';'))
                    {
                        message.Bcc.Add(new MailAddress(s));
                        BCC = s + ",";
                    }
                }
            }
            else if (Email.Contains(','))
            {
                if (submodule.ToUpper() == "QUOTE WITH MISSING ORIGIN/DESTINATION CHARGES")
                {

                    foreach (string s in Email.Split(','))
                    {
                        message.To.Add(new MailAddress(s));
                    }
                    TO = Email;
                }
                else
                {
                    foreach (string s in Email.Split(','))
                    {
                        message.Bcc.Add(new MailAddress(s));
                    }
                    BCC = Email;
                }
            }
            else
            {
                if (submodule.ToUpper() == "QUOTE WITH MISSING ORIGIN/DESTINATION CHARGES")
                {
                    message.To.Add(new MailAddress(Email));
                    TO = Email;
                }
                else if (submodule.ToUpper() == "CONTACT US REQUEST")
                {
                    message.To.Add(new MailAddress(Email));
                    TO = Email;
                }
                else
                {
                    message.CC.Add(new MailAddress(Email));
                    CC = Email;
                }
            }
            message.From = new MailAddress("noreply@shipafreight.com");
            message.Subject = subject;
            if (bytes != null)
                message.Attachments.Add(new Attachment(new MemoryStream(bytes), QuotationNumber + ".pdf"));

            message.IsBodyHtml = true;
            message.Body = body;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            client.Dispose();
            message.Dispose();
            APIDynamicClass.SendMailAndStoreMailCommunication("noreply@shipafreight.com", TO, CC, BCC,
             "Quotation", submodule, QuotationNumber.ToString(), CreatedBy, body, docid.ToString(), MailStore, message.Subject.ToString());

        }

        private void Sendmail_CollaborationQuote(string userId, string userId_supplier)
        {
            string EnvironmentName = APIDynamicClass.GetEnvironmentName();
            string msubject = "New Quote Available for Your Shipment" + EnvironmentName;
            string toEmail = userId;
            string mbody = string.Empty;
            string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/Collaboration-Quote.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{UserId}", userId_supplier);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMail(toEmail, mbody, msubject, "Collaboration quote request", false, null, "");
        }

        private void Sendmail_NoCollaborationQuote(string userId, string userId_supplier)
        {
            string msubject = "No change to Weight/Dimensions";
            string toEmail = userId;
            string mbody = string.Empty;
            string mapPath = HttpContext.Current.Server.MapPath("~/App_Data/Collaboration-NoQuote.html");
            using (StreamReader reader = new StreamReader(mapPath))
            { mbody = reader.ReadToEnd(); }
            mbody = mbody.Replace("{UserId}", userId_supplier);
            mbody = mbody.Replace("{webappurl}", System.Configuration.ConfigurationManager.AppSettings["APPURL"]);
            mbody = mbody.Replace("{copyright}", "&copy; " + System.Configuration.ConfigurationManager.AppSettings["Copyright"]);
            SendMail(toEmail, mbody, msubject, "Collaboration No quote", false, null, "");
        }
        #endregion


    }


    #region Models
    public class SactionedModel
    {
        public int OriginCountryId { get; set; }
        public int DestinationCountryId { get; set; }
        public int ProductId { get; set; }
        public string PackageTypes { get; set; }
    }

    #endregion Models
}