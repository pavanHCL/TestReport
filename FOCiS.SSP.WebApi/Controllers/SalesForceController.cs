﻿using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.WebApi.InterfaceManager;
using FOCiS.SSP.WebApi.ShipaFreight.Components.SalesForceIntegration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class SalesForceController : BaseController
    {
        SalesForcePushObject SalesForceObject = new SalesForcePushObject();
        [Route("api/salesforce/PushDatatoMessageQueue")]
        [HttpPost]
        public async Task<SalesForceEntity> PushDatatoMessageQueue(SalesForceEntity SalesForceEntity)
        {
          
            try
            {
                SalesForceEntity = await Task.Run(() => SalesForceObject.PushDatatoMessageQueue(SalesForceEntity)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return SalesForceEntity;
        }

        [Route("api/salesforce/GetDataFromShipaHelpDesk")]
        [HttpPost]
        public HttpResponseMessage GetDataFromShipaHelpDesk([FromBody]object RequestObject)
        {
            try
            {

               string jsondata = RequestObject.ToString();
               FocisShipaData focisShipaObject = JsonConvert.DeserializeObject<FocisShipaData>(jsondata);
               SalesForceObject.GetQuotationDataById(Convert.ToInt64(focisShipaObject.ID), focisShipaObject.TYPE);               
               return new HttpResponseMessage()
               {
                   Content = new JsonContent(new
                   {
                       StatusCode = HttpStatusCode.OK,
                       Message = "Success"
                   })
               };
            }
            catch (Exception ex)
            {
                LogError("api/salesforce/GetDataFromShipaHelpDesk", "GetDataFromShipaHelpDesk", ex.Message.ToString(), ex.StackTrace.ToString());
                return new HttpResponseMessage()
                {
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.ExpectationFailed,
                        Message = ex.Message
                    })
                };

            }
        }

    }
}