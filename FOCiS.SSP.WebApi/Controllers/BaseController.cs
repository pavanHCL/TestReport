﻿using FOCiS.SSP.DBFactory;
using FOCiS.SSP.Models.UserManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace FOCiS.SSP.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        public T GetDataFromApi<T>(string endpoint, string method, object pun)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(endpoint);
                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Method = method;
                string inputJson = (new JavaScriptSerializer()).Serialize(pun);
                byte[] bytes = Encoding.UTF8.GetBytes(inputJson);
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }
                string json;
                using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    {
                        json = (new StreamReader(stream)).ReadToEnd();

                    }
                }
                var responseObject = JsonConvert.DeserializeObject<T>(json.ToString());

                return responseObject;

            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public string WriteStream(string url, string postStruct)
        {
            string data = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Testing";
            request.Method = System.Net.WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";

            byte[] postArray = Encoding.UTF8.GetBytes(postStruct);
            request.ContentLength = postArray.Length;

            Stream apiStream = request.GetRequestStream();
            apiStream.Write(postArray, 0, postArray.Length);

            apiStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            apiStream = response.GetResponseStream();
            StreamReader apiReader = new StreamReader(apiStream);

            data = apiReader.ReadToEnd();

            apiReader.Close();
            apiStream.Close();
            response.Close();

            return data;
        }

        public string ReadStream(string url)
        {
            string data = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var apiStream = response.GetResponseStream();
                if (apiStream != null)
                {
                    var apiReader = new StreamReader(apiStream);
                    data = apiReader.ReadToEnd();
                    apiStream.Close();
                }
            }
            response.Close();

            return data;
        }

        protected dynamic SafeRun<T>(Func<T> objAction)
        {
            try
            {
                return objAction();
            }
            catch (Exception ex)
            {
                QuotationDBFactory mFactory = new QuotationDBFactory();
                var requri = this.ActionContext.Request.RequestUri.ToString();
                mFactory.InsertErrorLog(
                    "", 
                    "api", 
                    requri.Substring(requri.IndexOf("/api/")), 
                    ex.Message.ToString(), 
                    ex.StackTrace.ToString()
                 );
                SendEmail(ex.StackTrace.ToString(),ex.Message.ToString());
                
                return new System.Net.Http.HttpResponseMessage()
                {                   
                    Content = new JsonContent(new
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Error = "Something Went Wrong" 
                    })
                };
            }
        }

        protected void LogError(string controller,string action, string message, string stacktrace,string user = "")
        {
            QuotationDBFactory mFactory = new QuotationDBFactory();
            mFactory.InsertErrorLog(
                user,
                controller,
                action,
                message,
                stacktrace
             );
            SendEmail(stacktrace, message);
        }

        internal void SendEmail(string st, string ex)
        {
            var Content = string.Empty;
            if (this.ActionContext.Request.Method.ToString() == "POST")
            {                    
                var d = new StreamReader(this.ActionContext.Request.Content.ReadAsStreamAsync().Result);
                d.BaseStream.Seek(0, SeekOrigin.Begin);
                Content = d.ReadToEnd();
            }
            var hostdata = ((System.Web.HttpRequestWrapper)this.RequestContext.GetType().Assembly.GetType("System.Web.Http.WebHost.WebHostHttpRequestContext").GetProperty("WebRequest").GetMethod.Invoke(this.RequestContext, null));
            var exdata = new ExceptionEmailDto()
            {
                ActionMethod = this.ActionContext.Request.Method.ToString(),
                AppUrl = System.Configuration.ConfigurationManager.AppSettings["APPURL"],
                Error = ex,
                Headers = this.ActionContext.Request.Headers.ToString(),
                ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"],
                StackTrace = st,
                Url = this.ActionContext.Request.RequestUri.ToString(),
                UserAgent = hostdata == null ? string.Empty : hostdata.UserAgent.ToString(),
                UserHostAddress = hostdata == null ? string.Empty : hostdata.UserHostAddress.ToString(),
                UserHostName = hostdata == null ? string.Empty : hostdata.UserHostName.ToString(),
                UTC = DateTime.UtcNow.ToString(),
                Content = Content,
                ServerVariables = hostdata == null ? "undefined" : hostdata.ServerVariables.ToString()
            };
            FOCiS.SSP.Web.UI.Controllers.APIDynamicClass.SendExceptionEmailV2(
                exdata
            );
        }
    }
}
