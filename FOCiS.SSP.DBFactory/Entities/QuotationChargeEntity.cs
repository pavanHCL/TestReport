﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class QuotationChargeEntity
    {
        public long BASECHARGEID { get; set; }
        public string BASECHARGELOCALNAME { get; set; }
        public string BASECHARGENAME { get; set; }
        public string CHARGEAPPLICABILITY { get; set; }
        public string CHARGECONDITIONS { get; set; }
        public long CHARGEID { get; set; }
        public string CHARGELOCALNAME { get; set; }
        public string CHARGENAME { get; set; }
        public string CURRENCYID { get; set; }
        public long QUOTATIONID { get; set; }
        public long RATEBASISID { get; set; }
        public string RATEBASISNAME { get; set; }
        public long RATEREFERENCEID { get; set; }       
        public long RouteTypeId { get; set; }
        public string RouteTypeName { get; set; }
        public long SERVICECHARGEID { get; set; }
        public decimal TOTALPRICE { get; set; }
        public decimal UNITRATE { get; set; }
        public string UOM { get; set; }
        public long WEIGHTAPP { get; set; }
        public long ISCHARGEINCLUDED { get; set; }
        public string ROUNDEDTOTALPRICE { get; set; }
        public string CHARGESOURCE { get; set; }
        public string CHARGESTATUS { get; set; }
    }
}
