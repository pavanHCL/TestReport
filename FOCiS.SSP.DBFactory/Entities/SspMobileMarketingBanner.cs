﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class SspMobileMarketingBanner
    {
        public string CardType { get; set; }
        public string Title { get; set; }
        public string SubTtitle { get; set; }
        public string ImgUrl { get; set; }
        public string WebUrl { get; set; }
    }
}
