﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
   public class CreditCommentEntity
    {
        public decimal COMMENTID { get; set; }
        public decimal CREDITAPPID { get; set; }
        public string COMMENTS { get; set; }
        public string CREATEDBY { get; set; }
        public string COMMENTSFROM { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public decimal? ADDITIONALCREDITID { get; set; }
        public decimal? INFOTYPE { get; set; }
        public List<CreditDocEntity> CreditDoc { get; set; }
    }
   public class AddCreditCommentEntity
   {
       public decimal COMMENTID { get; set; }
       public decimal CREDITAPPID { get; set; }
       public string COMMENTS { get; set; }
       public string CREATEDBY { get; set; }
       public string COMMENTSFROM { get; set; }
       public DateTime CREATEDDATE { get; set; }
       public decimal? ADDITIONALCREDITID { get; set; }
       public decimal? INFOTYPE { get; set; }
       public List<AddCreditDocEntity> CreditDoc { get; set; }
   }
}
