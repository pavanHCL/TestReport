﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class OnlinePaymentTransactionEntity
    {
        public long TRANSATIONID { get; set; }
        public long JOBNUMBER { get; set; }
        public string STATUS { get; set; }
        public string AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string PAYMENTOPTION { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string TOKENID { get; set; }
        public string CHARGEID { get; set; }
        public string STRIPETRANSACTIONID { get; set; }
        public string PAYOUTCURRENCY { get; set; }
        public string PAYOUTTOTAL { get; set; }
        public string PAYOUTNET { get; set; }
        public string STRIPEFEE { get; set; }
        public string PAYOUTSTATUS { get; set; }
        public string EXCHANGERATE { get; set; }
        public string REFUNDID { get; set; }
        public string REFUNDAMOUNT { get; set; }
        public string REFUNDSTATUS { get; set; }
        public string STRIPEACCOUNT { get; set; }
        public long QUOTATIONID { get; set; }
        public string CREATEDBY { get; set; }
    }


    public class OnlineRefundEntity
    {
        public long Id { get; set; }
        public string JobNumber { get; set; }
        public string Currency { get; set; }
        public string RefundAmount { get; set; }
        public string RefundRefNumber { get; set; }
        public string RefundStatus { get; set; }
    }


}
