﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class ProfileInfo
    {
        public string USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string MOBILENUMBER { get; set; }
        public string COMPANYLINK { get; set; }
        public string ImgBase64 { get; set; }
        public byte[] IMGCONTENT { get; set; }
        public string ISNPC { get; set; }
        public string BCOUNTRYSETUP { get; set; }
        public string COUNTRYNAME { get; set; }

    }
    public class ProfileImage
    {
        public string USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PREFERREDCURRENCYCODE { get; set; }
        public string PREFERREDCURRENCY { get; set; }
        public string WEIGHTUNIT { get; set; }
        public string LENGTHUNIT { get; set; }
        public byte[] IMGCONTENT { get; set; }
    }
    public class DBMBModel
    {
        public decimal BOOKINGSDONE { get; set; }
        public decimal TOTALVOLUME { get; set; }
        public decimal CREDITLIMIT { get; set; }
        public decimal TotalSpent { get; set; }
    }

    public class DashBoardList
    {
        public ProfileInfo PINFO { get; set; }
        public DBMBModel DBData { get; set; }
    }

    public class MBPROFILE
    {
        public string USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string COMPANYLINK { get; set; }
        public string ISDCODE { get; set; }
        public string MOBILENUMBER { get; set; }
        public long UAUSERADTLSID { get; set; }
        public decimal UACOUNTRYID { get; set; }
        public string UACOUNTRYCODE { get; set; }
        public string UACOUNTRYNAME { get; set; }
        public long UASTATEID { get; set; }
        public string UASTATECODE { get; set; }
        public string UASTATENAME { get; set; }
        public long UACITYID { get; set; }
        public string UACITYCODE { get; set; }
        public string UACITYNAME { get; set; }
        public string UAPOSTCODE { get; set; }
        public string UAFULLADDRESS { get; set; }
        public string UAHOUSENO { get; set; }
        public string UABUILDINGNAME { get; set; }
        public string UAADDRESSLINE1 { get; set; }
        public string UAADDRESSLINE2 { get; set; }
        public DateTime UADATECREATED { get; set; }
        public string INDIRECTTAXLABEL { get; set; }
        public string VATREGNUMBER { get; set; }
        public string SALUTATION { get; set; }
        public string PREFERREDCURRENCYCODE { get; set; }
        public string PREFERREDCURRENCY { get; set; }
        public string WEIGHTUNIT { get; set; }
        public string LENGTHUNIT { get; set; }
        public long NOTIFICATIONSUBSCRIPTION { get; set; }
        public string INDTAXMAXIMUMLENGTH { get; set; }
        public bool ISCOUNTRYDISABLE { get; set; }
        public string INDUSTRY { get; set; }
        public string OTHERINDUSTRY { get; set; }
        public long ISFAPIAO { get; set; }
        //Added for FAPIAO
        public long FUSERFAPIAOID { get; set; }
        public string FCOMPANYNAMECHINESE { get; set; }
        public string FCOMPANYNAME { get; set; }
        public string FPHONENUMBER { get; set; }
        public string FEMAILID { get; set; }
        public string FCOMPANYADDRESSCHINESE { get; set; }
        public string FCOMPANYADDRESS { get; set; }
        public string FTAXIDENTIFICATIONNO { get; set; }
        public string FBANKACCOUNTNO { get; set; }
        public string FBANKNAME { get; set; }
        public DateTime FDATECREATED { get; set; }
        public DateTime FDATEMODIFIED { get; set; }
        public long ISINDIVIDUAL { get; set; }
        public string FINDIVIDUALNAME { get; set; }
        public string FINDTAXIDENTIFICATION { get; set; }
        public string FINDPHONENUMBER { get; set; }
        public string FINDEMAILID { get; set; }
        public string ISNPC { get; set; }
        public string BCOUNTRYSETUP { get; set; }
        public string SHIPPINGEXPERIENCE { get; set; }
        public string SHIPMENTPROCESS { get; set; }
        public string PERSONALASSISTANCE { get; set; }
        public string EXISTINGFREIGHTFORWARDER { get; set; }
        public decimal BOOKINGCOUNT { get; set; }
        public int QUOTECOUNT { get; set; }
    }
    public class MBCREDIT
    {
        public string USERID { get; set; }
        public long CREDITAPPID { get; set; }
        public string INDUSTRY { get; set; }
        public string OTHERINDUSTRY { get; set; }
        public string INDUSTRYID { get; set; }
        public long NOOFEMP { get; set; }
        public string SUBVERTICAL { get; set; }
        public string PARENTCOMPANY { get; set; }
        public decimal ANNUALTURNOVER { get; set; }
        public string ANNUALTURNOVERCURRENCY { get; set; }
        public string DBNUMBER { get; set; }
        public string PRODUCTPROFILE { get; set; }
        public string SALESCHANNEL { get; set; }
        public decimal PREVIOUSYEARREVENUE { get; set; }
        public decimal PREVIOUSYEARNR { get; set; }
        public decimal PREVIOUSYEARNRMARGIN { get; set; }
        public decimal PREVIOUSYEARBILLING { get; set; }
        public decimal NEXTYEARREVENUE { get; set; }
        public decimal NEXTYEARNR { get; set; }
        public decimal NEXTYEARNRMARGIN { get; set; }
        public decimal NEXTYEARBILLING { get; set; }
        public decimal CURRENTYEARREVENUE { get; set; }
        public decimal CURRENTYEARNR { get; set; }
        public decimal CURRENTYEARNRMARGIN { get; set; }
        public decimal CURRENTYEARBILLING { get; set; }
        public string HFMENTRYCODE { get; set; }
        public string HFMENTRYCURRENCY { get; set; }
        public decimal REQUESTEDCREDITLIMIT { get; set; }
        public string REQUESTEDCREDITLIMITCURRENCY { get; set; }
        public string CURRENTPAYTERMSNAME { get; set; }
        public string COMMENTS { get; set; }
        public DateTime CREDITLIMITEXPDATE { get; set; }
        public string CURRENCYID { get; set; }
        public string CURRENCYDESC { get; set; }
        public string DESCRIPTIONOFKS { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime CreditDATECREATED { get; set; }
        public DateTime CreditDATEMODIFIED { get; set; }
        public string MODIFIEDBY { get; set; }
        public string OWNERORGID { get; set; }
        public string OWNERLOCID { get; set; }
        public string STATEID { get; set; }
        public decimal APPROVEDCREDITLIMIT { get; set; }
        public decimal CURRENTOSBALANCE { get; set; }
        public Int64 NOTIFICATIONID { get; set; }
        public string PROFILESTATUS { get; set; }
        public long EXISTINGCUSTOMER { get; set; }
        public string CREDITREFNO { get; set; }
        public IList<CreditDocEntity> CreditDoc { get; set; }
        public List<CreditCommentEntity> CreditComments { get; set; }
        public string REJECTIONCOMMENTS { get; set; }
        public long USEMYCREIDT { get; set; }
    }

    public class MBDocs
    {

        public virtual long DOCID { get; set; }
        public virtual long JOBNUMBER { get; set; }
        public virtual string SOURCE { get; set; }
        public virtual string DOCUMNETTYPE { get; set; }
        public virtual string FILENAME { get; set; }
        public virtual string FILEEXTENSION { get; set; }
        public virtual long FILESIZE { get; set; }
        public virtual byte[] FILECONTENT { get; set; }
        public virtual string DocumentName { get; set; }
        public virtual string DOCUMENTTEMPLATE { get; set; }
        public virtual string CONDMAND { get; set; }
        public virtual string DIRECTION { get; set; }
        public virtual string DOCDELETE { get; set; }
        public virtual string PROVIDEDDBY { get; set; }
        public virtual DateTime DOCUPLOADDATE { get; set; }
        public virtual string PROVIDERMAILID { get; set; }
        public virtual string PROVIDER { get; set; }
        public virtual DateTime DATECREATED { get; set; }
    }
    

    public class CouponCode
    {
        public string CouponStatus { get; set; }
        public decimal NetAmount { get; set; }
        public decimal USDNetAmount { get; set; }
        public decimal DiscountAmount { get; set; }
    }
}
