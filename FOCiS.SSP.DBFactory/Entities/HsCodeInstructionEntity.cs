﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    /// <summary>
    /// HS Code Instucation Details
    /// </summary>
    public class HsCodeInstructionEntity
    {
        
        //public string InstrucationType { get; set; }
        //public string INSTRUCATION { get; set; }
        public string HSCODE { get; set; }
        //public string DUTYTYPE { get; set; }
        //public string DUTYRATEPERCENTAGE { get; set; }
    }

    public class HsCodeDutyEntity
    {
        public string HSCODE { get; set; }
        public string DUTYTYPE { get; set; }
        public string DUTYRATEPERCENTAGE { get; set; }
    }
}
