﻿using System;
using System.Collections.Generic;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class QuotationEntity
    {
        public QuotationEntity()
        {
            this.PartyType = 1;
        }
        public decimal CARGOVALUE { get; set; }
        public string CurrencyMode { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public long CUSTOMERID { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public decimal GRANDTOTAL { get; set; }
        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public long ISEVENTCARGO { get; set; }
        public long ISINSURANCEREQUIRED { get; set; }
        public string MODIFIEDBY { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string LANGUAGEMOVEMENTTYPENAME { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public long ORIGINPLACEID { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public long TOTALQUANTITY { get; set; }
        public string CREATEDBY { get; set; }
        public long HASMISSINGCHARGES { get; set; }
        public IList<QuotationShipmentEntity> ShipmentItems { get; set; }
        public IList<QuotationChargeEntity> QuotationCharges { get; set; }
        public IList<QDesChargeEntity> QDesCharges { get; set; }
        public IList<QInterChargeEntity> QInterCharges { get; set; }
        public IList<QOrgChargeEntity> QOrgCharges { get; set; }
        public IList<QAdditionalChargeEntity> QAdditionalCharges { get; set; }
        public IList<QuotationTermsConditionEntity> TermAndConditions { get; set; }
        public IList<QuotationTemplateEntity> QuotationTemplates { get; set; }
        public IList<UserNameEntity> Users { get; set; }
        public string GUESTCOMPANY { get; set; }
        public string GUESTEMAIL { get; set; }
        public string GUESTNAME { get; set; }
        public string GUESTLASTNAME { get; set; }
        public string GUESTCOUNTRYNAME { get; set; }
        public int NOTIFICATIONSUBSCRIPTION { get; set; }
        public string GUESTCOUNTRYCODE { get; set; }
        public string  SHIPMENTPROCESS { get; set; }
        public string SHIPPINGEXPERIENE { get; set; }
        public string PERSONALASSISTANCE { get; set; }
        public string EXISTINGFREIGHTFORWARDER { get; set; }
        public string USEOFQUOTE { get; set; }
        public string ORIGINSUBDIVISIONCODE { get; set; }
        public string DESTINATIONSUBDIVISIONCODE { get; set; }
        public long ThresholdQty { get; set; }
        public decimal OCOUNTRYID { get; set; }
        public decimal DCOUNTRYID { get; set; }
        public long OPORTCOUNTRYID { get; set; }
        public long DPORTCOUNTRYID { get; set; }
        public string ORIGINZIPCODE { get; set; }
        public string DESTINATIONZIPCODE { get; set; }
        public string OCOUNTRYCODE { get; set; }
        public string DCOUNTRYCODE { get; set; }
        public string OCOUNTRYNAME { get; set; }
        public string DCOUNTRYNAME { get; set; }
        public string HAZARDOUSGOODSTYPE { get; set; }
        public long ISCUSTOMREQUIRED { get; set; }
        public string CUSTOMSDISCLAIMR { get; set; }
        public string SUBMISSIONCOMMENTS { get; set; }
        public long ISOVERSIZECARGO { get; set; }
        public decimal co2emission { get; set; }
        public long ISPROHIBITEDCARGO { get; set; }
        public string QuoteSource { get; set; }
        public long ISOFFERAPPLICABLE { get; set; }
        public string OFFERCODE { get; set; }
        public string REQUESTTYPE { get; set; }
        public string TRANSITTIME { get; set; }
        public string ROUNDEDGRANDTOTALPRICE { get; set; }
        public string GSSCREASON { get; set; }
        public int DECIMALCOUNT { get; set; }
        public string CE { get; set; }
        public long INCOTERMID { get; set; }
        public string INCOTERMDESC { get; set; }
        public long PackageTypes { get; set; }
        public string AmazonSupplierID { get; set; }
        public string PALLETIZINGBY { get; set; }
        public string LABELLINGBY { get; set; }
        public string QUOTETYPE { get; set; }
        public string AMAZONFCCODE { get; set; }
        public long MSDSCERTIFICATEDOCID { get; set; }
        public long MULTIQUOTATIONID { get; set; }
        public string MTRANSITTIME { get; set; }
        public decimal MGRANDTOTAL { get; set; }
        public string MPRODUCTTYPENAME { get; set; }
        public string FirstName { get; set; }
        public long PartyType { get; set; }
    }

    public class QuoteStatusCountEntity
    {
        public decimal ALLQUOTES { get; set; }
        public decimal ACTIVEQUOTES { get; set; }
        public decimal EXPIREDQUOTES { get; set; }
        public decimal ALLJOBS { get; set; }
        public decimal ACTIVEJOBS { get; set; }
        public decimal EXPIREDJOBS { get; set; }
        public decimal COLLABORATEDJOBS { get; set; }
        public decimal INITIATEDJOBS { get; set; }
        public decimal INPROGRESSJOBS { get; set; }
        public decimal COMPLETEDJOBS { get; set; }
        public decimal NOMINATIONS { get; set; }
        public decimal CANCELLEDJOBS { get; set; }
    }
    public class PortPairsEntity
    {
        public decimal UNLOCATIONID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string COUNTRYCODE { get; set; }
        public Decimal COUNTRYID { get; set; }
        public string COUNTRYNAME { get; set; }
        public string SUBDIVISIONCODE { get; set; }
        public string SUBDIVISIONNAME { get; set; }
    }

    public class QDesChargeEntity
    {
        public long BASECHARGEID { get; set; }
        public string BASECHARGELOCALNAME { get; set; }
        public string BASECHARGENAME { get; set; }
        public string CHARGEAPPLICABILITY { get; set; }
        public string CHARGECONDITIONS { get; set; }
        public long CHARGEID { get; set; }
        public string CHARGELOCALNAME { get; set; }
        public string CHARGENAME { get; set; }
        public string CURRENCYID { get; set; }
        public long QUOTATIONID { get; set; }
        public long RATEBASISID { get; set; }
        public string RATEBASISNAME { get; set; }
        public long RATEREFERENCEID { get; set; }
        public long RouteTypeId { get; set; }
        public string RouteTypeName { get; set; }
        public long SERVICECHARGEID { get; set; }
        public decimal TOTALPRICE { get; set; }
        public decimal UNITRATE { get; set; }
        public string UOM { get; set; }
        public long WEIGHTAPP { get; set; }
        public long ISCHARGEINCLUDED { get; set; }
        public string ROUNDEDTOTALPRICE { get; set; }
    }

    public class QAdditionalChargeEntity
    {
        public long BASECHARGEID { get; set; }
        public string BASECHARGELOCALNAME { get; set; }
        public string BASECHARGENAME { get; set; }
        public string CHARGEAPPLICABILITY { get; set; }
        public string CHARGECONDITIONS { get; set; }
        public long CHARGEID { get; set; }
        public string CHARGELOCALNAME { get; set; }
        public string CHARGENAME { get; set; }
        public string CURRENCYID { get; set; }
        public long QUOTATIONID { get; set; }
        public long RATEBASISID { get; set; }
        public string RATEBASISNAME { get; set; }
        public long RATEREFERENCEID { get; set; }
        public long RouteTypeId { get; set; }
        public string RouteTypeName { get; set; }
        public long SERVICECHARGEID { get; set; }
        public decimal TOTALPRICE { get; set; }
        public decimal UNITRATE { get; set; }
        public string UOM { get; set; }
        public long WEIGHTAPP { get; set; }
        public long ISCHARGEINCLUDED { get; set; }
        public string ROUNDEDTOTALPRICE { get; set; }
    }

    public class QOptionalChargeEntity
    {
        public long BASECHARGEID { get; set; }
        public string BASECHARGELOCALNAME { get; set; }
        public string BASECHARGENAME { get; set; }
        public string CHARGEAPPLICABILITY { get; set; }
        public string CHARGECONDITIONS { get; set; }
        public long CHARGEID { get; set; }
        public string CHARGELOCALNAME { get; set; }
        public string CHARGENAME { get; set; }
        public string CURRENCYID { get; set; }
        public long QUOTATIONID { get; set; }
        public long RATEBASISID { get; set; }
        public string RATEBASISNAME { get; set; }
        public long RATEREFERENCEID { get; set; }
        public long RouteTypeId { get; set; }
        public string RouteTypeName { get; set; }
        public long SERVICECHARGEID { get; set; }
        public decimal TOTALPRICE { get; set; }
        public decimal UNITRATE { get; set; }
        public string UOM { get; set; }
        public long WEIGHTAPP { get; set; }
        public long ISCHARGEINCLUDED { get; set; }
        public string ROUNDEDTOTALPRICE { get; set; }
    }

    public class QInterChargeEntity
    {
        public long BASECHARGEID { get; set; }
        public string BASECHARGELOCALNAME { get; set; }
        public string BASECHARGENAME { get; set; }
        public string CHARGEAPPLICABILITY { get; set; }
        public string CHARGECONDITIONS { get; set; }
        public long CHARGEID { get; set; }
        public string CHARGELOCALNAME { get; set; }
        public string CHARGENAME { get; set; }
        public string CURRENCYID { get; set; }
        public long QUOTATIONID { get; set; }
        public long RATEBASISID { get; set; }
        public string RATEBASISNAME { get; set; }
        public long RATEREFERENCEID { get; set; }
        public long RouteTypeId { get; set; }
        public string RouteTypeName { get; set; }
        public long SERVICECHARGEID { get; set; }
        public decimal TOTALPRICE { get; set; }
        public decimal UNITRATE { get; set; }
        public string UOM { get; set; }
        public long WEIGHTAPP { get; set; }
        public long ISCHARGEINCLUDED { get; set; }
        public string ROUNDEDTOTALPRICE { get; set; }
    }

    public class QOrgChargeEntity
    {
        public long BASECHARGEID { get; set; }
        public string BASECHARGELOCALNAME { get; set; }
        public string BASECHARGENAME { get; set; }
        public string CHARGEAPPLICABILITY { get; set; }
        public string CHARGECONDITIONS { get; set; }
        public long CHARGEID { get; set; }
        public string CHARGELOCALNAME { get; set; }
        public string CHARGENAME { get; set; }
        public string CURRENCYID { get; set; }
        public long QUOTATIONID { get; set; }
        public long RATEBASISID { get; set; }
        public string RATEBASISNAME { get; set; }
        public long RATEREFERENCEID { get; set; }
        public long RouteTypeId { get; set; }
        public string RouteTypeName { get; set; }
        public long SERVICECHARGEID { get; set; }
        public decimal TOTALPRICE { get; set; }
        public decimal UNITRATE { get; set; }
        public string UOM { get; set; }
        public long WEIGHTAPP { get; set; }
        public long ISCHARGEINCLUDED { get; set; }
        public string ROUNDEDTOTALPRICE { get; set; }
        public string LANGUAGECHARGENAME { get; set; }
    }

    public class QuotationTemplateEntity
    {
        public long QUOTATIONID { get; set; }
        public string TEMPLATENAME { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string PRODUCTNAME { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public decimal TOTALCBM { get; set; }
    }

    public class QuotationComplainceEntity
    {
        public string PRODUCTCATEGORYNAME { get; set; }
        public List<ComplainceDescription> ComplainceDescription { get; set; }
        public QuotationComplainceEntity()
        {
            ComplainceDescription = new List<ComplainceDescription>();
        }
    }

    public class ComplainceDescription
    {
        public string DescriptionName { get; set; }
    }
}