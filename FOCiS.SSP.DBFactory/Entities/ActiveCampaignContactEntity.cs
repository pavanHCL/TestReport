﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class ActiveCampaignContactEntity
    {
        public long ContactId { get; set; }
        public long SubscriberId { get; set; }
        public int SenderLastShould { get; set; }
        public string SendLastdid { get; set; }
        public int ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public string ResultOutPut { get; set; }
        public string ContactEmailId { get; set; }
        public string UserType { get; set; }
    }

    public class ActCampaignEditEntity
    {
        public string QuotationNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public string QuoteRequestDate { get; set; }
        public string QuoteGivenByGSSCDate { get; set; }
        public string OperationalJobNumber { get; set; }
        public string OCountryName { get; set; }
        public string DCountryName { get; set; }
        public string ProductName { get; set; }
        public string MovementTypeName { get; set; }
        public string OriginPortName { get; set; }
        public string DestinationPortName { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerStatus { get; set; }
        public string StateId { get; set; }
        public string Currency { get; set; }
        public string QuoteGrandTotal { get; set; }
        public string JobGrandTotal { get; set; }
        public long SubscriberId { get; set; }


    }
}
