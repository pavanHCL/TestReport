﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class JobBookingEntity
    {
        public decimal CARGOVALUE { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public long CUSTOMERID { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public string MODIFIEDBY { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public long ORIGINPLACEID { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public decimal GRANDTOTAL { get; set; }
        //public ICollection<QuotationShipmentEntity> QuotationShipmentEntity {get;set;}
        public IEnumerable<QuotationShipmentEntity> ShipmentItems { get; set; }
        public IEnumerable<JobBookingShipmentEntity> JobShipmentItems { get; set; }
        public IEnumerable<SSPDocumentsEntity> DocumentDetails { get; set; }
        public IEnumerable<ScheduleEntity> ScheduleDetails { get; set; }

        public IList<SSPJobDetailsEntity> JobItems { get; set; }
        public IList<SSPPartyDetailsEntity> PartyItems { get; set; }
        public IList<PaymentChargesEntity> PaymentCharges { get; set; }

        public IList<HsCodeInstructionEntity> HSCodeInsItems { get; set; }
        public IList<Compliance_Fields> CopmlnceFlds { get; set; }
        
        public IList<HsCodeDutyEntity> HSCodeDutyItems { get; set; }
        public CargoAvabilityEntity CargoAvabilityEntity { get; set; }
        //shipper  details
        public string SHADDRESSTYPE { get; set; }
        public string SHBUILDINGNAME { get; set; }
        public string SHCITYCODE { get; set; }
        public string SHCITYNAME { get; set; }
        public string SHCLIENTID { get; set; }
        public string SHCLIENTNAME { get; set; }
        public string SHCOUNTRYCODE { get; set; }
        public string SHCOUNTRYID { get; set; }
        public string SHCOUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }
        public string SHFULLADDRESS { get; set; }
        public string SHHOUSENO { get; set; }
        public long SHISDEFAULTADDRESS { get; set; }
        public long SHJOBNUMBER { get; set; }
        public string SHLATITUDE { get; set; }
        public string SHLONGITUDE { get; set; }
        public long SHPARTYDETAILSID { get; set; }
        public string SHPARTYTYPE { get; set; }
        public string SHPINCODE { get; set; }
        public string SHSTATECODE { get; set; }
        public string SHSTATENAME { get; set; }
        public string SHSTREET1 { get; set; }
        public string SHSTREET2 { get; set; }
        public string SHFIRSTNAME { get; set; }
        public string SHLASTNAME { get; set; }
        public string SHPHONE { get; set; }
        public string SHEMAILID { get; set; }
        public string SHSALUTATION { get; set; }
        public int SHISSAVEDORNEWADDRESS { get; set; }


        //Consignee Details
        public string CONADDRESSTYPE { get; set; }
        public string CONBUILDINGNAME { get; set; }
        public string CONCITYCODE { get; set; }
        public string CONCITYNAME { get; set; }
        public string CONCLIENTID { get; set; }
        public string CONCLIENTNAME { get; set; }
        public string CONCOUNTRYCODE { get; set; }
        public string CONCOUNTRYNAME { get; set; }
        public string CONCOUNTRYID { get; set; }
        //public DateTime DATECREATED { get; set; }

        public string CONFULLADDRESS { get; set; }
        public string CONHOUSENO { get; set; }
        public long CONISDEFAULTADDRESS { get; set; }
        public long CONJOBNUMBER { get; set; }
        public string CONLATITUDE { get; set; }
        public string CONLONGITUDE { get; set; }
        public long CONPARTYDETAILSID { get; set; }
        public string CONPARTYTYPE { get; set; }
        public string CONPINCODE { get; set; }
        public string CONSTATECODE { get; set; }
        public string CONSTATENAME { get; set; }
        public string CONSTREET1 { get; set; }
        public string CONSTREET2 { get; set; }
        public string CONFIRSTNAME { get; set; }
        public string CONLASTNAME { get; set; }
        public string CONPHONE { get; set; }
        public string CONEMAILID { get; set; }
        public string CONSALUTATION { get; set; }
        public int CONISSAVEDORNEWADDRESS { get; set; }

        //JOBDETAILS
        public virtual long JJOBNUMBER { get; set; }
        public virtual string JOPERATIONALJOBNUMBER { get; set; }
        public virtual long JQUOTATIONID { get; set; }
        public virtual string JQUOTATIONNUMBER { get; set; }
        public virtual string JINCOTERMCODE { get; set; }
        public virtual string JINCOTERMDESCRIPTION { get; set; }
        public virtual DateTime JCARGOAVAILABLEFROM { get; set; }
        public virtual DateTime JCARGODELIVERYBY { get; set; }
        //public virtual string JORIGINCUSTOMSCLEARANCEBY { get; set; }
        //public virtual string JDESTINATIONCUSTOMSCLEARANCEBY { get; set; }
        public virtual int JDOCUMENTSREADY { get; set; }
        public virtual string JSLINUMBER { get; set; }
        public virtual string JCONSIGNMENTID { get; set; }
        public virtual DateTime JDATECREATED { get; set; }
        public virtual DateTime JDATEMODIFIED { get; set; }
        public virtual string JMODIFIEDBY { get; set; }
        public virtual string JSTATEID { get; set; }
        public string SPECIALINSTRUCTION { get; set; }
        public long LIVEUPLOADS { get; set; }
        public long LOADINGDOCKAVAILABLE { get; set; }
        public long CARGOPALLETIZED { get; set; }
        public long ORIGINALDOCUMENTSREQUIRED { get; set; }
        public long TEMPERATURECONTROLREQUIRED { get; set; }
        public long COMMERCIALPICKUPLOCATION { get; set; }

        //Receipt Header
        public IList<ReceiptHeaderEntity> ReceiptHeaderDetails { get; set; }

        public decimal OCOUNTRYID { get; set; }
        public decimal DCOUNTRYID { get; set; }

        public string ORIGINZIPCODE { get; set; }
        public string DESTINATIONZIPCODE { get; set; }
        public string ORIGINSUBDIVISIONCODE { get; set; }
        public string DESTINATIONSUBDIVISIONCODE { get; set; }

        public long SCHEDULEID { get; set; }
        public string CREATEDBY { get; set; }
        public string COLLABRATEDCONSIGNEE { get; set; }
        public string COLLABRATEDSHIPPER { get; set; }
        public int CHARGESETID { get; set; }
        public string HAZARDOUSGOODSTYPE { get; set; }
        public string TRANSITTIME { get; set; }
        public string QUOTETYPE { get; set; }
        public string AMAZONFCCODE { get; set; }
    }

    public class CargoAvabilityEntity
    {
        public string AIRAVAILABILITY { get; set; }
        public string AIRDELIVERY { get; set; }
        public string OCEANAVAILABILITY { get; set; }
        public string OCEANDELIVERY { get; set; }
    }

    public class SSPPartyDetailsEntity
    {
        public string ADDRESSTYPE { get; set; }
        public string BUILDINGNAME { get; set; }
        public string CITYCODE { get; set; }
        public string CITYNAME { get; set; }
        public string CLIENTID { get; set; }
        public string CLIENTNAME { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public string FULLADDRESS { get; set; }
        public string HOUSENO { get; set; }
        public long ISDEFAULTADDRESS { get; set; }
        public long JOBNUMBER { get; set; }
        public string LATITUDE { get; set; }
        public string LONGITUDE { get; set; }
        public string MODIFIEDBY { get; set; }
        public long PARTYDETAILSID { get; set; }
        public string PARTYTYPE { get; set; }
        public string PINCODE { get; set; }
        public string STATECODE { get; set; }
        public string STATEID { get; set; }
        public string STATENAME { get; set; }
        public string STREET1 { get; set; }
        public string STREET2 { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PHONE { get; set; }
        public string EMAILID { get; set; }
        public string SALUTATION { get; set; }
        public string CREATEDBY { get; set; }
        public decimal SSPCLIENTID { get; set; }
        public long COUNTRYID { get; set; }
    }


    public class JobBookingListEntity
    {
        public decimal CARGOVALUE { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public long CUSTOMERID { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTNAME { get; set; }

        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public long ISINSURANCEREQUIRED { get; set; }
        public string MODIFIEDBY { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public long ORIGINPLACEID { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public long TOTALQUANTITY { get; set; }
        public long JOBNUMBER { get; set; }
        public string OPERATIONALJOBNUMBER { get; set; }
        public decimal GRANDTOTAL { get; set; }
        public string JSTATEID { get; set; }
        public IList<QuotationShipmentEntity> ShipmentItems { get; set; }
        public IList<QuotationChargeEntity> QuotationCharges { get; set; }
        public IList<QuotationTermsConditionEntity> TermAndConditions { get; set; }
        public IList<SSPJobDetailsEntity> JobItems { get; set; }
        public IList<ReceiptHeaderEntity> ReceiptHeaderDetails { get; set; }
    }


    public class JobBookingDemoEntity
    {
        public decimal CARGOVALUE { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public long CUSTOMERID { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public long DESTINATIONPLACEID { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public long DESTINATIONPORTID { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public string MODIFIEDBY { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public long ORIGINPLACEID { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public long ORIGINPORTID { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public long PRODUCTID { get; set; }
        public string PRODUCTNAME { get; set; }
        public long PRODUCTTYPEID { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public decimal GRANDTOTAL { get; set; }
        //public ICollection<QuotationShipmentEntity> QuotationShipmentEntity {get;set;}
        public IEnumerable<QuotationShipmentEntity> ShipmentItems { get; set; }
        public IEnumerable<SSPDocumentsEntity> DocumentDetails { get; set; }

        public IList<SSPJobDetailsEntity> JobItems { get; set; }
        public IList<SSPPartyDetailsEntity> PartyItems { get; set; }
        public IList<PaymentChargesEntity> PaymentCharges { get; set; }

        //shipper  details
        public string SHADDRESSTYPE { get; set; }
        public string SHBUILDINGNAME { get; set; }
        public string SHCITYCODE { get; set; }
        public string SHCITYNAME { get; set; }
        public string SHCLIENTID { get; set; }
        public string SHCLIENTNAME { get; set; }
        public string SHCOUNTRYCODE { get; set; }
        public string SHCOUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }
        public string SHFULLADDRESS { get; set; }
        public string SHHOUSENO { get; set; }
        public long SHISDEFAULTADDRESS { get; set; }
        public long SHJOBNUMBER { get; set; }
        public string SHLATITUDE { get; set; }
        public string SHLONGITUDE { get; set; }
        public long SHPARTYDETAILSID { get; set; }
        public string SHPARTYTYPE { get; set; }
        public string SHPINCODE { get; set; }
        public string SHSTATECODE { get; set; }
        public string SHSTATENAME { get; set; }
        public string SHSTREET1 { get; set; }
        public string SHSTREET2 { get; set; }
        public string SHFIRSTNAME { get; set; }
        public string SHLASTNAME { get; set; }
        public string SHPHONE { get; set; }
        public string SHEMAILID { get; set; }
        public string SHSALUTATION { get; set; }
        public int SHISSAVEDORNEWADDRESS { get; set; }


        //Consignee Details
        public string CONADDRESSTYPE { get; set; }
        public string CONBUILDINGNAME { get; set; }
        public string CONCITYCODE { get; set; }
        public string CONCITYNAME { get; set; }
        public string CONCLIENTID { get; set; }
        public string CONCLIENTNAME { get; set; }
        public string CONCOUNTRYCODE { get; set; }
        public string CONCOUNTRYNAME { get; set; }
        //public DateTime DATECREATED { get; set; }

        public string CONFULLADDRESS { get; set; }
        public string CONHOUSENO { get; set; }
        public long CONISDEFAULTADDRESS { get; set; }
        public long CONJOBNUMBER { get; set; }
        public string CONLATITUDE { get; set; }
        public string CONLONGITUDE { get; set; }
        public long CONPARTYDETAILSID { get; set; }
        public string CONPARTYTYPE { get; set; }
        public string CONPINCODE { get; set; }
        public string CONSTATECODE { get; set; }
        public string CONSTATENAME { get; set; }
        public string CONSTREET1 { get; set; }
        public string CONSTREET2 { get; set; }
        public string CONFIRSTNAME { get; set; }
        public string CONLASTNAME { get; set; }
        public string CONPHONE { get; set; }
        public string CONEMAILID { get; set; }
        public string CONSALUTATION { get; set; }
        public int CONISSAVEDORNEWADDRESS { get; set; }

        //JOBDETAILS
        public virtual long JJOBNUMBER { get; set; }
        public virtual string JOPERATIONALJOBNUMBER { get; set; }
        public virtual long JQUOTATIONID { get; set; }
        public virtual string JQUOTATIONNUMBER { get; set; }
        public virtual string JINCOTERMCODE { get; set; }
        public virtual string JINCOTERMDESCRIPTION { get; set; }
        public virtual DateTime JCARGOAVAILABLEFROM { get; set; }
        public virtual DateTime JCARGODELIVERYBY { get; set; }
        //public virtual string JORIGINCUSTOMSCLEARANCEBY { get; set; }
        //public virtual string JDESTINATIONCUSTOMSCLEARANCEBY { get; set; }
        public virtual int JDOCUMENTSREADY { get; set; }
        public virtual string JSLINUMBER { get; set; }
        public virtual string JCONSIGNMENTID { get; set; }
        public virtual DateTime JDATECREATED { get; set; }
        public virtual DateTime JDATEMODIFIED { get; set; }
        public virtual string JMODIFIEDBY { get; set; }
        public virtual string JSTATEID { get; set; }
    }

    public class SSPClientMasterEntity
    {
        public string ADDRESSTYPE { get; set; }
        public string BUILDINGNAME { get; set; }
        public string CITYCODE { get; set; }
        public string CITYNAME { get; set; }
        public string CLIENTID { get; set; }
        public string CLIENTNAME { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public string FULLADDRESS { get; set; }
        public string HOUSENO { get; set; }
        public long ISDEFAULTADDRESS { get; set; }
        public long JOBNUMBER { get; set; }
        public string LATITUDE { get; set; }
        public string LONGITUDE { get; set; }
        public string MODIFIEDBY { get; set; }
        public long PARTYDETAILSID { get; set; }
        public string PARTYTYPE { get; set; }
        public string PINCODE { get; set; }
        public string STATECODE { get; set; }
        public string STATEID { get; set; }
        public string STATENAME { get; set; }
        public string STREET1 { get; set; }
        public string STREET2 { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PHONE { get; set; }
        public string EMAILID { get; set; }
        public string SALUTATION { get; set; }
        public long SSPCLIENTID { get; set; }
        public string CONTROLCLIENTID { get; set; }
        public string CREATEDBY { get; set; }
    }

    /// <summary>
    /// HS Code Entity Details
    /// </summary>
    public class HsCodeEntity
    {
        public Int64 HSCodeDtlsID { get; set; }
        public Int64 HSCodeID { get; set; }
        public Int64 JobNumber { get; set; }

        public Int64 CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public Int64 SectionId { get; set; }
        public string SectionCode { get; set; }
        public string SectionName { get; set; }

        public Int64 ChapterId { get; set; }
        public string ChapterCode { get; set; }
        public string ChapterName { get; set; }

        public Int64 HeadingId { get; set; }
        public string HeadingCode { get; set; }
        public string HeadingName { get; set; }

        public string SubHeadingId { get; set; }
        public string SubHeadingCode { get; set; }
        public string SubHeadingName { get; set; }

        public string HSCode { get; set; }
        public string OriginalHSCode { get; set; }
        public string HSName { get; set; }
        public string HSCodeVersion { get; set; }
    }

    public class ReceiptHeaderEntity
    {
        public long JOBNUMBER { get; set; }
        public decimal RECEIPTAMOUNT { get; set; }
        public decimal RECEIPTTAX { get; set; }
        public decimal RECEIPTNETAMOUNT { get; set; }
        public decimal DISCOUNTAMOUNT { get; set; }
    }


    public class PartyItems
    {
        public string CLIENTNAME { get; set; }
        public string CLIENTID { get; set; }
        public string PARTYTYPE { get; set; }

    }

    public class JobBookingStatusCountEntity
    {
        public decimal ALLJOBS { get; set; }
        public decimal ACTIVEJOBS { get; set; }
        public decimal EXPIREDJOBS { get; set; }
    }

    public class AdvanceReceiptEntity
    {

        public string RECEIPTFROMADDRESS { get; set; }
        public string RECEIPTNUMBER { get; set; }
        public string RECEIPTDATE { get; set; }
        public string PAYMENTTYPE { get; set; }
        public string PAYMENTREFNUMBER { get; set; }
        public string OPERATIONALJOBNUMBER { get; set; }
        public string PAYMENTOPTION { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public decimal NOTSUPPAMOUNTUSD { get; set; }

    }

    public class Compliance_Fields
    {
        public Int64 FIELDID { get; set; }
        public string FIELDINSTRUCTIONID { get; set; }
        public string FIELDDESCRIPTION { get; set; }
        public string FIELDTYPE { get; set; }
        public Int16 FIELDLENGTH { get; set; }
        public long JOBNUMBER { get; set; }
        public string VALUE { get; set; }
        public string DIRECTION { get; set; }
    }

}