﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    /// <summary>
    /// All Payment Details
    /// </summary>
    public class PaymentDetailsEntity
    {
        /// <summary>
        /// RECEIPT HEADER ID PRIMARY KEY
        /// </summary>
        public long RECEIPTHDRID { get; set; }
        /// <summary>
        /// JOB REFERENCE ID
        /// </summary>
        public long JOBNUMBER { get; set; }
        /// <summary>
        /// OPERATIONAL JOB NUMBER
        /// </summary>
        public string OPJOBNUMBER { get; set; }
        /// <summary>
        /// QUOTATION REFERENCE ID
        /// </summary>
        public long QUOTATIONID { get; set; }
        /// <summary>
        /// QUOTATION NUMBER
        /// </summary>
        public string QUOTATIONNUMBER { get; set; }
        /// <summary>
        /// RECEIPT TO NAME
        /// </summary>
        public string RECEIPTTO { get; set; }
        /// <summary>
        /// RECEIPT TYPE
        /// </summary>
        public string RECEIPTTYPE { get; set; }
        /// <summary>
        /// CUSTOMER TAX REG NO
        /// </summary>
        public string CUSTOMERTAXREGNO { get; set; }
        /// <summary>
        /// RECEIPT NUMBER
        /// </summary>
        public string RECEIPTNUMBER { get; set; }
        /// <summary>
        /// RECEIPT GENERATED DATE
        /// </summary>
        public DateTime RECEIPTDATE { get; set; }
        /// <summary>
        /// RECEIPT DUE DATE
        /// </summary>
        public DateTime RECEIPTDUEDATE { get; set; }
        /// <summary>
        /// PAYMENT TERMS
        /// </summary>
        public string PAYMENTTERMS { get; set; }
        /// <summary>
        /// RECEIPT AMOUNT
        /// </summary>
        public decimal RECEIPTAMOUNT { get; set; }
        /// <summary>
        /// RECEIPT TAX
        /// </summary>
        public decimal RECEIPTTAX { get; set; }
        /// <summary>
        /// RECEIPT NET AMOUNT
        /// </summary>
        public decimal RECEIPTNETAMOUNT { get; set; }
        /// <summary>
        /// CHARGE CURRENCY
        /// </summary>
        public string CURRENCY { get; set; }
        /// <summary>
        /// CHARGE CURRENCY DECIMAL
        /// </summary>
        public long CNYDECIMAL { get; set; }
        /// <summary>
        /// RECEIPT TO ADDRESS
        /// </summary>
        public string RECEIPTTOADDRESS { get; set; }
        /// <summary>
        /// RECEIPT FROM ADDRESS
        /// </summary>
        public string RECEIPTFROMADDRESS { get; set; }
        /// <summary>
        /// ENTITY ID
        /// </summary>
        public long ENTITYID { get; set; }
        /// <summary>
        /// ENTITY NAME
        /// </summary>
        public string ENTITYNAME { get; set; }
        /// <summary>
        /// STAKEHOLDER TAX GROUP ID
        /// </summary>
        public long STKTAXGROUPID { get; set; }
        /// <summary>
        /// JOB TAX GROUP ID
        /// </summary>
        public long JOBTAXGROUPID { get; set; }
        /// <summary>
        /// RECORD CREATED DATE
        /// </summary>
        public DateTime DATECREATED { get; set; }
        /// <summary>
        /// RECORD CREATED USER ID
        /// </summary>
        public string CREATEDBY { get; set; }
        /// <summary>
        /// RECORD MODIFIED DATE
        /// </summary>
        public DateTime DATEMODIFIED { get; set; }
        /// <summary>
        /// RECORD MODIFIED USER ID
        /// </summary>
        public string MODIFIEDBY { get; set; }
        /// <summary>
        /// STAKEHOLDER TAX GROUP CODE
        /// </summary>
        public string STKTAXGROUPCODE { get; set; }
        /// <summary>
        /// STAKEHOLDER TAX GROUP NAME
        /// </summary>
        public string STKTAXGROUPNAME { get; set; }
        /// <summary>
        /// JOB TAX GROUP CODE
        /// </summary>
        public string JOBTAXGROUPCODE { get; set; }
        /// <summary>
        /// JOB TAX GROUP NAME
        /// </summary>
        public string JOBTAXGROUPNAME { get; set; }
        /// <summary>
        /// ENTITY CODE
        /// </summary>
        public string ENTITYCODE { get; set; }
        /// <summary>
        /// Payment Charge Details
        /// </summary>
        public IList<PaymentChargeDetailsEntity> ChargeDetails { get; set; }
        /// <summary>
        /// Payment Tax Summary
        /// </summary>
        public IList<PaymentTaxSummaryEntity> TaxSummaryDetails { get; set; }
        /// <summary>
        /// Payment Transaction Details
        /// </summary>
        public IList<PaymentTransactionDetailsEntity> TransactionDetails { get; set; }
    }

    /// <summary>
    /// All Payment Details
    /// </summary>
    public class PaymentBasicDetailsEntity
    {
        /// <summary>
        /// RECEIPT HEADER ID PRIMARY KEY
        /// </summary>
        public long RECEIPTHDRID { get; set; }
        /// <summary>
        /// JOB REFERENCE ID
        /// </summary>
        public long JOBNUMBER { get; set; }
        /// <summary>
        /// OPERATIONAL JOB NUMBER
        /// </summary>
        public string OPJOBNUMBER { get; set; }
        /// <summary>
        /// QUOTATION REFERENCE ID
        /// </summary>
        public long QUOTATIONID { get; set; }
        /// <summary>
        /// QUOTATION NUMBER
        /// </summary>
        public string QUOTATIONNUMBER { get; set; }
        /// <summary>
        /// CUSTOMER TAX REG NO
        /// </summary>
        public string CUSTOMERTAXREGNO { get; set; }
        /// <summary>
        /// RECEIPT AMOUNT
        /// </summary>
        public decimal RECEIPTAMOUNT { get; set; }
        /// <summary>
        /// RECEIPT TAX
        /// </summary>
        public decimal RECEIPTTAX { get; set; }
        /// <summary>
        /// RECEIPT NET AMOUNT
        /// </summary>
        public decimal RECEIPTNETAMOUNT { get; set; }
        /// <summary>
        /// CHARGE CURRENCY
        /// </summary>
        public string CURRENCY { get; set; }
        /// <summary>
        /// CHARGE CURRENCY DECIMAL
        /// </summary>
        public long CNYDECIMAL { get; set; }
        /// <summary>
        /// ENTITY ID
        /// </summary>
        public long ENTITYID { get; set; }
        /// <summary>
        /// ENTITY NAME
        /// </summary>
        public string ENTITYNAME { get; set; }
        /// <summary>
        /// ENTITY CODE
        /// </summary>
        public string ENTITYCODE { get; set; }
        /// <summary>
        /// Payment Charge Details
        /// </summary>
        public IList<PaymentBasicChargeDetailsEntity> BasicChargeDetails { get; set; }
    }

    /// <summary>
    /// Payment Header Details
    /// </summary>
    public class PaymentHeaderDetailsEntity
    {
        /// <summary>
        /// RECEIPT HEADER ID PRIMARY KEY
        /// </summary>
        public long RECEIPTHDRID { get; set; }
        /// <summary>
        /// JOB REFERENCE ID
        /// </summary>
        public long JOBNUMBER { get; set; }
        /// <summary>
        /// OPERATIONAL JOB NUMBER
        /// </summary>
        public string OPJOBNUMBER { get; set; }
        /// <summary>
        /// QUOTATION REFERENCE ID
        /// </summary>
        public long QUOTATIONID { get; set; }
        /// <summary>
        /// QUOTATION NUMBER
        /// </summary>
        public string QUOTATIONNUMBER { get; set; }
        /// <summary>
        /// RECEIPT TO NAME
        /// </summary>
        public string RECEIPTTO { get; set; }
        /// <summary>
        /// RECEIPT TYPE
        /// </summary>
        public string RECEIPTTYPE { get; set; }
        /// <summary>
        /// CUSTOMER TAX REG NO
        /// </summary>
        public string CUSTOMERTAXREGNO { get; set; }
        /// <summary>
        /// RECEIPT NUMBER
        /// </summary>
        public string RECEIPTNUMBER { get; set; }
        /// <summary>
        /// RECEIPT GENERATED DATE
        /// </summary>
        public DateTime RECEIPTDATE { get; set; }
        /// <summary>
        /// RECEIPT DUE DATE
        /// </summary>
        public DateTime RECEIPTDUEDATE { get; set; }
        /// <summary>
        /// PAYMENT TERMS
        /// </summary>
        public string PAYMENTTERMS { get; set; }
        /// <summary>
        /// RECEIPT AMOUNT
        /// </summary>
        public decimal RECEIPTAMOUNT { get; set; }
        /// <summary>
        /// RECEIPT TAX
        /// </summary>
        public decimal RECEIPTTAX { get; set; }
        /// <summary>
        /// RECEIPT NET AMOUNT
        /// </summary>
        public decimal RECEIPTNETAMOUNT { get; set; }
        /// <summary>
        /// CHARGE CURRENCY
        /// </summary>
        public string CURRENCY { get; set; }
        /// <summary>
        /// CHARGE CURRENCY DECIMAL
        /// </summary>
        public long CNYDECIMAL { get; set; }
        /// <summary>
        /// RECEIPT TO ADDRESS
        /// </summary>
        public string RECEIPTTOADDRESS { get; set; }
        /// <summary>
        /// RECEIPT FROM ADDRESS
        /// </summary>
        public string RECEIPTFROMADDRESS { get; set; }
        /// <summary>
        /// ENTITY ID
        /// </summary>
        public long ENTITYID { get; set; }
        /// <summary>
        /// ENTITY NAME
        /// </summary>
        public string ENTITYNAME { get; set; }
        /// <summary>
        /// STAKEHOLDER TAX GROUP ID
        /// </summary>
        public long STKTAXGROUPID { get; set; }
        /// <summary>
        /// JOB TAX GROUP ID
        /// </summary>
        public long JOBTAXGROUPID { get; set; }
        /// <summary>
        /// RECORD CREATED DATE
        /// </summary>
        public DateTime DATECREATED { get; set; }
        /// <summary>
        /// RECORD CREATED USER ID
        /// </summary>
        public string CREATEDBY { get; set; }
        /// <summary>
        /// RECORD MODIFIED DATE
        /// </summary>
        public DateTime DATEMODIFIED { get; set; }
        /// <summary>
        /// RECORD MODIFIED USER ID
        /// </summary>
        public string MODIFIEDBY { get; set; }
        /// <summary>
        /// STAKEHOLDER TAX GROUP CODE
        /// </summary>
        public string STKTAXGROUPCODE { get; set; }
        /// <summary>
        /// STAKEHOLDER TAX GROUP NAME
        /// </summary>
        public string STKTAXGROUPNAME { get; set; }
        /// <summary>
        /// JOB TAX GROUP CODE
        /// </summary>
        public string JOBTAXGROUPCODE { get; set; }
        /// <summary>
        /// JOB TAX GROUP NAME
        /// </summary>
        public string JOBTAXGROUPNAME { get; set; }
        /// <summary>
        /// ENTITY CODE
        /// </summary>
        public string ENTITYCODE { get; set; }
    }

    /// <summary>
    /// Charge Details
    /// </summary>
    public class PaymentBasicChargeDetailsEntity
    {
        /// <summary>
        ///RECEIPT CHARGE DETAILS ID PRIMARY KEY
        /// </summary>
        long RECEIPTCHARGEDTLID { get; set; }
        /// <summary>
        ///RECEIPT HEADER ID FOREIGN KEY
        /// </summary>
        long RECEIPTHDRID { get; set; }
        /// <summary>
        ///QUOTATION SERVICE CHARGE ID REFERENCE
        /// </summary>
        long SERVICECHARGEID { get; set; }
        /// <summary>
        ///CHARGE ID
        /// </summary>
        long CHARGEID { get; set; }
        /// <summary>
        ///CHARGE CODE
        /// </summary>
        string CHARGECODE { get; set; }
        /// <summary>
        ///CHARGE NAME
        /// </summary>
        string CHARGENAME { get; set; }
        /// <summary>
        ///LOCAL CHARGE NAME
        /// </summary>
        string LOCALCHARGENAME { get; set; }
        /// <summary>
        ///CHARGE AMOUNT
        /// </summary>
        decimal AMOUNT { get; set; }
        /// <summary>
        ///CHARGE TAX AMOUNT
        /// </summary>
        decimal TAXAMOUNT { get; set; }
        /// <summary>
        ///CHARGE NET AMOUNT
        /// </summary>
        decimal NETAMOUNT { get; set; }
        /// <summary>
        ///CHARGE CURRENCY
        /// </summary>
        string CURRENCY { get; set; }
        /// <summary>
        ///CHARGE CURRENCY DECIMAL
        /// </summary>
        long CNYDECIMAL { get; set; }
    }

    /// <summary>
    /// Charge Details
    /// </summary>
    public class PaymentChargeDetailsEntity
    {/// <summary>
        ///RECEIPT CHARGE DETAILS ID PRIMARY KEY
        /// </summary>
        long RECEIPTCHARGEDTLID { get; set; }
        /// <summary>
        ///RECEIPT HEADER ID FOREIGN KEY
        /// </summary>
        long RECEIPTHDRID { get; set; }
        /// <summary>
        ///QUOTATION SERVICE CHARGE ID REFERENCE
        /// </summary>
        long SERVICECHARGEID { get; set; }
        /// <summary>
        ///CHARGE ID
        /// </summary>
        long CHARGEID { get; set; }
        /// <summary>
        ///CHARGE CODE
        /// </summary>
        string CHARGECODE { get; set; }
        /// <summary>
        ///CHARGE NAME
        /// </summary>
        string CHARGENAME { get; set; }
        /// <summary>
        ///LOCAL CHARGE NAME
        /// </summary>
        string LOCALCHARGENAME { get; set; }
        /// <summary>
        ///CHARGE AMOUNT
        /// </summary>
        decimal AMOUNT { get; set; }
        /// <summary>
        ///CHARGE TAX AMOUNT
        /// </summary>
        decimal TAXAMOUNT { get; set; }
        /// <summary>
        ///CHARGE NET AMOUNT
        /// </summary>
        decimal NETAMOUNT { get; set; }
        /// <summary>
        ///CHARGE CURRENCY
        /// </summary>
        string CURRENCY { get; set; }
        /// <summary>
        ///CHARGE CURRENCY DECIMAL
        /// </summary>
        long CNYDECIMAL { get; set; }
        /// <summary>
        ///SUB TAX APPLICABLE FOR CHARGE
        /// </summary>
        long ISSUBTAXAPPLICABLE { get; set; }
        /// <summary>
        ///TAX CODE ID
        /// </summary>
        long TAXCODEID { get; set; }
        /// <summary>
        ///TAX CODE
        /// </summary>
        string TAXCODE { get; set; }
        /// <summary>
        ///TAX DESCRIPTION
        /// </summary>
        string TAXDESC { get; set; }
        /// <summary>
        ///TAX PRINT DESCRIPTION
        /// </summary>
        string TAXPRINTDESC { get; set; }
        /// <summary>
        ///TAX RATE
        /// </summary>
        decimal TAXRATE { get; set; }
        /// <summary>
        ///CHARGE TAX GROUP ID
        /// </summary>
        long CHARGETAXGROUPID { get; set; }
        /// <summary>
        ///RECORD CREATED DATE
        /// </summary>
        DateTime DATECREATED { get; set; }
        /// <summary>
        ///RECORD CREATED USER ID
        /// </summary>
        string CREATEDBY { get; set; }
        /// <summary>
        ///RECORD MODIFIED DATE
        /// </summary>
        DateTime DATEMODIFIED { get; set; }
        /// <summary>
        ///RECORD MODIFIED USER ID
        /// </summary>
        string MODIFIEDBY { get; set; }
        /// <summary>
        ///UOM
        /// </summary>
        string UOM { get; set; }
        /// <summary>
        ///UNIT RATE
        /// </summary>
        decimal UNITRATE { get; set; }
        /// <summary>
        /// Charge Code tax Details
        /// </summary>
        public IList<PaymentTaxDetailsEntity> TaxDetails { get; set; }
    }

    /// <summary>
    /// Tax details for the charge code
    /// </summary>
    public class PaymentTaxDetailsEntity
    {
        /// <summary>
        ///RECEIPT TAX DETAILS ID PRIMARY KEY
        /// </summary>				
        public long RECEIPTTAXDTLID { get; set; }
        /// <summary>
        ///RECEIPT CHARGE DETAILS ID FOREIGN KEY
        /// </summary>				
        public long RECEIPTCHARGEDTLID { get; set; }
        /// <summary>
        ///RECEIPT HEADER ID FOREIGN KEY
        /// </summary>				
        public long RECEIPTHDRID { get; set; }
        /// <summary>
        ///TAX TYPE - SUB TAX / MAIN TAX
        /// </summary>				
        public string TAXTYPE { get; set; }
        /// <summary>
        ///TAX CODE ID
        /// </summary>				
        public long TAXCODEID { get; set; }
        /// <summary>
        ///TAX CODE
        /// </summary>				
        public string TAXCODE { get; set; }
        /// <summary>
        ///TAX DESCRIPTION
        /// </summary>				
        public string TAXDESC { get; set; }
        /// <summary>
        ///TAX PRINT DESCRIPTION
        /// </summary>				
        public string TAXPRINTDESC { get; set; }
        /// <summary>
        ///TAX RATE
        /// </summary>				
        public decimal TAXRATE { get; set; }
        /// <summary>
        ///SUB TAX CATEGORY TYPE ID
        /// </summary>				
        public long SUBTAXCATEGORYTYPEID { get; set; }
        /// <summary>
        ///SUB TAX CATEGORY TYPE
        /// </summary>				
        public string SUBTAXCATEGORYTYPE { get; set; }
        /// <summary>
        ///SUB TAX RATIO
        /// </summary>				
        public decimal SUBTAXRATIO { get; set; }
        /// <summary>
        ///DEPENDENT SUB TAX CODE ID IN CASE OF SUB TAX APPLICABLE
        /// </summary>				
        public long DEPENDENTSUBTAXCODEID { get; set; }
        /// <summary>
        ///CALCULATION BASIS ID IN CASE OF SUB TAX APPLICABLE
        /// </summary>				
        public long CALCULATIONBASISID { get; set; }
        /// <summary>
        ///CALCULATION BASIS IN CASE OF SUB TAX APPLICABLE
        /// </summary>				
        public string CALCULATIONBASIS { get; set; }
        /// <summary>
        ///AMOUNT FOR TAX CALCULATION
        /// </summary>				
        public decimal AMOUNTFORTAX { get; set; }
        /// <summary>
        ///TAX AMOUNT
        /// </summary>				
        public decimal TAXAMOUNT { get; set; }
        /// <summary>
        ///TAX CURRENCY
        /// </summary>				
        public string CURRENCY { get; set; }
        /// <summary>
        ///TAX CURRENCY DECIMAL
        /// </summary>				
        public long CNYDECIMAL { get; set; }
        /// <summary>
        ///TAX CODE DISPLAY ORDER
        /// </summary>				
        public long DISPLAYORDER { get; set; }
        /// <summary>
        ///SUB TAX APPLICABLE FOR CHARGE
        /// </summary>				
        public long ISSUBTAXAPPLICABLE { get; set; }
        /// <summary>
        ///RECORD CREATED DATE
        /// </summary>				
        public DateTime DATECREATED { get; set; }
        /// <summary>
        ///RECORD CREATED USER ID
        /// </summary>				
        public string CREATEDBY { get; set; }
        /// <summary>
        ///RECORD MODIFIED DATE
        /// </summary>				
        public DateTime DATEMODIFIED { get; set; }
        /// <summary>
        ///RECORD MODIFIED USER ID
        /// </summary>				
        public string MODIFIEDBY { get; set; }
    }

    /// <summary>
    /// Tax Summary Details
    /// </summary>
    public class PaymentTaxSummaryEntity
    {
        /// <summary>
        /// RECEIPT TAX SUMMARY ID PRIMARY KEY
        /// </summary>
        public long RECEIPTTAXSUMMARYID { get; set; }
        /// <summary>
        /// RECEIPT HEADER ID FOREIGN KEY
        /// </summary>
        public long RECEIPTHDRID { get; set; }
        /// <summary>
        /// TAX TYPE - TAX / SUB TAX
        /// </summary>
        public string TAXTYPE { get; set; }
        /// <summary>
        /// TAX CODE ID
        /// </summary>
        public long TAXCODEID { get; set; }
        /// <summary>
        /// TAX CODE
        /// </summary>
        public string TAXCODE { get; set; }
        /// <summary>
        /// TAX DESCRIPTION
        /// </summary>
        public string TAXDESC { get; set; }
        /// <summary>
        /// TAX PRINT DESCRIPTION
        /// </summary>
        public string TAXPRINTDESC { get; set; }
        /// <summary>
        /// CHARGE AMOUNT FOR TAX
        /// </summary>
        public decimal TOTALCHARGEAMOUNT { get; set; }
        /// <summary>
        /// CHARGE AMOUNT TAX
        /// </summary>
        public decimal TOTALTAXAMOUNT { get; set; }
        /// <summary>
        /// TAX CURRENCY
        /// </summary>
        public string CURRENCY { get; set; }
        /// <summary>
        /// TAX CURRENCY DECIMAL
        /// </summary>
        public long CNYDECIMAL { get; set; }
        /// <summary>
        /// RECORD CREATED DATE
        /// </summary>
        public DateTime DATECREATED { get; set; }
        /// <summary>
        /// RECORD CREATED USER ID
        /// </summary>
        public string CREATEDBY { get; set; }
        /// <summary>
        /// RECORD MODIFIED DATE
        /// </summary>
        public DateTime DATEMODIFIED { get; set; }
        /// <summary>
        /// RECORD MODIFIED USER ID
        /// </summary>
        public string MODIFIEDBY { get; set; }
    }

    /// <summary>
    /// Payment Transaction Details
    /// </summary>
    public class PaymentTransactionDetailsEntity
    {
        /// <summary>
        ///RECEIPT TAX SUMMARY ID PRIMARY KEY
        /// </summary>
        public long RECEIPTPAYMENTDTLSID { get; set; }
        /// <summary>
        ///RECEIPT HEADER ID PRIMARY KEY
        /// </summary>
        public long RECEIPTHDRID { get; set; }
        /// <summary>
        ///JOB REFERENCE ID
        /// </summary>
        public long JOBNUMBER { get; set; }
        /// <summary>
        ///QUOTE REFERENCE ID
        /// </summary>
        public long QUOTATIONID { get; set; }
        /// <summary>
        ///PAYMENT OPTION
        /// </summary>
        public string PAYMENTOPTION { get; set; }
        /// <summary>
        ///PAYMENT SUCCESSFUL
        /// </summary>
        public long ISPAYMENTSUCESS { get; set; }
        /// <summary>
        ///PAYMENT REFENCE NUMBER
        /// </summary>
        public string PAYMENTREFNUMBER { get; set; }
        /// <summary>
        ///RECORD CREATED DATE
        /// </summary>
        public DateTime DATECREATED { get; set; }
        /// <summary>
        ///RECORD CREATED USER ID
        /// </summary>
        public string CREATEDBY { get; set; }
        /// <summary>
        ///RECORD MODIFIED DATE
        /// </summary>
        public DateTime DATEMODIFIED { get; set; }
        /// <summary>
        ///RECORD MODIFIED USER ID
        /// </summary>
        public string MODIFIEDBY { get; set; }

        public string PAYMENTREFMESSAGE { get; set; }

        public string PAYMENTTYPE { get; set; }
        public string TRANSMODE { get; set; }
        public double TRANSAMOUNT { get; set; }
        public string TRANSCURRENCY { get; set; }
        public string TRANSDESCRIPTION { get; set; }
        public string TRANSBANKNAME { get; set; }
        public string TRANSBRANCHNAME { get; set; }
        public string TRANSCHEQUENO { get; set; }
        public DateTime? TRANSCHEQUEDATE { get; set; }

        public decimal BRANCHENTITYKEY { get; set; }
        public decimal ADDITIONALCHARGEAMOUNT { get; set; }
        public string ADDITIONALCHARGESTATUS { get; set; }
        public decimal ADDITIONALCHARGEAMOUNTINUSD { get; set; }

        public string PAIDCURRENCY { get; set; }
        public decimal PREFFEREDCURRENCYAMOUNT { get; set; }

    }
}