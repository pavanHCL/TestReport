﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class PaymentEntity
    {
        public long JOBNUMBER { get; set; }
        public long RECEIPTHDRID { get; set; }
        public string OPJOBNUMBER { get; set; }
        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public decimal RECEIPTNETAMOUNT { get; set; }
        public string CURRENCY { get; set; }

        public string TOKEN { get; set; }
        public double AMOUNT { get; set; }
        public string CARDHOLDERNAME { get; set; }
        public string CARDHOLDERFIRSTNAME { get; set; }
        public string CARDHOLDERLASTNAME { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public string ADDRESSLINE2 { get; set; }
        public string ADDRESSCITY { get; set; }
        public string ADDRESSPOSTCODE { get; set; }
        public string ADDRESSCOUNTRY { get; set; }

        public string SHIPPERNAME { get; set; }
        public string CONSIGNEENAME { get; set; }
        public string MODEOFTRANSPORT { get; set; }
        public string MOVEMENTTYPE { get; set; }
        public string CARDNUMBER { get; set; }

        public string CVV { get; set; }
        public string CARDTYPE { get; set; }
        public string EXPMONTH { get; set; }
        public string EXPYEAR { get; set; }

        public string PAYMENTOPTION { get; set; }
        public string PAYMENTTYPE { get; set; }
        public string TRANSACTIONMODE { get; set; }
        public string TRANSAMOUNT { get; set; }
        public string TRANSCURRENCY { get; set; }
        public string TRANSDESCRIPTION { get; set; }
        public string TRANSBANKNAME { get; set; }
        public string TRANSBRANCHNAME { get; set; }       
        public string TRANSCHEQUENO { get; set; }
        public DateTime? TRANSCHEQUEDATE { get; set; }
        public string TRANSCDDESCRIPTION { get; set; }
        public string TRANSCDAMOUNT { get; set; }
        public string TRANSCDCURRENCY { get; set; }
        

        public string WTTRANSBANKNAME { get; set; }
        public string WTTRANSBRANCHNAME { get; set; }
        public string WTTRANSCHEQUENO { get; set; }
        public DateTime? WTTRANSCHEQUEDATE { get; set; }
        public string WTTRANSAMOUNT { get; set; }
        public string WTTRANSCURRENCY { get; set; }
        public string WTTRANSDESCRIPTION { get; set; }
        
        public double CREDITAMOUNT { get; set; }
        public string CREDITCURRENCY { get; set; }

        public string OFFICESITECITY { get; set; }
        public IList<SSPBranchListEntity> BRANCHLIST { get; set; }
        public SSPEntityListEntity ENTITYLIST { get; set; }
        public SSPPaymentOptionList PAYMENTOPTIONLIST { get; set; }

        public DateTime CARGOAVAILABLEFROM { get; set; }
        public string DISCOUNTCODE { get; set; }
        public decimal DISCOUNTAMOUNT { get; set; }
        public DateTime DISCOUNTEXPIRYDATE { get; set; }
        public decimal NOTSUPPAMOUNTUSD { get; set; }
        public string UserId { get; set; }
        public decimal ADDITIONALCHARGEAMOUNT { get; set; }
        public string ADDITIONALCHARGESTATUS { get; set; }
        public decimal ADDITIONALCHARGEAMOUNTINUSD { get; set; }
        public Int64 CHARGESETID { get; set; }
        public string QUOTESOURCE { get; set; }
        public string stripePKKey { get; set; }
        public string REFERREREMAILID { get; set; }
        public string REFERRERMODEL { get; set; }
        public double REFERRERBALAMOUNT { get; set; }
        public double REFERRERBALCOUPONCOUNT { get; set; }
        public string REFERRERCOUPONCODE { get; set; }

        public int LIVEUPLOADS { get; set; }
        public int LOADINGDOCKAVAILABLE { get; set; }
        public int CARGOPALLETIZED { get; set; }
        public int ORIGINALDOCUMENTSREQUIRED { get; set; }
        public int TEMPERATURECONTROLREQUIRED { get; set; }
        public int COMMERCIALPICKUPLOCATION { get; set; }
        public string SPECIALINSTRUCTIONS { get; set; }
    }
    public class BranchCityEntity
    {
        public string NAME { get; set; }
        public decimal UNLOCATIONID { get; set; } 
    }
    public class GetCountryWeekends
    {
        public Int64 COUNTRYID { get; set; }
        public Int64 WEEKOFF { get; set; }
        public DateTime HOLIDAYDATE { get; set; }
    }
    public class GetHolidays
    {
        public Int64 COUNTRYID { get; set; }       
        public DateTime HOLIDAYDATE { get; set; }
    }
    public class GetCountryWeekendsList
    {
        public IList<GetCountryWeekends> GetCountryWeekends { get; set; }
    }

    public class JobDetails
    {
        public long JOBNUMBER { get; set; }
        //public long RECEIPTHDRID { get; set; }
        public string OPJOBNUMBER { get; set; }
        public decimal RECEIPTNETAMOUNT { get; set; }
        public string RRECEIPTNETAMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string SHIPPERNAME { get; set; }
        public string CONSIGNEENAME { get; set; }
        public string MODEOFTRANSPORT { get; set; }
        public string MOVEMENTTYPE { get; set; }
        public DateTime CARGOAVAILABLEFROM { get; set; }
        public string ADDITIONALCHARGESTATUS { get; set; }
        public decimal ADDITIONALCHARGEAMOUNT { get; set; }
        public string RADDITIONALCHARGEAMOUNT { get; set; }
        public decimal ADDITIONALCHARGEAMOUNTINUSD { get; set; }
    }

    public class PayementMenthods
    {
        public JobDetails JobDetails { get; set; }
        public SSPPaymentOptionList PayMethods { get; set; }
        public BusinessCredit BusinessCredit { get; set; }
    }

    public class BusinessCredit
    {
        public decimal APPROVEDCREDITLIMIT { get; set; }
        public decimal CURRENTOSBALANCE { get; set; }
        public string CURRENCY { get; set; }
    }

    public class JobandQuoteEntity
    {
        public string PRODUCTNAME { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public string OCOUNTRYNAME { get; set; }
        public string DCOUNTRYNAME { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public int LIVEUPLOADS { get; set; }
        public int LOADINGDOCKAVAILABLE { get; set; }
        public int CARGOPALLETIZED { get; set; }
        public int ORIGINALDOCUMENTSREQUIRED { get; set; }
        public int TEMPERATURECONTROLREQUIRED { get; set; }
        public int COMMERCIALPICKUPLOCATION { get; set; }
        public string SPECIALINSTRUCTIONS { get; set; }
        public string COUNTRYNAME { get; set; }
    }
 
}
