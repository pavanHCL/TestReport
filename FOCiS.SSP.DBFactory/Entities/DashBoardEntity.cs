﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FOCiS.SSP.DBFactory.Entities
{
    public class DashBoardEntity
    {
        public decimal BOOKINGSDONE { get; set; }
        public decimal TOTALVOLUME { get; set; }
        public decimal CREDITLIMIT { get; set; }
        public decimal TotalSpent { get; set; }
        public string Status { get; set; }
        public string CURRENCYID { get; set; }

    }    
}
