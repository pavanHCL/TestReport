﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class UserEntity
    {
        public string USERID { get; set; }
        public int USERTYPE { get; set; }
        public string EMAILID { get; set; }
        public string PASSWORD { get; set; }
        public int ISTEMPPASSWORD { get; set; }
        public string TOKEN { get; set; }
        public DateTime TOKENEXPIRY { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string ISDCODE { get; set; }
        public string MOBILENUMBER { get; set; }
        public int ISTERMSAGREED { get; set; }
        public long COUNTRYID { get; set; }
        public int ACCOUNTSTATUS { get; set; }
        public DateTime LASTLOGONTIME { get; set; }
        public long TIMEZONEID { get; set; }
        public long USERCULTUREID { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public int ISVATREGISTERED { get; set; }
        public string VATREGNUMBER { get; set; }
        public string SALUTATION { get; set; }
        public long JOBTITLEID { get; set; }
        public string JOBTITLE { get; set; }
        public string COMPANYLINK { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public string UAADDRESSTYPE { get; set; }
        public string UAHOUSENO { get; set; }
        public string UABUILDINGNAME { get; set; }
        public string UAADDRESSLINE1 { get; set; }
        public string UAADDRESSLINE2 { get; set; }
        public long UACOUNTRYID { get; set; }
        public string UACOUNTRYCODE { get; set; }
        public string UACOUNTRYNAME { get; set; }
        public long UASTATEID { get; set; }
        public string UASTATECODE { get; set; }
        public string UASTATENAME { get; set; }
        public long UACITYID { get; set; }
        public string UACITYCODE { get; set; }
        public string UACITYNAME { get; set; }
        public string UAPOSTCODE { get; set; }
        public string UAFULLADDRESS { get; set; }
        public int UAISDEFAULT { get; set; }
        public DateTime UADATECREATED { get; set; }
        public DateTime UADATEMODIFIED { get; set; }
        public string IMGFILENAME { get; set; }
        public byte[] IMGCONTENT { get; set; }
        public long DEPTID { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public string WORKPHONE { get; set; }
        public long FAILEDPASSWORDATTEMPT { get; set; }
        public IList<UMUserAddressDetailsEntity> UserAddress { get; set; }
        public IList<CreditEntity> CreditItems { get; set; }
        public long NOTIFICATIONID { get; set; }
        public string InstantPASSWORD { get; set; }
        public DateTime InstTime { get; set; }
        public DateTime ServerDate { get; set; }
        public string GuestEmail { get; set; }
        public long ListId { get; set; }
        public long ActiveCampaignContactId { get; set; }
        public bool NOTIFICATIONSUBSCRIPTION { get; set; }
        public int SubscriptionStatus { get; set; }
        public int SubscriberId { get; set; }
        public string CRMEmail { get; set; }
        public string SHIPPINGEXPERIENCE { get; set; }
        public string SHIPMENTPROCESS { get; set; }
    }

    public class UserProfileEntity
    {
        public string USERID { get; set; }
        public int USERTYPE { get; set; }
        public string EMAILID { get; set; }
        //public string PASSWORD { get; set; }
        public int ISTEMPPASSWORD { get; set; }
        public string TOKEN { get; set; }
        public DateTime TOKENEXPIRY { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string ISDCODE { get; set; }
        public string MOBILENUMBER { get; set; }
        public int ISTERMSAGREED { get; set; }
        public long COUNTRYID { get; set; }
        public int ACCOUNTSTATUS { get; set; }
        public DateTime LASTLOGONTIME { get; set; }
        public long TIMEZONEID { get; set; }
        public long USERCULTUREID { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public int ISVATREGISTERED { get; set; }
        public string VATREGNUMBER { get; set; }
        public string SALUTATION { get; set; }
        public string OTHERJOBTITLE { get; set; }
        public string COMPANYLINK { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public string WORKPHONE { get; set; }
        public string IMGFILENAME { get; set; }
        public long EXISTINGCUSTOMER { get; set; }
        public long NOTIFICATIONSUBSCRIPTION { get; set; }
        public long USEMYCREIDT { get; set; }
        public long UAUSERADTLSID { get; set; }
        public string UAADDRESSTYPE { get; set; }
        public string UAHOUSENO { get; set; }
        public string UABUILDINGNAME { get; set; }
        public string UAADDRESSLINE1 { get; set; }
        public string UAADDRESSLINE2 { get; set; }
        public long UACOUNTRYID { get; set; }
        public long UACOUNTRYDUMMYID { get; set; }
        public string UACOUNTRYCODE { get; set; }
        public string UACOUNTRYNAME { get; set; }
        public long UASTATEID { get; set; }
        public string UASTATECODE { get; set; }
        public string UASTATENAME { get; set; }
        public long UACITYID { get; set; }
        public string UACITYCODE { get; set; }
        public string UACITYNAME { get; set; }
        public string UAPOSTCODE { get; set; }
        public string UAFULLADDRESS { get; set; }
        public int UAISDEFAULT { get; set; }
        public DateTime UADATECREATED { get; set; }
        public DateTime UADATEMODIFIED { get; set; }
        // public IList<UMUserAddressDetailsEntity> UserAddress { get; set; }
        public long CREDITAPPID { get; set; }
        public string INDUSTRYID { get; set; }
        public long NOOFEMP { get; set; }
        public string SUBVERTICAL { get; set; }
        public string PARENTCOMPANY { get; set; }
        public decimal ANNUALTURNOVER { get; set; }
        public string ANNUALTURNOVERCURRENCY { get; set; }
        public string PARTY { get; set; }
        public string DBNUMBER { get; set; }
        public string PRODUCTPROFILE { get; set; }
        public string SALESCHANNEL { get; set; }
        public decimal PREVIOUSYEARREVENUE { get; set; }
        public decimal PREVIOUSYEARNR { get; set; }
        public decimal PREVIOUSYEARNRMARGIN { get; set; }
        public decimal PREVIOUSYEARBILLING { get; set; }
        public decimal NEXTYEARREVENUE { get; set; }
        public decimal NEXTYEARNR { get; set; }
        public decimal NEXTYEARNRMARGIN { get; set; }
        public decimal NEXTYEARBILLING { get; set; }
        public decimal CURRENTYEARREVENUE { get; set; }
        public decimal CURRENTYEARNR { get; set; }
        public decimal CURRENTYEARNRMARGIN { get; set; }
        public decimal CURRENTYEARBILLING { get; set; }
        public string HFMENTRYCODE { get; set; }
        public string HFMENTRYCURRENCY { get; set; }
        public decimal REQUESTEDCREDITLIMIT { get; set; }
        public string REQUESTEDCREDITLIMITCURRENCY { get; set; }
        public string CURRENTPAYTERMSNAME { get; set; }
        public string COMMENTS { get; set; }
        public DateTime CREDITLIMITEXPDATE { get; set; }
        public string CURRENCYID { get; set; }
        public string CURRENCYDESC { get; set; }
        public string DESCRIPTIONOFKS { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime CreditDATECREATED { get; set; }
        public DateTime CreditDATEMODIFIED { get; set; }
        public string MODIFIEDBY { get; set; }
        public string OWNERORGID { get; set; }
        public string OWNERLOCID { get; set; }
        public long STATEID { get; set; }
        public decimal APPROVEDCREDITLIMIT { get; set; }
        public decimal CURRENTOSBALANCE { get; set; }
        public Int64 NOTIFICATIONID { get; set; }
        public string CREDITREFNO { get; set; }
        public IList<UserPartyMaster> PartyMaster { get; set; }
        public IList<Requests> Requests { get; set; }
        public IList<Chat> Chat { get; set; }
        public IList<AlternateEmails> AlternateEmails { get; set; }        
        public decimal PREVIEW { get; set; }
        public decimal Submitted { get; set; }
        public decimal Closed { get; set; }
        public int TabStatus { get; set; }
        public string LENGTHUNIT { get; set; }
        public string WEIGHTUNIT { get; set; }
        public string PREFERREDCURRENCY { get; set; }
        public string Industry { get; set; }
        public string OtherIndustry { get; set; }
        public long ISFAPIAO { get; set; }
        public string PREFERREDCURRENCYCODE { get; set; }
        //public decimal EXTUSERFLAG { get; set; }
        public byte[] IMGCONTENT { get; set; }
        //Added for FAPIAO
        public long FUSERFAPIAOID { get; set; }
        public string FCOMPANYNAMECHINESE { get; set; }
        public string FCOMPANYNAME { get; set; }
        public string FPHONENUMBER { get; set; }
        public string FEMAILID { get; set; }
        public string FCOMPANYADDRESSCHINESE { get; set; }
        public string FCOMPANYADDRESS { get; set; }
        public string FTAXIDENTIFICATIONNO { get; set; }       
        public string FBANKACCOUNTNO { get; set; }
        public string FBANKNAME { get; set; }
        public DateTime FDATECREATED { get; set; }
        public DateTime FDATEMODIFIED { get; set; }
        public long ISINDIVIDUAL { get; set; }
        public string FINDIVIDUALNAME { get; set; }
        public string FINDTAXIDENTIFICATION { get; set; }
        public string FINDPHONENUMBER { get; set; }
        public string FINDEMAILID { get; set; }
        //public long DEPTID { get; set; }
        //public string DEPTCODE { get; set; }
        //public string DEPTNAME { get; set; }
        //public long JOBTITLEID { get; set; }
        //public string JOBTITLE { get; set; }
        //Added for Credit
        public string CreditStatus { get; set; }
        public string FILENAME { get; set; }
        public IList<CreditDocEntity> CreditDoc { get; set; }
        public string REJECTIONCOMMENTS { get; set; }
        public List<CreditCommentEntity> CreditComments { get; set; }
        public string ISNPC { get; set; }
        public string CRM { get; set; }
        public string BCOUNTRYSETUP { get; set; }
        public string SHIPPINGEXPERIENCE { get; set; }
        public string SHIPMENTPROCESS { get; set; }
        public string PERSONALASSISTANCE { get; set; }
        public string EXISTINGFREIGHTFORWARDER { get; set; }
        public bool ResetQuotes { get; set; }
        //For Referral points
        public string REFERRERMODEL { get; set; }
        public double REFERRERBALAMOUNT { get; set; }
        public double REFERRERBALCOUPONCOUNT { get; set; }
    }

    public class UMUserAddressDetailsEntity
    {
        public long USERADTLSID { get; set; }
        public string USERID { get; set; }
        public string ADDRESSTYPE { get; set; }
        public string HOUSENO { get; set; }
        public string BUILDINGNAME { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public string ADDRESSLINE2 { get; set; }
        public long COUNTRYID { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public long STATEID { get; set; }
        public string STATECODE { get; set; }
        public string STATENAME { get; set; }
        public long CITYID { get; set; }
        public string CITYCODE { get; set; }
        public string CITYNAME { get; set; }
        public string POSTCODE { get; set; }
        public string FULLADDRESS { get; set; }
        public int ISDEFAULT { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
    }

    public class UMConfigurationEntity
    {
        public long CONFIGID { get; set; }
        public string CONFIGNAME { get; set; }
        public string CONFIGVALUE { get; set; }
    }

    public class JobTitleEntity
    {
        public long JOBTITLEID { get; set; }
        public string JOBTITLEDESC { get; set; }
    }

    public class NotificationEntity
    {
        public Int64 NOTIFICATIONID { get; set; }
        public Int64 NOTIFICATIONTYPEID { get; set; }
        public string USERID { get; set; }
        public Int64 QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public DateTime DATECREATED { get; set; }
        public string NOTIFICATIONCODE { get; set; }
        public Int64 JOBNUMBER { get; set; }
        public DateTime DATEREQUESTED { get; set; }
        public string CONSIGNMENTID { get; set; }
        public string BOOKINGID { get; set; }
        public string NOTIFICATIONSTATUS { get; set; }
        public string PARTYNAME { get; set; }
        public string PARTYSTATUS { get; set; }
        public string REQID { get; set; }
        public Int64 CHARGESETID { get; set; }
    }
    public class UserNameEntity
    {
        public string USERID { get; set; }
        public int USERTYPE { get; set; }
        public string EMAILID { get; set; }
        public string PASSWORD { get; set; }
        public int ISTEMPPASSWORD { get; set; }
        public string TOKEN { get; set; }
        public DateTime TOKENEXPIRY { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string ISDCODE { get; set; }
        public long MOBILENUMBER { get; set; }
        public int ISTERMSAGREED { get; set; }
        public long COUNTRYID { get; set; }
        public int ACCOUNTSTATUS { get; set; }
        public DateTime LASTLOGONTIME { get; set; }
        public long TIMEZONEID { get; set; }
        public long USERCULTUREID { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
        public int ISVATREGISTERED { get; set; }
        public string VATREGNUMBER { get; set; }
        public string SALUTATION { get; set; }
        public long JOBTITLEID { get; set; }
        public string JOBTITLE { get; set; }
        public string COMPANYLINK { get; set; }
        public string COUNTRYCODE { get; set; }
        public string COUNTRYNAME { get; set; }
        public string UAADDRESSTYPE { get; set; }
        public string UAHOUSENO { get; set; }
        public string UABUILDINGNAME { get; set; }
        public string UAADDRESSLINE1 { get; set; }
        public string UAADDRESSLINE2 { get; set; }
        public long UACOUNTRYID { get; set; }
        public string UACOUNTRYCODE { get; set; }
        public string UACOUNTRYNAME { get; set; }
        public long UASTATEID { get; set; }
        public string UASTATECODE { get; set; }
        public string UASTATENAME { get; set; }
        public long UACITYID { get; set; }
        public string UACITYCODE { get; set; }
        public string UACITYNAME { get; set; }
        public string UAPOSTCODE { get; set; }
        public string UAFULLADDRESS { get; set; }
        public int UAISDEFAULT { get; set; }
        public DateTime UADATECREATED { get; set; }
        public DateTime UADATEMODIFIED { get; set; }
        public string IMGFILENAME { get; set; }
        public byte[] IMGCONTENT { get; set; }
        public long DEPTID { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public string WORKPHONE { get; set; }
        public IList<UMUserAddressDetailsEntity> UserAddress { get; set; }
        public IList<CreditEntity> CreditItems { get; set; }
        public long NOTIFICATIONID { get; set; }
    }

    public class UserPartyMaster
    {
        public Int64 SSPCLIENTID { get; set; }
        public string Clientname { get; set; }
        public string HOUSENO { get; set; }
        public string BUILDINGNAME { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public string ADDRESSLINE2 { get; set; }
        public string COUNTRYNAME { get; set; }
        public string CITYNAME { get; set; }
        public string STATENAME { get; set; }
        public string PHONE { get; set; }
        public string EMAILID { get; set; }
        public string Pincode { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string SALUTATION { get; set; }
        public decimal UPERSONAL { get; set; }
        public decimal COUNTRYID { get; set; }
        public decimal STATEID { get; set; }
        public decimal CITYID { get; set; }
        public string STATUS { get; set; }
        public string NICKNAME { get; set; }
        public string PARTYSTATUS { get; set; }
        public string TAXID { get; set; }
        public string FULLADDRESSINCHINESE { get; set; }
        public string ISDCODE { get; set; }
    }

    
    public class CreditDocEntity
    {
        public string FILENAME { get; set; }
        public Int64 DOCID { get; set; }
        public Int64 COMMENTID { get; set; }
    }

   

    public class AlternateEmails
    {
        public Decimal ID { get; set; }
        public string ALTERNATEEMAIL { get; set; }
        public string USERID { get; set; }
        public string TYPE { get; set; }
        public string PARTYNAME { get; set; }
        public Decimal PARTYID { get; set; }
        public DateTime DATECREATED { get; set; }
        public DateTime DATEMODIFIED { get; set; }
    }
    public class AddCreditDocEntity
    {
        public string FILENAME { get; set; }
        public Int64 DOCID { get; set; }
        public Int64 COMMENTID { get; set; }
        public string DocDownloadLink { get; set; }
    }
    public class AdditionalCreditEntity
    {
        public long CREDITAPPID { get; set; }
        public long ADDITIONALCREDITID { get; set; }
        public string INDUSTRYID { get; set; }
        public Int64 NOOFEMP { get; set; }
        public string PARENTCOMPANY { get; set; }
        public decimal ANNUALTURNOVER { get; set; }
        public string DBNUMBER { get; set; }
        public string HFMENTRYCODE { get; set; }
        public string CURRENCYID { get; set; }
        public decimal REQUESTEDCREDITLIMIT { get; set; }
        public string CURRENTPAYTERMSNAME { get; set; }
        public string COMMENTS { get; set; }
        public string CreditStatus { get; set; }
        public string ADDCREDITREFNO { get; set; }
        public long EXISTINGCUSTOMER { get; set; }
        public long USEMYCREIDT { get; set; }
        public IList<AddCreditDocEntity> CreditDoc { get; set; }
        public List<AddCreditCommentEntity> CreditComments { get; set; }
    }

    public class SSPMOBILEVERSIONSENTITY
    {
        public long SSPMOBILEVERSIONSID { get; set; }
        public string OS { get; set; }
        public string APPVERSION { get; set; }
        public string RECOMMENDUPGRADE { get; set; }
        public string FORCEUPGRADE { get; set; }
        public string DESCRIPTION { get; set; }
        public DateTime DATETIME { get; set; }
    }

}
