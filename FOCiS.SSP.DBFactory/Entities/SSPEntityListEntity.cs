﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class SSPEntityListEntity
    {
        public virtual string BANKACCOUNTNUMBER { get; set; }
        public virtual string BANKNAME { get; set; }
        public virtual string BENEFICIARYNAME { get; set; }
        public virtual string BRANCH { get; set; }
        public virtual string COUNTRYCODE { get; set; }
        public virtual Int64 ENTITYKEY { get; set; }
        public virtual decimal FINANCIALENTITYCODE { get; set; }
        public virtual string FINANCIALENTITYNAME { get; set; }
        public virtual string IFSCCODE { get; set; }
        public virtual string SWIFTCODE { get; set; }
        public virtual Int64 COUNTRYID { get; set; }
        public virtual string CURRENCYCODE { get; set; }
    }

    public class SSPOfflineBranchEntity
    {
        public List<SSPEntityListEntity> SSPEntityList { get; set; }
        public List<SSPBranchListEntity> SSPBranchList { get; set; }
        public string PREFERREDCURRENCYCODE { get; set; }
    }
}
