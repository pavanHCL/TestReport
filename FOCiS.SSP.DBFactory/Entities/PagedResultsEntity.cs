﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class PagedResultsEntity<T>
    {
        /// <summary>
        /// The page number this page represents.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// The size of this page.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// The total number of records available.
        /// </summary>
        public int TotalRows { get; set; }

        /// <summary>
        /// The records this page represents.
        /// </summary>
        public IEnumerable<T> Items { get; set; }
    }

    public class UnLocationsCities
    {
        public int id { get; set; }

        public string code { get; set; }

        public string countrycode { get; set; }

        public string subcode { get; set; }

        public string text { get; set; }

        public int countryid { get; set; }

        public string isalternatesearch { get; set; }

        public string  sortorder { get; set; }
  
    }

    public class ResultsEntity<T>
    {
        /// <summary>
        /// The records this page represents.
        /// </summary>
        public IEnumerable<T> Items { get; set; }
    }

    public class MultipleEntity<T>
    {
        /// <summary>
        /// The records this page represents.
        /// </summary>
        public IEnumerable<T> PkgTypes { get; set; }
        public IEnumerable<T> ConTypes { get; set; }
        public IEnumerable<T> LenUOMData { get; set; }
        public IEnumerable<T> WgtUOMData { get; set; }
    }


}
