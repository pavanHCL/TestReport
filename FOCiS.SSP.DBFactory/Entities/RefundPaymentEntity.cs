﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class RefundPaymentEntity
    {
        public string PAYMENTOPTION { get; set; }
        public string PAYMENTTYPE { get; set; }
        public string TRANSACTIONMODE { get; set; }
        //public decimal RECEIPTNETAMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string CREATEDBY { get; set; }
        public string OPJOBNUMBER { get; set; }
        public string ENTITYID { get; set; }
        public string ENTITYDETAILS { get; set; }
        public string REJCOMMENTS { get; set; }
        public Int64 QUOTATIONID { get; set; }
        public decimal MAILRECEIPTNETAMOUNT  { get; set; }
    }   
}
