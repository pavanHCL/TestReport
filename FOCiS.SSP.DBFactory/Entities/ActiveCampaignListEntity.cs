﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class ActiveCampaignListEntity
    {
        public long ID { get; set; }
        public int ListId { get; set; }
        public int ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public string ResultOutPut { get; set; }
        public string ListType { get; set; }
    }

}
