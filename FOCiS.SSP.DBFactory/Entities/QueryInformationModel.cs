﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class QueryInformationModel
    {
        public string QueryName { get; set; }

        public string PreferredContactMethod { get; set; }

        public string EmailId { get; set; }

        public string ContactNumber { get; set; }

        public string CompanyName { get; set; }

        public string QueryType { get; set; }

        public string Discription { get; set; }

        public string Howtoknow { get; set; }
    }
}
