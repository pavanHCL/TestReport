﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class SSPPaymentOptionList
    {
        public virtual Int64 COUNTRYID { get; set; }
        public virtual string ISCREDITCARD { get; set; }
        public virtual string ISBITCOIN { get; set; }
        public virtual string ISBUSINESSCREDIT { get; set; }
        public virtual string ISPAYATBRANCH { get; set; }
        public virtual string ISWIRETRANSFER { get; set; }
        public virtual string ISCASH { get; set; }
        public virtual string ISCHEQUE { get; set; }
        public virtual string ISDD { get; set; }
        public virtual string ISALIPAY { get; set; }        
    }
}
