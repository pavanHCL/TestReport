﻿using System;
using System.Collections.Generic;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class MDMDataEntity<TKey>
    {
        public TKey DataId { get; set; }
        public string DataName { get; set; }
    }

    public class Select2Entity<TKey>
    {
        public TKey id { get; set; }
        public string text { get; set; }
        public int DecimalPoint { get; set; }
    }

    public class SelectEntity<TKey>
    {
        public string CONTACTADDRESS { get; set; }
    }

    public class Select3Entity<TKey>
    {
        public TKey id { get; set; }
        public string text { get; set; }
        public string code { get; set; }
        public string ISZipCode { get; set; }
    }
    public class Select4Entity<TKey>
    {
        public TKey id { get; set; }
        public string text { get; set; }
        public string code { get; set; }
        public string cap { get; set; }
    }

    public class Select7Entity<TKey>
    {
        public TKey id { get; set; }
        public string text { get; set; }
        public string code { get; set; }
        public string cap { get; set; }
        public string MaxVol { get; set; }
        public string MaxWgt { get; set; }
        public string AvgWgt { get; set; }
    }

    public class Status<TKey>
    {
        public TKey id { get; set; }
        public string IsSantioned { get; set; }
    }
    public class ValidationEntity<TKey>
    {
        public TKey MESSAGETOUSER { get; set; }
        public string POPUP { get; set; }
    }
    public class PackageTypesEntity
    {
        public long PACKAGETYPEID { get; set; }
        public long TYPEID { get; set; }
        public string CODE { get; set; }
        public string UNALPHACODE { get; set; }
        public string UNNUMERICCODE { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        //public string ISROAD { get; set; }
        //public string ISAIR { get; set; }
        //public string ISOCEAN { get; set; }
        //public string STATEID { get; set; }
        //public string STATENAME { get; set; }
    }

    public class CountryEntity
    {
        public long COUNTRYID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string BASECURRENCYID { get; set; }
        //public string LOCALCURRENCYCODE { get; set; }
        //public string REGIONID { get; set; }
        //public string ALPHA3CODE { get; set; }
        //public string AGILITYREGION { get; set; }
        //public string GEOGRAPHICALREGION { get; set; }
        //public string INTERNATIONALDIALINGCODE { get; set; }
        //public string ISONUMERICCODE { get; set; }
        //public string AREAID { get; set; }
        //public string MEASUREMENTSYSTEM { get; set; }
        //public string TIMEZONE { get; set; }
        //public string OPENINGHOURS { get; set; }
        //public string CLOSINGHOURS { get; set; }
        //public string SUBDIVISIONTYPEID { get; set; }
        //public string STATEID { get; set; }
        //public string STATENAME { get; set; }
    }

    public class IncoTermsEntity
    {
        public long TERMSOFTRADEID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        //public string MODETYPEID { get; set; }
        //public string STATEID { get; set; }
        //public string STATENAME { get; set; }
    }

    public class UOM
    {
        public long UOMID { get; set; }
        public long TYPEID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        //public string MODEOFTRANSPORT { get; set; }
        //public string STATEID { get; set; }
        //public string STATENAME { get; set; }
    }

    public class StateEntity
    {
        public long SUBDIVISIONID { get; set; }
        public long COUNTRYID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        //public string MODEOFTRANSPORT { get; set; }
        //public string STATEID { get; set; }
        //public string STATENAME { get; set; }
    }

    public class AmazonFCStates
    {
        public decimal SUBDIVISIONID { get; set; }
        public string NAME { get; set; }
    }
    public class AmazonFBAWAREHOUSES
    {
        public decimal ID { get; set; }
        public string NAME { get; set; }
        public string CODE { get; set; }
        public string ZIPCODE { get; set; }
    }
    public class CityEntity
    {
        public string UNLOCATIONID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class DepartmentEntity
    {
        public long ORGANIZATIONALUNITID { get; set; }
        public int CODE { get; set; }
        public string NAME { get; set; }
    }

    public class DefaultCountryEntity
    {
        public long ID { get; set; }
        public string CODE { get; set; }
        public string TEXT { get; set; }

    }

    public class DefaultCurrencyEntity
    {
        public string ID { get; set; }
        public string TEXT { get; set; }
        public int DECIMALPOINT { get; set; }
    }

    public class zipcodeEntity
    {
        public string Zipcode { get; set; }
    }
    public class zipcodeEntityUnloc
    {
        public decimal UNLOCATIONID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string SUBDIVISIONCODE { get; set; }
        public decimal COUNTRYID { get; set; }
        public string COUNTRYCODE { get; set; }
    }
    public class CustomerSupportEntity
    {
        public decimal CATEGORYID { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
        public decimal SUBCATEGORYID { get; set; }

    }
}
