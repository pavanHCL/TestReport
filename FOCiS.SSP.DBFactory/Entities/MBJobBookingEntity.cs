﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class QuoteDetails
    {

        public long QUOTATIONID { get; set; }
        public string QUOTATIONNUMBER { get; set; }
        public decimal OCOUNTRYID { get; set; }
        public decimal DCOUNTRYID { get; set; }
        public long MOVEMENTTYPEID { get; set; }
        public string MOVEMENTTYPENAME { get; set; }
        public string ORIGINPLACECODE { get; set; }
        public string ORIGINPLACENAME { get; set; }
        public string ORIGINPORTCODE { get; set; }
        public string ORIGINPORTNAME { get; set; }
        public double DENSITYRATIO { get; set; }
        public string DESTINATIONPLACENAME { get; set; }
        public string DESTINATIONPORTCODE { get; set; }
        public string DESTINATIONPLACECODE { get; set; }
        public string DESTINATIONPORTNAME { get; set; }
        public string ORIGINZIPCODE { get; set; }
        public string DESTINATIONZIPCODE { get; set; }
        public string ORIGINSUBDIVISIONCODE { get; set; }
        public string DESTINATIONSUBDIVISIONCODE { get; set; }
        public string PRODUCTNAME { get; set; }
        public string PRODUCTTYPENAME { get; set; }
        public long INSUREDVALUE { get; set; }
        public string INSUREDVALUECURRENCYID { get; set; }
        public long ISHAZARDOUSCARGO { get; set; }
        public string HAZARDOUSGOODSTYPE { get; set; }
        public string PREFERREDCURRENCYID { get; set; }
        public string SHIPMENTDESCRIPTION { get; set; }
        public string STATEID { get; set; }
        public long CUSTOMERID { get; set; }
        public decimal CARGOVALUE { get; set; }
        public string CARGOVALUECURRENCYID { get; set; }
        public decimal CHARGEABLEVOLUME { get; set; }
        public decimal CHARGEABLEWEIGHT { get; set; }
        public decimal TOTALCBM { get; set; }
        public decimal TOTALGROSSWEIGHT { get; set; }
        public decimal VOLUMETRICWEIGHT { get; set; }
        public decimal GRANDTOTAL { get; set; }
        public DateTime DATEOFENQUIRY { get; set; }
        public DateTime DATEOFSHIPMENT { get; set; }
        public DateTime DATEOFVALIDITY { get; set; }
        public long ISINSURANCEREQUIRED { get; set; }
    }

    public class QuoteToJobDetails
    {
        public QuoteDetails QuoteDetails { get; set; }
        public IEnumerable<QuotationShipmentEntity> QuoteShipmentItems { get; set; }
        public IEnumerable<JobBookingShipmentEntity> JobShipmentItems { get; set; }
        public SSPJobDetailsEntity JobItems { get; set; }
        public CargoAvabilityEntity CargoAvabilityEntity { get; set; }
        public IEnumerable<Shipper> Shipper { get; set; }
        public IEnumerable<Consignee> Consignee { get; set; }
        public IList<IncoTermsEntity> IncoTerms { get; set; }
        public IList<SSPPartyDetailsEntity> PartyItems { get; set; }
        public IList<COUNTRYINSTRNS> COUNTRYINSTRNS { get; set; }
        //public IList<QuotationChargeEntity> QuotationChargeEntity { get; set; }
    }

    public class BookingSummary
    {
        public QuoteDetails QuoteDetails { get; set; }
        public IEnumerable<QuotationShipmentEntity> QuoteShipmentItems { get; set; }
        public IEnumerable<JobBookingShipmentEntity> JobShipmentItems { get; set; }
        public SSPJobDetailsEntity JobItems { get; set; }
        public CargoAvabilityEntity CargoAvabilityEntity { get; set; }
        public IEnumerable<Shipper> Shipper { get; set; }
        public IEnumerable<Consignee> Consignee { get; set; }
        public IList<IncoTermsEntity> IncoTerms { get; set; }
        public IList<SSPPartyDetailsEntity> PartyItems { get; set; }
        public IList<QuotationChargeEntity> QuotationChargeEntity { get; set; }
        public IList<ReceiptHeaderEntity> ReceiptHeaderDetails { get; set; }
        public IList<QDesChargeEntity> QDesCharges { get; set; }
        public IList<QInterChargeEntity> QInterCharges { get; set; }
        public IList<QOrgChargeEntity> QOrgCharges { get; set; }
        public IList<QAdditionalChargeEntity> QAdditionalCharges { get; set; }
        public IList<QOptionalChargeEntity> QOptionalCharges { get; set; }
        //public IList<DiscountEntity> DiscountCharges { get; set; }
        public IEnumerable<SSPDocumentsEntity_MB> DocumentDetails { get; set; }
        public int DecimalCount { get; set; }

    }
    public class DiscountEntity
    {

        public string DISCOUNTNAME { get; set; }
        public decimal DISCOUNTAMOUNT { get; set; }
    }
    public class Shipper
    {
        public string SHADDRESSTYPE { get; set; }
        public string SHBUILDINGNAME { get; set; }
        public string SHCITYCODE { get; set; }
        public string SHCITYNAME { get; set; }
        public string SHCLIENTID { get; set; }
        public string SHCLIENTNAME { get; set; }
        public string SHCOUNTRYCODE { get; set; }
        public string SHCOUNTRYID { get; set; }
        public string SHCOUNTRYNAME { get; set; }
        public string SHFULLADDRESS { get; set; }
        public string SHHOUSENO { get; set; }
        public long SHISDEFAULTADDRESS { get; set; }
        public long SHJOBNUMBER { get; set; }
        public long SHPARTYDETAILSID { get; set; }
        public string SHPARTYTYPE { get; set; }
        public string SHPINCODE { get; set; }
        public string SHSTATECODE { get; set; }
        public string SHSTATENAME { get; set; }
        public string SHSTREET1 { get; set; }
        public string SHSTREET2 { get; set; }
        public string SHFIRSTNAME { get; set; }
        public string SHLASTNAME { get; set; }
        public string SHPHONE { get; set; }
        public string SHEMAILID { get; set; }
        public string SHSALUTATION { get; set; }
        public int SHISSAVEDORNEWADDRESS { get; set; }
    }
    public class Consignee
    {
        public string CONADDRESSTYPE { get; set; }
        public string CONBUILDINGNAME { get; set; }
        public string CONCITYCODE { get; set; }
        public string CONCITYNAME { get; set; }
        public string CONCLIENTID { get; set; }
        public string CONCLIENTNAME { get; set; }
        public string CONCOUNTRYCODE { get; set; }
        public string CONCOUNTRYNAME { get; set; }
        public string CONCOUNTRYID { get; set; }
        public string CONFULLADDRESS { get; set; }
        public string CONHOUSENO { get; set; }
        public long CONISDEFAULTADDRESS { get; set; }
        public long CONJOBNUMBER { get; set; }
        public string CONLATITUDE { get; set; }
        public string CONLONGITUDE { get; set; }
        public long CONPARTYDETAILSID { get; set; }
        public string CONPARTYTYPE { get; set; }
        public string CONPINCODE { get; set; }
        public string CONSTATECODE { get; set; }
        public string CONSTATENAME { get; set; }
        public string CONSTREET1 { get; set; }
        public string CONSTREET2 { get; set; }
        public string CONFIRSTNAME { get; set; }
        public string CONLASTNAME { get; set; }
        public string CONPHONE { get; set; }
        public string CONEMAILID { get; set; }
        public string CONSALUTATION { get; set; }
        public int CONISSAVEDORNEWADDRESS { get; set; }
    }

    public class COUNTRYINSTRNS
    {
        public string DIRECTION { get; set; }
        public string NAME { get; set; }
        public string EXPORTINSTRUCTION { get; set; }
        public string INSTRNTYPE { get; set; }
        public Int64 COUNTRYID { get; set; }

    }
}
