﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
   public class PaymentChargesEntity
    {
        public long JOBNUMBER { get; set; }
        public string CHARGENAME { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal TAXAMOUNT { get; set; }
        public decimal NETAMOUNT { get; set; }
        public long RouteTypeId { get; set; }
        public string ChargeApplicability { get; set; }
        public int ISCHARGEINCLUDED { get; set; }
        public string CHARGESOURCE { get; set; }
        public string CHARGESTATUS { get; set; }
    }
}
