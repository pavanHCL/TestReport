﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class SSPBranchListEntity
    {  
        public virtual decimal BRANCHKEY { get; set; }
        public virtual string COUNTRYCODE { get; set; }
        public virtual decimal FINANCIALENTITYCODE { get; set; }
        public virtual string FINANCIALENTITYNAME { get; set; }
        public virtual string NWCCODE { get; set; }
        public virtual string NWCFUNCTION { get; set; }
        public virtual string SITEADDRESS { get; set; }
        public virtual string SITECITY { get; set; }
        public virtual string SITENAME { get; set; }
        public virtual string USC { get; set; }
        public virtual string ISCASH { get; set; }
        public virtual string ISCHEQUE { get; set; }
        public virtual string ISDD { get; set; }
        public virtual string CURRENCYCODE { get; set; }
        public virtual string CITYBRANCH { get; set; }
    }
}
