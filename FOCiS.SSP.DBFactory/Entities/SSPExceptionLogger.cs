﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    public class SSPExceptionLogger
    {
        public decimal ERRID { get; set; }
        public string EXCEPTIONMESSAGE { get; set; }
        public string CONTROLLERNAME { get; set; }
        public string ACTIONNAME { get; set; }
        public string EXCEPTIONSTACKTRACE { get; set; }
        public string USERID { get; set; }
        public DateTime DATELOGGED { get; set; }
        public string ENVIRONMENTNAME { get; set; }
    }
}
