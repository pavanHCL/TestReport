﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Entities
{
    
    public class SupportEntity
    {
        public string Email { get; set; }
        public string TabStatus { get; set; }
        public string SearchString { get; set; }
        public int PageNo { get; set; }
        public string USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANYNAME { get; set; }
        public string MOBILENUMBER { get; set; }
        public decimal PREVIEW { get; set; }
        public decimal Submitted { get; set; }
        public decimal Closed { get; set; }
        public decimal All { get; set; }
        public string REQID { get; set; }

        public IList<Requests> Requests { get; set; }
        public IList<Chat> Chat { get; set; }
    }


    public class Requests
    {
        public Decimal REQID { get; set; }
        public string CATEGORY { get; set; }
        public string MODULE { get; set; }
        public string QUERY { get; set; }
        public string SEQNO { get; set; }
        public string MODULEID { get; set; }
        public string DESCRIPTION { get; set; }
        public string REQSTATUS { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime DATECREATED { get; set; }
        public decimal ChatCount { get; set; }
    }
    
    public class Chat
    {
        public Decimal REQID { get; set; }
        public string CATEGORY { get; set; }
        public string MODULE { get; set; }
        public string QUERY { get; set; }
        public string SEQNO { get; set; }
        public string MODULEID { get; set; }
        public string DESCRIPTION { get; set; }
        public string REQSTATUS { get; set; }
        public DateTime DATECREATED { get; set; }
        public string CREATEDBY { get; set; }
        public string MESSAGE { get; set; }
        public byte[] FILE { get; set; }
        public string FILENAME { get; set; }
        public Decimal DOCREQID { get; set; }
        public string UPLOADEDFROM { get; set; }
    }
}
