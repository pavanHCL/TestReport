﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Core
{
    public interface IDBFactory<TKey, TEntity>
    {
        TEntity GetById(TKey id);
        TKey Save(TEntity entity);
        void Update(TEntity entity);
        void Delete(TKey key);
    }
}
