﻿using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Factory
{
    public interface ITrackingDBFactory
    {
        ListTrackingEntity ReturnTrackEntityList(
            string UserID, string ConsignmentId, int Jobnumber,
            string status, int PageNo, string SearchString
        );

        List<TrackStatusCount> GetTrackStatusCount(string p, string SearchString);
        List<PartyDetails> GetPartyDetails(string UserId, long Jobnumer);
        List<ShpimentEventComments> GetShipmentEventComments(string p, int Jobnumber, string ConsignmentId);
        List<SSPDocumentsEntity> GetUploadedDocuments(string p, int JobNumber, string ConsignmentId);
        void InsertShareComments(string EventName, string Comments, string Createdby, Int64 jobnumber, int falag);
        ListTrackingEntity ShipmentTracking(string p, string ConsignmentId, string BookingID);
    }
}
