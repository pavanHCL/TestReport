﻿using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Factory
{
    public interface IJobBookingDbFactory
    {
        DataSet CheckPaymentStatus(int jobnumber);
        PayementMenthods GetPaymentDetailsByUserCountry(Int32 JOBNUMBER, string userid);
        CouponCode GetAgilityDiscountPrice(string CouponCode, string JobNumber, string UserID);
        List<SSPEntityListEntity> GetWireTransferDetails(string Userid);
        DataTable GetJobinfoBasedonBookingID(int p);
        SSPEntityListEntity GetEntitydetailsBasedonEntity(int SSPPAYMENTSETUPID);
        List<SSPBranchListEntity> GetPayAtBrachDetails(string Userid);
        string GetUserIdByOpJobNumber(string opjobnumber);
        void SaveOfflinePaymentTransactionDetails(PaymentTransactionDetailsEntity entity);
        void SaveOfflinePaymentTransactionDetails(PaymentTransactionDetailsEntity entity, string DiscountCode, decimal DiscountAmount);
        string SaveOfflinePaymentBookingSummaryDoc(byte[] paymentpdfdata, int jobNumber, int quotation, string userName, string OPjobnumber, string GUID, string EnvironmentName, string ChargeSetId = "");
        int CheckIsMandatoryDocsUploaded(string jobNumber);
        PaymentEntity GetPaymentDetailsByJobNumber(int id, string userid);
        PaymentEntity GetPaymentDetailsByJobNumber(int id, string userid, string couponCode, double RequestedAmount);
        JobBookingEntity GetQuotedJobDetails(int id, long JobNumber);
        void SaveStripeTransactionDetails(string jobnumber, string status, string amount, string currency, string payoption
            , string token, string chargeid, string tranid, string pcurrency, string ptotal, string pnet, string sfee, string pstatus, string exchangerate, string paccount
            );
        void SavePaymentTransactionDetails(PaymentTransactionDetailsEntity entity, string CouponCode, decimal DiscountAmount, double PayAmount, string reason = "", string paidcurrency = "");
        string GetEmailList(string roles, string bookingId = "", string partyType = "", string countryId = "", string UserID = "");
        DataSet GetDocuments(int JobNumber);
        void SaveBusinessCreditTransactionDetails(PaymentTransactionDetailsEntity mUIModel, string p, string DiscountCode, decimal DiscountAmount);
        List<OnlinePaymentTransactionEntity> GetPendingPayouts();
        string UpdatePayoutStatus(List<OnlinePaymentTransactionEntity> data);
        IList<SSPDocumentsEntity> GetDocumentDetailsbyDocumetnId(string DOCID, string Userid);
        dynamic GetCurrencyAndOrder(string amount, string from, string to);
        OnlinePaymentTransactionEntity GetHsbcJobNumber(string id);
        void UpdateHsbcRefundStatus(string id);
        DataTable GetStateConfigurationFlag(string CountryId);
        dynamic GetCurrencyRounded(string amount, string cur);
    }
}
