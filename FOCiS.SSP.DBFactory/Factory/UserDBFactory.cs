﻿using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using FOCiS.SSP.DBFactory.Factory;

namespace FOCiS.SSP.DBFactory
{
    public class UserDBFactory : DBFactoryCore,IDBFactory<long, UserEntity>, IDisposable,IUserDBFactory
    {
        public UserEntity GetUserDetails(string UserId)
        {
            UserEntity mEntity = new UserEntity();
            ReadText(DBConnectionMode.SSP, @"SELECT USERID, USERTYPE, EMAILID, PASSWORD, ISTEMPPASSWORD, TOKEN,TOKENEXPIRY,SYSDATE AS ServerDate,
                                             FIRSTNAME, LASTNAME, COMPANYNAME, ISDCODE,TO_CHAR(MOBILENUMBER) Mobilenumber, ISTERMSAGREED, COUNTRYID, ACCOUNTSTATUS, 
                                             LASTLOGONTIME, TIMEZONEID,USERCULTUREID, DATECREATED, DATEMODIFIED, ISVATREGISTERED, VATREGNUMBER,
                                             SALUTATION, JOBTITLE, COMPANYLINK, COUNTRYCODE, COUNTRYNAME, IMGFILENAME,IMGCONTENT, 
                                            JOBTITLEID, DEPTID, DEPTCODE, DEPTNAME, WORKPHONE,FAILEDPASSWORDATTEMPT,InstantPASSWORD,
                                            (select DATECREATED from USERSINSTANTPASSWORD where LOWER(userid)=:USERID and ACCOUNTSTATUS=1) as InstTime
                                            FROM USERS WHERE LOWER(USERID) = :USERID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<UserEntity>(r);
            });
            return mEntity;
        }

        public UserEntity GetUserLoginInfo(string UserId)
        {
            UserEntity mEntity = new UserEntity();
            ReadText(DBConnectionMode.SSP, @"SELECT USERID, PASSWORD, SYSDATE AS ServerDate,ACCOUNTSTATUS,FAILEDPASSWORDATTEMPT,InstantPASSWORD,
                                            (select DATECREATED from USERSINSTANTPASSWORD where LOWER(userid)=:USERID and ACCOUNTSTATUS=1) as InstTime
                                            FROM USERS WHERE LOWER(USERID) = :USERID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<UserEntity>(r);
            });
            return mEntity;
        }


        public UserEntity GetUserFirstAndLastName(string UserId)
        {
            UserEntity mEntity = new UserEntity();
            ReadText(DBConnectionMode.SSP, @"SELECT USERID, FIRSTNAME, LASTNAME,COUNTRYNAME,CRM AS CRMEmail
                                            FROM USERS WHERE LOWER(USERID) = :USERID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<UserEntity>(r);
            });
            return mEntity;
        }
        public QuotationEntity GetQuoteDetails(int Id)
        {
            QuotationEntity mEntity = new QuotationEntity();
            ReadText(DBConnectionMode.SSP, "SELECT GUESTNAME,GUESTLASTNAME,GUESTEMAIL,GUESTCOMPANY,NOTIFICATIONSUBSCRIPTION,USEOFQUOTE,SHIPMENTPROCESS,GUESTCOUNTRYCODE FROM QMQUOTATION WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "QUOTATIONID", Value = Id } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<QuotationEntity>(r);
            });
            return mEntity;
        }

        public UserProfileEntity GetUserProfileDetails(string UserId, int tab)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("V_USERID", UserId); mCommand.Parameters.Add("V_TAB", tab);
                        mCommand.Parameters.Add("PROF_INFO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_CREDITDOC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_CREDITCOMMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "SSP_GETUSERPROFILEDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        DataTable dt = new DataTable();
                        UserProfileEntity mEntity = new UserProfileEntity();
                        if (tab == 1)
                        {
                            OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["PROF_INFO"].Value).GetDataReader();
                            dt.Load(reader1);
                            mEntity = MyClass.ConvertToEntity<DBFactory.Entities.UserProfileEntity>(dt);
                            reader1.Close();
                        }
                        else if (tab == 2)
                        {
                            dt = new DataTable();
                            OracleDataReader reader3 = ((OracleRefCursor)mCommand.Parameters["PROF_INFO"].Value).GetDataReader();
                            dt.Load(reader3);
                            mEntity = MyClass.ConvertToEntity<DBFactory.Entities.UserProfileEntity>(dt);
                            reader3.Close();

                            DataTable dtCreditDoc = new DataTable();
                            OracleDataReader creditreader = ((OracleRefCursor)mCommand.Parameters["CUR_CREDITDOC"].Value).GetDataReader();
                            dtCreditDoc.Load(creditreader);
                            List<DBFactory.Entities.CreditDocEntity> creditdocEntity = MyClass.ConvertToList<DBFactory.Entities.CreditDocEntity>(dtCreditDoc);
                            mEntity.CreditDoc = creditdocEntity;
                            creditreader.Close();
                            DataTable dtCreditComments = new DataTable();
                            OracleDataReader creditcommentreader = ((OracleRefCursor)mCommand.Parameters["CUR_CREDITCOMMENTS"].Value).GetDataReader();
                            dtCreditComments.Load(creditcommentreader);
                            List<DBFactory.Entities.CreditCommentEntity> creditcommentdocEntity = MyClass.ConvertToList<DBFactory.Entities.CreditCommentEntity>(dtCreditComments);
                            foreach (var item in creditcommentdocEntity)
                            {
                                item.CreditDoc = new List<CreditDocEntity>();
                                foreach (var doc in creditdocEntity)
                                {
                                    if (doc.COMMENTID == item.COMMENTID)
                                    {
                                        item.CreditDoc.Add(doc);
                                    }
                                }

                            }
                            mEntity.CreditComments = creditcommentdocEntity;
                            creditcommentreader.Close();
                        }

                        mConnection.Close();

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public AdditionalCreditEntity GetAddCreditLimit(string UserId, int addCreditId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("V_USERID", UserId);
                        mCommand.Parameters.Add("V_CREDITAPPID", addCreditId);
                        mCommand.Parameters.Add("PROF_INFO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_CREDITDOC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_CREDITCOMMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "SSP_GETUSERADDITIONALCREDIT";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        DataTable dt = new DataTable();

                        AdditionalCreditEntity mEntity = new AdditionalCreditEntity();
                        dt = new DataTable();
                        OracleDataReader addcreditreader = ((OracleRefCursor)mCommand.Parameters["PROF_INFO"].Value).GetDataReader();
                        dt.Load(addcreditreader);
                        mEntity = MyClass.ConvertToEntity<DBFactory.Entities.AdditionalCreditEntity>(dt);
                        addcreditreader.Close();

                        DataTable dtCreditDoc = new DataTable();
                        OracleDataReader creditreader = ((OracleRefCursor)mCommand.Parameters["CUR_CREDITDOC"].Value).GetDataReader();
                        dtCreditDoc.Load(creditreader);
                        List<DBFactory.Entities.AddCreditDocEntity> creditdocEntity = MyClass.ConvertToList<DBFactory.Entities.AddCreditDocEntity>(dtCreditDoc);
                        mEntity.CreditDoc = creditdocEntity;
                        creditreader.Close();
                        DataTable dtCreditComments = new DataTable();
                        OracleDataReader creditcommentreader = ((OracleRefCursor)mCommand.Parameters["CUR_CREDITCOMMENTS"].Value).GetDataReader();
                        dtCreditComments.Load(creditcommentreader);
                        List<DBFactory.Entities.AddCreditCommentEntity> creditcommentdocEntity = MyClass.ConvertToList<DBFactory.Entities.AddCreditCommentEntity>(dtCreditComments);
                        foreach (var item in creditcommentdocEntity)
                        {
                            item.CreditDoc = new List<AddCreditDocEntity>();
                            foreach (var doc in creditdocEntity)
                            {
                                if (doc.COMMENTID == item.COMMENTID)
                                {
                                    item.CreditDoc.Add(doc);
                                }
                            }

                        }
                        mEntity.CreditComments = creditcommentdocEntity;
                        creditcommentreader.Close();
                        mConnection.Close();

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        //        public UserProfileEntity GetUserProfileDetails(string UserId)
        //        {
        //            //INFO: EXTUSERFLAG :1=DISABLE; EXTUSERFLAG :2=ENABLE -- Anil G

        //            UserProfileEntity mEntity = new UserProfileEntity();
        //            ReadText(DBConnectionMode.SSP, @"SELECT USERID, USERTYPE, EMAILID, PASSWORD, ISTEMPPASSWORD, TOKEN,TOKENEXPIRY, FIRSTNAME, LASTNAME, COMPANYNAME, 
        //            ISDCODE,TO_CHAR(MOBILENUMBER) Mobilenumber, ISTERMSAGREED, COUNTRYID, ACCOUNTSTATUS, LASTLOGONTIME, TIMEZONEID,USERCULTUREID, DATECREATED, 
        //            DATEMODIFIED, ISVATREGISTERED, VATREGNUMBER,SALUTATION, JOBTITLE, COMPANYLINK, COUNTRYCODE, COUNTRYNAME, IMGFILENAME,JOBTITLEID,
        //            DEPTID, DEPTCODE, DEPTNAME, WORKPHONE,OTHERJOBTITLE
        //            FROM USERS U WHERE USERID = :USERID",
        //                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
        //            , delegate(IDataReader r)
        //            {
        //                mEntity = DataReaderMapToObject<UserProfileEntity>(r);
        //            });

        //            //User Address Details List
        //            List<UMUserAddressDetailsEntity> mAddrItems = new List<UMUserAddressDetailsEntity>();
        //            ReadText(DBConnectionMode.SSP, "SELECT * FROM UMUSERADDRESSDETAILS WHERE USERID = :USERID",
        //                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
        //            , delegate(IDataReader r)
        //            {
        //                mAddrItems.Add(DataReaderMapToObject<UMUserAddressDetailsEntity>(r));
        //            });
        //            mEntity.UserAddress = mAddrItems;

        //            List<CreditEntity> mCreditItems = new List<CreditEntity>();
        //            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPCREDITAPPLICATIONS WHERE USERID = :USERID",
        //                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
        //            , delegate(IDataReader r)
        //            {
        //                mCreditItems.Add(DataReaderMapToObject<CreditEntity>(r));
        //            });
        //            mEntity.CreditItems = mCreditItems;

        //            return mEntity;
        //        }

        public UserEntity GetById(long id)
        {
            throw new NotImplementedException();
        }

        public long Save(UserEntity entity)
        {
            string PREFERREDCURRENCY = ""; string PREFERREDCURRENCYCODE = "";
            OracleConnection mFocisConnection;
            OracleTransaction mfocisOracleTransaction = null;

            using (mFocisConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mFocisConnection.State == ConnectionState.Closed) mFocisConnection.Open();
                mfocisOracleTransaction = mFocisConnection.BeginTransaction();
                using (OracleCommand mCommand = mFocisConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mfocisOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        //mCommand.CommandText = "SELECT COUNTRYID,NAME FROM SMDM_COUNTRY WHERE UPPER(CODE)=UPPER(:CODE)";
                        //DataTable DTUsers = ReturnDatatable(DBConnectionMode.SSP, mCommand.CommandText,
                        //    new OracleParameter[] { new OracleParameter { ParameterName = "CODE", Value = entity.COUNTRYCODE } });

                        ////Check if data IsExist -- Added By Anil-G
                        //if (DTUsers.Rows.Count > 0)
                        //{
                        //    entity.COUNTRYNAME = DTUsers.Rows[0]["NAME"].ToString();
                        //    entity.COUNTRYID = Convert.ToInt64(DTUsers.Rows[0]["COUNTRYID"].ToString());
                        //}

                        //mCommand.Parameters.Clear();
                        mCommand.CommandText = " SELECT CURRENCYNAME,CURRENCYCODE FROM FOCIS.CURRENCIES WHERE CURRENCYID IN (SELECT BASECURRENCYID FROM FOCIS.SMDM_COUNTRY WHERE countryid=" + entity.COUNTRYID + ")";
                        DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, mCommand.CommandText, null);
                        if (dt.Rows.Count > 0)
                        {
                            PREFERREDCURRENCY = dt.Rows[0]["CURRENCYNAME"].ToString();
                            PREFERREDCURRENCYCODE = dt.Rows[0]["CURRENCYCODE"].ToString();
                        }
                        else
                        {
                            PREFERREDCURRENCY = "US Dollar";
                            PREFERREDCURRENCYCODE = "USD";
                        }
                        mFocisConnection.Close();
                    }
                    catch (Exception)
                    {
                        mfocisOracleTransaction.Rollback();
                        throw;
                    }
                }
            }

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        if (string.IsNullOrEmpty(entity.COUNTRYCODE))
                        {
                            entity.COUNTRYCODE = "";
                        }
                        mCommand.CommandText = @"INSERT INTO USERS (USERID,USERTYPE,EMAILID,PASSWORD,DATECREATED,FIRSTNAME,LASTNAME,COMPANYNAME,
                        ISDCODE,MOBILENUMBER,ISTERMSAGREED,COUNTRYID,ACCOUNTSTATUS,TIMEZONEID,USERCULTUREID,TOKEN,TOKENEXPIRY,COUNTRYCODE,COUNTRYNAME,
                        PREFERREDCURRENCY,PREFERREDCURRENCYCODE,NOTIFICATIONSUBSCRIPTION,SHIPMENTPROCESS,SHIPPINGEXPERIENCE) VALUES 
                        (:USERID,:USERTYPE,:EMAILID,:PASSWORD,:DATECREATED,:FIRSTNAME,:LASTNAME,:COMPANYNAME,:ISDCODE,:MOBILENUMBER,:ISTERMSAGREED,
                        :COUNTRYID,:ACCOUNTSTATUS,:TIMEZONEID,:USERCULTUREID,:TOKEN,:TOKENEXPIRY,:COUNTRYCODE,:COUNTRYNAME,:PREFERREDCURRENCY,
                        :PREFERREDCURRENCYCODE,:NOTIFICATIONSUBSCRIPTION,:SHIPMENTPROCESS,:SHIPPINGEXPERIENCE)";

                        mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                        mCommand.Parameters.Add("USERTYPE", entity.USERTYPE);
                        mCommand.Parameters.Add("EMAILID", entity.EMAILID);
                        mCommand.Parameters.Add("PASSWORD", entity.PASSWORD);
                        mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("FIRSTNAME", entity.FIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.LASTNAME);
                        mCommand.Parameters.Add("COMPANYNAME", entity.COMPANYNAME);
                        mCommand.Parameters.Add("ISDCODE", entity.ISDCODE);
                        mCommand.Parameters.Add("MOBILENUMBER", entity.MOBILENUMBER);
                        mCommand.Parameters.Add("ISTERMSAGREED", entity.ISTERMSAGREED);
                        mCommand.Parameters.Add("COUNTRYID", entity.COUNTRYID);
                        mCommand.Parameters.Add("ACCOUNTSTATUS", entity.ACCOUNTSTATUS);
                        mCommand.Parameters.Add("TIMEZONEID", entity.TIMEZONEID);
                        mCommand.Parameters.Add("USERCULTUREID", entity.USERCULTUREID);
                        mCommand.Parameters.Add("TOKEN", entity.TOKEN);
                        mCommand.Parameters.Add("TOKENEXPIRY", entity.TOKENEXPIRY);
                        mCommand.Parameters.Add("COUNTRYCODE", entity.COUNTRYCODE.ToUpper());
                        mCommand.Parameters.Add("COUNTRYNAME", entity.COUNTRYNAME);
                        mCommand.Parameters.Add("PREFERREDCURRENCY", PREFERREDCURRENCY);
                        mCommand.Parameters.Add("PREFERREDCURRENCYCODE", PREFERREDCURRENCYCODE);
                        mCommand.Parameters.Add("NOTIFICATIONSUBSCRIPTION", entity.NOTIFICATIONSUBSCRIPTION.Equals(true) ? 1 : 0);
                        mCommand.Parameters.Add("SHIPMENTPROCESS", entity.SHIPMENTPROCESS);
                        mCommand.Parameters.Add("SHIPPINGEXPERIENCE", entity.SHIPPINGEXPERIENCE);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
                //SSP NOTIFICATIONS ADDED BY SIVA
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT SSP_NOTIFICATIONS_SEQ.NEXTVAL FROM DUAL";
                        entity.NOTIFICATIONID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = "INSERT INTO SSPNOTIFICATIONS (NOTIFICATIONID,NOTIFICATIONTYPEID,USERID,DATECREATED,DATEREQUESTED,NOTIFICATIONCODE) VALUES (:NOTIFICATIONID,:NOTIFICATIONTYPEID,:USERID,:DATECREATED,:DATEREQUESTED,:NOTIFICATIONCODE)";
                        mCommand.Parameters.Add("NOTIFICATIONID", Convert.ToInt32(entity.NOTIFICATIONID));
                        mCommand.Parameters.Add("NOTIFICATIONTYPEID", 5111);
                        mCommand.Parameters.Add("USERID", entity.USERID.ToUpper());
                        mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("DATEREQUESTED", DateTime.Now);
                        mCommand.Parameters.Add("NOTIFICATIONCODE", "Profile Incomplete");
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }

            }

            //--------------------Sales Force---------------

            using (mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.Parameters.Clear();
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    mCommand.CommandText = "USP_SSP_CEPREQUOTEPROFILE";
                    try
                    {
                        mCommand.Parameters.Add("PRM_GUESTNAME", Convert.ToString(entity.FIRSTNAME));
                        mCommand.Parameters.Add("PRM_GUESTCOUNTRYCODE", entity.COUNTRYCODE);
                        mCommand.Parameters.Add("PRM_GUESTCOUNTRYNAME", entity.COUNTRYNAME);
                        mCommand.Parameters.Add("PRM_GUESTCOMPANY", entity.COMPANYNAME);
                        mCommand.Parameters.Add("PRM_GUESTEMAIL", entity.USERID);
                        mCommand.Parameters.Add("PRM_TELEPHONENUMBER", Convert.ToInt64(entity.MOBILENUMBER));

                        mCommand.Parameters.Add("OP_RESULTSET", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            //-----------------------------------------------
            return 1;
        }

        public void Update(UserEntity entity)
        {
            throw new NotImplementedException();
            //OracleConnection mConnection;
            //OracleTransaction mOracleTransaction = null;
            //using (mConnection = new OracleConnection(SSPConnectionString))
            //{
            //    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
            //    mOracleTransaction = mConnection.BeginTransaction();
            //    using (OracleCommand mCommand = mConnection.CreateCommand())
            //    {
            //        mCommand.BindByName = true;
            //        mCommand.Transaction = mOracleTransaction;
            //        mCommand.CommandType = CommandType.Text;
            //        try
            //        {
            //            mCommand.CommandText = "UPDATE USERS SET USERTYPE=:USERTYPE,PASSWORD:=PASSWORD,FIRSTNAME=:FIRSTNAME,LASTNAME=:LASTNAME,COMPANYNAME=:COMPANYNAME,ISDCODE=:ISDCODE,MOBILENUMBER=:MOBILENUMBER,ISTERMSAGREED=:ISTERMSAGREED,COUNTRYID=:COUNTRYID,ACCOUNTSTATUS=:ACCOUNTSTATUS,TIMEZONEID=:TIMEZONEID,USERCULTUREID=:USERCULTUREID,DATEMODIFIED=:DATEMODIFIED";
            //            mCommand.Parameters.Add("USERTYPE", entity.USERTYPE);
            //            mCommand.Parameters.Add("PASSWORD", entity.PASSWORD);
            //            mCommand.Parameters.Add("FIRSTNAME", entity.FIRSTNAME);
            //            mCommand.Parameters.Add("LASTNAME", entity.LASTNAME);
            //            mCommand.Parameters.Add("COMPANYNAME", entity.COMPANYNAME);
            //            mCommand.Parameters.Add("ISDCODE", entity.ISDCODE);
            //            mCommand.Parameters.Add("MOBILENUMBER", entity.MOBILENUMBER);
            //            mCommand.Parameters.Add("ISTERMSAGREED", entity.ISTERMSAGREED);
            //            mCommand.Parameters.Add("COUNTRYID", entity.COUNTRYID);
            //            mCommand.Parameters.Add("ACCOUNTSTATUS", entity.ACCOUNTSTATUS);
            //            mCommand.Parameters.Add("TIMEZONEID", entity.TIMEZONEID);
            //            mCommand.Parameters.Add("USERCULTUREID", entity.USERCULTUREID);
            //            mCommand.Parameters.Add("DATEMODIFIED", DateTime.Now);
            //            mCommand.ExecuteNonQuery();
            //            mOracleTransaction.Commit();
            //        }
            //        catch (Exception)
            //        {
            //            mOracleTransaction.Rollback();
            //            throw;
            //        }
            //    }
            //}
        }

        public void ActivateUserAccountStatus(string UserId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "UPDATE USERS SET ACCOUNTSTATUS=1 WHERE UPPER(USERID)=:USERID";
                        mCommand.Parameters.Add("USERID", UserId.ToUpperInvariant());
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void Delete(long key)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void UpdateUserProfile(UserProfileEntity entity, string hdnvalue, string notifymsg, string ChangeState)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        if (hdnvalue != "1")
                        {

                            mCommand.CommandText = @"UPDATE USERS SET COMPANYLINK=:COMPANYLINK,ISDCODE=:ISDCODE, MOBILENUMBER=:MOBILENUMBER,
                                                    FIRSTNAME=:FIRSTNAME,LASTNAME=:LASTNAME,COUNTRYID=:COUNTRYID,COUNTRYCODE=:COUNTRYCODE,
                                                    COUNTRYNAME=:COUNTRYNAME,ISVATREGISTERED=:ISVATREGISTERED,VATREGNUMBER=:VATREGNUMBER,DATEMODIFIED=:DATEMODIFIED,
                                                    OTHERJOBTITLE=:OTHERJOBTITLE,LENGTHUNIT=:LENGTHUNIT,WEIGHTUNIT=:WEIGHTUNIT,PREFERREDCURRENCY=:PREFERREDCURRENCY,
                                                    PREFERREDCURRENCYCODE=:PREFERREDCURRENCYCODE,
                                                    SALUTATION=:SALUTATION,NOTIFICATIONSUBSCRIPTION=:NOTIFICATIONSUBSCRIPTION,Industry=:Industry,
                                                    OtherIndustry=:OtherIndustry,ISFAPIAO=:ISFAPIAO,SHIPMENTPROCESS=:SHIPMENTPROCESS,SHIPPINGEXPERIENCE=:SHIPPINGEXPERIENCE,
                                                    PERSONALASSISTANCE=:PERSONALASSISTANCE,EXISTINGFREIGHTFORWARDER=:EXISTINGFREIGHTFORWARDER WHERE LOWER(USERID)=:USERID";
                            mCommand.Parameters.Add("COMPANYLINK", entity.COMPANYLINK);
                            mCommand.Parameters.Add("ISDCODE", entity.ISDCODE);
                            mCommand.Parameters.Add("FIRSTNAME", entity.FIRSTNAME);
                            mCommand.Parameters.Add("LASTNAME", entity.LASTNAME);
                            mCommand.Parameters.Add("MOBILENUMBER", entity.MOBILENUMBER);
                            mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                            mCommand.Parameters.Add("COUNTRYID", entity.COUNTRYID);
                            mCommand.Parameters.Add("COUNTRYCODE", entity.COUNTRYCODE);
                            mCommand.Parameters.Add("COUNTRYNAME", entity.COUNTRYNAME);
                            mCommand.Parameters.Add("DATEMODIFIED", DateTime.Now);
                            mCommand.Parameters.Add("ISVATREGISTERED", entity.ISVATREGISTERED);
                            mCommand.Parameters.Add("VATREGNUMBER", entity.VATREGNUMBER);
                            mCommand.Parameters.Add("OTHERJOBTITLE", entity.OTHERJOBTITLE);
                            mCommand.Parameters.Add("SALUTATION", entity.SALUTATION);
                            mCommand.Parameters.Add("LENGTHUNIT", entity.LENGTHUNIT);
                            mCommand.Parameters.Add("WEIGHTUNIT", entity.WEIGHTUNIT);
                            mCommand.Parameters.Add("PREFERREDCURRENCY", entity.PREFERREDCURRENCY);
                            mCommand.Parameters.Add("PREFERREDCURRENCYCODE", entity.PREFERREDCURRENCYCODE);
                            mCommand.Parameters.Add("NOTIFICATIONSUBSCRIPTION", entity.NOTIFICATIONSUBSCRIPTION);
                            mCommand.Parameters.Add("Industry", entity.Industry);
                            mCommand.Parameters.Add("OtherIndustry", entity.OtherIndustry);
                            mCommand.Parameters.Add("ISFAPIAO", entity.ISFAPIAO);
                            mCommand.Parameters.Add("SHIPMENTPROCESS", entity.SHIPMENTPROCESS);
                            mCommand.Parameters.Add("SHIPPINGEXPERIENCE", entity.SHIPPINGEXPERIENCE);
                            mCommand.Parameters.Add("PERSONALASSISTANCE", entity.PERSONALASSISTANCE);
                            mCommand.Parameters.Add("EXISTINGFREIGHTFORWARDER", entity.EXISTINGFREIGHTFORWARDER);
                            mCommand.ExecuteNonQuery();

                            if (entity.UAUSERADTLSID == 0)
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("V_USERADTLSID", null);
                                mCommand.Parameters.Add("V_USERID", entity.USERID.ToLower());
                                mCommand.Parameters.Add("V_ADDRESSTYPE", entity.UAADDRESSTYPE);
                                mCommand.Parameters.Add("V_HOUSENO", entity.UAHOUSENO);
                                mCommand.Parameters.Add("V_BUILDINGNAME", entity.UABUILDINGNAME);
                                mCommand.Parameters.Add("V_ADDRESSLINE1", entity.UAADDRESSLINE1);
                                mCommand.Parameters.Add("V_ADDRESSLINE2", entity.UAADDRESSLINE2);
                                mCommand.Parameters.Add("V_COUNTRYID", entity.UACOUNTRYID);
                                mCommand.Parameters.Add("V_COUNTRYCODE", entity.UACOUNTRYCODE);
                                mCommand.Parameters.Add("V_COUNTRYNAME", entity.UACOUNTRYNAME);
                                mCommand.Parameters.Add("V_STATEID", entity.UASTATEID);
                                mCommand.Parameters.Add("V_STATECODE", entity.UASTATECODE);
                                mCommand.Parameters.Add("V_STATENAME", entity.UASTATENAME);
                                mCommand.Parameters.Add("V_CITYID", entity.UACITYID);
                                mCommand.Parameters.Add("V_CITYCODE", entity.UACITYCODE);
                                mCommand.Parameters.Add("V_CITYNAME", entity.UACITYNAME);
                                mCommand.Parameters.Add("V_POSTCODE", entity.UAPOSTCODE);
                                mCommand.Parameters.Add("V_FULLADDRESS", entity.UAFULLADDRESS);
                                mCommand.Parameters.Add("V_ISDEFAULT", entity.UAISDEFAULT);
                                mCommand.CommandText = "SSPSAVEUPERSONALDETAILS";
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();

                            }
                            else
                            {
                                mCommand.CommandText = "UPDATE UMUSERADDRESSDETAILS SET USERADTLSID=:USERADTLSID,ADDRESSTYPE=:ADDRESSTYPE,HOUSENO=:HOUSENO,BUILDINGNAME=:BUILDINGNAME,ADDRESSLINE1=:ADDRESSLINE1,ADDRESSLINE2=:ADDRESSLINE2,COUNTRYID=:COUNTRYID,COUNTRYCODE=:COUNTRYCODE,COUNTRYNAME=:COUNTRYNAME,STATEID=:STATEID,STATECODE=:STATECODE,STATENAME=:STATENAME,CITYID=:CITYID,CITYCODE=:CITYCODE,CITYNAME=:CITYNAME,POSTCODE=:POSTCODE,FULLADDRESS=:FULLADDRESS,ISDEFAULT=:ISDEFAULT,DATEMODIFIED=:UADATEMODIFIED,PARTY=:PARTY WHERE LOWER(USERID)=:USERID";

                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("USERADTLSID", entity.UAUSERADTLSID);
                                mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                                mCommand.Parameters.Add("ADDRESSTYPE", entity.UAADDRESSTYPE);
                                mCommand.Parameters.Add("HOUSENO", entity.UAHOUSENO);
                                mCommand.Parameters.Add("BUILDINGNAME", entity.UABUILDINGNAME);
                                mCommand.Parameters.Add("ADDRESSLINE1", entity.UAADDRESSLINE1);
                                mCommand.Parameters.Add("ADDRESSLINE2", entity.UAADDRESSLINE2);
                                mCommand.Parameters.Add("COUNTRYID", entity.UACOUNTRYID);
                                mCommand.Parameters.Add("COUNTRYCODE", entity.UACOUNTRYCODE);
                                mCommand.Parameters.Add("COUNTRYNAME", entity.UACOUNTRYNAME);
                                mCommand.Parameters.Add("STATEID", entity.UASTATEID);
                                mCommand.Parameters.Add("STATECODE", entity.UASTATECODE);
                                mCommand.Parameters.Add("STATENAME", entity.UASTATENAME);
                                mCommand.Parameters.Add("CITYID", entity.UACITYID);
                                mCommand.Parameters.Add("CITYCODE", entity.UACITYCODE);
                                mCommand.Parameters.Add("CITYNAME", entity.UACITYNAME);
                                mCommand.Parameters.Add("POSTCODE", entity.UAPOSTCODE);
                                mCommand.Parameters.Add("FULLADDRESS", entity.UAFULLADDRESS);
                                mCommand.Parameters.Add("ISDEFAULT", entity.UAISDEFAULT);
                                mCommand.Parameters.Add("UADATEMODIFIED", DateTime.Now);
                                mCommand.Parameters.Add("PARTY", entity.PARTY);
                                mCommand.ExecuteNonQuery();

                                //----------- UPDATE User Personal Row in Party Master ---- Anil G-------------
                                mCommand.Parameters.Clear();

                                mCommand.CommandText = @"UPDATE SSPPARTYMASTER SET CLIENTNAME=:CLIENTNAME, HOUSENO=:HOUSENO,
                                                        BUILDINGNAME=:BUILDINGNAME, ADDRESSLINE1=:ADDRESSLINE1, ADDRESSLINE2=:ADDRESSLINE2,
                                                        PINCODE=:PINCODE, FULLADDRESS=:FULLADDRESS,
                                                        SALUTATION=:SALUTATION, FIRSTNAME=:FIRSTNAME, LASTNAME=:LASTNAME,
                                                        PHONE=:PHONE, EMAILID=:EMAILID, CREATEDBY=:CREATEDBY,
                                                        COUNTRYNAME=:COUNTRYNAME,STATENAME=:STATENAME,CITYNAME=:CITYNAME, 
                                                        STATECODE=:STATECODE,COUNTRYCODE=:COUNTRYCODE,CITYCODE=:CITYCODE,
                                                        STATEID=:STATEID,COUNTRYID=:COUNTRYID, CITYID=:CITYID
                                                        where LOWER(CREATEDBY)=:CREATEDBY AND UPERSONAL=1";

                                mCommand.Parameters.Add("CLIENTNAME", entity.COMPANYNAME);
                                mCommand.Parameters.Add("HOUSENO", entity.UAHOUSENO);
                                mCommand.Parameters.Add("BUILDINGNAME", entity.UABUILDINGNAME);
                                mCommand.Parameters.Add("ADDRESSLINE1", entity.UAADDRESSLINE1);
                                mCommand.Parameters.Add("ADDRESSLINE2", entity.UAADDRESSLINE2);
                                mCommand.Parameters.Add("PINCODE", entity.UAPOSTCODE);
                                mCommand.Parameters.Add("FULLADDRESS", entity.UAFULLADDRESS);
                                mCommand.Parameters.Add("SALUTATION", "Mr");
                                mCommand.Parameters.Add("FIRSTNAME", entity.FIRSTNAME);
                                mCommand.Parameters.Add("LASTNAME", entity.LASTNAME);
                                mCommand.Parameters.Add("PHONE", entity.MOBILENUMBER);
                                mCommand.Parameters.Add("EMAILID", entity.USERID);
                                mCommand.Parameters.Add("CREATEDBY", entity.USERID.ToLower());
                                mCommand.Parameters.Add("COUNTRYNAME", entity.UACOUNTRYNAME);
                                mCommand.Parameters.Add("STATENAME", entity.UASTATENAME);
                                mCommand.Parameters.Add("CITYNAME", entity.UACITYNAME);
                                mCommand.Parameters.Add("STATECODE", entity.UASTATECODE);
                                mCommand.Parameters.Add("COUNTRYCODE", entity.UACOUNTRYCODE);
                                mCommand.Parameters.Add("CITYCODE", entity.UACITYCODE);
                                mCommand.Parameters.Add("STATEID", entity.UASTATEID);
                                mCommand.Parameters.Add("COUNTRYID", entity.UACOUNTRYID);
                                mCommand.Parameters.Add("CITYID", entity.UACITYID);

                                mCommand.ExecuteNonQuery();
                            }

                            mCommand.CommandType = CommandType.Text;
                            //Fapio Details
                            if (entity.ISFAPIAO == 1)
                            {
                                if (entity.FUSERFAPIAOID == 0)
                                {
                                    mCommand.CommandText = "SELECT USERFAPIAODETAILS_SEQ.NEXTVAL FROM DUAL";
                                    entity.FUSERFAPIAOID = Convert.ToInt64(mCommand.ExecuteScalar());

                                    mCommand.CommandText = "INSERT INTO USERFAPIAODETAILS (FUSERFAPIAOID,USERID,FCOMPANYNAME,FCOMPANYNAMECHINESE,FCOMPANYADDRESS,FCOMPANYADDRESSCHINESE,FPHONENUMBER,FEMAILID,FTAXIDENTIFICATIONNO,FBANKNAME,FBANKACCOUNTNO,FDATECREATED,FDATEMODIFIED,FINDIVIDUALNAME,FINDTAXIDENTIFICATION,FINDPHONENUMBER,FINDEMAILID,ISINDIVIDUAL) VALUES (:FUSERFAPIAOID,:USERID,:FCOMPANYNAME,:FCOMPANYNAMECHINESE,:FCOMPANYADDRESS,:FCOMPANYADDRESSCHINESE,:FPHONENUMBER,:FEMAILID,:FTAXIDENTIFICATIONNO,:FBANKNAME,:FBANKACCOUNTNO,:FDATECREATED,:FDATEMODIFIED,:FINDIVIDUALNAME,:FINDTAXIDENTIFICATION,:FINDPHONENUMBER,:FINDEMAILID,:ISINDIVIDUAL)";
                                    mCommand.Parameters.Clear();
                                    mCommand.Parameters.Add("FUSERFAPIAOID", entity.FUSERFAPIAOID);
                                    mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                                    mCommand.Parameters.Add("FCOMPANYNAME", entity.FCOMPANYNAME);
                                    mCommand.Parameters.Add("FCOMPANYNAMECHINESE", entity.FCOMPANYNAMECHINESE);
                                    mCommand.Parameters.Add("FCOMPANYADDRESS", entity.FCOMPANYADDRESS);
                                    mCommand.Parameters.Add("FCOMPANYADDRESSCHINESE", entity.FCOMPANYADDRESSCHINESE);
                                    mCommand.Parameters.Add("FPHONENUMBER", entity.FPHONENUMBER);
                                    mCommand.Parameters.Add("FEMAILID", entity.FEMAILID);
                                    mCommand.Parameters.Add("FTAXIDENTIFICATIONNO", entity.FTAXIDENTIFICATIONNO);
                                    mCommand.Parameters.Add("FBANKNAME", entity.FBANKNAME);
                                    mCommand.Parameters.Add("FBANKACCOUNTNO", entity.FBANKACCOUNTNO);
                                    mCommand.Parameters.Add("FDATECREATED", DateTime.Now);
                                    mCommand.Parameters.Add("FDATEMODIFIED", DateTime.Now);
                                    mCommand.Parameters.Add("FINDIVIDUALNAME", entity.FINDIVIDUALNAME);
                                    mCommand.Parameters.Add("FINDTAXIDENTIFICATION", entity.FINDTAXIDENTIFICATION);
                                    mCommand.Parameters.Add("FINDPHONENUMBER", entity.FINDPHONENUMBER);
                                    mCommand.Parameters.Add("FINDEMAILID", entity.FINDEMAILID);
                                    mCommand.Parameters.Add("ISINDIVIDUAL", entity.ISINDIVIDUAL);
                                    mCommand.ExecuteNonQuery();
                                }
                                else
                                {
                                    mCommand.CommandText = "UPDATE USERFAPIAODETAILS SET FUSERFAPIAOID=:FUSERFAPIAOID,FCOMPANYNAMECHINESE=:FCOMPANYNAMECHINESE,FCOMPANYNAME=:FCOMPANYNAME,FCOMPANYADDRESSCHINESE=:FCOMPANYADDRESSCHINESE,FCOMPANYADDRESS=:FCOMPANYADDRESS,FPHONENUMBER=:FPHONENUMBER,FEMAILID=:FEMAILID,FTAXIDENTIFICATIONNO=:FTAXIDENTIFICATIONNO,FBANKNAME=:FBANKNAME,FBANKACCOUNTNO=:FBANKACCOUNTNO,FDATEMODIFIED=:FDATEMODIFIED,FINDIVIDUALNAME=:FINDIVIDUALNAME,FINDTAXIDENTIFICATION=:FINDTAXIDENTIFICATION,FINDPHONENUMBER=:FINDPHONENUMBER,FINDEMAILID=:FINDEMAILID,ISINDIVIDUAL=:ISINDIVIDUAL WHERE LOWER(USERID)=:USERID";
                                    mCommand.Parameters.Clear();
                                    mCommand.Parameters.Add("FUSERFAPIAOID", entity.FUSERFAPIAOID);
                                    mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                                    mCommand.Parameters.Add("FCOMPANYNAMECHINESE", entity.FCOMPANYNAMECHINESE);
                                    mCommand.Parameters.Add("FCOMPANYNAME", entity.FCOMPANYNAME);
                                    mCommand.Parameters.Add("FCOMPANYADDRESSCHINESE", entity.FCOMPANYADDRESSCHINESE);
                                    mCommand.Parameters.Add("FCOMPANYADDRESS", entity.FCOMPANYADDRESS);
                                    mCommand.Parameters.Add("FPHONENUMBER", entity.FPHONENUMBER);
                                    mCommand.Parameters.Add("FEMAILID", entity.FEMAILID);
                                    mCommand.Parameters.Add("FTAXIDENTIFICATIONNO", entity.FTAXIDENTIFICATIONNO);
                                    mCommand.Parameters.Add("FBANKNAME", entity.FBANKNAME);
                                    mCommand.Parameters.Add("FBANKACCOUNTNO", entity.FBANKACCOUNTNO);
                                    mCommand.Parameters.Add("FDATEMODIFIED", DateTime.Now);
                                    mCommand.Parameters.Add("FINDIVIDUALNAME", entity.FINDIVIDUALNAME);
                                    mCommand.Parameters.Add("FINDTAXIDENTIFICATION", entity.FINDTAXIDENTIFICATION);
                                    mCommand.Parameters.Add("FINDPHONENUMBER", entity.FINDPHONENUMBER);
                                    mCommand.Parameters.Add("FINDEMAILID", entity.FINDEMAILID);
                                    mCommand.Parameters.Add("ISINDIVIDUAL", entity.ISINDIVIDUAL);
                                    mCommand.ExecuteNonQuery();
                                }
                            }
                            else
                            {
                                mCommand.CommandText = "Delete from USERFAPIAODETAILS WHERE LOWER(USERID)=:USERID";
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                                mCommand.ExecuteNonQuery();
                            }

                            //-------------------Move Quotes to expity state if he changes country------------------------
                            if (ChangeState == "1")
                            {
                                mCommand.CommandText = "UPDATE QMQUOTATION SET DATEOFVALIDITY=:DATEOFVALIDITY WHERE (LOWER(CREATEDBY)=LOWER(:CREATEDBY) OR LOWER(GUESTEMAIL)=LOWER(:CREATEDBY))";
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("CREATEDBY", entity.USERID.ToLower());
                                mCommand.Parameters.Add("DATEOFVALIDITY", DateTime.Now.AddDays(-1).ToString("dd-MMM-yy"));
                                mCommand.ExecuteNonQuery();
                            }
                            //--------------------------------------------------------------------------------------------
                        }
                        if (hdnvalue == "1" && (entity.CREDITAPPID == 0))
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IP_INDUSTRYID", entity.INDUSTRYID);
                            mCommand.Parameters.Add("IP_TRADINGNAME", entity.COMPANYNAME);
                            mCommand.Parameters.Add("IP_COUNTRYID", entity.UACOUNTRYID);
                            mCommand.Parameters.Add("IP_COUNTRYCODE", entity.UACOUNTRYCODE);
                            mCommand.Parameters.Add("IP_COUNTRYNAME", entity.UACOUNTRYNAME);
                            mCommand.Parameters.Add("IP_CITYID", entity.UACITYID);
                            mCommand.Parameters.Add("IP_CITYNAME", entity.UACITYNAME);
                            mCommand.Parameters.Add("IP_NOOFEMP", entity.NOOFEMP);
                            mCommand.Parameters.Add("IP_SUBVERTICAL", null);
                            mCommand.Parameters.Add("IP_PARENTCOMPANY", entity.PARENTCOMPANY);
                            mCommand.Parameters.Add("IP_ANNUALTURNOVER", entity.ANNUALTURNOVER);
                            mCommand.Parameters.Add("IP_DBNUMBER", entity.DBNUMBER);
                            mCommand.Parameters.Add("IP_HFMENTRYCODE", entity.HFMENTRYCODE);
                            mCommand.Parameters.Add("IP_HFMENTRYCURRENCY", entity.HFMENTRYCURRENCY);
                            mCommand.Parameters.Add("IP_CURRENTCREDITLIMIT", entity.REQUESTEDCREDITLIMIT);
                            mCommand.Parameters.Add("IP_CURRENTPAYTERMSNAME", entity.CURRENTPAYTERMSNAME);
                            mCommand.Parameters.Add("IP_CREDITLIMITEXPDATE", null);
                            mCommand.Parameters.Add("IP_NEXTYEARREVENUE", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_NEXTYEARNR", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_NEXTYEARNRMARGIN", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_NEXTYEARBILLING", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_CURRENTYEARREVENUE", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_CURRENTYEARNR", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_CURRENTYEARNRMARGIN", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_CURRENTYEARBILLING", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_PREVIOUSYEARREVENUE", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_PREVIOUSYEARNR", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_PREVIOUSYEARNRMARGIN", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_PREVIOUSYEARBILLING", Convert.ToDecimal(0));
                            mCommand.Parameters.Add("IP_PRODUCTPROFILE", "Contract Logistics");
                            mCommand.Parameters.Add("IP_SALESCHANNEL", "ShipA/Others"); //SME ShipA Freight/Others
                            mCommand.Parameters.Add("IP_COMMENTS", entity.COMMENTS);
                            mCommand.Parameters.Add("IP_USERID", entity.USERID);
                            mCommand.Parameters.Add("IP_CURRENCYID", entity.CURRENCYID);
                            mCommand.Parameters.Add("IP_CURRENCYDESC", entity.CURRENCYDESC);
                            mCommand.Parameters.Add("IP_DESCRIPTIONOFKS", null);
                            mCommand.Parameters.Add("IP_CREATEDBY", entity.USERID);
                            mCommand.Parameters.Add("IP_DATECREATED", DateTime.Now.ToString("dd-MMM-yyyy"));
                            mCommand.Parameters.Add("IP_STATEID", "Requested");
                            mCommand.Parameters.Add("IP_EXISTINGCUSTOMER", entity.EXISTINGCUSTOMER);
                            mCommand.Parameters.Add("IP_USEMYCREIDT", entity.USEMYCREIDT);
                            mCommand.CommandText = "USP_INSERT_USERCREDITDATA";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }

                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }

                    //SSP NOTIFICATIONS ADDED BY SIVA
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand muCommand = mConnection.CreateCommand())
                    {

                        muCommand.BindByName = true;
                        muCommand.Transaction = mOracleTransaction;
                        muCommand.CommandType = CommandType.Text;
                        try
                        {
                            muCommand.CommandText = "SELECT SSP_NOTIFICATIONS_SEQ.NEXTVAL FROM DUAL";
                            entity.NOTIFICATIONID = Convert.ToInt64(muCommand.ExecuteScalar());

                            muCommand.CommandText = "INSERT INTO SSPNOTIFICATIONS (NOTIFICATIONID,NOTIFICATIONTYPEID,USERID,DATECREATED,DATEREQUESTED,NOTIFICATIONCODE) VALUES (:NOTIFICATIONID,:NOTIFICATIONTYPEID,:USERID,:DATECREATED,:DATEREQUESTED,:NOTIFICATIONCODE)";
                            muCommand.Parameters.Add("NOTIFICATIONID", Convert.ToInt32(entity.NOTIFICATIONID));
                            muCommand.Parameters.Add("NOTIFICATIONTYPEID", 5115);
                            muCommand.Parameters.Add("USERID", entity.USERID.ToUpper());
                            muCommand.Parameters.Add("DATECREATED", DateTime.Now);
                            muCommand.Parameters.Add("DATEREQUESTED", DateTime.Now);
                            muCommand.Parameters.Add("NOTIFICATIONCODE", notifymsg);
                            muCommand.ExecuteNonQuery();

                            mOracleTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            mOracleTransaction.Rollback();
                            throw;
                        }
                    }

                }
            }
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "DELETE FROM SSPNOTIFICATIONS WHERE NOTIFICATIONTYPEID=5111 AND UPPER(USERID) = :USERID",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "USERID",Value = entity.USERID.ToUpper()},
                });

        }
        public void UpdateCredit(UserProfileEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IP_INDUSTRYID", entity.INDUSTRYID);
                        mCommand.Parameters.Add("IP_TRADINGNAME", entity.COMPANYNAME);
                        mCommand.Parameters.Add("IP_COUNTRYID", entity.UACOUNTRYID);
                        mCommand.Parameters.Add("IP_COUNTRYCODE", entity.UACOUNTRYCODE);
                        mCommand.Parameters.Add("IP_COUNTRYNAME", entity.UACOUNTRYNAME);
                        mCommand.Parameters.Add("IP_CITYID", entity.UACITYID);
                        mCommand.Parameters.Add("IP_CITYNAME", entity.UACITYNAME);
                        mCommand.Parameters.Add("IP_NOOFEMP", entity.NOOFEMP);
                        mCommand.Parameters.Add("IP_SUBVERTICAL", null);
                        mCommand.Parameters.Add("IP_PARENTCOMPANY", entity.PARENTCOMPANY);
                        mCommand.Parameters.Add("IP_ANNUALTURNOVER", entity.ANNUALTURNOVER);
                        mCommand.Parameters.Add("IP_DBNUMBER", entity.DBNUMBER);
                        mCommand.Parameters.Add("IP_HFMENTRYCODE", entity.HFMENTRYCODE);
                        mCommand.Parameters.Add("IP_HFMENTRYCURRENCY", entity.HFMENTRYCURRENCY);
                        mCommand.Parameters.Add("IP_CURRENTCREDITLIMIT", entity.REQUESTEDCREDITLIMIT);
                        mCommand.Parameters.Add("IP_CURRENTPAYTERMSNAME", entity.CURRENTPAYTERMSNAME);
                        mCommand.Parameters.Add("IP_CREDITLIMITEXPDATE", null);
                        mCommand.Parameters.Add("IP_NEXTYEARREVENUE", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_NEXTYEARNR", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_NEXTYEARNRMARGIN", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_NEXTYEARBILLING", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARREVENUE", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARNR", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARNRMARGIN", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARBILLING", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARREVENUE", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARNR", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARNRMARGIN", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARBILLING", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PRODUCTPROFILE", "Contract Logistics");
                        mCommand.Parameters.Add("IP_SALESCHANNEL", "ShipA/Others"); //SME ShipA Freight/Others
                        mCommand.Parameters.Add("IP_COMMENTS", entity.COMMENTS);
                        mCommand.Parameters.Add("IP_USERID", entity.USERID);
                        mCommand.Parameters.Add("IP_CURRENCYID", entity.CURRENCYID);
                        mCommand.Parameters.Add("IP_CURRENCYDESC", entity.CURRENCYDESC);
                        mCommand.Parameters.Add("IP_DESCRIPTIONOFKS", null);
                        mCommand.Parameters.Add("IP_CREATEDBY", entity.USERID);
                        mCommand.Parameters.Add("IP_DATECREATED", DateTime.Now.ToString("dd-MMM-yyyy"));
                        mCommand.Parameters.Add("IP_STATEID", "Requested");
                        mCommand.Parameters.Add("IP_EXISTINGCUSTOMER", entity.EXISTINGCUSTOMER);
                        mCommand.Parameters.Add("IP_USEMYCREIDT", entity.USEMYCREIDT);
                        mCommand.CommandText = "USP_INSERT_USERCREDITDATA";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand muCommand = mConnection.CreateCommand())
                {
                    muCommand.BindByName = true;
                    muCommand.Transaction = mOracleTransaction;
                    muCommand.CommandType = CommandType.Text;
                    try
                    {
                        muCommand.CommandText = "SELECT SSP_NOTIFICATIONS_SEQ.NEXTVAL FROM DUAL";
                        entity.NOTIFICATIONID = Convert.ToInt64(muCommand.ExecuteScalar());

                        muCommand.CommandText = "INSERT INTO SSPNOTIFICATIONS (NOTIFICATIONID,NOTIFICATIONTYPEID,USERID,DATECREATED,DATEREQUESTED,NOTIFICATIONCODE) VALUES (:NOTIFICATIONID,:NOTIFICATIONTYPEID,:USERID,:DATECREATED,:DATEREQUESTED,:NOTIFICATIONCODE)";
                        muCommand.Parameters.Add("NOTIFICATIONID", Convert.ToInt32(entity.NOTIFICATIONID));
                        muCommand.Parameters.Add("NOTIFICATIONTYPEID", 5115);
                        muCommand.Parameters.Add("USERID", entity.USERID.ToUpper());
                        muCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        muCommand.Parameters.Add("DATEREQUESTED", DateTime.Now);
                        muCommand.Parameters.Add("NOTIFICATIONCODE", "Credit Requested");
                        muCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }

            }
        }
        public long? UpdateAdditionalCredit(UserProfileEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            long? addcreditId = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IP_CREDITAPPID", entity.CREDITAPPID);
                        mCommand.Parameters.Add("IP_INDUSTRYID", entity.INDUSTRYID);
                        mCommand.Parameters.Add("IP_TRADINGNAME", entity.COMPANYNAME);
                        mCommand.Parameters.Add("IP_COUNTRYID", entity.UACOUNTRYID);
                        mCommand.Parameters.Add("IP_COUNTRYCODE", entity.UACOUNTRYCODE);
                        mCommand.Parameters.Add("IP_COUNTRYNAME", entity.UACOUNTRYNAME);
                        mCommand.Parameters.Add("IP_CITYID", entity.UACITYID);
                        mCommand.Parameters.Add("IP_CITYNAME", entity.UACITYNAME);
                        mCommand.Parameters.Add("IP_NOOFEMP", entity.NOOFEMP);
                        mCommand.Parameters.Add("IP_SUBVERTICAL", null);
                        mCommand.Parameters.Add("IP_PARENTCOMPANY", entity.PARENTCOMPANY);
                        mCommand.Parameters.Add("IP_ANNUALTURNOVER", entity.ANNUALTURNOVER);
                        mCommand.Parameters.Add("IP_DBNUMBER", entity.DBNUMBER);
                        mCommand.Parameters.Add("IP_HFMENTRYCODE", entity.HFMENTRYCODE);
                        mCommand.Parameters.Add("IP_HFMENTRYCURRENCY", entity.HFMENTRYCURRENCY);
                        mCommand.Parameters.Add("IP_CURRENTCREDITLIMIT", entity.REQUESTEDCREDITLIMIT);
                        mCommand.Parameters.Add("IP_CURRENTPAYTERMSNAME", entity.CURRENTPAYTERMSNAME);
                        mCommand.Parameters.Add("IP_CREDITLIMITEXPDATE", null);
                        mCommand.Parameters.Add("IP_NEXTYEARREVENUE", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_NEXTYEARNR", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_NEXTYEARNRMARGIN", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_NEXTYEARBILLING", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARREVENUE", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARNR", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARNRMARGIN", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_CURRENTYEARBILLING", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARREVENUE", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARNR", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARNRMARGIN", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PREVIOUSYEARBILLING", Convert.ToDecimal(0));
                        mCommand.Parameters.Add("IP_PRODUCTPROFILE", "Contract Logistics");
                        mCommand.Parameters.Add("IP_SALESCHANNEL", "ShipA/Others"); //SME ShipA Freight/Others
                        mCommand.Parameters.Add("IP_COMMENTS", entity.COMMENTS);
                        mCommand.Parameters.Add("IP_USERID", entity.USERID);
                        mCommand.Parameters.Add("IP_CURRENCYID", entity.CURRENCYID);
                        mCommand.Parameters.Add("IP_CURRENCYDESC", entity.CURRENCYDESC);
                        mCommand.Parameters.Add("IP_DESCRIPTIONOFKS", null);
                        mCommand.Parameters.Add("IP_CREATEDBY", entity.USERID);
                        mCommand.Parameters.Add("IP_DATECREATED", DateTime.Now.ToString("dd-MMM-yyyy"));
                        mCommand.Parameters.Add("IP_STATEID", "Requested");
                        mCommand.Parameters.Add("IP_EXISTINGCUSTOMER", entity.EXISTINGCUSTOMER);
                        mCommand.Parameters.Add("IP_USEMYCREIDT", entity.USEMYCREIDT);
                        mCommand.Parameters.Add("IP_CREDITREFNO", entity.CREDITREFNO);
                        mCommand.Parameters.Add("OP_ADDITIONALCREDITID", OracleDbType.Int64, 18).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_INSERT_ADDITIONALCREDIT";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        if (((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["OP_ADDITIONALCREDITID"].Value)).Value > 0)
                        {
                            addcreditId = Convert.ToInt64(mCommand.Parameters["OP_ADDITIONALCREDITID"].Value.ToString());
                        };

                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        addcreditId = null;
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand muCommand = mConnection.CreateCommand())
                {
                    muCommand.BindByName = true;
                    muCommand.Transaction = mOracleTransaction;
                    muCommand.CommandType = CommandType.Text;
                    try
                    {
                        muCommand.CommandText = "SELECT SSP_NOTIFICATIONS_SEQ.NEXTVAL FROM DUAL";
                        entity.NOTIFICATIONID = Convert.ToInt64(muCommand.ExecuteScalar());

                        muCommand.CommandText = "INSERT INTO SSPNOTIFICATIONS (NOTIFICATIONID,NOTIFICATIONTYPEID,USERID,DATECREATED,DATEREQUESTED,NOTIFICATIONCODE) VALUES (:NOTIFICATIONID,:NOTIFICATIONTYPEID,:USERID,:DATECREATED,:DATEREQUESTED,:NOTIFICATIONCODE)";
                        muCommand.Parameters.Add("NOTIFICATIONID", Convert.ToInt32(entity.NOTIFICATIONID));
                        muCommand.Parameters.Add("NOTIFICATIONTYPEID", 5115);
                        muCommand.Parameters.Add("USERID", entity.USERID.ToUpper());
                        muCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        muCommand.Parameters.Add("DATEREQUESTED", DateTime.Now);
                        muCommand.Parameters.Add("NOTIFICATIONCODE", "Credit Requested");
                        muCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }

            }
            return addcreditId;
        }

        public void ResetPassword(string UserId, string Password)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        int FAILEDPASSWORDATTEMPT = 0; string TOKEN = ""; DateTime? TOKENEXPIRY = null;
                        mCommand.Parameters.Add("IN_USERID", UserId);
                        mCommand.Parameters.Add("IN_PASSWORD", Password);
                        mCommand.Parameters.Add("IN_FAILEDPASSWORDATTEMPT", FAILEDPASSWORDATTEMPT);
                        mCommand.Parameters.Add("IN_TOKEN", TOKEN);
                        mCommand.Parameters.Add("IN_TOKENEXP", TOKENEXPIRY);
                        mCommand.CommandText = "USP_SSP_RESETPASSWORD";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public string GetUMConfigValue(string ConfigName)
        {
            UMConfigurationEntity mEntity = new UMConfigurationEntity();
            ReadText(DBConnectionMode.SSP, "SELECT CONFIGVALUE FROM UMCONFIGURATIONS WHERE CONFIGNAME = :CONFIGNAME",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "CONFIGNAME", Value = ConfigName } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<UMConfigurationEntity>(r);
            });
            return mEntity.CONFIGVALUE;
        }

        public long UpdateUserToken(UserEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "UPDATE USERS SET TOKEN=:TOKEN,TOKENEXPIRY=:TOKENEXPIRY WHERE LOWER(USERID)=:USERID";
                        mCommand.Parameters.Add("USERID", entity.USERID.ToLower());
                        mCommand.Parameters.Add("TOKEN", entity.TOKEN);
                        mCommand.Parameters.Add("TOKENEXPIRY", entity.TOKENEXPIRY);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return 1;
        }

        public int UploadPhoto(byte[] ImgContent, string ImgName, string UserId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "UPDATE USERS SET IMGFILENAME=:IMGFILENAME,IMGCONTENT=:IMGCONTENT WHERE LOWER(USERID)=:USERID";
                        mCommand.Parameters.Add("IMGFILENAME", ImgName);
                        mCommand.Parameters.Add("IMGCONTENT", ImgContent);
                        mCommand.Parameters.Add("USERID", UserId.ToLower());
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return 1;
        }

        public List<JobTitleEntity> GetJobTitleDetails(string JobTitleDesc)
        {
            List<JobTitleEntity> mEntity = new List<JobTitleEntity>();

            string Query = "SELECT JOBTITLEID,JOBTITLEDESC from UMJOBTITLEMASTER where 1=1 ";
            if (!string.IsNullOrEmpty(JobTitleDesc.Trim()))
            {
                Query = Query + " AND Upper(JOBTITLEDESC) like '%" + JobTitleDesc.ToUpper() + "%'";
            }

            ReadText(DBConnectionMode.SSP, Query,
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<JobTitleEntity>(r));
            });

            return mEntity;
        }

        public long CreateJobTitle(JobTitleEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            long JobTitleId = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT UMJOBTITLEMASTER_SEQ.NEXTVAL FROM DUAL";
                        entity.JOBTITLEID = Convert.ToInt64(mCommand.ExecuteScalar());
                        JobTitleId = entity.JOBTITLEID;

                        mCommand.CommandText = "INSERT INTO UMJOBTITLEMASTER (JOBTITLEID,JOBTITLEDESC) VALUES (:JOBTITLEID,:JOBTITLEDESC)";
                        mCommand.Parameters.Add("JOBTITLEID", entity.JOBTITLEID);
                        mCommand.Parameters.Add("JOBTITLEDESC", entity.JOBTITLEDESC.ToString().Trim());

                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
                return JobTitleId;
            }
        }

        public List<NotificationEntity> GetNotificationDetails(string UserId)
        {
            List<NotificationEntity> mEntity = new List<NotificationEntity>();

            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPNOTIFICATIONS WHERE UPPER(USERID) = :USERID AND STATEID NOT IN ('NOTIFICATIONDELETEDSTATE') ORDER BY DATECREATED DESC",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToUpper() } }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<NotificationEntity>(r));
            });
            return mEntity;
        }

        public DataSet GetNotificationDetailsScript(string UserId)
        {
            DataSet ds = new DataSet();
            string Query = @"SELECT * FROM (SELECT  * FROM SSPNOTIFICATIONS WHERE UPPER(USERID) =:USERID AND STATEID NOT IN('NOTIFICATIONDELETEDSTATE') ORDER BY DATECREATED DESC) WHERE ROWNUM <= 2";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "USERID", Value = UserId.ToUpper() } });
            ds.Tables.Add(dt);
            return ds;
        }

        public DataTable ShowNotificationCount(string UserId)
        {
            if (UserId != null && UserId != "")
            {
                string Query = @"SELECT  COUNT(CASE NOTIFICATIONSTATUS WHEN 'N' THEN 1 END) NOTIFICATIONCOUNT, COUNT( CASE STATEID WHEN 'NOTIFICATIONCREATEDSTATE' THEN 1 END) TOTALNOTIFICATIONSCOUNT FROM SSPNOTIFICATIONS WHERE UPPER(USERID) =:USERID AND STATEID NOT IN('NOTIFICATIONDELETEDSTATE')";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "USERID", Value = UserId.ToUpper() } });
                return dt;
            }
            else
            {
                DataTable Table1 = new DataTable();
                Table1.Columns.Add("NOTIFICATIONCOUNT", typeof(string));
                Table1.Columns.Add("TOTALNOTIFICATIONSCOUNT", typeof(string));
                Table1.Rows.Add("0", "0");
                return Table1;
            }
        }

        public string DeleteNotification(int Id)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE SSPNOTIFICATIONS SET STATEID='NOTIFICATIONDELETEDSTATE' WHERE NOTIFICATIONID= :NOTIFICATIONID",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "NOTIFICATIONID",Value = Id},
                });
            return "Deleted";
        }
        public string NotificationsMark(int Id)
        {

            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE SSPNOTIFICATIONS SET NOTIFICATIONSTATUS='Y' WHERE NOTIFICATIONID= :NOTIFICATIONID",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "NOTIFICATIONID", Value = Id },
                });
            return "Marked";

        }

        public string UnlocationMapping(int CountryId)
        {
            string ISSTATEAPPLICABLE = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT ISSTATEAPPLICABLE FROM UNLOCATIONS_MAPPING WHERE COUNTRYID = :COUNTRYID",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "COUNTRYID", Value = CountryId },
                });

            if (ISSTATEAPPLICABLE == null)
                ISSTATEAPPLICABLE = "No";

            return ISSTATEAPPLICABLE;
        }

        public string NewNotification(int nTypeId, string Status, string BookingId, int JobNumber, int QuotationId, string QuotationNumber, string UserId)
        {
            UserEntity entity = new UserEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT SSP_NOTIFICATIONS_SEQ.NEXTVAL FROM DUAL";
                        entity.NOTIFICATIONID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "INSERT INTO SSPNOTIFICATIONS (NOTIFICATIONID,NOTIFICATIONTYPEID,USERID,DATECREATED,DATEREQUESTED,NOTIFICATIONCODE,JOBNUMBER,BOOKINGID,QUOTATIONID,QUOTATIONNUMBER) VALUES (:NOTIFICATIONID,:NOTIFICATIONTYPEID,:USERID,:DATECREATED,:DATEREQUESTED,:NOTIFICATIONCODE,:JOBNUMBER,:BOOKINGID,:QUOTATIONID,:QUOTATIONNUMBER)";
                        mCommand.Parameters.Add("NOTIFICATIONID", Convert.ToInt32(entity.NOTIFICATIONID));
                        mCommand.Parameters.Add("NOTIFICATIONTYPEID", Convert.ToInt32(nTypeId));
                        mCommand.Parameters.Add("USERID", UserId.ToLower());
                        mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("DATEREQUESTED", DateTime.Now);
                        mCommand.Parameters.Add("NOTIFICATIONCODE", Status);
                        mCommand.Parameters.Add("JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("BOOKINGID", BookingId);
                        mCommand.Parameters.Add("QUOTATIONID", Convert.ToInt32(QuotationId));
                        mCommand.Parameters.Add("QUOTATIONNUMBER", QuotationNumber);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return "Success";
        }

        public DataSet IsProfileUpdated(string UserId)
        {
            DataSet ds = new DataSet();
            if (UserId != null && UserId != "")
            {
                string Query = @"SELECT COUNT(1) AS NOTIFICATIONCOUNT FROM UMUSERADDRESSDETAILS WHERE UPPER(USERID) =:USERID ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "USERID", Value = UserId.ToUpper() } });
                ds.Tables.Add(dt);
                return ds;
            }
            else
            {
                DataTable Table1 = new DataTable();
                Table1.Columns.Add("NOTIFICATIONCOUNT", typeof(string));
                Table1.Rows.Add("0");
                ds.Tables.Add(Table1);
                return ds;
            }
        }

        public int InsertFailedPasswordAttemtsCount(string UserId, string status)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            int FAILEDPASSWORDATTEMPT = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        if (status == "False")
                        {
                            FAILEDPASSWORDATTEMPT = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT FAILEDPASSWORDATTEMPT FROM USERS WHERE LOWER(USERID)=:USERID",
                                        new OracleParameter[]{
                                        new OracleParameter{ ParameterName = "USERID", Value = UserId.ToLower()},                  
                                        });

                            mCommand.CommandText = "UPDATE USERS SET FAILEDPASSWORDATTEMPT=:FAILEDPASSWORDATTEMPT WHERE LOWER(USERID)=:USERID";
                            mCommand.Parameters.Add("USERID", UserId.ToLower());
                            mCommand.Parameters.Add("FAILEDPASSWORDATTEMPT", FAILEDPASSWORDATTEMPT + 1);
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                        }
                        else
                        {
                            mCommand.CommandText = "UPDATE USERS SET FAILEDPASSWORDATTEMPT=:FAILEDPASSWORDATTEMPT WHERE LOWER(USERID)=:USERID";
                            mCommand.Parameters.Add("USERID", UserId.ToLower());
                            mCommand.Parameters.Add("FAILEDPASSWORDATTEMPT", FAILEDPASSWORDATTEMPT);
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return FAILEDPASSWORDATTEMPT; ;
        }

        public DataTable GetUserImage(string USerid)
        {
            DataTable dt = new DataTable();
            if (USerid != "")
            {
                string Query = "select USERID,IMGCONTENT,IMGFILENAME from users where UPPER(userid)=:USERID ";
                dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "USERID", Value = USerid.ToUpper() } });
            }
            return dt;
        }

        public void RestInstantPassword(string UserId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        mCommand.CommandText = "UPDATE USERS SET INSTANTPASSWORD='' WHERE LOWER(USERID)=:USERID";
                        mCommand.Parameters.Add("USERID", UserId.ToLower());
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public List<DBFactory.Entities.UserPartyMaster> GetUserPartyMaster_test(string UserId)
        {
            List<DBFactory.Entities.UserPartyMaster> mDBEntity = new List<DBFactory.Entities.UserPartyMaster>();

            string Query = @"select SSPCLIENTID,Clientname,HOUSENO,BUILDINGNAME,ADDRESSLINE1,ADDRESSLINE2,COUNTRYNAME,CITYNAME,
                             STATENAME,PHONE,EMAILID,Pincode,FIRSTNAME,LASTNAME,SALUTATION,COUNTRYID,STATEID,CITYID,UPERSONAL from SSPPARTYMASTER where UPPER(CREATEDBY)=:CREATEDBY";
            ReadText(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "CREATEDBY", Value = UserId.ToUpper() } }, delegate(IDataReader r)
            {
                mDBEntity.Add(DataReaderMapToObject<UserPartyMaster>(r));
            });

            return mDBEntity;

        }

        public List<DBFactory.Entities.UserPartyMaster> GetUserPartyMaster(string UserId, string SearchString, int CountryId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("Loc_Createdby", UserId.ToUpper());
                        mCommand.Parameters.Add("SearchKey", SearchString.ToUpper());
                        mCommand.Parameters.Add("PRCountryId", CountryId);
                        mCommand.Parameters.Add("CUR_FPARTYS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_SSP_JOB_GETPARTYMASTER1";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_FPARTYS"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.UserPartyMaster> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.UserPartyMaster>(dt);
                        reader1.Close();
                        mConnection.Close();
                        return mDBEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataSet AddeNewPartyAddress(string SSPClientID, string CompanyName, string HouseNO, string BulName, string PAddrLine1,
                                            string PAddrLine2, string PostalCode, string FirstName, string LastName, string Email, string MobNumber,
                                            string CountryName, string StateName, string CityName, string CreatedBy,
                                            string CountryID, string StateID, string CityID, string Title, string CountryCode, string StateCode,
                                            string CityCode, string NickNmae, string TaxID, string FullAddressChinese, string ISDCODE)
        {
            UserEntity entity = new UserEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = "SELECT SSPPARTYMASTER_SEQ.NEXTVAL FROM DUAL";
                        int SSPCLIENTID = Convert.ToInt32(mCommand.ExecuteScalar());
                        mCommand.Parameters.Clear();
                        mCommand.CommandType = CommandType.StoredProcedure;
                        string fulladdress = HouseNO + "," + BulName + "," + PAddrLine1 + "," + PAddrLine2 + "," + CountryName + "," + StateName + "," + CityName + "," + PostalCode;
                        mCommand.Parameters.Add("IP_SSPCLIENTID", Convert.ToInt32(SSPCLIENTID));
                        mCommand.Parameters.Add("IN_CLIENTNAME", Convert.ToString(CompanyName));
                        mCommand.Parameters.Add("CLIENTID", Convert.ToString("SPD_" + SSPCLIENTID.ToString()));
                        mCommand.Parameters.Add("IN_HOUSENO", HouseNO);
                        mCommand.Parameters.Add("IN_BUILDINGNAME", BulName);
                        mCommand.Parameters.Add("IN_ADDRESSLINE1", PAddrLine1);
                        mCommand.Parameters.Add("IN_ADDRESSLINE2", PAddrLine2);
                        mCommand.Parameters.Add("IN_COUNTRYNAME", CountryName);
                        mCommand.Parameters.Add("IN_STATENAME", StateName);
                        mCommand.Parameters.Add("IN_CITYNAME", CityName);
                        mCommand.Parameters.Add("IN_PINCODE", PostalCode);
                        mCommand.Parameters.Add("IN_FIRSTNAME", FirstName);
                        mCommand.Parameters.Add("IN_LASTNAME", LastName);
                        mCommand.Parameters.Add("IN_SALUTATION", Title);
                        mCommand.Parameters.Add("IN_PHONE", MobNumber);
                        mCommand.Parameters.Add("IN_EMAILID", Email);
                        mCommand.Parameters.Add("IN_CREATEDBY", CreatedBy);
                        mCommand.Parameters.Add("IN_COUNTRYID", CountryID);
                        mCommand.Parameters.Add("IN_STATEID", string.IsNullOrEmpty(StateID) ? 0 : Convert.ToInt64(StateID));
                        mCommand.Parameters.Add("IN_CITYID", string.IsNullOrEmpty(CityID) ? 0 : Convert.ToInt64(CityID));
                        mCommand.Parameters.Add("IN_FULLADDRESS", fulladdress);
                        mCommand.Parameters.Add("IN_CountryCode", string.IsNullOrEmpty(CountryCode) ? "0" : CountryCode);
                        mCommand.Parameters.Add("IN_StateCode", string.IsNullOrEmpty(StateCode) ? "0" : StateCode);
                        mCommand.Parameters.Add("IN_CityCode", string.IsNullOrEmpty(CityCode) ? "0" : CityCode);
                        mCommand.Parameters.Add("IN_NICKNAME", NickNmae);
                        mCommand.Parameters.Add("IN_TAXID", TaxID);
                        mCommand.Parameters.Add("IN_FULLADDRESSINCHINESE", FullAddressChinese);
                        mCommand.Parameters.Add("IN_ISDCODE", ISDCODE);
                        mCommand.CommandText = "USP_SSP_INSERTPARTYMASTER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        mCommand.CommandType = CommandType.Text;
                        mCommand.Parameters.Clear();
                        DataSet ds = new DataSet();
                        string Query = @"SELECT SSPCLIENTID,Clientname,HOUSENO,BUILDINGNAME,ADDRESSLINE1,ADDRESSLINE2,COUNTRYNAME,CITYNAME,
                                         STATENAME,PHONE,EMAILID,Pincode,FIRSTNAME,LASTNAME,SALUTATION,COUNTRYID,STATEID,CITYID,UPERSONAL,TaxID,ISDCODE FROM
                                         SSPPARTYMASTER WHERE SSPCLIENTID =" + SSPCLIENTID;
                        DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { } });
                        ds.Tables.Add(dt);
                        mConnection.Close();
                        return ds;
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }


        public Int64 UpdtePartyAddress(string SSPCLIENTID, string CompanyName, string HouseNO, string BulName, string PAddrLine1,
                                        string PAddrLine2, string PostalCode, string FirstName, string LastName, string Email, string MobNumber, string CountryName,
                                        string StateName, string CityName, string CreatedBy, string CountryID, string StateID, string CityID, string Title,
            string CountryCode, string StateCode, string CityCode, string NickNmae, string TaxID, string FullAddressChinese, string ISDCODE)
        {
            UserEntity entity = new UserEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.CommandText = "SSP_UPDATEPARTYMASTER";

                        mCommand.Parameters.Add("V_SSPCLIENTID", Convert.ToInt32(SSPCLIENTID));
                        mCommand.Parameters.Add("V_Clientname", Convert.ToString(CompanyName));
                        mCommand.Parameters.Add("V_HOUSENO", HouseNO);
                        mCommand.Parameters.Add("V_BUILDINGNAME", BulName);
                        mCommand.Parameters.Add("V_ADDRESSLINE1", PAddrLine1);
                        mCommand.Parameters.Add("V_ADDRESSLINE2", PAddrLine2);
                        mCommand.Parameters.Add("V_COUNTRYNAME", CountryName);
                        mCommand.Parameters.Add("V_STATENAME", StateName);
                        mCommand.Parameters.Add("V_CITYNAME", CityName);
                        mCommand.Parameters.Add("V_Pincode", PostalCode);
                        mCommand.Parameters.Add("V_FIRSTNAME", FirstName);
                        mCommand.Parameters.Add("V_LASTNAME", LastName);
                        mCommand.Parameters.Add("V_SALUTATION", Title);
                        mCommand.Parameters.Add("V_PHONE", MobNumber);
                        mCommand.Parameters.Add("V_EMAILID", Email);
                        mCommand.Parameters.Add("V_CREATEDBY", CreatedBy);

                        mCommand.Parameters.Add("V_COUNTRYID", CountryID);
                        mCommand.Parameters.Add("V_STATEID", StateID);
                        mCommand.Parameters.Add("V_CITYID", CityID);

                        mCommand.Parameters.Add("IN_CountryCode", CountryCode);
                        mCommand.Parameters.Add("IN_StateCode", StateCode);
                        mCommand.Parameters.Add("IN_CityCode", CityCode);
                        mCommand.Parameters.Add("IN_NICKNAME", NickNmae);
                        mCommand.Parameters.Add("IN_TAXID", TaxID);
                        mCommand.Parameters.Add("IN_FULLADDRESSINCHINESE", FullAddressChinese);
                        mCommand.Parameters.Add("IN_ISDCODE", ISDCODE);
                        mCommand.Parameters.Add("OP_Count", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        Int64 Count = Convert.ToInt64(((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["OP_Count"].Value)).Value);
                        return Count;
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }
        public decimal DeleteParty(int SSPClientID, string p)
        {
            decimal SuccessCode = 0;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.CommandText = "SSPDELETEPARTY";
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("LOC_SSPCLIENTID", SSPClientID);
                        mCommand.Parameters.Add("LOC_Createdby", p);
                        mCommand.Parameters.Add("V_Success", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();

                        SuccessCode = ((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["V_Success"].Value)).Value;
                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return SuccessCode;
        }
        public DataSet GetCountryTaxDetails(int countryid)
        {
            DataSet ds = new DataSet();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_COUNTRYID", countryid);
                        mCommand.Parameters.Add("PRM_COUNTRYDATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_FOCIS_GETTAXDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["PRM_COUNTRYDATA"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);

                        ds.Tables.Add(dt);
                        reader1.Close();

                        mConnection.Close();
                        return ds;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }


        //public void InsertRecordInCreditSetup(string USerID)
        //{
        //    OracleConnection mConnection;
        //    OracleTransaction mOracleTransaction = null;

        //    using (mConnection = new OracleConnection(SSPConnectionString))
        //    {

        //        if (mConnection.State == ConnectionState.Closed) mConnection.Open();
        //        mOracleTransaction = mConnection.BeginTransaction();
        //        using (OracleCommand mCommand = mConnection.CreateCommand())
        //        {
        //            mCommand.BindByName = true;
        //            mCommand.Transaction = mOracleTransaction;
        //            mCommand.CommandType = CommandType.StoredProcedure;

        //            try
        //            {
        //                mCommand.Parameters.Clear();

        //                mCommand.Parameters.Add("V_CreatedBy", USerID);
        //                mCommand.CommandText = "USP_ExtUsr_CRDAuthReq";
        //                mCommand.CommandType = CommandType.StoredProcedure;
        //                mCommand.ExecuteNonQuery();
        //                mOracleTransaction.Commit();
        //                mConnection.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                mOracleTransaction.Rollback();
        //                throw new Exception(ex.ToString());
        //            }
        //        }
        //    }
        //}

        public UserProfileEntity GetUserPartyMasterForProfile(string UserId, string SearchString)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("Loc_Createdby", UserId.ToUpper());
                        mCommand.Parameters.Add("SearchKey", SearchString.ToUpper());
                        mCommand.Parameters.Add("CUR_FPARTYS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("PROF_INFO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "SSPGETPARTYMASTER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_FPARTYS"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.UserPartyMaster> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.UserPartyMaster>(dt);

                        OracleDataReader reader2 = ((OracleRefCursor)mCommand.Parameters["PROF_INFO"].Value).GetDataReader();
                        dt = new DataTable();
                        dt.Load(reader2);
                        var uentity = MyClass.ConvertToEntity<DBFactory.Entities.UserProfileEntity>(dt);

                        reader1.Close(); reader2.Close();
                        mConnection.Close();
                        uentity.PartyMaster = mDBEntity;

                        return uentity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public UserProfileEntity GetSupportRequests(string UserId, string TabStatus, string SearchString, int V_PAGENO)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("TabStatus", TabStatus.ToString());
                        mCommand.Parameters.Add("V_CREATEDBY", UserId.ToUpper());
                        mCommand.Parameters.Add("SearchKey", SearchString.ToUpper());
                        mCommand.Parameters.Add("V_PAGENO", V_PAGENO);
                        mCommand.Parameters.Add("REQ_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("PROF_INFO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSPGETREQUESTS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["REQ_CURSOR"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.Requests> mDBReq = MyClass.ConvertToList<DBFactory.Entities.Requests>(dt);

                        OracleDataReader reader2 = ((OracleRefCursor)mCommand.Parameters["PROF_INFO"].Value).GetDataReader();
                        dt = new DataTable();
                        dt.Load(reader2);
                        var uentity = MyClass.ConvertToEntity<DBFactory.Entities.UserProfileEntity>(dt);

                        reader1.Close(); reader2.Close();

                        reader1.Close();
                        mConnection.Close();
                        uentity.Requests = mDBReq;

                        return uentity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }


        public SupportEntity GetSupportRequests_New(string UserId, string TabStatus, string SearchString, int V_PAGENO)
        {
            SupportEntity UChat = new SupportEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("TabStatus", TabStatus.ToString());
                        mCommand.Parameters.Add("V_CREATEDBY", UserId.ToUpper());
                        mCommand.Parameters.Add("SearchKey", SearchString.ToUpper());
                        mCommand.Parameters.Add("V_PAGENO", V_PAGENO);
                        mCommand.Parameters.Add("REQ_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("PROF_INFO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSPGETSPTREQUESTS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["REQ_CURSOR"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.Requests> mDBReq = MyClass.ConvertToList<DBFactory.Entities.Requests>(dt);

                        if (dt.Rows.Count > 0)
                        {
                            UChat = GetSupportChat(Convert.ToString(dt.Rows[0][0]));

                        }
                        OracleDataReader reader2 = ((OracleRefCursor)mCommand.Parameters["PROF_INFO"].Value).GetDataReader();
                        dt = new DataTable();
                        dt.Load(reader2);
                        var uentity = MyClass.ConvertToEntity<DBFactory.Entities.SupportEntity>(dt);

                        reader1.Close(); reader2.Close();

                        reader1.Close();
                        mConnection.Close();
                        uentity.Requests = mDBReq;
                        uentity.Chat = UChat.Chat;
                        return uentity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public void SaveTicketDocuemnts(string ReferenceID, string user, string reqSeqNo, byte[] fileBytes, string fileName, string message, string UPLOADEDFROM)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = "SELECT SSPCUSTSUPPORTREQDOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        int REQDOCID = Convert.ToInt32(mCommand.ExecuteScalar());

                        mCommand.CommandText = "SELECT REQID FROM SSPCUSTSUPPORTREQ WHERE SEQNO=:REQSEQNO";
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("REQSEQNO", reqSeqNo);
                        mCommand.ExecuteScalar();
                        int REQID = Convert.ToInt32(mCommand.ExecuteScalar());

                        mCommand.Parameters.Clear();
                        mCommand.CommandType = CommandType.Text;
                        mCommand.Parameters.Add("IP_REQDOCID", REQDOCID);
                        mCommand.Parameters.Add("IP_CREATEDBY", user);
                        mCommand.Parameters.Add("IP_REQSEQNO", reqSeqNo);
                        mCommand.Parameters.Add("IP_REQID", REQID);
                        mCommand.Parameters.Add("IP_USERID", user);
                        mCommand.Parameters.Add("IP_FILENAME", fileName);
                        mCommand.Parameters.Add("IP_FILECONTENT", fileBytes);
                        mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("DATEMODIFIED", DateTime.Now);
                        mCommand.Parameters.Add("IP_MESSAGE", message);
                        mCommand.Parameters.Add("IP_UPLOADEDFROM", UPLOADEDFROM);

                        mCommand.CommandText = @"INSERT INTO SSPCUSTSUPPORTREQDOCUMENTS (REQID,USERID,DATECREATED,DATEMODIFIED,CREATEDBY,SEQNO,FILECONTENT,DOCREQID,FILENAME,MESSAGE,UPLOADEDFROM) 
                                                values(:IP_REQID,:IP_CREATEDBY,:DATECREATED,:DATEMODIFIED,:IP_USERID,:IP_REQSEQNO,:IP_FILECONTENT,:IP_REQDOCID,:IP_FILENAME,:IP_MESSAGE,:IP_UPLOADEDFROM)";

                        mCommand.ExecuteNonQuery();

                        //Change the Status of the ticket -- Anil G
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = @"update SSPCUSTSUPPORTREQ set REQSTATUS='Submitted', DATEMODIFIED=:IN_DATEMODIFIED,MODIFIEDBY=:IN_USERID where REQID=:IN_REQID";
                        mCommand.Parameters.Add("IN_DATEMODIFIED", DateTime.Now);
                        mCommand.Parameters.Add("IN_USERID", DateTime.Now);
                        mCommand.Parameters.Add("IN_REQID", REQID);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public string SaveTicket(string Module, string Type, string ReferenceID, string QueryTEXT, string Description, string CREATEDBY, string ModuleCode)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = "SELECT SSPCUSTSUPPORTREQ_SEQ.NEXTVAL FROM DUAL";
                        int REQID = Convert.ToInt32(mCommand.ExecuteScalar());
                        string codes = REQID.ToString();
                        string code = "000000000000";
                        code = code.Substring(codes.Length);
                        string reqSeqNo = "SPF-" + ModuleCode + "-" + code + REQID;
                        mCommand.Parameters.Clear();
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.Parameters.Add("IP_SeqNo", Convert.ToString(reqSeqNo));
                        mCommand.Parameters.Add("IP_REQID", Convert.ToInt32(REQID));
                        mCommand.Parameters.Add("IN_Module", Convert.ToString(Module));
                        mCommand.Parameters.Add("IN_Type", Convert.ToString(Type));
                        mCommand.Parameters.Add("IN_ReferenceID", ReferenceID);
                        mCommand.Parameters.Add("IN_Query", "ShipAfreight - Customer Support Request (" + reqSeqNo + ")");
                        mCommand.Parameters.Add("IN_Description", QueryTEXT);
                        mCommand.Parameters.Add("IN_CREATEDBY", CREATEDBY);
                        mCommand.Parameters.Add("IN_ModuleCode", ModuleCode);
                        mCommand.CommandText = "USP_SSP_INSERTNEWREQUEST";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mCommand.CommandType = CommandType.Text;
                        mCommand.Parameters.Clear();

                        mConnection.Close();
                        return reqSeqNo;
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public DataSet GetbytesDownloadAttchment(int DOCREQID)
        {
            DataSet ds = new DataSet();
            string Query = @"SELECT FILECONTENT,FILENAME from SSPCUSTSUPPORTREQDOCUMENTS WHERE DOCREQID=:DOCREQID";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "DOCREQID", Value = DOCREQID } });
            ds.Tables.Add(dt);
            return ds;
        }

        public string GetSEQNObyREQID(string REQID)
        {
            string SEQNO = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT SEQNO FROM SSPCUSTSUPPORTREQ WHERE REQID = :REQID",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "REQID",Value = REQID},
                });
            return SEQNO;
        }

        public SupportEntity GetSupportChat(string REQID)
        {
            SupportEntity uentity = new SupportEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("V_REQID", REQID);
                        mCommand.Parameters.Add("CHAT_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "SSPGETCHATBYREQ";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CHAT_CURSOR"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.Chat> mDBReq = MyClass.ConvertToList<DBFactory.Entities.Chat>(dt);

                        reader1.Close();
                        mConnection.Close();
                        uentity.Chat = mDBReq;

                        return uentity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public void SaveOrCloseChat(string ChatMes, string REQID, string p, int flag)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = "SELECT SSPCUSTSUPPORTCOMM_SEQ.NEXTVAL FROM DUAL";
                        int COMMID = Convert.ToInt32(mCommand.ExecuteScalar());
                        mCommand.Parameters.Clear();
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.Parameters.Add("IN_COMMID", Convert.ToInt32(COMMID));
                        mCommand.Parameters.Add("IN_MESSAGE", Convert.ToString(ChatMes));
                        mCommand.Parameters.Add("IN_REQID", Convert.ToInt32(REQID));
                        mCommand.Parameters.Add("IN_USERID", p);
                        mCommand.Parameters.Add("FLAG", flag);
                        mCommand.Parameters.Add("IN_DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("IN_DATEMODIFIED", DateTime.Now);

                        mCommand.CommandText = "USP_SSP_NEWCHAT_CLOSEREQ";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        mCommand.CommandType = CommandType.Text;
                        mCommand.Parameters.Clear();

                        mConnection.Close();

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void CloseChart(string REQID, string p)
        {
            throw new NotImplementedException();
        }

        public ProfileInfo GetProfileInfo(string UserId)
        {
            ProfileInfo mEntity = new ProfileInfo();
            ReadText(DBConnectionMode.SSP, @"SELECT U.USERID,  U.EMAILID,U.FIRSTNAME, U.LASTNAME, U.COMPANYNAME,
                                            U.ISDCODE,TO_CHAR(U.MOBILENUMBER) Mobilenumber,U.COMPANYLINK, U.IMGFILENAME,U.IMGCONTENT,U.COUNTRYNAME,
                                            (SELECT case when COUNT(1)=0 then 'No' else 'Yes' End  FROM SSpCountrygroup sc where sc.COUNTRYID = CASE WHEN UMADR.COUNTRYID=0 THEN U.COUNTRYID ELSE UMADR.COUNTRYID END AND NPCCOUNTRY='Y') AS ISNPC,
                                            (select ISBUSINESSCREDIT from FOCIS.SSPRATEMARGINCONFIG SRC where src.COUNTRYID = CASE WHEN UMADR.COUNTRYID=0 THEN U.COUNTRYID ELSE UMADR.COUNTRYID END) AS BCOUNTRYSETUP
                                            FROM USERS U
                                            left join UMUSERADDRESSDETAILS UMADR on  UMADR.USERID=U.USERID
                                            WHERE LOWER(U.USERID) = :USERID AND U.ACCOUNTSTATUS=1",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<ProfileInfo>(r);
            });
            return mEntity;
        }

        public MBPROFILE GetMBUserProfile(dynamic UserId, int tab)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("MB_CREATEDBY", UserId); mCommand.Parameters.Add("V_TAB", tab);
                        mCommand.Parameters.Add("MB_CUR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSP_MB_PROFILE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        DataTable dt = new DataTable();
                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["MB_CUR"].Value).GetDataReader();
                        dt.Load(reader1);
                        var mEntity = MyClass.ConvertToEntity<DBFactory.Entities.MBPROFILE>(dt);
                        if (dt.Rows.Count > 0)
                        {
                            int creditcount = Convert.ToInt32(dt.Rows[0]["CREDITCOUNT"]);
                            int bookingcount = Convert.ToInt32(dt.Rows[0]["BOOKINGCOUNT"]);
                            if (creditcount > 0 || bookingcount > 0)
                            {
                                mEntity.ISCOUNTRYDISABLE = true;
                            }
                            else
                            {
                                mEntity.ISCOUNTRYDISABLE = false;
                            }
                        }
                        else
                        {
                            mEntity.ISCOUNTRYDISABLE = false;
                        }

                        reader1.Close();

                        mConnection.Close();
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public MBCREDIT GetMBCreditProfile(string UserId, int tab)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        MBCREDIT mEntity = new MBCREDIT();
                        string query = @"SELECT CASE WHEN COUNT(0) = 0 THEN 'Profile Incomplete' ELSE 'Profile Updated' End AS PROFILESTATUS FROM UMUSERADDRESSDETAILS
                                            WHERE  upper(USERID) = upper(:USERID)";
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = query;
                        mCommand.Parameters.Add("USERID", UserId);
                        mCommand.CommandType = CommandType.Text;
                        string PROFILESTATUS = Convert.ToString(mCommand.ExecuteScalar());
                        mCommand.Parameters.Clear();
                        if (PROFILESTATUS == "Profile Incomplete")
                        {
                            mEntity.PROFILESTATUS = PROFILESTATUS;
                            return mEntity;
                        }

                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("MB_CREATEDBY", UserId); mCommand.Parameters.Add("V_TAB", tab);
                        mCommand.Parameters.Add("MB_CUR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSP_MB_PROFILE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        DataTable dt = new DataTable();


                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["MB_CUR"].Value).GetDataReader();
                        dt.Load(reader1);

                        mEntity = MyClass.ConvertToEntity<DBFactory.Entities.MBCREDIT>(dt);
                        reader1.Close();

                        if (mEntity.USERID == null)
                        {
                            mEntity.PROFILESTATUS = PROFILESTATUS;
                            mEntity.USERID = UserId;
                        }
                        mConnection.Close();
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public MBCREDIT GetMBCreditProfileNew(string UserId, int tab)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        MBCREDIT mEntity = new MBCREDIT();
                        string query = @"SELECT CASE WHEN COUNT(0) = 0 THEN 'Profile Incomplete' ELSE 'Profile Updated' End AS PROFILESTATUS FROM UMUSERADDRESSDETAILS
                                            WHERE  upper(USERID) = upper(:USERID)";
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = query;
                        mCommand.Parameters.Add("USERID", UserId);
                        mCommand.CommandType = CommandType.Text;
                        string PROFILESTATUS = Convert.ToString(mCommand.ExecuteScalar());
                        mCommand.Parameters.Clear();
                        if (PROFILESTATUS == "Profile Incomplete")
                        {
                            mEntity.PROFILESTATUS = PROFILESTATUS;
                            return mEntity;
                        }

                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("MB_CREATEDBY", UserId); mCommand.Parameters.Add("V_TAB", tab);
                        mCommand.Parameters.Add("MB_CUR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_CREDITDOC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_CREDITCOMMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSP_MB_PROFILE_NEW";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        DataTable dt = new DataTable();

                        if (tab == 2)
                        {
                            DataTable dtprofile = new DataTable();
                            OracleDataReader profilereader = ((OracleRefCursor)mCommand.Parameters["MB_CUR"].Value).GetDataReader();
                            dtprofile.Load(profilereader);
                            mEntity = MyClass.ConvertToEntity<DBFactory.Entities.MBCREDIT>(dtprofile);
                            profilereader.Close();

                            DataTable dtCreditDoc = new DataTable();
                            OracleDataReader creditreader = ((OracleRefCursor)mCommand.Parameters["CUR_CREDITDOC"].Value).GetDataReader();
                            dtCreditDoc.Load(creditreader);
                            List<DBFactory.Entities.CreditDocEntity> creditdocEntity = MyClass.ConvertToList<DBFactory.Entities.CreditDocEntity>(dtCreditDoc);
                            mEntity.CreditDoc = creditdocEntity;
                            creditreader.Close();

                            DataTable dtCreditComments = new DataTable();
                            OracleDataReader creditcommentreader = ((OracleRefCursor)mCommand.Parameters["CUR_CREDITCOMMENTS"].Value).GetDataReader();
                            dtCreditComments.Load(creditcommentreader);
                            List<DBFactory.Entities.CreditCommentEntity> creditcommentdocEntity = MyClass.ConvertToList<DBFactory.Entities.CreditCommentEntity>(dtCreditComments);
                            mEntity.CreditComments = creditcommentdocEntity;
                            creditcommentreader.Close();

                        }

                        if (mEntity.USERID == null)
                        {
                            mEntity.PROFILESTATUS = PROFILESTATUS;
                            mEntity.USERID = UserId;
                        }
                        mConnection.Close();
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public void ProcessCredit(decimal Crdlimit, string Status, string Userid)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = @"UPDATE SSPCREDITAPPLICATIONS SET APPROVEDCREDITLIMIT=:APPROVEDCREDITLIMIT,CURRENTOVERDUEBALANCE=:CURRENTOVERDUEBALANCE,
                        STATEID=:STATEID,DATEMODIFIED=:DATEMODIFIED where LOWER(USERID)=:USERID";
                        mCommand.Parameters.Add("APPROVEDCREDITLIMIT", Crdlimit);
                        mCommand.Parameters.Add("CURRENTOVERDUEBALANCE", Crdlimit);
                        mCommand.Parameters.Add("STATEID", Status);
                        mCommand.Parameters.Add("USERID", Userid.ToLower());
                        mCommand.Parameters.Add("DATEMODIFIED", DateTime.Now);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public ProfileImage GetOrSaveImage(dynamic UserId, int Status, byte[] image)
        {
            ProfileImage mEntity = new ProfileImage();
            if (Status == 1)
            {
                ReadText(DBConnectionMode.SSP, @"SELECT  USERID,FIRSTNAME,IMGFILENAME,IMGCONTENT,PREFERREDCURRENCY, LENGTHUNIT, WEIGHTUNIT, PREFERREDCURRENCYCODE FROM USERS WHERE LOWER(USERID) = :USERID",
                    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = UserId.ToLower() } }
                , delegate(IDataReader r)
                {
                    mEntity = DataReaderMapToObject<ProfileImage>(r);
                });
                return mEntity;
            }
            else
            {
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;
                        try
                        {
                            mCommand.CommandText = @"UPDATE USERS SET IMGCONTENT=:IMGCONTENT where USERID=:USERID";
                            mCommand.Parameters.Add("IMGCONTENT", image);
                            mCommand.Parameters.Add("USERID", UserId);
                            mCommand.Parameters.Add("DATEMODIFIED", DateTime.Now);
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            mOracleTransaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            return mEntity;
        }

        public int GetCountryIDBasedonCode(string CCode)
        {
            OracleConnection mFocisConnection;
            OracleTransaction mfocisOracleTransaction = null;
            using (mFocisConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mFocisConnection.State == ConnectionState.Closed) mFocisConnection.Open();
                mfocisOracleTransaction = mFocisConnection.BeginTransaction();
                using (OracleCommand mCommand = mFocisConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mfocisOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT COUNTRYID FROM SMDM_COUNTRY WHERE UPPER(CODE)=UPPER(:CODE)";
                        mCommand.Parameters.Add("CODE", CCode.ToUpper());
                        int COUNTRYID = Convert.ToInt32(mCommand.ExecuteScalar());
                        return COUNTRYID;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
        public void UpdateUserToken(string token, DateTime tokenexpiry, string userid)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = @"UPDATE USERS SET TOKEN=:TOKEN ,TOKENEXPIRY=:TOKENEXPIRY where LOWER(USERID)=:USERID";
                        mCommand.Parameters.Add("TOKEN", token);
                        mCommand.Parameters.Add("TOKENEXPIRY", tokenexpiry);
                        mCommand.Parameters.Add("USERID", userid.ToLower());
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void UpdateImage(dynamic userid, byte[] bytes)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = @"UPDATE USERS SET IMGCONTENT=:IMGCONTENT where LOWER(USERID)=:USERID";
                        mCommand.Parameters.Add("IMGCONTENT", bytes);
                        mCommand.Parameters.Add("USERID", userid.ToLower());
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public UserPartyMaster GetUserPersonalParty(string Createdby)
        {
            UserPartyMaster mEntity = new UserPartyMaster();
            ReadText(DBConnectionMode.SSP, @"SELECT *
                                            FROM SSPPARTYMASTER WHERE lower(Createdby) = :Createdby and UPERSONAL = :UPERSONAL  ",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Int64, ParameterName = "UPERSONAL", Value = 1 }, 
                                        new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "Createdby", Value = Createdby.ToLower() }}
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<UserPartyMaster>(r);
            });
            return mEntity;
        }
        public long SaveDocumentsWithParams(SSPDocumentsEntity entity, string UserName, string Source, string DocDelete = "NO")
        {

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        entity.DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = @"Insert into SSPDOCUMENTS (DOCID,JOBNUMBER,QUOTATIONID,DOCUMNETTYPE,FILENAME,FILEEXTENSION,FILESIZE,FILECONTENT,CREATEDBY,
                        DOCUMENTNAME,PAGEFLAG,SOURCE,DOCDELETE,DIRECTION,CONDMAND,DOCUMENTTEMPLATE,COMMENTID) values (:DOCID,:JOBNUMBER,:QUOTATIONID,:DOCUMNETTYPE,:FILENAME,:FILEEXTENSION,:FILESIZE,:FILECONTENT,:CREATEDBY,
                        :DOCUMENTNAME,:PAGEFLAG,:SOURCE,:DOCDELETE,:DIRECTION,:CONDMAND,:DOCUMENTTEMPLATE,:COMMENTID)";
                        mCommand.Parameters.Add("DOCID", entity.DOCID);

                        mCommand.Parameters.Add("CREATEDBY", UserName);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("DOCUMNETTYPE", entity.DOCUMNETTYPE);
                        mCommand.Parameters.Add("FILENAME", entity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", entity.FILEEXTENSION);
                        mCommand.Parameters.Add("FILESIZE", entity.FILESIZE);
                        mCommand.Parameters.Add("FILECONTENT", entity.FILECONTENT);
                        mCommand.Parameters.Add("DOCUMENTNAME", entity.DocumentName);
                        mCommand.Parameters.Add("PAGEFLAG", 5);
                        mCommand.Parameters.Add("SOURCE", Source);
                        mCommand.Parameters.Add("DOCDELETE", DocDelete);
                        mCommand.Parameters.Add("DIRECTION", entity.DIRECTION);
                        mCommand.Parameters.Add("CONDMAND", entity.CONDMAND);
                        mCommand.Parameters.Add("COMMENTID", entity.COMMENTID);
                        mCommand.Parameters.Add("DOCUMENTTEMPLATE", entity.DOCUMENTTEMPLATE);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return Convert.ToInt32(entity.DOCID);

        }
        public long AddCreditSaveDocumentsWithParams(SSPDocumentsEntity entity, string UserName, string Source, string DocDelete = "NO")
        {

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        entity.DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = @"Insert into SSPDOCUMENTS (DOCID,JOBNUMBER,QUOTATIONID,DOCUMNETTYPE,FILENAME,FILEEXTENSION,FILESIZE,FILECONTENT,CREATEDBY,
                        DOCUMENTNAME,PAGEFLAG,SOURCE,DOCDELETE,DIRECTION,CONDMAND,DOCUMENTTEMPLATE,COMMENTID) values (:DOCID,:JOBNUMBER,:QUOTATIONID,:DOCUMNETTYPE,:FILENAME,:FILEEXTENSION,:FILESIZE,:FILECONTENT,:CREATEDBY,
                        :DOCUMENTNAME,:PAGEFLAG,:SOURCE,:DOCDELETE,:DIRECTION,:CONDMAND,:DOCUMENTTEMPLATE,:COMMENTID)";
                        mCommand.Parameters.Add("DOCID", entity.DOCID);

                        mCommand.Parameters.Add("CREATEDBY", UserName);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("DOCUMNETTYPE", entity.DOCUMNETTYPE);
                        mCommand.Parameters.Add("FILENAME", entity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", entity.FILEEXTENSION);
                        mCommand.Parameters.Add("FILESIZE", entity.FILESIZE);
                        mCommand.Parameters.Add("FILECONTENT", entity.FILECONTENT);
                        mCommand.Parameters.Add("DOCUMENTNAME", entity.DocumentName);
                        mCommand.Parameters.Add("PAGEFLAG", 7);
                        mCommand.Parameters.Add("SOURCE", Source);
                        mCommand.Parameters.Add("DOCDELETE", DocDelete);
                        mCommand.Parameters.Add("DIRECTION", entity.DIRECTION);
                        mCommand.Parameters.Add("CONDMAND", entity.CONDMAND);
                        mCommand.Parameters.Add("COMMENTID", entity.COMMENTID);
                        mCommand.Parameters.Add("DOCUMENTTEMPLATE", entity.DOCUMENTTEMPLATE);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return Convert.ToInt32(entity.DOCID);

        }
        public IList<SSPDocumentsEntity> GetCreditDocument(string userid, string filename)
        {
            List<SSPDocumentsEntity> bookingdetails = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE Pageflag=5 and createdby = :USERID and FileName= :FileName ",
              new OracleParameter[] { new OracleParameter { ParameterName="USERID", Value= userid },new OracleParameter { ParameterName="FileName", Value= filename }
              }
            , delegate(IDataReader r)
            {
                bookingdetails.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            return bookingdetails;
        }
        public IList<SSPDocumentsEntity> GetAddCreditDocument(string userid, string filename)
        {
            List<SSPDocumentsEntity> bookingdetails = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE Pageflag=7 and createdby = :USERID and FileName= :FileName ",
              new OracleParameter[] { new OracleParameter { ParameterName="USERID", Value= userid },new OracleParameter { ParameterName="FileName", Value= filename }
              }
            , delegate(IDataReader r)
            {
                bookingdetails.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            return bookingdetails;
        }
        public void DeleteAddCreditDocument(string userid, int docid)
        {

            ReadText(DBConnectionMode.SSP, "Delete SSPDOCUMENTS WHERE Pageflag=7 and createdby = :USERID and DOCID=:DOCID ",
                 new OracleParameter[] { new OracleParameter { ParameterName="USERID", Value= userid },new OracleParameter { ParameterName="DOCID", Value= docid }
              }
               , delegate(IDataReader r)
               {
               });


        }
        public void DeleteCreditDocument(string userid, int docid)
        {

            ReadText(DBConnectionMode.SSP, "Delete SSPDOCUMENTS WHERE Pageflag=5 and createdby = :USERID and DOCID=:DOCID ",
                 new OracleParameter[] { new OracleParameter { ParameterName="USERID", Value= userid },new OracleParameter { ParameterName="DOCID", Value= docid }
              }
               , delegate(IDataReader r)
               {
               });


        }

        public void SaveCreditComments(CreditCommentEntity creditcomment)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("ADDITIONALCREDITID", creditcomment.ADDITIONALCREDITID);
                        mCommand.Parameters.Add("CREDITAPPID", creditcomment.CREDITAPPID);
                        mCommand.Parameters.Add("COMMENTS", creditcomment.COMMENTS);
                        mCommand.Parameters.Add("CREATEDBY", creditcomment.CREATEDBY);
                        mCommand.Parameters.Add("COMMENTSFROM", creditcomment.COMMENTSFROM);
                        mCommand.CommandText = "SSPINSERTCREDITCOMMENTS";
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        mCommand.CommandType = CommandType.Text;
                        mCommand.Parameters.Clear();

                        mConnection.Close();

                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public int GetDecimalPointByCurrencyCode(string currencyCode)
        {
            int decimalPoint;
            if (currencyCode != null)
            {
                decimalPoint = this.ExecuteScalar<int>(DBConnectionMode.SSP, "select decimals from CURRENCIES where currencycode=:currencyCode",
                  new OracleParameter[]{
                    new OracleParameter{ ParameterName = "currencyCode",Value = currencyCode.ToUpper()},
                });
            }
            else
            {
                decimalPoint = 0;
            }
            return decimalPoint;
        }

        public List<SSPMOBILEVERSIONSENTITY> GETAPPVERSIONS()
        {
            List<SSPMOBILEVERSIONSENTITY> mEntity = new List<SSPMOBILEVERSIONSENTITY>();
            ReadText(DBConnectionMode.SSP, "select SSPMOBILEVERSIONSID,OS,APPVERSION,RECOMMENDUPGRADE,FORCEUPGRADE,DESCRIPTION,DATETIME from SSPMOBILEVERSIONS",
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<SSPMOBILEVERSIONSENTITY>(r));
            });
            return mEntity;
        }

        public List<SspMobileMarketingBanner> GetMarketingBanner()
        {
            List<SspMobileMarketingBanner> mEntity = new List<SspMobileMarketingBanner>();
            ReadText(DBConnectionMode.SSP, @"
                                                SELECT * FROM (select *
                                                from SSPMOBILEMARKETINGBANNER 
                                                order by ID desc
                                                )WHERE ROWNUM <= 5
                                            ",
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<SspMobileMarketingBanner>(r));
            });
            return mEntity;
        }

        public void UpdateUserforUnsubscribefromEmailNotifications(string userId)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE USERS SET NOTIFICATIONSUBSCRIPTION=0 WHERE lower(USERID)=:USERID",
                new OracleParameter[] { 
                new OracleParameter{ ParameterName = "USERID",Value = userId.ToLower()},
                });
        }

        public ResultsEntity<CustomerSupportEntity> GetSupportCategories(Int32 CategoryID, string uiCulture)
        {
            ResultsEntity<CustomerSupportEntity> mResult = new ResultsEntity<CustomerSupportEntity>();
            List<CustomerSupportEntity> mList = new List<CustomerSupportEntity>();
            if (uiCulture == "en")
            {
                if (CategoryID == 0)
                {
                    ReadText(DBConnectionMode.SSP, "SELECT CATEGORYID,CODE,NAME from SSPCUSTSUPPORTCATEGOREIS where ACTIVE=1",
                           new OracleParameter[] { }
                           , delegate(IDataReader r)
                       {
                           mList.Add(DataReaderMapToObject<CustomerSupportEntity>(r));
                       });
                }
                else
                {
                    ReadText(DBConnectionMode.SSP, "SELECT CATEGORYID,SUBCATEGORYID,CODE,NAME from SSPCUSTSUPPORTSUBCATEGOREIS where CATEGORYID=:CategoryID",
                           new OracleParameter[] { new OracleParameter { ParameterName = "CATEGORYID", Value = CategoryID } }
                           , delegate(IDataReader r)
                           {
                               mList.Add(DataReaderMapToObject<CustomerSupportEntity>(r));
                           });
                }
            }
            else
            {
                if (CategoryID == 0)
                {
                    string query = string.Empty;

                    query = @"SELECT LANGUAGECATEGORYID,CODE,LANGUAGECATEGORYNAME from SSPCUSTSUPCATEGORIES_LANGUAGE  where LANGUAGETYPE='" + uiCulture + "'";

                    DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter {
                            
                    } 
                });
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CustomerSupportEntity objCustSupport = new CustomerSupportEntity();
                            objCustSupport.CATEGORYID = Convert.ToInt32(dt.Rows[i]["LANGUAGECATEGORYID"].ToString());
                            objCustSupport.CODE = dt.Rows[i]["CODE"].ToString();
                            objCustSupport.NAME = dt.Rows[i]["LANGUAGECATEGORYNAME"].ToString();
                            mList.Add(objCustSupport);
                        }
                    }
                }
                else
                {
                    string query = string.Empty;

                    query = @"SELECT LANGUAGECATEGORYID, LANGUAGESUBCATEGORYID,LANGUAGESUBCATEGORYNAME from SSPCUSTSUBCATEGORIES_LANGUAGE  where LANGUAGETYPE='" + uiCulture + "' AND LANGUAGECATEGORYID=" + CategoryID + "";

                    DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter {
                            
                    } 
                });
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CustomerSupportEntity objCustSupport = new CustomerSupportEntity();
                            objCustSupport.CATEGORYID = Convert.ToInt32(dt.Rows[i]["LANGUAGECATEGORYID"].ToString());
                            objCustSupport.SUBCATEGORYID = Convert.ToInt32(dt.Rows[i]["LANGUAGESUBCATEGORYID"].ToString());
                            objCustSupport.NAME = dt.Rows[i]["LANGUAGESUBCATEGORYNAME"].ToString();
                            mList.Add(objCustSupport);
                        }
                    }
                }

            }
            mResult.Items = mList;
            return mResult;
        }


        public ResultsEntity<CustomerSupportEntity> GetSupportCategories(Int32 CategoryID)
        {
            ResultsEntity<CustomerSupportEntity> mResult = new ResultsEntity<CustomerSupportEntity>();
            List<CustomerSupportEntity> mList = new List<CustomerSupportEntity>();

            if (CategoryID == 0)
            {
                ReadText(DBConnectionMode.SSP, "SELECT CATEGORYID,CODE,NAME from SSPCUSTSUPPORTCATEGOREIS where ACTIVE=1",
                       new OracleParameter[] { }
                       , delegate(IDataReader r)
                       {
                           mList.Add(DataReaderMapToObject<CustomerSupportEntity>(r));
                       });
            }
            else
            {
                ReadText(DBConnectionMode.SSP, "SELECT CATEGORYID,SUBCATEGORYID,CODE,NAME from SSPCUSTSUPPORTSUBCATEGOREIS where CATEGORYID=:CategoryID",
                       new OracleParameter[] { new OracleParameter { ParameterName = "CATEGORYID", Value = CategoryID } }
                       , delegate(IDataReader r)
                       {
                           mList.Add(DataReaderMapToObject<CustomerSupportEntity>(r));
                       });
            }
            mResult.Items = mList;
            return mResult;
        }

        public int getJobBookingCount(string userid)
        {
            int bookingCount;
            if (userid != null)
            {
                bookingCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "select count(CREDITAPPID) from sspcreditapplications where userid=:userid",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "userid",Value = userid},
                });
                if (bookingCount == 0)
                {
                    bookingCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "select Count(JOBNUMBER) from SSPJOBDETAILS where CREATEDBY=:userid",
                      new OracleParameter[]{
                        new OracleParameter{ ParameterName = "userid",Value = userid},
                    });
                }
            }
            else
            {
                bookingCount = 0;
            }
            return bookingCount;
        }
        public void SaveUserCountry(string p, string server, string userid)
        {
            //

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.CommandText = "SELECT SSPUSERCOUNTRYHISTORY_SEQ.NEXTVAL FROM DUAL";
                    Int64 id = Convert.ToInt64(mCommand.ExecuteScalar());

                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "INSERT INTO SSPUSERCOUNTRYHISTORY (ID,COUNTRYCODE,CREATEDDATE,SERVER,USERID)VALUES(:ID,:COUNTRYCODE,:CREATEDDATE,:SERVER,:USERID)";
                        mCommand.Parameters.Add("ID", id);
                        mCommand.Parameters.Add("COUNTRYCODE", p);
                        mCommand.Parameters.Add("CREATEDDATE", DateTime.Now);
                        mCommand.Parameters.Add("SERVER", server);
                        mCommand.Parameters.Add("USERID", userid);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }

            }


        }

        public dynamic GetQuotationCount(string UserID)
        {
            int QuotationCount;
            if (UserID != null)
            {
                QuotationCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, @"SELECT count(0) FROM QMQUOTATION where
                    (CREATEDBY=LOWER(:UserID) OR GUESTEMAIL=LOWER(:UserID)) AND DATEOFVALIDITY >=" + "'" + DateTime.Now.ToString("dd-MMM-yy") + "'",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "CREATEDBY",Value = UserID.ToLower()},
                });

            }
            else
            {
                QuotationCount = 0;
            }
            return QuotationCount;
        }

        public List<UserPartyMaster> GetPartyMasterList_MB(string Name, string Searchstring, int CountryId, string TabName)
        {
            if (CountryId == 0 && (TabName.ToUpper() == "PROFILE" || TabName == ""))
            {
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;

                using (mConnection = new OracleConnection(SSPConnectionString))
                {

                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            mCommand.Parameters.Clear();

                            mCommand.Parameters.Add("Loc_Createdby", Name.ToUpper());
                            mCommand.Parameters.Add("SearchKey", Searchstring.ToUpper());
                            mCommand.Parameters.Add("CUR_FPARTYS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("PROF_INFO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                            mCommand.CommandText = "SSPGETPARTYMASTER";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();

                            OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_FPARTYS"].Value).GetDataReader();
                            DataTable dt = new DataTable();
                            dt.Load(reader1);
                            List<DBFactory.Entities.UserPartyMaster> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.UserPartyMaster>(dt);

                            reader1.Close();
                            mConnection.Close();

                            return mDBEntity;
                        }
                        catch (Exception ex)
                        {
                            mOracleTransaction.Rollback();
                            throw new ShipaDbException(ex.ToString());
                        }
                    }
                }
            }
            else
            {
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;
                        try
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("Loc_Createdby", Name.ToUpper());
                            mCommand.Parameters.Add("SearchKey", Searchstring.ToUpper());
                            mCommand.Parameters.Add("PRCountryId", CountryId);
                            mCommand.Parameters.Add("CUR_FPARTYS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_SSP_JOB_GETPARTYMASTER1";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();

                            OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_FPARTYS"].Value).GetDataReader();
                            DataTable dt = new DataTable();
                            dt.Load(reader1);
                            List<DBFactory.Entities.UserPartyMaster> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.UserPartyMaster>(dt);
                            reader1.Close();
                            mConnection.Close();
                            return mDBEntity;
                        }
                        catch (Exception ex)
                        {
                            mOracleTransaction.Rollback();
                            throw new ShipaDbException(ex.ToString());
                        }
                    }
                }
            }
        }
    }
}
