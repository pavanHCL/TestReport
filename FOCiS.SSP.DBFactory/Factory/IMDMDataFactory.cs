﻿using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCiS.SSP.DBFactory.Factory
{
    public interface IMDMDataFactory
    {
        PagedResultsEntity<Select2Entity<string>> FindCurrency(int pageNumber, int pageSize, string s);
        PagedResultsEntity<Select3Entity<Int64>> FindCountry(string s, string CountryId);
        Task<ResultsEntity<zipcodeEntity>> GetZipcodesByCity(string Cityid, string CountryCode);
        ResultsEntity<zipcodeEntityUnloc> GetCitiesByZipCode(string Zip, string CountryCode);
        ResultsEntity<zipcodeEntity> IsZipcodesAvailable(string CountryCode);
        List<CityEntity> GetCitiesByCountry(int CountryID);

    }
}
