﻿using System;
using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;


namespace FOCiS.SSP.DBFactory.Factory
{
    public class PaymentDBFactory : DBFactoryCore
    {
        public List<PaymentChargesEntity> GetPaymentCharges(int id)
        {
            List<PaymentChargesEntity> mEntity = new List<PaymentChargesEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT JAH.JOBNUMBER,JAC.CHARGENAME,JAC.AMOUNT,JAC.TAXAMOUNT,JAC.NETAMOUNT " +
                                           " FROM JARECEIPTCHARGEDETAILS JAC " +
                                           " INNER JOIN JARECEIPTHEADER JAH ON JAC.RECEIPTHDRID = JAH.RECEIPTHDRID " +
                                           " WHERE JAH.JOBNUMBER = :JOBNUMBER",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = id } }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<PaymentChargesEntity>(r));
            });
            return mEntity;
        }

        public PaymentBasicDetailsEntity GetPaymentBasicDetailsByJobId(int JobNumberId)
        {
            PaymentBasicDetailsEntity ePayHdrDtls = new PaymentBasicDetailsEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM JARECEIPTHEADER WHERE JOBNUMBER = :JOBNUMBER",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JobNumberId } }
            , delegate(IDataReader r)
            {
                ePayHdrDtls = DataReaderMapToObject<PaymentBasicDetailsEntity>(r);
            });

            List<PaymentBasicChargeDetailsEntity> eChargeDetails = new List<PaymentBasicChargeDetailsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM JARECEIPTCHARGEDETAILS WHERE RECEIPTHDRID = :RECEIPTHDRID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "RECEIPTHDRID", Value = ePayHdrDtls.RECEIPTHDRID } }
            , delegate(IDataReader r)
            {
                eChargeDetails.Add(DataReaderMapToObject<PaymentBasicChargeDetailsEntity>(r));
            });
            ePayHdrDtls.BasicChargeDetails = eChargeDetails;
            return ePayHdrDtls;
        }

    }
}