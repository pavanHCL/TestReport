﻿using FOCiS.SSP.DBFactory.Entities;
using FOCiS.SSP.DBFactory.Core;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace FOCiS.SSP.DBFactory.Factory
{
    public class ActiveCampaignDBFactory : DBFactoryCore
    {
        OracleConnection mConnection;
        OracleTransaction mOracleTransaction = null;
        public int SaveList(ActiveCampaignListEntity entity)
        {
            int status = 0;
          
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT SSP_ACTIVECAMPAIGN_SEQ.NEXTVAL FROM DUAL";
                        entity.ID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = "INSERT INTO ACTIVECAMPAIGNLISTADD (ID,LISTID,RESULTCODE,RESULTMESSAGE,RESULTOUTPUT,DATECREATED,LISTTYPE) VALUES (:ID,:LISTID,:RESULTCODE,:RESULTMESSAGE,:RESULTOUTPUT,:DATECREATED,:LISTTYPE)";
                        mCommand.Parameters.Add("ID", entity.ID);
                        mCommand.Parameters.Add("LISTID", entity.ListId);
                        mCommand.Parameters.Add("RESULTCODE", entity.ResultCode);
                        mCommand.Parameters.Add("RESULTMESSAGE", entity.ResultMessage);
                        mCommand.Parameters.Add("RESULTOUTPUT", entity.ResultOutPut);
                        mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("LISTTYPE", entity.ListType);
                        mCommand.BindByName = true;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        status = 1;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        status = 0;
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return status;
        }

        public int SaveContactInList(ActiveCampaignContactEntity entity,string QueryType)
        {
            int status = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT SSP_ACTIVECAMPAIGNCONTACT_SEQ.NEXTVAL FROM DUAL";
                        entity.ContactId = Convert.ToInt64(mCommand.ExecuteScalar());
                        if (QueryType == "Insert")
                        {
                            mCommand.CommandText = "INSERT INTO ACTIVECAMPAIGNCONTACT (CONTACTID,SUBSCRIBERID,SENDERLASTSHOULD,SENDLASTDID,RESULTCODE,RESULTMESSAGE,RESULTOUTPUT,DATECREATED,CONTACTEMAILID,USERTYPE) VALUES (:CONTACTID,:SUBSCRIBERID,:SENDERLASTSHOULD,:SENDLASTDID,:RESULTCODE,:RESULTMESSAGE,:RESULTOUTPUT,:DATECREATED,:CONTACTEMAILID,:USERTYPE)";
                            mCommand.Parameters.Add("CONTACTID", entity.ContactId);
                            mCommand.Parameters.Add("SUBSCRIBERID", entity.SubscriberId);
                            mCommand.Parameters.Add("SENDERLASTSHOULD", entity.SenderLastShould);
                            mCommand.Parameters.Add("SENDLASTDID", entity.SendLastdid);
                            mCommand.Parameters.Add("RESULTCODE", entity.ResultCode);
                            mCommand.Parameters.Add("RESULTMESSAGE", entity.ResultMessage);
                            mCommand.Parameters.Add("RESULTOUTPUT", entity.ResultOutPut);
                            mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                            mCommand.Parameters.Add("CONTACTEMAILID", entity.ContactEmailId);
                            mCommand.Parameters.Add("USERTYPE", entity.UserType);
                            mCommand.BindByName = true;
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                            status = 1;
                        }
                        if (QueryType == "Update")
                        {
                            mCommand.CommandText = "UPDATE ACTIVECAMPAIGNCONTACT SET SENDERLASTSHOULD=:SENDERLASTSHOULD,SENDLASTDID=:SENDLASTDID,RESULTCODE=:RESULTCODE,RESULTMESSAGE=:RESULTMESSAGE,RESULTOUTPUT=:RESULTOUTPUT,DATEMODIFIED=:DATEMODIFIED,CONTACTEMAILID=:CONTACTEMAILID,USERTYPE=:USERTYPE WHERE SUBSCRIBERID=:SUBSCRIBERID";
                            mCommand.Parameters.Add("SUBSCRIBERID", entity.SubscriberId);
                            mCommand.Parameters.Add("SENDERLASTSHOULD", entity.SenderLastShould);
                            mCommand.Parameters.Add("SENDLASTDID", entity.SendLastdid);
                            mCommand.Parameters.Add("RESULTCODE", entity.ResultCode);
                            mCommand.Parameters.Add("RESULTMESSAGE", entity.ResultMessage);
                            mCommand.Parameters.Add("RESULTOUTPUT", entity.ResultOutPut);
                            mCommand.Parameters.Add("DATEMODIFIED", DateTime.Now);
                            mCommand.Parameters.Add("CONTACTEMAILID", entity.ContactEmailId);
                            mCommand.Parameters.Add("USERTYPE", entity.UserType);
                            mCommand.BindByName = true;
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                            status = 1;
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        status = 0;
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return status;
        }

        public ActiveCampaignListEntity GetListType(string LISTTYPE)
        {
            ActiveCampaignListEntity mEntity = new ActiveCampaignListEntity();
            try
            {
                string query = "SELECT LISTID,LISTTYPE FROM  ACTIVECAMPAIGNLISTADD WHERE LISTTYPE= :LISTTYPE ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter { ParameterName="LISTTYPE", Value= LISTTYPE} 
                });

                if (dt.Rows.Count > 0)
                {
                    mEntity.ListId = Convert.ToInt32(dt.Rows[0]["LISTID"]);
                    mEntity.ListType = dt.Rows[0]["LISTTYPE"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            } 
            return mEntity;
        }

        public List<ActiveCampaignContactEntity> GetUsersData(string UserType)
        {
            List<ActiveCampaignContactEntity> objlist = new List<ActiveCampaignContactEntity>();
            try
            {
                string query = "SELECT SUBSCRIBERID,CONTACTEMAILID FROM  ACTIVECAMPAIGNCONTACT WHERE UPPER(USERTYPE) = :USERTYPE ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter { ParameterName="USERTYPE", Value= UserType.ToUpper() } 
                });
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ActiveCampaignContactEntity mEntity = new ActiveCampaignContactEntity();
                        mEntity.SubscriberId = Convert.ToInt32(dt.Rows[i]["SUBSCRIBERID"]);
                        mEntity.ContactEmailId = dt.Rows[i]["CONTACTEMAILID"].ToString();
                        objlist.Add(mEntity);
                    }
                }

               
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
            return objlist;
        }

        public ActiveCampaignContactEntity GetContactEmailId(string EmailId,string UserType)
        {
            ActiveCampaignContactEntity mEntity = new ActiveCampaignContactEntity();
            try
            {
                string query = "SELECT SUBSCRIBERID,CONTACTEMAILID FROM  ACTIVECAMPAIGNCONTACT WHERE UPPER(USERTYPE) = :USERTYPE and UPPER(CONTACTEMAILID)= :CONTACTEMAILID ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter { ParameterName="USERTYPE", Value= UserType.ToUpper() } ,
                    new OracleParameter { ParameterName="CONTACTEMAILID", Value= EmailId.ToUpper() }
                });
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                     
                        mEntity.SubscriberId = Convert.ToInt32(dt.Rows[i]["SUBSCRIBERID"]);
                        mEntity.ContactEmailId = dt.Rows[i]["CONTACTEMAILID"].ToString();
                        
                    }
                }


            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
            return mEntity;
        }

        public List<ActiveCampaignContactEntity> GetActiveContacts()
        {
            List<ActiveCampaignContactEntity> objlist = new List<ActiveCampaignContactEntity>();
            try
            {
                string query = "SELECT SUBSCRIBERID,CONTACTEMAILID FROM  ACTIVECAMPAIGNCONTACT";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { new OracleParameter { } });
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ActiveCampaignContactEntity mEntity = new ActiveCampaignContactEntity();
                        mEntity.SubscriberId = Convert.ToInt32(dt.Rows[i]["SUBSCRIBERID"]);
                        mEntity.ContactEmailId = dt.Rows[i]["CONTACTEMAILID"].ToString();
                        objlist.Add(mEntity);
                    }
                  
                }
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
            return objlist;
        }

        public ActiveCampaignContactEntity GetActiveContactsByEmailId(string EmailId)
        {
            ActiveCampaignContactEntity mEntity = new ActiveCampaignContactEntity();
            try
            {
                string query = "SELECT SUBSCRIBERID,CONTACTEMAILID FROM  ACTIVECAMPAIGNCONTACT WHERE UPPER(CONTACTEMAILID)= :CONTACTEMAILID ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                      new OracleParameter { ParameterName="CONTACTEMAILID", Value= EmailId.ToUpper() }
                });
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        mEntity.SubscriberId = Convert.ToInt32(dt.Rows[i]["SUBSCRIBERID"]);
                        mEntity.ContactEmailId = dt.Rows[i]["CONTACTEMAILID"].ToString();   
                    }

                }
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
            return mEntity;
        }

        public List<UserEntity> GetGuestUsersData()
        {
            List<UserEntity> objlist = new List<UserEntity>();
            try
            {
                string query = "select * from users where accountstatus=0";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { new OracleParameter { } });
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserEntity mEntity = new UserEntity();
                        mEntity.USERID = dt.Rows[i]["USERID"].ToString();
                        mEntity.FIRSTNAME = dt.Rows[i]["FIRSTNAME"].ToString();
                        mEntity.LASTNAME = dt.Rows[i]["LASTNAME"].ToString();
                        mEntity.COMPANYNAME = dt.Rows[i]["COMPANYNAME"].ToString();
                        mEntity.MOBILENUMBER = dt.Rows[i]["MOBILENUMBER"].ToString();
                        objlist.Add(mEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
            return objlist;
        }

        public List<UserEntity> GetRegisterUsersData()
        {
            List<UserEntity> objlist = new List<UserEntity>();
            try
            {
                string query = "select * from users where accountstatus=1 and notificationsubscription=1";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { new OracleParameter { } });

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserEntity mEntity = new UserEntity();
                        mEntity.USERID = dt.Rows[i]["USERID"].ToString();
                        mEntity.FIRSTNAME = dt.Rows[i]["FIRSTNAME"].ToString();
                        mEntity.LASTNAME = dt.Rows[i]["LASTNAME"].ToString();
                        mEntity.COMPANYNAME = dt.Rows[i]["COMPANYNAME"].ToString();
                        mEntity.MOBILENUMBER = dt.Rows[i]["MOBILENUMBER"].ToString();
                        objlist.Add(mEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
            return objlist;
        }

        public int DeleteActiveContact(int ActiveContactId)
        {
            int contactId = 0;
            try
            {
                string query = "DELETE FROM ACTIVECAMPAIGNCONTACT WHERE SUBSCRIBERID= :SUBSCRIBERID ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter { ParameterName="SUBSCRIBERID", Value= ActiveContactId} 
                });
                contactId = 1;
            }
            catch (Exception ex)
            {
                contactId = 0;
                throw new ShipaDbException(ex.ToString());
            }
            return contactId;
        }

        public int DeleteActiveList(int ActiveListId)
        {
            int contactId = 0;
            try
            {
                string query = "DELETE FROM ACTIVECAMPAIGNLISTADD WHERE LISTID= :LISTID ";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                    new OracleParameter { ParameterName="LISTID", Value=ActiveListId } 
                });
                contactId = 1;
            }
            catch (Exception ex)
            {
                contactId = 0;
                throw new ShipaDbException(ex.ToString());
            }
            return contactId;
        }

        public List<ActCampaignStatusEntity> GetGuestUsersStatusforActCampaign(string userType) 
        {
            List<ActCampaignStatusEntity> lstActCampaigndata = new List<ActCampaignStatusEntity>();
            OracleParameter op = null;
            DataTable dt = new DataTable();
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {

                        mCommand.CommandText = "ACTCAMPAIGN_GUESTDATA";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("ACTCP_USERTYPE", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = userType;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("ACTCP_USERFLOWSTATUSDATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        OracleDataAdapter da = new OracleDataAdapter(mCommand);
                        DataSet ds = new DataSet();
                        da.Fill(dt);

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            ActCampaignStatusEntity objActiveCampaign = new ActCampaignStatusEntity();
                            objActiveCampaign.QuotationNumber = dt.Rows[i]["QUOTATIONNUMBER"].ToString();
                            objActiveCampaign.FirstName = dt.Rows[i]["GUESTNAME"].ToString();
                            objActiveCampaign.CompanyName = dt.Rows[i]["GUESTCOMPANY"].ToString();
                            objActiveCampaign.EmailId = dt.Rows[i]["GUESTEMAIL"].ToString();
                            objActiveCampaign.QuoteRequestDate =dt.Rows[i]["QUOTEREQUESTDATE"].ToString();
                            objActiveCampaign.QuoteGivenByGSSCDate = dt.Rows[i]["QUOTEGIVENBYGSSCDATE"].ToString();
                            objActiveCampaign.OperationalJobNumber = dt.Rows[i]["OPERATIONALJOBNUMBER"].ToString();
                            objActiveCampaign.OCountryName = dt.Rows[i]["OCOUNTRYNAME"].ToString();
                            objActiveCampaign.DCountryName = dt.Rows[i]["DCOUNTRYNAME"].ToString();
                            objActiveCampaign.ProductName = dt.Rows[i]["PRODUCTNAME"].ToString();
                            objActiveCampaign.MovementTypeName = dt.Rows[i]["MOVEMENTTYPENAME"].ToString();
                            objActiveCampaign.OriginPortName = dt.Rows[i]["ORIGINPORTNAME"].ToString();
                            objActiveCampaign.DestinationPortName = dt.Rows[i]["DESTINATIONPORTNAME"].ToString();
                            objActiveCampaign.CreatedBy = dt.Rows[i]["CREATEDBY"].ToString();
                            objActiveCampaign.CustomerStatus = dt.Rows[i]["CUSTOMERSTATUS"].ToString();
                            objActiveCampaign.StateId = dt.Rows[i]["STATEID"].ToString();
                            objActiveCampaign.Currency = dt.Rows[i]["CURRENCY"].ToString();
                            objActiveCampaign.QuoteGrandTotal =dt.Rows[i]["QUOTEGRANDTOTAL"].ToString();
                            objActiveCampaign.JobGrandTotal =dt.Rows[i]["JOBGRANDTOTAL"].ToString();
                            lstActCampaigndata.Add(objActiveCampaign);
                        }

                    }
                    catch (Exception ex) {
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }

            return lstActCampaigndata;
        
        }

        public List<ActCampaignStatusEntity> GetRegisterUsersStatusforActCampaign()
        {
            List<ActCampaignStatusEntity> lstActCampaigndata = new List<ActCampaignStatusEntity>();
            OracleParameter op = null;
            DataTable dt = new DataTable();
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {

                        mCommand.CommandText = "ACTCAMPAIGN_REGISTERUSERSDATA";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("ACTCP_USERFLOWSTATUSDATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        OracleDataAdapter da = new OracleDataAdapter(mCommand);
                        DataSet ds = new DataSet();
                        da.Fill(dt);

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            ActCampaignStatusEntity objActiveCampaign = new ActCampaignStatusEntity();
                            objActiveCampaign.QuotationNumber = dt.Rows[i]["QUOTATIONNUMBER"].ToString();
                            objActiveCampaign.FirstName = dt.Rows[i]["FIRSTNAME"].ToString();
                            objActiveCampaign.CompanyName = dt.Rows[i]["LASTNAME"].ToString();
                            objActiveCampaign.EmailId = dt.Rows[i]["CREATEDBY"].ToString();
                            objActiveCampaign.QuoteRequestDate = dt.Rows[i]["QUOTEREQUESTDATE"].ToString();
                            objActiveCampaign.QuoteGivenByGSSCDate = dt.Rows[i]["QUOTEGIVENBYGSSCDATE"].ToString();
                            objActiveCampaign.OperationalJobNumber = dt.Rows[i]["OPERATIONALJOBNUMBER"].ToString();
                            objActiveCampaign.OCountryName = dt.Rows[i]["OCOUNTRYNAME"].ToString();
                            objActiveCampaign.DCountryName = dt.Rows[i]["DCOUNTRYNAME"].ToString();
                            objActiveCampaign.ProductName = dt.Rows[i]["PRODUCTNAME"].ToString();
                            objActiveCampaign.MovementTypeName = dt.Rows[i]["MOVEMENTTYPENAME"].ToString();
                            objActiveCampaign.OriginPortName = dt.Rows[i]["ORIGINPORTNAME"].ToString();
                            objActiveCampaign.DestinationPortName = dt.Rows[i]["DESTINATIONPORTNAME"].ToString();
                            objActiveCampaign.CreatedBy = dt.Rows[i]["CREATEDBY"].ToString();
                            objActiveCampaign.CustomerStatus = dt.Rows[i]["CUSTOMERSTATUS"].ToString();
                            objActiveCampaign.StateId = dt.Rows[i]["STATEID"].ToString();
                            objActiveCampaign.Currency = dt.Rows[i]["CURRENCY"].ToString();
                            objActiveCampaign.QuoteGrandTotal = dt.Rows[i]["QUOTEGRANDTOTAL"].ToString();
                            objActiveCampaign.JobGrandTotal = dt.Rows[i]["JOBGRANDTOTAL"].ToString();
                            lstActCampaigndata.Add(objActiveCampaign);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }

            return lstActCampaigndata;

        }

        public int InsertWebHookResponse(string Response)
        {
            int responseId = 0;
            long PrimaryKey;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT WEBHOOKRESPONSE_SEQ.NEXTVAL FROM DUAL";
                        PrimaryKey = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "INSERT INTO WEBHOOKUNSUBSCRIBERRESPONSE (WEBHOOKID,WEBHOOKRESPONSE) VALUES (:WEBHOOKID,:WEBHOOKRESPONSE)";
                        mCommand.Parameters.Add("WEBHOOKID", PrimaryKey);
                        mCommand.Parameters.Add("WEBHOOKRESPONSE", Response);
                       
                        mCommand.BindByName = true;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        responseId = 1;
                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }

            return responseId;
        }
    }
}
