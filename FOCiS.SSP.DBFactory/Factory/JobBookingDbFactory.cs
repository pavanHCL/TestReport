﻿using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Oracle.DataAccess.Types;
using System.Web;

namespace FOCiS.SSP.DBFactory.Factory
{
    public class JobBookingDbFactory : DBFactoryCore, IJobBookingDbFactory, IDBFactory<int, JobBookingEntity>, IDisposable
    {
        #region Old Methods

        public JobBookingEntity GetById1(int id)
        {
            JobBookingEntity mEntity = new JobBookingEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMQUOTATION WHERE QUOTATIONID = :QUOTATIONID",
              new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
          , delegate(IDataReader r)
          {
              mEntity = DataReaderMapToObject<JobBookingEntity>(r);
          });

            //Shipment Details List
            List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
            });
            mEntity.ShipmentItems = mShipmentItems;

            //Document Details List
            List<SSPDocumentsEntity> mdocItems = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE PAGEFLAG=:PAGEFLAG and QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id }, 
                    new OracleParameter {OracleDbType = OracleDbType.Int32, ParameterName = "PAGEFLAG", Value = 1} }
            , delegate(IDataReader r)
            {
                mdocItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            mEntity.DocumentDetails = mdocItems;

            //JobDetails list 

            //List<SSPJobDetailsEntity> mjobItems = new List<SSPJobDetailsEntity>();
            //ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPJOBDETAILS WHERE QUOTATIONID = :QUOTATIONID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mjobItems.Add(DataReaderMapToObject<SSPJobDetailsEntity>(r));
            //});
            //mEntity.JobDetails = mjobItems;



            //ReadText(DBConnectionMode.SSP, "SELECT * FROM QMTERMSANDCONDITION WHERE QUOTATIONID = :QUOTATIONID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mEntity.TermAndConditions = DataReaderMapToList<QuotationTermsConditionEntity>(r);
            //});
            //ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPPARTYDETAILS WHERE PARTYDETAILSID = :PARTYDETAILSID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "PARTYDETAILSID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mEntity.PartyDetails = DataReaderMapToList<SSPPartyDetails>(r);
            //});

            //Createing job in the time of booking

            // SSPJobDetailsEntity job = CreateNewJobDetails(mEntity.QUOTATIONID, mEntity.QUOTATIONNUMBER,UserName);
            //mEntity.JJOBNUMBER = job.JOBNUMBER;
            //mEntity.JOPERATIONALJOBNUMBER = job.OPERATIONALJOBNUMBER;
            //mEntity.STATEID = job.STATEID;
            return mEntity;
        }

        public JobBookingEntity GetById1(int id, string UserName)
        {
            JobBookingEntity mEntity = new JobBookingEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMQUOTATION WHERE QUOTATIONID = :QUOTATIONID",
              new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
          , delegate(IDataReader r)
          {
              mEntity = DataReaderMapToObject<JobBookingEntity>(r);
          });

            //Shipment Details List
            List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
            });
            mEntity.ShipmentItems = mShipmentItems;


            //Document Details List
            List<SSPDocumentsEntity> mdocItems = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE PAGEFLAG=:PAGEFLAG and QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id },
                                        new OracleParameter {OracleDbType = OracleDbType.Int32, ParameterName = "PAGEFLAG", Value = 1} }
            , delegate(IDataReader r)
            {
                mdocItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            mEntity.DocumentDetails = mdocItems;

            //Createing job in the time of booking
            SSPJobDetailsEntity job = CreateNewJobDetails(mEntity.QUOTATIONID, mEntity.QUOTATIONNUMBER, UserName, mEntity.GRANDTOTAL);
            mEntity.JJOBNUMBER = job.JOBNUMBER;
            mEntity.JOPERATIONALJOBNUMBER = job.OPERATIONALJOBNUMBER;
            mEntity.STATEID = job.STATEID;

            //Commented by sindhu
            //UpdateHSCodeDetails(mEntity.QUOTATIONID, UserName, job.JOBNUMBER);
            //Hscode Instructions Based on Jobnumber

            //------------Push Country Based Instructions-------------------------( New call Added BY Anil G)
            PushCountryBasedIns(job.JOBNUMBER);

            string Query = @"select HSCODE
                                         from FOCSSP.HSCODEDETAILS hscode
                                        inner join FOCSSP.HSCODEINSTRUCATIONS hsins on hsins.HSCODEDTLSID=hscode.HSCODEDTLSID
                                        where JOBNUMBER=" + job.JOBNUMBER + "and QuotationID=" + id;
            List<HsCodeInstructionEntity> HscodeItems = new List<HsCodeInstructionEntity>();
            ReadText(DBConnectionMode.SSP, Query,
                new OracleParameter[] { new OracleParameter { } }
            , delegate(IDataReader r)
            {
                HscodeItems.Add(DataReaderMapToObject<HsCodeInstructionEntity>(r));
            });
            mEntity.HSCodeInsItems = HscodeItems;

            return mEntity;
        }

        public PaymentEntity GetPaymentDetailsByJobNumber1(int id, string userid)
        {
            PaymentEntity mEntity = new PaymentEntity();
            ReadText(DBConnectionMode.SSP, "SELECT JH.RECEIPTHDRID,JH.JOBNUMBER,JH.OPJOBNUMBER,JH.QUOTATIONID,JH.QUOTATIONNUMBER,JH.RECEIPTNETAMOUNT,JH.CURRENCY,Q.PRODUCTNAME      AS MODEOFTRANSPORT,Q.MOVEMENTTYPENAME AS MOVEMENTTYPE,SP.CLIENTNAME SHIPPERNAME,SP1.CLIENTNAME CONSIGNEENAME FROM JARECEIPTHEADER JH INNER JOIN QMQUOTATION Q ON JH.QUOTATIONID=Q.QUOTATIONID INNER JOIN SSPPARTYDETAILS SP ON JH.JOBNUMBER =SP.JOBNUMBER AND SP.PARTYTYPE=91 INNER JOIN SSPPARTYDETAILS SP1 ON JH.JOBNUMBER   =SP1.JOBNUMBER AND SP1.PARTYTYPE =92 WHERE JH.JOBNUMBER=:JOBNUMBER",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = id } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<PaymentEntity>(r);
            });

            PaymentEntity mCargoDate = new PaymentEntity();
            ReadText(DBConnectionMode.SSP, "SELECT CARGOAVAILABLEFROM FROM SSPJOBDETAILS WHERE JOBNUMBER=:JOBNUMBER",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = id } }
            , delegate(IDataReader r)
            {
                mCargoDate = DataReaderMapToObject<PaymentEntity>(r);
            });
            mEntity.CARGOAVAILABLEFROM = mCargoDate.CARGOAVAILABLEFROM;

            List<SSPBranchListEntity> mBranchList = new List<SSPBranchListEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPPAYMENTBRANCHLIST WHERE UPPER(SITECITY)= (SELECT UPPER(CITYNAME) FROM UMUSERADDRESSDETAILS WHERE UPPER(USERID)=:USERID) AND UPPER(COUNTRYCODE)=(SELECT UPPER(COUNTRYCODE) FROM UMUSERADDRESSDETAILS WHERE UPPER(USERID)=:USERID)",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "USERID", Value = userid.ToString().ToUpper() } }
            , delegate(IDataReader r)
            {
                mBranchList.Add(DataReaderMapToObject<SSPBranchListEntity>(r));
            });

            mEntity.BRANCHLIST = mBranchList;
            //SELECT * FROM SSPPAYMENTENTITYLIST WHERE UPPER(COUNTRYCODE) =(SELECT UPPER(COUNTRYCODE) FROM UMUSERADDRESSDETAILS WHERE UPPER(USERID)=:USERID)
            SSPEntityListEntity mEntityList = new SSPEntityListEntity();
            ReadText(DBConnectionMode.SSP, "SELECT ENTITYKEY,FINANCIALENTITYCODE,FINANCIALENTITYNAME,COUNTRYCODE,BENEFICIARYNAME,BANKNAME,BANKACCOUNTNUMBER,BRANCH,SUBSTR(IFSCCODE, 1, 50) AS IFSCCODE,SWIFTCODE FROM SSPPAYMENTENTITYLIST WHERE UPPER(COUNTRYCODE) =(SELECT UPPER(COUNTRYCODE) FROM UMUSERADDRESSDETAILS WHERE UPPER(USERID)=:USERID)",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "USERID", Value = userid.ToString().ToUpper() } }
            , delegate(IDataReader r)
            {
                mEntityList = DataReaderMapToObject<SSPEntityListEntity>(r);
            });

            mEntity.ENTITYLIST = mEntityList;

            if (!string.IsNullOrEmpty(userid))
            {
                ReadText(DBConnectionMode.SSP, "SELECT NVL(FOCIS.RATECALC_GET_EXCHANGE_RATE(CRDTAPP.CURRENTOSBALANCE,CRDTAPP.HFMENTRYCODE,RECPHDR.CURRENCY),0) CURRENTOSBALANCE,NVL(RECPHDR.CURRENCY,'USD') AS HFMENTRYCODE  FROM JARECEIPTHEADER RECPHDR LEFT JOIN SSPCREDITAPPLICATIONS CRDTAPP ON RECPHDR.CREATEDBY = CRDTAPP.USERID WHERE CRDTAPP.STATEID='Approved' AND UPPER(CRDTAPP.USERID)=:USERID AND RECPHDR.JOBNUMBER=:JOBNUMBER",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = userid.ToString().ToUpper() } ,
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = id }
                }
                , delegate(IDataReader r)
                {
                    mEntity.CREDITAMOUNT = Convert.ToDouble(r["CURRENTOSBALANCE"]);
                    if (Convert.ToString(r["HFMENTRYCODE"]) != null)
                    {
                        mEntity.CREDITCURRENCY = Convert.ToString(r["HFMENTRYCODE"]);
                    }
                });
            }
            return mEntity;
        }

        public void SavePaymentTransactionDetails1(PaymentTransactionDetailsEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT JARECEIPTPAYMENTDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.RECEIPTPAYMENTDTLSID = Convert.ToInt64(mCommand.ExecuteScalar());

                        //mCommand.CommandText = "INSERT INTO QMQUOTATION (QUOTATIONID,QUOTATIONNUMBER, DATEOFENQUIRY, DATEOFVALIDITY, DATEOFSHIPMENT, PRODUCTID, PRODUCTTYPEID, MOVEMENTTYPEID, ORIGINPLACEID, ORIGINPORTID, DESTINATIONPLACEID, DESTINATIONPORTID, TOTALGROSSWEIGHT, TOTALCBM, DENSITYRATIO, CHARGEABLEWEIGHT, CHARGEABLEVOLUME, VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION, ISHAZARDOUSCARGO, CARGOVALUE, CARGOVALUECURRENCYID, INSUREDVALUE, INSUREDVALUECURRENCYID, CUSTOMERID, PREFERREDCURRENCYID, DATEMODIFIED, MODIFIEDBY, STATEID, PRODUCTNAME, PRODUCTTYPENAME, MOVEMENTTYPENAME, ORIGINPLACECODE, ORIGINPLACENAME, ORIGINPORTCODE, ORIGINPORTNAME, DESTINATIONPLACECODE, DESTINATIONPLACENAME, DESTINATIONPORTCODE, DESTINATIONPORTNAME,ISINSURANCEREQUIRED,CREATEDBY ) VALUES (:QUOTATIONID, :QUOTATIONNUMBER, TRUNC(SYSDATE), TRUNC(SYSDATE)+7, TRUNC(SYSDATE), :PRODUCTID, :PRODUCTTYPEID, :MOVEMENTTYPEID, :ORIGINPLACEID, :ORIGINPORTID, :DESTINATIONPLACEID, :DESTINATIONPORTID, :TOTALGROSSWEIGHT, :TOTALCBM, :DENSITYRATIO, :CHARGEABLEWEIGHT, :CHARGEABLEVOLUME, :VOLUMETRICWEIGHT, :SHIPMENTDESCRIPTION, :ISHAZARDOUSCARGO, :CARGOVALUE, :CARGOVALUECURRENCYID, :INSUREDVALUE, :INSUREDVALUECURRENCYID, :CUSTOMERID, :PREFERREDCURRENCYID, TRUNC(SYSDATE), :MODIFIEDBY, :STATEID, :PRODUCTNAME, :PRODUCTTYPENAME, :MOVEMENTTYPENAME, :ORIGINPLACECODE, :ORIGINPLACENAME, :ORIGINPORTCODE, :ORIGINPORTNAME, :DESTINATIONPLACECODE, :DESTINATIONPLACENAME, :DESTINATIONPORTCODE, :DESTINATIONPORTNAME, :ISINSURANCEREQUIRED, :CREATEDBY)";
                        mCommand.CommandText = "INSERT INTO JARECEIPTPAYMENTDETAILS ( RECEIPTPAYMENTDTLSID, RECEIPTHDRID, JOBNUMBER, QUOTATIONID, PAYMENTOPTION, ISPAYMENTSUCESS, PAYMENTREFNUMBER, PAYMENTREFMESSAGE, DATECREATED, CREATEDBY, DATEMODIFIED, MODIFIEDBY, TRANSACTIONMODE, PAYMENTTYPE) VALUES ( :RECEIPTPAYMENTDTLSID, :RECEIPTHDRID, :JOBNUMBER, :QUOTATIONID, :PAYMENTOPTION, :ISPAYMENTSUCESS, :PAYMENTREFNUMBER, :PAYMENTREFMESSAGE, TRUNC(SYSDATE), :CREATEDBY, TRUNC(SYSDATE), :MODIFIEDBY, :TRANSACTIONMODE, :PAYMENTTYPE)";
                        mCommand.Parameters.Add("RECEIPTPAYMENTDTLSID", entity.RECEIPTPAYMENTDTLSID);
                        mCommand.Parameters.Add("RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        mCommand.Parameters.Add("PAYMENTREFMESSAGE", entity.PAYMENTREFMESSAGE);
                        //mCommand.Parameters.Add("DATECREATED", entity.DATECREATED);
                        mCommand.Parameters.Add("CREATEDBY", entity.CREATEDBY);
                        //mCommand.Parameters.Add("DATEMODIFIED", entity.DATEMODIFIED);
                        mCommand.Parameters.Add("MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.ExecuteNonQuery();
                        if (entity.ISPAYMENTSUCESS != 1)
                        {
                            mOracleTransaction.Commit();
                        }
                        else if (entity.ISPAYMENTSUCESS == 1)
                        {
                            mCommand.CommandText = "UPDATE SSPJOBDETAILS SET STATEID=:STATEID WHERE JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("STATEID", "Payment Completed");
                            mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                            mCommand.ExecuteNonQuery();
                            //mOracleTransaction.Commit();

                            mCommand.CommandText = "UPDATE JARECEIPTHEADER SET STATUS=:STATUS WHERE JOBNUMBER=:JOBNUMBER AND QUOTATIONID=:QUOTATIONID ";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("STATUS", "Payment Completed");
                            mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();

                            //Invoking the USP_SSP_SSPSTGDTLS  stored procedure  to store the job details in IL_SSPSTGDTLS for SML Generation
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JOBNUMBER);
                            mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void SaveOfflinePaymentTransactionDetails1(PaymentTransactionDetailsEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT JARECEIPTPAYMENTDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.RECEIPTPAYMENTDTLSID = Convert.ToInt64(mCommand.ExecuteScalar());
                        if (entity.PAYMENTTYPE.ToLower() == "wire transfer")
                        {
                            //mCommand.CommandText = "INSERT INTO QMQUOTATION (QUOTATIONID,QUOTATIONNUMBER, DATEOFENQUIRY, DATEOFVALIDITY, DATEOFSHIPMENT, PRODUCTID, PRODUCTTYPEID, MOVEMENTTYPEID, ORIGINPLACEID, ORIGINPORTID, DESTINATIONPLACEID, DESTINATIONPORTID, TOTALGROSSWEIGHT, TOTALCBM, DENSITYRATIO, CHARGEABLEWEIGHT, CHARGEABLEVOLUME, VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION, ISHAZARDOUSCARGO, CARGOVALUE, CARGOVALUECURRENCYID, INSUREDVALUE, INSUREDVALUECURRENCYID, CUSTOMERID, PREFERREDCURRENCYID, DATEMODIFIED, MODIFIEDBY, STATEID, PRODUCTNAME, PRODUCTTYPENAME, MOVEMENTTYPENAME, ORIGINPLACECODE, ORIGINPLACENAME, ORIGINPORTCODE, ORIGINPORTNAME, DESTINATIONPLACECODE, DESTINATIONPLACENAME, DESTINATIONPORTCODE, DESTINATIONPORTNAME,ISINSURANCEREQUIRED,CREATEDBY ) VALUES (:QUOTATIONID, :QUOTATIONNUMBER, TRUNC(SYSDATE), TRUNC(SYSDATE)+7, TRUNC(SYSDATE), :PRODUCTID, :PRODUCTTYPEID, :MOVEMENTTYPEID, :ORIGINPLACEID, :ORIGINPORTID, :DESTINATIONPLACEID, :DESTINATIONPORTID, :TOTALGROSSWEIGHT, :TOTALCBM, :DENSITYRATIO, :CHARGEABLEWEIGHT, :CHARGEABLEVOLUME, :VOLUMETRICWEIGHT, :SHIPMENTDESCRIPTION, :ISHAZARDOUSCARGO, :CARGOVALUE, :CARGOVALUECURRENCYID, :INSUREDVALUE, :INSUREDVALUECURRENCYID, :CUSTOMERID, :PREFERREDCURRENCYID, TRUNC(SYSDATE), :MODIFIEDBY, :STATEID, :PRODUCTNAME, :PRODUCTTYPENAME, :MOVEMENTTYPENAME, :ORIGINPLACECODE, :ORIGINPLACENAME, :ORIGINPORTCODE, :ORIGINPORTNAME, :DESTINATIONPLACECODE, :DESTINATIONPLACENAME, :DESTINATIONPORTCODE, :DESTINATIONPORTNAME, :ISINSURANCEREQUIRED, :CREATEDBY)";
                            mCommand.CommandText = "INSERT INTO JARECEIPTPAYMENTDETAILS ( RECEIPTPAYMENTDTLSID, RECEIPTHDRID, JOBNUMBER, QUOTATIONID,PAYMENTOPTION, ISPAYMENTSUCESS, PAYMENTREFNUMBER, PAYMENTREFMESSAGE, DATECREATED, CREATEDBY, DATEMODIFIED, MODIFIEDBY, PAYMENTTYPE, TRANSACTIONMODE, PAYERBANK, PAYERBRANCH, CHEQUENO, CHEQUEDATE,ENTITYKEY ) VALUES ( :RECEIPTPAYMENTDTLSID, :RECEIPTHDRID, :JOBNUMBER, :QUOTATIONID, :PAYMENTOPTION, :ISPAYMENTSUCESS, :PAYMENTREFNUMBER, :PAYMENTREFMESSAGE, TRUNC(SYSDATE), :CREATEDBY, TRUNC(SYSDATE), :MODIFIEDBY, :PAYMENTTYPE, :TRANSACTIONMODE, :PAYERBANK, :PAYERBRANCH, :CHEQUENO, :CHEQUEDATE, :ENTITYKEY )";
                            mCommand.Parameters.Add("ENTITYKEY", entity.BRANCHENTITYKEY);
                        }
                        else
                        {
                            mCommand.CommandText = "INSERT INTO JARECEIPTPAYMENTDETAILS ( RECEIPTPAYMENTDTLSID, RECEIPTHDRID, JOBNUMBER, QUOTATIONID,PAYMENTOPTION, ISPAYMENTSUCESS, PAYMENTREFNUMBER, PAYMENTREFMESSAGE, DATECREATED, CREATEDBY, DATEMODIFIED, MODIFIEDBY, PAYMENTTYPE, TRANSACTIONMODE, PAYERBANK, PAYERBRANCH, CHEQUENO, CHEQUEDATE,BRANCHKEY ) VALUES ( :RECEIPTPAYMENTDTLSID, :RECEIPTHDRID, :JOBNUMBER, :QUOTATIONID, :PAYMENTOPTION, :ISPAYMENTSUCESS, :PAYMENTREFNUMBER, :PAYMENTREFMESSAGE, TRUNC(SYSDATE), :CREATEDBY, TRUNC(SYSDATE), :MODIFIEDBY, :PAYMENTTYPE, :TRANSACTIONMODE, :PAYERBANK, :PAYERBRANCH, :CHEQUENO, :CHEQUEDATE, :BRANCHKEY )";
                            mCommand.Parameters.Add("BRANCHKEY", entity.BRANCHENTITYKEY);
                        }
                        mCommand.Parameters.Add("RECEIPTPAYMENTDTLSID", entity.RECEIPTPAYMENTDTLSID);
                        mCommand.Parameters.Add("RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        mCommand.Parameters.Add("PAYMENTREFMESSAGE", entity.PAYMENTREFMESSAGE);
                        //mCommand.Parameters.Add("DATECREATED", entity.DATECREATED);
                        mCommand.Parameters.Add("CREATEDBY", entity.CREATEDBY);
                        //mCommand.Parameters.Add("DATEMODIFIED", entity.DATEMODIFIED);
                        mCommand.Parameters.Add("MODIFIEDBY", entity.MODIFIEDBY);

                        mCommand.Parameters.Add("PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PAYERBANK", entity.TRANSBANKNAME);
                        mCommand.Parameters.Add("PAYERBRANCH", entity.TRANSBRANCHNAME);
                        mCommand.Parameters.Add("CHEQUENO", entity.TRANSCHEQUENO);
                        mCommand.Parameters.Add("CHEQUEDATE", entity.TRANSCHEQUEDATE);

                        mCommand.ExecuteNonQuery();
                        if (entity.ISPAYMENTSUCESS != 1)
                        {
                            mOracleTransaction.Commit();
                        }
                        else if (entity.ISPAYMENTSUCESS == 1)
                        {

                            mCommand.CommandText = "UPDATE SSPJOBDETAILS SET STATEID=:STATEID WHERE JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("STATEID", "Payment in Pending");
                            mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "UPDATE JARECEIPTHEADER SET RECEIPTAMOUNT=:RECEIPTAMOUNT, RECEIPTNETAMOUNT = :RECEIPTNETAMOUNT, CURRENCY=:CURRENCY, PAYMENTDESCRIPTION=:PAYMENTDESCRIPTION, STATUS=:STATUS WHERE JOBNUMBER=:JOBNUMBER AND QUOTATIONID=:QUOTATIONID ";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("STATUS", "Payment in Pending");
                            mCommand.Parameters.Add("RECEIPTAMOUNT", entity.TRANSAMOUNT);
                            mCommand.Parameters.Add("RECEIPTNETAMOUNT", entity.TRANSAMOUNT);
                            mCommand.Parameters.Add("CURRENCY", entity.TRANSCURRENCY);
                            mCommand.Parameters.Add("PAYMENTDESCRIPTION", entity.TRANSDESCRIPTION);

                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();

                            ////Invoking the USP_SSP_SSPSTGDTLS  stored procedure  to store the job details in IL_SSPSTGDTLS for SML Generation
                            //mCommand.Parameters.Clear();
                            //mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JOBNUMBER);
                            //mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                            //mCommand.CommandType = CommandType.StoredProcedure;
                            //mCommand.ExecuteNonQuery();
                        }

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void SaveBusinessCreditTransactionDetails1(PaymentTransactionDetailsEntity entity, string userid)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT JARECEIPTPAYMENTDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.RECEIPTPAYMENTDTLSID = Convert.ToInt64(mCommand.ExecuteScalar());

                        //mCommand.CommandText = "INSERT INTO QMQUOTATION (QUOTATIONID,QUOTATIONNUMBER, DATEOFENQUIRY, DATEOFVALIDITY, DATEOFSHIPMENT, PRODUCTID, PRODUCTTYPEID, MOVEMENTTYPEID, ORIGINPLACEID, ORIGINPORTID, DESTINATIONPLACEID, DESTINATIONPORTID, TOTALGROSSWEIGHT, TOTALCBM, DENSITYRATIO, CHARGEABLEWEIGHT, CHARGEABLEVOLUME, VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION, ISHAZARDOUSCARGO, CARGOVALUE, CARGOVALUECURRENCYID, INSUREDVALUE, INSUREDVALUECURRENCYID, CUSTOMERID, PREFERREDCURRENCYID, DATEMODIFIED, MODIFIEDBY, STATEID, PRODUCTNAME, PRODUCTTYPENAME, MOVEMENTTYPENAME, ORIGINPLACECODE, ORIGINPLACENAME, ORIGINPORTCODE, ORIGINPORTNAME, DESTINATIONPLACECODE, DESTINATIONPLACENAME, DESTINATIONPORTCODE, DESTINATIONPORTNAME,ISINSURANCEREQUIRED,CREATEDBY ) VALUES (:QUOTATIONID, :QUOTATIONNUMBER, TRUNC(SYSDATE), TRUNC(SYSDATE)+7, TRUNC(SYSDATE), :PRODUCTID, :PRODUCTTYPEID, :MOVEMENTTYPEID, :ORIGINPLACEID, :ORIGINPORTID, :DESTINATIONPLACEID, :DESTINATIONPORTID, :TOTALGROSSWEIGHT, :TOTALCBM, :DENSITYRATIO, :CHARGEABLEWEIGHT, :CHARGEABLEVOLUME, :VOLUMETRICWEIGHT, :SHIPMENTDESCRIPTION, :ISHAZARDOUSCARGO, :CARGOVALUE, :CARGOVALUECURRENCYID, :INSUREDVALUE, :INSUREDVALUECURRENCYID, :CUSTOMERID, :PREFERREDCURRENCYID, TRUNC(SYSDATE), :MODIFIEDBY, :STATEID, :PRODUCTNAME, :PRODUCTTYPENAME, :MOVEMENTTYPENAME, :ORIGINPLACECODE, :ORIGINPLACENAME, :ORIGINPORTCODE, :ORIGINPORTNAME, :DESTINATIONPLACECODE, :DESTINATIONPLACENAME, :DESTINATIONPORTCODE, :DESTINATIONPORTNAME, :ISINSURANCEREQUIRED, :CREATEDBY)";
                        mCommand.CommandText = "INSERT INTO JARECEIPTPAYMENTDETAILS ( RECEIPTPAYMENTDTLSID, RECEIPTHDRID, JOBNUMBER, QUOTATIONID,PAYMENTOPTION, ISPAYMENTSUCESS, PAYMENTREFNUMBER, PAYMENTREFMESSAGE, DATECREATED, CREATEDBY, DATEMODIFIED, MODIFIEDBY, PAYMENTTYPE, TRANSACTIONMODE) VALUES ( :RECEIPTPAYMENTDTLSID, :RECEIPTHDRID, :JOBNUMBER, :QUOTATIONID, :PAYMENTOPTION, :ISPAYMENTSUCESS, :PAYMENTREFNUMBER, :PAYMENTREFMESSAGE, TRUNC(SYSDATE), :CREATEDBY, TRUNC(SYSDATE), :MODIFIEDBY, :PAYMENTTYPE, :TRANSACTIONMODE)";

                        mCommand.Parameters.Add("RECEIPTPAYMENTDTLSID", entity.RECEIPTPAYMENTDTLSID);
                        mCommand.Parameters.Add("RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        mCommand.Parameters.Add("PAYMENTREFMESSAGE", entity.PAYMENTREFMESSAGE);
                        //mCommand.Parameters.Add("DATECREATED", entity.DATECREATED);
                        mCommand.Parameters.Add("CREATEDBY", entity.CREATEDBY);
                        //mCommand.Parameters.Add("DATEMODIFIED", entity.DATEMODIFIED);
                        mCommand.Parameters.Add("MODIFIEDBY", entity.MODIFIEDBY);

                        mCommand.Parameters.Add("PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("TRANSACTIONMODE", entity.TRANSMODE);
                        //mCommand.Parameters.Add("PAYERBANK", entity.TRANSBANKNAME);
                        //mCommand.Parameters.Add("PAYERBRANCH", entity.TRANSBRANCHNAME);
                        //mCommand.Parameters.Add("CHEQUENO", entity.TRANSCHEQUENO);
                        //mCommand.Parameters.Add("CHEQUEDATE", entity.TRANSCHEQUEDATE);

                        mCommand.ExecuteNonQuery();
                        if (entity.ISPAYMENTSUCESS != 1)
                        {
                            mOracleTransaction.Commit();
                        }
                        else if (entity.ISPAYMENTSUCESS == 1)
                        {
                            mCommand.CommandText = "UPDATE SSPJOBDETAILS SET STATEID=:STATEID WHERE JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("STATEID", "Payment Completed");
                            mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "UPDATE JARECEIPTHEADER SET STATUS=:STATUS, RECEIPTAMOUNT=:RECEIPTAMOUNT, RECEIPTNETAMOUNT = :RECEIPTNETAMOUNT, CURRENCY=:CURRENCY, PAYMENTDESCRIPTION=:PAYMENTDESCRIPTION WHERE JOBNUMBER=:JOBNUMBER AND QUOTATIONID=:QUOTATIONID ";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("STATUS", "Payment Completed");
                            mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("RECEIPTAMOUNT", entity.TRANSAMOUNT);
                            mCommand.Parameters.Add("RECEIPTNETAMOUNT", entity.TRANSAMOUNT);
                            mCommand.Parameters.Add("CURRENCY", entity.TRANSCURRENCY);
                            mCommand.Parameters.Add("PAYMENTDESCRIPTION", entity.TRANSDESCRIPTION);
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "UPDATE SSPCREDITAPPLICATIONS CRDTAPP SET CURRENTOSBALANCE=CURRENTOSBALANCE-NVL(FOCIS.RATECALC_GET_EXCHANGE_RATE(:TRANSAMOUNT,:CURRENCY,CRDTAPP.HFMENTRYCODE),0) WHERE UPPER(USERID)=:USERID";
                            mCommand.Parameters.Clear();

                            mCommand.Parameters.Add("TRANSAMOUNT", entity.TRANSAMOUNT);
                            mCommand.Parameters.Add("CURRENCY", entity.TRANSCURRENCY);
                            mCommand.Parameters.Add("USERID", userid.ToUpper());
                            mCommand.ExecuteNonQuery();

                            mOracleTransaction.Commit();

                            //Invoking the USP_SSP_SSPSTGDTLS  stored procedure  to store the job details in IL_SSPSTGDTLS for SML Generation
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JOBNUMBER);
                            mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }

                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public int SaveJob1(JobBookingEntity entity, string UserId, string BtnFlag, string HSCode, string FieldValues, string FieldNames)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            long Customerid = 0;
            decimal TotalCBM = 0;
            decimal TotalGrossWeight = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    entity.SHISDEFAULTADDRESS = 0;

                    try
                    {

                        if (entity.JJOBNUMBER > 0)
                        {
                            mCommand.CommandText = "Update  SSPJOBDETAILS  set INCOTERMCODE=:INCOTERMCODE,INCOTERMDESCRIPTION=:INCOTERMDESCRIPTION,CARGOAVAILABLEFROM=:CARGOAVAILABLEFROM,CARGODELIVERYBY=:CARGODELIVERYBY,DOCUMENTSREADY=:DOCUMENTSREADY,SPECIALINSTRUCTIONS=:SPECIALINSTRUCTION where JOBNUMBER=:JOBNUMBER";
                            // mCommand.Parameters.Add("PARTYDETAILSID", entity.CONPARTYDETAILSID);
                            //entity.MODIFIEDBY = "GUEST";
                            //entity.STATEID = "QUOTECREATED";
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("INCOTERMCODE", entity.JINCOTERMCODE);
                            mCommand.Parameters.Add("INCOTERMDESCRIPTION", entity.JINCOTERMDESCRIPTION);
                            if (entity.JCARGOAVAILABLEFROM.ToString() != "1/1/0001 12:00:00 AM")
                                mCommand.Parameters.Add("CARGOAVAILABLEFROM", entity.JCARGOAVAILABLEFROM);
                            else
                                mCommand.Parameters.Add("CARGOAVAILABLEFROM", "");
                            if (entity.JCARGODELIVERYBY.ToString() != "1/1/0001 12:00:00 AM")
                                mCommand.Parameters.Add("CARGODELIVERYBY", entity.JCARGODELIVERYBY);
                            else
                                mCommand.Parameters.Add("CARGODELIVERYBY", "");
                            //mCommand.Parameters.Add("ORIGINCUSTOMSCLEARANCEBY", entity.JORIGINCUSTOMSCLEARANCEBY);
                            //mCommand.Parameters.Add("DESTINATIONCUSTOMSCLEARANCEBY", entity.JDESTINATIONCUSTOMSCLEARANCEBY);
                            mCommand.Parameters.Add("DOCUMENTSREADY", entity.JDOCUMENTSREADY);
                            mCommand.Parameters.Add("SPECIALINSTRUCTION", entity.SPECIALINSTRUCTION);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }

                        //Getting countrycode by countryid
                        //mCommand.Parameters.Clear();
                        //mCommand.CommandText = "select C.CODE from SMDM_COUNTRY C Inner Join DataProfileClassWFStates DP on C.STATEID=DP.STATEID Where 1=1 and C.COUNTRYID=:COUNTRYID";
                        //mCommand.Parameters.Add("COUNTRYID", entity.SHCOUNTRYCODE);
                        //entity.SHCOUNTRYCODE = mCommand.ExecuteScalar().ToString();
                        //mCommand.Parameters.Clear();


                        mCommand.CommandText = "SELECT SSP_PARTYDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.SHPARTYDETAILSID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Insert into SSPPARTYDETAILS (PARTYDETAILSID,JOBNUMBER,CLIENTID,CLIENTNAME,PARTYTYPE,ADDRESSTYPE,HOUSENO,BUILDINGNAME,STREET1,STREET2,COUNTRYCODE,COUNTRYNAME,CITYCODE,CITYNAME,STATECODE,STATENAME,PINCODE,FULLADDRESS,ISDEFAULTADDRESS,LATITUDE,LONGITUDE,FIRSTNAME,LASTNAME,PHONE,EMAILID,SALUTATION,CREATEDBY) values (:PARTYDETAILSID,:JOBNUMBER,:CLIENTID,:CLIENTNAME,:PARTYTYPE,:ADDRESSTYPE,:HOUSENO,:BUILDINGNAME,:STREET1,:STREET2,:COUNTRYCODE,:COUNTRYNAME,:CITYCODE,:CITYNAME,:STATECODE,:STATENAME,:PINCODE,:FULLADDRESS,:ISDEFAULTADDRESS,:LATITUDE,:LONGITUDE,:FIRSTNAME,:LASTNAME,:PHONE,:EMAILID,:SALUTATION,:CREATEDBY)";
                        mCommand.Parameters.Add("PARTYDETAILSID", entity.SHPARTYDETAILSID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("CREATEDBY", UserId);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CLIENTID", "SSP" + entity.SHPARTYDETAILSID.ToString());
                        mCommand.Parameters.Add("CLIENTNAME", entity.SHCLIENTNAME);
                        mCommand.Parameters.Add("PARTYTYPE", 91);
                        mCommand.Parameters.Add("ADDRESSTYPE", entity.SHADDRESSTYPE);
                        mCommand.Parameters.Add("HOUSENO", entity.SHHOUSENO);
                        mCommand.Parameters.Add("BUILDINGNAME", entity.SHBUILDINGNAME);
                        mCommand.Parameters.Add("STREET1", entity.SHSTREET1);
                        mCommand.Parameters.Add("STREET2", entity.SHSTREET2);
                        mCommand.Parameters.Add("COUNTRYCODE", string.IsNullOrEmpty(entity.SHCOUNTRYCODE) ? "0" : GetCountryCodeById(Convert.ToInt64(entity.SHCOUNTRYCODE)));
                        mCommand.Parameters.Add("COUNTRYNAME", entity.SHCOUNTRYNAME);
                        mCommand.Parameters.Add("CITYCODE", entity.SHCITYCODE);
                        mCommand.Parameters.Add("CITYNAME", entity.SHCITYNAME);
                        mCommand.Parameters.Add("STATECODE", string.IsNullOrEmpty(entity.SHSTATECODE) ? "0" : GetStateCodeBySubDivisionId(Convert.ToInt64(entity.SHSTATECODE)));
                        mCommand.Parameters.Add("STATENAME", entity.SHSTATENAME);
                        mCommand.Parameters.Add("PINCODE", entity.SHPINCODE);
                        mCommand.Parameters.Add("FULLADDRESS", entity.SHFULLADDRESS);
                        mCommand.Parameters.Add("ISDEFAULTADDRESS", entity.SHISDEFAULTADDRESS);
                        mCommand.Parameters.Add("LATITUDE", entity.SHLATITUDE);
                        mCommand.Parameters.Add("LONGITUDE", entity.SHLONGITUDE);
                        mCommand.Parameters.Add("FIRSTNAME", entity.SHFIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.SHLASTNAME);
                        mCommand.Parameters.Add("PHONE", entity.SHPHONE);
                        mCommand.Parameters.Add("EMAILID", entity.SHEMAILID);
                        mCommand.Parameters.Add("SALUTATION", entity.SHSALUTATION);
                        mCommand.ExecuteNonQuery();
                        mCommand.Parameters.Clear();


                        mCommand.CommandText = "SELECT SSP_PARTYDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.SHPARTYDETAILSID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Insert into SSPPARTYDETAILS (PARTYDETAILSID,JOBNUMBER,CLIENTID,CLIENTNAME,PARTYTYPE,ADDRESSTYPE,HOUSENO,BUILDINGNAME,STREET1,STREET2,COUNTRYCODE,COUNTRYNAME,CITYCODE,CITYNAME,STATECODE,STATENAME,PINCODE,FULLADDRESS,ISDEFAULTADDRESS,LATITUDE,LONGITUDE,FIRSTNAME,LASTNAME,PHONE,EMAILID,SALUTATION,CREATEDBY) values (:PARTYDETAILSID,:JOBNUMBER,:CLIENTID,:CLIENTNAME,:PARTYTYPE,:ADDRESSTYPE,:HOUSENO,:BUILDINGNAME,:STREET1,:STREET2,:COUNTRYCODE,:COUNTRYNAME,:CITYCODE,:CITYNAME,:STATECODE,:STATENAME,:PINCODE,:FULLADDRESS,:ISDEFAULTADDRESS,:LATITUDE,:LONGITUDE,:FIRSTNAME,:LASTNAME,:PHONE,:EMAILID,:SALUTATION,:CREATEDBY)";

                        mCommand.Parameters.Add("PARTYDETAILSID", entity.SHPARTYDETAILSID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("CREATEDBY", UserId);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CLIENTID", "SSP" + entity.SHPARTYDETAILSID.ToString());
                        mCommand.Parameters.Add("CLIENTNAME", entity.CONCLIENTNAME);
                        mCommand.Parameters.Add("PARTYTYPE", 92);
                        mCommand.Parameters.Add("ADDRESSTYPE", entity.CONADDRESSTYPE);
                        mCommand.Parameters.Add("HOUSENO", entity.CONHOUSENO);
                        mCommand.Parameters.Add("BUILDINGNAME", entity.CONBUILDINGNAME);
                        mCommand.Parameters.Add("STREET1", entity.CONSTREET1);
                        mCommand.Parameters.Add("STREET2", entity.CONSTREET2);
                        //mCommand.Parameters.Add("COUNTRYCODE", GetCountryCodeById(Convert.ToInt64(entity.CONCOUNTRYCODE)));
                        mCommand.Parameters.Add("COUNTRYCODE", string.IsNullOrEmpty(entity.CONCOUNTRYCODE) ? "0" : GetCountryCodeById(Convert.ToInt64(entity.CONCOUNTRYCODE)));
                        mCommand.Parameters.Add("COUNTRYNAME", entity.CONCOUNTRYNAME);
                        mCommand.Parameters.Add("CITYCODE", entity.CONCITYCODE);
                        mCommand.Parameters.Add("CITYNAME", entity.CONCITYNAME);
                        //mCommand.Parameters.Add("STATECODE", GetStateCodeBySubDivisionId(Convert.ToInt64(entity.CONSTATECODE)));
                        mCommand.Parameters.Add("STATECODE", string.IsNullOrEmpty(entity.CONSTATECODE) ? "0" : GetStateCodeBySubDivisionId(Convert.ToInt64(entity.CONSTATECODE)));
                        mCommand.Parameters.Add("STATENAME", entity.CONSTATENAME);
                        mCommand.Parameters.Add("PINCODE", entity.CONPINCODE);
                        mCommand.Parameters.Add("FULLADDRESS", entity.CONFULLADDRESS);
                        mCommand.Parameters.Add("ISDEFAULTADDRESS", entity.CONISDEFAULTADDRESS);
                        mCommand.Parameters.Add("LATITUDE", entity.CONLATITUDE);
                        mCommand.Parameters.Add("LONGITUDE", entity.CONLONGITUDE);
                        mCommand.Parameters.Add("FIRSTNAME", entity.CONFIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.CONLASTNAME);
                        mCommand.Parameters.Add("PHONE", entity.CONPHONE);
                        mCommand.Parameters.Add("EMAILID", entity.CONEMAILID);
                        mCommand.Parameters.Add("SALUTATION", entity.CONSALUTATION);
                        mCommand.ExecuteNonQuery();
                        mCommand.Parameters.Clear();

                        if (entity.ShipmentItems.Count() > 0)
                        {
                            foreach (var item in entity.ShipmentItems)
                            {
                                if (item.ITEMHEIGHT != 0)
                                {
                                    mCommand.CommandText = "SELECT SSPJOBSHIPMENTITEM_SEQ.NEXTVAL FROM DUAL";
                                    long JOBSHIPMENTITEMID = Convert.ToInt64(mCommand.ExecuteScalar());

                                    //mCommand.CommandText = "Insert into SSPJOBSHIPMENTITEM (JOBSHIPMENTITEMID,CONTAINERID,CONTAINERNAME,ITEMDESCRIPTION,ITEMHEIGHT,ITEMLENGTH,ITEMWIDTH,JOBSHIPMENTID,LENGTHUOM,LENGTHUOMCODE,LENGTHUOMNAME,PACKAGETYPECODE,PACKAGETYPEID,PACKAGETYPENAME,QUANTITY,QUOTATIONID,TOTALCBM,WEIGHTPERPIECE,WEIGHTTOTAL,WEIGHTUOM,WEIGHTUOMCODE,WEIGHTUOMNAME) values (:JOBSHIPMENTITEMID,:CONTAINERID,:CONTAINERNAME,:ITEMDESCRIPTION,:ITEMHEIGHT,:PACKAGETYPECODE,:PACKAGETYPEID,:PACKAGETYPENAME,:QUANTITY,:QUOTATIONID,:TOTALCBM,:WEIGHTPERPIECE,:WEIGHTTOTAL,:WEIGHTUOM,:WEIGHTUOMCODE,:WEIGHTUOMNAME)";
                                    mCommand.CommandText = @"Insert into SSPJOBSHIPMENTITEM (JOBSHIPMENTITEMID,CONTAINERID,CONTAINERNAME,
                                ITEMDESCRIPTION,ITEMHEIGHT,ITEMLENGTH,ITEMWIDTH,JOBSHIPMENTID,LENGTHUOM,LENGTHUOMCODE,LENGTHUOMNAME,PACKAGETYPECODE,
                                PACKAGETYPEID,PACKAGETYPENAME,QUANTITY,QUOTATIONID,TOTALCBM,WEIGHTPERPIECE,WEIGHTTOTAL,WEIGHTUOM,WEIGHTUOMCODE,WEIGHTUOMNAME,JOBNUMBER,CONTAINERCODE) 
                                values (:JOBSHIPMENTITEMID,:CONTAINERID,:CONTAINERNAME,:ITEMDESCRIPTION,:ITEMHEIGHT,:ITEMLENGTH,:ITEMWIDTH,:JOBSHIPMENTID,
                                :LENGTHUOM,:LENGTHUOMCODE,:LENGTHUOMNAME,:PACKAGETYPECODE,:PACKAGETYPEID,:PACKAGETYPENAME,:QUANTITY,
                                :QUOTATIONID,:TOTALCBM,:WEIGHTPERPIECE,:WEIGHTTOTAL,:WEIGHTUOM,:WEIGHTUOMCODE,:WEIGHTUOMNAME,:JOBNUMBER,:CONTAINERCODE)";

                                    mCommand.Parameters.Add("JOBSHIPMENTITEMID", JOBSHIPMENTITEMID);

                                    mCommand.Parameters.Add("CONTAINERID", item.CONTAINERID != null ? item.CONTAINERID : 0);
                                    if (item.CONTAINERNAME != null && item.CONTAINERNAME.Contains("'"))
                                    {
                                        item.CONTAINERNAME = item.CONTAINERNAME.Replace("'", "-");
                                    }
                                    mCommand.Parameters.Add("CONTAINERNAME", item.CONTAINERNAME != null ? item.CONTAINERNAME : "");
                                    mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                                    mCommand.Parameters.Add("ITEMDESCRIPTION", item.ITEMDESCRIPTION != null ? item.ITEMDESCRIPTION : "");
                                    mCommand.Parameters.Add("ITEMHEIGHT", item.ITEMHEIGHT != null ? item.ITEMHEIGHT : 0);
                                    mCommand.Parameters.Add("ITEMLENGTH", item.ITEMLENGTH != null ? item.ITEMLENGTH : 0);
                                    mCommand.Parameters.Add("ITEMWIDTH", item.ITEMWIDTH != null ? item.ITEMWIDTH : 0);
                                    mCommand.Parameters.Add("JOBSHIPMENTID", item.SHIPMENTITEMID != null ? item.SHIPMENTITEMID : 0);
                                    mCommand.Parameters.Add("LENGTHUOM", item.LENGTHUOM != null ? item.LENGTHUOM : 0);
                                    mCommand.Parameters.Add("LENGTHUOMCODE", item.LENGTHUOMCODE != null ? item.LENGTHUOMCODE : "");
                                    mCommand.Parameters.Add("LENGTHUOMNAME", item.LENGTHUOMNAME != null ? item.LENGTHUOMNAME : "");
                                    mCommand.Parameters.Add("PACKAGETYPECODE", item.PACKAGETYPECODE != null ? item.PACKAGETYPECODE : "");
                                    mCommand.Parameters.Add("PACKAGETYPEID", item.PACKAGETYPEID != null ? item.PACKAGETYPEID : 0);
                                    mCommand.Parameters.Add("PACKAGETYPENAME", item.PACKAGETYPENAME != null ? item.PACKAGETYPENAME : "");
                                    mCommand.Parameters.Add("QUANTITY", item.QUANTITY != null ? item.QUANTITY : 0);
                                    mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID != null ? entity.QUOTATIONID : 0);
                                    mCommand.Parameters.Add("TOTALCBM", item.TOTALCBM != null ? item.TOTALCBM : 0);
                                    mCommand.Parameters.Add("WEIGHTPERPIECE", item.WEIGHTPERPIECE != null ? item.WEIGHTPERPIECE : 0);
                                    mCommand.Parameters.Add("WEIGHTTOTAL", item.WEIGHTTOTAL != null ? item.WEIGHTTOTAL : 0);
                                    mCommand.Parameters.Add("WEIGHTUOM", item.WEIGHTUOM != null ? item.WEIGHTUOM : 0);
                                    mCommand.Parameters.Add("WEIGHTUOMCODE", item.WEIGHTUOMCODE != null ? item.WEIGHTUOMCODE : "");
                                    mCommand.Parameters.Add("WEIGHTUOMNAME", item.WEIGHTUOMNAME != null ? item.WEIGHTUOMNAME : "");
                                    mCommand.Parameters.Add("CONTAINERCODE", item.CONTAINERCODE != null ? item.CONTAINERCODE : "");
                                    mCommand.ExecuteNonQuery();
                                    mCommand.Parameters.Clear();

                                    TotalCBM = TotalCBM + item.TOTALCBM;
                                    TotalGrossWeight = TotalGrossWeight + item.WEIGHTTOTAL;

                                }
                            }
                            mCommand.Parameters.Clear();
                            mCommand.CommandText = "Update  SSPJOBDETAILS  set TOTALCBM=:TOTALCBM, WEIGHTTOTAL=:WEIGHTTOTAL where JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("TOTALCBM", TotalCBM);
                            mCommand.Parameters.Add("WEIGHTTOTAL", TotalGrossWeight);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }
                        else
                        {
                            mCommand.Parameters.Clear();
                            mCommand.CommandText = "Update  SSPJOBDETAILS  set TOTALCBM=:TOTALCBM, WEIGHTTOTAL=:WEIGHTTOTAL where JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("TOTALCBM", entity.TOTALCBM);
                            mCommand.Parameters.Add("WEIGHTTOTAL", entity.TOTALGROSSWEIGHT);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }




                        //Charge details
                        if (BtnFlag == "ContPayment")
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IP_JOBNUMBER ", entity.JJOBNUMBER).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_QUOTATIONID", entity.QUOTATIONID).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_USERID", UserId).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("OP_MESSAGE", OracleDbType.Varchar2, 10).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTID", OracleDbType.Int64, 18).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTNO", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_JA_INS_RECEIPTDETAILS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();


                            //Inserting into Staging table in the time of payment 
                            //mCommand.Parameters.Clear();
                            //mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JJOBNUMBER);
                            //mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                            //mCommand.CommandType = CommandType.StoredProcedure;
                            //mCommand.ExecuteNonQuery();
                        }
                        mOracleTransaction.Commit();

                        //saving into master client table
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                        mCommand.CommandText = "USP_JP_SAVE_CLIENTMASTER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();


                        //saving HSCODE Details----------------(Added By Anil G)
                        if (!string.IsNullOrEmpty(HSCode))//&& HSCode.IndexOf('$') > -1
                        {

                            string OrgHSCode = HSCode;//.Split('$')[0].ToString();
                            //string DestHSCode = HSCode.Split('$')[1].ToString();

                            if (!string.IsNullOrEmpty(OrgHSCode))
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_QUOTATIONID", null);
                                mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                //mCommand.Parameters.Add("IP_DIRECTION  ", "O");
                                mCommand.Parameters.Add("IP_HSCODE", OrgHSCode);//HSCode.Trim().Replace(" ", string.Empty)
                                mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                                mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                                mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS1";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_HSC_INS_N
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }

                            //if (!string.IsNullOrEmpty(DestHSCode))
                            //{
                            //    mCommand.Parameters.Clear();
                            //    mCommand.Parameters.Add("IP_QUOTATIONID", null);
                            //    mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                            //    mCommand.Parameters.Add("IP_DIRECTION  ", "D");
                            //    mCommand.Parameters.Add("IP_HSCODE", DestHSCode);
                            //    mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                            //    mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                            //    mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_DESTHSC_INS_N
                            //    mCommand.CommandType = CommandType.StoredProcedure;
                            //    mCommand.ExecuteNonQuery();
                            //}

                        }

                        //---------------Save Field Values------------------------
                        if (FieldValues != "")
                        {
                            for (int i = 0; i < FieldValues.Split('_').Length; i++)
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                mCommand.Parameters.Add("IP_FIELDDESCRIPTION", FieldNames.Split('_')[i]);
                                mCommand.Parameters.Add("IP_VALUE", FieldValues.Split('_')[i]);
                                mCommand.Parameters.Add("IP_CREATEDBY", UserId);
                                mCommand.CommandText = "SSP_SAVE_FIELDVALUES";
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }
                        }

                        //--------------------------------------------------------

                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return Convert.ToInt32(Customerid);

        }

        public int UpdateJob1(JobBookingEntity entity, string UserId, string BtnFlag, string HSCode, string FieldValues, string FieldNames)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    entity.SHISDEFAULTADDRESS = 0;
                    decimal TotalCBM = 0;
                    decimal TotalGrossWeight = 0;
                    try
                    {
                        //Job Details 
                        if (entity.JJOBNUMBER > 0)
                        {
                            mCommand.CommandText = "Update  SSPJOBDETAILS  set INCOTERMCODE=:INCOTERMCODE,INCOTERMDESCRIPTION=:INCOTERMDESCRIPTION,CARGOAVAILABLEFROM=:CARGOAVAILABLEFROM,CARGODELIVERYBY=:CARGODELIVERYBY,DOCUMENTSREADY=:DOCUMENTSREADY,SPECIALINSTRUCTIONS=:SPECIALINSTRUCTION where JOBNUMBER=:JOBNUMBER";
                            // mCommand.Parameters.Add("PARTYDETAILSID", entity.CONPARTYDETAILSID);
                            //entity.MODIFIEDBY = "GUEST";
                            //entity.STATEID = "QUOTECREATED";
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("INCOTERMCODE", entity.JINCOTERMCODE);
                            mCommand.Parameters.Add("INCOTERMDESCRIPTION", entity.JINCOTERMDESCRIPTION);
                            if (entity.JCARGOAVAILABLEFROM.ToString() != "1/1/0001 12:00:00 AM")
                                mCommand.Parameters.Add("CARGOAVAILABLEFROM", entity.JCARGOAVAILABLEFROM);
                            else
                                mCommand.Parameters.Add("CARGOAVAILABLEFROM", "");
                            if (entity.JCARGODELIVERYBY.ToString() != "1/1/0001 12:00:00 AM")
                                mCommand.Parameters.Add("CARGODELIVERYBY", entity.JCARGODELIVERYBY);
                            else
                                mCommand.Parameters.Add("CARGODELIVERYBY", "");
                            //mCommand.Parameters.Add("ORIGINCUSTOMSCLEARANCEBY", entity.JORIGINCUSTOMSCLEARANCEBY);
                            //mCommand.Parameters.Add("DESTINATIONCUSTOMSCLEARANCEBY", entity.JDESTINATIONCUSTOMSCLEARANCEBY);
                            mCommand.Parameters.Add("DOCUMENTSREADY", entity.JDOCUMENTSREADY);
                            mCommand.Parameters.Add("SPECIALINSTRUCTION", entity.SPECIALINSTRUCTION);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }


                        mCommand.CommandText = "update SSPPARTYDETAILS  set JOBNUMBER=:JOBNUMBER,CLIENTID=:CLIENTID,CLIENTNAME=:CLIENTNAME,PARTYTYPE=:PARTYTYPE,ADDRESSTYPE=:ADDRESSTYPE,HOUSENO=:HOUSENO,BUILDINGNAME=:BUILDINGNAME,STREET1=:STREET1,STREET2=:STREET2,COUNTRYCODE=:COUNTRYCODE,COUNTRYNAME=:COUNTRYNAME,CITYCODE=:CITYCODE,CITYNAME=:CITYNAME,STATECODE=:STATECODE,STATENAME=:STATENAME,PINCODE=:PINCODE,FULLADDRESS=:FULLADDRESS,ISDEFAULTADDRESS=:ISDEFAULTADDRESS,LATITUDE=:LATITUDE,LONGITUDE=:LONGITUDE,FIRSTNAME=:FIRSTNAME,LASTNAME=:LASTNAME,PHONE=:PHONE,EMAILID=:EMAILID,SALUTATION=:SALUTATION where PARTYDETAILSID=:PARTYDETAILSID";
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PARTYDETAILSID", entity.SHPARTYDETAILSID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CLIENTID", entity.SHCLIENTID);
                        mCommand.Parameters.Add("CLIENTNAME", entity.SHCLIENTNAME);
                        mCommand.Parameters.Add("PARTYTYPE", 91);
                        mCommand.Parameters.Add("ADDRESSTYPE", entity.SHADDRESSTYPE);
                        mCommand.Parameters.Add("HOUSENO", entity.SHHOUSENO);
                        mCommand.Parameters.Add("BUILDINGNAME", entity.SHBUILDINGNAME);
                        mCommand.Parameters.Add("STREET1", entity.SHSTREET1);
                        mCommand.Parameters.Add("STREET2", entity.SHSTREET2);
                        mCommand.Parameters.Add("COUNTRYCODE", string.IsNullOrEmpty(entity.SHCOUNTRYCODE) ? "0" : GetCountryCodeById(Convert.ToInt64(entity.SHCOUNTRYCODE)));
                        mCommand.Parameters.Add("COUNTRYNAME", entity.SHCOUNTRYNAME);
                        mCommand.Parameters.Add("CITYCODE", entity.SHCITYCODE);
                        mCommand.Parameters.Add("CITYNAME", entity.SHCITYNAME);
                        mCommand.Parameters.Add("STATECODE", string.IsNullOrEmpty(entity.SHSTATECODE) ? "0" : GetStateCodeBySubDivisionId(Convert.ToInt64(entity.SHSTATECODE)));
                        mCommand.Parameters.Add("STATENAME", entity.SHSTATENAME);
                        mCommand.Parameters.Add("PINCODE", entity.SHPINCODE);
                        mCommand.Parameters.Add("FULLADDRESS", entity.SHFULLADDRESS);
                        mCommand.Parameters.Add("ISDEFAULTADDRESS", entity.SHISDEFAULTADDRESS);
                        mCommand.Parameters.Add("LATITUDE", entity.SHLATITUDE);
                        mCommand.Parameters.Add("LONGITUDE", entity.SHLONGITUDE);
                        mCommand.Parameters.Add("FIRSTNAME", entity.SHFIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.SHLASTNAME);
                        mCommand.Parameters.Add("PHONE", entity.SHPHONE);
                        mCommand.Parameters.Add("EMAILID", entity.SHEMAILID);
                        mCommand.Parameters.Add("SALUTATION", entity.SHSALUTATION);

                        mCommand.ExecuteNonQuery();
                        mCommand.Parameters.Clear();
                        //}

                        //consignee details
                        //mCommand.CommandText = "SELECT SSP_PARTYDETAILS_SEQ.NEXTVAL FROM DUAL";
                        //entity.SHPARTYDETAILSID = Convert.ToInt64(mCommand.ExecuteScalar());
                        //if (entity.CONISSAVEDORNEWADDRESS == 1)
                        //{
                        mCommand.CommandText = "update SSPPARTYDETAILS  set JOBNUMBER=:JOBNUMBER,CLIENTID=:CLIENTID,CLIENTNAME=:CLIENTNAME,PARTYTYPE=:PARTYTYPE,ADDRESSTYPE=:ADDRESSTYPE,HOUSENO=:HOUSENO,BUILDINGNAME=:BUILDINGNAME,STREET1=:STREET1,STREET2=:STREET2,COUNTRYCODE=:COUNTRYCODE,COUNTRYNAME=:COUNTRYNAME,CITYCODE=:CITYCODE,CITYNAME=:CITYNAME,STATECODE=:STATECODE,STATENAME=:STATENAME,PINCODE=:PINCODE,FULLADDRESS=:FULLADDRESS,ISDEFAULTADDRESS=:ISDEFAULTADDRESS,LATITUDE=:LATITUDE,LONGITUDE=:LONGITUDE,FIRSTNAME=:FIRSTNAME,LASTNAME=:LASTNAME,PHONE=:PHONE,EMAILID=:EMAILID,SALUTATION=:SALUTATION where PARTYDETAILSID=:PARTYDETAILSID";
                        mCommand.Parameters.Add("PARTYDETAILSID", entity.CONPARTYDETAILSID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CLIENTID", entity.CONCLIENTID);
                        mCommand.Parameters.Add("CLIENTNAME", entity.CONCLIENTNAME);
                        mCommand.Parameters.Add("PARTYTYPE", 92);
                        mCommand.Parameters.Add("ADDRESSTYPE", entity.CONADDRESSTYPE);
                        mCommand.Parameters.Add("HOUSENO", entity.CONHOUSENO);
                        mCommand.Parameters.Add("BUILDINGNAME", entity.CONBUILDINGNAME);
                        mCommand.Parameters.Add("STREET1", entity.CONSTREET1);
                        mCommand.Parameters.Add("STREET2", entity.CONSTREET2);
                        mCommand.Parameters.Add("COUNTRYCODE", string.IsNullOrEmpty(entity.CONCOUNTRYCODE) ? "0" : GetCountryCodeById(Convert.ToInt64(entity.CONCOUNTRYCODE)));
                        mCommand.Parameters.Add("COUNTRYNAME", entity.CONCOUNTRYNAME);
                        mCommand.Parameters.Add("CITYCODE", entity.CONCITYCODE);
                        mCommand.Parameters.Add("CITYNAME", entity.CONCITYNAME);
                        mCommand.Parameters.Add("STATECODE", string.IsNullOrEmpty(entity.CONSTATECODE) ? "0" : GetStateCodeBySubDivisionId(Convert.ToInt64(entity.CONSTATECODE)));
                        mCommand.Parameters.Add("STATENAME", entity.CONSTATENAME);
                        mCommand.Parameters.Add("PINCODE", entity.CONPINCODE);
                        mCommand.Parameters.Add("FULLADDRESS", entity.CONFULLADDRESS);
                        mCommand.Parameters.Add("ISDEFAULTADDRESS", entity.CONISDEFAULTADDRESS);
                        mCommand.Parameters.Add("LATITUDE", entity.CONLATITUDE);
                        mCommand.Parameters.Add("LONGITUDE", entity.CONLONGITUDE);
                        mCommand.Parameters.Add("FIRSTNAME", entity.CONFIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.CONLASTNAME);
                        mCommand.Parameters.Add("PHONE", entity.CONPHONE);
                        mCommand.Parameters.Add("EMAILID", entity.CONEMAILID);
                        mCommand.Parameters.Add("SALUTATION", entity.CONSALUTATION);
                        mCommand.ExecuteNonQuery();

                        mCommand.Parameters.Clear();

                        if (entity.ShipmentItems.Count() > 0)
                        {
                            mCommand.CommandText = "Delete from SSPJOBSHIPMENTITEM where QUOTATIONID=:QUOTATIONID and JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }


                        if (entity.ShipmentItems.Count() > 0)
                        {
                            foreach (var item in entity.ShipmentItems)
                            {
                                if (item.ITEMHEIGHT != 0)
                                {

                                    mCommand.CommandText = "SELECT SSPJOBSHIPMENTITEM_SEQ.NEXTVAL FROM DUAL";
                                    long JOBSHIPMENTITEMID = Convert.ToInt64(mCommand.ExecuteScalar());

                                    //mCommand.CommandText = "Insert into SSPJOBSHIPMENTITEM (JOBSHIPMENTITEMID,CONTAINERID,CONTAINERNAME,ITEMDESCRIPTION,ITEMHEIGHT,ITEMLENGTH,ITEMWIDTH,JOBSHIPMENTID,LENGTHUOM,LENGTHUOMCODE,LENGTHUOMNAME,PACKAGETYPECODE,PACKAGETYPEID,PACKAGETYPENAME,QUANTITY,QUOTATIONID,TOTALCBM,WEIGHTPERPIECE,WEIGHTTOTAL,WEIGHTUOM,WEIGHTUOMCODE,WEIGHTUOMNAME) values (:JOBSHIPMENTITEMID,:CONTAINERID,:CONTAINERNAME,:ITEMDESCRIPTION,:ITEMHEIGHT,:PACKAGETYPECODE,:PACKAGETYPEID,:PACKAGETYPENAME,:QUANTITY,:QUOTATIONID,:TOTALCBM,:WEIGHTPERPIECE,:WEIGHTTOTAL,:WEIGHTUOM,:WEIGHTUOMCODE,:WEIGHTUOMNAME)";
                                    mCommand.CommandText = @"Insert into SSPJOBSHIPMENTITEM (JOBSHIPMENTITEMID,CONTAINERID,CONTAINERNAME,
                                ITEMDESCRIPTION,ITEMHEIGHT,ITEMLENGTH,ITEMWIDTH,JOBSHIPMENTID,LENGTHUOM,LENGTHUOMCODE,LENGTHUOMNAME,PACKAGETYPECODE,
                                PACKAGETYPEID,PACKAGETYPENAME,QUANTITY,QUOTATIONID,TOTALCBM,WEIGHTPERPIECE,WEIGHTTOTAL,WEIGHTUOM,WEIGHTUOMCODE,WEIGHTUOMNAME,JOBNUMBER,CONTAINERCODE) 
                                values (:JOBSHIPMENTITEMID,:CONTAINERID,:CONTAINERNAME,:ITEMDESCRIPTION,:ITEMHEIGHT,:ITEMLENGTH,:ITEMWIDTH,:JOBSHIPMENTID,
                                :LENGTHUOM,:LENGTHUOMCODE,:LENGTHUOMNAME,:PACKAGETYPECODE,:PACKAGETYPEID,:PACKAGETYPENAME,:QUANTITY,
                                :QUOTATIONID,:TOTALCBM,:WEIGHTPERPIECE,:WEIGHTTOTAL,:WEIGHTUOM,:WEIGHTUOMCODE,:WEIGHTUOMNAME,:JOBNUMBER,:CONTAINERCODE)";

                                    mCommand.Parameters.Add("JOBSHIPMENTITEMID", JOBSHIPMENTITEMID);

                                    mCommand.Parameters.Add("CONTAINERID", item.CONTAINERID != null ? item.CONTAINERID : 0);
                                    if (item.CONTAINERNAME != null && item.CONTAINERNAME.Contains("'"))
                                    {
                                        item.CONTAINERNAME = item.CONTAINERNAME.Replace("'", "-");
                                    }
                                    mCommand.Parameters.Add("CONTAINERNAME", item.CONTAINERNAME != null ? item.CONTAINERNAME : "");
                                    mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                                    mCommand.Parameters.Add("ITEMDESCRIPTION", item.ITEMDESCRIPTION != null ? item.ITEMDESCRIPTION : "");
                                    mCommand.Parameters.Add("ITEMHEIGHT", item.ITEMHEIGHT != null ? item.ITEMHEIGHT : 0);
                                    mCommand.Parameters.Add("ITEMLENGTH", item.ITEMLENGTH != null ? item.ITEMLENGTH : 0);
                                    mCommand.Parameters.Add("ITEMWIDTH", item.ITEMWIDTH != null ? item.ITEMWIDTH : 0);
                                    mCommand.Parameters.Add("JOBSHIPMENTID", item.SHIPMENTITEMID != null ? item.SHIPMENTITEMID : 0);
                                    mCommand.Parameters.Add("LENGTHUOM", item.LENGTHUOM != null ? item.LENGTHUOM : 0);
                                    mCommand.Parameters.Add("LENGTHUOMCODE", item.LENGTHUOMCODE != null ? item.LENGTHUOMCODE : "");
                                    mCommand.Parameters.Add("LENGTHUOMNAME", item.LENGTHUOMNAME != null ? item.LENGTHUOMNAME : "");
                                    mCommand.Parameters.Add("PACKAGETYPECODE", item.PACKAGETYPECODE != null ? item.PACKAGETYPECODE : "");
                                    mCommand.Parameters.Add("PACKAGETYPEID", item.PACKAGETYPEID != null ? item.PACKAGETYPEID : 0);
                                    mCommand.Parameters.Add("PACKAGETYPENAME", item.PACKAGETYPENAME != null ? item.PACKAGETYPENAME : "");
                                    mCommand.Parameters.Add("QUANTITY", item.QUANTITY != null ? item.QUANTITY : 0);
                                    mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID != null ? entity.QUOTATIONID : 0);
                                    mCommand.Parameters.Add("TOTALCBM", item.TOTALCBM != null ? item.TOTALCBM : 0);
                                    mCommand.Parameters.Add("WEIGHTPERPIECE", item.WEIGHTPERPIECE != null ? item.WEIGHTPERPIECE : 0);
                                    mCommand.Parameters.Add("WEIGHTTOTAL", item.WEIGHTTOTAL != null ? item.WEIGHTTOTAL : 0);
                                    mCommand.Parameters.Add("WEIGHTUOM", item.WEIGHTUOM != null ? item.WEIGHTUOM : 0);
                                    mCommand.Parameters.Add("WEIGHTUOMCODE", item.WEIGHTUOMCODE != null ? item.WEIGHTUOMCODE : "");
                                    mCommand.Parameters.Add("WEIGHTUOMNAME", item.WEIGHTUOMNAME != null ? item.WEIGHTUOMNAME : "");
                                    mCommand.Parameters.Add("CONTAINERCODE", item.CONTAINERCODE != null ? item.CONTAINERCODE : "");
                                    mCommand.ExecuteNonQuery();
                                    mCommand.Parameters.Clear();
                                    TotalCBM = TotalCBM + item.TOTALCBM;
                                    TotalGrossWeight = TotalGrossWeight + item.WEIGHTTOTAL;
                                }
                            }
                            mCommand.CommandText = "Update  SSPJOBDETAILS  set TOTALCBM=:TOTALCBM, WEIGHTTOTAL=:WEIGHTTOTAL where JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("TOTALCBM", TotalCBM);
                            mCommand.Parameters.Add("WEIGHTTOTAL", TotalGrossWeight);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }
                        else
                        {
                            mCommand.CommandText = "Update  SSPJOBDETAILS  set TOTALCBM=:TOTALCBM, WEIGHTTOTAL=:WEIGHTTOTAL where JOBNUMBER=:JOBNUMBER";
                            mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("TOTALCBM", entity.TOTALCBM);
                            mCommand.Parameters.Add("WEIGHTTOTAL", entity.TOTALGROSSWEIGHT);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }

                        //Charge details
                        if (BtnFlag == "ContPayment")
                        {
                            // Inserting into staging table in the time of payment 
                            //mCommand.Parameters.Clear();
                            //mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JJOBNUMBER);
                            //mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                            //mCommand.CommandType = CommandType.StoredProcedure;
                            //mCommand.ExecuteNonQuery();

                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IP_JOBNUMBER ", entity.JJOBNUMBER).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_QUOTATIONID", entity.QUOTATIONID).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_USERID", UserId).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("OP_MESSAGE", OracleDbType.Varchar2, 10).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTID", OracleDbType.Int64, 18).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTNO", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_JA_INS_RECEIPTDETAILS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }
                        mOracleTransaction.Commit();

                        //saving into master client table
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                        mCommand.CommandText = "USP_JP_SAVE_CLIENTMASTER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();


                        //saving HSCODE Details----------------(Added By Anil G)
                        if (!string.IsNullOrEmpty(HSCode))//&& HSCode.IndexOf('$') > -1
                        {

                            string OrgHSCode = HSCode;//.Split('$')[0].ToString();
                            //string DestHSCode = HSCode.Split('$')[1].ToString();

                            if (!string.IsNullOrEmpty(OrgHSCode))
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_QUOTATIONID", null);
                                mCommand.Parameters.Add("IP_JOBNUMBER", entity.JJOBNUMBER);
                                //mCommand.Parameters.Add("IP_DIRECTION", "O");
                                mCommand.Parameters.Add("IP_HSCODE", OrgHSCode);//HSCode.Trim().Replace(" ", string.Empty)
                                mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                                mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                                mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS1";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_HSC_INS_N
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }

                            //if (!string.IsNullOrEmpty(DestHSCode))
                            //{
                            //    mCommand.Parameters.Clear();
                            //    mCommand.Parameters.Add("IP_QUOTATIONID", null);
                            //    mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                            //    mCommand.Parameters.Add("IP_DIRECTION  ", "D");
                            //    mCommand.Parameters.Add("IP_HSCODE", DestHSCode);
                            //    mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                            //    mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                            //    mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_DESTHSC_INS_N
                            //    mCommand.CommandType = CommandType.StoredProcedure;
                            //    mCommand.ExecuteNonQuery();
                            //}

                        }

                        //---------------Save Field Values------------------------
                        if (FieldValues != "")
                        {
                            for (int i = 0; i < FieldValues.Split('_').Length; i++)
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                mCommand.Parameters.Add("IP_FIELDDESCRIPTION", FieldNames.Split('_')[i]);
                                mCommand.Parameters.Add("IP_VALUE", FieldValues.Split('_')[i]);
                                mCommand.Parameters.Add("IP_CREATEDBY", UserId);
                                mCommand.CommandText = "SSP_SAVE_FIELDVALUES";
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }
                        }

                        //--------------------------------------------------------
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return Convert.ToInt32(entity.QUOTATIONID);
        }

        #endregion

        public JobBookingEntity GetById(int id)
        {
            JobBookingEntity mEntity = new JobBookingEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_INDEXGETBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<JobBookingEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPDocuments = ((OracleRefCursor)mCommand.Parameters["CUR_SSPDOCUMENTS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPDocuments);
                        List<DBFactory.Entities.SSPDocumentsEntity> mSSPDocuments = MyClass.ConvertToList<DBFactory.Entities.SSPDocumentsEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtSSPDocuments.Close();
                        mConnection.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.DocumentDetails = mSSPDocuments;

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public JobBookingEntity GetById(int id, string UserName, SSPJobDetailsEntity JOBDATA)
        {
            JobBookingEntity mEntity = new JobBookingEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("L_JOBNUMBER",JOBDATA.JOBNUMBER);
                        
                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JOBSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("CUR_SCHEDULES", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        // mCommand.Parameters.Add("CUR_HSCODE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_INDEXGETBYID";
                        //mCommand.CommandText = "USP_GET_INDEXGETBYID_TEST";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<JobBookingEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_JOBSERVICECHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.PaymentChargesEntity> mQuotationCharges = MyClass.ConvertToList<DBFactory.Entities.PaymentChargesEntity>(dtQuotation);

                        //dtQuotation = new DataTable();
                        //OracleDataReader dtSSPDocuments = ((OracleRefCursor)mCommand.Parameters["CUR_SSPDOCUMENTS"].Value).GetDataReader();
                        //dtQuotation.Load(dtSSPDocuments);
                        //List<DBFactory.Entities.SSPDocumentsEntity> mSSPDocuments = MyClass.ConvertToList<DBFactory.Entities.SSPDocumentsEntity>(dtQuotation);

                        //dtQuotation = new DataTable();
                        //OracleDataReader dtSSPSchedules = ((OracleRefCursor)mCommand.Parameters["CUR_SCHEDULES"].Value).GetDataReader();
                        //dtQuotation.Load(dtSSPSchedules);
                        //List<DBFactory.Entities.ScheduleEntity> mSSPSchedules = MyClass.ConvertToList<DBFactory.Entities.ScheduleEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        //dtSSPDocuments.Close();
                        //dtSSPSchedules.Close();
                        mConnection.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.PaymentCharges = mQuotationCharges;
                        //mEntity.DocumentDetails = mSSPDocuments;
                        //mEntity.ScheduleDetails = mSSPSchedules;

                        //----------------UPDATE job DATA TO ENTITY----------------------------------------------------
                        mEntity.JJOBNUMBER = JOBDATA.JOBNUMBER;
                        mEntity.JOPERATIONALJOBNUMBER = JOBDATA.OPERATIONALJOBNUMBER;
                        mEntity.STATEID = JOBDATA.STATEID;

                        //Insert Docs & Fields into Shipa Tables from focis DB
                        mEntity = InsertCountryBasedDocsFIELDS(Convert.ToInt64(mEntity.OCOUNTRYID), Convert.ToInt64(mEntity.DCOUNTRYID), JOBDATA.JOBNUMBER.ToString(), mEntity.QUOTATIONID.ToString(), UserName, mEntity);

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public DataTable AlipayInit(Int64 id, string discountcode, string servername)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            DataTable dtMessage = new DataTable();
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_JOBNUMBER", id);
                        mCommand.Parameters.Add("PRM_DISCOUNTCODE", discountcode);
                        mCommand.Parameters.Add("PRM_SERVERNAME", servername);

                        mCommand.Parameters.Add("CUR_SSPALIPAYAMOUNT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSP_USP_ALIPAYINITIALIZE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        OracleDataReader AMOUNT = ((OracleRefCursor)mCommand.Parameters["CUR_SSPALIPAYAMOUNT"].Value).GetDataReader();
                        dtMessage.Load(AMOUNT);
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return dtMessage;
        }

        public void SaveStripeTransactionDetails(string jobnumber, string status, string amount, string currency, string payoption
            , string token, string chargeid, string tranid, string pcurrency, string ptotal, string pnet, string sfee, string pstatus, string exchangerate, string paccount
            )
        {
            string serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_JOBNUMBER", jobnumber);
                        mCommand.Parameters.Add("PRM_EXCHANGERATE", exchangerate);
                        mCommand.Parameters.Add("PRM_STATUS", status);
                        mCommand.Parameters.Add("PRM_AMOUNT", amount);
                        mCommand.Parameters.Add("PRM_CURRENCY", currency);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", payoption);
                        mCommand.Parameters.Add("PRM_TOKENID", token);
                        mCommand.Parameters.Add("PRM_CHARGEID", chargeid);
                        mCommand.Parameters.Add("PRM_TRANSACTIONID", tranid);
                        mCommand.Parameters.Add("PRM_PAYOUTCURRENCY", pcurrency);
                        mCommand.Parameters.Add("PRM_PAYOUTTOTAL", ptotal);
                        mCommand.Parameters.Add("PRM_PAYOUTNET", pnet);
                        mCommand.Parameters.Add("PRM_STRIPEFEE", sfee);
                        mCommand.Parameters.Add("PRM_PAYOUTSTATUS", pstatus);
                        mCommand.Parameters.Add("PRM_STRIPEACCOUNT", paccount);
                        mCommand.CommandText = "USP_TransactionDetails";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        private void PushCountryBasedIns(long jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();

                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.Parameters.Clear();
                    mCommand.Parameters.Add("IP_JOBNUMBER", Convert.ToInt64(jobnumber));
                    mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                    mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                    mCommand.Parameters.Add("OP_INSTRUCTION_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    mCommand.CommandText = "USP_SSP_SAVE_COUNTRY_INSTR";
                    mCommand.CommandType = CommandType.StoredProcedure;
                    mCommand.ExecuteNonQuery();

                    mOracleTransaction.Commit();

                }
            }

        }

        private void UpdateHSCodeDetails(Int64 QuotationId, string UserName, Int64 jobnumber)
        {
            string Query = "select * from HSCODEDETAILS where QUOTATIONID= :QUOTATIONID and jobnumber=:JOBNUMBER";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
	            new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = QuotationId  },
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER" , Value = 0 }
            });

            if (dt.Rows.Count > 0)
            {
                //If current quotation have any hscodedeatils then Update jobnumber---------(Anil.G)
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();

                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        //saving HSCODE Details----------------(Added By Anil G)
                        if (!string.IsNullOrEmpty(dt.Rows[0]["HSCODE"].ToString()))
                        {

                            //if (dt.Rows[0]["HSCODE"].ToString().IndexOf(',') > 0)
                            //{
                            //    //string[] HSCodeArray = new string[HSCode.Split(',').Length];
                            //    for (int r = 0; r < dt.Rows[0]["HSCODE"].ToString().Split(',').Length; r++)
                            //    {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IP_QUOTATIONID", null);
                            mCommand.Parameters.Add("IP_JOBNUMBER", Convert.ToInt64(jobnumber));
                            mCommand.Parameters.Add("IP_HSCODE", dt.Rows[0]["HSCODE"].ToString().Trim().Replace(" ", string.Empty));//
                            mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_JP_SAVE_HSC_INS_N";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();

                            mOracleTransaction.Commit();
                            //    }
                            //}
                        }
                    }

                }
            }
        }
        //Creating new record in JobDetails Table

        public SSPJobDetailsEntity CreateNewJobDetails(long QuotationId, string QuotationNumber, string UserName, decimal GrandTotal)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        JobBookingEntity entity = new JobBookingEntity();
                        mCommand.CommandText = "SELECT COUNT(*) FROM SSPJOBDETAILS WHERE QUOTATIONID=:QUOTATIONID";
                        mCommand.Parameters.Add("QUOTATIONID", QuotationId);
                        long quotecount = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.Parameters.Clear();

                        mCommand.CommandText = "SELECT SSP_JOBDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.JJOBNUMBER = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Insert into SSPJOBDETAILS (JOBNUMBER,OPERATIONALJOBNUMBER,QUOTATIONID,QUOTATIONNUMBER,STATEID,CREATEDBY,JOBGRANDTOTAL) values (:JOBNUMBER,:OPERATIONALJOBNUMBER,:QUOTATIONID,:QUOTATIONNUMBER,:STATEID,:CREATEDBY,:JOBGRANDTOTAL)";
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CREATEDBY", UserName);
                        quotecount = quotecount + 1;
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "Job Initiated";
                        string opjobnumber = "A" + quotecount.ToString("00") + QuotationNumber.ToString().Replace("QT", "");
                        mCommand.Parameters.Add("OPERATIONALJOBNUMBER", opjobnumber);
                        mCommand.Parameters.Add("QUOTATIONID", QuotationId);
                        mCommand.Parameters.Add("QUOTATIONNUMBER", QuotationNumber);
                        mCommand.Parameters.Add("STATEID", entity.STATEID);
                        mCommand.Parameters.Add("JOBGRANDTOTAL", GrandTotal);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        SSPJobDetailsEntity job = new SSPJobDetailsEntity();
                        job.QUOTATIONID = QuotationId;
                        job.QUOTATIONNUMBER = QuotationNumber;
                        job.JOBNUMBER = entity.JJOBNUMBER;
                        job.OPERATIONALJOBNUMBER = opjobnumber;
                        job.STATEID = entity.STATEID;
                        return job;

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public int SaveJob(JobBookingEntity entity, string UserId, string BtnFlag, string HSCode, string DestHSCode, string FieldValues, string FieldIDS)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            long Customerid = 0;
            decimal TotalCBM = 0;
            decimal TotalGrossWeight = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    entity.SHISDEFAULTADDRESS = 0;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("IN_INCOTERMCODE", entity.JINCOTERMCODE);
                        mCommand.Parameters.Add("IN_INCOTERMDESCRIPTION", entity.JINCOTERMDESCRIPTION);
                        if (entity.JCARGOAVAILABLEFROM.ToString() != "1/1/0001 12:00:00 AM")
                            mCommand.Parameters.Add("IN_CARGOAVAILABLEFROM", entity.JCARGOAVAILABLEFROM);
                        else
                            mCommand.Parameters.Add("IN_CARGOAVAILABLEFROM", "");
                        if (entity.JCARGODELIVERYBY.ToString() != "1/1/0001 12:00:00 AM")
                            mCommand.Parameters.Add("IN_CARGODELIVERYBY", entity.JCARGODELIVERYBY);
                        else
                            mCommand.Parameters.Add("IN_CARGODELIVERYBY", "");

                        mCommand.Parameters.Add("IN_DOCUMENTSREADY", entity.JDOCUMENTSREADY);
                        mCommand.Parameters.Add("IN_SPECIALINSTRUCTION", entity.SPECIALINSTRUCTION);
                        mCommand.Parameters.Add("IN_CREATEDBY", UserId);
                        mCommand.Parameters.Add("IN_SHPARTYID", entity.SHCLIENTID);
                        mCommand.Parameters.Add("IN_CONPARTYID", entity.CONCLIENTID);
                        mCommand.Parameters.Add("IN_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("IN_LIVEUPLOADS", entity.LIVEUPLOADS);
                        mCommand.Parameters.Add("IN_LOADINGDOCKAVAILABLE", entity.LOADINGDOCKAVAILABLE);
                        mCommand.Parameters.Add("IN_CARGOPALLETIZED", entity.CARGOPALLETIZED);
                        mCommand.Parameters.Add("IN_ORIGINALDOCUMENTSREQUIRED", entity.ORIGINALDOCUMENTSREQUIRED);
                        mCommand.Parameters.Add("IN_TEMPERATURECONTROLREQUIRED", entity.TEMPERATURECONTROLREQUIRED);
                        mCommand.Parameters.Add("IN_COMMERCIALPICKUPLOCATION", entity.COMMERCIALPICKUPLOCATION);
                        mCommand.CommandText = "USP_SSP_SAVEJOB";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        if (entity.ShipmentItems.Count() > 0)
                        {
                            foreach (var item in entity.ShipmentItems)
                            {
                                if (item.TOTALCBM >= 0)
                                {
                                    mCommand.Parameters.Clear();
                                    mCommand.Parameters.Add("IN_CONTAINERID", item.CONTAINERID != null ? item.CONTAINERID : 0);
                                    if (item.CONTAINERNAME != null && item.CONTAINERNAME.Contains("'"))
                                    {
                                        item.CONTAINERNAME = item.CONTAINERNAME.Replace("'", "-");
                                    }
                                    mCommand.Parameters.Add("IN_CONTAINERNAME", item.CONTAINERNAME != null ? item.CONTAINERNAME : "");
                                    mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                                    mCommand.Parameters.Add("IN_ITEMDESCRIPTION", item.ITEMDESCRIPTION != null ? item.ITEMDESCRIPTION : "");
                                    mCommand.Parameters.Add("IN_ITEMHEIGHT", item.ITEMHEIGHT != null ? item.ITEMHEIGHT : 0);
                                    mCommand.Parameters.Add("IN_ITEMLENGTH", item.ITEMLENGTH != null ? item.ITEMLENGTH : 0);
                                    mCommand.Parameters.Add("IN_ITEMWIDTH", item.ITEMWIDTH != null ? item.ITEMWIDTH : 0);
                                    mCommand.Parameters.Add("IN_JOBSHIPMENTID", item.SHIPMENTITEMID != null ? item.SHIPMENTITEMID : 0);
                                    mCommand.Parameters.Add("IN_LENGTHUOM", item.LENGTHUOM != null ? item.LENGTHUOM : 0);
                                    mCommand.Parameters.Add("IN_LENGTHUOMCODE", item.LENGTHUOMCODE != null ? item.LENGTHUOMCODE : "");
                                    mCommand.Parameters.Add("IN_LENGTHUOMNAME", item.LENGTHUOMNAME != null ? item.LENGTHUOMNAME : "");
                                    mCommand.Parameters.Add("IN_PACKAGETYPECODE", item.PACKAGETYPECODE != null ? item.PACKAGETYPECODE : "");
                                    mCommand.Parameters.Add("IN_PACKAGETYPEID", item.PACKAGETYPEID != null ? item.PACKAGETYPEID : 0);
                                    mCommand.Parameters.Add("IN_PACKAGETYPENAME", item.PACKAGETYPENAME != null ? item.PACKAGETYPENAME : "");
                                    mCommand.Parameters.Add("IN_QUANTITY", item.QUANTITY != null ? item.QUANTITY : 0);
                                    mCommand.Parameters.Add("IN_QUOTATIONID", entity.QUOTATIONID != null ? entity.QUOTATIONID : 0);
                                    mCommand.Parameters.Add("IN_TOTALCBM", item.TOTALCBM != null ? item.TOTALCBM : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTPERPIECE", item.WEIGHTPERPIECE != null ? item.WEIGHTPERPIECE : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTTOTAL", item.WEIGHTTOTAL != null ? item.WEIGHTTOTAL : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTUOM", item.WEIGHTUOM != null ? item.WEIGHTUOM : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTUOMCODE", item.WEIGHTUOMCODE != null ? item.WEIGHTUOMCODE : "");
                                    mCommand.Parameters.Add("IN_WEIGHTUOMNAME", item.WEIGHTUOMNAME != null ? item.WEIGHTUOMNAME : "");
                                    mCommand.Parameters.Add("IN_CONTAINERCODE", item.CONTAINERCODE != null ? item.CONTAINERCODE : "");

                                    mCommand.Parameters.Add("IN_TOTALWGTCBM", item.TotalWgtCBM != null ? item.TotalWgtCBM : 0);
                                    mCommand.Parameters.Add("IN_TOTALWGTKG", item.TotalWgtKG != null ? item.TotalWgtKG : 0);
                                    mCommand.Parameters.Add("IN_TOTALWGTTON", item.TotalWgtTON != null ? item.TotalWgtTON : 0);

                                    mCommand.CommandText = "USP_SAVE_SSPJOBSHIPMENTITEM";
                                    mCommand.CommandType = CommandType.StoredProcedure;
                                    mCommand.ExecuteNonQuery();

                                    TotalCBM = TotalCBM + item.TOTALCBM;
                                    TotalGrossWeight = TotalGrossWeight + item.WEIGHTTOTAL;
                                }
                            }
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("IN_TOTALCBM", TotalCBM);
                            mCommand.Parameters.Add("IN_WEIGHTTOTAL", TotalGrossWeight);
                            mCommand.CommandText = "USP_SSP_TOTALCBMWEIGHT";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("IN_TOTALCBM", entity.TOTALCBM);
                            mCommand.Parameters.Add("IN_WEIGHTTOTAL", entity.TOTALGROSSWEIGHT);
                            mCommand.CommandText = "USP_SSP_TOTALCBMWEIGHT";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }

                        //Charge details
                        if (BtnFlag == "ContPayment")
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IP_JOBNUMBER ", entity.JJOBNUMBER).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_QUOTATIONID", entity.QUOTATIONID).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_USERID", UserId).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("OP_MESSAGE", OracleDbType.Varchar2, 10).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTID", OracleDbType.Int64, 18).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTNO", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_JA_INS_RECEIPTDETAILS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }

                        //saving HSCODE Details----------------(Added By Anil G)
                        if (!string.IsNullOrEmpty(HSCode))//&& HSCode.IndexOf('$') > -1
                        {

                            string OrgHSCode = HSCode;//.Split('$')[0].ToString();
                            //string DestHSCode = HSCode.Split('$')[1].ToString();

                            if (!string.IsNullOrEmpty(OrgHSCode))
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_QUOTATIONID", null);
                                mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                mCommand.Parameters.Add("IP_DIRECTION  ", "O");
                                mCommand.Parameters.Add("IP_HSCODE", OrgHSCode);//HSCode.Trim().Replace(" ", string.Empty)
                                mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                                mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                                mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_HSC_INS_N
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }

                            if (!string.IsNullOrEmpty(DestHSCode))
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_QUOTATIONID", null);
                                mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                mCommand.Parameters.Add("IP_DIRECTION  ", "D");
                                mCommand.Parameters.Add("IP_HSCODE", DestHSCode);
                                mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                                mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                                mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_DESTHSC_INS_N
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }

                        }

                        //---------------Save Field Values------------------------
                        if (FieldValues != "" && FieldValues != null)
                        {
                            for (int i = 0; i < FieldValues.Split('_').Length; i++)
                            {
                                //mCommand.Parameters.Clear();
                                //mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                //mCommand.Parameters.Add("IP_FIELDDESCRIPTION", FieldNames.Split('_')[i]);
                                //mCommand.Parameters.Add("IP_VALUE", FieldValues.Split('_')[i]);
                                //mCommand.Parameters.Add("IP_CREATEDBY", UserId);
                                //mCommand.CommandText = "SSP_SAVE_FIELDVALUES";
                                //mCommand.CommandType = CommandType.StoredProcedure;
                                //mCommand.ExecuteNonQuery();

                                mCommand.Parameters.Clear();
                                mCommand.CommandType = CommandType.Text;
                                mCommand.CommandText = "UPDATE SSP_FIELD_VALUES SET VALUE=:IP_VALUE WHERE FIELDID=:FIELDID";
                                mCommand.Parameters.Add("IP_VALUE", FieldValues.Split('_')[i]);
                                mCommand.Parameters.Add("FIELDID", FieldIDS.Split('_')[i]);
                                mCommand.ExecuteNonQuery();
                            }
                        }
                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return Convert.ToInt32(Customerid);
        }

        public PaymentEntity GetPaymentDetailsByJobNumber(int id, string userid)
        {
            PaymentEntity mEntity = new PaymentEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    //mCommand.CommandType = CommandType.Text;
                    //mCommand.CommandText = "UPDATE SSPDOCUMENTS SET PAGEFLAG=:PAGEFLAG WHERE PAGEFLAG=1 and JOBNUMBER=:JOBNUMBER";
                    //mCommand.Parameters.Add("PAGEFLAG", null);
                    //mCommand.Parameters.Add("JOBNUMBER", id);
                    //mCommand.ExecuteNonQuery();


                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        //Update documents provider name based on selected party -- Anil g
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_JOBNUMBER", id);
                        mCommand.CommandText = "SSPUPDATESAVEFORLATER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        //--------------------------------------------------------------------

                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_JOBNUMBER", id);
                        mCommand.Parameters.Add("L_USERID", userid.ToUpper());
                        //mCommand.Parameters.Add("PRM_COUPONCODE", "");

                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("CUR_SSPPAYMENTBRANCHLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPAYMENTENTITYLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_PAYMENTOPTIONLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_PAYMENTVIEW";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtPaymentDetails = new DataTable();
                        OracleDataReader Jareceiptheader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtPaymentDetails.Load(Jareceiptheader);
                        mEntity = MyClass.ConvertToEntity<PaymentEntity>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPJobdetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPJobdetails);
                        DBFactory.Entities.PaymentEntity mSSPJobdetails = MyClass.ConvertToEntity<PaymentEntity>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPPaymentOptionlist = ((OracleRefCursor)mCommand.Parameters["CUR_PAYMENTOPTIONLIST"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPPaymentOptionlist);
                        DBFactory.Entities.SSPPaymentOptionList mSSPPaymentOptionList = MyClass.ConvertToEntity<SSPPaymentOptionList>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPPaymententitylist = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPAYMENTENTITYLIST"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPPaymententitylist);
                        DBFactory.Entities.SSPEntityListEntity mSSPPaymententitylist = MyClass.ConvertToEntity<SSPEntityListEntity>(dtPaymentDetails);




                        Jareceiptheader.Close();
                        SSPJobdetails.Close();
                        SSPPaymententitylist.Close();
                        SSPPaymentOptionlist.Close();
                        mConnection.Close();

                        mEntity.CARGOAVAILABLEFROM = mSSPJobdetails.CARGOAVAILABLEFROM;
                        mEntity.ADDITIONALCHARGEAMOUNT = mSSPJobdetails.ADDITIONALCHARGEAMOUNT;
                        mEntity.ADDITIONALCHARGESTATUS = mSSPJobdetails.ADDITIONALCHARGESTATUS;
                        mEntity.ADDITIONALCHARGEAMOUNTINUSD = mSSPJobdetails.ADDITIONALCHARGEAMOUNTINUSD;
                        mEntity.ENTITYLIST = mSSPPaymententitylist;
                        mEntity.PAYMENTOPTIONLIST = mSSPPaymentOptionList;
                        mEntity.CHARGESETID = mSSPJobdetails.CHARGESETID;
                        mEntity.REFERREREMAILID = mSSPJobdetails.REFERREREMAILID == null ? null : mSSPJobdetails.REFERREREMAILID;
                        mEntity.REFERRERMODEL = mSSPJobdetails.REFERRERMODEL == null ? null : mSSPJobdetails.REFERRERMODEL;
                        mEntity.REFERRERCOUPONCODE = mSSPJobdetails.REFERRERCOUPONCODE == null ? null : mSSPJobdetails.REFERRERCOUPONCODE;
                        mEntity.REFERRERBALAMOUNT = mSSPJobdetails.REFERRERBALAMOUNT;
                        mEntity.REFERRERBALCOUPONCOUNT = mSSPJobdetails.REFERRERBALCOUPONCOUNT;

                        mEntity.LIVEUPLOADS = mSSPJobdetails.LIVEUPLOADS;
                        mEntity.LOADINGDOCKAVAILABLE = mSSPJobdetails.LOADINGDOCKAVAILABLE;
                        mEntity.CARGOPALLETIZED = mSSPJobdetails.CARGOPALLETIZED;
                        mEntity.ORIGINALDOCUMENTSREQUIRED = mSSPJobdetails.ORIGINALDOCUMENTSREQUIRED;
                        mEntity.TEMPERATURECONTROLREQUIRED = mSSPJobdetails.TEMPERATURECONTROLREQUIRED;
                        mEntity.COMMERCIALPICKUPLOCATION = mSSPJobdetails.COMMERCIALPICKUPLOCATION;
                        mEntity.SPECIALINSTRUCTIONS = mSSPJobdetails.SPECIALINSTRUCTIONS;

                        if (!string.IsNullOrEmpty(userid))
                        {
                            ReadText(DBConnectionMode.SSP, "SELECT NVL(FOCIS.RATECALC_GET_EXCHANGE_RATE(CRDTAPP.CURRENTOSBALANCE,CRDTAPP.HFMENTRYCODE,RECPHDR.CURRENCY),0) CURRENTOSBALANCE,NVL(RECPHDR.CURRENCY,'USD') AS HFMENTRYCODE  FROM JARECEIPTHEADER RECPHDR LEFT JOIN SSPCREDITAPPLICATIONS CRDTAPP ON upper(RECPHDR.CREATEDBY) = upper(CRDTAPP.USERID) WHERE CRDTAPP.STATEID='Approved' AND UPPER(CRDTAPP.USERID)=:USERID AND RECPHDR.JOBNUMBER=:JOBNUMBER",
                            new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = userid.ToString().ToUpper() } ,
                                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = id }
                            }
                            , delegate(IDataReader r)
                            {
                                mEntity.CREDITAMOUNT = Convert.ToDouble(r["CURRENTOSBALANCE"]);
                                if (Convert.ToString(r["HFMENTRYCODE"]) != null)
                                {
                                    mEntity.CREDITCURRENCY = Convert.ToString(r["HFMENTRYCODE"]);
                                }
                            });
                        }

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public PaymentEntity GetPaymentDetailsByJobNumber(int id, string userid, string couponCode, double requestedAmount)
        {
            PaymentEntity mEntity = new PaymentEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    //mCommand.CommandType = CommandType.Text;
                    //mCommand.CommandText = "UPDATE SSPDOCUMENTS SET PAGEFLAG=:PAGEFLAG WHERE PAGEFLAG=1 and JOBNUMBER=:JOBNUMBER";
                    //mCommand.Parameters.Add("PAGEFLAG", null);
                    //mCommand.Parameters.Add("JOBNUMBER", id);
                    //mCommand.ExecuteNonQuery();


                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_JOBNUMBER", id);
                        mCommand.Parameters.Add("L_USERID", userid.ToUpper());
                        mCommand.Parameters.Add("PRM_COUPONCODE", couponCode);
                        mCommand.Parameters.Add("PRM_REQUESTEDAMOUNT", requestedAmount);

                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("CUR_SSPPAYMENTBRANCHLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPAYMENTENTITYLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_PAYMENTOPTIONLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_PAYMENTDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtPaymentDetails = new DataTable();
                        OracleDataReader Jareceiptheader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtPaymentDetails.Load(Jareceiptheader);
                        mEntity = MyClass.ConvertToEntity<PaymentEntity>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPJobdetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPJobdetails);
                        DBFactory.Entities.PaymentEntity mSSPJobdetails = MyClass.ConvertToEntity<PaymentEntity>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPPaymentOptionlist = ((OracleRefCursor)mCommand.Parameters["CUR_PAYMENTOPTIONLIST"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPPaymentOptionlist);
                        DBFactory.Entities.SSPPaymentOptionList mSSPPaymentOptionList = MyClass.ConvertToEntity<SSPPaymentOptionList>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPPaymententitylist = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPAYMENTENTITYLIST"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPPaymententitylist);
                        DBFactory.Entities.SSPEntityListEntity mSSPPaymententitylist = MyClass.ConvertToEntity<SSPEntityListEntity>(dtPaymentDetails);




                        Jareceiptheader.Close();
                        SSPJobdetails.Close();
                        SSPPaymententitylist.Close();
                        SSPPaymentOptionlist.Close();
                        mConnection.Close();

                        mEntity.CARGOAVAILABLEFROM = mSSPJobdetails.CARGOAVAILABLEFROM;
                        mEntity.ENTITYLIST = mSSPPaymententitylist;
                        mEntity.PAYMENTOPTIONLIST = mSSPPaymentOptionList;
                        mEntity.ADDITIONALCHARGEAMOUNT = mSSPJobdetails.ADDITIONALCHARGEAMOUNT;
                        mEntity.ADDITIONALCHARGESTATUS = mSSPJobdetails.ADDITIONALCHARGESTATUS;
                        mEntity.ADDITIONALCHARGEAMOUNTINUSD = mSSPJobdetails.ADDITIONALCHARGEAMOUNTINUSD;

                        mEntity.LIVEUPLOADS = mSSPJobdetails.LIVEUPLOADS;
                        mEntity.LOADINGDOCKAVAILABLE = mSSPJobdetails.LOADINGDOCKAVAILABLE;
                        mEntity.CARGOPALLETIZED = mSSPJobdetails.CARGOPALLETIZED;
                        mEntity.ORIGINALDOCUMENTSREQUIRED = mSSPJobdetails.ORIGINALDOCUMENTSREQUIRED;
                        mEntity.TEMPERATURECONTROLREQUIRED = mSSPJobdetails.TEMPERATURECONTROLREQUIRED;
                        mEntity.COMMERCIALPICKUPLOCATION = mSSPJobdetails.COMMERCIALPICKUPLOCATION;
                        mEntity.SPECIALINSTRUCTIONS = mSSPJobdetails.SPECIALINSTRUCTIONS;


                        mEntity.CHARGESETID = mSSPJobdetails.CHARGESETID;
                        mEntity.QUOTESOURCE = mSSPJobdetails.QUOTESOURCE;

                        if (!string.IsNullOrEmpty(userid))
                        {
                            ReadText(DBConnectionMode.SSP, "SELECT NVL(FOCIS.RATECALC_GET_EXCHANGE_RATE(CRDTAPP.CURRENTOSBALANCE,CRDTAPP.HFMENTRYCODE,RECPHDR.CURRENCY),0) CURRENTOSBALANCE,NVL(RECPHDR.CURRENCY,'USD') AS HFMENTRYCODE  FROM JARECEIPTHEADER RECPHDR LEFT JOIN SSPCREDITAPPLICATIONS CRDTAPP ON RECPHDR.CREATEDBY = CRDTAPP.USERID WHERE CRDTAPP.STATEID='Approved' AND UPPER(CRDTAPP.USERID)=:USERID AND RECPHDR.JOBNUMBER=:JOBNUMBER",
                            new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "USERID", Value = userid.ToString().ToUpper() } ,
                                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = id }
                            }
                            , delegate(IDataReader r)
                            {
                                mEntity.CREDITAMOUNT = Convert.ToDouble(r["CURRENTOSBALANCE"]);
                                if (Convert.ToString(r["HFMENTRYCODE"]) != null)
                                {
                                    mEntity.CREDITCURRENCY = Convert.ToString(r["HFMENTRYCODE"]);
                                }
                            });
                        }

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        //updating the shipper and consignee details
        public int UpdateJob(JobBookingEntity entity, string UserId, string BtnFlag, string HSCode, string DestHSCode, string FieldValues, string FieldIDS)
        {
            FieldValues = (FieldValues == null) ? "" : FieldValues;
            FieldIDS = (FieldIDS == null) ? "" : FieldIDS;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    entity.SHISDEFAULTADDRESS = 0;
                    decimal TotalCBM = 0;
                    decimal TotalGrossWeight = 0;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("IN_INCOTERMCODE", entity.JINCOTERMCODE);
                        mCommand.Parameters.Add("IN_INCOTERMDESCRIPTION", entity.JINCOTERMDESCRIPTION);
                        if (entity.JCARGOAVAILABLEFROM.ToString() != "1/1/0001 12:00:00 AM")
                            mCommand.Parameters.Add("IN_CARGOAVAILABLEFROM", entity.JCARGOAVAILABLEFROM);
                        else
                            mCommand.Parameters.Add("IN_CARGOAVAILABLEFROM", "");
                        if (entity.JCARGODELIVERYBY.ToString() != "1/1/0001 12:00:00 AM")
                            mCommand.Parameters.Add("IN_CARGODELIVERYBY", entity.JCARGODELIVERYBY);
                        else
                            mCommand.Parameters.Add("IN_CARGODELIVERYBY", "");

                        mCommand.Parameters.Add("IN_DOCUMENTSREADY", entity.JDOCUMENTSREADY);
                        mCommand.Parameters.Add("IN_SPECIALINSTRUCTION", entity.SPECIALINSTRUCTION);
                        mCommand.Parameters.Add("IN_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("IN_SHPARTYID", entity.SHCLIENTID);
                        mCommand.Parameters.Add("IN_CONPARTYID", entity.CONCLIENTID);
                        mCommand.Parameters.Add("IN_LIVEUPLOADS", entity.LIVEUPLOADS);
                        mCommand.Parameters.Add("IN_LOADINGDOCKAVAILABLE", entity.LOADINGDOCKAVAILABLE);
                        mCommand.Parameters.Add("IN_CARGOPALLETIZED", entity.CARGOPALLETIZED);
                        mCommand.Parameters.Add("IN_ORIGINALDOCUMENTSREQUIRED", entity.ORIGINALDOCUMENTSREQUIRED);
                        mCommand.Parameters.Add("IN_TEMPERATURECONTROLREQUIRED", entity.TEMPERATURECONTROLREQUIRED);
                        mCommand.Parameters.Add("IN_COMMERCIALPICKUPLOCATION", entity.COMMERCIALPICKUPLOCATION);
                        mCommand.CommandText = "USP_SSP_UPDATEJOB";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        if (entity.ShipmentItems.Count() > 0)
                        {
                            foreach (var item in entity.ShipmentItems)
                            {
                                if (item.TOTALCBM >= 0)
                                {
                                    mCommand.Parameters.Clear();
                                    mCommand.Parameters.Add("IN_CONTAINERID", item.CONTAINERID != null ? item.CONTAINERID : 0);
                                    if (item.CONTAINERNAME != null && item.CONTAINERNAME.Contains("'"))
                                    {
                                        item.CONTAINERNAME = item.CONTAINERNAME.Replace("'", "-");
                                    }
                                    mCommand.Parameters.Add("IN_CONTAINERNAME", item.CONTAINERNAME != null ? item.CONTAINERNAME : "");
                                    mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                                    mCommand.Parameters.Add("IN_ITEMDESCRIPTION", item.ITEMDESCRIPTION != null ? item.ITEMDESCRIPTION : "");
                                    mCommand.Parameters.Add("IN_ITEMHEIGHT", item.ITEMHEIGHT != null ? item.ITEMHEIGHT : 0);
                                    mCommand.Parameters.Add("IN_ITEMLENGTH", item.ITEMLENGTH != null ? item.ITEMLENGTH : 0);
                                    mCommand.Parameters.Add("IN_ITEMWIDTH", item.ITEMWIDTH != null ? item.ITEMWIDTH : 0);
                                    mCommand.Parameters.Add("IN_JOBSHIPMENTID", item.SHIPMENTITEMID != null ? item.SHIPMENTITEMID : 0);
                                    mCommand.Parameters.Add("IN_LENGTHUOM", item.LENGTHUOM != null ? item.LENGTHUOM : 0);
                                    mCommand.Parameters.Add("IN_LENGTHUOMCODE", item.LENGTHUOMCODE != null ? item.LENGTHUOMCODE : "");
                                    mCommand.Parameters.Add("IN_LENGTHUOMNAME", item.LENGTHUOMNAME != null ? item.LENGTHUOMNAME : "");
                                    mCommand.Parameters.Add("IN_PACKAGETYPECODE", item.PACKAGETYPECODE != null ? item.PACKAGETYPECODE : "");
                                    mCommand.Parameters.Add("IN_PACKAGETYPEID", item.PACKAGETYPEID != null ? item.PACKAGETYPEID : 0);
                                    mCommand.Parameters.Add("IN_PACKAGETYPENAME", item.PACKAGETYPENAME != null ? item.PACKAGETYPENAME : "");
                                    mCommand.Parameters.Add("IN_QUANTITY", item.QUANTITY != null ? item.QUANTITY : 0);
                                    mCommand.Parameters.Add("IN_QUOTATIONID", entity.QUOTATIONID != null ? entity.QUOTATIONID : 0);
                                    mCommand.Parameters.Add("IN_TOTALCBM", item.TOTALCBM != null ? item.TOTALCBM : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTPERPIECE", item.WEIGHTPERPIECE != null ? item.WEIGHTPERPIECE : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTTOTAL", item.WEIGHTTOTAL != null ? item.WEIGHTTOTAL : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTUOM", item.WEIGHTUOM != null ? item.WEIGHTUOM : 0);
                                    mCommand.Parameters.Add("IN_WEIGHTUOMCODE", item.WEIGHTUOMCODE != null ? item.WEIGHTUOMCODE : "");
                                    mCommand.Parameters.Add("IN_WEIGHTUOMNAME", item.WEIGHTUOMNAME != null ? item.WEIGHTUOMNAME : "");
                                    mCommand.Parameters.Add("IN_CONTAINERCODE", item.CONTAINERCODE != null ? item.CONTAINERCODE : "");

                                    mCommand.Parameters.Add("IN_TOTALWGTCBM", item.TotalWgtCBM != null ? item.TotalWgtCBM : 0);
                                    mCommand.Parameters.Add("IN_TOTALWGTKG", item.TotalWgtKG != null ? item.TotalWgtKG : 0);
                                    mCommand.Parameters.Add("IN_TOTALWGTTON", item.TotalWgtTON != null ? item.TotalWgtTON : 0);

                                    mCommand.CommandText = "USP_SAVE_SSPJOBSHIPMENTITEM";
                                    mCommand.CommandType = CommandType.StoredProcedure;
                                    mCommand.ExecuteNonQuery();

                                    TotalCBM = TotalCBM + item.TOTALCBM;
                                    TotalGrossWeight = TotalGrossWeight + item.WEIGHTTOTAL;
                                }
                            }
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("IN_TOTALCBM", TotalCBM);
                            mCommand.Parameters.Add("IN_WEIGHTTOTAL", TotalGrossWeight);
                            mCommand.CommandText = "USP_SSP_TOTALCBMWEIGHT";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IN_JOBNUMBER", entity.JJOBNUMBER);
                            mCommand.Parameters.Add("IN_TOTALCBM", entity.TOTALCBM);
                            mCommand.Parameters.Add("IN_WEIGHTTOTAL", entity.TOTALGROSSWEIGHT);
                            mCommand.CommandText = "USP_SSP_TOTALCBMWEIGHT";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }

                        //Charge details
                        if (BtnFlag == "ContPayment")
                        {
                            // Inserting into staging table in the time of payment 
                            //mCommand.Parameters.Clear();
                            //mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JJOBNUMBER);
                            //mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                            //mCommand.CommandType = CommandType.StoredProcedure;
                            //mCommand.ExecuteNonQuery();

                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("IP_JOBNUMBER ", entity.JJOBNUMBER).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_QUOTATIONID", entity.QUOTATIONID).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("IP_USERID", UserId).Direction = ParameterDirection.Input;
                            mCommand.Parameters.Add("OP_MESSAGE", OracleDbType.Varchar2, 10).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTID", OracleDbType.Int64, 18).Direction = ParameterDirection.Output;
                            mCommand.Parameters.Add("OP_NEW_RECEIPTNO", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_JA_INS_RECEIPTDETAILS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();
                        }
                        mOracleTransaction.Commit();

                        ////saving into master client table
                        //mCommand.Parameters.Clear();
                        //mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                        //mCommand.CommandText = "USP_JP_SAVE_CLIENTMASTER";
                        //mCommand.CommandType = CommandType.StoredProcedure;
                        //mCommand.ExecuteNonQuery();


                        //saving HSCODE Details----------------(Added By Anil G)
                        if (!string.IsNullOrEmpty(HSCode))//&& HSCode.IndexOf('$') > -1
                        {

                            string OrgHSCode = HSCode;//.Split('$')[0].ToString();
                            //string DestHSCode = HSCode.Split('$')[1].ToString();

                            if (!string.IsNullOrEmpty(OrgHSCode))
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_QUOTATIONID", null);
                                mCommand.Parameters.Add("IP_JOBNUMBER", entity.JJOBNUMBER);
                                mCommand.Parameters.Add("IP_DIRECTION", "O");
                                mCommand.Parameters.Add("IP_HSCODE", OrgHSCode);//HSCode.Trim().Replace(" ", string.Empty)
                                mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                                mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                                mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_HSC_INS_N
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }

                            if (!string.IsNullOrEmpty(DestHSCode))
                            {
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("IP_QUOTATIONID", null);
                                mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                mCommand.Parameters.Add("IP_DIRECTION  ", "D");
                                mCommand.Parameters.Add("IP_HSCODE", DestHSCode);
                                mCommand.Parameters.Add("OP_SUCCESS_CODE", OracleDbType.Int64, 10).Direction = ParameterDirection.Output;
                                mCommand.Parameters.Add("OP_SUCCESS_MSG", OracleDbType.Varchar2, 5000).Direction = ParameterDirection.Output;
                                mCommand.CommandText = "USP_JP_SAVE_HSCODEDETAILS";//USP_JP_SAVE_HSC_INSTRUCTIONS USP_JP_SAVE_DESTHSC_INS_N
                                mCommand.CommandType = CommandType.StoredProcedure;
                                mCommand.ExecuteNonQuery();
                            }

                        }
                        //---------------Save Field Values------------------------
                        if (FieldValues != "" && FieldValues != null)
                        {
                            for (int i = 0; i < FieldValues.Split('_').Length; i++)
                            {
                                //mCommand.Parameters.Clear();
                                //mCommand.Parameters.Add("IP_JOBNUMBER  ", entity.JJOBNUMBER);
                                //mCommand.Parameters.Add("IP_FIELDDESCRIPTION", FieldNames.Split('_')[i]);
                                //mCommand.Parameters.Add("IP_VALUE", FieldValues.Split('_')[i]);
                                //mCommand.Parameters.Add("IP_CREATEDBY", UserId);
                                //mCommand.CommandText = "SSP_SAVE_FIELDVALUES";
                                //mCommand.CommandType = CommandType.StoredProcedure;
                                //mCommand.ExecuteNonQuery();
                                mCommand.Parameters.Clear();
                                mCommand.CommandType = CommandType.Text;
                                mCommand.CommandText = "UPDATE SSP_FIELD_VALUES SET VALUE=:IP_VALUE WHERE FIELDID=:FIELDID";
                                mCommand.Parameters.Add("IP_VALUE", FieldValues.Split('_')[i]);
                                mCommand.Parameters.Add("FIELDID", FieldIDS.Split('_')[i]);
                                mCommand.ExecuteNonQuery();
                            }
                        }

                        //--------------------------------------------------------
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return Convert.ToInt32(entity.QUOTATIONID);
        }

        #region Document calls
        public IList<SSPDocumentsEntity> GetDocumentDetailsbyQuotationId(long QuotationId, long JobNumber)
        {

            //Shipment Details List
            List<SSPDocumentsEntity> mShipmentItems = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE PAGEFLAG=:PAGEFLAG and  JOBNUMBER=:JOBNUMBER order by DATEMODIFIED desc",
                new OracleParameter[] {new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JobNumber},
                                        new OracleParameter {OracleDbType = OracleDbType.Int32, ParameterName = "PAGEFLAG", Value = 1}
                }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            return mShipmentItems;
        }
        public IList<SSPDocumentsEntity> GetDocumentDetailsbyQuotationIdAndSummary(long QuotationId, long JobNumber)
        {

            //Shipment Details List
            List<SSPDocumentsEntity> mShipmentItems = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE PAGEFLAG=:PAGEFLAG and SOURCE=:SOURCE and  JOBNUMBER=:JOBNUMBER order by DATEMODIFIED desc",
                new OracleParameter[] {new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JobNumber},
                                        new OracleParameter {OracleDbType = OracleDbType.Int32, ParameterName = "PAGEFLAG", Value = 1},
                                        new OracleParameter {OracleDbType = OracleDbType.Varchar2, ParameterName = "SOURCE", Value = "BookSum"}
                }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            return mShipmentItems;
        }
        public DataSet GetDocuments(int JobNumber)
        {
            DataSet ds = new DataSet();
            string Query = "SELECT * FROM SSPDOCUMENTS WHERE PAGEFLAG=1 AND FILENAME IS NOT NULL AND JOBNUMBER=:JOBNUMBER";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JobNumber } 
            });
            ds.Tables.Add(dt);
            return ds;
        }
        public long SaveDocuments(SSPDocumentsEntity entity, string UserName, string Source, string GUID, string EnvironmentName, string DocDelete = "NO", int PageFlag = 1)
        {

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        entity.DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = @"Insert into SSPDOCUMENTS (DOCID,JOBNUMBER,QUOTATIONID,DOCUMNETTYPE,FILENAME,FILEEXTENSION,FILESIZE,FILECONTENT,CREATEDBY,
                        DOCUMENTNAME,PAGEFLAG,SOURCE,DOCDELETE) values (:DOCID,:JOBNUMBER,:QUOTATIONID,:DOCUMNETTYPE,:FILENAME,:FILEEXTENSION,:FILESIZE,:FILECONTENT,:CREATEDBY,
                        :DOCUMENTNAME,:PAGEFLAG,:SOURCE,:DOCDELETE)";
                        mCommand.Parameters.Add("DOCID", entity.DOCID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("CREATEDBY", UserName);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("DOCUMNETTYPE", entity.DOCUMNETTYPE);
                        mCommand.Parameters.Add("FILENAME", entity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", entity.FILEEXTENSION);
                        mCommand.Parameters.Add("FILESIZE", entity.FILESIZE);
                        mCommand.Parameters.Add("FILECONTENT", entity.FILECONTENT);
                        mCommand.Parameters.Add("DOCUMENTNAME", entity.DocumentName);
                        mCommand.Parameters.Add("PAGEFLAG", 1);
                        mCommand.Parameters.Add("SOURCE", Source);
                        mCommand.Parameters.Add("DOCDELETE", DocDelete);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        mCommand.Parameters.Clear();
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.CommandText = "USP_SSP_DMS_INTEGRATION";
                        mCommand.Parameters.Add("IP_USERID", UserName);
                        mCommand.Parameters.Add("IP_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("IP_DOCID", entity.DOCID);
                        mCommand.Parameters.Add("IP_MESSAGEID", EnvironmentName + "-" + GUID);
                        mCommand.Parameters.Add("OP_SPMESSAGE", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output; ;
                        mCommand.Parameters.Add("OP_SPSTATUS", OracleDbType.Long, 2000).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return Convert.ToInt32(entity.DOCID);

        }
        public long SaveDocumentsWithParams(SSPDocumentsEntity entity, string UserName, string Source, string GUID, string EnvironmentName, string ProviderMailid, string DocDelete = "NO")
        {

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        entity.DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = @"Insert into SSPDOCUMENTS (DOCID,JOBNUMBER,QUOTATIONID,DOCUMNETTYPE,FILENAME,FILEEXTENSION,FILESIZE,FILECONTENT,CREATEDBY,
                        DOCUMENTNAME,PAGEFLAG,SOURCE,DOCDELETE,DIRECTION,CONDMAND,DOCUMENTTEMPLATE,PROVIDERMAILID) values (:DOCID,:JOBNUMBER,:QUOTATIONID,:DOCUMNETTYPE,:FILENAME,:FILEEXTENSION,:FILESIZE,:FILECONTENT,:CREATEDBY,
                        :DOCUMENTNAME,:PAGEFLAG,:SOURCE,:DOCDELETE,:DIRECTION,:CONDMAND,:DOCUMENTTEMPLATE,:PROVIDERMAILID)";

                        mCommand.Parameters.Add("DOCID", entity.DOCID);
                        mCommand.Parameters.Add("CREATEDBY", UserName);
                        mCommand.Parameters.Add("JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("DOCUMNETTYPE", entity.DOCUMNETTYPE);
                        mCommand.Parameters.Add("FILENAME", entity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", entity.FILEEXTENSION);
                        mCommand.Parameters.Add("FILESIZE", entity.FILESIZE);
                        mCommand.Parameters.Add("FILECONTENT", entity.FILECONTENT);
                        mCommand.Parameters.Add("DOCUMENTNAME", entity.DocumentName);
                        mCommand.Parameters.Add("PAGEFLAG", 1);
                        mCommand.Parameters.Add("SOURCE", Source);
                        mCommand.Parameters.Add("DOCDELETE", DocDelete);
                        mCommand.Parameters.Add("DIRECTION", entity.DIRECTION);
                        mCommand.Parameters.Add("CONDMAND", entity.CONDMAND);
                        mCommand.Parameters.Add("DOCUMENTTEMPLATE", entity.DOCUMENTTEMPLATE);
                        mCommand.Parameters.Add("PROVIDERMAILID", ProviderMailid);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                        mCommand.Parameters.Clear();
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.CommandText = "USP_SSP_DMS_INTEGRATION";
                        mCommand.Parameters.Add("IP_USERID", UserName);
                        mCommand.Parameters.Add("IP_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("IP_DOCID", entity.DOCID);
                        mCommand.Parameters.Add("IP_MESSAGEID", EnvironmentName + "-" + GUID);
                        mCommand.Parameters.Add("OP_SPMESSAGE", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output; ;
                        mCommand.Parameters.Add("OP_SPSTATUS", OracleDbType.Long, 2000).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return Convert.ToInt32(entity.DOCID);

        }
        public void UpdateDocumentBytes(SSPDocumentsEntity docentity, string p, string GUID, string EnvironmentName)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();
            OracleTransaction mOracleTransaction = null;
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = @"Update  SSPDOCUMENTS SET 
                        FILECONTENT=:FILECONTENT, FILESIZE=:FILESIZE, FILENAME=:FILENAME, FILEEXTENSION=:FILEEXTENSION 
                                                where DOCID=:DOCID";

                        mCommand.Parameters.Add("FILECONTENT", docentity.FILECONTENT);
                        mCommand.Parameters.Add("FILESIZE", docentity.FILESIZE);
                        mCommand.Parameters.Add("FILENAME", docentity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", docentity.FILEEXTENSION);
                        mCommand.Parameters.Add("DOCID", docentity.DOCID);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                        mCommand.Parameters.Clear();

                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.CommandText = "USP_SSP_DMS_INTEGRATION";
                        mCommand.Parameters.Add("IP_USERID", p);
                        mCommand.Parameters.Add("IP_JOBNUMBER", docentity.JOBNUMBER);
                        mCommand.Parameters.Add("IP_DOCID", docentity.DOCID);
                        mCommand.Parameters.Add("IP_MESSAGEID", EnvironmentName + "-" + GUID);
                        mCommand.Parameters.Add("OP_SPMESSAGE", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output; ;
                        mCommand.Parameters.Add("OP_SPSTATUS", OracleDbType.Long, 2000).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }
        public void UpdateDocumentBytesCollabration(SSPDocumentsEntity docentity, string p, string GUID, string EnvironmentName, string ModifiedBy)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();
            OracleTransaction mOracleTransaction = null;
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = @"Update  SSPDOCUMENTS SET 
                        FILECONTENT=:FILECONTENT, FILESIZE=:FILESIZE, FILENAME=:FILENAME, FILEEXTENSION=:FILEEXTENSION,MODIFIEDBY=:MODIFIEDBY,PROVIDERMAILID=:PROVIDERMAILID
                                                where DOCID=:DOCID";

                        mCommand.Parameters.Add("FILECONTENT", docentity.FILECONTENT);
                        mCommand.Parameters.Add("FILESIZE", docentity.FILESIZE);
                        mCommand.Parameters.Add("FILENAME", docentity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", docentity.FILEEXTENSION);
                        mCommand.Parameters.Add("MODIFIEDBY", ModifiedBy);
                        mCommand.Parameters.Add("PROVIDERMAILID", ModifiedBy);
                        mCommand.Parameters.Add("DOCID", docentity.DOCID);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                        mCommand.Parameters.Clear();

                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.CommandText = "USP_SSP_DMS_INTEGRATION";
                        mCommand.Parameters.Add("IP_USERID", p);
                        mCommand.Parameters.Add("IP_JOBNUMBER", docentity.JOBNUMBER);
                        mCommand.Parameters.Add("IP_DOCID", docentity.DOCID);
                        mCommand.Parameters.Add("IP_MESSAGEID", EnvironmentName + "-" + GUID);
                        mCommand.Parameters.Add("OP_SPMESSAGE", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output; ;
                        mCommand.Parameters.Add("OP_SPSTATUS", OracleDbType.Long, 2000).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void DeleteDocContentbyDocId(long DOCID, string p)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();
            OracleTransaction mOracleTransaction = null;
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = @"Update  SSPDOCUMENTS SET  
                        FILECONTENT=:FILECONTENT, FILESIZE=:FILESIZE, FILENAME=:FILENAME, FILEEXTENSION=:FILEEXTENSION 
                                                where DOCID=:DOCID";

                        mCommand.Parameters.Add("FILECONTENT", null);
                        mCommand.Parameters.Add("FILESIZE", null);
                        mCommand.Parameters.Add("FILENAME", null);
                        mCommand.Parameters.Add("FILEEXTENSION", null);
                        mCommand.Parameters.Add("DOCID", DOCID);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                        mCommand.Parameters.Clear();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }
        public void DeleteDocByDocId(long DocId, string UserID)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {

                        //mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        //entity.DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Delete from  SSPDOCUMENTS where DOCID= :DOCID and CREATEDBY=:CREATEDBY";
                        mCommand.Parameters.Add("DOCID", DocId);
                        mCommand.Parameters.Add("CREATEDBY", UserID);

                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }


        }
        public void DeleteMsdsDocByDocId(long DocId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {

                        mCommand.CommandText = "Delete from  SSPDOCUMENTS where DOCID= :DOCID";
                        mCommand.Parameters.Add("DOCID", DocId);


                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }


        }
        #endregion


        public QuoteToJobDetails GetByQuotationID(int QuotationID, string CreatedBy)
        {
            QuoteToJobDetails QJ = new QuoteToJobDetails();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", QuotationID);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_INDEXGETBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        QuoteDetails QTJ = MyClass.ConvertToEntity<QuoteDetails>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        Quotationreader.Close(); Quotationreader.Dispose();
                        dtShipment.Close(); dtShipment.Dispose();
                        mConnection.Close();


                        //----------------Creating job in the time of booking----------------------------------------------------
                        SSPJobDetailsEntity job = CreateNewJobDetails(QTJ.QUOTATIONID, QTJ.QUOTATIONNUMBER, CreatedBy, QTJ.GRANDTOTAL);


                        DataSet DsCountry = GetCountryInstructions(Convert.ToInt64(QTJ.OCOUNTRYID), Convert.ToInt64(QTJ.DCOUNTRYID));
                        List<DBFactory.Entities.COUNTRYINSTRNS> COUNTRYINSTRNS = MyClass.ConvertToList<DBFactory.Entities.COUNTRYINSTRNS>(DsCountry.Tables[1]);

                        //Inserting QMSERVICECHARGE TABLE DATA TO JOBSERVICECHARGE TABLE.
                        try
                        {
                            if (mConnection.State == ConnectionState.Closed) mConnection.Open();

                            mCommand.Parameters.Clear();

                            mCommand.Parameters.Add("IP_QUOTATIONID", Convert.ToInt64(QuotationID));
                            mCommand.Parameters.Add("IP_JOBNUMBER", job.JOBNUMBER);

                            mCommand.CommandText = "USP_SSP_JOB_SERVICECHARGE";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();

                            mConnection.Close();
                        }
                        catch (Exception ex)
                        {
                            mOracleTransaction.Rollback();
                            throw new Exception(ex.ToString());
                        }


                        QJ.QuoteDetails = QTJ;
                        QJ.QuoteShipmentItems = mShipmentItems;
                        QJ.JobItems = job;
                        QJ.COUNTRYINSTRNS = COUNTRYINSTRNS;

                        return QJ;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }
        //Get Quoted Job Details
        public JobBookingEntity GetQuotedJobDetails(int id, long JobNumber)
        {
            JobBookingEntity mEntity = new JobBookingEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("L_JOBNUMBER", JobNumber);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPARTYDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JOBSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_HSCODEDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_COMFIELDS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        //mCommand.CommandText = "USP_GET_BSBYID_TEST";
                        mCommand.CommandText = "USP_GET_BOOKINGSUMMARYBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<JobBookingEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobShipmentItem = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobShipmentItem);
                        List<DBFactory.Entities.JobBookingShipmentEntity> mSspJobShipmentItem = MyClass.ConvertToList<DBFactory.Entities.JobBookingShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPDocuments = ((OracleRefCursor)mCommand.Parameters["CUR_SSPDOCUMENTS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPDocuments);
                        List<DBFactory.Entities.SSPDocumentsEntity> mSSPDocuments = MyClass.ConvertToList<DBFactory.Entities.SSPDocumentsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobDetails);
                        List<DBFactory.Entities.SSPJobDetailsEntity> mSSPJobDetails = MyClass.ConvertToList<DBFactory.Entities.SSPJobDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPPartyDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPARTYDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPPartyDetails);
                        List<DBFactory.Entities.SSPPartyDetailsEntity> mSSPPartyDetails = MyClass.ConvertToList<DBFactory.Entities.SSPPartyDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_JOBSERVICECHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.PaymentChargesEntity> mQuotationCharges = MyClass.ConvertToList<DBFactory.Entities.PaymentChargesEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtHSCodeDetails = ((OracleRefCursor)mCommand.Parameters["CUR_HSCODEDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtHSCodeDetails);
                        List<DBFactory.Entities.HsCodeInstructionEntity> mHSCodeDetails = MyClass.ConvertToList<DBFactory.Entities.HsCodeInstructionEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtJareceiptHeader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtQuotation.Load(dtJareceiptHeader);
                        List<DBFactory.Entities.ReceiptHeaderEntity> mJareceiptHeader = MyClass.ConvertToList<DBFactory.Entities.ReceiptHeaderEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtfields = ((OracleRefCursor)mCommand.Parameters["CUR_COMFIELDS"].Value).GetDataReader();
                        dtQuotation.Load(dtfields);
                        List<DBFactory.Entities.Compliance_Fields> mCopmlnceflds = MyClass.ConvertToList<DBFactory.Entities.Compliance_Fields>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtSSPJobShipmentItem.Close();
                        dtSSPDocuments.Close();
                        dtSSPJobDetails.Close();
                        dtSSPPartyDetails.Close();
                        dtServiceCharge.Close();
                        dtHSCodeDetails.Close();
                        dtJareceiptHeader.Close();
                        //dtSSPSchedules.Close();
                        mConnection.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.JobShipmentItems = mSspJobShipmentItem;
                        mEntity.DocumentDetails = mSSPDocuments;
                        mEntity.JobItems = mSSPJobDetails;
                        mEntity.PartyItems = mSSPPartyDetails;
                        mEntity.PaymentCharges = mQuotationCharges;
                        mEntity.HSCodeInsItems = mHSCodeDetails;
                        mEntity.ReceiptHeaderDetails = mJareceiptHeader;
                        mEntity.CopmlnceFlds = mCopmlnceflds;

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }
        public JobBookingEntity GetQuotedJobDetailsCollabration(int id, long JobNumber, string Provideremailid)
        {
            JobBookingEntity mEntity = new JobBookingEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("L_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("L_PROMAILID", Provideremailid);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPARTYDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JOBSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_HSCODEDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_COMFIELDS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        //mCommand.CommandText = "USP_GET_BSBYID_TEST";
                        mCommand.CommandText = "USP_GET_COLLABRATION";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<JobBookingEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobShipmentItem = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobShipmentItem);
                        List<DBFactory.Entities.JobBookingShipmentEntity> mSspJobShipmentItem = MyClass.ConvertToList<DBFactory.Entities.JobBookingShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPDocuments = ((OracleRefCursor)mCommand.Parameters["CUR_SSPDOCUMENTS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPDocuments);
                        List<DBFactory.Entities.SSPDocumentsEntity> mSSPDocuments = MyClass.ConvertToList<DBFactory.Entities.SSPDocumentsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobDetails);
                        List<DBFactory.Entities.SSPJobDetailsEntity> mSSPJobDetails = MyClass.ConvertToList<DBFactory.Entities.SSPJobDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPPartyDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPARTYDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPPartyDetails);
                        List<DBFactory.Entities.SSPPartyDetailsEntity> mSSPPartyDetails = MyClass.ConvertToList<DBFactory.Entities.SSPPartyDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_JOBSERVICECHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.PaymentChargesEntity> mQuotationCharges = MyClass.ConvertToList<DBFactory.Entities.PaymentChargesEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtHSCodeDetails = ((OracleRefCursor)mCommand.Parameters["CUR_HSCODEDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtHSCodeDetails);
                        List<DBFactory.Entities.HsCodeInstructionEntity> mHSCodeDetails = MyClass.ConvertToList<DBFactory.Entities.HsCodeInstructionEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtJareceiptHeader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtQuotation.Load(dtJareceiptHeader);
                        List<DBFactory.Entities.ReceiptHeaderEntity> mJareceiptHeader = MyClass.ConvertToList<DBFactory.Entities.ReceiptHeaderEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtfields = ((OracleRefCursor)mCommand.Parameters["CUR_COMFIELDS"].Value).GetDataReader();
                        dtQuotation.Load(dtfields);
                        List<DBFactory.Entities.Compliance_Fields> mCopmlnceflds = MyClass.ConvertToList<DBFactory.Entities.Compliance_Fields>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtSSPJobShipmentItem.Close();
                        dtSSPDocuments.Close();
                        dtSSPJobDetails.Close();
                        dtSSPPartyDetails.Close();
                        dtServiceCharge.Close();
                        dtHSCodeDetails.Close();
                        dtJareceiptHeader.Close();
                        //dtSSPSchedules.Close();
                        mConnection.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.JobShipmentItems = mSspJobShipmentItem;
                        mEntity.DocumentDetails = mSSPDocuments;
                        mEntity.JobItems = mSSPJobDetails;
                        mEntity.PartyItems = mSSPPartyDetails;
                        mEntity.PaymentCharges = mQuotationCharges;
                        mEntity.HSCodeInsItems = mHSCodeDetails;
                        mEntity.ReceiptHeaderDetails = mJareceiptHeader;
                        mEntity.CopmlnceFlds = mCopmlnceflds;


                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public QuoteToJobDetails GetQuotedJobDetails_MB(int id, Int64 JobNumber)
        {
            QuoteToJobDetails mEntity = new QuoteToJobDetails();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("L_JOBNUMBER", JobNumber);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPARTYDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JOBSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_HSCODEDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_COMFIELDS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        //mCommand.CommandText = "USP_GET_BSBYID_TEST";
                        mCommand.CommandText = "USP_GET_BOOKINGSUMMARYBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        QuoteDetails QD = MyClass.ConvertToEntity<QuoteDetails>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobShipmentItem = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobShipmentItem);
                        List<DBFactory.Entities.JobBookingShipmentEntity> mSspJobShipmentItem = MyClass.ConvertToList<DBFactory.Entities.JobBookingShipmentEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobDetails);
                        DBFactory.Entities.SSPJobDetailsEntity mSSPJobDetails = MyClass.ConvertToEntity<DBFactory.Entities.SSPJobDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPPartyDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPARTYDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPPartyDetails);
                        List<DBFactory.Entities.SSPPartyDetailsEntity> mSSPPartyDetails = MyClass.ConvertToList<DBFactory.Entities.SSPPartyDetailsEntity>(dtQuotation);

                        DataSet DsCountry = GetCountryInstructions(Convert.ToInt64(QD.OCOUNTRYID), Convert.ToInt64(QD.DCOUNTRYID));
                        List<DBFactory.Entities.COUNTRYINSTRNS> COUNTRYINSTRNS = MyClass.ConvertToList<DBFactory.Entities.COUNTRYINSTRNS>(DsCountry.Tables[1]);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtSSPJobShipmentItem.Close();
                        dtSSPJobDetails.Close();
                        dtSSPPartyDetails.Close();
                        mConnection.Close();
                        mEntity.QuoteDetails = QD;
                        mEntity.QuoteShipmentItems = mShipmentItems;
                        mEntity.JobShipmentItems = mSspJobShipmentItem;
                        mEntity.JobItems = mSSPJobDetails;
                        mEntity.PartyItems = mSSPPartyDetails;
                        mEntity.COUNTRYINSTRNS = COUNTRYINSTRNS;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public BookingSummary GetBookingSummary_MB(int id, Int64 JobNumber)
        {
            BookingSummary mEntity = new BookingSummary();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("L_JOBNUMBER", JobNumber);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPDOCUMENTS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPARTYDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JOBSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_HSCODEDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_COMFIELDS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        //mCommand.CommandText = "USP_GET_BSBYID_TEST";
                        mCommand.CommandText = "USP_GET_BOOKINGSUMMARYBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        QuoteDetails QD = MyClass.ConvertToEntity<QuoteDetails>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobShipmentItem = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobShipmentItem);
                        List<DBFactory.Entities.JobBookingShipmentEntity> mSspJobShipmentItem = MyClass.ConvertToList<DBFactory.Entities.JobBookingShipmentEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobDetails);
                        DBFactory.Entities.SSPJobDetailsEntity mSSPJobDetails = MyClass.ConvertToEntity<DBFactory.Entities.SSPJobDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPPartyDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPARTYDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPPartyDetails);
                        List<DBFactory.Entities.SSPPartyDetailsEntity> mSSPPartyDetails = MyClass.ConvertToList<DBFactory.Entities.SSPPartyDetailsEntity>(dtQuotation);


                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_JOBSERVICECHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.QuotationChargeEntity> mQuotationCharges = MyClass.ConvertToList<DBFactory.Entities.QuotationChargeEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPDocuments = ((OracleRefCursor)mCommand.Parameters["CUR_SSPDOCUMENTS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPDocuments);
                        List<DBFactory.Entities.SSPDocumentsEntity_MB> mSSPDocuments = MyClass.ConvertToList<DBFactory.Entities.SSPDocumentsEntity_MB>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtRHEader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtQuotation.Load(dtRHEader);
                        List<DBFactory.Entities.ReceiptHeaderEntity> mRHeader = MyClass.ConvertToList<DBFactory.Entities.ReceiptHeaderEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtSSPJobShipmentItem.Close();
                        dtSSPJobDetails.Close();
                        dtSSPPartyDetails.Close();
                        mConnection.Close();
                        mEntity.QuoteDetails = QD;
                        mEntity.QuoteShipmentItems = mShipmentItems;
                        mEntity.JobShipmentItems = mSspJobShipmentItem;
                        mEntity.JobItems = mSSPJobDetails;
                        mEntity.PartyItems = mSSPPartyDetails;
                        mEntity.QuotationChargeEntity = mQuotationCharges;
                        mEntity.DocumentDetails = mSSPDocuments;
                        mEntity.ReceiptHeaderDetails = mRHeader;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }
        //Get document by documentid
        public SSPDocumentsEntity GetDocumentDetailsbyDOCID(int DOCID)
        {
            SSPDocumentsEntity mEntity = new SSPDocumentsEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE DOCID=:DOCID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "DOCID", Value = DOCID } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<SSPDocumentsEntity>(r);
            });

            return mEntity;

        }


        public void SavePaymentTransactionDetails(PaymentTransactionDetailsEntity entity, string CouponCode, decimal DiscountAmount, double PayAmount, string reason = "", string paidcurrency = "")
        {
            string serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
            string PAYMENTREFMESSAGE = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("PRM_ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PRM_PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER); //entity.PAYMENTREFMESSAGE
                        PAYMENTREFMESSAGE = (entity.ADDITIONALCHARGESTATUS.ToLower() == "paid" || entity.ADDITIONALCHARGESTATUS.ToLower() == "rejected") ? entity.PAYMENTREFMESSAGE + "_" + "Additional" : entity.PAYMENTREFMESSAGE;
                        mCommand.Parameters.Add("PRM_PAYMENTREFMESSAGE", PAYMENTREFMESSAGE);
                        mCommand.Parameters.Add("PRM_CREATEDBY", entity.CREATEDBY);
                        mCommand.Parameters.Add("PRM_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PRM_PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("PRM_TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JOBNUMBER);
                        mCommand.Parameters.Add("PRM_DISCOUNTCODE", CouponCode);
                        mCommand.Parameters.Add("PRM_DISCOUNTAMOUNT  ", DiscountAmount);
                        mCommand.Parameters.Add("PRM_SERVERNAME  ", serverName);
                        mCommand.Parameters.Add("PRM_SOURCETYPE", entity.ADDITIONALCHARGESTATUS);

                        mCommand.Parameters.Add("PRM_ADDITIONALCHARGEAMOUNT", entity.ADDITIONALCHARGEAMOUNT);
                        mCommand.Parameters.Add("PRM_ADDCHARGEAMOUNTINUSD", entity.ADDITIONALCHARGEAMOUNTINUSD);
                        mCommand.Parameters.Add("PRM_STRIPEMESSAGE", reason);
                        mCommand.Parameters.Add("PRM_PAYAMOUNT", (PayAmount/100));
                        mCommand.Parameters.Add("PRM_PAIDCURRENCY", paidcurrency);
                        mCommand.CommandText = "USP_ONLINETRANSACTIONDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public void SaveOfflinePaymentTransactionDetails(PaymentTransactionDetailsEntity entity, string DiscountCode, decimal DiscountAmount)
        {
            string serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            string PAYMENTREFMESSAGE = string.Empty;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("PRM_ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PRM_PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        PAYMENTREFMESSAGE = (entity.ADDITIONALCHARGESTATUS != null && (entity.ADDITIONALCHARGESTATUS.ToLower() == "pay now")) ? entity.PAYMENTREFMESSAGE + "_" + "Additional" : entity.PAYMENTREFMESSAGE;
                        mCommand.Parameters.Add("PRM_PAYMENTREFMESSAGE", PAYMENTREFMESSAGE);
                        mCommand.Parameters.Add("PRM_CREATEDBY", entity.CREATEDBY);
                        mCommand.Parameters.Add("PRM_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PRM_PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("PRM_TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PRM_PAYERBANK", entity.TRANSBANKNAME);
                        mCommand.Parameters.Add("PRM_PAYERBRANCH", entity.TRANSBRANCHNAME);
                        mCommand.Parameters.Add("PRM_CHEQUENO", entity.TRANSCHEQUENO);
                        mCommand.Parameters.Add("PRM_CHEQUEDATE", entity.TRANSCHEQUEDATE);
                        mCommand.Parameters.Add("PRM_RECEIPTAMOUNT", entity.TRANSAMOUNT);
                        mCommand.Parameters.Add("PRM_CURRENCY", entity.TRANSCURRENCY);
                        mCommand.Parameters.Add("PRM_PAYMENTDESCRIPTION", entity.TRANSDESCRIPTION);
                        mCommand.Parameters.Add("PRM_DISCOUNTCODE", DiscountCode);
                        mCommand.Parameters.Add("PRM_DISCOUNTAMOUNT  ", DiscountAmount);
                        mCommand.Parameters.Add("PRM_SERVERNAME  ", serverName);
                        mCommand.Parameters.Add("PRM_SOURCETYPE", entity.ADDITIONALCHARGESTATUS);
                        mCommand.Parameters.Add("PRM_ADDITIONALCHARGEAMOUNT", entity.ADDITIONALCHARGEAMOUNT);
                        mCommand.Parameters.Add("PRM_ADDCHARGEAMOUNTINUSD", entity.ADDITIONALCHARGEAMOUNTINUSD);
                        mCommand.Parameters.Add("PRM_PAIDCURRENCY", entity.PAIDCURRENCY);

                        mCommand.Parameters.Add("PRM_ENTITYBRANCHKEY", entity.BRANCHENTITYKEY.ToString().Length > 0 ? entity.BRANCHENTITYKEY : 0);

                        mCommand.CommandText = "USP_OFFLINETRANSACTIONDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public void SaveOfflinePaymentTransactionDetails(PaymentTransactionDetailsEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("PRM_ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PRM_PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        mCommand.Parameters.Add("PRM_PAYMENTREFMESSAGE", entity.PAYMENTREFMESSAGE);
                        mCommand.Parameters.Add("PRM_CREATEDBY", entity.CREATEDBY);
                        mCommand.Parameters.Add("PRM_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PRM_PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("PRM_TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PRM_PAYERBANK", entity.TRANSBANKNAME);
                        mCommand.Parameters.Add("PRM_PAYERBRANCH", entity.TRANSBRANCHNAME);
                        mCommand.Parameters.Add("PRM_CHEQUENO", entity.TRANSCHEQUENO);
                        mCommand.Parameters.Add("PRM_CHEQUEDATE", entity.TRANSCHEQUEDATE);
                        mCommand.Parameters.Add("PRM_RECEIPTAMOUNT", entity.TRANSAMOUNT);
                        mCommand.Parameters.Add("PRM_CURRENCY", entity.TRANSCURRENCY);
                        mCommand.Parameters.Add("PRM_PAYMENTDESCRIPTION", entity.TRANSDESCRIPTION);

                        mCommand.Parameters.Add("PRM_ENTITYBRANCHKEY", entity.BRANCHENTITYKEY.ToString().Length > 0 ? entity.BRANCHENTITYKEY : 0);

                        mCommand.CommandText = "USP_OFFLINETRANSACTIONDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public void SaveBusinessCreditTransactionDetails(PaymentTransactionDetailsEntity entity, string userid)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("PRM_ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PRM_PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        mCommand.Parameters.Add("PRM_PAYMENTREFMESSAGE", entity.PAYMENTREFMESSAGE);
                        mCommand.Parameters.Add("PRM_CREATEDBY", entity.CREATEDBY);
                        mCommand.Parameters.Add("PRM_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PRM_PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("PRM_TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PRM_RECEIPTAMOUNT", entity.TRANSAMOUNT);
                        mCommand.Parameters.Add("PRM_CURRENCY", entity.TRANSCURRENCY);
                        mCommand.Parameters.Add("PRM_PAYMENTDESCRIPTION", entity.TRANSDESCRIPTION);
                        mCommand.Parameters.Add("PRM_USERID", userid.ToUpper());

                        mCommand.CommandText = "USP_BCREDITTRANSACTIONDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public void SaveBusinessCreditTransactionDetails(PaymentTransactionDetailsEntity entity, string userid, string DiscountCode, decimal DiscountAmount)
        {
            string serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            string PAYMENTREFMESSAGE = string.Empty;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("PRM_ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PRM_PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        PAYMENTREFMESSAGE = (entity.ADDITIONALCHARGESTATUS != null && (entity.ADDITIONALCHARGESTATUS.ToLower() == "paid")) ? entity.PAYMENTREFMESSAGE + "_" + "Additional" : entity.PAYMENTREFMESSAGE;
                        mCommand.Parameters.Add("PRM_PAYMENTREFMESSAGE", PAYMENTREFMESSAGE);
                        mCommand.Parameters.Add("PRM_CREATEDBY", entity.CREATEDBY);
                        mCommand.Parameters.Add("PRM_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PRM_PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("PRM_TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PRM_RECEIPTAMOUNT", entity.TRANSAMOUNT);
                        mCommand.Parameters.Add("PRM_CURRENCY", entity.TRANSCURRENCY);
                        mCommand.Parameters.Add("PRM_PAYMENTDESCRIPTION", entity.TRANSDESCRIPTION);
                        mCommand.Parameters.Add("PRM_USERID", userid.ToUpper());
                        mCommand.Parameters.Add("PRM_DISCOUNTCODE", DiscountCode);
                        mCommand.Parameters.Add("PRM_DISCOUNTAMOUNT  ", DiscountAmount);
                        mCommand.Parameters.Add("PRM_SERVERNAME  ", serverName);
                        mCommand.Parameters.Add("PRM_SOURCETYPE", entity.ADDITIONALCHARGESTATUS);
                        mCommand.Parameters.Add("PRM_ADDITIONALCHARGEAMOUNT", entity.ADDITIONALCHARGEAMOUNT);
                        mCommand.Parameters.Add("PRM_ADDCHARGEAMOUNTINUSD", entity.ADDITIONALCHARGEAMOUNTINUSD);

                        mCommand.CommandText = "USP_BCREDITTRANSACTIONDETAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        //Getting party details for  loading the shipper and consignee details in drop down
        public List<SSPPartyDetailsEntity> GetPartyDetails(string UserId)
        {
            List<SSPPartyDetailsEntity> mEntity = new List<SSPPartyDetailsEntity>();

            string Query = "SELECT  * from SSPPARTYDETAILS where CREATEDBY=:CREATEDBY and CLIENTNAME is not null";
            //if (!string.IsNullOrEmpty(Code.Trim()))
            //{
            //    Query = Query + " AND Upper(Code) like '%" + Code.ToUpper() + "%'";
            //}
            //if (!string.IsNullOrEmpty(Name.Trim()))
            //{
            //    Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
            //}
            ReadText(DBConnectionMode.SSP, Query,
                new OracleParameter[] {
                    new OracleParameter {OracleDbType = OracleDbType.Varchar2, ParameterName = "CREATEDBY" , Value = UserId }
                }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<SSPPartyDetailsEntity>(r));
            });

            return mEntity;
        }


        //Getting party details from SSPClientMaster table for auto populating the values 
        public List<SSPClientMasterEntity> GetPartyDetailsFromSSPClientMaster(string UserId)
        {
            List<SSPClientMasterEntity> mEntity = new List<SSPClientMasterEntity>();

            string Query = "SELECT  * from SSPCLIENTMASTER where CREATEDBY=:CREATEDBY and CLIENTNAME is not null";
            //if (!string.IsNullOrEmpty(Code.Trim()))
            //{
            //    Query = Query + " AND Upper(Code) like '%" + Code.ToUpper() + "%'";
            //}
            //if (!string.IsNullOrEmpty(Name.Trim()))
            //{
            //    Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
            //}
            ReadText(DBConnectionMode.SSP, Query,
                new OracleParameter[] { 
                    new OracleParameter {OracleDbType = OracleDbType.Varchar2, ParameterName = "CREATEDBY" , Value = UserId }
                }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<SSPClientMasterEntity>(r));
            });

            return mEntity;
        }

        //Getting countrycode by countryid

        public string GetCountryCodeById(long Countryid)
        {
            CountryEntity country = new CountryEntity();
            ReadText(DBConnectionMode.FOCiS, "select C.CODE from SMDM_COUNTRY C Inner Join DataProfileClassWFStates DP on C.STATEID=DP.STATEID Where 1=1 and C.COUNTRYID=:COUNTRYID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "COUNTRYID", Value = Countryid } }
            , delegate(IDataReader r)
            {
                country = DataReaderMapToObject<CountryEntity>(r);
            });

            return country.CODE;
        }

        //Gettting countryid by countrycode

        public long GetCountryIdByCountryCode(string CountryCode)
        {
            CountryEntity country = new CountryEntity();
            ReadText(DBConnectionMode.FOCiS, "select C.COUNTRYID from SMDM_COUNTRY C Inner Join DataProfileClassWFStates DP on C.STATEID=DP.STATEID Where 1=1 and C.CODE=:CODE",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CODE", Value = CountryCode } }
            , delegate(IDataReader r)
            {
                country = DataReaderMapToObject<CountryEntity>(r);
            });

            return country.COUNTRYID;
        }

        //Getting statecode by subdivisionid
        public string GetStateCodeBySubDivisionId(long subdivisionid)
        {

            StateEntity stateentity = new StateEntity();
            ReadText(DBConnectionMode.FOCiS, "Select SUBDIVISIONID,COUNTRYID,CODE,NAME,DESCRIPTION from SMDM_SUBDIVISIONS where SUBDIVISIONID=:SUBDIVISIONID",
                 new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "SUBDIVISIONID", Value = subdivisionid } }
             , delegate(IDataReader r)
             {
                 stateentity = DataReaderMapToObject<StateEntity>(r);
             });
            return stateentity.CODE;
        }

        //Getting Subdivisionid by statecode
        //Getting statecode by subdivisionid
        public long GetSubDivisionIdByStateCode(string statecode, long CountryId)
        {

            StateEntity stateentity = new StateEntity();
            ReadText(DBConnectionMode.FOCiS, "Select SUBDIVISIONID,COUNTRYID,CODE,NAME,DESCRIPTION from SMDM_SUBDIVISIONS where CODE=:CODE and COUNTRYID=:COUNTRYID",
                 new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CODE", Value = statecode }, new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "COUNTRYID", Value = CountryId } }
             , delegate(IDataReader r)
             {
                 stateentity = DataReaderMapToObject<StateEntity>(r);
             });
            return stateentity.SUBDIVISIONID;
        }

        #region Getting partydetails by passing party details id


        public SSPPartyDetailsEntity GetPartyDetForAutoFillByPartyDetailsId(long partydetailsid)
        {

            SSPPartyDetailsEntity party = new SSPPartyDetailsEntity();
            ReadText(DBConnectionMode.SSP, "Select * from SSPPARTYDETAILS where PARTYDETAILSID=:PARTYDETAILSID",
                 new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "PARTYDETAILSID", Value = partydetailsid } }
             , delegate(IDataReader r)
             {
                 party = DataReaderMapToObject<SSPPartyDetailsEntity>(r);
             });
            return party;
        }


        public SSPClientMasterEntity GetClientMasterForAutoFillBySSPclientId(string sspclientid, string Userid)
        {
            string Query = "";

            if (sspclientid == "-1206") // Get Details from USers table
            {
                Query = @"select  u.USERID as EMAILID,   u.FIRSTNAME, u.LASTNAME, u.COMPANYNAME as CLIENTNAME,TO_CHAR(u.MOBILENUMBER)as PHONE ,
                        uad.COUNTRYID,u.SALUTATION,
                         uad.COUNTRYCODE, uad.COUNTRYNAME, 
                        uad.HOUSENO,uad.ADDRESSTYPE, uad.BUILDINGNAME, uad.HOUSENO, uad.ADDRESSLINE1 as STREET1,
                        uad.ADDRESSLINE2 as STREET2,TO_CHAR(uad.STATEID) as STATEID, uad.STATECODE, uad.STATENAME, uad.CITYID, uad.CITYCODE, uad.CITYNAME, 
                        uad.POSTCODE as PINCODE,uad.DATECREATED,uad.DATEMODIFIED,uad.FULLADDRESS
                        from USERS u
                        inner join UMUSERADDRESSDETAILS uad on uad.userid =u.userid Where u.USERID='" + Userid + "'";
            }
            else
            {
                Query = "Select * from SSPCLIENTMASTER where SSPCLIENTID=" + sspclientid;
            }
            SSPClientMasterEntity party = new SSPClientMasterEntity();
            ReadText(DBConnectionMode.SSP, Query,
                 new OracleParameter[] { new OracleParameter { } }
             , delegate(IDataReader r)
             {
                 party = DataReaderMapToObject<SSPClientMasterEntity>(r);
             });
            return party;
        }

        #endregion

        public DataTable GetHsCodesBasedonCountryCode(string CountryCode, string Inputhscode)
        {

            OracleParameter op = null;
            DataTable dt = new DataTable();
            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.CommandText = "usp_ssp_get_hscode_dtls";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("ip_UnlocationID", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = CountryCode;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_HSCODE", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Inputhscode;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_HSCodeDetails", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        OracleDataReader Hscodedetails = ((OracleRefCursor)mCommand.Parameters["OP_HSCodeDetails"].Value).GetDataReader();

                        dt.Load(Hscodedetails);
                    }
                    catch (Exception EX)
                    {
                    }
                }
            }
            return dt;
        }

        public DataTable GetCustomerDocumentList()
        {
            string Query = @"Select ID,TRACKINGDOCUMENT,DMSDOCUMENTTYPE as DOCUMENTNAME from SSPCUSTOMERDOCUMENTS where  STATUS=1";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { } });
            return dt;
        }

        public DataSet GetComplianceAndDutysOfHsCode(long OriginPlaceID, long DestinationPlaceID, string HSCode, string HSCodeDes)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();

            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.CommandText = "USP_SSP_GET_HSC_INSTRC"; //USP_SSP_GET_HSC_DETAILS //USP_SSP_GET_HSC_INSTRUCTIONS
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_ORG_PLACEID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = OriginPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_DEST_PLACEID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = DestinationPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_ORG_HSCODE", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = HSCode;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_DEST_HSCODE", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = HSCodeDes;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_CODE", OracleDbType.Int64);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_MSG", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_INSTRUCTION_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_DEST_HSCODE_DTLS", OracleDbType.RefCursor, ParameterDirection.Output);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_HSCODE_DESCIPTION", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_DUTY_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        mCommand.ExecuteNonQuery();

                        int Successcode = Convert.ToInt32(((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["OP_SUCCESS_CODE"].Value)).Value);
                        //string Success_Msg = Convert.ToString(((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["OP_SUCCESS_MSG"].Value)).Value);

                        if (Successcode == 1)
                        {
                            OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["OP_HSCODE_DESCIPTION"].Value).GetDataReader();
                            OracleDataReader reader2 = ((OracleRefCursor)mCommand.Parameters["OP_DEST_HSCODE_DTLS"].Value).GetDataReader();
                            OracleDataReader HsCodeDes = ((OracleRefCursor)mCommand.Parameters["OP_INSTRUCTION_DATA"].Value).GetDataReader();
                            OracleDataReader Hscodedetails = ((OracleRefCursor)mCommand.Parameters["OP_DUTY_DATA"].Value).GetDataReader();

                            DataTable dt = new DataTable();

                            dt.Load(reader1);
                            ds.Tables.Add(dt);

                            dt = new DataTable();
                            dt.Load(reader2);
                            ds.Tables.Add(dt);

                            dt = new DataTable();
                            dt.Load(HsCodeDes);
                            ds.Tables.Add(dt);

                            dt = new DataTable();
                            dt.Load(Hscodedetails);
                            ds.Tables.Add(dt);

                            reader1.Close(); reader1.Dispose(); reader2.Close(); reader2.Dispose(); HsCodeDes.Dispose(); Hscodedetails.Dispose();
                            mConnection.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }

                }
            }
            return ds;
        }

        public DataSet GetCountryInstructions(long OriginPlaceID, long DestinationPlaceID)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();

            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        //------------------Documents Country Instructins---------------------
                        mCommand.Parameters.Clear();

                        mCommand.CommandText = "USP_SHIPA_GET_COUNTRY_INSTR";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_ORG_COUNTRYID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = OriginPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_DEST_COUNTRYID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = DestinationPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_CODE", OracleDbType.Int64);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_MSG", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);


                        op = new OracleParameter("OP_INSTRUCTION_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);


                        op = new OracleParameter("OP_CUN_INSTRUCTION_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_FIELDS_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        mCommand.ExecuteNonQuery();

                        OracleDataReader readerCountry = ((OracleRefCursor)mCommand.Parameters["OP_INSTRUCTION_DATA"].Value).GetDataReader();
                        DataTable dt = new DataTable();

                        OracleDataReader readerCountryIns = ((OracleRefCursor)mCommand.Parameters["OP_CUN_INSTRUCTION_DATA"].Value).GetDataReader();
                        OracleDataReader FIELDSDATA = ((OracleRefCursor)mCommand.Parameters["OP_FIELDS_DATA"].Value).GetDataReader();


                        dt.Load(readerCountry);
                        ds.Tables.Add(dt);
                        dt.TableName = "Documents";

                        dt = new DataTable();
                        dt.Load(readerCountryIns);
                        dt.TableName = "CountryIns";
                        ds.Tables.Add(dt);

                        dt = new DataTable();
                        dt.Load(FIELDSDATA);
                        dt.TableName = "FIELDSDATA";
                        ds.Tables.Add(dt);


                        readerCountry.Close(); readerCountry.Dispose(); readerCountryIns.Close(); readerCountryIns.Dispose(); FIELDSDATA.Close(); FIELDSDATA.Dispose();
                        //----------------------------------------------------------

                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return ds;
        }


        public DataSet GetCountryInstructions_Old(long OriginPlaceID, long DestinationPlaceID)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();

            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        //------------------Country Instructins---------------------
                        mCommand.Parameters.Clear();

                        mCommand.CommandText = "USP_SSP_GET_COUNTRY_INSTR";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_ORG_PLACEID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = OriginPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_DEST_PLACEID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = DestinationPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_CODE", OracleDbType.Int64);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_MSG", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_INSTRUCTION_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);


                        op = new OracleParameter("OP_FIELDS_DATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        OracleDataReader readerCountry = ((OracleRefCursor)mCommand.Parameters["OP_INSTRUCTION_DATA"].Value).GetDataReader();
                        DataTable dt = new DataTable();

                        OracleDataReader readerFields = ((OracleRefCursor)mCommand.Parameters["OP_FIELDS_DATA"].Value).GetDataReader();


                        dt.Load(readerCountry);
                        dt.TableName = "CountryIns";
                        ds.Tables.Add(dt);

                        dt = new DataTable();
                        dt.Load(readerFields);
                        dt.TableName = "Fields";
                        ds.Tables.Add(dt);

                        readerCountry.Close(); readerCountry.Dispose(); readerFields.Close(); readerFields.Dispose();
                        //----------------------------------------------------------

                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return ds;
        }
        public DataSet GetSavedHscodeInstructionsDutys(string Jobnumber)
        {
            DataSet ds = new DataSet();

            string Query = @"select hscode.HSCODE,hscode.HSNAME,hsins.INSTRUCATIONTYPE,hsins.INSTRUCATION
                                         from FOCSSP.HSCODEDETAILS hscode
                                        inner join FOCSSP.HSCODEINSTRUCATIONS hsins on hsins.HSCODEDTLSID=hscode.HSCODEDTLSID
                                        where JOBNUMBER=:JOBNUMBER";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "JOBNUMBER", Value= Convert.ToInt64(Jobnumber) } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet GetSavedHscodeDuties(string Jobnumber)
        {
            DataSet ds = new DataSet();

            //Hscode Duty's Based on Jobnumber
            string Query = @"select hscode.HSCODE, HSDUTY.DUTYTYPE,HSDUTY.DUTYRATEPERCENTAGE
                        from FOCSSP.HSCODEDETAILS hscode
                        inner join HScodeDUTY HSDUTY on HSDUTY.HSCODEDTLSID=hscode.HSCODEDTLSID where JOBNUMBER=:JOBNUMBER";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "JOBNUMBER", Value= Convert.ToInt64(Jobnumber) } 
            });
            ds.Tables.Add(dt);

            return ds;
        }

        public DataSet GetSavedHscodeInstructionsDutys(string QuotationID, string Jobnumber)
        {
            DataSet ds = new DataSet();

            string Query = @"select hscode.HSCODE,hscode.HSNAME,hsins.INSTRUCATIONTYPE,hsins.INSTRUCATION
                                         from FOCSSP.HSCODEDETAILS hscode
                                        inner join FOCSSP.HSCODEINSTRUCATIONS hsins on hsins.HSCODEDTLSID=hscode.HSCODEDTLSID
                                        where JOBNUMBER=0 and QuotationID=:QUOTATIONID";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "QUOTATIONID", Value= Convert.ToInt64(QuotationID) } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet GetSavedHscodeDuties(string QuotationID, string Jobnumber)
        {
            DataSet ds = new DataSet();

            //Hscode Duty's Based on Jobnumber
            string Query = @"select hscode.HSCODE, HSDUTY.DUTYTYPE,HSDUTY.DUTYRATEPERCENTAGE
                        from FOCSSP.HSCODEDETAILS hscode
                        inner join HScodeDUTY HSDUTY on HSDUTY.HSCODEDTLSID=hscode.HSCODEDTLSID where JOBNUMBER=0 and QuotationID=:QUOTATIONID";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "QUOTATIONID", Value= Convert.ToInt64(QuotationID)  } });
            ds.Tables.Add(dt);

            return ds;
        }

        public DataSet GetSavedHSCodes(string Jobnumber)
        {
            DataSet ds = new DataSet();

            string Query = @"select HSCODE,HSNAME,ORG_HSCODE,HSCODETYPE from FOCSSP.HSCODEDETAILS where JOBNUMBER=:JOBNUMBER";// + " AND HSCODETYPE='O'";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "JOBNUMBER", Value= Convert.ToInt64(Jobnumber)  } });
            ds.Tables.Add(dt);
            return ds;
        }

        //public DataSet GetSavedDesHSCodes(string Jobnumber)
        //{
        //    DataSet ds = new DataSet();

        //    string Query = @"select HSCODE,HSNAME,ORG_HSCODE from FOCSSP.HSCODEDETAILS where JOBNUMBER=" + Jobnumber + " AND HSCODETYPE='D'";

        //    DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { } });
        //    ds.Tables.Add(dt);
        //    return ds;
        //}

        //public DataSet GetSavedHSCodes(string QuotationID, string Jobnumber)
        //{
        //    DataSet ds = new DataSet();

        //    string Query = @"select HSCODE,HSNAME,ORG_HSCODE from FOCSSP.HSCODEDETAILS where JOBNUMBER=0 and QuotationID=" + QuotationID;

        //    DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { } });
        //    ds.Tables.Add(dt);
        //    return ds;
        //}

        public DataSet GetUpdatedHSCodeInstructions(string Jobnumber, string HSCode)
        {
            DataSet ds = new DataSet();

            string Query = @"select hscode.HSCODE,hscode.HSNAME,hsins.INSTRUCATIONTYPE,hsins.INSTRUCATION
                                         from FOCSSP.HSCODEDETAILS hscode
                                        inner join FOCSSP.HSCODEINSTRUCATIONS hsins on hsins.HSCODEDTLSID=hscode.HSCODEDTLSID
                                        where JOBNUMBER=:JOBNUMBER AND hscode.ORG_HSCODE IN (:ORG_HSCODE)";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "JOBNUMBER", Value= Convert.ToInt64(Jobnumber)  } ,
                new OracleParameter { OracleDbType = OracleDbType.Varchar2 , ParameterName = "ORG_HSCODE", Value= HSCode  } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet GetUpdatedHSCodeDuties(string Jobnumber, string HSCode)
        {
            DataSet ds = new DataSet();

            //Hscode Duty's Based on Jobnumber
            string Query = @"select hscode.HSCODE, HSDUTY.DUTYTYPE,HSDUTY.DUTYRATEPERCENTAGE
                        from FOCSSP.HSCODEDETAILS hscode
                        inner join HScodeDUTY HSDUTY on HSDUTY.HSCODEDTLSID=hscode.HSCODEDTLSID where JOBNUMBER=:JOBNUMBER AND hscode.ORG_HSCODE IN (:ORG_HSCODE)";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long , ParameterName = "JOBNUMBER", Value= Convert.ToInt64(Jobnumber) },
                new OracleParameter { OracleDbType = OracleDbType.Varchar2 , ParameterName = "ORG_HSCODE", Value= HSCode  } 
            });
            ds.Tables.Add(dt);

            return ds;
        }

        public DataTable GetHscodeISExistCount(string QuotationId)
        {
            string Query = @"select HSCODE
                         from FOCSSP.HSCODEDETAILS hscode
                        inner join FOCSSP.HSCODEINSTRUCATIONS hsins on hsins.HSCODEDTLSID=hscode.HSCODEDTLSID
                        where JOBNUMBER=0 and QuotationID=:QUOTATIONID";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter {  OracleDbType = OracleDbType.Long , ParameterName = "QUOTATIONID", Value= Convert.ToInt64(QuotationId)} 
            });
            return dt;
        }

        public IList<PartyItems> GetPartyData(string p, string jobnumber)
        {
            List<PartyItems> mEntity = new List<PartyItems>();

            long Count = 0;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.CommandText = "SELECT COUNT(*) FROM SSPCLIENTMASTER WHERE CREATEDBY=:CREATEDBY and CLIENTNAME is not null";
                    mCommand.Parameters.Add("CREATEDBY", p);
                    Count = Convert.ToInt64(mCommand.ExecuteScalar());
                    mConnection.Close();
                }
            }

            string Query = "";
            if (Count > 0)
            {
                //                if (jobnumber == "0")
                //                {
                //                    //Book now
                //                    Query = @"SELECT  TO_CHAR(SSPCLIENTID) AS CLIENTID,CLIENTNAME,PARTYTYPE
                //                        from SSPCLIENTMASTER where CREATEDBY='" + p + "'" + "and CLIENTNAME is not null UNION Select TO_CHAR(-1206) as CLIENTID,COMPANYNAME as CLIENTNAME," + "'" + 91 + "'" + " as PARTYTYPE from USERS where USERID='" + p + "'";
                //                }
                //                else
                //                {
                //Resume booking
                Query = "SELECT  TO_CHAR(SSPCLIENTID) AS CLIENTID,CLIENTNAME,PARTYTYPE from SSPCLIENTMASTER where CREATEDBY=:CREATEDBY and CLIENTNAME is not null";
                //}
            }
            else //Fisrt Time job Booking
                Query = "Select TO_CHAR(-1206) AS CLIENTID,COMPANYNAME as CLIENTNAME,'" + 91 + "'" + " as PARTYTYPE from USERS where USERID=:CREATEDBY";


            ReadText(DBConnectionMode.SSP, Query,
                new OracleParameter[] { 
                    new OracleParameter{ OracleDbType = OracleDbType.Varchar2, ParameterName="CREATEDBY", Value=p }
                }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<PartyItems>(r));
            });
            return mEntity;
        }

        public IList<SSPDocumentsEntity> GetDocumentDetailsbyDocumetnId(string DOCID, string Userid)
        {
            //Shipment Details List
            List<SSPDocumentsEntity> mShipmentItems = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE DOCID = :DOCID ", //and CREATEDBY=:CREATEDBY ,new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "CREATEDBY", Value = Userid }
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "DOCID", Value = DOCID } }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            return mShipmentItems;
        }

        public List<BranchCityEntity> GetBranchCity(string USERID)
        {
            List<BranchCityEntity> mEntity = new List<BranchCityEntity>();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_USERID", USERID);

                        mCommand.Parameters.Add("CUR_GETBRANCHCITY", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_GETBRANCHCITY";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtJobBooking = new DataTable();
                        OracleDataReader JobBookingReader = ((OracleRefCursor)mCommand.Parameters["CUR_GETBRANCHCITY"].Value).GetDataReader();
                        dtJobBooking.Load(JobBookingReader);

                        List<DBFactory.Entities.BranchCityEntity> GETBRANCHCITY = MyClass.ConvertToList<DBFactory.Entities.BranchCityEntity>(dtJobBooking);

                        JobBookingReader.Close();
                        mConnection.Close();
                        mEntity = GETBRANCHCITY;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }

        public List<SSPBranchListEntity> GetMyLocation(string location)
        {
            List<SSPBranchListEntity> mBranchList = new List<SSPBranchListEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM FOCIS.SSPPAYMENTBRANCHLIST WHERE UPPER(SITECITY)= :SITECITY",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "SITECITY", Value = location.ToString().ToUpper() } }
            , delegate(IDataReader r)
            {
                mBranchList.Add(DataReaderMapToObject<SSPBranchListEntity>(r));
            });

            return mBranchList;
        }

        public List<SSPEntityListEntity> GetWireCity(string user, string currency)
        {
            var mBranchList = new List<SSPEntityListEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT PS.COUNTRYID,PS.SSPPAYMENTSETUPID AS ENTITYKEY,PS.ACCHOLDERNAME AS BENEFICIARYNAME,PS.BANKBRANCH AS BANKNAME,PS.WIREACCOUNTNUMBER AS BANKACCOUNTNUMBER,PS.BRANCHADDRESS AS BRANCH,SUBSTR(PS.IFSC, 1, 50) AS IFSCCODE,PS.SWIFTCODE  FROM FOCIS.SSPPAYMENTSETUP PS JOIN FOCIS.SSPWIRETRANSFERCURRENCY WCU on PS.SSPPAYMENTSETUPID=WCU.SSPPAYMENTSETUPID WHERE PS.COUNTRYID = (SELECT DISTINCT UD.COUNTRYID FROM UMUSERADDRESSDETAILS UD WHERE UPPER(UD.USERID)=UPPER(:USERID))",
                new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "USERID", Value = user.ToString().ToUpper() }
                }
            , delegate(IDataReader r)
            {
                mBranchList.Add(DataReaderMapToObject<SSPEntityListEntity>(r));
            });

            return mBranchList;
        }

        public List<SSPBranchListEntity> GetBranList(string user, string currency)
        {
            var mBranchList = new List<SSPBranchListEntity>();
            var query = @"
                            SELECT BRANCHKEY, SITECITY, ISCASH, ISCHEQUE, ISDD, SITENAME,SITEADDRESS
                            FROM FOCIS.SSPPAYMENTBRANCHLIST  
                            WHERE UPPER(COUNTRYCODE)=
                                (SELECT DISTINCT UPPER(COUNTRYCODE)
                                FROM UMUSERADDRESSDETAILS
                                WHERE UPPER(USERID)=UPPER(:USERID)
                                ) 
                             AND (lower(STATEID) NOT IN ('deleted') OR lower(STATEID) is null)
                        ";
            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "USERID", Value = user.ToString().ToUpper() }
                }
            , delegate(IDataReader r)
            {
                mBranchList.Add(DataReaderMapToObject<SSPBranchListEntity>(r));
            });

            return mBranchList;
        }


        public dynamic GetCurrencyConversion(string amount, string from, string to)
        {
            string Query = "";
            //Query = @"SELECT FOCIS.RATECALC_GET_EXCHANGE_RATE((ROUND((:AMOUNT),2) ),:FROMCURR,:TOCURR) as currency FROM DUAL ";
            Query = @"
                        SELECT TRIM(TO_CHAR(
                            (SELECT FOCIS.RATECALC_GET_EXCHANGE_RATE((ROUND((:AMOUNT),2) ),:FROMCURR,:TOCURR) FROM DUAL ),
                            RPAD(LPAD('0d',38,'9'),38 + (SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID) = :TOCURR ),'0'))) 
                            as currency
                        FROM DUAL
                    ";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "AMOUNT", Value = amount},
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "FROMCURR", Value = from } ,
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "TOCURR", Value = to } 
            });
            return dt;

        }

        public dynamic GetCurrencyRounded(string amount, string cur)
        {
            string Query = "";
            Query = @"
                        SELECT TRIM(TO_CHAR(
                            :AMOUNT,
                            RPAD(LPAD('0d',38,'9'),38 + (SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID) = :CURR ),'0'))) 
                            as currency,
                            (SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID) = :CURR ) as DecimalCount
                        FROM DUAL
                    ";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "AMOUNT", Value = amount},
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CURR", Value = cur } 
            });
            return dt;

        }

        public SSPOfflineBranchEntity GetCurrencies(string USERID)
        {
            OracleConnection mConnection;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_USERID", USERID);

                        mCommand.Parameters.Add("CUR_SSPPAYCURRENCYLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPWIRECURRENCYLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPUSERCURRENCY", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_CURRENCIES";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader SSPPAYCURRENCYLISTREADER = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPAYCURRENCYLIST"].Value).GetDataReader();

                        DataTable dt = new DataTable();
                        dt.Load(SSPPAYCURRENCYLISTREADER);
                        List<SSPBranchListEntity> Blist = MyClass.ConvertToList<SSPBranchListEntity>(dt);

                        var dt2 = new DataTable();
                        OracleDataReader SSPWIRECURRENCYLISTREADER = ((OracleRefCursor)mCommand.Parameters["CUR_SSPWIRECURRENCYLIST"].Value).GetDataReader();
                        dt2.Load(SSPWIRECURRENCYLISTREADER);
                        List<SSPEntityListEntity> Wlist = MyClass.ConvertToList<SSPEntityListEntity>(dt2);

                        var dt3 = new DataTable();
                        OracleDataReader UserCurrencyReader = ((OracleRefCursor)mCommand.Parameters["CUR_SSPUSERCURRENCY"].Value).GetDataReader();
                        dt3.Load(UserCurrencyReader);

                        SSPPAYCURRENCYLISTREADER.Close();
                        SSPWIRECURRENCYLISTREADER.Close();
                        var data = new SSPOfflineBranchEntity()
                        {
                            SSPEntityList = Wlist,
                            SSPBranchList = Blist,
                            PREFERREDCURRENCYCODE = dt3.Rows[0]["PREFERREDCURRENCYCODE"].ToString()
                        };
                        mConnection.Close();
                        return data;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }


        #region Unused Methods

        //get by id for jobbooking demo
        public JobBookingListEntity GetByQuotationid(int id)
        {
            JobBookingListEntity mEntity = new JobBookingListEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMQUOTATION WHERE QUOTATIONID = :QUOTATIONID",
              new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
          , delegate(IDataReader r)
          {
              mEntity = DataReaderMapToObject<JobBookingListEntity>(r);
          });

            //Shipment Details List
            List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
            });
            mEntity.ShipmentItems = mShipmentItems;

            //Document Details List
            //List<SSPDocumentsEntity> mdocItems = new List<SSPDocumentsEntity>();
            //ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE QUOTATIONID = :QUOTATIONID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mdocItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            //});
            //mEntity.DocumentDetails = mdocItems;

            //JobDetails list 

            //List<SSPJobDetailsEntity> mjobItems = new List<SSPJobDetailsEntity>();
            //ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPJOBDETAILS WHERE QUOTATIONID = :QUOTATIONID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mjobItems.Add(DataReaderMapToObject<SSPJobDetailsEntity>(r));
            //});
            //mEntity.JobDetails = mjobItems;



            //ReadText(DBConnectionMode.SSP, "SELECT * FROM QMTERMSANDCONDITION WHERE QUOTATIONID = :QUOTATIONID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mEntity.TermAndConditions = DataReaderMapToList<QuotationTermsConditionEntity>(r);
            //});
            //ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPPARTYDETAILS WHERE PARTYDETAILSID = :PARTYDETAILSID",
            //    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "PARTYDETAILSID", Value = id } }
            //, delegate(IDataReader r)
            //{
            //    mEntity.PartyDetails = DataReaderMapToList<SSPPartyDetails>(r);
            //});
            return mEntity;
        }

        public int Save(JobBookingEntity entity)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            long Customerid = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    entity.SHISDEFAULTADDRESS = 0;

                    try
                    {

                        mCommand.CommandText = "SELECT COUNT(*) FROM SSPJOBDETAILS WHERE QUOTATIONID=:QUOTATIONID";
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        long quotecount = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.Parameters.Clear();

                        //Job Details
                        mCommand.CommandText = "SELECT SSP_JOBDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.JJOBNUMBER = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Insert into SSPJOBDETAILS (JOBNUMBER,OPERATIONALJOBNUMBER,QUOTATIONID,QUOTATIONNUMBER,INCOTERMCODE,INCOTERMDESCRIPTION,CARGOAVAILABLEFROM,CARGODELIVERYBY,DOCUMENTSREADY,SLINUMBER,CONSIGNMENTID,STATEID) values     (:JOBNUMBER,:OPERATIONALJOBNUMBER,:QUOTATIONID,:QUOTATIONNUMBER,:INCOTERMCODE,:INCOTERMDESCRIPTION,:CARGOAVAILABLEFROM,:CARGODELIVERYBY,:DOCUMENTSREADY,:SLINUMBER,:CONSIGNMENTID,:STATEID)";
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        quotecount = quotecount + 1;
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "Job Initiated";
                        mCommand.Parameters.Add("OPERATIONALJOBNUMBER", "A" + quotecount.ToString("00") + entity.QUOTATIONNUMBER.ToString().Replace("QT", ""));
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("QUOTATIONNUMBER", entity.QUOTATIONNUMBER);
                        mCommand.Parameters.Add("INCOTERMCODE", entity.JINCOTERMCODE);
                        mCommand.Parameters.Add("INCOTERMDESCRIPTION", entity.JINCOTERMDESCRIPTION);
                        mCommand.Parameters.Add("CARGOAVAILABLEFROM", entity.JCARGOAVAILABLEFROM);
                        mCommand.Parameters.Add("CARGODELIVERYBY", entity.JCARGODELIVERYBY);
                        //mCommand.Parameters.Add("ORIGINCUSTOMSCLEARANCEBY", entity.JORIGINCUSTOMSCLEARANCEBY);
                        //mCommand.Parameters.Add("DESTINATIONCUSTOMSCLEARANCEBY", entity.JDESTINATIONCUSTOMSCLEARANCEBY);
                        mCommand.Parameters.Add("DOCUMENTSREADY", entity.JDOCUMENTSREADY);
                        mCommand.Parameters.Add("SLINUMBER", entity.JSLINUMBER);
                        mCommand.Parameters.Add("CONSIGNMENTID", entity.JCONSIGNMENTID);
                        mCommand.Parameters.Add("STATEID", entity.STATEID);
                        mCommand.ExecuteNonQuery();
                        mCommand.Parameters.Clear();


                        mCommand.CommandText = "SELECT SSP_PARTYDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.SHPARTYDETAILSID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Insert into SSPPARTYDETAILS (PARTYDETAILSID,JOBNUMBER,CLIENTID,CLIENTNAME,PARTYTYPE,ADDRESSTYPE,HOUSENO,BUILDINGNAME,STREET1,STREET2,COUNTRYCODE,COUNTRYNAME,CITYCODE,CITYNAME,STATECODE,STATENAME,PINCODE,FULLADDRESS,ISDEFAULTADDRESS,LATITUDE,LONGITUDE,FIRSTNAME,LASTNAME,PHONE,EMAILID,SALUTATION) values (:PARTYDETAILSID,:JOBNUMBER,:CLIENTID,:CLIENTNAME,:PARTYTYPE,:ADDRESSTYPE,:HOUSENO,:BUILDINGNAME,:STREET1,:STREET2,:COUNTRYCODE,:COUNTRYNAME,:CITYCODE,:CITYNAME,:STATECODE,:STATENAME,:PINCODE,:FULLADDRESS,:ISDEFAULTADDRESS,:LATITUDE,:LONGITUDE,:FIRSTNAME,:LASTNAME,:PHONE,:EMAILID,:SALUTATION)";
                        mCommand.Parameters.Add("PARTYDETAILSID", entity.SHPARTYDETAILSID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CLIENTID", entity.SHCLIENTID);
                        mCommand.Parameters.Add("CLIENTNAME", entity.SHCLIENTNAME);
                        mCommand.Parameters.Add("PARTYTYPE", 91);
                        mCommand.Parameters.Add("ADDRESSTYPE", entity.SHADDRESSTYPE);
                        mCommand.Parameters.Add("HOUSENO", entity.SHHOUSENO);
                        mCommand.Parameters.Add("BUILDINGNAME", entity.SHBUILDINGNAME);
                        mCommand.Parameters.Add("STREET1", entity.SHSTREET1);
                        mCommand.Parameters.Add("STREET2", entity.SHSTREET2);
                        mCommand.Parameters.Add("COUNTRYCODE", entity.SHCOUNTRYCODE);
                        mCommand.Parameters.Add("COUNTRYNAME", entity.SHCOUNTRYNAME);
                        mCommand.Parameters.Add("CITYCODE", entity.SHCITYCODE);
                        mCommand.Parameters.Add("CITYNAME", entity.SHCITYNAME);
                        mCommand.Parameters.Add("STATECODE", entity.SHSTATECODE);
                        mCommand.Parameters.Add("STATENAME", entity.SHSTATENAME);
                        mCommand.Parameters.Add("PINCODE", entity.SHPINCODE);
                        mCommand.Parameters.Add("FULLADDRESS", entity.SHFULLADDRESS);
                        mCommand.Parameters.Add("ISDEFAULTADDRESS", entity.SHISDEFAULTADDRESS);
                        mCommand.Parameters.Add("LATITUDE", entity.SHLATITUDE);
                        mCommand.Parameters.Add("LONGITUDE", entity.SHLONGITUDE);

                        mCommand.Parameters.Add("FIRSTNAME", entity.SHFIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.SHLASTNAME);
                        mCommand.Parameters.Add("PHONE", entity.SHPHONE);
                        mCommand.Parameters.Add("EMAILID", entity.SHEMAILID);
                        mCommand.Parameters.Add("SALUTATION", entity.SHSALUTATION);

                        mCommand.ExecuteNonQuery();
                        mCommand.Parameters.Clear();

                        //consignee details
                        mCommand.CommandText = "SELECT SSP_PARTYDETAILS_SEQ.NEXTVAL FROM DUAL";
                        entity.SHPARTYDETAILSID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "Insert into SSPPARTYDETAILS (PARTYDETAILSID,JOBNUMBER,CLIENTID,CLIENTNAME,PARTYTYPE,ADDRESSTYPE,HOUSENO,BUILDINGNAME,STREET1,STREET2,COUNTRYCODE,COUNTRYNAME,CITYCODE,CITYNAME,STATECODE,STATENAME,PINCODE,FULLADDRESS,ISDEFAULTADDRESS,LATITUDE,LONGITUDE,FIRSTNAME,LASTNAME,PHONE,EMAILID,SALUTATION) values (:PARTYDETAILSID,:JOBNUMBER,:CLIENTID,:CLIENTNAME,:PARTYTYPE,:ADDRESSTYPE,:HOUSENO,:BUILDINGNAME,:STREET1,:STREET2,:COUNTRYCODE,:COUNTRYNAME,:CITYCODE,:CITYNAME,:STATECODE,:STATENAME,:PINCODE,:FULLADDRESS,:ISDEFAULTADDRESS,:LATITUDE,:LONGITUDE,:FIRSTNAME,:LASTNAME,:PHONE,:EMAILID,:SALUTATION)";

                        mCommand.Parameters.Add("PARTYDETAILSID", entity.SHPARTYDETAILSID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("JOBNUMBER", entity.JJOBNUMBER);
                        mCommand.Parameters.Add("CLIENTID", entity.CONCLIENTID);
                        mCommand.Parameters.Add("CLIENTNAME", entity.CONCLIENTNAME);
                        mCommand.Parameters.Add("PARTYTYPE", 92);
                        mCommand.Parameters.Add("ADDRESSTYPE", entity.CONADDRESSTYPE);
                        mCommand.Parameters.Add("HOUSENO", entity.CONHOUSENO);
                        mCommand.Parameters.Add("BUILDINGNAME", entity.CONBUILDINGNAME);
                        mCommand.Parameters.Add("STREET1", entity.CONSTREET1);
                        mCommand.Parameters.Add("STREET2", entity.CONSTREET2);
                        mCommand.Parameters.Add("COUNTRYCODE", entity.CONCOUNTRYCODE);
                        mCommand.Parameters.Add("COUNTRYNAME", entity.CONCOUNTRYNAME);
                        mCommand.Parameters.Add("CITYCODE", entity.CONCITYCODE);
                        mCommand.Parameters.Add("CITYNAME", entity.CONCITYNAME);
                        mCommand.Parameters.Add("STATECODE", entity.CONSTATECODE);
                        mCommand.Parameters.Add("STATENAME", entity.CONSTATENAME);
                        mCommand.Parameters.Add("PINCODE", entity.CONPINCODE);
                        mCommand.Parameters.Add("FULLADDRESS", entity.CONFULLADDRESS);
                        mCommand.Parameters.Add("ISDEFAULTADDRESS", entity.CONISDEFAULTADDRESS);
                        mCommand.Parameters.Add("LATITUDE", entity.CONLATITUDE);
                        mCommand.Parameters.Add("LONGITUDE", entity.CONLONGITUDE);
                        mCommand.Parameters.Add("FIRSTNAME", entity.CONFIRSTNAME);
                        mCommand.Parameters.Add("LASTNAME", entity.CONLASTNAME);
                        mCommand.Parameters.Add("PHONE", entity.CONPHONE);
                        mCommand.Parameters.Add("EMAILID", entity.CONEMAILID);
                        mCommand.Parameters.Add("SALUTATION", entity.CONSALUTATION);

                        mCommand.ExecuteNonQuery();

                        ////Getting Customerid from quotation table
                        //mCommand.CommandText = "SELECT CUSTOMERID FROM QMQUOTATION WHERE QUOTATIONID=:QUOTATIONID";
                        //mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        // Customerid = Convert.ToInt64(mCommand.ExecuteScalar());
                        //mCommand.Parameters.Clear();

                        //mCommand.Parameters.Clear();
                        //mCommand.Parameters.Add("PRM_JOBNUMBER  ", entity.JJOBNUMBER);
                        //mCommand.CommandText = "USP_SSP_SSPSTGDTLS";
                        //mCommand.CommandType = CommandType.StoredProcedure;
                        //mCommand.ExecuteNonQuery();


                        //Charge details

                        //mCommand.Parameters.Clear();
                        //mCommand.Parameters.Add("IP_JOBNUMBER ", entity.JJOBNUMBER).Direction = ParameterDirection.Input;
                        //mCommand.Parameters.Add("IP_QUOTATIONID", entity.QUOTATIONID).Direction = ParameterDirection.Input;
                        //mCommand.Parameters.Add("IP_USERID", UserId).Direction = ParameterDirection.Input;
                        //mCommand.Parameters.Add("OP_MESSAGE", OracleDbType.Varchar2, 10).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("OP_NEW_RECEIPTID", OracleDbType.Int64, 18).Direction = ParameterDirection.Output;
                        //mCommand.Parameters.Add("OP_NEW_RECEIPTNO", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                        //mCommand.CommandText = "USP_JA_INS_RECEIPTDETAILS";
                        //mCommand.CommandType = CommandType.StoredProcedure;
                        //mCommand.ExecuteNonQuery();



                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return Convert.ToInt32(Customerid);
        }

        public void Update(JobBookingEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int key)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        //Getting quote details and job details list for JobBooking List page
        public List<JobBookingListEntity> GetSavedQuotes(string userId, string status)
        {
            string mQuery = string.Empty;
            status = (status ?? "").ToUpperInvariant().Trim();
            switch (status)
            {
                case "ACTIVE":
                    mQuery = " and (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) OR job.STATEID  = 'Payment in Pending') AND ((job.STATEID  = 'Job Initiated') OR (job.STATEID  = 'Payment in Progress')) ";
                    break;
                case "EXPIRED":
                    mQuery = " and (QU.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) AND job.STATEID  <> 'Payment in Pending' OR (job.STATEID  = 'Payment Completed'))";
                    break;
                default:
                    break;
            }
            List<JobBookingListEntity> mEntity = new List<JobBookingListEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT QU.*,job.JOBNUMBER,job.OPERATIONALJOBNUMBER,job.STATEID as JSTATEID FROM QMQUOTATION QU inner join  SSPJOBDETAILS job on  QU.QUOTATIONID=job.QUOTATIONID  WHERE QU.CREATEDBY = :CUSTOMERID" + mQuery,
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<JobBookingListEntity>(r));
            });

            List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN(SELECT QUOTATIONID FROM QMQUOTATION WHERE CREATEDBY = :CUSTOMERID) ",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
            });
            foreach (var item in mEntity)
            {
                item.ShipmentItems = mShipmentItems.FindAll(delegate(QuotationShipmentEntity qsItems)
                {
                    return qsItems.QUOTATIONID == item.QUOTATIONID;
                });
            }

            List<SSPJobDetailsEntity> mJobItems = new List<SSPJobDetailsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPJOBDETAILS WHERE QUOTATIONID IN(SELECT QUOTATIONID FROM QMQUOTATION WHERE CREATEDBY = :CUSTOMERID) ",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mJobItems.Add(DataReaderMapToObject<SSPJobDetailsEntity>(r));
            });
            foreach (var item in mEntity)
            {
                item.JobItems = mJobItems.FindAll(delegate(SSPJobDetailsEntity qsItems)
                {
                    return qsItems.QUOTATIONID == item.QUOTATIONID;
                });
            }

            //Receipt Header Details
            List<ReceiptHeaderEntity> header = new List<ReceiptHeaderEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT RECEIPT.JOBNUMBER,RECEIPT.RECEIPTAMOUNT,RECEIPT.RECEIPTTAX,RECEIPT.RECEIPTNETAMOUNT FROM  JARECEIPTHEADER RECEIPT WHERE CREATEDBY = :CUSTOMERID",
               new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
           , delegate(IDataReader r)
           {
               header.Add(DataReaderMapToObject<ReceiptHeaderEntity>(r));
           });
            foreach (var item in mEntity)
            {
                item.ReceiptHeaderDetails = header.FindAll(delegate(ReceiptHeaderEntity qsItems)
                {
                    return qsItems.JOBNUMBER == item.JOBNUMBER;
                });
            }

            return mEntity;
        }

        //Get Counts of jobs
        public JobBookingStatusCountEntity GetJobStatusCount(string userId)
        {
            JobBookingStatusCountEntity mEntity = new JobBookingStatusCountEntity();
            ReadText(DBConnectionMode.SSP, "select (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(SP.CREATEDBY) = :CREATEDBY) AS ALLJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(SP.CREATEDBY) = :CREATEDBY and (QM.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) OR sp.stateid  = 'Payment in Pending') AND ((sp.stateid  = 'Job Initiated') OR (sp.stateid  = 'Payment in Progress') OR (sp.stateid  = 'Payment in Pending'))) AS ACTIVEJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(SP.CREATEDBY) = :CREATEDBY and (QM.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) AND sp.stateid  <> 'Payment in Pending' OR (sp.stateid  = 'Payment Completed'))) AS EXPIREDJOBS from dual",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<JobBookingStatusCountEntity>(r);
            });

            return mEntity;
        }

        #endregion

        public DataTable GetStateConfigurationFlag(string CountryId)
        {
            string Query = "";
            Query = @" select ISSTATEAPPLICABLE,COUNTRYCODE from UNLOCATIONS_Mapping 
                    where COUNTRYID=:COUNTRYID";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "COUNTRYID", Value = CountryId} 
            });
            return dt;
        }

        public DataTable Getdocumentnames(string jobnumber, string quotationid, string userid)
        {
            int jobno = Convert.ToInt32(jobnumber);
            int quono = Convert.ToInt32(quotationid);
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PJOBNUMBER", jobno);
                        mCommand.Parameters.Add("PQUOTATIONID", quono);
                        mCommand.Parameters.Add("PCREATEDBY", userid);
                        mCommand.Parameters.Add("O_RESULTSET", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_DOUCMENTSLIST";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtJobBooking = new DataTable();
                        OracleDataReader JobBookingReader = ((OracleRefCursor)mCommand.Parameters["O_RESULTSET"].Value).GetDataReader();
                        dtJobBooking.Load(JobBookingReader);
                        JobBookingReader.Close();
                        mConnection.Close();

                        return dtJobBooking;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }
        public IList<GetCountryWeekends> GetCountryWeekends(Int64 originCountryID, Int64 destinationCountryID)
        {
            List<GetCountryWeekends> mEntity = new List<GetCountryWeekends>();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_ORIGINCOUNTRYID", originCountryID);
                        mCommand.Parameters.Add("IN_DESTINATIONCOUNTRYID", destinationCountryID);

                        mCommand.Parameters.Add("CUR_GETCOUNTRYWEEKENDS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_GETCOUNTRYWEEKENDS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtJobBooking = new DataTable();
                        OracleDataReader JobBookingReader = ((OracleRefCursor)mCommand.Parameters["CUR_GETCOUNTRYWEEKENDS"].Value).GetDataReader();
                        dtJobBooking.Load(JobBookingReader);

                        List<DBFactory.Entities.GetCountryWeekends> GETCOUNTRYWEEKENDS = MyClass.ConvertToList<DBFactory.Entities.GetCountryWeekends>(dtJobBooking);

                        JobBookingReader.Close();
                        mConnection.Close();
                        mEntity = GETCOUNTRYWEEKENDS;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataSet GetCountryHolidays(Int64 originCountryID, Int64 destinationCountryID)
        {
            DataSet ds = new DataSet();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_ORIGINCOUNTRYID", originCountryID);
                        mCommand.Parameters.Add("IN_DESTINATIONCOUNTRYID", destinationCountryID);

                        mCommand.Parameters.Add("CUR_GETCOUNTRYHOLIDAYS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_GETCOUNTRYHOLIDAYS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader readerCountry = ((OracleRefCursor)mCommand.Parameters["CUR_GETCOUNTRYHOLIDAYS"].Value).GetDataReader();
                        DataTable dt = new DataTable();

                        dt.Load(readerCountry);
                        ds.Tables.Add(dt);

                        readerCountry.Close(); readerCountry.Dispose();
                        mConnection.Close();

                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return ds;

        }

        public CargoAvabilityEntity GetPickUpTransitDates(long p)
        {
            CargoAvabilityEntity mEntity = new CargoAvabilityEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_COUNTRYID", p);

                        mCommand.Parameters.Add("CUR_PICKUPTRANSITDATES", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_PICKUPTRANSITDATES";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtJobBooking = new DataTable();
                        OracleDataReader JobBookingReader = ((OracleRefCursor)mCommand.Parameters["CUR_PICKUPTRANSITDATES"].Value).GetDataReader();
                        dtJobBooking.Load(JobBookingReader);
                        mEntity = MyClass.ConvertToEntity<CargoAvabilityEntity>(dtJobBooking);

                        JobBookingReader.Close();
                        mConnection.Close();

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        #region PDF related Methods by Sindhu

        public AdvanceReceiptEntity GetAdvanceReceiptDtls(int jobnumber)
        {
            AdvanceReceiptEntity mEntity = new AdvanceReceiptEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_JOBNUMBER", jobnumber);

                        mCommand.Parameters.Add("O_RESULTSET", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_JP_FXRPT_BOOKINGCONFIRM";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtJobBooking = new DataTable();
                        OracleDataReader JobBookingReader = ((OracleRefCursor)mCommand.Parameters["O_RESULTSET"].Value).GetDataReader();
                        dtJobBooking.Load(JobBookingReader);
                        mEntity = MyClass.ConvertToEntity<AdvanceReceiptEntity>(dtJobBooking);

                        JobBookingReader.Close();
                        mConnection.Close();

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetChargeDtls(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT ROUTETYPEID,SUM(TOTALPRICE) TOTALPRICE FROM QMSERVICECHARGE WHERE QUOTATIONID=(SELECT QUOTATIONID FROM SSPJOBDETAILS WHERE JOBNUMBER=:JOBNUMBER ) AND UPPER(CHARGEAPPLICABILITY)='CURRENT' AND ISCHARGEINCLUDED=1 GROUP BY ROUTETYPEID";
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetCargoDtls(int jobnumber)
        {


            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_JOBNUMBER", jobnumber);

                        mCommand.Parameters.Add("O_RESULTSET", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_RPT_BOOKINGCONSHIPMENT";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtJobBooking = new DataTable();
                        OracleDataReader JobBookingReader = ((OracleRefCursor)mCommand.Parameters["O_RESULTSET"].Value).GetDataReader();
                        dtJobBooking.Load(JobBookingReader);
                        JobBookingReader.Close();
                        mConnection.Close();

                        return dtJobBooking;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetQuoteDtls(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT QUOTATIONNUMBER,OPERATIONALJOBNUMBER,CARGOAVAILABLEFROM,ORIGINCUSTOMSCLEARANCEBY,CARGODELIVERYBY,DESTINATIONCUSTOMSCLEARANCEBY,SPECIALINSTRUCTIONS FROM SSPJOBDETAILS where JOBNUMBER=:JOBNUMBER";
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetPartyDtls(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT PARTYTYPE,FULLADDRESS,SALUTATION,FIRSTNAME,LASTNAME,PHONE,EMAILID,CLIENTNAME FROM SSPPARTYDETAILS WHERE JOBNUMBER=:JOBNUMBER";
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetBillingInfo(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT RP.PAYMENTREFNUMBER,RP.PAYMENTTYPE,RH.RECEIPTNETAMOUNT,RH.RECEIPTNUMBER,RH.RECEIPTDATE,RP.PAYMENTOPTION,RH.CURRENCY,RP.PAIDCURRENCY,RP.JOBTOTAL FROM JARECEIPTPAYMENTDETAILS RP,JARECEIPTHEADER RH WHERE RP.JOBNUMBER=RH.JOBNUMBER AND RP.JOBNUMBER=:JOBNUMBER AND RH.JOBNUMBER  = :JOBNUMBER ORDER BY RP.RECEIPTPAYMENTDTLSID ASC";
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetShipmentDtls(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT PRODUCTNAME,MOVEMENTTYPENAME,ORIGINPLACENAME,ORIGINPORTNAME,DESTINATIONPLACENAME,DESTINATIONPORTNAME,PRODUCTID,ISHAZARDOUSCARGO,HAZARDOUSGOODSTYPE FROM QMQUOTATION WHERE QUOTATIONID=(SELECT QUOTATIONID FROM SSPJOBDETAILS WHERE JOBNUMBER= :JOBNUMBER )";
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetVATDtls(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT VATREGNUMBER FROM USERS WHERE LOWER(USERID)=(SELECT LOWER(CREATEDBY) FROM SSPJOBDETAILS WHERE JOBNUMBER= :JOBNUMBER )";
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetUserDtls(int jobnumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT USERS.USERID,USERS.FIRSTNAME,UA.FULLADDRESS FROM USERS INNER JOIN UMUSERADDRESSDETAILS UA ON USERS.USERID=UA.USERID WHERE LOWER(USERS.USERID)=(SELECT LOWER(CREATEDBY) FROM SSPJOBDETAILS WHERE JOBNUMBER= :JOBNUMBER )";
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        #endregion;

        public DataSet CheckExistingJob(int quotationId, string user)
        {
            string Query = "";
            DataSet ds = new DataSet();
            Query = @"SELECT JD.JOBNUMBER, JD.JOBGRANDTOTAL, QM.GRANDTOTAL
            FROM SSPJOBDETAILS JD
            JOIN QMQUOTATION QM
            ON QM.QUOTATIONID = JD.QUOTATIONID
            WHERE LOWER(JD.CREATEDBY)=:CREATEDBY AND JD.QUOTATIONID= :QUOTATIONID AND LOWER(JD.STATEID)='job initiated' AND JD.JOBNUMBER = (SELECT MAX(JOBNUMBER) FROM SSPJOBDETAILS WHERE LOWER(CREATEDBY)=:CREATEDBY AND QUOTATIONID =:QUOTATIONID AND LOWER(STATEID)='job initiated')";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Varchar2 , ParameterName = "CREATEDBY", Value = user.ToLower() },
                new OracleParameter {OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value= quotationId}
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public string getCountryFinanceCashierList(string role, Int64 countryID)
        {
            string recipientsList = "";
            string query = "";
            if (role.ToLower() == "finance")
            {
                query = "SELECT LISTAGG(EMAILID, ',') WITHIN GROUP (ORDER BY EMAILID) AS EMAILID FROM FOCIS.USERPROFILE WHERE UPPER(ROLES) LIKE UPPER('%SSPCountryFinance%') AND COUNTRYID=:COUNTRYID";
            }
            else if (role.ToLower() == "cashier")
            {
                query = "SELECT LISTAGG(EMAILID, ',') WITHIN GROUP (ORDER BY EMAILID) AS EMAILID FROM FOCIS.USERPROFILE WHERE UPPER(ROLES) LIKE UPPER('%SSPCountryCashier%') AND COUNTRYID=:COUNTRYID";
            }

            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "COUNTRYID", Value = countryID } }
                , delegate(IDataReader r)
                {
                    recipientsList = Convert.ToString(r["EMAILID"]);
                });
            return recipientsList;
        }

        public string getMailsOPsFinanceList(string OPJobNumber)
        {
            string recipientsList = "";
            string query = "";
            query = "SELECT LISTAGG(EMAILID, ',') WITHIN GROUP (ORDER BY EMAILID) AS EMAILID FROM FOCIS.USERPROFILE WHERE ((UPPER(ROLES) LIKE ('%SSPCOUNTRYFINANCE%')) OR (UPPER(ROLES) LIKE ('%SSPCOUNTRYJOBOPS%'))) AND (((upper(COUNTRYCODE) like (SELECT OCOUNTRYCODE FROM QMQUOTATION WHERE QUOTATIONID=(SELECT QUOTATIONID FROM SSPJOBDETAILS WHERE OPERATIONALJOBNUMBER=:OPERATIONALJOBNUMBER))) or ((upper(COUNTRYCODE) like (SELECT DCOUNTRYCODE FROM QMQUOTATION WHERE QUOTATIONID=(SELECT QUOTATIONID FROM SSPJOBDETAILS WHERE OPERATIONALJOBNUMBER=:OPERATIONALJOBNUMBER))))))";

            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "OPERATIONALJOBNUMBER", Value = OPJobNumber } 
                }
                , delegate(IDataReader r)
                {
                    recipientsList = Convert.ToString(r["EMAILID"]);
                });
            return recipientsList;
        }
        public string getMailsGSSCList(string userID)
        {
            string recipientsList = "";
            string query = "";
            query = "SELECT LISTAGG(EMAILID, ',') WITHIN GROUP (ORDER BY EMAILID) AS EMAILID FROM FOCIS.USERPROFILE WHERE UPPER(ROLES) LIKE ('%SSPGSSC%')";

            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { }
                , delegate(IDataReader r)
                {
                    recipientsList = Convert.ToString(r["EMAILID"]);
                });
            return recipientsList;
        }

        public string SaveOfflinePaymentBookingSummaryDoc(byte[] paymentpdfdata, int jobNumber, int quotation, string userName, string OPjobnumber, string GUID, string EnvironmentName, string ChargeSetId = "")
        {
            string FileName = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                DataTable AdvReceiptChargeDtls = null;
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        int ChargeCount = 0;
                        if (!string.IsNullOrEmpty(ChargeSetId))
                        {
                            AdvReceiptChargeDtls = GetAdditionalChargeSum(jobNumber, Convert.ToInt32(ChargeSetId));
                            ChargeCount = Convert.ToInt32(AdvReceiptChargeDtls.Rows[0]["TCOUNT"].ToString());
                        }

                        mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        int DOCID = Convert.ToInt32(mCommand.ExecuteScalar());
                        mCommand.CommandText = @"Insert into SSPDOCUMENTS (DOCID,JOBNUMBER,QUOTATIONID,DOCUMNETTYPE,FILENAME,FILEEXTENSION,FILESIZE,FILECONTENT,CREATEDBY,
                        DOCUMENTNAME,PAGEFLAG,SOURCE,DOCDELETE) values (:DOCID,:JOBNUMBER,:QUOTATIONID,:DOCUMNETTYPE,:FILENAME,:FILEEXTENSION,:FILESIZE,:FILECONTENT,:CREATEDBY,
                        :DOCUMENTNAME,:PAGEFLAG,:SOURCE,:DOCDELETE)";
                        mCommand.Parameters.Add("DOCID", DOCID);
                        mCommand.Parameters.Add("CREATEDBY", userName);
                        mCommand.Parameters.Add("JOBNUMBER", jobNumber);
                        mCommand.Parameters.Add("QUOTATIONID", quotation);
                        mCommand.Parameters.Add("DOCUMNETTYPE", "ShipAFreightDoc"); //Booking Request -- Anil G
                        FileName = (ChargeCount > 0) ? "Booking Request_AdditionalCharges_" + OPjobnumber + "_" + ChargeCount + ".pdf" : "Booking Request_" + OPjobnumber + ".pdf";
                        mCommand.Parameters.Add("FILENAME", FileName);
                        mCommand.Parameters.Add("FILEEXTENSION", "application/pdf");
                        mCommand.Parameters.Add("FILESIZE", paymentpdfdata.Length);
                        mCommand.Parameters.Add("FILECONTENT", paymentpdfdata);
                        mCommand.Parameters.Add("DOCUMENTNAME", "Booking Request"); //Booking Request -- Anil G
                        mCommand.Parameters.Add("PAGEFLAG", 1);
                        mCommand.Parameters.Add("SOURCE", "System Generated");
                        mCommand.Parameters.Add("DOCDELETE", "NO");
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mCommand.Parameters.Clear();

                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.CommandText = "USP_SSP_DMS_INTEGRATION";
                        mCommand.Parameters.Add("IP_USERID", userName);
                        mCommand.Parameters.Add("IP_JOBNUMBER", jobNumber);
                        mCommand.Parameters.Add("IP_DOCID", DOCID);
                        mCommand.Parameters.Add("IP_MESSAGEID", EnvironmentName + "-" + GUID);
                        mCommand.Parameters.Add("OP_SPMESSAGE", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output; ;
                        mCommand.Parameters.Add("OP_SPSTATUS", OracleDbType.Long, 2000).Direction = ParameterDirection.Output;
                        mCommand.ExecuteNonQuery();
                        return FileName;
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        //Get Booking Request
        public IList<SSPDocumentsEntity> GetBookingRequestDetails(int JobNumber, int QuotationId, string doctype)
        {
            List<SSPDocumentsEntity> bookingdetails = new List<SSPDocumentsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE Pageflag=1 and Jobnumber = :JOBNUMBER and QuotationId=:QUOTATIONID and FILENAME=:FILENAME",
              new OracleParameter[] { 
                  new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="JOBNUMBER", Value= JobNumber },
                  new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="QUOTATIONID", Value= QuotationId },
                  new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="FILENAME", Value= doctype }
              }
            , delegate(IDataReader r)
            {
                bookingdetails.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
            });
            return bookingdetails;
        }
        //Get Booking Request

        //new JobBooking List
        public List<QuoteJobListEntity> GetSavedJobs(string userId, string status, string list, int PageNo, string SearchString, string Order)
        {
            string mQuery = string.Empty;
            string mSearch = string.Empty;
            status = (status ?? "").ToUpperInvariant().Trim();
            List<QuoteJobListEntity> mEntity = new List<QuoteJobListEntity>();
            if (list.ToUpperInvariant() == "JOBLIST")
            {
                string Query = "";
                int StartQuote = (PageNo - 1) * 10;
                int EndQuote = PageNo * 10;
                if (SearchString != null && SearchString != "")
                {
                    if (SearchString.ToLowerInvariant().Contains("airport"))
                    {
                        SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) =  :SearchString  OR UPPER(QU.PRODUCTNAME) = 'AIR'  AND UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                    }
                    else if (SearchString.ToLowerInvariant().Contains("port"))
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) =  :SearchString  OR UPPER(QU.PRODUCTNAME) = 'OCEAN'  AND UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                    }
                    else
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) =  :SearchString  OR UPPER(QU.PRODUCTNAME) = :SearchString  OR UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                    }
                }
                switch (status)
                {
                    case "INITIATED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Job Initiated')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "INPROGRESS":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Payment in Pending','Payment in Progress')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL) OR job.STATEID IN('Payment in Pending')) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "COMPLETED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Payment Completed')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (job.STATEID = 'Payment Completed') ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "EXPIRED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE (STATEID <>'Payment Completed')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                         " AND (QU.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL) OR job.STATEID ='Expired') ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "CANCELLED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE UPPER(STATEID) IN('CANCELLED','CANCELLATION PENDING')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (UPPER(job.STATEID) IN ('CANCELLATION PENDING','CANCELLED')) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "COLLABORATED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE (LOWER(COLLABRATEDCONSIGNEE)=:CUSTOMERID or LOWER(COLLABRATEDSHIPPER)=:CUSTOMERID)
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    default:
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Job Initiated')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID LEFT JOIN(SELECT ISCHARGEINCLUDED,JOBNUMBER  FROM JOBSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) JC on JC.JOBNUMBER = job.JOBNUMBER WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                }

                Query = @"SELECT * FROM ((SELECT ROWNUM AS rid, ABC.* FROM (SELECT QU.*, job.JOBNUMBER, job.OPERATIONALJOBNUMBER, job.STATEID AS JSTATEID,
                job.DATEMODIFIED AS JDDATEMODIFIED, job.JOBGRANDTOTAL AS JOBGRANDTOTAL, job.ADDITIONALCHARGESTATUS AS ADDITIONALCHARGESTATUS, job.ADDITIONALCHARGEAMOUNT AS ADDITIONALCHARGEAMOUNT,job.CHARGESETID  AS CHARGESETID, (SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID)=QU.PREFERREDCURRENCYID) AS DECIMALCOUNT,JC.ISCHARGEINCLUDED FROM QMQUOTATION QU " + mQuery + " WHERE rid > :StartQuote  AND rid <= :EndQuote";

                DataTable dt;

                if (SearchString != null && SearchString != "")
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote },
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString } 
                    });
                }
                else
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote } 
                    });
                }

                mEntity = MyClass.ConvertToList<QuoteJobListEntity>(dt);

                List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
                ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN(SELECT QUOTATIONID FROM QMQUOTATION WHERE CREATEDBY = :CUSTOMERID) ",
                    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
                , delegate(IDataReader r)
                {
                    mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                });
                foreach (var item in mEntity)
                {
                    item.ShipmentItems = mShipmentItems.FindAll(delegate(QuotationShipmentEntity qsItems)
                    {
                        return qsItems.QUOTATIONID == item.QUOTATIONID;
                    });
                }
            }

            List<SSPJobDetailsEntity> mJobItems = new List<SSPJobDetailsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPJOBDETAILS WHERE QUOTATIONID IN(SELECT QUOTATIONID FROM QMQUOTATION WHERE CREATEDBY = :CUSTOMERID) ",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mJobItems.Add(DataReaderMapToObject<SSPJobDetailsEntity>(r));
            });
            foreach (var item in mEntity)
            {
                item.JobItems = mJobItems.FindAll(delegate(SSPJobDetailsEntity qsItems)
                {
                    return qsItems.QUOTATIONID == item.QUOTATIONID;
                });
            }

            List<ReceiptHeaderEntity> header = new List<ReceiptHeaderEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT RECEIPT.JOBNUMBER,RECEIPT.RECEIPTAMOUNT,RECEIPT.RECEIPTTAX,RECEIPT.RECEIPTNETAMOUNT,RECEIPT.DISCOUNTAMOUNT FROM  JARECEIPTHEADER RECEIPT WHERE CREATEDBY = :CUSTOMERID",
               new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
           , delegate(IDataReader r)
           {
               header.Add(DataReaderMapToObject<ReceiptHeaderEntity>(r));
           });
            foreach (var item in mEntity)
            {
                item.ReceiptHeaderDetails = header.FindAll(delegate(ReceiptHeaderEntity qsItems)
                {
                    return qsItems.JOBNUMBER == item.JOBNUMBER;
                });
            }

            return mEntity;
        }

        public List<QuoteJobListEntity> GetSavedJobs_MB(string userId, string status, string list, int PageNo, string SearchString, string Order)
        {
            string mQuery = string.Empty;
            string mSearch = string.Empty;
            status = (status ?? "").ToUpperInvariant().Trim();
            List<QuoteJobListEntity> mEntity = new List<QuoteJobListEntity>();
            if (list.ToUpperInvariant() == "JOBLIST")
            {
                string Query = "";
                string SelectAll = "SELECT * FROM ";
                string SelectQuotationid = "SELECT QUOTATIONID FROM";
                string SelectJobNumber = "SELECT JOBNUMBER FROM";

                int StartQuote = (PageNo - 1) * 10;
                int EndQuote = PageNo * 10;
                if (SearchString != null && SearchString != "")
                {
                    if (SearchString.ToLowerInvariant().Contains("airport"))
                    {
                        SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) =  :SearchString  OR UPPER(QU.PRODUCTNAME) = 'AIR'  AND UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                    }
                    else if (SearchString.ToLowerInvariant().Contains("port"))
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) =  :SearchString  OR UPPER(QU.PRODUCTNAME) = 'OCEAN'  AND UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                    }
                    else
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) =  :SearchString  OR UPPER(QU.PRODUCTNAME) = :SearchString  OR UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                    }
                }
                switch (status)
                {
                    case "INITIATED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Job Initiated')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "INPROGRESS":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Payment in Pending','Payment in Progress')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL) OR job.STATEID IN('Payment in Pending')) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "COMPLETED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Payment Completed')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (job.STATEID = 'Payment Completed') ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "EXPIRED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE (STATEID <>'Payment Completed')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                         " AND (QU.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL) OR job.STATEID ='Expired') ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "CANCELLED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE UPPER(STATEID) IN('CANCELLED','CANCELLATION PENDING')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (UPPER(job.STATEID) IN ('CANCELLATION PENDING','CANCELLED')) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "COLLABORATED":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE (LOWER(COLLABRATEDCONSIGNEE)=:CUSTOMERID or LOWER(COLLABRATEDSHIPPER)=:CUSTOMERID)
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    default:
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Job Initiated')
                                            ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE (QU.CREATEDBY = :CUSTOMERID OR QU.GUESTEMAIL = :CUSTOMERID) " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                }

                Query = @"((SELECT ROWNUM AS rid, ABC.* FROM (SELECT QU.*, job.JOBNUMBER, job.OPERATIONALJOBNUMBER, job.STATEID AS JSTATEID,
                job.DATEMODIFIED AS JDDATEMODIFIED, job.JOBGRANDTOTAL AS JOBGRANDTOTAL, job.ADDITIONALCHARGESTATUS AS ADDITIONALCHARGESTATUS, job.ADDITIONALCHARGEAMOUNT AS ADDITIONALCHARGEAMOUNT,job.CHARGESETID  AS CHARGESETID, (SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID)=QU.PREFERREDCURRENCYID) AS DECIMALCOUNT FROM QMQUOTATION QU " + mQuery + " WHERE rid > :StartQuote  AND rid <= :EndQuote";

                DataTable dt;

                if (SearchString != null && SearchString != "")
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, SelectAll + Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote },
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString } 
                    });
                }
                else
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, SelectAll + Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote } 
                    });
                }

                mEntity = MyClass.ConvertToList<QuoteJobListEntity>(dt);


                List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
                string shipitemquery = "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN(" + SelectQuotationid + Query + ")";
                if (SearchString != null && SearchString != "")
                {
                    ReadText(DBConnectionMode.SSP, shipitemquery,
                    new OracleParameter[] 
                    { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() },
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote },
                        new OracleParameter { ParameterName = "SearchString", Value = SearchString } 
                    },
                    delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }
                else
                {
                    ReadText(DBConnectionMode.SSP, shipitemquery,
                    new OracleParameter[] 
                    { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() },
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote }
                    }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }

                foreach (var item in mEntity)
                {
                    item.ShipmentItems = mShipmentItems.FindAll(o => o.QUOTATIONID == item.QUOTATIONID);
                }
                
                List<ReceiptHeaderEntity> header = new List<ReceiptHeaderEntity>();
                string jareceiptquery = "SELECT RECEIPT.JOBNUMBER,RECEIPT.RECEIPTAMOUNT,RECEIPT.RECEIPTTAX,RECEIPT.RECEIPTNETAMOUNT,RECEIPT.DISCOUNTAMOUNT FROM  JARECEIPTHEADER RECEIPT WHERE CREATEDBY = :CUSTOMERID AND RECEIPT.JOBNUMBER IN(" + SelectJobNumber + Query + ")";
                if (SearchString != null && SearchString != "")
                {
                    ReadText(DBConnectionMode.SSP, jareceiptquery,
                       new OracleParameter[] 
                   { 
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() },
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote },
                       new OracleParameter { ParameterName = "SearchString", Value = SearchString } 
                   }
                   , delegate(IDataReader r)
                   {
                       header.Add(DataReaderMapToObject<ReceiptHeaderEntity>(r));
                   });

                }
                else
                {
                    ReadText(DBConnectionMode.SSP, jareceiptquery,
                       new OracleParameter[] 
                   { 
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() },
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "StartQuote", Value = StartQuote } ,
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "EndQuote", Value = EndQuote }
                   }
                   , delegate(IDataReader r)
                   {
                       header.Add(DataReaderMapToObject<ReceiptHeaderEntity>(r));
                   });
                }
                foreach (var item in mEntity)
                {
                    item.ReceiptHeaderDetails = header.FindAll(o => o.JOBNUMBER == item.JOBNUMBER);
                }
            }
            return mEntity;
        }

        public QuoteStatusCountEntity GetJobStatusCount(string userId, string SearchString)
        {
            string mSearch = string.Empty;
            string query = string.Empty;

            if (SearchString != null && SearchString != "")
            {
                if (SearchString.ToLowerInvariant().Contains("airport"))
                {
                    SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(QM.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%'  OR UPPER(QM.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.QUOTATIONNUMBER) = :SearchString  OR UPPER(QM.PRODUCTNAME) = 'AIR'  AND UPPER(QM.MOVEMENTTYPENAME) = :SearchString  OR UPPER(SP.OPERATIONALJOBNUMBER) = :SearchString )";
                }
                else if (SearchString.ToLowerInvariant().Contains("port"))
                {
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(QM.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%'  OR UPPER(QM.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.QUOTATIONNUMBER) = :SearchString  OR UPPER(QM.PRODUCTNAME) = 'OCEAN'  AND UPPER(QM.MOVEMENTTYPENAME) = :SearchString  OR UPPER(SP.OPERATIONALJOBNUMBER) = :SearchString )";
                }
                else
                {
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(QM.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%'  OR UPPER(QM.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.QUOTATIONNUMBER) = :SearchString  OR UPPER(QM.PRODUCTNAME) = :SearchString  OR UPPER(QM.MOVEMENTTYPENAME) = :SearchString  OR UPPER(SP.OPERATIONALJOBNUMBER) = :SearchString )";
                }
            }
            //string filter = userId.ToLower() + mSearch;

            query = "SELECT (SELECT COUNT(1) FROM SSPJOBDETAILS SP INNER JOIN QMQUOTATION QM ON SP.QUOTATIONID = QM.QUOTATIONID WHERE (LOWER(QM.CREATEDBY) = :CREATEDBY OR LOWER(QM.GUESTEMAIL) = :CREATEDBY)" + mSearch + "";
            query += " AND (QM.DATEOFVALIDITY   >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) AND sp.stateid = 'Job Initiated') AS INITIATEDJOBS, (SELECT COUNT(1) FROM SSPJOBDETAILS SP INNER JOIN QMQUOTATION QM ON SP.QUOTATIONID = QM.QUOTATIONID  WHERE (LOWER(QM.CREATEDBY) = :CREATEDBY OR LOWER(QM.GUESTEMAIL) = :CREATEDBY)" + mSearch + " AND (((QM.DATEOFVALIDITY >=";
            query += " (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL )) AND (sp.stateid = 'Payment in Progress')) OR (sp.stateid   = 'Payment in Pending'))) AS INPROGRESSJOBS,";

            query += " (SELECT COUNT(1) FROM SSPJOBDETAILS SP INNER JOIN QMQUOTATION QM ON SP.QUOTATIONID = QM.QUOTATIONID WHERE (LOWER(QM.CREATEDBY) = :CREATEDBY OR LOWER(QM.GUESTEMAIL) = :CREATEDBY)" + mSearch + "";
            query += " AND (sp.stateid = 'Payment Completed')) AS COMPLETEDJOBS,";

            query += " (SELECT COUNT(1) FROM SSPJOBDETAILS SP INNER JOIN QMQUOTATION QM ON SP.QUOTATIONID = QM.QUOTATIONID WHERE (LOWER(QM.CREATEDBY) = :CREATEDBY OR LOWER(QM.GUESTEMAIL) = :CREATEDBY)" + mSearch + "";
            query += " AND (UPPER(sp.stateid) IN ('CANCELLATION PENDING','CANCELLED'))) AS CANCELLEDJOBS,";

            query += " (select count(1) from SSPJOBDETAILS SP INNER JOIN QMQUOTATION QM ON SP.QUOTATIONID = QM.QUOTATIONID where (LOWER(SP.COLLABRATEDCONSIGNEE)=:CREATEDBY or LOWER(SP.COLLABRATEDSHIPPER)=:CREATEDBY)" + mSearch + "";
            query += " AND (QM.DATEOFVALIDITY   >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL))) AS COLLABORATEDJOBS,";

            query += " (SELECT COUNT(1) FROM SSPJOBDETAILS SP INNER JOIN QMQUOTATION QM ON SP.QUOTATIONID = QM.QUOTATIONID WHERE (LOWER(QM.CREATEDBY) = :CREATEDBY OR LOWER(QM.GUESTEMAIL) = :CREATEDBY)" + mSearch + "";
            query += " AND (sp.stateid = 'Expired' or ((QM.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL)) AND(sp.stateid <> 'Payment Completed')))) AS EXPIREDJOBS FROM dual";
            QuoteStatusCountEntity mEntity = new QuoteStatusCountEntity();
            //ReadText(DBConnectionMode.SSP, "select (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + ") AS ALLJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + " and (QM.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) OR sp.stateid  = 'Payment in Pending') AND ((sp.stateid  = 'Job Initiated') OR (sp.stateid  = 'Payment in Progress') OR (sp.stateid  = 'Payment in Pending'))) AS ACTIVEJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + " and (QM.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) AND sp.stateid  <> 'Payment in Pending' OR (sp.stateid  = 'Payment Completed'))) AS EXPIREDJOBS from dual",

            if (SearchString != null && SearchString != "")
            {
                ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString } 
                }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
            });

            }
            else
            {
                ReadText(DBConnectionMode.SSP, query,
               new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
           , delegate(IDataReader r)
           {
               mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
           });

            }
            return mEntity;
        }


        public string GetGlobalMngmntMailIds()
        {
            string tomailids = "";

            ReadText(DBConnectionMode.FOCiS, "SELECT WM_CONCAT(USERID) USERIDS FROM SSPUSERSCONFIG WHERE ROLEID='GLOBALMANAGEMENT'",
                new OracleParameter[] { new OracleParameter { } }
            , delegate(IDataReader r)
            {
                if (Convert.ToString(r["USERIDS"]) != null)
                {
                    tomailids = Convert.ToString(r["USERIDS"]);
                }
            });

            return tomailids;
        }

        public IList<MBDocs> GetDocumentDetailsForMobile(int p, Int64 JobNumber)
        {
            //Shipment Details List
            List<MBDocs> mShipmentItems = new List<MBDocs>();
            ReadText(DBConnectionMode.SSP, @"SELECT DOCID,FILENAME,DOCUMNETTYPE,FILEEXTENSION,DocumentName,CONDMAND,DIRECTION,CONDMAND,SOURCE,
                                             DOCDELETE,PROVIDEDDBY,DOCUPLOADDATE,PROVIDERMAILID,PROVIDER,DOCUMENTTEMPLATE,DATECREATED FROM SSPDOCUMENTS
                                             WHERE PAGEFLAG=:PAGEFLAG and  JOBNUMBER=:JOBNUMBER
                                             UNION 
                                             SELECT DOCID,FILENAME,DOCUMNETTYPE,FILEEXTENSION,DocumentName,CONDMAND,DIRECTION,CONDMAND,SOURCE,
                                             DOCDELETE,PROVIDEDDBY,DOCUPLOADDATE,PROVIDERMAILID,PROVIDER,DOCUMENTTEMPLATE,DATECREATED FROM SSPDOCUMENTS 
                                             WHERE QUOTATIONID=(select QUOTATIONID from SSPJOBDETAILS where JOBNUMBER=:JOBNUMBER) AND SOURCE='SHIPAQUOTE'
                                            ",
                new OracleParameter[] {new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JobNumber},
                                        new OracleParameter {OracleDbType = OracleDbType.Int32, ParameterName = "PAGEFLAG", Value = 1}
                }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<MBDocs>(r));
            });
            return mShipmentItems;
        }

        public IList<MBDocs> DownloadDocByID(Int64 DOCID, string Userid)
        {
            //Shipment Details List
            List<MBDocs> mShipmentItems = new List<MBDocs>();
            ReadText(DBConnectionMode.SSP, "SELECT DOCID,FILECONTENT,FILEEXTENSION FROM SSPDOCUMENTS WHERE DOCID = :DOCID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "DOCID", Value = DOCID } }
            , delegate(IDataReader r)
            {
                mShipmentItems.Add(DataReaderMapToObject<MBDocs>(r));
            });
            return mShipmentItems;
        }

        public string InviteParty(int SelectedClient, string Doc, string Token, DateTime TokenExpiry, string invitedUserId, string password, int JobNumber, int Countryid, string SelectEmailID, string Party)
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_SSPCLIENTID", SelectedClient);
                        mCommand.Parameters.Add("IN_DOCDATES", Doc);
                        mCommand.Parameters.Add("IN_TOKEN", Token);
                        mCommand.Parameters.Add("IN_TOKENEXP", TokenExpiry);
                        mCommand.Parameters.Add("IN_EMAILID", invitedUserId);
                        mCommand.Parameters.Add("IN_PASSWORD", password);
                        mCommand.Parameters.Add("IN_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("IN_COUNTRYID", Countryid);
                        mCommand.Parameters.Add("IN_PARTY", Party.ToUpper());
                        mCommand.Parameters.Add("IN_SELECTEDEMAILID", SelectEmailID);

                        mCommand.Parameters.Add("CUR_USERSTATUS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_SSP_INSERTINVITEDPARTY";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["CUR_USERSTATUS"].Value).GetDataReader();
                        while (UserStatus.Read())
                        {
                            result = UserStatus[0].ToString();
                        }
                        mConnection.Close();

                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }


        public void SubmitToCustomer(long jobnumber, string party)
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("IN_JOBNUMBER", jobnumber);
                        mCommand.Parameters.Add("IN_PARTY", party);
                        mCommand.CommandText = "USP_SSP_UPDATEFINALPARTY";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public string GetDiscountedValue(string CouponCode, string JobNumber, string userid)
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_COUPONCODE", CouponCode);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("PRM_USERID", userid);

                        mCommand.Parameters.Add("CUR_RTNMSG", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_APPLY_DISCOUNT";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["CUR_RTNMSG"].Value).GetDataReader();
                        while (UserStatus.Read())
                        {
                            for (int i = 0; i < UserStatus.FieldCount; i++)
                            {
                                result = result + "," + UserStatus[i].ToString();

                            }
                        }
                        mConnection.Close();
                        result = result.Substring(1);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public string GETDiscounts(long JOBNUMBER)
        {
            string discount = string.Empty;

            discount = this.ExecuteScalar<string>(DBConnectionMode.SSP, "select nvl(DISCOUNTAMOUNT,0) as DISCOUNTAMOUNT from JARECEIPTHEADER where JOBNUMBER= :JOBNUMBER",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "JOBNUMBER",Value = JOBNUMBER},
                });
            return discount;
        }

        public JobBookingEntity InsertCountryBasedDocsFIELDS(long OriginPlaceID, long DestinationPlaceID, string Jobnumber, string QuotationID,
            string CreatedBy, JobBookingEntity mEntity)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();

            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        //------------------Documents Country Instructins---------------------
                        mCommand.Parameters.Clear();

                        mCommand.CommandText = "USP_INSERT_CUN_BASED_DOCS";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_ORG_COUNTRYID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = OriginPlaceID;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_DEST_COUNTRYID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = DestinationPlaceID;
                        mCommand.Parameters.Add(op);


                        op = new OracleParameter("IP_JOBNUMBER", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Convert.ToInt64(Jobnumber);
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_QuotationID", OracleDbType.Long);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Convert.ToInt64(QuotationID);
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_CreatedBy", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Convert.ToString(CreatedBy);
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_DOCS", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);


                        op = new OracleParameter("OP_FIELDS", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        mCommand.ExecuteNonQuery();

                        OracleDataReader OP_DOCS = ((OracleRefCursor)mCommand.Parameters["OP_DOCS"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(OP_DOCS);
                        ds.Tables.Add(dt);
                        dt.TableName = "Docs";
                        dt = new DataTable();
                        OracleDataReader OP_FIELDS = ((OracleRefCursor)mCommand.Parameters["OP_FIELDS"].Value).GetDataReader();
                        dt.Load(OP_FIELDS);
                        ds.Tables.Add(dt);
                        dt.TableName = "FIELDS";
                        mConnection.Close(); OP_DOCS.Dispose(); OP_DOCS.Close(); OP_FIELDS.Dispose(); OP_FIELDS.Close();
                        List<DBFactory.Entities.SSPDocumentsEntity> mdocs = MyClass.ConvertToList<DBFactory.Entities.SSPDocumentsEntity>(ds.Tables[0]);

                        List<DBFactory.Entities.Compliance_Fields> MFields = MyClass.ConvertToList<DBFactory.Entities.Compliance_Fields>(ds.Tables[1]);

                        mEntity.DocumentDetails = mdocs;
                        mEntity.CopmlnceFlds = MFields;
                        //----------------------------------------------------------
                        return mEntity;

                    }
                    catch (Exception ex)
                    {
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }

        }

        public DataTable GetCountryBasedDocuments(string Jobnumber, string QuotationID, string p)
        {

            string Query = "Select * from SSPDOCUMENTS where Jobnumber=:JOBNUMBER";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="JOBNUMBER", Value=Jobnumber} 
            });
            return dt;
        }

        public DataSet GetSavedFieldValues(string Jobnumber)
        {
            DataSet ds = new DataSet();

            string Query = @"select * from SSP_FIELD_VALUES where JOBNUMBER=:JOBNUMBER";// + " AND HSCODETYPE='O'";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = Jobnumber } });
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet GetComplianceLevelsData(string MASTERTYPE, int MASTERID)
        {
            OracleParameter op = null;
            DataSet ds = new DataSet();

            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.CommandText = "USP_SSP_GETHSMASTERDTLS";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_MASTERTYPE", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = MASTERTYPE;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_MASTERID", OracleDbType.Int64);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Convert.ToInt64(MASTERID);
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_CODE", OracleDbType.Int64);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_MSG", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("USP_SSP_GET_HSC_INSTRC", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        mCommand.ExecuteNonQuery();

                        int Successcode = Convert.ToInt32(((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["OP_SUCCESS_CODE"].Value)).Value);
                        //string Success_Msg = Convert.ToString(((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["OP_SUCCESS_MSG"].Value)).Value);

                        if (Successcode == 1)
                        {
                            OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["USP_SSP_GET_HSC_INSTRC"].Value).GetDataReader();
                            DataTable dt = new DataTable();
                            dt.Load(reader1);
                            ds.Tables.Add(dt);

                            reader1.Close(); reader1.Dispose();
                            mConnection.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ShipaDbException(ex.ToString());
                    }

                }
            }
            return ds;
        }

        public SSPJobDetailsEntity CreateNewJobBasedonQID(int QUOTATIONID, string CREATEDBY)
        {
            OracleParameter op = null;
            DBFactory.Entities.SSPJobDetailsEntity mjOB = new SSPJobDetailsEntity();
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.CommandText = "SSP_CREATEJOB_BASEDONQID";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("V_QUOTATIONID", OracleDbType.Int64);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Convert.ToInt64(QUOTATIONID);
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("V_CREATEDBY", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = Convert.ToString(CREATEDBY);
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OUT_JOBDATA", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        OracleDataReader JobData = ((OracleRefCursor)mCommand.Parameters["OUT_JOBDATA"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(JobData);
                        mjOB = MyClass.ConvertToEntity<DBFactory.Entities.SSPJobDetailsEntity>(dt);
                        mConnection.Close(); JobData.Close(); JobData.Dispose();

                    }
                    catch (Exception ex)
                    {
                        throw new ShipaDbException(ex.ToString());
                    }

                }
            }
            return mjOB;
        }

        public void UpdateSaveforlaterfordocument(string DOCID, string DocupDate, string ProvidedBy, string PROVIDERMAILID)
        {
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        if (PROVIDERMAILID != "Upload later all")
                        {
                            mCommand.CommandText = @"UPDATE SSPDOCUMENTS SET PROVIDER=:Provider,DOCUPLOADDATE=:DocupDate,PROVIDERMAILID=:PROVIDERMAILID
                                                     WHERE  DOCID=:DOCID";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("Provider", ProvidedBy);
                            mCommand.Parameters.Add("DocupDate", DocupDate);
                            mCommand.Parameters.Add("PROVIDERMAILID", PROVIDERMAILID);
                            mCommand.Parameters.Add("DOCID", DOCID);
                            mCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            mCommand.CommandText = @"UPDATE SSPDOCUMENTS SET PROVIDER=:Provider,DOCUPLOADDATE=:DocupDate,PROVIDERMAILID=:PROVIDERMAILID 
                                                    WHERE  JOBNUMBER=:JOBNUMBER and CONDMAND=:CONDMAND and Provider is null and FILENAME is null";
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("Provider", ProvidedBy);
                            mCommand.Parameters.Add("DocupDate", DocupDate);
                            mCommand.Parameters.Add("PROVIDERMAILID", "");
                            mCommand.Parameters.Add("JOBNUMBER", DOCID);
                            mCommand.Parameters.Add("CONDMAND", "Mandatory");
                            mCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }
        }

        public void UpdateCollabratedocument(string DOCID, string DocupDate, string ProvidedBy, string Provideremailid)
        {

            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        if (DOCID != "")
                        {
                            var listDocId = DOCID.Split(',').ToList();
                            if (listDocId.Count == 1)
                            {
                                mCommand.CommandText = @"UPDATE SSPDOCUMENTS SET PROVIDER=:Provider,DOCUPLOADDATE=:DocupDate,PROVIDERMAILID=:PROVIDERMAILID
                                                     WHERE  DOCID IN (:DOCID)";
                                mCommand.Parameters.Clear();
                                mCommand.Parameters.Add("Provider", ProvidedBy);
                                mCommand.Parameters.Add("DocupDate", DocupDate);
                                mCommand.Parameters.Add("PROVIDERMAILID", Provideremailid);
                                mCommand.Parameters.Add("DOCID", DOCID);
                                mCommand.ExecuteNonQuery();
                            }
                            else
                            {
                                foreach (var item in listDocId)
                                {
                                    mCommand.CommandText = @"UPDATE SSPDOCUMENTS SET PROVIDER=:Provider,DOCUPLOADDATE=:DocupDate,PROVIDERMAILID=:PROVIDERMAILID
                                                     WHERE  DOCID IN (:DOCID)";
                                    mCommand.Parameters.Clear();
                                    mCommand.Parameters.Add("Provider", ProvidedBy);
                                    mCommand.Parameters.Add("DocupDate", DocupDate);
                                    mCommand.Parameters.Add("PROVIDERMAILID", Provideremailid);
                                    mCommand.Parameters.Add("DOCID", item);
                                    mCommand.ExecuteNonQuery();
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }
        }

        public DataTable GetPartyNames(string Countryid, string CREATEDBY)
        {
            string Query = @"select SSPCLIENTID,EMAILID from SSPPARTYMASTER where COUNTRYID=:COUNTRYID AND UPPER(CREATEDBY)=:CREATEDBY";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] 
            {
             new OracleParameter { ParameterName = "COUNTRYID", Value = Countryid },
             new OracleParameter{ ParameterName = "CREATEDBY", Value = CREATEDBY.ToUpper() }
            });
            return dt;
        }

        public void ClearUploadLater(string DOCID)
        {
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    try
                    {
                        mCommand.CommandText = "UPDATE SSPDOCUMENTS SET PROVIDEDDBY=:ProvidedBy,Provider=:ProvidedBy,DOCUPLOADDATE=:DocupDate,PROVIDERMAILID=:PROVIDERMAILID WHERE  DOCID=:DOCID";
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("ProvidedBy", "");
                        mCommand.Parameters.Add("Provider", "");
                        mCommand.Parameters.Add("DocupDate", null);
                        mCommand.Parameters.Add("PROVIDERMAILID", "");
                        mCommand.Parameters.Add("DOCID", DOCID);
                        mCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }
        }

        public DataSet CheckPaymentStatus(int jobnumber)
        {
            DataSet ds = new DataSet();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_JOBNUMBER", jobnumber);

                        mCommand.Parameters.Add("CUR_GETPAYMENTSTATUS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_CHECKPAYMENTSTATUS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader readerCountry = ((OracleRefCursor)mCommand.Parameters["CUR_GETPAYMENTSTATUS"].Value).GetDataReader();
                        DataTable dt = new DataTable();

                        dt.Load(readerCountry);
                        ds.Tables.Add(dt);

                        readerCountry.Close(); readerCountry.Dispose();
                        mConnection.Close();

                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return ds;
        }

        public DataTable GETNEXTFLYINGDATE(string QuotationId, string PickupDate)
        {
            DataTable dt = new DataTable();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("V_QUOTATIONID", QuotationId);
                        mCommand.Parameters.Add("V_PICKUPDATE", PickupDate);
                        mCommand.Parameters.Add("O_SUCCESS", OracleDbType.Int32).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("NEXT_FLYDATE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "GETNEXTFLYINGDATE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        if (((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["O_SUCCESS"].Value)).Value > 0)
                        {
                            OracleDataReader NEXT_FLYDATE = ((OracleRefCursor)mCommand.Parameters["NEXT_FLYDATE"].Value).GetDataReader();
                            dt.Load(NEXT_FLYDATE);
                            NEXT_FLYDATE.Close(); NEXT_FLYDATE.Dispose();
                        };

                        mConnection.Close();

                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return dt;
        }

        //Get Quoted Job Details
        public JobBookingEntity GetAdditionalChargeDetails(int id, long JobNumber, string sourceType, int chargeSetid)
        {
            JobBookingEntity mEntity = new JobBookingEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("L_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("L_SOURCETYPE", sourceType);
                        mCommand.Parameters.Add("L_CHARGESETID", chargeSetid);

                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPJOBDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_SSPPARTYDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_ADDITIONALCHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_ADDITIONALCHARGE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<JobBookingEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobShipmentItem = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobShipmentItem);
                        List<DBFactory.Entities.JobBookingShipmentEntity> mSspJobShipmentItem = MyClass.ConvertToList<DBFactory.Entities.JobBookingShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPJobDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPJOBDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPJobDetails);
                        List<DBFactory.Entities.SSPJobDetailsEntity> mSSPJobDetails = MyClass.ConvertToList<DBFactory.Entities.SSPJobDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtSSPPartyDetails = ((OracleRefCursor)mCommand.Parameters["CUR_SSPPARTYDETAILS"].Value).GetDataReader();
                        dtQuotation.Load(dtSSPPartyDetails);
                        List<DBFactory.Entities.SSPPartyDetailsEntity> mSSPPartyDetails = MyClass.ConvertToList<DBFactory.Entities.SSPPartyDetailsEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_ADDITIONALCHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.PaymentChargesEntity> mJobServiceCharges = MyClass.ConvertToList<DBFactory.Entities.PaymentChargesEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtJareceiptHeader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtQuotation.Load(dtJareceiptHeader);
                        List<DBFactory.Entities.ReceiptHeaderEntity> mJareceiptHeader = MyClass.ConvertToList<DBFactory.Entities.ReceiptHeaderEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtSSPJobShipmentItem.Close();
                        dtSSPJobDetails.Close();
                        dtSSPPartyDetails.Close();
                        dtServiceCharge.Close();
                        dtJareceiptHeader.Close();
                        mConnection.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.JobShipmentItems = mSspJobShipmentItem;
                        mEntity.JobItems = mSSPJobDetails;
                        mEntity.PartyItems = mSSPPartyDetails;
                        mEntity.PaymentCharges = mJobServiceCharges;
                        mEntity.ReceiptHeaderDetails = mJareceiptHeader;

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        //public string AdditionalChargeAutoApprove(int JobNumber)
        //{
        //    string result = string.Empty;
        //    OracleConnection mConnection;
        //    OracleTransaction mOracleTransaction = null;
        //    using (mConnection = new OracleConnection(SSPConnectionString))
        //    {
        //        if (mConnection.State == ConnectionState.Closed) mConnection.Open();
        //        mOracleTransaction = mConnection.BeginTransaction();
        //        using (OracleCommand mCommand = mConnection.CreateCommand())
        //        {
        //            mCommand.BindByName = true;
        //            mCommand.Transaction = mOracleTransaction;
        //            mCommand.CommandType = CommandType.StoredProcedure;
        //            try
        //            {
        //                mCommand.Parameters.Clear();                     
        //                mCommand.Parameters.Add("IN_JOBNUMBER", JobNumber);
        //                mCommand.Parameters.Add("IN_CHARGESTATUS", "Accepted by customer");
        //                mCommand.Parameters.Add("CUR_ADDCHARGEAUTOAPPROVE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
        //                mCommand.CommandText = "USP_SSP_ADDCHARGEAUTOAPPROVE";
        //                mCommand.CommandType = CommandType.StoredProcedure;
        //                mCommand.ExecuteNonQuery();

        //                OracleDataReader status = ((OracleRefCursor)mCommand.Parameters["CUR_ADDCHARGEAUTOAPPROVE"].Value).GetDataReader();
        //                while (status.Read())
        //                {
        //                    result = status[0].ToString();
        //                }
        //                mConnection.Close();

        //                return result;
        //            }
        //            catch (Exception ex)
        //            {
        //                mOracleTransaction.Rollback();
        //                throw new Exception(ex.ToString());
        //            }
        //        }
        //    }
        //}

        public DataTable GetAdditionalCharges(int Jobnumber, int QuotationID, string sourceType)
        {
            string Query = "SELECT OPERATIONALJOBNUMBER,CREATEDBY,ADDITIONALCHARGESTATUS,ADDITIONALCHARGEAMOUNT,(SELECT C.PREFERREDCURRENCYID FROM QMQUOTATION C WHERE C.QUOTATIONID=S.QUOTATIONID) CURRENCY FROM SSPJOBDETAILS S WHERE JOBNUMBER=:JOBNUMBER";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] {
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="JOBNUMBER", Value= Jobnumber } });
            return dt;
        }

        public string AdditionalChargeAutoApprove(PaymentTransactionDetailsEntity entity, string userid)
        {
            string serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_RECEIPTHDRID", entity.RECEIPTHDRID);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", entity.JOBNUMBER);
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("PRM_PAYMENTOPTION", entity.PAYMENTOPTION);
                        mCommand.Parameters.Add("PRM_ISPAYMENTSUCESS", entity.ISPAYMENTSUCESS);
                        mCommand.Parameters.Add("PRM_PAYMENTREFNUMBER", entity.PAYMENTREFNUMBER);
                        mCommand.Parameters.Add("PRM_PAYMENTREFMESSAGE", "approved_Additional");
                        mCommand.Parameters.Add("PRM_CREATEDBY", entity.CREATEDBY);
                        mCommand.Parameters.Add("PRM_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("PRM_PAYMENTTYPE", entity.PAYMENTTYPE);
                        mCommand.Parameters.Add("PRM_TRANSACTIONMODE", entity.TRANSMODE);
                        mCommand.Parameters.Add("PRM_RECEIPTAMOUNT", entity.TRANSAMOUNT);
                        mCommand.Parameters.Add("PRM_CURRENCY", entity.TRANSCURRENCY);
                        mCommand.Parameters.Add("PRM_SERVERNAME  ", serverName);
                        mCommand.Parameters.Add("PRM_SOURCETYPE", entity.ADDITIONALCHARGESTATUS);
                        mCommand.Parameters.Add("PRM_ADDITIONALCHARGEAMOUNT", entity.ADDITIONALCHARGEAMOUNT);
                        mCommand.Parameters.Add("PRM_ADDCHARGEAMOUNTINUSD", entity.ADDITIONALCHARGEAMOUNTINUSD);

                        mCommand.CommandText = "USP_SSP_ADDCHARGEAUTOAPPROVE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        mConnection.Close();
                        return "success";
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public string NewNotification(int nTypeId, string Status, string BookingId, int JobNumber, int QuotationId, int chargeSetid, string UserId)
        {
            UserEntity entity = new UserEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT SSP_NOTIFICATIONS_SEQ.NEXTVAL FROM DUAL";
                        entity.NOTIFICATIONID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = "INSERT INTO SSPNOTIFICATIONS (NOTIFICATIONID,NOTIFICATIONTYPEID,USERID,DATECREATED,DATEREQUESTED,NOTIFICATIONCODE,JOBNUMBER,BOOKINGID,QUOTATIONID,CHARGESETID) VALUES (:NOTIFICATIONID,:NOTIFICATIONTYPEID,:USERID,:DATECREATED,:DATEREQUESTED,:NOTIFICATIONCODE,:JOBNUMBER,:BOOKINGID,:QUOTATIONID,:CHARGESETID)";
                        mCommand.Parameters.Add("NOTIFICATIONID", Convert.ToInt32(entity.NOTIFICATIONID));
                        mCommand.Parameters.Add("NOTIFICATIONTYPEID", Convert.ToInt32(nTypeId));
                        mCommand.Parameters.Add("USERID", UserId.ToLower());
                        mCommand.Parameters.Add("DATECREATED", DateTime.Now);
                        mCommand.Parameters.Add("DATEREQUESTED", DateTime.Now);
                        mCommand.Parameters.Add("NOTIFICATIONCODE", Status);
                        mCommand.Parameters.Add("JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("BOOKINGID", BookingId);
                        mCommand.Parameters.Add("QUOTATIONID", Convert.ToInt32(QuotationId));
                        mCommand.Parameters.Add("CHARGESETID", chargeSetid);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return "Success";
        }

        public DataTable GetAdditionalChargeSum(int jobnumber, int ChargeSetId)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "select sum(TOTALPRICE) TOTALPRICE,(select count(DISTINCT(C.CHARGESETID))as TCOUNT from JOBSERVICECHARGE C where C.jobnumber=S.jobnumber) TCOUNT from JOBSERVICECHARGE S where S.jobnumber=:JOBNUMBER and S.CHARGESETID=:CHARGESETID GROUP BY S.JOBNUMBER";
                        mCommand.Parameters.Add("CHARGESETID", ChargeSetId);
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public int CheckIsMandatoryDocsUploaded(string jobNumber)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "select DOCID from SSPDOCUMENTS where JOBNUMBER=:JOBNUMBER and CONDMAND='Mandatory' and FILENAME is null";
                        mCommand.Parameters.Add("JOBNUMBER", jobNumber);
                        OracleDataReader rdr = mCommand.ExecuteReader();

                        DataTable dtCharges = new DataTable();
                        dtCharges.Load(rdr);
                        rdr.Close();
                        mConnection.Close();

                        return dtCharges.Rows.Count;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public string GetEmailList(string roles, string bookingId = "", string partyType = "", string countryId = "", string UserID = "")
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_ROLEIDS", roles);
                        if (partyType != "")
                            mCommand.Parameters.Add("PRM_OPJOBNUMBER", bookingId);
                        else
                            mCommand.Parameters.Add("PRM_OPJOBNUMBER", DBNull.Value);

                        if (partyType != "")
                            mCommand.Parameters.Add("PRM_NOTIFICATIONTYPE", partyType);
                        else
                            mCommand.Parameters.Add("PRM_NOTIFICATIONTYPE", DBNull.Value);


                        if (UserID != "")
                            mCommand.Parameters.Add("PRM_USERID", UserID);
                        else
                            mCommand.Parameters.Add("PRM_USERID", DBNull.Value);

                        if (countryId != "")
                            mCommand.Parameters.Add("PRM_COUNTRYIDS", countryId);
                        else
                            mCommand.Parameters.Add("PRM_COUNTRYIDS", DBNull.Value);


                        mCommand.Parameters.Add("PRM_EMAILLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_SSP_NOTIFICATIONEMAILIDS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["PRM_EMAILLIST"].Value).GetDataReader();
                        //DataTable dtQuotation = new DataTable(); dtQuotation.Load(UserStatus);

                        while (UserStatus.Read())
                        {
                            result = UserStatus[0].ToString();
                        }
                        mConnection.Close();

                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public DataTable GetChargeDescriptionList(int jobNumber, int chargeSetID, string ChargeType)
        {
            try
            {
                string Query = "select DISTINCT(CHARGENAME) from JOBSERVICECHARGE where JOBNUMBER=:JOBNUMBER and upper(CHARGESTATUS)=:CHARGESTATUS";
                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER" , Value = jobNumber },
                    new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "CHARGESTATUS" , Value = ChargeType }
                });
                return dt;
            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
        }

        public int GetDecimalPointByCurrencyCode(string currencyCode)
        {
            int decimalPoint;
            if (currencyCode != null)
            {
                decimalPoint = this.ExecuteScalar<int>(DBConnectionMode.SSP, "select decimals from CURRENCIES where currencycode=:currencyCode",
                  new OracleParameter[]{
                    new OracleParameter{ ParameterName = "currencyCode",Value = currencyCode.ToUpper()},
                });
            }
            else
            {
                decimalPoint = 0;
            }
            return decimalPoint;
        }

        public string RequestCancelJob(int JobNumber, string OpJobNumber, string ReasonDescription, string Reason, string User)
        {
            string serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("PRM_REASONDESCRIPTION", ReasonDescription);
                        mCommand.Parameters.Add("PRM_OPJOBNUMBER", OpJobNumber);
                        mCommand.Parameters.Add("PRM_REASON", Reason);
                        mCommand.Parameters.Add("PRM_CREATEDBY", User);
                        mCommand.Parameters.Add("PRM_SERVERNAME", serverName);

                        mCommand.CommandText = "USP_SSP_REQUESTCANCELJOBMAIN";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        result = "success";
                        mConnection.Close();

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = "fail";
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public int getQuotationId(int JobNumber)
        {
            int quotationId;
            if (JobNumber > 0)
            {
                quotationId = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT QUOTATIONID FROM SSPJOBDETAILS WHERE JOBNUMBER=:JobNumber",
                  new OracleParameter[]{
                    new OracleParameter{ ParameterName = "JobNumber",Value = JobNumber},
                });
            }
            else
            {
                quotationId = 0;
            }
            return quotationId;
        }

        public RefundPaymentEntity GetBookingCancellationDetails(string id)
        {
            RefundPaymentEntity mEntity = new RefundPaymentEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IN_RECEIPTPAYMENTDTLSID", Convert.ToInt64(id));
                        mCommand.Parameters.Add("CUR_DETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_REFUNDJOBDETATAILS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtPaymentDetails = new DataTable();
                        OracleDataReader PaymentDetails = ((OracleRefCursor)mCommand.Parameters["CUR_DETAILS"].Value).GetDataReader();
                        dtPaymentDetails.Load(PaymentDetails);
                        mEntity = MyClass.ConvertToEntity<RefundPaymentEntity>(dtPaymentDetails);

                        PaymentDetails.Close();
                        mConnection.Close();
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }
        public string TradeFinanceSave(string name, string interested, string tradetype, byte[] HTMLBODY, string sender, string receiver, int bookingid, int quotationid)
        {
            UserEntity entity = new UserEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {

                        mCommand.CommandText = "INSERT INTO SSPTRADEFINANCE (USERID,INTERESTED,TRADETYPE,HTMLBODY,SENDER,RECEIVER,JOBNUMBER,QUOTATIONID) VALUES (:USERID,:INTERESTED,:TRADETYPE,:HTMLBODY,:SENDER,:RECEIVER,:JOBNUMBER,:QUOTATIONID)";
                        mCommand.Parameters.Add("USERID", name);
                        mCommand.Parameters.Add("INTERESTED", interested);
                        mCommand.Parameters.Add("TRADETYPE", tradetype);
                        mCommand.Parameters.Add("HTMLBODY", HTMLBODY);
                        mCommand.Parameters.Add("SENDER", sender);
                        mCommand.Parameters.Add("RECEIVER", receiver);
                        mCommand.Parameters.Add("JOBNUMBER", bookingid);
                        mCommand.Parameters.Add("QUOTATIONID", quotationid);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return "Success";
        }

        public DataTable GetCancellationReceiptDtls(int jobnumber)
        {
            try
            {
                string Query = @" SELECT ROUTETYPEID,SUM(TOTALPRICE) TOTALPRICE FROM QMSERVICECHARGE WHERE QUOTATIONID=(SELECT QUOTATIONID FROM SSPJOBDETAILS WHERE JOBNUMBER=:JOBNUMBER) AND UPPER(CHARGEAPPLICABILITY)='CURRENT' AND ISCHARGEINCLUDED=1 GROUP BY ROUTETYPEID
                                  union all SELECT COUNT(*) AS ROUTETYPEID,  SUM(TOTALPRICE) TOTALPRICE FROM JOBSERVICECHARGE WHERE UPPER(CHARGESTATUS) IN ('PAID','ACCEPTED BY CUSTOMER') AND JOBNUMBER=:JOBNUMBER GROUP BY QUOTATIONID";

                DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = jobnumber} 
            });
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<OnlinePaymentTransactionEntity> GetOnlineTransactionDetails(string id)
        {

            List<OnlinePaymentTransactionEntity> data = new List<OnlinePaymentTransactionEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM ONLINEPAYMENTTRANSACTION WHERE JOBNUMBER = :JOBNUMBER",
                new OracleParameter[] { new OracleParameter { ParameterName = "JOBNUMBER", Value = id } }
            , delegate(IDataReader r)
            {
                data.Add(DataReaderMapToObject<OnlinePaymentTransactionEntity>(r));
            });
            return data;
        }

        public string UpdateRefundStatus(List<OnlineRefundEntity> data)
        {
            foreach (var item in data)
            {
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;
                        mCommand.CommandText = "UPDATE ONLINEPAYMENTTRANSACTION SET REFUNDID=:REFUNDID,REFUNDAMOUNT=:AMOUNT,REFUNDSTATUS=:STATUS WHERE TRANSATIONID=:TID";
                        mCommand.Parameters.Add("TID", item.Id.ToString());
                        mCommand.Parameters.Add("REFUNDID", item.RefundRefNumber);
                        mCommand.Parameters.Add("AMOUNT", item.RefundAmount);
                        mCommand.Parameters.Add("STATUS", item.RefundStatus);
                        mCommand.ExecuteNonQuery();
                        mCommand.Transaction.Commit();
                        mCommand.Parameters.Clear();
                    }
                }
            };
            return "OK";
        }

        public List<OnlinePaymentTransactionEntity> GetPendingPayouts()
        {
            List<OnlinePaymentTransactionEntity> data = new List<OnlinePaymentTransactionEntity>();
            ReadText(DBConnectionMode.SSP, @"SELECT * FROM
                                            ONLINEPAYMENTTRANSACTION
                                            WHERE STATUS='succeeded'
                                            AND CREATEDDATE <= SYSDATE - 6
                                            AND PAYOUTSTATUS != 'available'
                                            AND STRIPEACCOUNT != 'HSBC'
                                            ",
                new OracleParameter[] { new OracleParameter { } }
            , delegate(IDataReader r)
            {
                data.Add(DataReaderMapToObject<OnlinePaymentTransactionEntity>(r));
            });
            return data;
        }

        public string UpdatePayoutStatus(List<OnlinePaymentTransactionEntity> data)
        {
            foreach (var item in data)
            {
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;
                        mCommand.CommandText = "UPDATE ONLINEPAYMENTTRANSACTION SET PAYOUTSTATUS=:STATUS WHERE STRIPETRANSACTIONID=:STRIPETRANSACTIONID";
                        mCommand.Parameters.Add("STRIPETRANSACTIONID", item.STRIPETRANSACTIONID);
                        mCommand.Parameters.Add("STATUS", item.PAYOUTSTATUS);
                        mCommand.ExecuteNonQuery();
                        mCommand.Transaction.Commit();
                        mCommand.Parameters.Clear();
                    }
                }
            };
            return "OK";
        }

        public string GetUserFirstNInsertSupplier(string BookingId, string SupplierUser, string Owner, string note)
        {
            string FirstName = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            DataTable dtMessage = new DataTable();
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_BOOKINGID", BookingId);
                        mCommand.Parameters.Add("PRM_SUPPLIERUSER", SupplierUser.ToLowerInvariant());
                        mCommand.Parameters.Add("PRM_OWNER", Owner.ToLowerInvariant());
                        mCommand.Parameters.Add("PRM_NOTE", note);

                        mCommand.Parameters.Add("CUR_NAME", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "SSP_USP_GETNSERTSUPPLIER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mCommand.Transaction.Commit();

                        OracleDataReader UserData = ((OracleRefCursor)mCommand.Parameters["CUR_NAME"].Value).GetDataReader();
                        while (UserData.Read())
                        {
                            for (int i = 0; i < UserData.FieldCount; i++)
                            {
                                FirstName = UserData[i].ToString();
                            }
                        }
                        mCommand.Parameters.Clear();
                        mConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return FirstName;
        }

        public object GetAmazonAddressselected(string Code)
        {
            try
            {
                SSPPartyDetailsEntity mEntity = new SSPPartyDetailsEntity();
                OracleConnection mConnection;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.CommandType = CommandType.StoredProcedure;
                        try
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("PMR_CODE", Code);
                            mCommand.Parameters.Add("CUR_AMAZONPARTYDETAILS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                            mCommand.CommandText = "USP_GET_AMAZONPARTYDETAILS";
                            mCommand.CommandType = CommandType.StoredProcedure;
                            mCommand.ExecuteNonQuery();

                            DataTable dt = new DataTable();
                            OracleDataReader dtSSPPartyDetails = ((OracleRefCursor)mCommand.Parameters["CUR_AMAZONPARTYDETAILS"].Value).GetDataReader();
                            dt.Load(dtSSPPartyDetails);
                            DBFactory.Entities.SSPPartyDetailsEntity mSSPPartyDetails = MyClass.ConvertToEntity<SSPPartyDetailsEntity>(dt);

                            dtSSPPartyDetails.Close();
                            mEntity = mSSPPartyDetails;

                            mConnection.Close();
                            return mEntity;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.ToString());
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new ShipaDbException(ex.ToString());
            }
        }

        public PayementMenthods GetPaymentDetailsByUserCountry(Int32 JOBNUMBER, string userid)
        {
            PayementMenthods mEntity = new PayementMenthods();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {

                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {

                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_JOBNUMBER", JOBNUMBER);
                        mCommand.Parameters.Add("L_USERID", userid.ToUpper());
                        mCommand.Parameters.Add("CUR_PAYMENTOPTIONLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_JARECEIPTHEADER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_BC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_GET_PAYMENTDETAILS_MB";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtPaymentDetails = new DataTable();
                        OracleDataReader Jareceiptheader = ((OracleRefCursor)mCommand.Parameters["CUR_JARECEIPTHEADER"].Value).GetDataReader();
                        dtPaymentDetails.Load(Jareceiptheader);
                        mEntity.JobDetails = MyClass.ConvertToEntity<JobDetails>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader SSPPaymentOptionlist = ((OracleRefCursor)mCommand.Parameters["CUR_PAYMENTOPTIONLIST"].Value).GetDataReader();
                        dtPaymentDetails.Load(SSPPaymentOptionlist);
                        mEntity.PayMethods = MyClass.ConvertToEntity<SSPPaymentOptionList>(dtPaymentDetails);

                        dtPaymentDetails = new DataTable();
                        OracleDataReader BC = ((OracleRefCursor)mCommand.Parameters["CUR_BC"].Value).GetDataReader();
                        dtPaymentDetails.Load(BC);
                        mEntity.BusinessCredit = MyClass.ConvertToEntity<BusinessCredit>(dtPaymentDetails);

                        Jareceiptheader.Close();
                        SSPPaymentOptionlist.Close(); BC.Close();
                        mConnection.Close();

                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }

        }



        public CouponCode GetAgilityDiscountPrice(string CouponCode, string JobNumber, string UserID)
        {


            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_COUPONCODE", CouponCode);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("PRM_USERID", UserID);

                        mCommand.Parameters.Add("CUR_RTNMSG", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_APPLY_DISCOUNT_MB";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader Couponrdr = ((OracleRefCursor)mCommand.Parameters["CUR_RTNMSG"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(Couponrdr);
                        CouponCode cc = new CouponCode();
                        cc = MyClass.ConvertToEntity<CouponCode>(dt);
                        mConnection.Close();

                        return cc;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public List<SSPEntityListEntity> GetWireTransferDetails(string Userid)
        {
            var mBranchList = new List<SSPEntityListEntity>();
            var query = @"SELECT DISTINCT REGEXP_SUBSTR(CURRENCYCODE,'[^,]+', 1, LEVEL) CURRENCYCODE,PS.SSPPAYMENTSETUPID AS ENTITYKEY,PS.ACCHOLDERNAME AS BENEFICIARYNAME,PS.BANKBRANCH AS BANKNAME,PS.WIREACCOUNTNUMBER AS BANKACCOUNTNUMBER,PS.BRANCHADDRESS AS BRANCH,SUBSTR(PS.IFSC, 1, 50) AS IFSCCODE,PS.SWIFTCODE
                        FROM FOCIS.SSPPAYMENTSETUP PS WHERE CURRENCYCODE IS NOT NULL AND (LOWER(STATEID) IS NULL OR LOWER(STATEID) NOT IN ('deleted')) AND COUNTRYID = (SELECT DISTINCT UD.COUNTRYID FROM UMUSERADDRESSDETAILS UD WHERE UPPER(UD.USERID)=:USERID) CONNECT BY REGEXP_SUBSTR(CURRENCYCODE,'[^,]+', 1, LEVEL) IS NOT NULL";
            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "USERID", Value = Userid.ToString().ToUpper() }
                }
            , delegate(IDataReader r)
            {
                mBranchList.Add(DataReaderMapToObject<SSPEntityListEntity>(r));
            });

            return mBranchList;
        }

        public DataTable GetJobinfoBasedonBookingID(int p)
        {
            var Query = @"select jd.jobnumber,JD.OPERATIONALJOBNUMBER,jd.quotationid,jh.receipthdrid,
                            (select SSPPARTYDETAILS.CLIENTNAME from SSPPARTYDETAILS where jobnumber=jd.jobnumber and partytype=91) as Shipper,
                            (select SSPPARTYDETAILS.CLIENTNAME from SSPPARTYDETAILS where jobnumber=jd.jobnumber and partytype=92) as Consignee
                            from sspjobdetails jd
                            inner join jareceiptheader jh on jh.jobnumber=jd.jobnumber
                            where JD.JOBNUMBER=:JOBNUMBER";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = p } });
            return dt;
        }

        public SSPEntityListEntity GetEntitydetailsBasedonEntity(int SSPPAYMENTSETUPID)
        {
            SSPEntityListEntity mBranchList = new SSPEntityListEntity();
            var query = @"select * FROM FOCIS.SSPPAYMENTSETUP where SSPPAYMENTSETUPID=:SSPPAYMENTSETUPID";
            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "SSPPAYMENTSETUPID", Value = SSPPAYMENTSETUPID }
                }
            , delegate(IDataReader r)
            {
                mBranchList = DataReaderMapToObject<SSPEntityListEntity>(r);
            });

            return mBranchList;
        }

        public List<SSPBranchListEntity> GetPayAtBrachDetails(string Userid)
        {


            var mBranchList = new List<SSPBranchListEntity>();
            var query = @"SELECT DISTINCT REGEXP_SUBSTR(CURRENCYCODE,'[^,]+', 1, LEVEL) CURRENCYCODE,BRANCHKEY, SITECITY ||' - ' || SITENAME as CITYBRANCH,ISCASH,ISCHEQUE,ISDD,SITENAME,SITEADDRESS
                            FROM FOCIS.SSPPAYMENTBRANCHLIST WHERE CURRENCYCODE IS NOT NULL
                              AND COUNTRYID = (SELECT DISTINCT UD.COUNTRYID FROM UMUSERADDRESSDETAILS UD WHERE UPPER(UD.USERID)=:USERID )
                              AND (LOWER(STATEID) IS NULL OR LOWER(STATEID) NOT IN ('deleted')) 
                            CONNECT BY REGEXP_SUBSTR(CURRENCYCODE,'[^,]+', 1, LEVEL) IS NOT NULL";
            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "USERID", Value = Userid.ToString().ToUpper() }
                }
            , delegate(IDataReader r)
            {
                mBranchList.Add(DataReaderMapToObject<SSPBranchListEntity>(r));
            });

            return mBranchList;
        }

        public string GetUserIdByOpJobNumber(string opjobnumber)
        {
            string userId = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT CREATEDBY FROM SSPJOBDETAILS WHERE OPERATIONALJOBNUMBER=:OPJOBNUMBER",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "OPJOBNUMBER",Value = opjobnumber},
                });

            return userId;
        }

        public DateTime GetDiscountDateByDiscountCode(string discountCode)
        {
            DateTime dicountExpiryDate = this.ExecuteScalar<DateTime>(DBConnectionMode.SSP, "select validto from sspdiscounts where discountcode=:discountCode",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "discountcode",Value = discountCode},
                });

            return dicountExpiryDate;
        }

        public string GetDiscountedReferralValue(string CouponCode, string JobNumber, string userid)
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_COUPONCODE", CouponCode);
                        mCommand.Parameters.Add("PRM_JOBNUMBER", JobNumber);
                        mCommand.Parameters.Add("PRM_USERID", userid);

                        mCommand.Parameters.Add("CUR_RTNMSG", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_APPLY_REFERRAL_DISCOUNT";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["CUR_RTNMSG"].Value).GetDataReader();
                        while (UserStatus.Read())
                        {
                            for (int i = 0; i < UserStatus.FieldCount; i++)
                            {
                                result = result + "," + UserStatus[i].ToString();

                            }
                        }
                        mConnection.Close();
                        result = result.Substring(1);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public string SendReferal_CreditAdded_Email(string couponcode, string user, string currency, double discountValue)
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_COUPONCODE", couponcode);
                        mCommand.Parameters.Add("PRM_USER", user);
                        mCommand.Parameters.Add("PRM_PREFFCURRENCY", currency);
                        if (discountValue > 0)
                        {
                            mCommand.Parameters.Add("PRM_DISCOUNTVALUE", discountValue);
                        }
                        mCommand.Parameters.Add("CUR_RTNMSG", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_UPDATEREFERRAL";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["CUR_RTNMSG"].Value).GetDataReader();
                        while (UserStatus.Read())
                        {
                            for (int i = 0; i < UserStatus.FieldCount; i++)
                            {
                                result = result + "," + UserStatus[i].ToString();

                            }
                        }
                        mConnection.Close();
                        result = result.Substring(1);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public string GetCouponCode(int jobNumber1)
        {
            string result = string.Empty;
            ReadText(DBConnectionMode.SSP, "select (DISCOUNTCODE||','||DISCOUNTAMOUNT||','||CURRENCY) as dtls from JARECEIPTHEADER where JOBNUMBER=:JOBNUMBER",
               new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = jobNumber1 }
                }
               , delegate(IDataReader r)
               {
                   result = Convert.ToString(r["dtls"]);
               });
            return result;
        }

        public dynamic GetCurrencyAndOrder(string amount, string from, string to)
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_TOCURRENCY", to);
                        mCommand.Parameters.Add("PRM_FROMCURRENCY", from);
                        mCommand.Parameters.Add("PRM_AMOUNT", amount);

                        mCommand.Parameters.Add("CUR_RTNMSG", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_SSP_GETCURRENCYANDORDER";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["CUR_RTNMSG"].Value).GetDataReader();
                        while (UserStatus.Read())
                        {
                            for (int i = 0; i < UserStatus.FieldCount; i++)
                            {
                                result = result + "," + UserStatus[i].ToString();

                            }
                        }
                        mConnection.Close();
                        result = result.Substring(1);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public OnlinePaymentTransactionEntity GetHsbcJobNumber(string id)
        {
            OnlinePaymentTransactionEntity data = new OnlinePaymentTransactionEntity();
            ReadText(DBConnectionMode.SSP, @"SELECT OT.JOBNUMBER,JO.QUOTATIONID,JO.CREATEDBY
                                            FROM ONLINEPAYMENTTRANSACTION OT
                                            INNER JOIN SSPJOBDETAILS JO ON OT.JOBNUMBER = JO.JOBNUMBER
                                            WHERE OT.TOKENID= :TOKENID
                                            ",
                new OracleParameter[] { new OracleParameter { ParameterName = "TOKENID", Value = id } }
            , delegate(IDataReader r)
            {
                data = DataReaderMapToObject<OnlinePaymentTransactionEntity>(r);
            });
            return data;
        }

        public void UpdateHsbcRefundStatus(string id)
        {
            using (OracleConnection mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.CommandText = "UPDATE OnlinepaymentTransaction SET REFUNDSTATUS='succeeded' WHERE REFUNDID=:id";
                    mCommand.Parameters.Clear();
                    mCommand.Parameters.Add("id", id);
                    mCommand.ExecuteNonQuery();
                }
            }
        }

        public JobandQuoteEntity GetJobandQuotedetails(int JOBNUMBER)
        {
            JobandQuoteEntity mEntity = new JobandQuoteEntity();
            string query= @"SELECT Q.PRODUCTNAME, Q.MOVEMENTTYPENAME, Q.ORIGINPLACENAME, Q.ORIGINPORTNAME,
                        Q.DESTINATIONPLACENAME, Q.DESTINATIONPORTNAME, Q.OCOUNTRYNAME, Q.DCOUNTRYNAME,
                        J.LIVEUPLOADS,J.LOADINGDOCKAVAILABLE,J.CARGOPALLETIZED ,J.ORIGINALDOCUMENTSREQUIRED
                        ,J.TEMPERATURECONTROLREQUIRED,J.COMMERCIALPICKUPLOCATION,J.SPECIALINSTRUCTIONS,U.COUNTRYNAME
                        FROM QMQUOTATION Q
                        LEFT JOIN SSPJOBDETAILS J ON  Q.QUOTATIONID=J.QUOTATIONID LEFT JOIN USERS U ON UPPER(U.USERID)=UPPER(J.CREATEDBY) WHERE J.JOBNUMBER=:JOBNUMBER";
            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JOBNUMBER } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<JobandQuoteEntity>(r);
            });

            return mEntity;

        }
    }
}
