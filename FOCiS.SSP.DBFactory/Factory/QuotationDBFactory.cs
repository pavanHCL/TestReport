﻿using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;
using FOCiS.SSP.DBFactory.Factory;

namespace FOCiS.SSP.DBFactory
{
    public class QuotationDBFactory : DBFactoryCore, IDisposable, IQuotationDBFactory
    {
        public List<QuoteJobListEntity> GetSavedQuotes(string userId, string status, string list, int PageNo, string SearchString, string Order)
        {
            string mQuery = string.Empty;
            string mSearch = string.Empty;
            status = (status ?? "").ToUpperInvariant().Trim();
            List<QuoteJobListEntity> mEntity = new List<QuoteJobListEntity>();
            if (list.ToUpperInvariant() == "QUOTELIST")
            {
                if (SearchString != null && SearchString != "")
                {
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%'  OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString  OR UPPER(PRODUCTNAME) = :SearchString  OR UPPER(MOVEMENTTYPENAME) = :SearchString )";
                }
                switch (status)
                {
                    case "ACTIVE":
                        mQuery = mSearch + " AND (TRUNC(DATEOFVALIDITY) >= TRUNC(SYSDATE) OR TRUNC(DATEOFVALIDITY) IS NULL) ";
                        break;
                    case "EXPIRED":
                        mQuery = mSearch + " AND TRUNC(DATEOFVALIDITY) < TRUNC(SYSDATE) ";
                        break;
                    default:
                        mQuery = mSearch;
                        break;
                }
                int StartQuote = (PageNo - 1) * 10;
                int EndQuote = PageNo * 10;
                string Query = "";
                if (Order == "ASC")
                {
                    Query = @"SELECT * FROM (SELECT ROWNUM AS rids,ABCD.* FROM ( SELECT * FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION WHERE LOWER(CREATEDBY) = :CREATEDBY " + mQuery + " ORDER BY DATEOFENQUIRY " + Order + ") ABC) ORDER BY rid DESC ) ABCD) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + " ORDER BY rids DESC";
                }
                else
                {
                    Query = "SELECT * FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION WHERE LOWER(CREATEDBY) = :CREATEDBY " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote;
                }

                DataTable dt;
                if (SearchString != null && SearchString != "")
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToLowerInvariant() }
                    });
                }
                else
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } 
                    });
                }
                mEntity = MyClass.ConvertToList<QuoteJobListEntity>(dt);

                List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();


                if (SearchString != null && SearchString != "")
                {
                    ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION WHERE LOWER(CREATEDBY) = :CREATEDBY " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + ")",
                   new OracleParameter[] { 
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                       new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToLowerInvariant() }
                   }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }
                else
                {
                    ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION WHERE LOWER(CREATEDBY) = :CREATEDBY " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + ")",
                        new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }

                foreach (var item in mEntity)
                {
                    item.ShipmentItems = mShipmentItems.FindAll(delegate(QuotationShipmentEntity qsItems)
                    {
                        return qsItems.QUOTATIONID == item.QUOTATIONID;
                    });
                }
            }
            else
            {
                string Query = "";
                int StartQuote = (PageNo - 1) * 10;
                int EndQuote = PageNo * 10;
                if (SearchString != null && SearchString != "")
                {
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(QU.ORIGINPLACENAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QU.ORIGINPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPLACENAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QU.DESTINATIONPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QU.QUOTATIONNUMBER) = :SearchString  OR UPPER(QU.PRODUCTNAME) = :SearchString OR UPPER(QU.MOVEMENTTYPENAME) = :SearchString  OR UPPER(job.OPERATIONALJOBNUMBER) = :SearchString )";
                }
                switch (status)
                {
                    case "ACTIVE":
                        mQuery = @" INNER JOIN (SELECT * FROM SSPJOBDETAILS WHERE STATEID IN('Payment in Pending','Payment in Progress','Job Initiated')
                        ORDER BY DATEMODIFIED DESC)job ON QU.QUOTATIONID = job.QUOTATIONID WHERE QU.CREATEDBY = :CUSTOMERID " + mSearch +
                        " AND (QU.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL) OR job.STATEID = 'Payment in Pending') ORDER BY job.DATEMODIFIED DESC ) ABC)) ";
                        break;
                    case "EXPIRED":
                        mQuery = @" INNER JOIN (select * from SSPJOBDETAILS  order by DATEMODIFIED desc) job ON QU.QUOTATIONID = job.QUOTATIONID
                        WHERE QU.CREATEDBY = :CUSTOMERID " + mSearch +
                        " AND (QU.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY') FROM DUAL) AND job.STATEID <> 'Payment in Pending' OR (job.STATEID  = 'Payment Completed'))) ABC))";
                        break;
                    default:
                        mQuery = @" INNER JOIN (select * from SSPJOBDETAILS  order by DATEMODIFIED desc) job ON QU.QUOTATIONID  =job.QUOTATIONID
                        WHERE QU.CREATEDBY = :CUSTOMERID " + mSearch + " ) ABC))";
                        break;
                }

                Query = @"SELECT * FROM ((SELECT ROWNUM AS rid, ABC.* FROM (SELECT QU.*, job.JOBNUMBER, job.OPERATIONALJOBNUMBER, job.STATEID AS JSTATEID,
                job.DATEMODIFIED AS JDDATEMODIFIED, job.JOBGRANDTOTAL AS JOBGRANDTOTAL FROM QMQUOTATION QU " + mQuery + " WHERE rid > " + StartQuote + " AND rid <= " + EndQuote;

                DataTable dt;
                if (SearchString != null && SearchString != "")
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() },
                        new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToLowerInvariant() }
                    });
                }
                else
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } 
                    });
                }

                mEntity = MyClass.ConvertToList<QuoteJobListEntity>(dt);

                List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();
                ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN(SELECT QUOTATIONID FROM QMQUOTATION WHERE CREATEDBY = :CUSTOMERID) ",
                    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
                , delegate(IDataReader r)
                {
                    mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                });
                foreach (var item in mEntity)
                {
                    item.ShipmentItems = mShipmentItems.FindAll(delegate(QuotationShipmentEntity qsItems)
                    {
                        return qsItems.QUOTATIONID == item.QUOTATIONID;
                    });
                }
            }

            List<SSPJobDetailsEntity> mJobItems = new List<SSPJobDetailsEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPJOBDETAILS WHERE QUOTATIONID IN(SELECT QUOTATIONID FROM QMQUOTATION WHERE CREATEDBY = :CUSTOMERID) ",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mJobItems.Add(DataReaderMapToObject<SSPJobDetailsEntity>(r));
            });
            foreach (var item in mEntity)
            {
                item.JobItems = mJobItems.FindAll(delegate(SSPJobDetailsEntity qsItems)
                {
                    return qsItems.QUOTATIONID == item.QUOTATIONID;
                });
            }

            List<ReceiptHeaderEntity> header = new List<ReceiptHeaderEntity>();
            ReadText(DBConnectionMode.SSP, "SELECT RECEIPT.JOBNUMBER,RECEIPT.RECEIPTAMOUNT,RECEIPT.RECEIPTTAX,RECEIPT.RECEIPTNETAMOUNT FROM  JARECEIPTHEADER RECEIPT WHERE CREATEDBY = :CUSTOMERID",
               new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CUSTOMERID", Value = userId.ToLowerInvariant() } }
           , delegate(IDataReader r)
           {
               header.Add(DataReaderMapToObject<ReceiptHeaderEntity>(r));
           });
            foreach (var item in mEntity)
            {
                item.ReceiptHeaderDetails = header.FindAll(delegate(ReceiptHeaderEntity qsItems)
                {
                    return qsItems.JOBNUMBER == item.JOBNUMBER;
                });
            }
            return mEntity;
        }

        public List<QuoteJobListEntity> GetOnlySavedQuotes(string userId, string status, string list, int PageNo, string SearchString, string Order)
        {
            string mQuery = string.Empty;
            string mSearch = string.Empty;
            status = (status ?? "").ToUpperInvariant().Trim();
            List<QuoteJobListEntity> mEntity = new List<QuoteJobListEntity>();
            if (list.ToUpperInvariant() == "QUOTELIST")
            {
                if (SearchString != null && SearchString != "")
                {
                    if (SearchString.ToLowerInvariant().Contains("airport"))
                    {
                        SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase); //SearchString.ToUpperInvariant();
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = 'AIR'  AND UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    }
                    else if (SearchString.ToLowerInvariant().Contains("port"))
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = 'OCEAN'  AND UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    }
                    else
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = :SearchString  OR UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    }
                }
                switch (status)
                {
                    case "ACTIVE":
                        mQuery = mSearch + " AND (TRUNC(DATEOFVALIDITY) >= TRUNC(SYSDATE) OR TRUNC(DATEOFVALIDITY) IS NULL) ";
                        break;
                    case "EXPIRED":
                        mQuery = mSearch + " AND TRUNC(DATEOFVALIDITY) < TRUNC(SYSDATE) ";
                        break;
                    default:
                        mQuery = mSearch;
                        break;
                }
                int StartQuote = (PageNo - 1) * 10;
                int EndQuote = PageNo * 10;
                string Query = "";
                if (Order == "ASC")
                {
                    Query = @"SELECT * FROM (SELECT ROWNUM AS rids,ABCD.* FROM ( SELECT * FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION LEFT JOIN(SELECT ISCHARGEINCLUDED,QUOTATIONID  FROM QMSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) QC on QC.QUOTATIONID = QMQUOTATION.QUOTATIONID WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY)" + mQuery + " ORDER BY DATEOFENQUIRY " + Order + ") ABC) ORDER BY rid DESC ) ABCD) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + " ORDER BY rids DESC";
                }
                else
                {
                    Query = "SELECT * FROM (SELECT ROWNUM AS rid, ABC.*, (SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID)=ABC.PREFERREDCURRENCYID) AS DECIMALCOUNT FROM (SELECT * FROM QMQUOTATION LEFT JOIN(SELECT ISCHARGEINCLUDED,QUOTATIONID  FROM QMSERVICECHARGE WHERE LOWER(CHARGENAME) LIKE 'insurance' ) QC on QC.QUOTATIONID = QMQUOTATION.QUOTATIONID WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote;
                }
                DataTable dt;
                if (SearchString != null && SearchString != "")
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                        new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() }
                    });
                }
                else
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } });
                }

                mEntity = MyClass.ConvertToList<QuoteJobListEntity>(dt);
                List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();

                if (SearchString != null && SearchString != "")
                {
                    ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + ")",
                    new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                        new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() }
                    }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }
                else
                {
                    ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEM WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATION WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + ")",
                        new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }

                foreach (var item in mEntity)
                {
                    item.LANGUAGEMOVEMENTTYPENAME = item.MOVEMENTTYPENAME;
                    item.ShipmentItems = mShipmentItems.FindAll(delegate(QuotationShipmentEntity qsItems)
                    {
                        return qsItems.QUOTATIONID == item.QUOTATIONID;
                    });
                }
            }
            return mEntity;
        }

        public List<QuoteJobListEntity> GetOnlySavedTemplates(string userId, string status, string list, int PageNo, string SearchString, string Order)
        {
            string mQuery = string.Empty;
            string mSearch = string.Empty;
            status = (status ?? "").ToUpperInvariant().Trim();
            List<QuoteJobListEntity> mEntity = new List<QuoteJobListEntity>();
            if (list.ToUpperInvariant() == "QUOTELIST")
            {
                if (SearchString != null && SearchString != "")
                {
                    if (SearchString.ToLowerInvariant().Contains("airport"))
                    {
                        SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase); //SearchString.ToUpperInvariant();
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = 'AIR'  AND UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    }
                    else if (SearchString.ToLowerInvariant().Contains("port"))
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = 'OCEAN'  AND UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    }
                    else
                    {
                        SearchString = SearchString.ToUpperInvariant();
                        mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(TEMPLATENAME) = :SearchString OR UPPER(PRODUCTNAME) = :SearchString OR UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    }
                }
                mQuery = mSearch;
                int StartQuote = (PageNo - 1) * 10;
                int EndQuote = PageNo * 10;
                string Query = "";
                if (Order == "ASC")
                {
                    Query = @"SELECT * FROM (SELECT ROWNUM AS rids,ABCD.* FROM (SELECT * FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATIONTEMPLATE WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND REQUESTTYPE IS NULL AND LOWER(CREATEDBY) = :CREATEDBY " + mQuery + " ORDER BY DATEOFENQUIRY " + Order + ") ABC) ORDER BY rid DESC ) ABCD) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + " ORDER BY rids DESC";
                }
                else
                {
                    Query = "SELECT * FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATIONTEMPLATE WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND REQUESTTYPE IS NULL AND LOWER(CREATEDBY) = :CREATEDBY " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote;
                }
                DataTable dt;
                if (SearchString != null && SearchString != "")
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() } 
                    });
                }
                else
                {
                    dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } });
                }

                mEntity = MyClass.ConvertToList<QuoteJobListEntity>(dt);
                List<QuotationShipmentEntity> mShipmentItems = new List<QuotationShipmentEntity>();

                if (SearchString != null && SearchString != "")
                {
                    ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEMTEMPLATE WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATIONTEMPLATE WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND REQUESTTYPE IS NULL AND LOWER(CREATEDBY) = :CREATEDBY " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + ")",
                    new OracleParameter[] { 
                        new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                        new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() }
                    }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }
                else
                {
                    ReadText(DBConnectionMode.SSP, "SELECT * FROM QMSHIPMENTITEMTEMPLATE WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM (SELECT ROWNUM AS rid, ABC.* FROM (SELECT * FROM QMQUOTATIONTEMPLATE WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND REQUESTTYPE IS NULL AND LOWER(CREATEDBY) = :CREATEDBY " + mQuery + "  ORDER BY DATEOFENQUIRY " + Order + ") ABC) WHERE rid > " + StartQuote + " AND rid <= " + EndQuote + ")",
                        new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
                    , delegate(IDataReader r)
                    {
                        mShipmentItems.Add(DataReaderMapToObject<QuotationShipmentEntity>(r));
                    });
                }

                foreach (var item in mEntity)
                {
                    item.ShipmentItems = mShipmentItems.FindAll(delegate(QuotationShipmentEntity qsItems)
                    {
                        return qsItems.QUOTATIONID == item.QUOTATIONID;
                    });
                }
            }
            return mEntity;
        }

        public QuoteStatusCountEntity GetQuoteStatusCount(string userId, string SearchString, string status)
        {
            string mSearch = string.Empty;
            string mSearchNomination = string.Empty;
            if (SearchString != null && SearchString != "")
            {
                if (SearchString.ToLowerInvariant().Contains("airport"))
                {
                    SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase); //SearchString.ToUpperInvariant();
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = 'AIR'  AND UPPER(MOVEMENTTYPENAME) = :SearchString )";
                }
                else if (SearchString.ToLowerInvariant().Contains("port"))
                {
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString OR UPPER(PRODUCTNAME) = 'OCEAN'  AND UPPER(MOVEMENTTYPENAME) = :SearchString )";
                }
                else
                {
                    SearchString = SearchString.ToUpperInvariant();
                    mSearch = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QUOTATIONNUMBER) = :SearchString  OR UPPER(PRODUCTNAME) = :SearchString OR UPPER(MOVEMENTTYPENAME) = :SearchString )";
                    mSearchNomination = " AND (UPPER(ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(TEMPLATENAME) = :SearchString OR UPPER(PRODUCTNAME) = :SearchString  OR UPPER(MOVEMENTTYPENAME) = :SearchString  )";
                }
            }

            QuoteStatusCountEntity mEntity = new QuoteStatusCountEntity();
            if (SearchString != null && SearchString != "")
            {
                ReadText(DBConnectionMode.SSP, "SELECT (SELECT COUNT(1) FROM QMQUOTATION WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mSearch + " AND (TRUNC (DATEOFVALIDITY) >= TRUNC(SYSDATE) OR TRUNC(DATEOFVALIDITY) IS NULL)) AS ACTIVEQUOTES, (SELECT COUNT(1) FROM QMQUOTATION WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mSearch + " AND TRUNC (DATEOFVALIDITY) < TRUNC(SYSDATE)) AS EXPIREDQUOTES, (SELECT COUNT(1) FROM QMQUOTATIONTEMPLATE WHERE LOWER(CREATEDBY) = :CREATEDBY " + mSearch + " AND REQUESTTYPE IS NULL " + mSearchNomination + " ) AS NOMINATIONS FROM DUAL",
                new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() }
                }
                , delegate(IDataReader r)
                {
                    mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
                });
            }
            else
            {
                ReadText(DBConnectionMode.SSP, "SELECT (SELECT COUNT(1) FROM QMQUOTATION WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mSearch + " AND (TRUNC (DATEOFVALIDITY) >= TRUNC(SYSDATE) OR TRUNC(DATEOFVALIDITY) IS NULL)) AS ACTIVEQUOTES, (SELECT COUNT(1) FROM QMQUOTATION WHERE (STATEID <> 'QUOTEDELETED' AND STATEID <> 'COPYQUOTE') AND (LOWER(CREATEDBY) = :CREATEDBY OR LOWER(GUESTEMAIL) = :CREATEDBY) " + mSearch + " AND TRUNC (DATEOFVALIDITY) < TRUNC(SYSDATE)) AS EXPIREDQUOTES, (SELECT COUNT(1) FROM QMQUOTATIONTEMPLATE WHERE LOWER(CREATEDBY) = :CREATEDBY AND REQUESTTYPE IS NULL " + mSearchNomination + " ) AS NOMINATIONS FROM DUAL",
                    new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
                , delegate(IDataReader r)
                {
                    mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
                });
            }
            return mEntity;
        }

        public QuoteStatusCountEntity GetJobStatusCount(string userId, string SearchString)
        {
            string mSearch = string.Empty;
            if (SearchString != null && SearchString != "")
            {
                SearchString = SearchString.ToUpperInvariant();
                mSearch = " AND (UPPER(QM.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.ORIGINPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.QUOTATIONNUMBER) = :SearchString OR UPPER(QM.PRODUCTNAME) = :SearchString  OR UPPER(QM.MOVEMENTTYPENAME) = :SearchString  OR UPPER(SP.OPERATIONALJOBNUMBER) = :SearchString  )";
            }
            QuoteStatusCountEntity mEntity = new QuoteStatusCountEntity();
            if (SearchString != null && SearchString != "")
            {
                ReadText(DBConnectionMode.SSP, "select (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + ") AS ALLJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + " and (QM.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) OR sp.stateid  = 'Payment in Pending') AND ((sp.stateid  = 'Job Initiated') OR (sp.stateid  = 'Payment in Progress') OR (sp.stateid  = 'Payment in Pending'))) AS ACTIVEJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + " and (QM.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) AND sp.stateid  <> 'Payment in Pending' OR (sp.stateid  = 'Payment Completed'))) AS EXPIREDJOBS from dual",
                    new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() }
                }
                    , delegate(IDataReader r)
                    {
                        mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
                    });
            }
            else
            {
                ReadText(DBConnectionMode.SSP, "select (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + ") AS ALLJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + " and (QM.DATEOFVALIDITY >= (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) OR sp.stateid  = 'Payment in Pending') AND ((sp.stateid  = 'Job Initiated') OR (sp.stateid  = 'Payment in Progress') OR (sp.stateid  = 'Payment in Pending'))) AS ACTIVEJOBS, (select count(1) from SSPJOBDETAILS SP inner join QMQUOTATION  QM on SP.QUOTATIONID = QM.QUOTATIONID where LOWER(QM.CREATEDBY) = :CREATEDBY " + mSearch + " and (QM.DATEOFVALIDITY < (SELECT TO_CHAR (SYSDATE, 'DD-MON-YYYY')  FROM DUAL) AND sp.stateid  <> 'Payment in Pending' OR (sp.stateid  = 'Payment Completed'))) AS EXPIREDJOBS from dual",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
            });
            }
            return mEntity;
        }

        public DataTable ChargeNamebyLanguagetable(string languageName)
        {
            string languageChargeData = string.Empty;
            DataTable dtChargeData = null;
            try
            {
                string query = @"select * from   SSPLANGUAGEBASEDDATA_QUOTATION where languagetype='" + languageName.ToLower() + "'";
                dtChargeData = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                     new OracleParameter {   
                     } 
                  });
            }
            catch (Exception ex)
            {

            }

            return dtChargeData;
        }
        public string[] GetConversionSupportDataBySingleObject(string languageName, string ConversionObjectName, string ConversionObjectType)
        {
            string[] languageChargeData = new string[2];
            DataTable dtCustomerSupportData = null;
            try
            {
                string queryModule = @" SELECT SP.NAME FROM SSPCUSTSUPPORTCATEGOREIS SP join SSPCUSTSUPCATEGORIES_LANGUAGE  SL  ON SP.CATEGORYID= SL.CATEGORYID  where SL.LANGUAGECATEGORYNAME='" + ConversionObjectName + "'";
                dtCustomerSupportData = ReturnDatatable(DBConnectionMode.SSP, queryModule, new OracleParameter[] { 
                     new OracleParameter {   
                     } 
                  });
                if (dtCustomerSupportData.Rows.Count > 0)
                {
                    languageChargeData[0] = dtCustomerSupportData.Rows[0]["NAME"].ToString();
                }
                string queryType = @"  SELECT SPB.NAME FROM SSPCUSTSUPPORTSUBCATEGOREIS SPB join SSPCUSTSUBCATEGORIES_LANGUAGE  SBL  ON SBL.LANGUAGESUBCATEGORYID= SPB.subcategoryid  where SBL.languagesubcategoryname='" + ConversionObjectType + "'";
                dtCustomerSupportData = ReturnDatatable(DBConnectionMode.SSP, queryType, new OracleParameter[] { 
                     new OracleParameter {   
                     } 
                  });
                if (dtCustomerSupportData.Rows.Count > 0)
                {
                    languageChargeData[1] = dtCustomerSupportData.Rows[0]["NAME"].ToString();
                }
            }
            catch (Exception ex)
            {

            }
            return languageChargeData;
        }

        public SupportEntity GetConversionSupportDataByMultiObject(string languageName, SupportEntity SupportRequests)
        {
            SupportEntity objSupport = new SupportEntity();
            DataTable dtCustomerSupportData = null;
            DataTable dtCustomerSubSupportData = null;
            if (SupportRequests.Requests != null)
            {
                for (int k = 0; k < SupportRequests.Requests.Count; k++)
                {
                    string Module = SupportRequests.Requests[k].MODULE;
                    string Category = SupportRequests.Requests[k].CATEGORY;
                    try
                    {
                        string queryModule = @"SELECT SP.LANGUAGECATEGORYNAME FROM SSPCUSTSUPCATEGORIES_LANGUAGE  SP join SSPCUSTSUPPORTCATEGOREIS  SL  ON SP.CATEGORYID= SL.CATEGORYID  where SL.NAME='" + Module + "' AND SP.LANGUAGETYPE='" + languageName + "'";
                        dtCustomerSupportData = ReturnDatatable(DBConnectionMode.SSP, queryModule, new OracleParameter[] { 
                     new OracleParameter {   
                     } 
                  });
                        if (dtCustomerSupportData.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtCustomerSupportData.Rows.Count; i++)
                            {
                                string queryType = @" SELECT SPB.LANGUAGESUBCATEGORYNAME FROM SSPCUSTSUBCATEGORIES_LANGUAGE  SPB join SSPCUSTSUPPORTSUBCATEGOREIS  SBL  ON SPB.LANGUAGESUBCATEGORYID= SBL.subcategoryid  where SBL.NAME='" + Category + "' AND SPB.LANGUAGETYPE='" + languageName + "'";
                                dtCustomerSubSupportData = ReturnDatatable(DBConnectionMode.SSP, queryType, new OracleParameter[] { 
                        new OracleParameter {   
                       } 
                        });
                                if (dtCustomerSubSupportData.Rows.Count > 0)
                                {
                                    for (int j = 0; j < dtCustomerSubSupportData.Rows.Count; j++)
                                    {
                                        SupportRequests.Requests[k].MODULE = dtCustomerSupportData.Rows[i]["LANGUAGECATEGORYNAME"].ToString();
                                        SupportRequests.Requests[k].CATEGORY = dtCustomerSubSupportData.Rows[j]["LANGUAGESUBCATEGORYNAME"].ToString();
                                        SupportRequests.Chat[k].MODULE = dtCustomerSupportData.Rows[i]["LANGUAGECATEGORYNAME"].ToString();
                                        SupportRequests.Chat[k].CATEGORY = dtCustomerSubSupportData.Rows[j]["LANGUAGESUBCATEGORYNAME"].ToString();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                for (int k = 0; k < SupportRequests.Chat.Count; k++)
                {
                    string Module = SupportRequests.Chat[k].MODULE;
                    string Category = SupportRequests.Chat[k].CATEGORY;
                    try
                    {
                        string queryModule = @"SELECT SP.LANGUAGECATEGORYNAME FROM SSPCUSTSUPCATEGORIES_LANGUAGE  SP join SSPCUSTSUPPORTCATEGOREIS  SL  ON SP.CATEGORYID= SL.CATEGORYID  where SL.NAME='" + Module + "'";
                        dtCustomerSupportData = ReturnDatatable(DBConnectionMode.SSP, queryModule, new OracleParameter[] { 
                     new OracleParameter {   
                     } 
                     });
                        if (dtCustomerSupportData.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtCustomerSupportData.Rows.Count; i++)
                            {
                                string queryType = @" SELECT SPB.LANGUAGESUBCATEGORYNAME FROM SSPCUSTSUBCATEGORIES_LANGUAGE  SPB join SSPCUSTSUPPORTSUBCATEGOREIS  SBL  ON SPB.LANGUAGESUBCATEGORYID= SBL.subcategoryid  where SBL.NAME='" + Category + "'";
                                dtCustomerSubSupportData = ReturnDatatable(DBConnectionMode.SSP, queryType, new OracleParameter[] { 
                        new OracleParameter {   
                       } 
                       });
                                if (dtCustomerSubSupportData.Rows.Count > 0)
                                {
                                    for (int j = 0; j < dtCustomerSubSupportData.Rows.Count; j++)
                                    {
                                        SupportRequests.Chat[k].MODULE = dtCustomerSupportData.Rows[i]["LANGUAGECATEGORYNAME"].ToString();
                                        SupportRequests.Chat[k].CATEGORY = dtCustomerSubSupportData.Rows[j]["LANGUAGESUBCATEGORYNAME"].ToString();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return SupportRequests;
        }
        public string ChargeNamebyLanguage(string languageName, string defaultChargeName)
        {
            string languageChargeData = string.Empty;
            try
            {
                string query = @"select MODIFIEDREQUIREMENTDATA from SSPLANGUAGEBASEDDATA_QUOTATION where languagetype='" + languageName.ToLower() + "' and originalrequirementdata= '" + defaultChargeName + "'";
                DataTable dtChargeData = ReturnDatatable(DBConnectionMode.SSP, query, new OracleParameter[] { 
                     new OracleParameter {   
                     } 
                  });
                if (dtChargeData.Rows.Count > 0)
                {
                    languageChargeData = dtChargeData.Rows[0]["MODIFIEDREQUIREMENTDATA"].ToString();
                }
            }
            catch (Exception ex)
            {

            }
            return languageChargeData;
        }
        public QuoteStatusCountEntity GetQuoteJobCount(string userId, string SearchString)
        {
            string mSearch = string.Empty;
            string nSearch = string.Empty;
            string query = "";
            if (SearchString != null && SearchString != "")
            {
                SearchString = SearchString.ToUpperInvariant();
                mSearch = " AND (UPPER(QM.ORIGINPLACENAME) LIKE '%' || :SearchString  || '%' OR UPPER(QM.ORIGINPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPLACENAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.QUOTATIONNUMBER) = :SearchString OR UPPER(QM.PRODUCTNAME) = :SearchString OR UPPER(QM.MOVEMENTTYPENAME) = :SearchString )";
                nSearch = " AND (UPPER(QM.ORIGINPLACENAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.ORIGINPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPLACENAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.DESTINATIONPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(QM.QUOTATIONNUMBER) = :SearchString  OR UPPER(QM.PRODUCTNAME) = :SearchString  OR UPPER(QM.MOVEMENTTYPENAME) = :SearchString  OR UPPER(SP.OPERATIONALJOBNUMBER) = :SearchString  )";
            }
            query = "SELECT (SELECT COUNT(1) FROM QMQUOTATION QM WHERE LOWER(CREATEDBY) = :CREATEDBY " + mSearch + ") AS ALLQUOTES ,(SELECT COUNT(1) FROM SSPJOBDETAILS SP INNER JOIN QMQUOTATION  QM ON SP.QUOTATIONID = QM.QUOTATIONID WHERE LOWER(QM.CREATEDBY) = :CREATEDBY " + nSearch + ") AS ALLJOBS FROM DUAL";

            QuoteStatusCountEntity mEntity = new QuoteStatusCountEntity();
            if (SearchString != null && SearchString != "")
            {
                ReadText(DBConnectionMode.SSP, query,
                       new OracleParameter[] { 
                       new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() },
                       new OracleParameter {  ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() }
                   }
               , delegate(IDataReader r)
               {
                   mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
               });
            }
            else
            {
                ReadText(DBConnectionMode.SSP, query,
                   new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "CREATEDBY", Value = userId.ToLowerInvariant() } }
               , delegate(IDataReader r)
               {
                   mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
               });
            }
            return mEntity;
        }

        public List<QuoteOffersEntity> GetSavedOffers(string SearchString)
        {
            string mQuery = string.Empty;
            string mSearch = string.Empty;
            List<QuoteOffersEntity> mEntity = new List<QuoteOffersEntity>();

            if (SearchString != null && SearchString != "")
            {
                SearchString = SearchString.ToUpperInvariant();
                mSearch = " WHERE (UPPER(ORIGINPORTNAME) LIKE  '%' || :SearchString  || '%' OR UPPER(DESTINATIONPORTNAME) LIKE  '%' || :SearchString  || '%' )";
            }
            string Query = "";
            Query = "SELECT * FROM SSPEXCLUSIVEOFFERS";
            DataTable dt;
            if (SearchString != null && SearchString != "")
            {
                dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                    new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant()}
                });
            }
            else
            {
                dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { });
            }
            mEntity = MyClass.ConvertToList<QuoteOffersEntity>(dt);
            return mEntity;
        }

        public QuoteStatusCountEntity GetSavedOffersCount(string SearchString)
        {
            string mSearch = string.Empty;
            if (SearchString != null && SearchString != "")
            {
                SearchString = SearchString.ToUpperInvariant();
                mSearch = " WHERE (UPPER(ORIGINPORTNAME) LIKE '%' || :SearchString  || '%'  OR UPPER(DESTINATIONPORTNAME) LIKE '%' || :SearchString  || '%'  )";
            }
            QuoteStatusCountEntity mEntity = new QuoteStatusCountEntity();
            if (SearchString != null && SearchString != "")
            {
                ReadText(DBConnectionMode.SSP, "SELECT COUNT(TEMPLATENAME) AS NOMINATIONS FROM SSPEXCLUSIVEOFFERS" + mSearch,
                    new OracleParameter[] { new OracleParameter { ParameterName = "SearchString", Value = SearchString.ToUpperInvariant() } }
                , delegate(IDataReader r)
                {
                    mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
                });
            }
            else
            {
                ReadText(DBConnectionMode.SSP, "SELECT COUNT(TEMPLATENAME) AS NOMINATIONS FROM SSPEXCLUSIVEOFFERS" + mSearch,
                    new OracleParameter[] { }
                , delegate(IDataReader r)
                {
                    mEntity = DataReaderMapToObject<QuoteStatusCountEntity>(r);
                });
            }
            return mEntity;
        }

        public int GetQuotationIdCount(long id)
        {
            int quotationidcount = 0;
            quotationidcount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(QUOTATIONID) FROM QMQUOTATION WHERE QUOTATIONID =:QUOTATIONID",
                       new OracleParameter[]{
                        new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                    });
            return quotationidcount;
        }

        public QuotationEntity GetById(long id)
        {
            QuotationEntity mEntity = new QuotationEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QUOTETERMSANDCONDITIONID", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_USERS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_TEMPLATES", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_GET_QUOTEDETAILSBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<QuotationEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_QMSERVICECHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.QuotationChargeEntity> mQuotationCharges = MyClass.ConvertToList<DBFactory.Entities.QuotationChargeEntity>(dtQuotation);

                        //dtQuotation = new DataTable();
                        //OracleDataReader dtTermsandCondition = ((OracleRefCursor)mCommand.Parameters["CUR_QUOTETERMSANDCONDITIONID"].Value).GetDataReader();
                        //dtQuotation.Load(dtTermsandCondition);
                        //List<DBFactory.Entities.QuotationTermsConditionEntity> mTermsAndConditions = MyClass.ConvertToList<DBFactory.Entities.QuotationTermsConditionEntity>(dtQuotation);

                        List<DBFactory.Entities.QuotationTermsConditionEntity> mTermsAndConditions = new List<DBFactory.Entities.QuotationTermsConditionEntity>();
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Any cargo that is mis-declared at time of quote, the quote will become void and any costs / fines relating to the un-suitable nature of the cargo will be for the shipper's account.", QUOTETERMSANDCONDITIONID = 1 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Shipper has sole responsibility for any demurrage, inspection, storage, detention and/or third party pass-through charges or costs or costs resulting from a force majeure event.", QUOTETERMSANDCONDITIONID = 2 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Agility is not liable for any damage, theft, or pilferage and any claims for the same shall be shipper's responsibility to process with the carrier concerned.  Agility is not responsbile for any consequential damages/ liabilities caused by delayed/ damaged/ missing shipments.", QUOTETERMSANDCONDITIONID = 3 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "At Agility's discretion, interest may apply to any amounts which may become overdue for which payment has not been received.  Agility reserves the right to on charge any and all costs associated to the process of recovering any monies owed outside of agreed terms.", QUOTETERMSANDCONDITIONID = 4 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Transit times are subject to carrier delays, schedule changes, or upliftment and carriers reserve the right to transship goods.", QUOTETERMSANDCONDITIONID = 5 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "All goods are carried subject to the clauses of carriage as shown on the Airway Bill or Bill of Lading.", QUOTETERMSANDCONDITIONID = 6 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Subject to the terms and conditions herein, this quotation is valid for 14 days for ocean transport and valid for 7 days for air transport.", QUOTETERMSANDCONDITIONID = 7 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "All business is transacted subject to our standard trading terms and conditions.  For services not covered by the standard trading terms and conditions, the origin country standard industry terms shall apply (e.g. British International Forwarding Association [BIFA], Federation of Freight Forwarders' Association of India [FFFAI], etc...)", QUOTETERMSANDCONDITIONID = 8 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Quotation is based on current tariffs and present rates of exchange and are therefore subject to normal market fluctuations and without notice.", QUOTETERMSANDCONDITIONID = 9 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of any duties, taxes, or fees that may be applicable including but not limited to customs duties, agency or port fees, VAT, GST, federal, provincial, state and local taxes.", QUOTETERMSANDCONDITIONID = 10 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of any legalization, notarization, or other certified attestation required.", QUOTETERMSANDCONDITIONID = 11 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of any loading / offloading, special equipment required at the delivery site, or other incidental charges.  Any incidental charges that may arise at origin/ destination not mentioned in the quotation will be indvidually line-itemed at cost in the invoice.", QUOTETERMSANDCONDITIONID = 12 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of insurance.  Where permitted by local law and upon request, Agility can also provide a quotation for cargo insurance.", QUOTETERMSANDCONDITIONID = 13 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Quotation is subject to empty equipment availability, space availability, cargo acceptance, and confirmation at time of booking.", QUOTETERMSANDCONDITIONID = 14 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Quotation is subject to General Carrier Adjustment (GCA), dependent on market conditions, fuel surcharges, weight surcharges, fumigation charges, or other surcharges without prior notice as per carrier terms.", QUOTETERMSANDCONDITIONID = 15 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Final shipment price is based on the actual or cubic dimensional weight, whichever is greater as determined by the carrier. Any changes in the weight whether volumetric or actual will result in rate adjustments.", QUOTETERMSANDCONDITIONID = 16 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Final shipment price will be based on cargo receiving date, not estimated departure date.", QUOTETERMSANDCONDITIONID = 17 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Pick-up and/or delivery is based on regular service during normal business hours.  Additional requirements such as, but not limited to, tailgate delivery, appointment delivery, hand load/unload, inside delivery, rush delivery, will be subject to additional charges.", QUOTETERMSANDCONDITIONID = 18 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Shipper has the sole responsibility and shall comply with all applicable laws, government regulations, and/or registrations required of any country to, from, through or over which the goods may be carried, including those relating to the packing, carriage or delivery of the goods, and shall furnish such information and attach such documents to this air waybill as may be necessary to comply with such laws and regulations. Neither Agility or the carrier is not liable to the shipper for loss or expense due to the shipper’s failure to comply with this provision.", QUOTETERMSANDCONDITIONID = 19 });
                        mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Shipper has sole responsibility to ensure that the shipment is correctly labeled, goods are correctly declared/labled, and all necessary documentation provided, including but not limited to hazardous material declarations, licenses, and material data safety sheets. Any costs, fines, and/or fees relating to or resulting from the incorrect labeling or misdelcaration will be for the account of the shipper, including cost incurred by Agility.", QUOTETERMSANDCONDITIONID = 20 });
                        if (mEntity.QuoteSource == "WEB-OFFER")
                        {
                            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "This is a discount quote", QUOTETERMSANDCONDITIONID = 21 });
                            // mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = mEntity.SHIPMENTDESCRIPTION, QUOTETERMSANDCONDITIONID = 22 });

                        }
                        dtQuotation = new DataTable();
                        OracleDataReader dtUsers = ((OracleRefCursor)mCommand.Parameters["CUR_USERS"].Value).GetDataReader();
                        dtQuotation.Load(dtUsers);
                        List<DBFactory.Entities.UserNameEntity> mUsers = MyClass.ConvertToList<DBFactory.Entities.UserNameEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtTemplates = ((OracleRefCursor)mCommand.Parameters["CUR_TEMPLATES"].Value).GetDataReader();
                        dtQuotation.Load(dtTemplates);
                        List<DBFactory.Entities.QuotationTemplateEntity> mTemplates = MyClass.ConvertToList<DBFactory.Entities.QuotationTemplateEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtServiceCharge.Close();
                        // dtTermsandCondition.Close();
                        mConnection.Close();
                        dtUsers.Close();
                        dtTemplates.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.QuotationCharges = mQuotationCharges;
                        mEntity.TermAndConditions = mTermsAndConditions;
                        mEntity.Users = mUsers;
                        mEntity.QuotationTemplates = mTemplates;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }
        public string showhidetradefinancediv(int? QUOTATIONID)
        {
            string message = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "select INTERESTED FROM SSPTRADEFINANCE WHERE QUOTATIONID=:QUOTATIONID";
                        mCommand.Parameters.Add("QUOTATIONID", QUOTATIONID);
                        message = Convert.ToString(mCommand.ExecuteScalar());

                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }
            return message;
        }

        public QuotationEntity GetByTemplateId(long id)
        {
            QuotationEntity mEntity = new QuotationEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_TEMPLATES", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_GET_TEMPLATEDETAILSBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();

                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<QuotationEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtTemplates = ((OracleRefCursor)mCommand.Parameters["CUR_TEMPLATES"].Value).GetDataReader();
                        dtQuotation.Load(dtTemplates);
                        List<DBFactory.Entities.QuotationTemplateEntity> mTemplates = MyClass.ConvertToList<DBFactory.Entities.QuotationTemplateEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        mConnection.Close();
                        dtTemplates.Close();
                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.QuotationTemplates = mTemplates;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public QuotationEntity GetByIdforPreview(long id)
        {
            QuotationEntity mEntity = new QuotationEntity();
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", id);
                        mCommand.Parameters.Add("CUR_QMQUOTATION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSHIPMENTITEM", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QMSERVICECHARGE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_QUOTETERMSANDCONDITIONID", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_USERS", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_GET_QUOTEDETAILSBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        DataTable dtQuotation = new DataTable();
                        OracleDataReader Quotationreader = ((OracleRefCursor)mCommand.Parameters["CUR_QMQUOTATION"].Value).GetDataReader();
                        dtQuotation.Load(Quotationreader);
                        mEntity = MyClass.ConvertToEntity<QuotationEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtShipment = ((OracleRefCursor)mCommand.Parameters["CUR_QMSHIPMENTITEM"].Value).GetDataReader();
                        dtQuotation.Load(dtShipment);
                        List<DBFactory.Entities.QuotationShipmentEntity> mShipmentItems = MyClass.ConvertToList<DBFactory.Entities.QuotationShipmentEntity>(dtQuotation);

                        dtQuotation = new DataTable();
                        OracleDataReader dtServiceCharge = ((OracleRefCursor)mCommand.Parameters["CUR_QMSERVICECHARGE"].Value).GetDataReader();
                        dtQuotation.Load(dtServiceCharge);
                        List<DBFactory.Entities.QuotationChargeEntity> mQuotationCharges = MyClass.ConvertToList<DBFactory.Entities.QuotationChargeEntity>(dtQuotation);

                        //dtQuotation = new DataTable();
                        //OracleDataReader dtTermsandCondition = ((OracleRefCursor)mCommand.Parameters["CUR_QUOTETERMSANDCONDITIONID"].Value).GetDataReader();
                        //dtQuotation.Load(dtTermsandCondition);
                        //List<DBFactory.Entities.QuotationTermsConditionEntity> mTermsAndConditions = MyClass.ConvertToList<DBFactory.Entities.QuotationTermsConditionEntity>(dtQuotation);

                        dtQuotation = new DataTable();

                        OracleDataReader dtUsers = ((OracleRefCursor)mCommand.Parameters["CUR_USERS"].Value).GetDataReader();
                        dtQuotation.Load(dtUsers);
                        List<DBFactory.Entities.UserNameEntity> mUsers = MyClass.ConvertToList<DBFactory.Entities.UserNameEntity>(dtQuotation);

                        Quotationreader.Close();
                        dtShipment.Close();
                        dtServiceCharge.Close();
                        // dtTermsandCondition.Close();
                        mConnection.Close();
                        dtUsers.Close();

                        mEntity.ShipmentItems = mShipmentItems;
                        mEntity.QuotationCharges = mQuotationCharges;
                        // mEntity.TermAndConditions = mTermsAndConditions;
                        mEntity.Users = mUsers;
                        return mEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public void UpdateCustomCharges(long Quotationid, long id, bool IsChecked)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("L_QUOTATIONID", Quotationid);
                        mCommand.Parameters.Add("L_CHARGEID", id);
                        mCommand.Parameters.Add("L_ISCHECKED", IsChecked ? 1 : 0);
                        mCommand.CommandText = "USP_UPDATE_CUSTOMCHARGESBYID";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public int SaveQueryInformation(QueryInformationModel QueryModel)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("Query_Name", QueryModel.QueryName);
                        mCommand.Parameters.Add("Preferred_ContactMethod", QueryModel.PreferredContactMethod);
                        mCommand.Parameters.Add("Email_Id", QueryModel.EmailId);
                        mCommand.Parameters.Add("Contact_Number", QueryModel.ContactNumber);
                        mCommand.Parameters.Add("Company_Name", QueryModel.CompanyName);
                        mCommand.Parameters.Add("Query_Type", QueryModel.QueryType);
                        mCommand.Parameters.Add("Discription", QueryModel.Discription);
                        mCommand.Parameters.Add("HOWTOKNOW", QueryModel.Howtoknow);
                        mCommand.CommandText = "INSERTQUERYINFORMATION";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());

                    }
                }
            }
            return 1;
        }

        public DataTable Descriptionvalidation(Int64 OCountryId, Int64 DCountryId, string Description)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            DataTable dtMessage = new DataTable();
            using (mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("IP_ORG_COUNTRYID", OCountryId);
                        mCommand.Parameters.Add("IP_DEST_COUNTRYID", DCountryId);
                        mCommand.Parameters.Add("IP_ITEM_DESC", Description);
                        OracleParameter op = null;

                        op = new OracleParameter("OP_SUCCESS_CODE", OracleDbType.Long);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_SUCCESS_MSG", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_PROHIBITED_EXIST", OracleDbType.Long);
                        op.Direction = ParameterDirection.Output;
                        op.Size = 50000;
                        mCommand.Parameters.Add(op);

                        mCommand.Parameters.Add("OP_PROHIBITED_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_SSP_GET_PROHIBITED_ITEMS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        string PROHIBITED_EXIST = Convert.ToString(mCommand.Parameters["OP_PROHIBITED_EXIST"].Value);
                        if (PROHIBITED_EXIST == "1")
                        {
                            OracleDataReader PROHIBITED_DATA = ((OracleRefCursor)mCommand.Parameters["OP_PROHIBITED_DATA"].Value).GetDataReader();
                            dtMessage.Load(PROHIBITED_DATA);
                        }
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return dtMessage;
        }

        public long NoofQuotes(string userID)
        {
            int QuotationCount = 0;
            int IsQuoteBlock = 0;
            long quotelimit = QuoteLimit(userID);
            if (!userID.Contains("agility"))
            {
                IsQuoteBlock = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT ISQUOTEBLOCK FROM USERS WHERE LOWER(USERID) =:USERID",
                    new OracleParameter[]{
                        new OracleParameter{ ParameterName = "USERID",Value = userID.ToLower()},
                    });

                int PaymentCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(1) FROM JARECEIPTPAYMENTDETAILS WHERE LOWER(CREATEDBY)=:CREATEDBY AND UPPER(PAYMENTREFMESSAGE) = 'APPROVED' AND DATECREATED  > (SYSDATE - 90)",
                new OracleParameter[]{
                new OracleParameter{ ParameterName = "CREATEDBY",Value = userID.ToLower()},
                });

                if (PaymentCount == 0)
                {
                    QuotationCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(QUOTATIONID) FROM QMQUOTATION WHERE LOWER(CREATEDBY)=:CREATEDBY AND DATEOFENQUIRY > (SYSDATE - 1)",
                    new OracleParameter[]{                   
                    new OracleParameter{ ParameterName = "CREATEDBY",Value = userID.ToLower()},
                    });
                }
            }
            else
            {
                QuotationCount = 0;
            }
            if (QuotationCount >= quotelimit && IsQuoteBlock != 0)
            {
                this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE USERS SET ISQUOTEBLOCK = 1 WHERE LOWER(USERID)=:CREATEDBY",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "CREATEDBY",Value = userID.ToLower()},
                });
            }
            return QuotationCount;
        }

        public long QuoteLimit(string userID)
        {
            int QuotationLimit = 0;
            int IsQuoteBlock = 0;
            if (!userID.Contains("agility"))
            {
                IsQuoteBlock = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT ISQUOTEBLOCK FROM USERS WHERE LOWER(USERID) =:USERID",
                      new OracleParameter[]{
                        new OracleParameter{ ParameterName = "USERID",Value = userID.ToLower()},
                    });

                if (IsQuoteBlock == 0)
                {

                    QuotationLimit = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT QUOTELIMIT FROM USERS WHERE LOWER(USERID) =:USERID",
                            new OracleParameter[]{                   
                    new OracleParameter{ ParameterName = "CREATEDBY",Value = userID.ToLower()},
                    });
                }
                else
                {
                    QuotationLimit = 0;
                }
            }
            else
            {
                QuotationLimit = 1;
            }
            return QuotationLimit;
        }

        public long NoofQuotesForGuest(string userID)
        {

            int QuotationCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(QUOTATIONID) FROM QMQUOTATION WHERE GUESTEMAIL=:GUESTEMAIL AND DATEOFENQUIRY > (SYSDATE - 1)",
               new OracleParameter[]{                   
                    new OracleParameter{ ParameterName = "CREATEDBY",Value = userID},
                    });
            return QuotationCount;

        }

        public long Precatchmentarea(int OriginPlaceId, int DestinationPlaceId, string ProductTypeId, int OriginPortId, int DestinationPortId, int MovementTypeId, string OfferCode)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            long retPrecatchmentarea = 0;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("ORIGINPLACEID", OriginPlaceId);
                        mCommand.Parameters.Add("DESTINATIONPLACEID", DestinationPlaceId);
                        mCommand.Parameters.Add("PRODUCTTYPEID", ProductTypeId);
                        mCommand.Parameters.Add("ORIGINPORTID", OriginPortId);
                        mCommand.Parameters.Add("DESTINATIONPORTID", DestinationPortId);
                        mCommand.Parameters.Add("MOVEMENTTYPEID", MovementTypeId);
                        mCommand.Parameters.Add("OFFERCODE", OfferCode);
                        mCommand.Parameters.Add("PRM_RETURN", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_PRECATCHMENT_AREA";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["PRM_RETURN"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        reader1.Close();
                        mConnection.Close();
                        retPrecatchmentarea = Convert.ToInt32(dt.Rows[0]["RETURNDATA"]);
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return retPrecatchmentarea;
        }

        public string SaveTemplate(QuotationEntity entity, string TempalteName)
        {
            string message = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        mCommand.CommandText = "SELECT SSP_QMQUOTATIONTEMPLATE_SEQ.NEXTVAL FROM DUAL";
                        entity.QUOTATIONID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = "INSERT INTO QMQUOTATIONTEMPLATE (QUOTATIONID,QUOTATIONNUMBER, DATEOFENQUIRY, DATEOFSHIPMENT, PRODUCTID, PRODUCTTYPEID, MOVEMENTTYPEID, ORIGINPLACEID, ORIGINPORTID, DESTINATIONPLACEID, DESTINATIONPORTID, TOTALGROSSWEIGHT, TOTALCBM, DENSITYRATIO, CHARGEABLEWEIGHT, CHARGEABLEVOLUME, VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION, ISHAZARDOUSCARGO, CARGOVALUE, CARGOVALUECURRENCYID, INSUREDVALUE, INSUREDVALUECURRENCYID, CUSTOMERID, PREFERREDCURRENCYID, DATEMODIFIED, MODIFIEDBY, STATEID, PRODUCTNAME, PRODUCTTYPENAME, MOVEMENTTYPENAME, ORIGINPLACECODE, ORIGINPLACENAME, ORIGINPORTCODE, ORIGINPORTNAME, DESTINATIONPLACECODE, DESTINATIONPLACENAME, DESTINATIONPORTCODE, DESTINATIONPORTNAME,ISINSURANCEREQUIRED,CREATEDBY,GUESTCOMPANY,GUESTEMAIL,GUESTNAME,GUESTLASTNAME,ORIGINSUBDIVISIONCODE,DESTINATIONSUBDIVISIONCODE,ORIGINZIPCODE,DESTINATIONZIPCODE,OCOUNTRYCODE,DCOUNTRYCODE,HAZARDOUSGOODSTYPE,ISCUSTOMREQUIRED,ISOVERSIZECARGO ,QUOTESOURCE,ISPROHIBITEDCARGO,TEMPLATENAME,INCOTERMID,INCOTERMDESC,PACKAGETYPES,PARTYTYPE) VALUES (:QUOTATIONID, :QUOTATIONNUMBER, SYSDATE, SYSDATE, :PRODUCTID, :PRODUCTTYPEID, :MOVEMENTTYPEID, :ORIGINPLACEID, :ORIGINPORTID, :DESTINATIONPLACEID, :DESTINATIONPORTID, :TOTALGROSSWEIGHT, :TOTALCBM, :DENSITYRATIO, :CHARGEABLEWEIGHT, :CHARGEABLEVOLUME, :VOLUMETRICWEIGHT, :SHIPMENTDESCRIPTION, :ISHAZARDOUSCARGO, :CARGOVALUE, :CARGOVALUECURRENCYID, :INSUREDVALUE, :INSUREDVALUECURRENCYID, :CUSTOMERID, :PREFERREDCURRENCYID, SYSDATE, :MODIFIEDBY, :STATEID, :PRODUCTNAME, :PRODUCTTYPENAME, :MOVEMENTTYPENAME, :ORIGINPLACECODE, :ORIGINPLACENAME, :ORIGINPORTCODE, :ORIGINPORTNAME, :DESTINATIONPLACECODE, :DESTINATIONPLACENAME, :DESTINATIONPORTCODE, :DESTINATIONPORTNAME, :ISINSURANCEREQUIRED, :CREATEDBY, :GUESTCOMPANY, :GUESTEMAIL, :GUESTNAME, :GUESTLASTNAME, :ORIGINSUBDIVISIONCODE, :DESTINATIONSUBDIVISIONCODE, :ORIGINZIPCODE, :DESTINATIONZIPCODE, :OCOUNTRYCODE,:DCOUNTRYCODE,:HAZARDOUSGOODSTYPE,:ISCUSTOMREQUIRED,:ISOVERSIZECARGO,:QUOTESOURCE,:ISPROHIBITEDCARGO,:TEMPLATENAME,:INCOTERMID,:INCOTERMDESC,:PACKAGETYPES,:PARTYTYPE)";
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("QUOTATIONNUMBER", entity.QUOTATIONNUMBER);
                        mCommand.Parameters.Add("PRODUCTID", entity.PRODUCTID);
                        mCommand.Parameters.Add("PRODUCTTYPEID", entity.PRODUCTTYPEID);
                        mCommand.Parameters.Add("MOVEMENTTYPEID", entity.MOVEMENTTYPEID);
                        mCommand.Parameters.Add("ORIGINPLACEID", entity.ORIGINPLACEID);
                        mCommand.Parameters.Add("ORIGINPORTID", entity.ORIGINPORTID);
                        mCommand.Parameters.Add("DESTINATIONPLACEID", entity.DESTINATIONPLACEID);
                        mCommand.Parameters.Add("DESTINATIONPORTID", entity.DESTINATIONPORTID);
                        mCommand.Parameters.Add("TOTALGROSSWEIGHT", entity.TOTALGROSSWEIGHT);
                        mCommand.Parameters.Add("TOTALCBM", entity.TOTALCBM);
                        mCommand.Parameters.Add("DENSITYRATIO", entity.DENSITYRATIO);
                        mCommand.Parameters.Add("CHARGEABLEWEIGHT", entity.CHARGEABLEWEIGHT);
                        mCommand.Parameters.Add("CHARGEABLEVOLUME", entity.CHARGEABLEVOLUME);
                        mCommand.Parameters.Add("VOLUMETRICWEIGHT", entity.VOLUMETRICWEIGHT);
                        mCommand.Parameters.Add("SHIPMENTDESCRIPTION", entity.SHIPMENTDESCRIPTION);
                        mCommand.Parameters.Add("ISHAZARDOUSCARGO", entity.ISHAZARDOUSCARGO);
                        mCommand.Parameters.Add("CARGOVALUE", entity.CARGOVALUE);
                        mCommand.Parameters.Add("CARGOVALUECURRENCYID", entity.CARGOVALUECURRENCYID);
                        mCommand.Parameters.Add("INSUREDVALUE", entity.INSUREDVALUE);
                        mCommand.Parameters.Add("INSUREDVALUECURRENCYID", entity.INSUREDVALUECURRENCYID);
                        mCommand.Parameters.Add("CUSTOMERID", entity.CUSTOMERID);
                        mCommand.Parameters.Add("PREFERREDCURRENCYID", entity.PREFERREDCURRENCYID);
                        mCommand.Parameters.Add("MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("STATEID", entity.STATEID);
                        mCommand.Parameters.Add("PRODUCTNAME", entity.PRODUCTNAME);
                        mCommand.Parameters.Add("PRODUCTTYPENAME", entity.PRODUCTTYPENAME);
                        mCommand.Parameters.Add("MOVEMENTTYPENAME", entity.MOVEMENTTYPENAME);
                        mCommand.Parameters.Add("ORIGINPLACECODE", entity.ORIGINPLACECODE);
                        mCommand.Parameters.Add("ORIGINPLACENAME", entity.ORIGINPLACENAME);
                        mCommand.Parameters.Add("ORIGINPORTCODE", entity.ORIGINPORTCODE);
                        mCommand.Parameters.Add("ORIGINPORTNAME", entity.ORIGINPORTNAME);
                        mCommand.Parameters.Add("DESTINATIONPLACECODE", entity.DESTINATIONPLACECODE);
                        mCommand.Parameters.Add("DESTINATIONPLACENAME", entity.DESTINATIONPLACENAME);
                        mCommand.Parameters.Add("DESTINATIONPORTCODE", entity.DESTINATIONPORTCODE);
                        mCommand.Parameters.Add("DESTINATIONPORTNAME", entity.DESTINATIONPORTNAME);
                        mCommand.Parameters.Add("ISINSURANCEREQUIRED", entity.ISINSURANCEREQUIRED);
                        mCommand.Parameters.Add("CREATEDBY", entity.CREATEDBY.ToLowerInvariant());
                        mCommand.Parameters.Add("GUESTCOMPANY", entity.GUESTCOMPANY);
                        mCommand.Parameters.Add("GUESTEMAIL", entity.GUESTEMAIL);
                        mCommand.Parameters.Add("GUESTNAME", entity.GUESTNAME);
                        mCommand.Parameters.Add("GUESTLASTNAME", entity.GUESTLASTNAME);
                        mCommand.Parameters.Add("ORIGINSUBDIVISIONCODE", entity.ORIGINSUBDIVISIONCODE);
                        mCommand.Parameters.Add("DESTINATIONSUBDIVISIONCODE", entity.DESTINATIONSUBDIVISIONCODE);
                        mCommand.Parameters.Add("ORIGINZIPCODE", entity.ORIGINZIPCODE);
                        mCommand.Parameters.Add("DESTINATIONZIPCODE", entity.DESTINATIONZIPCODE);
                        mCommand.Parameters.Add("OCOUNTRYCODE", entity.OCOUNTRYCODE);
                        mCommand.Parameters.Add("DCOUNTRYCODE", entity.DCOUNTRYCODE);
                        mCommand.Parameters.Add("HAZARDOUSGOODSTYPE", entity.HAZARDOUSGOODSTYPE);
                        mCommand.Parameters.Add("ISCUSTOMREQUIRED", 1);
                        mCommand.Parameters.Add("ISOVERSIZECARGO", entity.ISOVERSIZECARGO);
                        mCommand.Parameters.Add("QUOTESOURCE", entity.QuoteSource);
                        mCommand.Parameters.Add("ISPROHIBITEDCARGO", entity.ISPROHIBITEDCARGO);
                        mCommand.Parameters.Add("TEMPLATENAME", TempalteName);
                        mCommand.Parameters.Add("INCOTERMID", entity.INCOTERMID);
                        mCommand.Parameters.Add("INCOTERMDESC", entity.INCOTERMDESC);
                        mCommand.Parameters.Add("PACKAGETYPES", entity.PackageTypes);
                        mCommand.Parameters.Add("PARTYTYPE", entity.PartyType);
                        mCommand.ExecuteNonQuery();

                        foreach (var entityItems in entity.ShipmentItems)
                        {
                            mCommand.Parameters.Clear();
                            mCommand.CommandText = "SELECT SSP_QMSHIPMENTITEMTEMPLATE_SEQ.NEXTVAL FROM DUAL";
                            entityItems.SHIPMENTITEMID = Convert.ToInt64(mCommand.ExecuteScalar());

                            mCommand.CommandText = "INSERT INTO QMSHIPMENTITEMTEMPLATE ( SHIPMENTITEMID,QUOTATIONID, ITEMDESCRIPTION, QUANTITY, PACKAGETYPEID, ITEMLENGTH, ITEMWIDTH, ITEMHEIGHT, LENGTHUOM, WEIGHTPERPIECE, WEIGHTTOTAL, WEIGHTUOM, CONTAINERID, PACKAGETYPECODE, PACKAGETYPENAME, LENGTHUOMCODE, LENGTHUOMNAME, WEIGHTUOMCODE, WEIGHTUOMNAME, TOTALCBM, CONTAINERNAME,CONTAINERCODE ) VALUES (:SHIPMENTITEMID, :QUOTATIONID, :ITEMDESCRIPTION, :QUANTITY, :PACKAGETYPEID, :ITEMLENGTH, :ITEMWIDTH, :ITEMHEIGHT, :LENGTHUOM, :WEIGHTPERPIECE, :WEIGHTTOTAL, :WEIGHTUOM, :CONTAINERID, :PACKAGETYPECODE, :PACKAGETYPENAME, :LENGTHUOMCODE, :LENGTHUOMNAME, :WEIGHTUOMCODE, :WEIGHTUOMNAME, :TOTALCBM, :CONTAINERNAME, :CONTAINERCODE )";

                            mCommand.Parameters.Add("SHIPMENTITEMID", entityItems.SHIPMENTITEMID);
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("ITEMDESCRIPTION", entityItems.ITEMDESCRIPTION);
                            mCommand.Parameters.Add("QUANTITY", entityItems.QUANTITY);
                            mCommand.Parameters.Add("PACKAGETYPEID", entityItems.PACKAGETYPEID);
                            mCommand.Parameters.Add("ITEMLENGTH", entityItems.ITEMLENGTH);
                            mCommand.Parameters.Add("ITEMWIDTH", entityItems.ITEMWIDTH);
                            mCommand.Parameters.Add("ITEMHEIGHT", entityItems.ITEMHEIGHT);
                            mCommand.Parameters.Add("LENGTHUOM", entityItems.LENGTHUOM);
                            mCommand.Parameters.Add("WEIGHTPERPIECE", entityItems.WEIGHTPERPIECE);
                            mCommand.Parameters.Add("WEIGHTTOTAL", entityItems.WEIGHTTOTAL);
                            mCommand.Parameters.Add("WEIGHTUOM", entityItems.WEIGHTUOM);
                            mCommand.Parameters.Add("CONTAINERID", entityItems.CONTAINERID);
                            mCommand.Parameters.Add("PACKAGETYPECODE", entityItems.PACKAGETYPECODE);
                            mCommand.Parameters.Add("PACKAGETYPENAME", entityItems.PACKAGETYPENAME);
                            mCommand.Parameters.Add("LENGTHUOMCODE", entityItems.LENGTHUOMCODE);
                            mCommand.Parameters.Add("LENGTHUOMNAME", entityItems.LENGTHUOMNAME);
                            mCommand.Parameters.Add("WEIGHTUOMCODE", entityItems.WEIGHTUOMCODE);
                            mCommand.Parameters.Add("WEIGHTUOMNAME", entityItems.WEIGHTUOMNAME);
                            mCommand.Parameters.Add("CONTAINERCODE", entityItems.CONTAINERCODE);
                            mCommand.Parameters.Add("TOTALCBM", entityItems.TOTALCBM);
                            mCommand.Parameters.Add("CONTAINERNAME", entityItems.CONTAINERNAME);
                            mCommand.ExecuteNonQuery();
                        }

                        mCommand.Parameters.Clear();
                        mOracleTransaction.Commit();
                        message = "Template Saved Successfully";

                    }
                    catch (Exception ex)
                    {
                        message = "Tempalte not saved";
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return message;
        }

        //public long Save(QuotationEntity entity, string HSCode)
        //{
        //    OracleConnection mConnection;
        //    OracleTransaction mOracleTransaction = null;
        //    long QUOTATIONID = 0;
        //    using (mConnection = new OracleConnection(SSPConnectionString))
        //    {
        //        if (mConnection.State == ConnectionState.Closed) mConnection.Open();
        //        mOracleTransaction = mConnection.BeginTransaction();
        //        using (OracleCommand mCommand = mConnection.CreateCommand())
        //        {
        //            mCommand.BindByName = true;
        //            mCommand.Transaction = mOracleTransaction;
        //            mCommand.CommandType = CommandType.Text;
        //            try
        //            {
        //                QUOTATIONID = InsertSave(entity, HSCode);
        //            }
        //            catch (Exception ex)
        //            {
        //                mOracleTransaction.Rollback();
        //                throw new ShipaDbException(ex.ToString());
        //            }
        //        }
        //    }
        //    return QUOTATIONID;
        //}

        public long InsertSave(QuotationEntity entity, string HSCode)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        int? DPROXIMITYTYPEID = 0;
                        if (entity.DESTINATIONZIPCODE != null && entity.DESTINATIONZIPCODE != string.Empty)
                            DPROXIMITYTYPEID = 105;
                        else
                            DPROXIMITYTYPEID = 103;

                        int? OPROXIMITYTYPEID = 0;
                        if (entity.ORIGINZIPCODE != null && entity.ORIGINZIPCODE != string.Empty)
                            OPROXIMITYTYPEID = 105;
                        else
                            OPROXIMITYTYPEID = 103;

                        if (entity.PRODUCTID == 2)
                        {
                            var date = DateTime.Now;
                            var DATEOFVALIDITY = date.AddDays(14);
                            entity.DATEOFVALIDITY = DATEOFVALIDITY;
                        }
                        else
                        {
                            var date = DateTime.Now;
                            var DATEOFVALIDITY = date.AddDays(7);
                            entity.DATEOFVALIDITY = DATEOFVALIDITY;
                        }

                        mCommand.CommandText = "SELECT SSP_QMQUOTATION_SEQ.NEXTVAL FROM DUAL";
                        entity.QUOTATIONID = Convert.ToInt64(mCommand.ExecuteScalar());
                        if (entity.ISHAZARDOUSCARGO == 1)
                        {
                            if (!string.IsNullOrEmpty(entity.QUOTATIONNUMBER))
                            {
                                if (entity.MSDSCERTIFICATEDOCID != 0)
                                {
                                    SSPDocumentsEntity mEntity = new SSPDocumentsEntity();
                                    ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE DOCID = :DOCID AND SOURCE='SHIPAQUOTE'",
                                        new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "DOCID", Value = entity.MSDSCERTIFICATEDOCID } }
                                    , delegate(IDataReader r)
                                    {
                                        mEntity = DataReaderMapToObject<SSPDocumentsEntity>(r);
                                    });

                                    if (mEntity != null)
                                    {
                                        if (mEntity.QUOTATIONID != null && mEntity.QUOTATIONID != 0)
                                        {
                                            mEntity.QUOTATIONID = entity.QUOTATIONID;

                                            Guid obj = Guid.NewGuid();
                                            string EnvironmentName = ServerEnvironment.GetEnvironmentNameDocs();

                                            long returneddocid = this.UploadMSDSCertificate(mEntity, "", EnvironmentName);
                                        }
                                        else
                                        {
                                            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE SSPDOCUMENTS SET QUOTATIONID = :QUOTATIONID WHERE DOCID = :DOCID",
                                   new OracleParameter[]{
                                 new OracleParameter{ ParameterName = "QUOTATIONID",Value = entity.QUOTATIONID},
                               new OracleParameter{ ParameterName = "DOCID",Value = entity.MSDSCERTIFICATEDOCID},
                            });
                                        }
                                    }
                                }
                                else
                                {
                                    SSPDocumentsEntity mEntity = new SSPDocumentsEntity();
                                    mEntity = this.GetMsdsdocdetails(entity.QUOTATIONNUMBER);
                                    if (Convert.ToBoolean(mEntity.FILECONTENT))
                                    {
                                        mEntity.QUOTATIONID = entity.QUOTATIONID;
                                        Guid obj = Guid.NewGuid();
                                        string EnvironmentName = ServerEnvironment.GetEnvironmentNameDocs();
                                        long returneddocid = this.UploadMSDSCertificate(mEntity, "", EnvironmentName);
                                    }
                                }
                            }
                            else
                            {
                                if (entity.ISHAZARDOUSCARGO == 1)
                                {
                                    if (entity.MSDSCERTIFICATEDOCID != 0)
                                    {
                                        this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE SSPDOCUMENTS SET QUOTATIONID = :QUOTATIONID WHERE DOCID = :DOCID",
                               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = entity.QUOTATIONID},
                    new OracleParameter{ ParameterName = "DOCID",Value = entity.MSDSCERTIFICATEDOCID},
                });
                                    }
                                }
                            }
                        }
                        //"INSERT INTO QMQUOTATION (QUOTATIONID,QUOTATIONNUMBER, DATEOFENQUIRY, DATEOFVALIDITY, DATEOFSHIPMENT, PRODUCTID, PRODUCTTYPEID, MOVEMENTTYPEID, ORIGINPLACEID, ORIGINPORTID, DESTINATIONPLACEID, DESTINATIONPORTID, TOTALGROSSWEIGHT, TOTALCBM, DENSITYRATIO, CHARGEABLEWEIGHT, CHARGEABLEVOLUME, VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION, ISHAZARDOUSCARGO, CARGOVALUE, CARGOVALUECURRENCYID, INSUREDVALUE, INSUREDVALUECURRENCYID, CUSTOMERID, PREFERREDCURRENCYID, DATEMODIFIED, MODIFIEDBY, STATEID, PRODUCTNAME, PRODUCTTYPENAME, MOVEMENTTYPENAME, ORIGINPLACECODE, ORIGINPLACENAME, ORIGINPORTCODE, ORIGINPORTNAME, DESTINATIONPLACECODE, DESTINATIONPLACENAME, DESTINATIONPORTCODE, DESTINATIONPORTNAME,ISINSURANCEREQUIRED,CREATEDBY,GUESTCOMPANY,GUESTEMAIL,GUESTNAME,GUESTLASTNAME,ORIGINSUBDIVISIONCODE,DESTINATIONSUBDIVISIONCODE,ORIGINZIPCODE,DESTINATIONZIPCODE,OPROXIMITYTYPEID,DPROXIMITYTYPEID,OCOUNTRYCODE,DCOUNTRYCODE,HAZARDOUSGOODSTYPE,ISCUSTOMREQUIRED,ISOVERSIZECARGO ,QUOTESOURCE,ISPROHIBITEDCARGO,ISEVENTCARGO,INCOTERMID,INCOTERMDESC,PACKAGETYPES,AMAZONSUPPLIERID,PALLETIZINGBY,LABELLINGBY,AMAZONFCCODE,QUOTETYPE,NOTIFICATIONSUBSCRIPTION,GUESTCOUNTRYCODE,USEOFQUOTE,SHIPMENTPROCESS) VALUES (:QUOTATIONID, :QUOTATIONNUMBER, SYSDATE, :DATEOFVALIDITY, SYSDATE, :PRODUCTID, :PRODUCTTYPEID, :MOVEMENTTYPEID, :ORIGINPLACEID, :ORIGINPORTID, :DESTINATIONPLACEID, :DESTINATIONPORTID, :TOTALGROSSWEIGHT, :TOTALCBM, :DENSITYRATIO, :CHARGEABLEWEIGHT, :CHARGEABLEVOLUME, :VOLUMETRICWEIGHT, :SHIPMENTDESCRIPTION, :ISHAZARDOUSCARGO, :CARGOVALUE, :CARGOVALUECURRENCYID, :INSUREDVALUE, :INSUREDVALUECURRENCYID, :CUSTOMERID, :PREFERREDCURRENCYID, SYSDATE, :MODIFIEDBY, :STATEID, :PRODUCTNAME, :PRODUCTTYPENAME, :MOVEMENTTYPENAME, :ORIGINPLACECODE, :ORIGINPLACENAME, :ORIGINPORTCODE, :ORIGINPORTNAME, :DESTINATIONPLACECODE, :DESTINATIONPLACENAME, :DESTINATIONPORTCODE, :DESTINATIONPORTNAME, :ISINSURANCEREQUIRED, :CREATEDBY, :GUESTCOMPANY, :GUESTEMAIL, :GUESTNAME, :GUESTLASTNAME, :ORIGINSUBDIVISIONCODE, :DESTINATIONSUBDIVISIONCODE, :ORIGINZIPCODE, :DESTINATIONZIPCODE, :OPROXIMITYTYPEID, :DPROXIMITYTYPEID,:OCOUNTRYCODE,:DCOUNTRYCODE,:HAZARDOUSGOODSTYPE,:ISCUSTOMREQUIRED,:ISOVERSIZECARGO,:QUOTESOURCE,:ISPROHIBITEDCARGO,:ISEVENTCARGO,:INCOTERMID,:INCOTERMDESC,:PACKAGETYPES,:AMAZONSUPPLIERID,:PALLETIZINGBY,:LABELLINGBY,:AMAZONFCCODE,:QUOTETYPE,:NOTIFICATIONSUBSCRIPTION,:GUESTCOUNTRYCODE,:USEOFQUOTE,:SHIPMENTPROCESS)";
                        mCommand.Parameters.Clear();
                        mCommand.CommandText = "SHIPA_CREATEQUOTE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("Q_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.Parameters.Add("Q_QUOTATIONNUMBER", entity.QUOTATIONNUMBER);
                        mCommand.Parameters.Add("Q_DATEOFVALIDITY", entity.DATEOFVALIDITY);
                        mCommand.Parameters.Add("Q_PRODUCTID", entity.PRODUCTID);
                        mCommand.Parameters.Add("Q_PRODUCTTYPEID", entity.PRODUCTTYPEID);
                        mCommand.Parameters.Add("Q_MOVEMENTTYPEID", entity.MOVEMENTTYPEID);
                        mCommand.Parameters.Add("Q_ORIGINPLACEID", entity.ORIGINPLACEID);
                        mCommand.Parameters.Add("Q_ORIGINPORTID", entity.ORIGINPORTID);
                        mCommand.Parameters.Add("Q_DESTINATIONPLACEID", entity.DESTINATIONPLACEID);
                        mCommand.Parameters.Add("Q_DESTINATIONPORTID", entity.DESTINATIONPORTID);
                        mCommand.Parameters.Add("Q_TOTALGROSSWEIGHT", entity.TOTALGROSSWEIGHT);
                        mCommand.Parameters.Add("Q_TOTALCBM", entity.TOTALCBM);
                        mCommand.Parameters.Add("Q_DENSITYRATIO", entity.DENSITYRATIO);
                        mCommand.Parameters.Add("Q_CHARGEABLEWEIGHT", entity.CHARGEABLEWEIGHT);
                        mCommand.Parameters.Add("Q_CHARGEABLEVOLUME", entity.CHARGEABLEVOLUME);
                        mCommand.Parameters.Add("Q_VOLUMETRICWEIGHT", entity.VOLUMETRICWEIGHT);
                        mCommand.Parameters.Add("Q_SHIPMENTDESCRIPTION", entity.SHIPMENTDESCRIPTION);
                        mCommand.Parameters.Add("Q_ISHAZARDOUSCARGO", entity.ISHAZARDOUSCARGO);
                        mCommand.Parameters.Add("Q_ISEVENTCARGO", entity.ISEVENTCARGO);
                        mCommand.Parameters.Add("Q_CARGOVALUE", entity.CARGOVALUE);
                        mCommand.Parameters.Add("Q_CARGOVALUECURRENCYID", entity.CARGOVALUECURRENCYID);
                        mCommand.Parameters.Add("Q_INSUREDVALUE", entity.INSUREDVALUE);
                        mCommand.Parameters.Add("Q_INSUREDVALUECURRENCYID", entity.INSUREDVALUECURRENCYID);
                        mCommand.Parameters.Add("Q_CUSTOMERID", entity.CUSTOMERID);
                        mCommand.Parameters.Add("Q_PREFERREDCURRENCYID", entity.PREFERREDCURRENCYID);
                        mCommand.Parameters.Add("Q_MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("Q_STATEID", entity.STATEID);
                        mCommand.Parameters.Add("Q_PRODUCTNAME", entity.PRODUCTNAME);
                        mCommand.Parameters.Add("Q_PRODUCTTYPENAME", entity.PRODUCTTYPENAME);
                        mCommand.Parameters.Add("Q_MOVEMENTTYPENAME", entity.MOVEMENTTYPENAME);
                        mCommand.Parameters.Add("Q_ORIGINPLACECODE", entity.ORIGINPLACECODE);
                        mCommand.Parameters.Add("Q_ORIGINPLACENAME", entity.ORIGINPLACENAME);
                        mCommand.Parameters.Add("Q_ORIGINPORTCODE", entity.ORIGINPORTCODE);
                        mCommand.Parameters.Add("Q_ORIGINPORTNAME", entity.ORIGINPORTNAME);
                        mCommand.Parameters.Add("Q_DESTINATIONPLACECODE", entity.DESTINATIONPLACECODE);
                        mCommand.Parameters.Add("Q_DESTINATIONPLACENAME", entity.DESTINATIONPLACENAME);
                        mCommand.Parameters.Add("Q_DESTINATIONPORTCODE", entity.DESTINATIONPORTCODE);
                        mCommand.Parameters.Add("Q_DESTINATIONPORTNAME", entity.DESTINATIONPORTNAME);
                        mCommand.Parameters.Add("Q_ISINSURANCEREQUIRED", entity.ISINSURANCEREQUIRED);
                        mCommand.Parameters.Add("Q_CREATEDBY", entity.CREATEDBY.ToLowerInvariant());
                        mCommand.Parameters.Add("Q_GUESTCOMPANY", entity.GUESTCOMPANY);
                        mCommand.Parameters.Add("Q_GUESTEMAIL", entity.GUESTEMAIL);
                        mCommand.Parameters.Add("Q_GUESTNAME", entity.GUESTNAME);
                        mCommand.Parameters.Add("Q_GUESTLASTNAME", entity.GUESTLASTNAME);
                        mCommand.Parameters.Add("Q_GUESTCOUNTRYNAME", entity.GUESTCOUNTRYNAME);
                        mCommand.Parameters.Add("Q_ORIGINSUBDIVISIONCODE", entity.ORIGINSUBDIVISIONCODE);
                        mCommand.Parameters.Add("Q_DESTINATIONSUBDIVISIONCODE", entity.DESTINATIONSUBDIVISIONCODE);
                        mCommand.Parameters.Add("Q_ORIGINZIPCODE", entity.ORIGINZIPCODE);
                        mCommand.Parameters.Add("Q_DESTINATIONZIPCODE", entity.DESTINATIONZIPCODE);
                        mCommand.Parameters.Add("Q_OPROXIMITYTYPEID", OPROXIMITYTYPEID);
                        mCommand.Parameters.Add("Q_DPROXIMITYTYPEID", DPROXIMITYTYPEID);
                        mCommand.Parameters.Add("Q_OCOUNTRYCODE", entity.OCOUNTRYCODE);
                        mCommand.Parameters.Add("Q_DCOUNTRYCODE", entity.DCOUNTRYCODE);
                        mCommand.Parameters.Add("Q_HAZARDOUSGOODSTYPE", entity.HAZARDOUSGOODSTYPE);
                        mCommand.Parameters.Add("Q_ISCUSTOMREQUIRED", 1);
                        mCommand.Parameters.Add("Q_ISOVERSIZECARGO", entity.ISOVERSIZECARGO);
                        mCommand.Parameters.Add("Q_QUOTESOURCE", entity.QuoteSource);
                        mCommand.Parameters.Add("Q_ISPROHIBITEDCARGO", entity.ISPROHIBITEDCARGO);
                        mCommand.Parameters.Add("Q_INCOTERMID", entity.INCOTERMID);
                        mCommand.Parameters.Add("Q_INCOTERMDESC", entity.INCOTERMDESC);
                        mCommand.Parameters.Add("Q_PACKAGETYPES", entity.PackageTypes);
                        mCommand.Parameters.Add("Q_AMAZONSUPPLIERID", entity.AmazonSupplierID);
                        mCommand.Parameters.Add("Q_PALLETIZINGBY", entity.PALLETIZINGBY);
                        mCommand.Parameters.Add("Q_LABELLINGBY", entity.LABELLINGBY);
                        mCommand.Parameters.Add("Q_QUOTETYPE", entity.QUOTETYPE);
                        mCommand.Parameters.Add("Q_AMAZONFCCODE", entity.AMAZONFCCODE);
                        mCommand.Parameters.Add("Q_NOTIFICATIONSUBSCRIPTION", entity.NOTIFICATIONSUBSCRIPTION);
                        mCommand.Parameters.Add("Q_GUESTCOUNTRYCODE", entity.GUESTCOUNTRYCODE);
                        mCommand.Parameters.Add("Q_USEOFQUOTE", entity.USEOFQUOTE);
                        mCommand.Parameters.Add("Q_SHIPMENTPROCESS", entity.SHIPMENTPROCESS);
                        mCommand.Parameters.Add("Q_ISOFFERAPPLICABLE", entity.ISOFFERAPPLICABLE);
                        mCommand.Parameters.Add("Q_OFFERCODE", entity.OFFERCODE);
                        mCommand.Parameters.Add("Q_PARTYTYPE", entity.PartyType);
                        mCommand.ExecuteNonQuery();

                        //INSERT INTO QMSHIPMENTITEM ( QUOTATIONID, ITEMDESCRIPTION, QUANTITY, PACKAGETYPEID, ITEMLENGTH, ITEMWIDTH, ITEMHEIGHT, LENGTHUOM, WEIGHTPERPIECE, WEIGHTTOTAL, WEIGHTUOM, CONTAINERID, PACKAGETYPECODE, PACKAGETYPENAME, LENGTHUOMCODE, LENGTHUOMNAME, WEIGHTUOMCODE, WEIGHTUOMNAME, TOTALCBM, CONTAINERNAME,CONTAINERCODE ) VALUES (:QUOTATIONID, :ITEMDESCRIPTION, :QUANTITY, :PACKAGETYPEID, :ITEMLENGTH, :ITEMWIDTH, :ITEMHEIGHT, :LENGTHUOM, :WEIGHTPERPIECE, :WEIGHTTOTAL, :WEIGHTUOM, :CONTAINERID, :PACKAGETYPECODE, :PACKAGETYPENAME, :LENGTHUOMCODE, :LENGTHUOMNAME, :WEIGHTUOMCODE, :WEIGHTUOMNAME, :TOTALCBM, :CONTAINERNAME, :CONTAINERCODE );

                        mCommand.CommandText = "SHIPA_CREATE_QSHIPMENTS";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        foreach (var entityItems in entity.ShipmentItems)
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("V_QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("V_ITEMDESCRIPTION", entityItems.ITEMDESCRIPTION);
                            mCommand.Parameters.Add("V_QUANTITY", entityItems.QUANTITY);
                            mCommand.Parameters.Add("V_PACKAGETYPEID", entityItems.PACKAGETYPEID);
                            mCommand.Parameters.Add("V_ITEMLENGTH", entityItems.ITEMLENGTH);
                            mCommand.Parameters.Add("V_ITEMWIDTH", entityItems.ITEMWIDTH);
                            mCommand.Parameters.Add("V_ITEMHEIGHT", entityItems.ITEMHEIGHT);
                            mCommand.Parameters.Add("V_LENGTHUOM", entityItems.LENGTHUOM);
                            mCommand.Parameters.Add("V_WEIGHTPERPIECE", entityItems.WEIGHTPERPIECE);
                            mCommand.Parameters.Add("V_WEIGHTTOTAL", entityItems.WEIGHTTOTAL);
                            mCommand.Parameters.Add("V_WEIGHTUOM", entityItems.WEIGHTUOM);
                            mCommand.Parameters.Add("V_CONTAINERID", entityItems.CONTAINERID);
                            mCommand.Parameters.Add("V_PACKAGETYPECODE", entityItems.PACKAGETYPECODE);
                            mCommand.Parameters.Add("V_PACKAGETYPENAME", entityItems.PACKAGETYPENAME);
                            mCommand.Parameters.Add("V_LENGTHUOMCODE", entityItems.LENGTHUOMCODE);
                            mCommand.Parameters.Add("V_LENGTHUOMNAME", entityItems.LENGTHUOMNAME);
                            mCommand.Parameters.Add("V_WEIGHTUOMCODE", entityItems.WEIGHTUOMCODE);
                            mCommand.Parameters.Add("V_WEIGHTUOMNAME", entityItems.WEIGHTUOMNAME);
                            mCommand.Parameters.Add("V_CONTAINERCODE", entityItems.CONTAINERCODE);
                            mCommand.Parameters.Add("V_TOTALCBM", entityItems.TOTALCBM);
                            mCommand.Parameters.Add("V_CONTAINERNAME", entityItems.CONTAINERNAME);
                            mCommand.ExecuteNonQuery();
                        }

                        //if (entity.ISOFFERAPPLICABLE == 1 && entity.OFFERCODE != null)
                        //{
                        //    mCommand.CommandText = "INSERT INTO SSPOFFERQUOTES (QUOTATIONID, ISOFFERAPPLICABLE,OFFERCODE ) VALUES (:QUOTATIONID,:ISOFFERAPPLICABLE, :OFFERCODE)";
                        //    mCommand.Parameters.Clear();
                        //    mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        //    mCommand.Parameters.Add("ISOFFERAPPLICABLE", entity.ISOFFERAPPLICABLE);
                        //    mCommand.Parameters.Add("OFFERCODE", entity.OFFERCODE.ToUpperInvariant());
                        //    mCommand.ExecuteNonQuery();
                        //}

                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.CommandText = "USP_GET_QUOTE";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return entity.QUOTATIONID;
        }

        public void ErrorSave(QuotationEntity entity, string HSCode)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        int? DPROXIMITYTYPEID = 0;
                        if (entity.DESTINATIONZIPCODE != null)
                            DPROXIMITYTYPEID = 105;
                        else
                            DPROXIMITYTYPEID = 103;

                        int? OPROXIMITYTYPEID = 0;
                        if (entity.ORIGINZIPCODE != null)
                            OPROXIMITYTYPEID = 105;
                        else
                            OPROXIMITYTYPEID = 103;

                        mCommand.CommandText = "INSERT INTO QMERRORQUOTATION (QUOTATIONID,QUOTATIONNUMBER, DATEOFENQUIRY, DATEOFVALIDITY, DATEOFSHIPMENT, PRODUCTID, PRODUCTTYPEID, MOVEMENTTYPEID, ORIGINPLACEID, ORIGINPORTID, DESTINATIONPLACEID, DESTINATIONPORTID, TOTALGROSSWEIGHT, TOTALCBM, DENSITYRATIO, CHARGEABLEWEIGHT, CHARGEABLEVOLUME, VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION, ISHAZARDOUSCARGO, CARGOVALUE, CARGOVALUECURRENCYID, INSUREDVALUE, INSUREDVALUECURRENCYID, CUSTOMERID, PREFERREDCURRENCYID, DATEMODIFIED, MODIFIEDBY, STATEID, PRODUCTNAME, PRODUCTTYPENAME, MOVEMENTTYPENAME, ORIGINPLACECODE, ORIGINPLACENAME, ORIGINPORTCODE, ORIGINPORTNAME, DESTINATIONPLACECODE, DESTINATIONPLACENAME, DESTINATIONPORTCODE, DESTINATIONPORTNAME,ISINSURANCEREQUIRED,CREATEDBY,GUESTCOMPANY,GUESTEMAIL,GUESTNAME,GUESTLASTNAME,ORIGINSUBDIVISIONCODE,DESTINATIONSUBDIVISIONCODE,ORIGINZIPCODE,DESTINATIONZIPCODE,OPROXIMITYTYPEID,DPROXIMITYTYPEID,OCOUNTRYCODE,DCOUNTRYCODE,HAZARDOUSGOODSTYPE,ISCUSTOMREQUIRED,ISOVERSIZECARGO ,QUOTESOURCE,ISPROHIBITEDCARGO) VALUES (:QUOTATIONID, :QUOTATIONNUMBER, SYSDATE, :DATEOFVALIDITY, SYSDATE, :PRODUCTID, :PRODUCTTYPEID, :MOVEMENTTYPEID, :ORIGINPLACEID, :ORIGINPORTID, :DESTINATIONPLACEID, :DESTINATIONPORTID, :TOTALGROSSWEIGHT, :TOTALCBM, :DENSITYRATIO, :CHARGEABLEWEIGHT, :CHARGEABLEVOLUME, :VOLUMETRICWEIGHT, :SHIPMENTDESCRIPTION, :ISHAZARDOUSCARGO, :CARGOVALUE, :CARGOVALUECURRENCYID, :INSUREDVALUE, :INSUREDVALUECURRENCYID, :CUSTOMERID, :PREFERREDCURRENCYID, SYSDATE, :MODIFIEDBY, :STATEID, :PRODUCTNAME, :PRODUCTTYPENAME, :MOVEMENTTYPENAME, :ORIGINPLACECODE, :ORIGINPLACENAME, :ORIGINPORTCODE, :ORIGINPORTNAME, :DESTINATIONPLACECODE, :DESTINATIONPLACENAME, :DESTINATIONPORTCODE, :DESTINATIONPORTNAME, :ISINSURANCEREQUIRED, :CREATEDBY, :GUESTCOMPANY, :GUESTEMAIL, :GUESTNAME, :GUESTLASTNAME, :ORIGINSUBDIVISIONCODE, :DESTINATIONSUBDIVISIONCODE, :ORIGINZIPCODE, :DESTINATIONZIPCODE, :OPROXIMITYTYPEID, :DPROXIMITYTYPEID,:OCOUNTRYCODE,:DCOUNTRYCODE,:HAZARDOUSGOODSTYPE,:ISCUSTOMREQUIRED,:ISOVERSIZECARGO,:QUOTESOURCE,:ISPROHIBITEDCARGO)";
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("QUOTATIONNUMBER", entity.QUOTATIONNUMBER);
                        mCommand.Parameters.Add("DATEOFVALIDITY", entity.DATEOFVALIDITY);
                        mCommand.Parameters.Add("PRODUCTID", entity.PRODUCTID);
                        mCommand.Parameters.Add("PRODUCTTYPEID", entity.PRODUCTTYPEID);
                        mCommand.Parameters.Add("MOVEMENTTYPEID", entity.MOVEMENTTYPEID);
                        mCommand.Parameters.Add("ORIGINPLACEID", entity.ORIGINPLACEID);
                        mCommand.Parameters.Add("ORIGINPORTID", entity.ORIGINPORTID);
                        mCommand.Parameters.Add("DESTINATIONPLACEID", entity.DESTINATIONPLACEID);
                        mCommand.Parameters.Add("DESTINATIONPORTID", entity.DESTINATIONPORTID);
                        mCommand.Parameters.Add("TOTALGROSSWEIGHT", entity.TOTALGROSSWEIGHT);
                        mCommand.Parameters.Add("TOTALCBM", entity.TOTALCBM);
                        mCommand.Parameters.Add("DENSITYRATIO", entity.DENSITYRATIO);
                        mCommand.Parameters.Add("CHARGEABLEWEIGHT", entity.CHARGEABLEWEIGHT);
                        mCommand.Parameters.Add("CHARGEABLEVOLUME", entity.CHARGEABLEVOLUME);
                        mCommand.Parameters.Add("VOLUMETRICWEIGHT", entity.VOLUMETRICWEIGHT);
                        mCommand.Parameters.Add("SHIPMENTDESCRIPTION", entity.SHIPMENTDESCRIPTION);
                        mCommand.Parameters.Add("ISHAZARDOUSCARGO", entity.ISHAZARDOUSCARGO);
                        mCommand.Parameters.Add("CARGOVALUE", entity.CARGOVALUE);
                        mCommand.Parameters.Add("CARGOVALUECURRENCYID", entity.CARGOVALUECURRENCYID);
                        mCommand.Parameters.Add("INSUREDVALUE", entity.INSUREDVALUE);
                        mCommand.Parameters.Add("INSUREDVALUECURRENCYID", entity.INSUREDVALUECURRENCYID);
                        mCommand.Parameters.Add("CUSTOMERID", entity.CUSTOMERID);
                        mCommand.Parameters.Add("PREFERREDCURRENCYID", entity.PREFERREDCURRENCYID);
                        mCommand.Parameters.Add("MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("STATEID", entity.STATEID);
                        mCommand.Parameters.Add("PRODUCTNAME", entity.PRODUCTNAME);
                        mCommand.Parameters.Add("PRODUCTTYPENAME", entity.PRODUCTTYPENAME);
                        mCommand.Parameters.Add("MOVEMENTTYPENAME", entity.MOVEMENTTYPENAME);
                        mCommand.Parameters.Add("ORIGINPLACECODE", entity.ORIGINPLACECODE);
                        mCommand.Parameters.Add("ORIGINPLACENAME", entity.ORIGINPLACENAME);
                        mCommand.Parameters.Add("ORIGINPORTCODE", entity.ORIGINPORTCODE);
                        mCommand.Parameters.Add("ORIGINPORTNAME", entity.ORIGINPORTNAME);
                        mCommand.Parameters.Add("DESTINATIONPLACECODE", entity.DESTINATIONPLACECODE);
                        mCommand.Parameters.Add("DESTINATIONPLACENAME", entity.DESTINATIONPLACENAME);
                        mCommand.Parameters.Add("DESTINATIONPORTCODE", entity.DESTINATIONPORTCODE);
                        mCommand.Parameters.Add("DESTINATIONPORTNAME", entity.DESTINATIONPORTNAME);
                        mCommand.Parameters.Add("ISINSURANCEREQUIRED", entity.ISINSURANCEREQUIRED);
                        mCommand.Parameters.Add("CREATEDBY", entity.CREATEDBY.ToLowerInvariant());
                        mCommand.Parameters.Add("GUESTCOMPANY", entity.GUESTCOMPANY);
                        mCommand.Parameters.Add("GUESTEMAIL", entity.GUESTEMAIL);
                        mCommand.Parameters.Add("GUESTNAME", entity.GUESTNAME);
                        mCommand.Parameters.Add("GUESTLASTNAME", entity.GUESTLASTNAME);
                        mCommand.Parameters.Add("ORIGINSUBDIVISIONCODE", entity.ORIGINSUBDIVISIONCODE);
                        mCommand.Parameters.Add("DESTINATIONSUBDIVISIONCODE", entity.DESTINATIONSUBDIVISIONCODE);
                        mCommand.Parameters.Add("ORIGINZIPCODE", entity.ORIGINZIPCODE);
                        mCommand.Parameters.Add("DESTINATIONZIPCODE", entity.DESTINATIONZIPCODE);
                        mCommand.Parameters.Add("OPROXIMITYTYPEID", OPROXIMITYTYPEID);
                        mCommand.Parameters.Add("DPROXIMITYTYPEID", DPROXIMITYTYPEID);
                        mCommand.Parameters.Add("OCOUNTRYCODE", entity.OCOUNTRYCODE);
                        mCommand.Parameters.Add("DCOUNTRYCODE", entity.DCOUNTRYCODE);
                        mCommand.Parameters.Add("HAZARDOUSGOODSTYPE", entity.HAZARDOUSGOODSTYPE);
                        mCommand.Parameters.Add("ISCUSTOMREQUIRED", 1);
                        mCommand.Parameters.Add("ISOVERSIZECARGO", entity.ISOVERSIZECARGO);
                        mCommand.Parameters.Add("QUOTESOURCE", entity.QuoteSource);
                        mCommand.Parameters.Add("ISPROHIBITEDCARGO", entity.ISPROHIBITEDCARGO);
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO QMERRORSHIPMENTITEM ( QUOTATIONID, ITEMDESCRIPTION, QUANTITY, PACKAGETYPEID, ITEMLENGTH, ITEMWIDTH, ITEMHEIGHT, LENGTHUOM, WEIGHTPERPIECE, WEIGHTTOTAL, WEIGHTUOM, CONTAINERID, PACKAGETYPECODE, PACKAGETYPENAME, LENGTHUOMCODE, LENGTHUOMNAME, WEIGHTUOMCODE, WEIGHTUOMNAME, TOTALCBM, CONTAINERNAME,CONTAINERCODE ) VALUES (:QUOTATIONID, :ITEMDESCRIPTION, :QUANTITY, :PACKAGETYPEID, :ITEMLENGTH, :ITEMWIDTH, :ITEMHEIGHT, :LENGTHUOM, :WEIGHTPERPIECE, :WEIGHTTOTAL, :WEIGHTUOM, :CONTAINERID, :PACKAGETYPECODE, :PACKAGETYPENAME, :LENGTHUOMCODE, :LENGTHUOMNAME, :WEIGHTUOMCODE, :WEIGHTUOMNAME, :TOTALCBM, :CONTAINERNAME, :CONTAINERCODE )";
                        foreach (var entityItems in entity.ShipmentItems)
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("ITEMDESCRIPTION", entityItems.ITEMDESCRIPTION);
                            mCommand.Parameters.Add("QUANTITY", entityItems.QUANTITY);
                            mCommand.Parameters.Add("PACKAGETYPEID", entityItems.PACKAGETYPEID);
                            mCommand.Parameters.Add("ITEMLENGTH", entityItems.ITEMLENGTH);
                            mCommand.Parameters.Add("ITEMWIDTH", entityItems.ITEMWIDTH);
                            mCommand.Parameters.Add("ITEMHEIGHT", entityItems.ITEMHEIGHT);
                            mCommand.Parameters.Add("LENGTHUOM", entityItems.LENGTHUOM);
                            mCommand.Parameters.Add("WEIGHTPERPIECE", entityItems.WEIGHTPERPIECE);
                            mCommand.Parameters.Add("WEIGHTTOTAL", entityItems.WEIGHTTOTAL);
                            mCommand.Parameters.Add("WEIGHTUOM", entityItems.WEIGHTUOM);
                            mCommand.Parameters.Add("CONTAINERID", entityItems.CONTAINERID);
                            mCommand.Parameters.Add("PACKAGETYPECODE", entityItems.PACKAGETYPECODE);
                            mCommand.Parameters.Add("PACKAGETYPENAME", entityItems.PACKAGETYPENAME);
                            mCommand.Parameters.Add("LENGTHUOMCODE", entityItems.LENGTHUOMCODE);
                            mCommand.Parameters.Add("LENGTHUOMNAME", entityItems.LENGTHUOMNAME);
                            mCommand.Parameters.Add("WEIGHTUOMCODE", entityItems.WEIGHTUOMCODE);
                            mCommand.Parameters.Add("WEIGHTUOMNAME", entityItems.WEIGHTUOMNAME);
                            mCommand.Parameters.Add("CONTAINERCODE", entityItems.CONTAINERCODE);
                            mCommand.Parameters.Add("TOTALCBM", entityItems.TOTALCBM);
                            mCommand.Parameters.Add("CONTAINERNAME", entityItems.CONTAINERNAME);
                            mCommand.ExecuteNonQuery();
                        }

                        mCommand.Parameters.Clear();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public long UpdateSave(QuotationEntity entity, string HSCode)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;
                    try
                    {
                        int? DPROXIMITYTYPEID = 0;
                        if (entity.DESTINATIONZIPCODE != null)
                            DPROXIMITYTYPEID = 105;
                        else
                            DPROXIMITYTYPEID = 103;

                        int? OPROXIMITYTYPEID = 0;
                        if (entity.ORIGINZIPCODE != null)
                            OPROXIMITYTYPEID = 105;
                        else
                            OPROXIMITYTYPEID = 103;

                        mCommand.CommandText = @"UPDATE QMQUOTATION SET DATEOFENQUIRY = SYSDATE, DATEOFVALIDITY=SYSDATE+14, DATEOFSHIPMENT=SYSDATE, PRODUCTID=:PRODUCTID , PRODUCTTYPEID=:PRODUCTTYPEID, MOVEMENTTYPEID=:MOVEMENTTYPEID, 
                                                 ORIGINPLACEID=:ORIGINPLACEID,  ORIGINPORTID=:ORIGINPORTID, DESTINATIONPLACEID=:DESTINATIONPLACEID,DESTINATIONPORTID=:DESTINATIONPORTID, TOTALGROSSWEIGHT=:TOTALGROSSWEIGHT, TOTALCBM=:TOTALCBM,
                                                 DENSITYRATIO=:DENSITYRATIO, CHARGEABLEWEIGHT=:CHARGEABLEWEIGHT,CHARGEABLEVOLUME=:CHARGEABLEVOLUME,VOLUMETRICWEIGHT=:VOLUMETRICWEIGHT, SHIPMENTDESCRIPTION= :SHIPMENTDESCRIPTION, 
                                                 ISHAZARDOUSCARGO=:ISHAZARDOUSCARGO, CARGOVALUE=:CARGOVALUE, CARGOVALUECURRENCYID=:CARGOVALUECURRENCYID, INSUREDVALUE=:INSUREDVALUE, INSUREDVALUECURRENCYID=:INSUREDVALUECURRENCYID, 
                                                 CUSTOMERID=:CUSTOMERID,  PREFERREDCURRENCYID=:PREFERREDCURRENCYID, DATEMODIFIED=SYSDATE, MODIFIEDBY=:MODIFIEDBY, STATEID=:STATEID,  PRODUCTNAME=:PRODUCTNAME, PRODUCTTYPENAME=:PRODUCTTYPENAME,
                                                 MOVEMENTTYPENAME=:MOVEMENTTYPENAME,ORIGINPLACECODE=:ORIGINPLACECODE, ORIGINPLACENAME=:ORIGINPLACENAME, ORIGINPORTCODE=:ORIGINPORTCODE, ORIGINPORTNAME=:ORIGINPORTNAME, DESTINATIONPLACECODE=:DESTINATIONPLACECODE,
                                                 DESTINATIONPLACENAME=:DESTINATIONPLACENAME,  DESTINATIONPORTCODE=:DESTINATIONPORTCODE,  DESTINATIONPORTNAME=:DESTINATIONPORTNAME,ISINSURANCEREQUIRED=:ISINSURANCEREQUIRED, CREATEDBY=:CREATEDBY,
                                                 GUESTCOMPANY=:GUESTCOMPANY, GUESTEMAIL=:GUESTEMAIL,GUESTNAME=:GUESTNAME,GUESTLASTNAME=:GUESTLASTNAME,ORIGINSUBDIVISIONCODE=:ORIGINSUBDIVISIONCODE, DESTINATIONSUBDIVISIONCODE=:DESTINATIONSUBDIVISIONCODE,
                                                 ORIGINZIPCODE=:ORIGINZIPCODE,DESTINATIONZIPCODE=:DESTINATIONZIPCODE,OPROXIMITYTYPEID=:OPROXIMITYTYPEID,DPROXIMITYTYPEID=:DPROXIMITYTYPEID,OCOUNTRYCODE=:OCOUNTRYCODE,DCOUNTRYCODE=:DCOUNTRYCODE,
                                                 HAZARDOUSGOODSTYPE=:HAZARDOUSGOODSTYPE,ISCUSTOMREQUIRED=:ISCUSTOMREQUIRED,ISOVERSIZECARGO=:ISOVERSIZECARGO
                                                 WHERE QUOTATIONID=:QUOTATIONID";
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        entity.MODIFIEDBY = "GUEST";
                        entity.STATEID = "QUOTECREATED";
                        mCommand.Parameters.Add("PRODUCTID", entity.PRODUCTID);
                        mCommand.Parameters.Add("PRODUCTTYPEID", entity.PRODUCTTYPEID);
                        mCommand.Parameters.Add("MOVEMENTTYPEID", entity.MOVEMENTTYPEID);
                        mCommand.Parameters.Add("ORIGINPLACEID", entity.ORIGINPLACEID);
                        mCommand.Parameters.Add("ORIGINPORTID", entity.ORIGINPORTID);
                        mCommand.Parameters.Add("DESTINATIONPLACEID", entity.DESTINATIONPLACEID);
                        mCommand.Parameters.Add("DESTINATIONPORTID", entity.DESTINATIONPORTID);
                        mCommand.Parameters.Add("TOTALGROSSWEIGHT", entity.TOTALGROSSWEIGHT);
                        mCommand.Parameters.Add("TOTALCBM", entity.TOTALCBM);
                        mCommand.Parameters.Add("DENSITYRATIO", entity.DENSITYRATIO);
                        mCommand.Parameters.Add("CHARGEABLEWEIGHT", entity.CHARGEABLEWEIGHT);
                        mCommand.Parameters.Add("CHARGEABLEVOLUME", entity.CHARGEABLEVOLUME);
                        mCommand.Parameters.Add("VOLUMETRICWEIGHT", entity.VOLUMETRICWEIGHT);
                        mCommand.Parameters.Add("SHIPMENTDESCRIPTION", entity.SHIPMENTDESCRIPTION);
                        mCommand.Parameters.Add("ISHAZARDOUSCARGO", entity.ISHAZARDOUSCARGO);
                        mCommand.Parameters.Add("CARGOVALUE", entity.CARGOVALUE);
                        mCommand.Parameters.Add("CARGOVALUECURRENCYID", entity.CARGOVALUECURRENCYID);
                        mCommand.Parameters.Add("INSUREDVALUE", entity.INSUREDVALUE);
                        mCommand.Parameters.Add("INSUREDVALUECURRENCYID", entity.INSUREDVALUECURRENCYID);
                        mCommand.Parameters.Add("CUSTOMERID", entity.CUSTOMERID);
                        mCommand.Parameters.Add("PREFERREDCURRENCYID", entity.PREFERREDCURRENCYID);
                        mCommand.Parameters.Add("MODIFIEDBY", entity.MODIFIEDBY);
                        mCommand.Parameters.Add("STATEID", entity.STATEID);
                        mCommand.Parameters.Add("PRODUCTNAME", entity.PRODUCTNAME);
                        mCommand.Parameters.Add("PRODUCTTYPENAME", entity.PRODUCTTYPENAME);
                        mCommand.Parameters.Add("MOVEMENTTYPENAME", entity.MOVEMENTTYPENAME);
                        mCommand.Parameters.Add("ORIGINPLACECODE", entity.ORIGINPLACECODE);
                        mCommand.Parameters.Add("ORIGINPLACENAME", entity.ORIGINPLACENAME);
                        mCommand.Parameters.Add("ORIGINPORTCODE", entity.ORIGINPORTCODE);
                        mCommand.Parameters.Add("ORIGINPORTNAME", entity.ORIGINPORTNAME);
                        mCommand.Parameters.Add("DESTINATIONPLACECODE", entity.DESTINATIONPLACECODE);
                        mCommand.Parameters.Add("DESTINATIONPLACENAME", entity.DESTINATIONPLACENAME);
                        mCommand.Parameters.Add("DESTINATIONPORTCODE", entity.DESTINATIONPORTCODE);
                        mCommand.Parameters.Add("DESTINATIONPORTNAME", entity.DESTINATIONPORTNAME);
                        mCommand.Parameters.Add("ISINSURANCEREQUIRED", entity.ISINSURANCEREQUIRED);
                        mCommand.Parameters.Add("CREATEDBY", entity.CREATEDBY.ToLowerInvariant());
                        mCommand.Parameters.Add("GUESTCOMPANY", entity.GUESTCOMPANY);
                        mCommand.Parameters.Add("GUESTEMAIL", entity.GUESTEMAIL);
                        mCommand.Parameters.Add("GUESTNAME", entity.GUESTNAME);
                        mCommand.Parameters.Add("GUESTLASTNAME", entity.GUESTLASTNAME);
                        mCommand.Parameters.Add("ORIGINSUBDIVISIONCODE", entity.ORIGINSUBDIVISIONCODE);
                        mCommand.Parameters.Add("DESTINATIONSUBDIVISIONCODE", entity.DESTINATIONSUBDIVISIONCODE);
                        mCommand.Parameters.Add("ORIGINZIPCODE", entity.ORIGINZIPCODE);
                        mCommand.Parameters.Add("DESTINATIONZIPCODE", entity.DESTINATIONZIPCODE);
                        mCommand.Parameters.Add("OPROXIMITYTYPEID", OPROXIMITYTYPEID);
                        mCommand.Parameters.Add("DPROXIMITYTYPEID", DPROXIMITYTYPEID);
                        mCommand.Parameters.Add("OCOUNTRYCODE", entity.OCOUNTRYCODE);
                        mCommand.Parameters.Add("DCOUNTRYCODE", entity.DCOUNTRYCODE);
                        mCommand.Parameters.Add("HAZARDOUSGOODSTYPE", entity.HAZARDOUSGOODSTYPE);
                        mCommand.Parameters.Add("ISCUSTOMREQUIRED", 1);
                        mCommand.Parameters.Add("ISOVERSIZECARGO", entity.ISOVERSIZECARGO);

                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "DELETE FROM QMSHIPMENTITEM WHERE QUOTATIONID=:QUOTATIONID";
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO QMSHIPMENTITEM ( QUOTATIONID, ITEMDESCRIPTION, QUANTITY, PACKAGETYPEID, ITEMLENGTH, ITEMWIDTH, ITEMHEIGHT, LENGTHUOM, WEIGHTPERPIECE, WEIGHTTOTAL, WEIGHTUOM, CONTAINERID, PACKAGETYPECODE, PACKAGETYPENAME, LENGTHUOMCODE, LENGTHUOMNAME, WEIGHTUOMCODE, WEIGHTUOMNAME, TOTALCBM, CONTAINERNAME,CONTAINERCODE ) VALUES (:QUOTATIONID, :ITEMDESCRIPTION, :QUANTITY, :PACKAGETYPEID, :ITEMLENGTH, :ITEMWIDTH, :ITEMHEIGHT, :LENGTHUOM, :WEIGHTPERPIECE, :WEIGHTTOTAL, :WEIGHTUOM, :CONTAINERID, :PACKAGETYPECODE, :PACKAGETYPENAME, :LENGTHUOMCODE, :LENGTHUOMNAME, :WEIGHTUOMCODE, :WEIGHTUOMNAME, :TOTALCBM, :CONTAINERNAME, :CONTAINERCODE )";
                        foreach (var entityItems in entity.ShipmentItems)
                        {
                            mCommand.Parameters.Clear();
                            mCommand.Parameters.Add("QUOTATIONID", entity.QUOTATIONID);
                            mCommand.Parameters.Add("ITEMDESCRIPTION", entityItems.ITEMDESCRIPTION);
                            mCommand.Parameters.Add("QUANTITY", entityItems.QUANTITY);
                            mCommand.Parameters.Add("PACKAGETYPEID", entityItems.PACKAGETYPEID);
                            mCommand.Parameters.Add("ITEMLENGTH", entityItems.ITEMLENGTH);
                            mCommand.Parameters.Add("ITEMWIDTH", entityItems.ITEMWIDTH);
                            mCommand.Parameters.Add("ITEMHEIGHT", entityItems.ITEMHEIGHT);
                            mCommand.Parameters.Add("LENGTHUOM", entityItems.LENGTHUOM);
                            mCommand.Parameters.Add("WEIGHTPERPIECE", entityItems.WEIGHTPERPIECE);
                            mCommand.Parameters.Add("WEIGHTTOTAL", entityItems.WEIGHTTOTAL);
                            mCommand.Parameters.Add("WEIGHTUOM", entityItems.WEIGHTUOM);
                            mCommand.Parameters.Add("CONTAINERID", entityItems.CONTAINERID);
                            mCommand.Parameters.Add("PACKAGETYPECODE", entityItems.PACKAGETYPECODE);
                            mCommand.Parameters.Add("PACKAGETYPENAME", entityItems.PACKAGETYPENAME);
                            mCommand.Parameters.Add("LENGTHUOMCODE", entityItems.LENGTHUOMCODE);
                            mCommand.Parameters.Add("LENGTHUOMNAME", entityItems.LENGTHUOMNAME);
                            mCommand.Parameters.Add("WEIGHTUOMCODE", entityItems.WEIGHTUOMCODE);
                            mCommand.Parameters.Add("WEIGHTUOMNAME", entityItems.WEIGHTUOMNAME);
                            mCommand.Parameters.Add("CONTAINERCODE", entityItems.CONTAINERCODE);
                            mCommand.Parameters.Add("TOTALCBM", entityItems.TOTALCBM);
                            mCommand.Parameters.Add("CONTAINERNAME", entityItems.CONTAINERNAME);
                            mCommand.ExecuteNonQuery();
                        }

                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("PRM_QUOTATIONID", entity.QUOTATIONID);
                        mCommand.CommandText = "USP_GET_QUOTE_NILESH";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return entity.QUOTATIONID;
        }

        public void Update(QuotationEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(long key)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void SaveQuoteForLater(string userId, int id)
        {
            string Createdby = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT CREATEDBY FROM QMQUOTATION WHERE QUOTATIONID = :QUOTATIONID",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                });

            if (Createdby == "guest")
            {
                this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET CREATEDBY = :CREATEDBY WHERE QUOTATIONID = :QUOTATIONID",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "CREATEDBY",Value = userId.ToLowerInvariant()},
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                });
            }
        }

        public void RequestQuote(int id)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;

                        mCommand.Parameters.Add("PRM_QID", Oracle.DataAccess.Client.OracleDbType.Int32).Value = Convert.ToInt32(id);
                        mCommand.Parameters.Add("PRM_DATEREQUESTED", Oracle.DataAccess.Client.OracleDbType.Date).Value = DateTime.Now;//ds.Tables[0].Rows[0]["DATEOFENQUIRY"];

                        mCommand.CommandText = "USP_SSP_REQUESTQUOTE";
                        mCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET STATEID = 'SUBMITTED2HD' WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                });
        }

        public int MultiCarrierSaveQuote(int QUOTATIONID)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.Parameters.Add("PRM_QUOTATIONID", Oracle.DataAccess.Client.OracleDbType.Int32).Value = Convert.ToInt32(QUOTATIONID);

                        mCommand.CommandText = "USP_GET_QUOTE_MULTICARRIER";
                        mCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }

                    int QuoteExist = 0;
                    QuoteExist = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(1) FROM QMQUOTATION WHERE QUOTATIONID = :QUOTATIONID",
                    new OracleParameter[]{
                        new OracleParameter{ ParameterName = "QUOTATIONID",Value = QUOTATIONID},
                    });
                    return QuoteExist;
                }
            }
        }

        public SSPDocumentsEntity GetMsdsdocdetailsbyid(int QUOTEID)
        {
            SSPDocumentsEntity mEntity = new SSPDocumentsEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE QUOTATIONID=:QUOTATIONID and SOURCE='SHIPAQUOTE'",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = QUOTEID } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<SSPDocumentsEntity>(r);
            });
            return mEntity;
        }
        //public SSPDocumentsEntity GetMsdsdocdetailsbyid(int QUOTEID)
        //{
        //    SSPDocumentsEntity mEntity = new SSPDocumentsEntity();
        //    ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE QUOTATIONID=:QUOTATIONID and SOURCE='SHIPAQUOTE'",
        //        new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONID", Value = QUOTEID } }
        //    , delegate(IDataReader r)
        //    {
        //        mEntity = DataReaderMapToObject<SSPDocumentsEntity>(r);
        //    });
        //    return mEntity;
        //}
        public async Task<long> RegenerateQuote(string userID, int id, string HSCode)
        {
            QuotationEntity mUIModel = this.GetById(id);
            mUIModel.CREATEDBY = userID.ToLowerInvariant();
            mUIModel.GUESTEMAIL = null;
            //Checking Insurance Required or not
            long orgincountryid = Convert.ToInt64(mUIModel.OCOUNTRYID);
            long destcountryid = Convert.ToInt64(mUIModel.DCOUNTRYID);
            string insuranceRequired = InsuranceRequiredOrNot(orgincountryid, destcountryid);
            if (insuranceRequired.ToUpperInvariant() == "NO")
            {
                mUIModel.ISINSURANCEREQUIRED = 0;
                mUIModel.CARGOVALUE = 0;
            }
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET STATEID = 'DELETED' WHERE QUOTATIONID = :QUOTATIONID",
               new OracleParameter[]{                   
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                });
            return await Task.Run(() => this.InsertSave(mUIModel, HSCode)).ConfigureAwait(false);


        }

        public string DeleteQuote(int id, string UserId)
        {
            int QuotationCount = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(1) FROM JARECEIPTPAYMENTDETAILS WHERE QUOTATIONID= :QUOTATIONID ",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                });
            if (QuotationCount == 0)
            {
                this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET STATEID = 'QUOTEDELETED' WHERE QUOTATIONID = :QUOTATIONID AND UPPER(CREATEDBY)=:USERID ",
                      new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                    new OracleParameter{ ParameterName = "USERID",Value = UserId.ToUpper()},
                      });

                this.ExecuteScalar<int>(DBConnectionMode.FOCiS, "UPDATE SSPQUOTATION SET STATEID = 'QUOTEDELETED' WHERE QUOTATIONID= :QUOTATIONID AND UPPER(CREATEDBY)=:USERID ",
                     new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                    new OracleParameter{ ParameterName = "USERID",Value = UserId.ToUpper()},
                     });

                return "Deleted";
            }
            else
            {
                return "";
            }
        }

        public string DeleteTemplate(int id, string UserId)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "DELETE QMQUOTATIONTEMPLATE WHERE QUOTATIONID = :QUOTATIONID AND UPPER(CREATEDBY)=:USERID ",
                    new OracleParameter[]{
                new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
                new OracleParameter{ ParameterName = "USERID",Value = UserId.ToUpper()},
                    });

            this.ExecuteScalar<int>(DBConnectionMode.SSP, "DELETE QMSHIPMENTITEMTEMPLATE WHERE QUOTATIONID = :QUOTATIONID",
                    new OracleParameter[]{
                new OracleParameter{ ParameterName = "QUOTATIONID",Value = id}        
                    });

            return "Deleted";
        }

        public void UpdateMulticarrier(int QUOTATIONID)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET MULTIQUOTATIONID = NULL WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { 
                     new OracleParameter{ ParameterName = "QUOTATIONID",Value = QUOTATIONID},
                });
        }

        public async Task<string> ZipCodeConfigbyCountryId(String CountryId)
        {
            string ZipCodeConfig = string.Empty;
            UserDBFactory mFactory = new UserDBFactory();
            DataSet Result = await Task.Run(() => mFactory.GetCountryTaxDetails(Convert.ToInt32(CountryId))).ConfigureAwait(false);
            if (!ReferenceEquals(Result, null) && Result.Tables[0].Rows.Count > 0)
            {
                ZipCodeConfig = Convert.ToString(Result.Tables[0].Rows[0]["ISZIPCODE"]);
            }

            return ZipCodeConfig;
        }

        public int TemplateNameCheck(String TemplateName, string UserId)
        {
            int NameExist = 0;
            NameExist = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(1) FROM QMQUOTATIONTEMPLATE WHERE UPPER(TEMPLATENAME)= :TEMPLATENAME AND UPPER(CREATEDBY)=:USERID  ",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "TEMPLATENAME",Value = TemplateName.ToUpper()},
                    new OracleParameter{ ParameterName = "USERID",Value = UserId.ToUpper()},
                });

            return NameExist;
        }

        public async Task<string> SanctionedCountrybyCountryId(String FromCountryId, String ToCountryId, string productid, string productypeid)
        {
            string SanctionedCountry = await Task.Run(() => this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT MESSAGE FROM SHIPABANNEDCOUNTRIES WHERE FROMCOUNTRYID = :FROMCOUNTRYID AND TOCOUNTRYID = :TOCOUNTRYID AND PRODUCTID=:PRODUCTID AND PRODUCTYPEID=:PRODUCTYPEID",
                new OracleParameter[]{
                    new OracleParameter{ ParameterName = "FROMCOUNTRYID",Value = FromCountryId},
                    new OracleParameter{ ParameterName = "TOCOUNTRYID",Value = ToCountryId},
                    new OracleParameter{ ParameterName = "PRODUCTID",Value = productid},
                    new OracleParameter{ ParameterName = "PRODUCTYPEID",Value = productypeid},
                })).ConfigureAwait(false);

            return SanctionedCountry;
        }

        public long InsertUserTrackDetails(string user, string ipAddress, string serverName)
        {
            int Id = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT FOCSSP.SSPUSERLOGINTRACK_SEQ.NEXTVAL FROM DUAL",
               new OracleParameter[] { });
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "INSERT INTO SSPUSERLOGINTRACK (TRACKID, LOGINTIME, USERID, IPADDRESS,SERVERNAME) VALUES (:TRACKID, SYSDATE, :USERID, :IPADDRESS, :SERVERNAME)",
                new OracleParameter[] { 
                     new OracleParameter{ ParameterName = "TRACKID",Value = Id},
                     new OracleParameter{ ParameterName = "USERID",Value = user},
                     new OracleParameter{ ParameterName = "IPADDRESS",Value = ipAddress},
                     new OracleParameter{ ParameterName = "SERVERNAME",Value = serverName},
                });
            return Id;
        }

        public void UpdateUserTrackDetails(int id, string user)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE SSPUSERLOGINTRACK SET LOGOUTTIME = SYSDATE WHERE TRACKID = :TRACKID AND USERID = :USERID",
                new OracleParameter[] { 
                    new OracleParameter {OracleDbType = OracleDbType.Long, ParameterName="TRACKID", Value = id },
                    new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName="USERID", Value = user }
                });
        }

        public void InsertErrorLog(string user, string controller, string action, string errmessage, string trace)
        {
            string exceptionstacktrace = string.Concat(trace.Take(4000));
            long ERRID = this.ExecuteScalar<long>(DBConnectionMode.SSP, "SELECT SSP_SSPEXCEPTIONLOGGER_SEQ.NEXTVAL FROM DUAL",
                new OracleParameter[] { });
            string EnvironmentName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "INSERT INTO SSPEXCEPTIONLOGGER(ERRID,EXCEPTIONMESSAGE,CONTROLLERNAME,ACTIONNAME,USERID,DATELOGGED,ENVIRONMENTNAME,EXCEPTIONSTACKTRACE) VALUES (:ERRID,:EXCEPTIONMESSAGE,:CONTROLLERNAME,:ACTIONNAME,:USERID,SYSDATE,:SERVERNAME,:EXCEPTIONSTACKTRACE)",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "ERRID", Value = ERRID},
                    new OracleParameter{ ParameterName = "EXCEPTIONMESSAGE", Value = errmessage},
                    new OracleParameter{ ParameterName = "CONTROLLERNAME",Value = controller},
                    new OracleParameter{ ParameterName = "ACTIONNAME",Value = action},                   
                    new OracleParameter{ ParameterName = "USERID",Value = user},
                    new OracleParameter{ ParameterName = "SERVERNAME",Value = EnvironmentName},
                    new OracleParameter{ ParameterName = "EXCEPTIONSTACKTRACE",Value = exceptionstacktrace},
                });
        }

        public dynamic GetErrorLog(int count)
        {
            var ErrorList = new List<SSPExceptionLogger>();
            var query = @"
                            SELECT * FROM (SELECT * FROM SSPEXCEPTIONLOGGER ORDER BY ERRID DESC)  WHERE ROWNUM <= :COUNT
                        ";
            ReadText(DBConnectionMode.SSP, query,
                new OracleParameter[] { 
                        new OracleParameter { ParameterName = "COUNT", Value = count }
                }
            , delegate(IDataReader r)
            {
                ErrorList.Add(DataReaderMapToObject<SSPExceptionLogger>(r));
            });

            return ErrorList;
        }

        public long SaveQuoteDocument(byte[] bytes, string QuotationId)
        {
            string QuotationNumber = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT QUOTATIONNUMBER FROM QMQUOTATION WHERE QUOTATIONID=:QUOTATIONID ",
               new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = QuotationId},
                });
            string QuoteDocCount = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT count(1) FROM SSPDOCUMENTS WHERE QUOTATIONID=:QUOTATIONID and PAGEFLAG=2 ",
             new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = QuotationId},
                });

            if (QuoteDocCount == "0")
            {
                OracleConnection mConnection;
                OracleTransaction mOracleTransaction = null;
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;
                        try
                        {
                            mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                            var DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                            mCommand.CommandText = "Insert into SSPDOCUMENTS (DOCID,QUOTATIONID,FILENAME,FILECONTENT,PAGEFLAG) values (:DOCID,:QUOTATIONID,:FILENAME,:FILECONTENT,2)";
                            mCommand.Parameters.Add("DOCID", DOCID);
                            mCommand.Parameters.Add("QUOTATIONID", QuotationId);
                            mCommand.Parameters.Add("FILENAME", QuotationNumber + ".pdf");
                            mCommand.Parameters.Add("FILECONTENT", bytes);
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            mOracleTransaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            return Convert.ToInt32(QuotationId);
        }

        public PortPairsEntity GETPORTPAIRSDATA(long? O)
        {
            PortPairsEntity mEntity = new PortPairsEntity();
            ReadText(DBConnectionMode.SSP, "select UNLOCATIONID,CODE,NAME,COUNTRYCODE,COUNTRYID,COUNTRYNAME,SUBDIVISIONCODE,SUBDIVISIONNAME from UNLOCATIONS WHERE UNLOCATIONID = :UNLOCATIONID",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "UNLOCATIONID", Value = O } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<PortPairsEntity>(r);
            });

            return mEntity;
        }

        public DataTable GetAddressBook(string Createdby)
        {
            string Query = "";
            Query = @"select DISTINCT CONTACTADDRESS from ADDRESSBOOK where lower(Email)=lower(:Createby)";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter 
                                            { OracleDbType = OracleDbType.Varchar2, ParameterName = "Createby", Value = Createdby.ToLowerInvariant() } });
            return dt;
        }

        public DataTable GetAddressBookfornominate(string Createdby)
        {
            string Query = "";
            Query = "SELECT ALTERNATEEMAIL FROM SSPALTERNATEEMAILS WHERE USERID =:Createby";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter 
                                            { OracleDbType = OracleDbType.Varchar2, ParameterName = "Createby", Value = Createdby } });
            return dt;
        }

        public DataTable GetNPCdetails(Int64 CountryId)
        {
            string Query = "SELECT NPCMAILID AS AIRNPCMAILID, OCEANNPCMAILID,ONPCMAILIDESCLTN,NPCMAILIDESCLTN,AIRMAXWEIGHT,OCEANMAXWEIGHT,CBM AS AIRCBM,OCEANCBM FROM SSPRATEMARGINCONFIG WHERE CountryId=:CountryId";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] { new OracleParameter 
                                          { OracleDbType = OracleDbType.Varchar2, ParameterName = "CountryId", Value = CountryId } });
            return dt;
        }

        public DataTable CouponVerification(string Coupon, int ProductId)
        {
            int Discountid = 0;
            DataTable dt = new DataTable();

            Discountid = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT * FROM (SELECT D.DISCOUNTID FROM FOCIS.DISCOUNT D INNER JOIN FOCIS.DISCOUNT_PRODUCT DP  ON D.DISCOUNTID=DP.DISCOUNTID WHERE D.DISCOUNTCODE=:DISCOUNTCODE AND D.APPLICABILITYFROM < TRUNC(SYSDATE) AND D.APPLICABILITYTO >= TRUNC(SYSDATE) AND D.STATUS=1 AND DP.PRODUCTID=:ProductId) A WHERE ROWNUM <= 1",
                    new OracleParameter[]{
                       new OracleParameter { OracleDbType = OracleDbType.Varchar2, ParameterName = "DISCOUNTCODE", Value = Coupon },
                       new OracleParameter { OracleDbType = OracleDbType.Int32, ParameterName = "ProductId", Value = ProductId }
                    });

            if (Discountid > 0)
            {
                string Query = "SELECT ULO.COUNTRYID AS ORIGINCOUNTRYID,ULD.COUNTRYID AS DESTINATIONCOUNTRYID FROM FOCIS.DISCOUNT_TRADELINE DT INNER JOIN FOCIS.UNLOCATIONS ULO ON DT.FROMUNLOCATIONID=ULO.UNLOCATIONID INNER JOIN FOCIS.UNLOCATIONS ULD ON DT.TOUNLOCATIONID=ULD.UNLOCATIONID WHERE DT.DISCOUNTID=:Discountid";
                dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] {               
                new OracleParameter { OracleDbType = OracleDbType.Int32, ParameterName = "Discountid", Value = Discountid }});
            }

            return dt;
        }
        public string InsuranceRequiredOrNot(Int64? orgingCountryId, Int64? destinationCountryId)
        {

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            string isInsuranceRequired = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("PRM_OCOUNTRYID", orgingCountryId);
                        mCommand.Parameters.Add("PRM_DCOUNTRYID", destinationCountryId);
                        mCommand.Parameters.Add("PRM_INSURENCEFLAG", OracleDbType.RefCursor).Direction = ParameterDirection.Output;


                        mCommand.CommandText = "USP_FOC_INSURANCEFLAG";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["PRM_INSURENCEFLAG"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        reader1.Close();
                        mConnection.Close();
                        isInsuranceRequired = Convert.ToString(dt.Rows[0]["INSURENCERESULT"]);
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
            return isInsuranceRequired;
        }
        public string GetRegisterVideo(string name, string VideoType = "UNIVERSAL")
        {
            string videolink = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT VIDEOLINK FROM SHIPAVIDEOLINKS WHERE VIDEONAME=:VIDEONAME AND VIDEOTYPE=:VIDEOTYPE",
            new OracleParameter[]{
                new OracleParameter{ ParameterName = "VIDEONAME",Value = name},
                new OracleParameter{ ParameterName = "VIDEOTYPE",Value = VideoType},   
            });
            return videolink;
        }

        public void InsertNewEmailsIntoAddressBook(string Email, string CreatedBy)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            if (Email.Contains(","))
            {
                foreach (string s in Email.Split(','))
                {
                    using (mConnection = new OracleConnection(SSPConnectionString))
                    {
                        if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                        mOracleTransaction = mConnection.BeginTransaction();
                        using (OracleCommand mCommand = mConnection.CreateCommand())
                        {
                            mCommand.BindByName = true;
                            mCommand.Transaction = mOracleTransaction;
                            mCommand.CommandType = CommandType.Text;
                            try
                            {
                                mCommand.CommandText = "SELECT ADDRESSBOOK_SEQ.NEXTVAL FROM DUAL";
                                var ID = Convert.ToInt64(mCommand.ExecuteScalar());
                                mCommand.CommandText = "Insert into ADDRESSBOOK (ID,CONTACTADDRESS,EMAIL,ACTIVE) values (:ID,:CONTACTADDRESS,:EMAIL,:ACTIVE)";
                                mCommand.Parameters.Add("ID", ID);
                                mCommand.Parameters.Add("CONTACTADDRESS", s);
                                mCommand.Parameters.Add("EMAIL", CreatedBy);
                                mCommand.Parameters.Add("ACTIVE", 1);
                                mCommand.ExecuteNonQuery();
                                mOracleTransaction.Commit();
                            }
                            catch (Exception)
                            {
                                mOracleTransaction.Rollback();
                                throw;
                            }
                        }
                    }
                }
            }
            else
            {
                using (mConnection = new OracleConnection(SSPConnectionString))
                {
                    if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                    mOracleTransaction = mConnection.BeginTransaction();
                    using (OracleCommand mCommand = mConnection.CreateCommand())
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;
                        try
                        {
                            mCommand.CommandText = "SELECT ADDRESSBOOK_SEQ.NEXTVAL FROM DUAL";
                            var ID = Convert.ToInt64(mCommand.ExecuteScalar());
                            mCommand.CommandText = "Insert into ADDRESSBOOK (ID,CONTACTADDRESS,EMAIL,ACTIVE) values (:ID,:CONTACTADDRESS,:EMAIL,:ACTIVE)";
                            mCommand.Parameters.Add("ID", ID);
                            mCommand.Parameters.Add("CONTACTADDRESS", Email);
                            mCommand.Parameters.Add("EMAIL", CreatedBy);
                            mCommand.Parameters.Add("ACTIVE", 1);
                            mCommand.ExecuteNonQuery();
                            mOracleTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            mOracleTransaction.Rollback();
                            throw;
                        }
                    }
                }
            }
        }

        public DataSet ValidateMinWeight(string countryId)
        {
            DataSet ds = new DataSet();

            string Query = @"SELECT AIRMINWEIGHT,OCEANMINWEIGHT FROM SSPRATEMARGINCONFIG WHERE COUNTRYID=:COUNTRYID";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] {
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="COUNTRYID",Value = countryId } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet GetLatLon(long oplaceid, long oportid, long dplaceid, long dportid)
        {
            DataSet ds = new DataSet();
            string Query = "SELECT UNLOCATIONID,LATITUDE,LONGITUDE FROM UNLOCATIONS WHERE UNLOCATIONID IN (:oplaceid , :oportid , :dplaceid , :dportid ) AND LATITUDE IS NOT NULL AND LONGITUDE IS NOT NULL";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="oplaceid",Value = oplaceid  } ,
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="oportid",Value = oportid  } ,
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="dplaceid",Value = dplaceid  } ,
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="dportid",Value = dportid  } 
            });
            ds.Tables.Add(dt);
            return ds;
        }
        public DataSet GetLatLon(long oportid, long dportid)
        {
            DataSet ds = new DataSet();
            string Query = "SELECT UNLOCATIONID,LATITUDE,LONGITUDE FROM UNLOCATIONS WHERE UNLOCATIONID IN (:oportid , :dportid) AND LATITUDE IS NOT NULL AND LONGITUDE IS NOT NULL";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="oportid",Value = oportid } ,
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="dportid",Value = dportid  } 
            });
            ds.Tables.Add(dt);
            return ds;
        }
        public DataSet GetLatLon(long oplaceid, long oportid, long dplaceid)
        {
            DataSet ds = new DataSet();
            string Query = "SELECT UNLOCATIONID,LATITUDE,LONGITUDE FROM UNLOCATIONS WHERE UNLOCATIONID IN (:oplaceid , :oportid , :dplaceid ) AND LATITUDE IS NOT NULL AND LONGITUDE IS NOT NULL";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] { 
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="oplaceid",Value = oplaceid },
                new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="oportid",Value = oportid  } ,
                  new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName="dplaceid",Value = dplaceid  }
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public void UpdateCo2(Int64 QuotationId, double CO2EMISSION)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET CO2EMISSION = :CO2EMISSION WHERE QUOTATIONID = :QUOTATIONID",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "CO2EMISSION",Value = CO2EMISSION},
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = QuotationId},
                });
        }

        public DataSet GetUserCurrencyandUOM(string user)
        {
            DataSet ds = new DataSet();
            string Query = "SELECT LENGTHUNIT,WEIGHTUNIT,PREFERREDCURRENCY,PREFERREDCURRENCYCODE FROM USERS WHERE USERID=:USERID";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { ParameterName="USERID",Value= user } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public int GetDecimalPointByCurrencyCode(string currencyCode)
        {
            int decimalPoint;
            if (currencyCode != null)
            {
                decimalPoint = this.ExecuteScalar<int>(DBConnectionMode.SSP, "select decimals from CURRENCIES where currencycode=:currencyCode",
                  new OracleParameter[]{
                    new OracleParameter{ ParameterName = "currencyCode",Value = currencyCode.ToUpper()},
                });
            }
            else
            {
                decimalPoint = 0;
            }
            return decimalPoint;
        }

        public int CheckQuoteBooked(long id)
        {
            int QuoteBooked;

            QuoteBooked = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT COUNT(JOBNUMBER) FROM SSPJOBDETAILS WHERE QUOTATIONID=:QUOTATIONID",
                new OracleParameter[]{
                new OracleParameter{ ParameterName = "QUOTATIONID",Value = id},
            });

            return QuoteBooked;
        }

        public string RoundedValue(string value, string currency)
        {
            string RoundedValue = string.Empty;
            string Decimals = string.Empty;

            Decimals = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT DECIMALS FROM CURRENCIES WHERE UPPER(CURRENCYID) = :CURRENCY",
               new OracleParameter[]{
                new OracleParameter{ ParameterName = "CURRENCY",Value = currency},               
            });

            RoundedValue = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT UFN_SSP_ROUNDEDVALUE(:value , :Decimals) FROM DUAL",
                new OracleParameter[]{ 
                     new OracleParameter{ ParameterName = "value",Value = value},
                      new OracleParameter{ ParameterName = "Decimals",Value = Decimals }
                });

            return RoundedValue;
        }

        public DataSet GetCountryID(string code)
        {
            DataSet ds = new DataSet();
            string Query = "SELECT COUNTRYID,NAME FROM SMDM_COUNTRY WHERE UPPER(CODE) =:code ";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] { 
                new OracleParameter {  ParameterName = "code",Value = code } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public QuotationEntity GetQuotationTemplates(string user)
        {
            QuotationEntity mEntity = new QuotationEntity();
            string Query = " SELECT * FROM QMQUOTATIONTEMPLATE WHERE UPPER(CREATEDBY) = UPPER(:CREATEDBY)";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter {  ParameterName = "CREATEDBY",Value = user } 
            });
            List<DBFactory.Entities.QuotationTemplateEntity> mTemplates = MyClass.ConvertToList<DBFactory.Entities.QuotationTemplateEntity>(dt);
            mEntity.QuotationTemplates = mTemplates;
            return mEntity;
        }
        public QuoteOffersEntity GetOfferbyId(int id)
        {
            QuoteOffersEntity mEntity = new QuoteOffersEntity();
            string Query = "SELECT * FROM SSPEXCLUSIVEOFFERS WHERE QUOTATIONID=:QUOTATIONID";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter {  ParameterName = "QUOTATIONID",Value = id } 
            });
            mEntity = MyClass.ConvertToEntity<QuoteOffersEntity>(dt);
            return mEntity;
        }

        public List<QuotationTermsConditionEntity> GetTermsAndConditions()
        {
            List<DBFactory.Entities.QuotationTermsConditionEntity> mTermsAndConditions = new List<DBFactory.Entities.QuotationTermsConditionEntity>();
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Any cargo that is mis-declared at time of quote, the quote will become void and any costs / fines relating to the un-suitable nature of the cargo will be for the shipper's account.", QUOTETERMSANDCONDITIONID = 1 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Shipper has sole responsibility for any demurrage, inspection, storage, detention and/or third party pass-through charges or costs or costs resulting from a force majeure event.", QUOTETERMSANDCONDITIONID = 2 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Agility is not liable for any damage, theft, or pilferage and any claims for the same shall be shipper's responsibility to process with the carrier concerned.  Agility is not responsbile for any consequential damages/ liabilities caused by delayed/ damaged/ missing shipments.", QUOTETERMSANDCONDITIONID = 3 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "At Agility's discretion, interest may apply to any amounts which may become overdue for which payment has not been received.  Agility reserves the right to on charge any and all costs associated to the process of recovering any monies owed outside of agreed terms.", QUOTETERMSANDCONDITIONID = 4 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Transit times are subject to carrier delays, schedule changes, or upliftment and carriers reserve the right to transship goods.", QUOTETERMSANDCONDITIONID = 5 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "All goods are carried subject to the clauses of carriage as shown on the Airway Bill or Bill of Lading.", QUOTETERMSANDCONDITIONID = 6 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Subject to the terms and conditions herein, this quotation is valid for 14 days for ocean transport and valid for 7 days for air transport.", QUOTETERMSANDCONDITIONID = 7 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "All business is transacted subject to our standard trading terms and conditions.  For services not covered by the standard trading terms and conditions, the origin country standard industry terms shall apply (e.g. British International Forwarding Association [BIFA], Federation of Freight Forwarders' Association of India [FFFAI], etc...)", QUOTETERMSANDCONDITIONID = 8 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Quotation is based on current tariffs and present rates of exchange and are therefore subject to normal market fluctuations and without notice.", QUOTETERMSANDCONDITIONID = 9 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of any duties, taxes, or fees that may be applicable including but not limited to customs duties, agency or port fees, VAT, GST, federal, provincial, state and local taxes.", QUOTETERMSANDCONDITIONID = 10 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of any legalization, notarization, or other certified attestation required.", QUOTETERMSANDCONDITIONID = 11 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of any loading / offloading, special equipment required at the delivery site, or other incidental charges.  Any incidental charges that may arise at origin/ destination not mentioned in the quotation will be indvidually line-itemed at cost in the invoice.", QUOTETERMSANDCONDITIONID = 12 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Unless otherwise stated, quotation is exclusive of insurance.  Where permitted by local law and upon request, Agility can also provide a quotation for cargo insurance.", QUOTETERMSANDCONDITIONID = 13 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Quotation is subject to empty equipment availability, space availability, cargo acceptance, and confirmation at time of booking.", QUOTETERMSANDCONDITIONID = 14 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Quotation is subject to General Carrier Adjustment (GCA), dependent on market conditions, fuel surcharges, weight surcharges, fumigation charges, or other surcharges without prior notice as per carrier terms.", QUOTETERMSANDCONDITIONID = 15 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Final shipment price is based on the actual or cubic dimensional weight, whichever is greater as determined by the carrier. Any changes in the weight whether volumetric or actual will result in rate adjustments.", QUOTETERMSANDCONDITIONID = 16 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Final shipment price will be based on cargo receiving date, not estimated departure date.", QUOTETERMSANDCONDITIONID = 17 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Pick-up and/or delivery is based on regular service during normal business hours.  Additional requirements such as, but not limited to, tailgate delivery, appointment delivery, hand load/unload, inside delivery, rush delivery, will be subject to additional charges.", QUOTETERMSANDCONDITIONID = 18 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Shipper has the sole responsibility and shall comply with all applicable laws, government regulations, and/or registrations required of any country to, from, through or over which the goods may be carried, including those relating to the packing, carriage or delivery of the goods, and shall furnish such information and attach such documents to this air waybill as may be necessary to comply with such laws and regulations. Neither Agility or the carrier is not liable to the shipper for loss or expense due to the shipper’s failure to comply with this provision.", QUOTETERMSANDCONDITIONID = 19 });
            mTermsAndConditions.Add(new DBFactory.Entities.QuotationTermsConditionEntity { DESCRIPTION = "Shipper has sole responsibility to ensure that the shipment is correctly labeled, goods are correctly declared/labled, and all necessary documentation provided, including but not limited to hazardous material declarations, licenses, and material data safety sheets. Any costs, fines, and/or fees relating to or resulting from the incorrect labeling or misdelcaration will be for the account of the shipper, including cost incurred by Agility.", QUOTETERMSANDCONDITIONID = 20 });
            return mTermsAndConditions;
        }

        public void SubmitReasonforUnbook(Int64 quotationId, Int64 mquotationId, string reason)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET DATEOFVALIDITY = SYSDATE-1, STATEID = 'SSPQuoteClosed',MULTIQUOTATIONID = NULL, NOTHANKSREASON=:NOTHANKSREASON WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { 
                new OracleParameter{ ParameterName = "NOTHANKSREASON",Value = reason},
                new OracleParameter{ ParameterName = "QUOTATIONID",Value = quotationId}
                });
            this.ExecuteScalar<int>(DBConnectionMode.FOCiS, "UPDATE SSPQUOTATION   SET STATEID = 'SSPQuoteClosed', CLOSINGREASON =:CLOSINGREASON  WHERE QUOTATIONID = :QUOTATIONID",
                new OracleParameter[] { 
                new OracleParameter{ ParameterName = "CLOSINGREASON",Value = reason},
                new OracleParameter{ ParameterName = "QUOTATIONID",Value = quotationId}
                });
            if (mquotationId != 0 && mquotationId != null)
            {
                this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET MULTIQUOTATIONID = NULL WHERE QUOTATIONID = :mquotationId",
                new OracleParameter[] {                
                new OracleParameter{ ParameterName = "QUOTATIONID",Value = mquotationId}
                });
            }
        }

        public void UpdateTransitTime(Int64 QuotationId, string transitTime)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET TRANSITTIME = :TRANSITTIME WHERE QUOTATIONID = :QUOTATIONID",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "TRANSITTIME",Value = transitTime},
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = QuotationId},
                });

        }

        public void InsertGSSCREASON(Int64 quotationId, string reason)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;

                        mCommand.Parameters.Add("PRM_QUOTATIONID", Oracle.DataAccess.Client.OracleDbType.Int32).Value = Convert.ToInt32(quotationId);
                        mCommand.Parameters.Add("PRM_REASON", Oracle.DataAccess.Client.OracleDbType.Varchar2).Value = reason;

                        mCommand.CommandText = "USP_FOC_FOCQMQUOTATINSAVE";
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
            }
        }


        public DataSet GetShipmentItemsDetails(int countryId)
        {
            DataSet ds = new DataSet();
            string Query = @"SELECT COUNTRYID,NVL(AIRHEIGHT,0) AIRHEIGHT,NVL(AIRWIDTH,0) AIRWIDTH,NVL(AIRLENGTH,0) AIRLENGTH,NVL(AIRUOM,'') AIRUOM FROM  FOCIS.SSPRATEMARGINCONFIG 
                             WHERE COUNTRYID IN (CASE WHEN ((SELECT COUNT(1) COUNT FROM focis.SSPRATEMARGINCONFIG where COUNTRYID=:COUNTRYID)>0) 
                             THEN :COUNTRYID ELSE 0 END)";
            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter {  ParameterName = "COUNTRYID",Value = countryId } 
            });
            ds.Tables.Add(dt);
            return ds;
        }

        public string GetNPCAndCountryManagerEmails(string CountryId)
        {
            string Query = @" SELECT RTRIM( REGEXP_REPLACE( LISTAGG((L.EMAILID), ';') WITHIN GROUP (ORDER BY L.EMAILID), '([^,]*)(,\1)+($|,)','\1\3'),',') AS EMAILID
                           FROM ( SELECT DISTINCT U.EMAILID
                           FROM FOCIS.USERS U
                           INNER JOIN FOCIS.ROLEPRINCIPALS RP
                           ON LOWER(U.USERID)=LOWER(RP.PRINCIPALID)
                           INNER JOIN FOCIS.LOCATIONDEPARTMENTUSERS LDU
                           ON LOWER(U.USERID)=LOWER(LDU.USERID)
                           INNER JOIN FOCIS.MV_LOCATIONDEPARTMENT LD
                           ON LDU.LOCATIONDEPARTMENTID=LD.LOCATIONDEPARTMENTID
                           WHERE UPPER(RP.ROLEID) IN ('SSPCOUNTRYADMIN','SSPNPC')
                           AND (LD.COUNTRYID) IN (" + CountryId + "))L";

            string emails = this.ExecuteScalar<string>(DBConnectionMode.FOCiS, Query, new OracleParameter[] { new OracleParameter 
                                          { } });

            return emails;
        }

        public int GetExsistingTransitTime(long originportid, long destinationportid, string productname)
        {
            string query = string.Empty;
            if (productname.ToUpperInvariant() == "AIR")
            {
                query = "select NVL(transittime,0) FROM FOCIS.RMGAFUNIQUEROUTES where rownum=1 and originportid=:ORIGINPORTID and destinationportid=:DESTINATIONPORTID";
            }
            else if (productname.ToUpperInvariant() == "OCEAN")
            {
                query = "select NVL(transittime,0) FROM FOCIS.RMGOFUNIQUEROUTES where rownum=1 and originportid=:ORIGINPORTID and destinationportid=:DESTINATIONPORTID";
            }
            int trasitTime = this.ExecuteScalar<int>(DBConnectionMode.SSP, query,
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "ORIGINPORTID",Value = originportid},
                    new OracleParameter{ ParameterName = "DESTINATIONPORTID",Value = destinationportid},
                });
            return trasitTime;
        }

        public string UpdateCEForQuotation(Int64 QuotationId)
        {
            this.ExecuteScalar<int>(DBConnectionMode.SSP, "UPDATE QMQUOTATION SET CE=(SELECT NAME FROM  FOCIS.SSPTYPES WHERE TYPETYPEID=4 AND ROWNUM=1) WHERE QUOTATIONID=:QUOTATIONID",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = QuotationId},
                });

            string CE = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT CE FROM QMQUOTATION WHERE QUOTATIONID=:QUOTATIONID",
                   new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QUOTATIONID",Value = QuotationId},
                });
            return CE;
        }
        public string GetNPCdetails_Focis(Int64 CountryId)
        {
            string npcEmail = this.ExecuteScalar<string>(DBConnectionMode.SSP, "SELECT LISTAGG(EMAILID, ';') WITHIN GROUP (ORDER BY EMAILID) AS EMAILID FROM focis.USERPROFILE WHERE UPPER(ROLES) LIKE '%SSPNPC%' AND COUNTRYID=:CountryId",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "CountryId",Value = CountryId},
                });

            return npcEmail;
        }

        public string GetUserEmailByQuotationNumber(string quotationnumber)
        {
            string userId = this.ExecuteScalar<string>(DBConnectionMode.SSP, "select NVL(guestemail, createdby) from qmquotation where  quotationnumber=:QuotationNumber",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "QuotationNumber",Value = quotationnumber},
                });

            return userId;
        }

        public long GetStakeholderIdByUserId(string userId)
        {
            long stakeholderid = this.ExecuteScalar<long>(DBConnectionMode.SSP, "SELECT NVL(STAKEHOLDERID,0) FROM USERS WHERE USERID=:USERID",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "USERID",Value = userId},
                });

            return stakeholderid;
        }

        public List<QuotationComplainceEntity> GetComplaienceData()
        {
            List<QuotationComplainceEntity> RemoveDuplicateList = new List<QuotationComplainceEntity>();
            try
            {
                List<QuotationComplainceEntity> mResult = new List<QuotationComplainceEntity>();
                QuotationComplainceEntity ProductCategory = new QuotationComplainceEntity();
                string query = string.Empty;
                query = "select  productcategoryname,descrtiption from vw_chapterproductcategory where productcategoryname is not null";
                DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, query, new OracleParameter[] { 
                    new OracleParameter {
                            
                    } 
                });
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string ProductCategoryName = dt.Rows[i]["PRODUCTCATEGORYNAME"].ToString();
                        string DescriptionName = dt.Rows[i]["DESCRTIPTION"].ToString();
                        ProductCategory = mResult.Where(d => d.PRODUCTCATEGORYNAME == ProductCategoryName).FirstOrDefault();
                        if (ProductCategory == null)
                        {
                            ProductCategory = new QuotationComplainceEntity();
                            ProductCategory.PRODUCTCATEGORYNAME = dt.Rows[i]["PRODUCTCATEGORYNAME"].ToString();
                            ComplainceDescription compObj = new ComplainceDescription();
                            compObj.DescriptionName = dt.Rows[i]["DESCRTIPTION"].ToString();
                            ProductCategory.ComplainceDescription = new List<ComplainceDescription>();
                            ProductCategory.ComplainceDescription.Add(compObj);

                        }
                        else
                        {
                            ComplainceDescription compObj = new ComplainceDescription();
                            compObj.DescriptionName = DescriptionName;
                            if (!ProductCategory.ComplainceDescription.Contains(compObj))
                                ProductCategory.ComplainceDescription.Add(compObj);

                        }
                        mResult.Add(ProductCategory);

                    }
                    RemoveDuplicateList = mResult.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return RemoveDuplicateList;
        }

        public ResultsEntity<AmazonFCStates> GetAmazonFCStates()
        {
            ResultsEntity<AmazonFCStates> mResult = new ResultsEntity<AmazonFCStates>();
            List<AmazonFCStates> mList = new List<AmazonFCStates>();

            ReadText(DBConnectionMode.SSP, "SELECT distinct SUBDIVISIONID,SUBDIVISONNAME as NAME FROM SSPAMAZONFC",
                   new OracleParameter[] { }
                   , delegate(IDataReader r)
                   {
                       mList.Add(DataReaderMapToObject<AmazonFCStates>(r));
                   });

            mResult.Items = mList;
            return mResult;
        }

        public ResultsEntity<AmazonFBAWAREHOUSES> GetAmazonFBAWAREHOUSES(string SUBDIVISIONID)
        {
            ResultsEntity<AmazonFBAWAREHOUSES> mResult = new ResultsEntity<AmazonFBAWAREHOUSES>();
            List<AmazonFBAWAREHOUSES> mList = new List<AmazonFBAWAREHOUSES>();

            ReadText(DBConnectionMode.SSP, "SELECT CODE,CITYNAME as NAME,UNLOCATIONID as ID, ZIPCODE FROM SSPAMAZONFC where SUBDIVISIONID=:SUBDIVISIONID",
                   new OracleParameter[] { new OracleParameter { ParameterName = "SUBDIVISIONID", Value = SUBDIVISIONID } }
                   , delegate(IDataReader r)
                   {
                       mList.Add(DataReaderMapToObject<AmazonFBAWAREHOUSES>(r));
                   });

            mResult.Items = mList;
            return mResult;
        }

        public DataTable GETAMZINFO(string AMAZONFCCODE)
        {
            DataTable dt = new DataTable();
            string Query = "SELECT * FROM SSPAMAZONFC WHERE CODE=:CODE";
            dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { ParameterName = "CODE", Value = AMAZONFCCODE } });
            return dt;
        }
        public SSPDocumentsEntity GetMsdsdocdetails(string QUOTEID)
        {
            SSPDocumentsEntity mEntity = new SSPDocumentsEntity();
            ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE QUOTATIONID IN (SELECT QUOTATIONID FROM QMQUOTATION WHERE QUOTATIONNUMBER=:QUOTATIONNUMBER) AND SOURCE='SHIPAQUOTE'",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUOTATIONNUMBER", Value = QUOTEID } }
            , delegate(IDataReader r)
            {
                mEntity = DataReaderMapToObject<SSPDocumentsEntity>(r);
            });

            return mEntity;

        }

        public long UploadMSDSCertificate(SSPDocumentsEntity docentity, string p, string GUID)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.Text;

                    try
                    {
                        mCommand.CommandText = "SELECT SSP_DOCUMENTS_SEQ.NEXTVAL FROM DUAL";
                        docentity.DOCID = Convert.ToInt64(mCommand.ExecuteScalar());
                        mCommand.CommandText = @"Insert into SSPDOCUMENTS (DOCID,QUOTATIONID,FILENAME,FILEEXTENSION,FILESIZE,FILECONTENT,PAGEFLAG,SOURCE,DOCUMENTTEMPLATE,CONDMAND,DIRECTION,DOCUMENTNAME) values (:DOCID,:QUOTATIONID,:FILENAME,:FILEEXTENSION,:FILESIZE,:FILECONTENT,:PAGEFLAG,:SOURCE,:DOCUMENTTEMPLATE,:CONDMAND,:DIRECTION,:DOCUMENTNAME)";
                        mCommand.Parameters.Add("DOCID", docentity.DOCID);

                        //mCommand.Parameters.Add("DOCUMNETTYPE", docentity.DOCUMNETTYPE);
                        mCommand.Parameters.Add("QUOTATIONID", docentity.QUOTATIONID);
                        mCommand.Parameters.Add("FILENAME", docentity.FILENAME);
                        mCommand.Parameters.Add("FILEEXTENSION", docentity.FILEEXTENSION);
                        mCommand.Parameters.Add("FILESIZE", docentity.FILESIZE);
                        mCommand.Parameters.Add("FILECONTENT", docentity.FILECONTENT);
                        mCommand.Parameters.Add("SOURCE", "SHIPAQUOTE");
                        mCommand.Parameters.Add("DOCUMENTTEMPLATE", "NA");
                        mCommand.Parameters.Add("CONDMAND", "Mandatory");
                        mCommand.Parameters.Add("DIRECTION", "IMPORT");
                        mCommand.Parameters.Add("DOCUMENTNAME", " MSDS Certificate");
                        mCommand.Parameters.Add("PAGEFLAG", 1);
                        mCommand.ExecuteNonQuery();
                        mOracleTransaction.Commit();

                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
            return Convert.ToInt32(docentity.DOCID);

        }

        public async Task<string> GetQuotationNumberById(long quoteid)
        {
            string userId = this.ExecuteScalar<string>(DBConnectionMode.SSP, "select Quotationnumber from qmquotation where Quotationid=:Quotationid",
                    new OracleParameter[]{
                    new OracleParameter{ ParameterName = "Quotationid",Value = quoteid},
                });

            return await Task.Run(() => userId).ConfigureAwait(false);
        }

        public string GetNPCAndCountryManagerEmailsForNPC(string countryId = "")
        {
            string result = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        mCommand.Parameters.Clear();

                        if (countryId != "")
                            mCommand.Parameters.Add("PRM_COUNTRYIDS", countryId);
                        else
                            mCommand.Parameters.Add("PRM_COUNTRYIDS", DBNull.Value);


                        mCommand.Parameters.Add("PRM_ROLEIDS", "SSPCOUNTRYADMIN,SSPNPC");
                        mCommand.Parameters.Add("PRM_NOTIFICATIONTYPE", "CHARGESMISSING");

                        mCommand.Parameters.Add("PRM_EMAILLIST", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.CommandText = "USP_SSP_NOTIFICATIONEMAILIDS";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader UserStatus = ((OracleRefCursor)mCommand.Parameters["PRM_EMAILLIST"].Value).GetDataReader();
                        while (UserStatus.Read())
                        {
                            result = UserStatus[0].ToString();
                        }
                        mConnection.Close();

                        return result;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public int CheckUserRestriction(string userId)
        {
            int result = this.ExecuteScalar<int>(DBConnectionMode.SSP, "select COUNT(1) from FOCIS.SSPUSERSFILTER where  UPPER(USERID)=:USERID",
                 new OracleParameter[] { 
                new OracleParameter{ ParameterName = "USERID",Value = userId},
                });
            return result;
        }
        public void savesubscription(string email)
        {

            try
            {
                int Id = this.ExecuteScalar<int>(DBConnectionMode.SSP, "SELECT FOCSSP.SSPUSERSUBSCRIPTION_SEQ.NEXTVAL FROM DUAL",
              new OracleParameter[] { });
                this.ExecuteScalar<int>(DBConnectionMode.SSP, "INSERT INTO Usersubscription (ID,EMAIL, DATECREATED ) VALUES (:ID,:EMAIL,:DATECREATED)",
                    new OracleParameter[] { 
                     new OracleParameter{ ParameterName = "ID",Value = Id},
                     new OracleParameter{ ParameterName = "EMAIL",Value = email},
                     new OracleParameter{ ParameterName = "DATECREATED",Value = DateTime.Now},
                     
                });
            }
            catch (Exception ex)
            {

            }
        }

        public List<IncoTermsEntity> GetIncoTermsByMovementType(long MovementTypeId, long PartyTypeId)
        {
            List<IncoTermsEntity> mEntity = new List<IncoTermsEntity>();
            string movemenType = string.Empty;
            if (MovementTypeId == 1)
            {
                movemenType = "DOORTODOOR";
            }
            else if (MovementTypeId == 2)
            {
                movemenType = "PORTTOPORT";
            }
            else if (MovementTypeId == 3)
            {
                movemenType = "DOORTOPORT";
            }
            else if (MovementTypeId == 4)
            {
                movemenType = "PORTTODOOR";
            }
            string Query = "SELECT * FROM SHIPAINCOTERMS WHERE  PARTYTYPEID=:PARTYTYPEID AND " + movemenType.ToUpper() + "=1";
            ReadText(DBConnectionMode.SSP, Query,
                 new OracleParameter[] {
                    new OracleParameter {ParameterName = "PARTYTYPEID", Value =PartyTypeId } , 
                }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<IncoTermsEntity>(r));
            });
            return mEntity;
        }
    }
}