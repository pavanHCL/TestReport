﻿using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOCiS.SSP.DBFactory.Core;
namespace FOCiS.SSP.DBFactory.Factory
{
    public interface IQuotationDBFactory
    {
        int TemplateNameCheck(string TemplateName, string userId);
        string SaveTemplate(QuotationEntity obj, string TemplateName);
        int GetQuotationIdCount(long id);
        QuotationEntity GetById(long id);
        string GetRegisterVideo(string videoName, string VideoType="");
        long NoofQuotesForGuest(string email);
        DataTable Descriptionvalidation(Int64 OCountryId, Int64 DCountryId, string Description);
        DataSet ValidateMinWeight(string Countryid);
        DataSet GetShipmentItemsDetails(int OriginCountryId);
        long NoofQuotes(string Email);
        long QuoteLimit(string userId);
        void SubmitReasonforUnbook(long quotationId,long mquotationId,string reason);

    }
}
