﻿using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace FOCiS.SSP.DBFactory.Factory
{
    public class TrackingDBFactory : DBFactoryCore, ITrackingDBFactory, IDBFactory<long, TrackingEntity>, IDisposable
    {
        public List<PartyDetails> GetPartyDetails(string UserId, long Jobnumer)
        {
            List<PartyDetails> mEntity = new List<PartyDetails>();
            string Query = @"select ClientName,EMAILID,PHONE,PARTYTYPE,

                        (Select DATECREATED FROM (select DATECREATED from  a$sspjobdetails where  jobnumber=:JOBNUMBER "
             + "and STATEID='Payment Completed' ORDER BY datecreated) where ROWNUM=1) as PaymentCompleted,(Select DATECREATED from (select DATECREATED from  a$sspjobdetails where  jobnumber=:JOBNUMBER "
             + "and STATEID='Job Initiated' ORDER BY datecreated) where ROWNUM=1) as JobInitiated from SSPPARTYDETAILS where jobnumber=:JOBNUMBER ";
            ReadText(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { ParameterName="JOBNUMBER", Value= Jobnumer } 
            }, delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<PartyDetails>(r));
            });
            return mEntity;
        }

        public List<ShpimentEventComments> GetShipmentEventComments(string p, int Jobnumber, string ConsignmentId)
        {
            List<ShpimentEventComments> mEntity = new List<ShpimentEventComments>();

            string Query = "Select JOBNUMBER,EVENTNAME,COMMENTS,COMMENTEDDATE,COMMENTEDBY from  SHIPMENTEVENTCOMMENTS Where STATUS=1 and Jobnumber=:JOBNUMBER";

            ReadText(DBConnectionMode.SSP, Query,
            new OracleParameter[] { new OracleParameter { ParameterName = "JOBNUMBER", Value = Jobnumber } }
        , delegate(IDataReader r)
        {
            mEntity.Add(DataReaderMapToObject<ShpimentEventComments>(r));
        });
            return mEntity;
        }

        public void InsertShareComments(string EventName, string Comments, string Createdby, Int64 jobnumber, int falag)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {

                        if (falag == 1)
                        {
                            mCommand.CommandText = "Update SHIPMENTEVENTCOMMENTS set status=0 where EVENTNAME=:EVENTNAME and jobnumber=:JOBNUMBER";
                            mCommand.Parameters.Add("EVENTNAME", EventName);
                            mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                            mCommand.ExecuteNonQuery();
                            mCommand.Parameters.Clear();
                        }

                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;

                        mCommand.CommandText = "SELECT SHIPMENTEVENTCOMMENTS_SEQ.NEXTVAL FROM DUAL";
                        Int64 ID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = "Insert into SHIPMENTEVENTCOMMENTS(ID,JOBNUMBER,EVENTNAME,COMMENTS,STATUS,COMMENTEDBY,COMMENTEDDATE) values(:ID,:JOBNUMBER,:EVENTNAME,:COMMENTS,:STATUS,:COMMENTEDBY,:COMMENTEDDATE)";
                        mCommand.Parameters.Add("ID", ID);
                        mCommand.Parameters.Add("JOBNUMBER", jobnumber);
                        mCommand.Parameters.Add("EVENTNAME", EventName);
                        mCommand.Parameters.Add("COMMENTS", Comments);
                        mCommand.Parameters.Add("STATUS", Convert.ToInt64(1));
                        mCommand.Parameters.Add("COMMENTEDBY", Createdby);
                        mCommand.Parameters.Add("COMMENTEDDATE", DateTime.Now);

                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void InsertFeedBackComments(string business, string ConfirmGoods, string Schedule, string TeamSuppport, string Material, string Payment,
            string businessRatings, string ConfirmGoodsRatings, string ScheduleRatings, string TeamSuppportRatings, string MaterialRatings, string PaymentRatings, string jobnumber, string NPS)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {

                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;

                        mCommand.CommandText = "SELECT CUSTOMERFEEDBACK_SEQ.NEXTVAL FROM DUAL";
                        Int64 ID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = @"Insert into CUSTOMERFEEDBACK(ID,EASEOFBUSINESS,EASEOFBUSINESSRATING,SCHEDULECONFIRM,
                        SCHEDULECONFIRMRATINGS,MATERIALHANDLING,MATERIALHANDLINGRATINGS,RATECONFIRMATION,RATECONFIRMATIONRATINGS,
                        KTTEAMSUPPORT,KTTEAMSUPPORTRATINGS,PAYMENTPROCESSING,PAYMENTPROCESSINGRATINGS,JOBNUMBER,NPSRATING) 
                        values(
                        :ID,:EASEOFBUSINESS,:EASEOFBUSINESSRATING,:SCHEDULECONFIRM,:SCHEDULECONFIRMRATINGS,
                        :MATERIALHANDLING,:MATERIALHANDLINGRATINGS,:RATECONFIRMATION,:RATECONFIRMATIONRATINGS,
                        :KTTEAMSUPPORT,:KTTEAMSUPPORTRATINGS,:PAYMENTPROCESSING,:PAYMENTPROCESSINGRATINGS,:JOBNUMBER,:NPSRATING)";

                        mCommand.Parameters.Add("ID", Convert.ToInt32(ID));
                        mCommand.Parameters.Add("EASEOFBUSINESS", business);
                        mCommand.Parameters.Add("EASEOFBUSINESSRATING", Convert.ToInt32(businessRatings));
                        mCommand.Parameters.Add("SCHEDULECONFIRM", Schedule);
                        mCommand.Parameters.Add("SCHEDULECONFIRMRATINGS", Convert.ToInt32(ScheduleRatings));
                        mCommand.Parameters.Add("MATERIALHANDLING", Material);
                        mCommand.Parameters.Add("MATERIALHANDLINGRATINGS", Convert.ToInt32(MaterialRatings));
                        mCommand.Parameters.Add("RATECONFIRMATION", ConfirmGoods);
                        mCommand.Parameters.Add("RATECONFIRMATIONRATINGS", Convert.ToInt32(ConfirmGoodsRatings));
                        mCommand.Parameters.Add("KTTEAMSUPPORT", TeamSuppport);
                        mCommand.Parameters.Add("KTTEAMSUPPORTRATINGS", Convert.ToInt32(TeamSuppportRatings));
                        mCommand.Parameters.Add("PAYMENTPROCESSING", Payment);
                        mCommand.Parameters.Add("PAYMENTPROCESSINGRATINGS", Convert.ToInt32(PaymentRatings));
                        mCommand.Parameters.Add("JOBNUMBER", Convert.ToInt64(jobnumber));
                        mCommand.Parameters.Add("NPSRATING", Convert.ToInt64(NPS));
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public TrackingEntity GetById(long id)
        {
            throw new NotImplementedException();
        }

        public long Save(TrackingEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(TrackingEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(long key)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<TrackingEntity> GetTrackDetails(string UserID, string ConsignmentId, string Jobnumber)
        {
            List<TrackingEntity> mEntity = new List<TrackingEntity>();

            string Query = "Select * from UVW_SSP_TRACKINGINFO Where Jobnumber=:JOBNUMBER";


            DataTable dtTable = new DataTable();
            List<TrackingEntity> companyList = dtTable.ConvertToList<TrackingEntity>();

            ReadText(DBConnectionMode.SSP, Query,
            new OracleParameter[] { new OracleParameter { ParameterName = "JOBNUMBER", Value = Jobnumber } }
        , delegate(IDataReader r)
        {
            mEntity.Add(DataReaderMapToObject<TrackingEntity>(r));
        });

            return mEntity;
        }

        public ListTrackingEntity ReturnTrackEntityList(string UserID, string ConsignmentId, int Jobnumber, string status, int PageNo, string SearchString)
        {
            ListTrackingEntity te = new ListTrackingEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("LOC_Jobnumber", Jobnumber == 0 ? (object)null : Jobnumber);
                        mCommand.Parameters.Add("LOC_CONSIGNMENTID", ConsignmentId == "" ? null : ConsignmentId);
                        mCommand.Parameters.Add("Loc_Createdby", UserID);
                        mCommand.Parameters.Add("Loc_PageNo", PageNo);
                        mCommand.Parameters.Add("Loc_SearchString", SearchString);

                        mCommand.Parameters.Add("CUR_TRACKMAIN", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_TRACKPACK", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_TRACKEVENT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("TabStatus", status.ToUpper());

                        mCommand.CommandText = "USP_SSP_TRACKDETAILS_New";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_TRACKMAIN"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.TrackingEntity> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.TrackingEntity>(dt);

                        dt = new DataTable();
                        OracleDataReader reader2 = ((OracleRefCursor)mCommand.Parameters["CUR_TRACKPACK"].Value).GetDataReader();
                        dt.Load(reader2);
                        List<DBFactory.Entities.PackageDetailsEntity> mDPackageEntity = MyClass.ConvertToList<DBFactory.Entities.PackageDetailsEntity>(dt);

                        dt = new DataTable();
                        OracleDataReader reader3 = ((OracleRefCursor)mCommand.Parameters["CUR_TRACKEVENT"].Value).GetDataReader();
                        dt.Load(reader3);
                        List<DBFactory.Entities.Track> EntityTrack = MyClass.ConvertToList<DBFactory.Entities.Track>(dt);

                        reader1.Close();
                        reader2.Close();
                        reader3.Close();
                        mConnection.Close();

                        te.TrackingItems = mDBEntity;
                        te.PackageDetailsItems = mDPackageEntity;
                        te.TrackItems = EntityTrack;
                        return te;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public ListTrackingEntity ShipmentTracking(string UserID, string ConsignmentId, string BookingID)
        {
            ListTrackingEntity te = new ListTrackingEntity();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;

            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();
                        mCommand.Parameters.Add("LOC_CONSIGNMENTID", ConsignmentId == "" ? null : ConsignmentId);
                        mCommand.Parameters.Add("Loc_Createdby", UserID);
                        mCommand.Parameters.Add("Loc_BookingID", BookingID == "" ? null : BookingID);

                        mCommand.Parameters.Add("CUR_TRACKMAIN", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_TRACKPACK", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("CUR_TRACKEVENT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add("O_Success", OracleDbType.Int32).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_SSP_TRACKSHIPMENT";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        decimal CheckStatus = ((Oracle.DataAccess.Types.OracleDecimal)(mCommand.Parameters["O_Success"].Value)).Value;

                        if (CheckStatus == 0)
                            return te;

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_TRACKMAIN"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        List<DBFactory.Entities.TrackingEntity> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.TrackingEntity>(dt);

                        dt = new DataTable();
                        OracleDataReader reader2 = ((OracleRefCursor)mCommand.Parameters["CUR_TRACKPACK"].Value).GetDataReader();
                        dt.Load(reader2);
                        List<DBFactory.Entities.PackageDetailsEntity> mDPackageEntity = MyClass.ConvertToList<DBFactory.Entities.PackageDetailsEntity>(dt);

                        dt = new DataTable();
                        OracleDataReader reader3 = ((OracleRefCursor)mCommand.Parameters["CUR_TRACKEVENT"].Value).GetDataReader();
                        dt.Load(reader3);
                        List<DBFactory.Entities.Track> EntityTrack = MyClass.ConvertToList<DBFactory.Entities.Track>(dt);

                        reader1.Close();
                        reader2.Close();
                        reader3.Close();
                        mConnection.Close();

                        te.TrackingItems = mDBEntity;
                        te.PackageDetailsItems = mDPackageEntity;
                        te.TrackItems = EntityTrack;
                        return te;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// This function get Dash Bord and return as entity Anil G
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public List<DashBoardEntity> GetDashBoardData(string UserID)
        {

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {

                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        mCommand.Parameters.Clear();

                        mCommand.Parameters.Add("Loc_Createdby", UserID);
                        mCommand.Parameters.Add("CUR_DB", OracleDbType.RefCursor).Direction = ParameterDirection.Output;


                        mCommand.CommandText = "SSP_SP_GETDASHBORDDATA";
                        mCommand.CommandType = CommandType.StoredProcedure;
                        mCommand.ExecuteNonQuery();

                        OracleDataReader reader1 = ((OracleRefCursor)mCommand.Parameters["CUR_DB"].Value).GetDataReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader1);
                        reader1.Close();
                        mConnection.Close();

                        List<DBFactory.Entities.DashBoardEntity> mDBEntity = MyClass.ConvertToList<DBFactory.Entities.DashBoardEntity>(dt);
                        return mDBEntity;
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
        }

        public List<SSPDocumentsEntity> GetUploadedDocuments(string p, int JobNumber, string ConsignmentId)
        {
            List<SSPDocumentsEntity> mdocItems = new List<SSPDocumentsEntity>();

            //Document Details List
            if (JobNumber != 0)
            {
                ReadText(DBConnectionMode.SSP, "SELECT * FROM SSPDOCUMENTS WHERE  JOBNUMBER=:JOBNUMBER and PAGEFLAG=:PAGEFLAG ",
                     new OracleParameter[] {new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "JOBNUMBER", Value = JobNumber },
                                        new OracleParameter {OracleDbType = OracleDbType.Int32, ParameterName = "PAGEFLAG", Value = 1}
                }
                , delegate(IDataReader r)
                {
                    mdocItems.Add(DataReaderMapToObject<SSPDocumentsEntity>(r));
                });

            }

            return mdocItems;
        }

        public List<TrackStatusCount> GetTrackStatusCount(string p, string SearchString)
        {
            List<TrackStatusCount> mdocItems = new List<TrackStatusCount>();

            string mSearch = "";

            if (SearchString.ToLowerInvariant().Contains("airport"))
            {
                SearchString = Regex.Replace(SearchString.ToLowerInvariant(), "air", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase); //SearchString.ToUpperInvariant();
                SearchString = SearchString.ToUpperInvariant();
            }

            if (SearchString != null && SearchString != "")
            {
                SearchString = SearchString.ToUpperInvariant();
                mSearch = @" AND (UPPER(OPERATIONALJOBNUMBER) LIKE '%" + SearchString + "%' OR UPPER(CONSIGNMENTID) LIKE '%" + SearchString + "%' OR UPPER(ORIGINPLACENAME) LIKE '%" + SearchString + "%' OR UPPER(ORIGINPORTNAME) LIKE '%" + SearchString + "%' OR UPPER(DESTINATIONPLACENAME) LIKE '%" + SearchString + "%' OR UPPER(DESTINATIONPORTNAME) LIKE '%" + SearchString + "%' OR UPPER(PRODUCTNAME) LIKE '%" + SearchString + "%' OR UPPER(MOVEMENTTYPENAME) LIKE '%" + SearchString + "%')";

                ReadText(DBConnectionMode.SSP, @"Select Count(Trackstatus) as TrackCount,Trackstatus from UVW_SSP_TRACKINGINFO where 
                                            UPPER(Createdby)=:CREATEDBY" + mSearch + "group by Trackstatus  order by Trackstatus",
                new OracleParameter[] { new OracleParameter { ParameterName = "CREATEDBY", Value = p.ToUpper() } }
           , delegate(IDataReader r)
           {
               mdocItems.Add(DataReaderMapToObject<TrackStatusCount>(r));
           });
                return mdocItems;
            }
            else
            {

                ReadText(DBConnectionMode.SSP, @"Select Count(Trackstatus) as TrackCount,Trackstatus from UVW_SSP_TRACKINGINFO where 
                                            UPPER(Createdby)=:CREATEDBY group by Trackstatus  order by Trackstatus",
                    new OracleParameter[] { new OracleParameter { ParameterName = "CREATEDBY", Value = p.ToUpper() } }
               , delegate(IDataReader r)
               {
                   mdocItems.Add(DataReaderMapToObject<TrackStatusCount>(r));
               });
                return mdocItems;
            }

        }

        public void StoreMailCommunication(string Module, string SubModule, string REFERENCEID, string Sender, string RECEIVER, string CC, string BCC, string CreatedBy, byte[] HTMLBODY, string ATTACHEMENTIDS, string SUBJECT)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {

                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;

                        mCommand.CommandText = "SELECT SSPEMAILCOMHISTROY_SEQ.NEXTVAL FROM DUAL";
                        Int64 ID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = @"Insert into SSPEMAILCOMMUNICATIONHITORY(HISTORYID, MODULE, SUBMODULE, REFERENCEID, HTMLBODY, 
                                                SENDER, RECEIVER,CC,BCC, CREATEDBY, CREATEDAT,ATTACHEMENTIDS,SUBJECT) 
                        values(:HISTORYID, :MODULE, :SUBMODULE, :REFERENCEID, :HTMLBODY, :SENDER, :RECEIVER,:CC,:BCC, :CREATEDBY, :CREATEDAT,:ATTACHEMENTIDS,:SUBJECT)";

                        mCommand.Parameters.Add("HISTORYID", Convert.ToInt32(ID));
                        mCommand.Parameters.Add("MODULE", Module);
                        mCommand.Parameters.Add("SUBMODULE", SubModule);
                        mCommand.Parameters.Add("REFERENCEID", REFERENCEID);
                        mCommand.Parameters.Add("HTMLBODY", HTMLBODY);
                        mCommand.Parameters.Add("SENDER", Sender);
                        mCommand.Parameters.Add("RECEIVER", RECEIVER);
                        mCommand.Parameters.Add("CC", CC);
                        mCommand.Parameters.Add("BCC", BCC);
                        mCommand.Parameters.Add("CREATEDBY", CreatedBy);
                        mCommand.Parameters.Add("CREATEDAT", DateTime.Now);
                        mCommand.Parameters.Add("ATTACHEMENTIDS", ATTACHEMENTIDS);
                        mCommand.Parameters.Add("SUBJECT", SUBJECT);
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public DataTable GetMailCommunicationHistory(Int64 HISTORYID)
        {
            string Query = @"Select * from SSPEMAILCOMMUNICATIONHITORY where  HISTORYID=:HISTORYID";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { new OracleParameter { 
                                             OracleDbType = OracleDbType.Long, ParameterName = "HISTORYID", Value = HISTORYID } });

            return dt;
        }

        public void UpdateCommunicationHistory(long HISTORYID, string ModifiedBy, int RESEND)
        {
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {

                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.Text;

                        mCommand.CommandText = "SELECT SSPEMAILCOMHISTROY_SEQ.NEXTVAL FROM DUAL";
                        Int64 ID = Convert.ToInt64(mCommand.ExecuteScalar());

                        mCommand.CommandText = @"UPDATE SSPEMAILCOMMUNICATIONHITORY set RESEND=:RESEND , MODIFIEDBY=:MODIFIEDBY, MODIFIEDAT=:MODIFIEDAT WHERE HISTORYID=:HISTORYID";

                        mCommand.Parameters.Add("HISTORYID", Convert.ToInt32(ID));
                        mCommand.Parameters.Add("RESEND", (RESEND + 1));
                        mCommand.Parameters.Add("MODIFIEDBY", ModifiedBy);
                        mCommand.Parameters.Add("MODIFIEDAT", DateTime.Now);

                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        mOracleTransaction.Rollback();
                        throw;
                    }
                }
            }
        }
        public DataTable GetDocumnets(string DOCID)
        {


            string Query = @"SELECT * FROM SSPDOCUMENTS WHERE DOCID IN(:DOCID)";

            DataTable dt = ReturnDatatable(DBConnectionMode.SSP, Query, new OracleParameter[] { 
                new OracleParameter { ParameterName="DOCID", Value = DOCID } 
            });

            return dt;
        }

    }
}



#region CommentedCode
//public List<PackageDetailsEntity> GetPackageDetails(string UserId, string QutotaionNo, string ConsignmentID)
//{
//    List<PackageDetailsEntity> mEntity = new List<PackageDetailsEntity>();

//    string Query = "";
//    if (QutotaionNo == "" && ConsignmentID == "")
//    {
//        Query = "Select * from UVW_GETPACKAGEDETAILS where upper(Createdby)=upper('" + UserId + "' )";
//    }
//    else
//        Query = "Select * from UVW_GETPACKAGEDETAILS Where Jobnumber='" + QutotaionNo + "' and upper(Createdby)=upper('" + UserId + "' )";// and Createdby=" + "'" + UserId + "' and ConsignmentID='" + ConsignmentID + "'";



//    ReadText(DBConnectionMode.SSP, Query,
//    new OracleParameter[] { new OracleParameter { } }
//, delegate(IDataReader r)
//{
//    mEntity.Add(DataReaderMapToObject<PackageDetailsEntity>(r));
//});




//    return mEntity;
//}

//public List<TrackingEntity> GetTrackingInfoDetails(string UserId, string QutotaionNo, string ConsignmentID)
//{
//    List<TrackingEntity> mEntity = new List<TrackingEntity>();

//    string Query = "";
//    if (QutotaionNo == "" && ConsignmentID == "")
//    {
//        Query = "Select * from UVW_SSP_TRACKINGINFO where upper(Createdby)=upper('" + UserId + "' )";
//    }
//    else
//        Query = "Select * from UVW_SSP_TRACKINGINFO Where Jobnumber='" + QutotaionNo + "' and upper(Createdby)=upper('" + UserId + "' )";


//    DataTable dtTable = new DataTable();
//    List<TrackingEntity> companyList = dtTable.ConvertToList<TrackingEntity>();

//    ReadText(DBConnectionMode.SSP, Query,
//    new OracleParameter[] { new OracleParameter { } }
//, delegate(IDataReader r)
//{
//    mEntity.Add(DataReaderMapToObject<TrackingEntity>(r));
//});

//    return mEntity;
//}

//public List<Track> GetTrack(string UserID, string CONSIGNMENTID)
//{
//    List<Track> mentrack = new List<Track>();

//    string Query = "";

//    if (CONSIGNMENTID != "")
//    {
//        Query = @"Select * from UVW_SSP_TRACKEVENTS where CONSIGNMENTID = '" + CONSIGNMENTID + "'";
//    }
//    else
//    {
//        Query = @"Select * from  UVW_SSP_TRACKEVENTS where createdby='" + UserID + "'";
//    }

//    ReadText(DBConnectionMode.SSP, Query,
//    new OracleParameter[] { new OracleParameter { } }
//, delegate(IDataReader r)
//{
//    mentrack.Add(DataReaderMapToObject<Track>(r));
//});

//    return mentrack;
//}
#endregion