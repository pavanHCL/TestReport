﻿using FOCiS.SSP.DBFactory.Core;
using FOCiS.SSP.DBFactory.Entities;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Oracle.DataAccess.Types;
using System.Threading.Tasks;
using System.Collections;
using System.Dynamic;

namespace FOCiS.SSP.DBFactory.Factory
{
    public partial class MDMDataFactory : DBFactoryCore, IMDMDataFactory
    {

        public PagedResultsEntity<Select2Entity<string>> FindCurrency(int pageNumber, int pageSize, string s)
        {
            PagedResultsEntity<Select2Entity<string>> mResult = new PagedResultsEntity<Select2Entity<string>>();
            mResult.Page = pageNumber;
            mResult.PageSize = pageSize;

            List<Select2Entity<string>> mList = new List<Select2Entity<string>>();

            mResult.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, "SELECT COUNT(1) TOTALROWS FROM CURRENCIES WHERE CURRENCYID LIKE :QUERY || '%' OR CURRENCYNAME LIKE :QUERY || '%'",
                new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "QUERY", Value = s.ToUpperInvariant() } });

            ReadText(DBConnectionMode.FOCiS, "SELECT CURRENCYCODE ID, CURRENCYNAME TEXT,DECIMALS DecimalPoint FROM ( SELECT CURRENCYCODE, CURRENCYNAME,DECIMALS, RANK() OVER (ORDER BY CURRENCYCODE) ROWNUMBER FROM CURRENCIES WHERE CURRENCYCODE NOT IN('MXV','BOV','XDR','SSP','XSU','CLF','COU','UYI','CHW','ZWL','CHE','SGP') AND UPPER(CURRENCYID) LIKE :QUERY || '%' OR UPPER(CURRENCYNAME) LIKE :QUERY || '%' ORDER BY CURRENCYCODE ) WHERE ROWNUMBER BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ",
               new OracleParameter[] { 
                   new OracleParameter { OracleDbType = OracleDbType.Varchar2, Size = s.Length, ParameterName = "QUERY", Value = s.ToUpperInvariant().Trim() } ,
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mResult.PageSize } , 
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mResult.Page } 
               }
           , delegate(IDataReader r)
           {
               mList.Add(DataReaderMapToObject<Select2Entity<string>>(r));
           });

            mResult.Items = mList;
            return mResult;
        }

        public PagedResultsEntity<Select3Entity<Int64>> FindCountry(string s, string CountryId)
        {
            PagedResultsEntity<Select3Entity<Int64>> mResult = new PagedResultsEntity<Select3Entity<Int64>>();
            List<Select3Entity<Int64>> mList = new List<Select3Entity<Int64>>();

            if (s == null || s == "null" || s == "")
            {
                ReadText(DBConnectionMode.FOCiS, "SELECT C.COUNTRYID ID, C.CODE CODE, C.NAME TEXT,(SELECT ISZIPCODE FROM SSPRATEMARGINCONFIG WHERE COUNTRYID = :COUNTRYID) AS ISZIPCODE FROM SMDM_COUNTRY C  WHERE COUNTRYID = :COUNTRYID AND STATEID = 'CountryActivatedState' ",
                   new OracleParameter[] { 
                   new OracleParameter { OracleDbType = OracleDbType.Varchar2,  ParameterName = "COUNTRYID", Value = CountryId } //Size = s.Length,
               }, delegate(IDataReader r)
               {
                   mList.Add(DataReaderMapToObject<Select3Entity<Int64>>(r));
               });
                mResult.Items = mList;
            }
            else
            {
                ReadText(DBConnectionMode.FOCiS, "select aa.*,(select ISZIPCODE from SSPRATEMARGINCONFIG where SSPRATEMARGINCONFIG.COUNTRYID=aa.ID) as ISZipCode FROM (SELECT C.COUNTRYID ID, C.CODE CODE, C.NAME TEXT FROM SMDM_COUNTRY C  WHERE (UPPER(CODE) LIKE :QUERY || '%' OR UPPER(NAME) LIKE :QUERY || '%') AND STATEID = 'CountryActivatedState' AND COUNTRYID NOT IN (108,51,211,0,196,121, :COUNTRYID ))AA",
                   new OracleParameter[] { 
                   new OracleParameter { OracleDbType = OracleDbType.Varchar2, Size = s.Length, ParameterName = "QUERY", Value = s.ToUpperInvariant().Trim() },
                     new OracleParameter { ParameterName = "COUNTRYID", Value = CountryId } 
               }, delegate(IDataReader r)
                   {
                       mList.Add(DataReaderMapToObject<Select3Entity<Int64>>(r));
                   });
                mResult.Items = mList;
            }
            return mResult;
        }

        public DataTable FindCurrencyData(string s)
        {
            string Query = "SELECT CURRENCYCODE ID, CURRENCYNAME TEXT FROM ( SELECT CURRENCYCODE, CURRENCYNAME, RANK() OVER (ORDER BY CURRENCYCODE) ROWNUMBER FROM CURRENCIES WHERE UPPER(CURRENCYID) LIKE :QUERY || '%' OR UPPER(CURRENCYNAME) LIKE :QUERY || '%' ORDER BY CURRENCYCODE )";
            DataTable dt = ReturnDatatable(DBConnectionMode.FOCiS, Query, new OracleParameter[] { new OracleParameter 
                                            { OracleDbType = OracleDbType.Varchar2, ParameterName = "QUERY", Value = s.ToUpperInvariant().Trim()  } });
            return dt;
        }

        public PagedResultsEntity<Select2Entity<string>> GetResult(DataTable dt)
        {
            PagedResultsEntity<Select2Entity<string>> mResult = new PagedResultsEntity<Select2Entity<string>>();
            List<Select2Entity<string>> mList = new List<Select2Entity<string>>();


            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select2Entity<string>>(dr));
            }

            mResult.Items = mList;
            return mResult;

        }
        public ResultsEntity<ValidationEntity<string>> GetValidationResult(DataTable dt)
        {
            ResultsEntity<ValidationEntity<string>> mResult = new ResultsEntity<ValidationEntity<string>>();
            List<ValidationEntity<string>> mList = new List<ValidationEntity<string>>();

            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<ValidationEntity<string>>(dr));
            }

            mResult.Items = mList;
            return mResult;

        }
        public ResultsEntity<SelectEntity<string>> GetR(DataTable dt)
        {
            ResultsEntity<SelectEntity<string>> mResult = new ResultsEntity<SelectEntity<string>>();
            List<SelectEntity<string>> mList = new List<SelectEntity<string>>();


            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<SelectEntity<string>>(dr));
            }

            mResult.Items = mList;
            return mResult;
        }

        public PagedResultsEntity<Select3Entity<string>> GetResult3(DataTable dt)
        {
            PagedResultsEntity<Select3Entity<string>> mResult = new PagedResultsEntity<Select3Entity<string>>();
            List<Select3Entity<string>> mList = new List<Select3Entity<string>>();


            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select3Entity<string>>(dr));
            }

            mResult.Items = mList;
            return mResult;

        }

        public PagedResultsEntity<Select4Entity<string>> GetResult4(DataTable dt)
        {
            PagedResultsEntity<Select4Entity<string>> mResult = new PagedResultsEntity<Select4Entity<string>>();
            List<Select4Entity<string>> mList = new List<Select4Entity<string>>();

            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select4Entity<string>>(dr));
            }
            mResult.Items = mList;
            return mResult;
        }

        public PagedResultsEntity<Select7Entity<string>> GetResult7(DataTable dt)
        {
            PagedResultsEntity<Select7Entity<string>> mResult = new PagedResultsEntity<Select7Entity<string>>();
            List<Select7Entity<string>> mList = new List<Select7Entity<string>>();

            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select7Entity<string>>(dr));
            }
            mResult.Items = mList;
            return mResult;
        }

        public MultipleEntity<Select4Entity<string>> GetResultMul(DataTable dt, DataTable dt1, DataTable DtPack)
        {
            MultipleEntity<Select4Entity<string>> mResult = new MultipleEntity<Select4Entity<string>>();
            List<Select4Entity<string>> mList = new List<Select4Entity<string>>();

            foreach (DataRow dr in dt.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select4Entity<string>>(dr));
            }
            mResult.LenUOMData = mList;

            mList = new List<Select4Entity<string>>();

            foreach (DataRow dr in dt1.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select4Entity<string>>(dr));
            }
            mResult.WgtUOMData = mList;


            mList = new List<Select4Entity<string>>();
            foreach (DataRow dr in DtPack.Rows)
            {
                mList.Add(DataReaderMapToObjectJson<Select4Entity<string>>(dr));
            }
            mResult.PkgTypes = mList;

            return mResult;
        }

        private static T1 DataReaderMapToObjectJson<T1>(DataRow Data)
        {
            T1 objj = default(T1);
            objj = Activator.CreateInstance<T1>();
            string mPropertyName = string.Empty;
            try
            {
                foreach (PropertyInfo prop in objj.GetType().GetProperties())
                {
                    mPropertyName = prop.Name;
                    try
                    {
                        if (!object.Equals(Data[mPropertyName], DBNull.Value))
                            prop.SetValue(objj, Data[mPropertyName], null);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        //
                    }
                }
            }
            catch (ArgumentException ax)
            {
                throw new ArgumentException(mPropertyName + " : " + ax.Message, ax);
            }

            return objj;
        }

        public PagedResultsEntity<dynamic> FindUnlocationWithoutCode(int pageNumber, int pageSize, string s, string f, string countryid, string xcountryid)
        {
            PagedResultsEntity<dynamic> mResult = new PagedResultsEntity<dynamic>();
            mResult.Page = pageNumber;
            mResult.PageSize = pageSize;

            string mQuery = string.Empty, wQuery = string.Empty, uString = string.Empty;
            string nameTransl = string.Empty, alternateTransl = string.Empty;

            #region"Translate"
            nameTransl = "UPPER(NAME)";
            alternateTransl = "UPPER(ALTERNATENAMES)";
            #endregion

            uString = s.ToUpperInvariant().Trim() + '%';

            #region "MovementType Condition"
            string mSep = "";
            for (int i = 0; i < f.Length; i++)
            {
                switch (f.ToUpperInvariant()[i])
                {
                    case '1':
                        mQuery = mQuery + mSep + " ISPORT = '1'";
                        break;
                    case '2':
                        mQuery = mQuery + mSep + " ISRAILTERMINAL = '1'";
                        break;
                    case '3':
                        mQuery = mQuery + mSep + " ISROADTERMINAL = '1'";
                        break;
                    case '4':
                        mQuery = mQuery + mSep + " ISAIRPORT = '1'";
                        break;
                    case '5':
                        mQuery = mQuery + mSep + " ISPOSTALEXCHANGEOFFICE = '1'";
                        break;
                    case '6':
                        mQuery = mQuery + mSep + " ISMULTIMODAL = '1'";
                        break;
                    case '8':
                        mQuery = mQuery + mSep + " ISCARRIERCFS = '1'";
                        break;
                    case '9':
                        mQuery = mQuery + mSep + " ISCITY = '1'";
                        break;
                    case '0':
                        mQuery = mQuery + mSep + " ISAIRPORT = '0'";
                        break;
                    default:
                        break;
                }
                mSep = " OR ";
            }
            #endregion

            if (mQuery.Length > 0)
            {
                mQuery = "AND (" + mQuery + ")";
                if (countryid == "0")
                {
                    mQuery = mQuery + " AND COUNTRYID NOT IN (108,51,211,0,196,121," + xcountryid + ") AND STATEID = 'UNLocationsActivatedState' ";
                }
                else
                {
                    mQuery = mQuery + " AND COUNTRYID IN (" + countryid + ") AND STATEID = 'UNLocationsActivatedState'";
                }
            }

            wQuery = " WHERE (UPPER(CODE) LIKE '" + uString + "' OR " + nameTransl + " LIKE '" + uString + "' OR " + alternateTransl + " LIKE  '" + uString + "') ";

            mResult.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, "SELECT COUNT(1) TOTALROWS FROM UNLOCATIONS " + wQuery + mQuery, null);

            IEnumerable<dynamic> mResults = ReadDynamic(DBConnectionMode.FOCiS, "SELECT UNLOCATIONID id, NAME text, code ,SUBDIVISIONCODE subcode,COUNTRYID countryid,COUNTRYCODE countrycode FROM ( SELECT UNLOCATIONID, CODE, NAME, SUBDIVISIONNAME AS SUBDIVISIONCODE,COUNTRYID,COUNTRYCODE, RANK() OVER (ORDER BY NAME) ROWNUMBER FROM UNLOCATIONS" + wQuery + mQuery + " ORDER BY NAME ) WHERE ROWNUMBER BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ",
               new OracleParameter[] { 
                   //new OracleParameter { OracleDbType = OracleDbType.Varchar2, Size = s.Length, ParameterName = "QUERY", Value = s.ToUpperInvariant().Trim() } ,
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mResult.PageSize } , 
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mResult.Page } 
               }, CommandType.Text);

            mResult.Items = mResults;
            return mResult;
        }

        public PagedResultsEntity<dynamic> FindUnlocation(int pageNumber, int pageSize, string s, string f, string countryid, string xcountryid)
        {
            PagedResultsEntity<dynamic> mResult = new PagedResultsEntity<dynamic>();
            mResult.Page = pageNumber;
            mResult.PageSize = pageSize;

            string mQuery = string.Empty, wQuery = string.Empty, uString = string.Empty;
            string nameTransl = string.Empty, alternateTransl = string.Empty;

            #region"Translate"
            nameTransl = "UPPER(NAME)";
            alternateTransl = "UPPER(ALTERNATENAMES)";
            #endregion

            uString = s.ToUpperInvariant().Trim() + '%';

            #region "MovementType Condition"
            string mSep = "";
            for (int i = 0; i < f.Length; i++)
            {
                switch (f.ToUpperInvariant()[i])
                {
                    case '1':
                        mQuery = mQuery + mSep + " ISPORT = '1' AND ISMAINPORT = '1'";
                        break;
                    case '2':
                        mQuery = mQuery + mSep + " ISRAILTERMINAL = '1'";
                        break;
                    case '3':
                        mQuery = mQuery + mSep + " ISROADTERMINAL = '1'";
                        break;
                    case '4':
                        mQuery = mQuery + mSep + " ISAIRPORT = '1' AND ISMAINPORT = '1'";
                        break;
                    case '5':
                        mQuery = mQuery + mSep + " ISPOSTALEXCHANGEOFFICE = '1'";
                        break;
                    case '6':
                        mQuery = mQuery + mSep + " ISMULTIMODAL = '1'";
                        break;
                    case '8':
                        mQuery = mQuery + mSep + " ISCARRIERCFS = '1'";
                        break;
                    case '9':
                        mQuery = mQuery + mSep + " ISCITY = '1'";
                        break;
                    case '0':
                        mQuery = mQuery + mSep + " ISAIRPORT = '0'";
                        break;
                    default:
                        break;
                }
                mSep = " OR ";
            }
            #endregion

            if (mQuery.Length > 0)
            {
                mQuery = "AND (" + mQuery + ")";
                if (countryid == "0")
                {
                    mQuery = mQuery + " AND COUNTRYID NOT IN (108,51,211,0,196,121," + xcountryid + ") AND STATEID = 'UNLocationsActivatedState' ";
                }
                else
                {
                    mQuery = mQuery + " AND COUNTRYID IN (" + countryid + ") AND STATEID = 'UNLocationsActivatedState'";
                }
            }
            wQuery = " WHERE (UPPER(CODE) LIKE '" + uString + "' OR " + nameTransl + " LIKE '" + uString + "' OR " + alternateTransl + " LIKE  '" + uString + "') ";

            mResult.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, "SELECT COUNT(1) TOTALROWS FROM UNLOCATIONS " + wQuery + mQuery, null);

            IEnumerable<dynamic> mResults = ReadDynamic(DBConnectionMode.FOCiS, "SELECT UNLOCATIONID id, NAME text, code ,SUBDIVISIONCODE subcode,COUNTRYID countryid,COUNTRYCODE countrycode FROM ( SELECT UNLOCATIONID, CODE, NAME, SUBDIVISIONNAME AS SUBDIVISIONCODE,COUNTRYID,COUNTRYCODE, RANK() OVER (ORDER BY NAME) ROWNUMBER FROM UNLOCATIONS" + wQuery + mQuery + " ORDER BY NAME ) WHERE ROWNUMBER BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ",
               new OracleParameter[] { 
                   //new OracleParameter { OracleDbType = OracleDbType.Varchar2, Size = s.Length, ParameterName = "QUERY", Value = s.ToUpperInvariant().Trim() } ,
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mResult.PageSize } , 
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mResult.Page } 
               }, CommandType.Text);

            mResult.Items = mResults;
            return mResult;
        }

        public PagedResultsEntity<dynamic> FindUnlocDetails(int pageNumber, int pageSize, string s, string f, string countryid, string xcountryid)
        {
            PagedResultsEntity<dynamic> mResult = new PagedResultsEntity<dynamic>();
            mResult.Page = pageNumber;
            mResult.PageSize = pageSize;
            OracleParameter op = null;
            DataTable dt = new DataTable();
            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {
                        mCommand.CommandText = "USP_SSP_GETUNLOCATIONDTLS";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_PAGESIZE", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = pageSize;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_PAGE", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = pageNumber;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_USERTEXT", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = s;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_HRDMOVEMENTTYPE", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = f;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_USERSELCOUNTRYID", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = countryid;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_OTHERCOUNTRYID", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = xcountryid;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_TOTALROWS", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_LOCDTLS", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        string totalRows = Convert.ToString(mCommand.Parameters["OP_TOTALROWS"].Value);
                        mResult.TotalRows = Convert.ToInt32(totalRows);

                        OracleDataAdapter da = new OracleDataAdapter(mCommand);
                        DataSet ds = new DataSet();
                        da.Fill(dt);
                        DataView view = dt.DefaultView;
                        view.Sort = "SORTORDER";
                        dt = view.Table;
                        IEnumerable<UnLocationsCities> items = new UnLocationsCities[] { };
                        List<Object> objlist = new List<Object>();

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            UnLocationsCities objUnloctions = new UnLocationsCities();
                            objUnloctions.id = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
                            objUnloctions.code = dt.Rows[i]["CODE"].ToString();
                            objUnloctions.text = dt.Rows[i]["TEXT"].ToString();
                            objUnloctions.subcode = dt.Rows[i]["SUBCODE"].ToString();
                            objUnloctions.countryid = Convert.ToInt32(dt.Rows[i]["COUNTRYID"].ToString());
                            objUnloctions.countrycode = dt.Rows[i]["COUNTRYCODE"].ToString();
                            objUnloctions.isalternatesearch = dt.Rows[i]["ISALTERNATESRCH"].ToString();
                            objUnloctions.sortorder = dt.Rows[i]["SORTORDER"].ToString();
                            objlist.Add(objUnloctions);
                        }
                        mResult.Items = objlist.AsEnumerable<dynamic>();

                    }
                    catch (Exception EX)
                    {
                        //
                    }
                }

            }
            return mResult;
        }

        public PagedResultsEntity<dynamic> FindDynamicUnlocDetails(int pageNumber, int pageSize, string search, string f, string countryid, string xcountryid, string transportType, string packageType, string direction)
        {
            PagedResultsEntity<dynamic> mResult = new PagedResultsEntity<dynamic>();
            mResult.Page = pageNumber;
            mResult.PageSize = pageSize;
            string dynamicQuery = string.Empty;
            if (f == "9") {
                dynamicQuery = "ISCITY=1";
            }
            if (f == "10")
            {
                if (transportType == "1")
                {
                    if (packageType == "LCL")
                    {
                        dynamicQuery = "(ISPORT = 1 OR ISCARRIERCFS='1') AND ISLCLMAINPORT  = 1 AND (PORT_LCL_DIRECTION =0 OR PORT_LCL_DIRECTION=" + ((direction == "EXPORT") ? -1118 : 1117) + ")";
                       
                    }
                    if (packageType == "FCL")
                    {
                        dynamicQuery = " ISPORT = 1 AND ISFCLMAINPORT = 1 AND (PORT_FCL_DIRECTION  =0 OR PORT_FCL_DIRECTION =" + ((direction == "EXPORT") ? -1118 : 1117) + ")";
                    }
                    //dynamicQuery += " AND (PORT_DIRECTION =0 OR PORT_DIRECTION=" + ((direction == "EXPORT") ? -1118 : 1117)+")";
                   
                }
                if (transportType == "4")
                {
                    dynamicQuery = " ISAIRPORT = 1 AND ISAIRMAINPORT  = 1 AND (PORT_DIRECTION =0 OR PORT_DIRECTION=" + ((direction == "EXPORT") ? -1118 : 1117) + ")";
                }
                pageSize = 99999;
            }
            OracleParameter op = null;
            DataTable dt = new DataTable();
            using (OracleConnection mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {
                        mCommand.CommandText = "USP_SSP_GETUNLOCATIONDTLS_WEB";
                        mCommand.CommandType = CommandType.StoredProcedure;

                        op = new OracleParameter("IP_PAGESIZE", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = pageSize;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_PAGE", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = pageNumber;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_USERTEXT", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = search;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_USERSELCOUNTRYID", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = countryid;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_OTHERCOUNTRYID", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Input;
                        op.Value = xcountryid;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("IP_WHRCOND", OracleDbType.Varchar2);
                        op.Direction = ParameterDirection.Input;
                        op.Value = dynamicQuery;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_TOTALROWS", OracleDbType.Int32);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);

                        op = new OracleParameter("OP_LOCDTLS", OracleDbType.RefCursor);
                        op.Direction = ParameterDirection.Output;
                        mCommand.Parameters.Add(op);
                        mCommand.ExecuteNonQuery();

                        string totalRows = Convert.ToString(mCommand.Parameters["OP_TOTALROWS"].Value);
                        mResult.TotalRows = Convert.ToInt32(totalRows);

                        OracleDataAdapter da = new OracleDataAdapter(mCommand);
                        DataSet ds = new DataSet();
                        da.Fill(dt);
                        DataView view = dt.DefaultView;
                        view.Sort = "SORTORDER";
                        dt = view.Table;
                        IEnumerable<UnLocationsCities> items = new UnLocationsCities[] { };
                        List<Object> objlist = new List<Object>();

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            UnLocationsCities objUnloctions = new UnLocationsCities();
                            objUnloctions.id = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
                            objUnloctions.code = dt.Rows[i]["CODE"].ToString();
                            objUnloctions.text = dt.Rows[i]["TEXT"].ToString();
                            objUnloctions.subcode = dt.Rows[i]["SUBCODE"].ToString();
                            objUnloctions.countryid = Convert.ToInt32(dt.Rows[i]["COUNTRYID"].ToString());
                            objUnloctions.countrycode = dt.Rows[i]["COUNTRYCODE"].ToString();
                            objUnloctions.isalternatesearch = dt.Rows[i]["ISALTERNATESRCH"].ToString();
                            objUnloctions.sortorder = dt.Rows[i]["SORTORDER"].ToString();
                            objlist.Add(objUnloctions);
                        }
                        mResult.Items = objlist.AsEnumerable<dynamic>();

                    }
                    catch (Exception EX)
                    {
                        //
                    }
                }

            }
            return mResult;
        }

        public PagedResultsEntity<dynamic> GetAllPortsbyCountry(string f, string countryid)
        {
            PagedResultsEntity<dynamic> mResult = new PagedResultsEntity<dynamic>();

            string mQuery = string.Empty;
            string mSep = "";
            for (int i = 0; i < f.Length; i++)
            {
                switch (f.ToUpperInvariant()[i])
                {
                    case '1':
                        mQuery = mQuery + mSep + " ISPORT = '1' AND ISMAINPORT = '1'";
                        break;
                    case '2':
                        mQuery = mQuery + mSep + " ISRAILTERMINAL = '1'";
                        break;
                    case '3':
                        mQuery = mQuery + mSep + " ISROADTERMINAL = '1'";
                        break;
                    case '4':
                        mQuery = mQuery + mSep + " ISAIRPORT = '1' AND ISMAINPORT = '1'";
                        break;
                    case '5':
                        mQuery = mQuery + mSep + " ISPOSTALEXCHANGEOFFICE = '1'";
                        break;
                    case '6':
                        mQuery = mQuery + mSep + " ISMULTIMODAL = '1'";
                        break;
                    case '8':
                        mQuery = mQuery + mSep + " ISCARRIERCFS = '1'";
                        break;
                    case '9':
                        mQuery = mQuery + mSep + " ISCITY = '1'";
                        break;
                    case '0':
                        mQuery = mQuery + mSep + " ISAIRPORT = '0'";
                        break;
                    default:
                        break;
                }
                mSep = " OR ";
            }
            if (mQuery.Length > 0)
            {
                mQuery = "AND (" + mQuery + ")";
                mQuery = mQuery + " AND STATEID = 'UNLocationsActivatedState'";
            }

            IEnumerable<dynamic> mResults = ReadDynamic(DBConnectionMode.SSP, "SELECT UNLOCATIONID, CODE, NAME, SUBDIVISIONNAME AS SUBDIVISIONCODE,COUNTRYID,COUNTRYCODE, RANK() OVER (ORDER BY NAME) ROWNUMBER FROM UNLOCATIONS WHERE COUNTRYID=:QUERY " + mQuery + " ORDER BY NAME",
               new OracleParameter[] {
                   new OracleParameter { OracleDbType = OracleDbType.Long, Size = 10, ParameterName = "QUERY", Value = countryid }                
               }, CommandType.Text);

            mResult.Items = mResults;
            return mResult;
        }

        public List<PackageTypesEntity> GetPackageTypes(string Code, string Name)
        {
            List<PackageTypesEntity> mEntity = new List<PackageTypesEntity>();
            string Query = "select PACKAGETYPEID,TYPEID,CODE,UNALPHACODE,UNNUMERICCODE,NAME,DESCRIPTION " +
                    "from SMDM_PACKAGETYPE PT Inner Join DataProfileClassWFStates DP on PT.STATEID=DP.STATEID where 1=1";
            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(PT.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(PT.NAME) like '%" + Name.ToUpper() + "%'";
            }
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<PackageTypesEntity>(r));
            });
            return mEntity;
        }

        public PagedResultsEntity<PackageTypesEntity> GetPackageTypesPopup(int pageNumber, int pageSize, string Code, string Name)
        {
            PagedResultsEntity<PackageTypesEntity> mEntity = new PagedResultsEntity<PackageTypesEntity>();
            mEntity.Page = pageNumber;
            mEntity.PageSize = pageSize;

            List<PackageTypesEntity> mList = new List<PackageTypesEntity>();

            string Query = "select PACKAGETYPEID,TYPEID,CODE,UNALPHACODE,UNNUMERICCODE,NAME,DESCRIPTION " +
                    "from SMDM_PACKAGETYPE PT Inner Join DataProfileClassWFStates DP on PT.STATEID=DP.STATEID where 1=1";
            string CntQuery = "SELECT COUNT(1) from SMDM_PACKAGETYPE PT Inner Join DataProfileClassWFStates DP on PT.STATEID=DP.STATEID where 1=1 ";
            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(PT.Code) like '%" + Code.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(PT.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(PT.NAME) like '%" + Name.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(PT.NAME) like '%" + Name.ToUpper() + "%'";
            }
            Query = Query + " AND ROWNUM BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ";
            mEntity.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, CntQuery, new OracleParameter[] { });

            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mEntity.PageSize } , 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mEntity.Page } 
                }
            , delegate(IDataReader r)
            {
                mList.Add(DataReaderMapToObject<PackageTypesEntity>(r));
            });
            return mEntity;
        }

        public List<CountryEntity> GetCountries(string Code, string Name)
        {
            List<CountryEntity> mEntity = new List<CountryEntity>();
            string Query = "select C.COUNTRYID,C.CODE,C.NAME,C.DESCRIPTION,C.BASECURRENCYID " +
                            "from SMDM_COUNTRY C Inner Join DataProfileClassWFStates DP on C.STATEID=DP.STATEID Where 1=1 ";
            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(C.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(C.NAME) like '%" + Name.ToUpper() + "%'";
            }
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<CountryEntity>(r));
            });
            return mEntity;
        }

        public PagedResultsEntity<CountryEntity> GetCountriesPopup(int pageNumber, int pageSize, string Code, string Name)
        {
            PagedResultsEntity<CountryEntity> mEntity = new PagedResultsEntity<CountryEntity>();
            mEntity.Page = pageNumber;
            mEntity.PageSize = pageSize;

            List<CountryEntity> mList = new List<CountryEntity>();

            string Query = "select C.COUNTRYID,C.CODE,C.NAME,C.DESCRIPTION,C.BASECURRENCYID " +
                            "from SMDM_COUNTRY C Inner Join DataProfileClassWFStates DP on C.STATEID=DP.STATEID Where 1=1";
            string CntQuery = "SELECT COUNT(1) from SMDM_COUNTRY C Inner Join DataProfileClassWFStates DP on C.STATEID=DP.STATEID Where 1=1";

            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(C.Code) like '%" + Code.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(C.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(C.NAME) like '%" + Name.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(C.NAME) like '%" + Name.ToUpper() + "%'";
            }
            Query = Query + " AND ROWNUM BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ";
            mEntity.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, CntQuery, new OracleParameter[] { });
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] {
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mEntity.PageSize } , 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mEntity.Page } 
                }
            , delegate(IDataReader r)
            {
                mList.Add(DataReaderMapToObject<CountryEntity>(r));
            });
            return mEntity;
        }

        public List<IncoTermsEntity> GetIncoTerms(string Code, string Name)
        {
            List<IncoTermsEntity> mEntity = new List<IncoTermsEntity>();
            string Query = "Select IT.TERMSOFTRADEID,IT.CODE,IT.NAME,IT.DESCRIPTION " +
                "From SMDM_TERMSOFTRADE IT inner join DataProfileClassWFStates DP on IT.STATEID=DP.STATEID Where 1=1";
            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(IT.Code) like '%' || :CODE || '%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(IT.NAME) like '%'|| :NAME || '%'";
            }
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] {
                    new OracleParameter {ParameterName = "CODE", Value = Code.ToUpper() } , 
                      new OracleParameter {ParameterName = "NAME", Value = Name.ToUpper() } 
                }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<IncoTermsEntity>(r));
            });
            mEntity.Remove(mEntity.SingleOrDefault(x => x.CODE == "DDP"));
            return mEntity;
        }

        public PagedResultsEntity<IncoTermsEntity> GetIncoTermsPopup(int pageNumber, int pageSize, string Code, string Name)
        {
            PagedResultsEntity<IncoTermsEntity> mEntity = new PagedResultsEntity<IncoTermsEntity>();
            mEntity.Page = pageNumber;
            mEntity.PageSize = pageSize;

            List<IncoTermsEntity> mList = new List<IncoTermsEntity>();

            string Query = "Select IT.TERMSOFTRADEID,IT.CODE,IT.NAME,IT.DESCRIPTION " +
                "From SMDM_TERMSOFTRADE IT inner join DataProfileClassWFStates DP on IT.STATEID=DP.STATEID Where 1=1";

            string CntQuery = "SELECT COUNT(1) From SMDM_TERMSOFTRADE IT inner join DataProfileClassWFStates DP on IT.STATEID=DP.STATEID Where 1=1";

            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(IT.Code) like '%" + Code.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(IT.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(IT.NAME) like '%" + Name.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(IT.NAME) like '%" + Name.ToUpper() + "%'";
            }
            Query = Query + " AND ROWNUM BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ";
            mEntity.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, CntQuery, new OracleParameter[] { });
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] {
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mEntity.PageSize } , 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mEntity.Page } 
                }
            , delegate(IDataReader r)
            {
                mList.Add(DataReaderMapToObject<IncoTermsEntity>(r));
            });
            return mEntity;
        }

        public List<UOM> GetUOM(string Code, string Name)
        {
            List<UOM> mEntity = new List<UOM>();
            string Query = "Select U.UOMID,U.TYPEID,U.CODE,U.NAME,U.DESCRIPTION " +
                "From SMDM_UOM U Inner Join DataProfileClassWFStates DP on U.STATEID=DP.STATEID  WHERE TYPEID <> 1011";
            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(U.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(U.NAME) like '%" + Name.ToUpper() + "%'";
            }
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<UOM>(r));
            });
            return mEntity;
        }

        public PagedResultsEntity<UOM> GetUOMPopup(int pageNumber, int pageSize, string Code, string Name)
        {
            PagedResultsEntity<UOM> mEntity = new PagedResultsEntity<UOM>();
            mEntity.Page = pageNumber;
            mEntity.PageSize = pageSize;

            List<UOM> mList = new List<UOM>();

            string Query = "Select U.UOMID,U.TYPEID,U.CODE,U.NAME,U.DESCRIPTION " +
                "From SMDM_UOM U Inner Join DataProfileClassWFStates DP on U.STATEID=DP.STATEID  WHERE TYPEID <> 1011";

            string CntQuery = "SELECT COUNT(1) From SMDM_UOM U Inner Join DataProfileClassWFStates DP on U.STATEID=DP.STATEID  WHERE TYPEID <> 1011";

            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND Upper(U.Code) like '%" + Code.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(U.Code) like '%" + Code.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(U.NAME) like '%" + Name.ToUpper() + "%'";
                CntQuery = CntQuery + " AND Upper(U.NAME) like '%" + Name.ToUpper() + "%'";
            }
            Query = Query + " AND ROWNUM BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ";
            mEntity.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, CntQuery, new OracleParameter[] { });
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] {
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mEntity.PageSize } , 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mEntity.Page } 
                }
            , delegate(IDataReader r)
            {
                mList.Add(DataReaderMapToObject<UOM>(r));
            });
            return mEntity;
        }

        public List<StateEntity> GetStatesByCountry(int? CountryId, string Code, string Name)
        {
            List<StateEntity> mEntity = new List<StateEntity>();
            if (CountryId != 0 && CountryId != null)
            {
                string Query = "Select SUBDIVISIONID,COUNTRYID,CODE,NAME,DESCRIPTION from SMDM_SUBDIVISIONS where COUNTRYID=:COUNTRYID";
                if (!string.IsNullOrEmpty(Code.Trim()))
                {
                    Query = Query + " AND Upper(Code) like '%" + Code.ToUpper() + "%'";
                }
                if (!string.IsNullOrEmpty(Name.Trim()))
                {
                    Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
                }
                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] { new OracleParameter { ParameterName = "COUNTRYID", Value = CountryId } }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<StateEntity>(r));
                });
            }
            return mEntity;
        }

        public PagedResultsEntity<StateEntity> GetStatesByCountryPopup(int pageNumber, int pageSize, int CountryId, string Code, string Name)
        {
            PagedResultsEntity<StateEntity> mEntity = new PagedResultsEntity<StateEntity>();
            mEntity.Page = pageNumber;
            mEntity.PageSize = pageSize;
            if (CountryId != 0 && CountryId != null)
            {
                List<StateEntity> mList = new List<StateEntity>();

                string Query = "select SUBDIVISIONID,COUNTRYID,CODE,NAME,DESCRIPTION from SMDM_SUBDIVISIONS where COUNTRYID=" + CountryId;

                string CntQuery = "SELECT COUNT(1) From SMDM_SUBDIVISIONS where COUNTRYID=" + CountryId;

                if (!string.IsNullOrEmpty(Code.Trim()))
                {
                    Query = Query + " AND Upper(CODE) like '%" + Code.ToUpper() + "%'";
                    CntQuery = CntQuery + " AND Upper(CODE) like '%" + Code.ToUpper() + "%'";
                }
                if (!string.IsNullOrEmpty(Name.Trim()))
                {
                    Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
                    CntQuery = CntQuery + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
                }
                Query = Query + " AND ROWNUM BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ";
                mEntity.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, CntQuery, new OracleParameter[] { });
                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] {
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mEntity.PageSize } , 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mEntity.Page } 
                }
                , delegate(IDataReader r)
                {
                    mList.Add(DataReaderMapToObject<StateEntity>(r));
                });
            }
            return mEntity;
        }

        public List<CityEntity> GetCitiesByState(int SubDivisionId, string Code, string Name)
        {
            List<CityEntity> mEntity = new List<CityEntity>();
            if (SubDivisionId != 0 && SubDivisionId != null)
            {
                //|| '_' || SUBDIVISIONID 
                string Query = "SELECT TO_CHAR(UNLOCATIONID) UNLOCATIONID,CODE AS CODE, NAME from UNLOCATIONS where ISCITY=1 and STATEID='UNLocationsActivatedState' and SUBDIVISIONID=" + SubDivisionId;
                if (!string.IsNullOrEmpty(Code.Trim()))
                {
                    Query = Query + " AND Upper(Code) like '%" + Code.ToUpper() + "%'";
                }
                if (!string.IsNullOrEmpty(Name.Trim()))
                {
                    Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
                }
                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] { }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<CityEntity>(r));
                });
            }
            return mEntity;
        }

        public List<CityEntity> GetCitiesByStateJob(int SubDivisionId)
        {
            List<CityEntity> mEntity = new List<CityEntity>();
            if (SubDivisionId != 0 && SubDivisionId != null)
            {
                string Query = "SELECT TO_CHAR(UNLOCATIONID) UNLOCATIONID, CODE, NAME from UNLOCATIONS where ISCITY=1 and code IS NOT NULL and SUBDIVISIONID=:SUBDIVISIONID";
                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] { 
                        new OracleParameter{ OracleDbType = OracleDbType.Long, ParameterName="SUBDIVISIONID", Value= SubDivisionId }
                    }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<CityEntity>(r));
                });
            }
            return mEntity;
        }

        public List<CityEntity> GetCitiesByCountry(int CountryID)
        {
            List<CityEntity> mEntity = new List<CityEntity>();
            if (CountryID != 0)
            {
                //|| '_' || SUBDIVISIONID
                string Query = "SELECT TO_CHAR(UNLOCATIONID) UNLOCATIONID, CODE  AS CODE, CASE WHEN SUBDIVISIONCODE IS NOT NULL THEN  NAME || ', ' || SUBDIVISIONCODE ELSE NAME END as NAME from UNLOCATIONS where ISCITY=1 and STATEID='UNLocationsActivatedState' and CountryID=:COUNTRYID";

                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] {
                        new OracleParameter{ OracleDbType = OracleDbType.Long, ParameterName="COUNTRYID", Value= CountryID }
                    }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<CityEntity>(r));
                });
            }
            return mEntity;
        }

        public IList<StateEntity> GetStateByCityId(string CountryID)
        {
            List<StateEntity> mEntity = new List<StateEntity>();
            if (CountryID != "")
            {
                string Query = "Select SUBDIVISIONID,COUNTRYID,CODE,NAME,DESCRIPTION from SMDM_SUBDIVISIONS where SUBDIVISIONID=:SUBDIVISIONID";

                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] { 
                        new OracleParameter{ OracleDbType = OracleDbType.Long, ParameterName="SUBDIVISIONID", Value= CountryID }
                    }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<StateEntity>(r));
                });
            }
            return mEntity;
        }

        public PagedResultsEntity<CityEntity> GetCitiesByStatePopup(int pageNumber, int pageSize, int SubDivisionId, string Code, string Name)
        {
            PagedResultsEntity<CityEntity> mEntity = new PagedResultsEntity<CityEntity>();
            mEntity.Page = pageNumber;
            mEntity.PageSize = pageSize;
            if (SubDivisionId != 0)
            {
                List<CityEntity> mList = new List<CityEntity>();

                string Query = "select CAST(UNLOCATIONID AS NUMBER) UNLOCATIONID, CODE, NAME from UNLOCATIONS where ISCITY=1 and SUBDIVISIONID=:SUBDIVISIONID";

                string CntQuery = "SELECT COUNT(1) from UNLOCATIONS where ISCITY=1 and SUBDIVISIONID=:SUBDIVISIONID";

                if (!string.IsNullOrEmpty(Code.Trim()))
                {
                    Query = Query + " AND Upper(CODE) like '%" + Code.ToUpper() + "%'";
                    CntQuery = CntQuery + " AND Upper(CODE) like '%" + Code.ToUpper() + "%'";
                }
                if (!string.IsNullOrEmpty(Name.Trim()))
                {
                    Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
                    CntQuery = CntQuery + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
                }
                Query = Query + " AND ROWNUM BETWEEN ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + 1 AND ( :V_PAGESIZE * ( :V_PAGE - 1 ) ) + :V_PAGESIZE ";
                mEntity.TotalRows = ExecuteScalar<int>(DBConnectionMode.FOCiS, CntQuery, new OracleParameter[] { new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "SUBDIVISIONID", Value = SubDivisionId } });
                ReadText(DBConnectionMode.FOCiS, Query,
                    new OracleParameter[] {
                    new OracleParameter { OracleDbType = OracleDbType.Long, ParameterName = "SUBDIVISIONID", Value = SubDivisionId },
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGESIZE", Value = mEntity.PageSize } , 
                    new OracleParameter { OracleDbType = OracleDbType.Long, Size=10, ParameterName = "V_PAGE", Value = mEntity.Page } 
                }
                , delegate(IDataReader r)
                {
                    mList.Add(DataReaderMapToObject<CityEntity>(r));
                });
            }
            return mEntity;
        }

        public List<DepartmentEntity> GetOrganizationalUnits(string Code, string Name)
        {
            List<DepartmentEntity> mEntity = new List<DepartmentEntity>();
            string Query = "SELECT TYPEID AS ORGANIZATIONALUNITID,TYPEID AS CODE,NAME FROM TYPES WHERE TYPETYPEID=42";
            if (!string.IsNullOrEmpty(Code.Trim()))
            {
                Query = Query + " AND TYPEID=" + Code;
            }
            if (!string.IsNullOrEmpty(Name.Trim()))
            {
                Query = Query + " AND Upper(NAME) like '%" + Name.ToUpper() + "%'";
            }
            Query = Query + " ORDER BY NAME";
            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<DepartmentEntity>(r));
            });
            return mEntity;
        }
        public ResultsEntity<Status<long>> CheckSanctionedCountry(string FromCountryId, string ToCountryId)
        {
            ResultsEntity<Status<Int64>> mResult = new ResultsEntity<Status<Int64>>();
            List<Status<Int64>> mList = new List<Status<Int64>>();

            ReadText(DBConnectionMode.FOCiS, "SELECT BANNEDCOUNTRYID id, NOTES IsSantioned FROM JPCOMPBANNEDCOUNTRIES WHERE FROMCOUNTRYID = :FROMCOUNTRYID AND TOCOUNTRYID = :TOCOUNTRYID",
                   new OracleParameter[] { 
                    new OracleParameter{ ParameterName = "FROMCOUNTRYID",Value = FromCountryId},
                    new OracleParameter{ ParameterName = "TOCOUNTRYID",Value = ToCountryId},
               }, delegate(IDataReader r)
               {
                   mList.Add(DataReaderMapToObject<Status<Int64>>(r));
               });
            mResult.Items = mList;
            return mResult;
        }

        public List<DefaultCountryEntity> GetDefaultCountries(string Code)
        {
            List<DefaultCountryEntity> mEntity = new List<DefaultCountryEntity>();
            string Query;
            if (Code != null && Code != string.Empty)
            {
                int convertedCode = Convert.ToInt32(Code);

                Query = "SELECT COUNTRYID ID, CODE CODE, NAME TEXT FROM SMDM_COUNTRY   WHERE STATEID = 'CountryActivatedState' AND COUNTRYID NOT IN (108,51,211,0,196,121) and  COUNTRYID not in( :Code ) ORDER BY Name";
            }
            else
            {
                Query = "SELECT COUNTRYID ID, CODE CODE, NAME TEXT FROM SMDM_COUNTRY   WHERE STATEID = 'CountryActivatedState' AND COUNTRYID NOT IN (108,51,211,0,196,121)  ORDER BY Name";
            }

            if (Code != null && Code != string.Empty)
            {
                ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { new OracleParameter { ParameterName = "Code", Value = Convert.ToInt32(Code) } }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<DefaultCountryEntity>(r));
                });
            }
            else
            {
                ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { }
                , delegate(IDataReader r)
                {
                    mEntity.Add(DataReaderMapToObject<DefaultCountryEntity>(r));
                });
            }            
            return mEntity;
        }

        public List<DefaultCurrencyEntity> GetDefaultCurrencies()
        {
            List<DefaultCurrencyEntity> mEntity = new List<DefaultCurrencyEntity>();
            string Query = "SELECT CURRENCYCODE ID, CURRENCYNAME TEXT,DECIMALS DecimalPoint FROM CURRENCIES WHERE CURRENCYCODE NOT IN('MXV','BOV','XDR','SSP','XSU','CLF','COU','UYI','CHW','ZWL','CHE','SGP')";

            ReadText(DBConnectionMode.FOCiS, Query,
                new OracleParameter[] { }
            , delegate(IDataReader r)
            {
                mEntity.Add(DataReaderMapToObject<DefaultCurrencyEntity>(r));
            });
            return mEntity;
        }
        public async Task<ResultsEntity<zipcodeEntity>> GetZipcodesByCity(string Cityid, string CountryCode)
        {
            
            ResultsEntity<zipcodeEntity> mEntity = new ResultsEntity<zipcodeEntity>();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    mCommand.Parameters.Clear();
                    mCommand.Parameters.Add("PRM_CITYID", Cityid);
                    //mCommand.Parameters.Add("PRM_ZIPCODE", dbnull)
                    mCommand.Parameters.Add("PRM_COUNTRYCODE", CountryCode);
                    mCommand.Parameters.Add("PRM_TYPE", 1);
                    mCommand.Parameters.Add("PRM_COUNTRYDATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    mCommand.CommandText = "USP_FOCIS_GETZIPCODEDATA";
                    mCommand.CommandType = CommandType.StoredProcedure;
                    mCommand.ExecuteNonQuery();

                    DataTable dtzipcodes = new DataTable();

                    OracleDataReader zipreader = ((OracleRefCursor)mCommand.Parameters["PRM_COUNTRYDATA"].Value).GetDataReader();
                    dtzipcodes.Load(zipreader);
                    List<zipcodeEntity> objlist = new List<zipcodeEntity>();

                    for (int i = 0; i < dtzipcodes.Rows.Count; i++)
                    {
                        zipcodeEntity objUnloctions = new  zipcodeEntity();
                        objUnloctions.Zipcode = dtzipcodes.Rows[i]["ZIPCODE"].ToString();
                        objlist.Add(objUnloctions);
                    }
                    mEntity.Items = await Task.Run(() => objlist.AsEnumerable<zipcodeEntity>()).ConfigureAwait(false);
                    zipreader.Close();
                    mConnection.Close();
                }
            }
            return mEntity;
        }
        public ResultsEntity<zipcodeEntityUnloc> GetCitiesByZipCode(string Zip, string CountryCode)
        {

            ResultsEntity<zipcodeEntityUnloc> mEntity = new ResultsEntity<zipcodeEntityUnloc>();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    mCommand.Parameters.Clear();
                    mCommand.Parameters.Add("PRM_ZIPCODE", Zip);
                    mCommand.Parameters.Add("PRM_COUNTRYCODE", CountryCode);
                    mCommand.Parameters.Add("PRM_TYPE", 1);
                    mCommand.Parameters.Add("PRM_COUNTRYDATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    mCommand.CommandText = "USP_FOCIS_GETZIPCODEDATA";
                    mCommand.CommandType = CommandType.StoredProcedure;
                    mCommand.ExecuteNonQuery();

                    DataTable dtzipcodes = new DataTable();

                    OracleDataReader zipreader = ((OracleRefCursor)mCommand.Parameters["PRM_COUNTRYDATA"].Value).GetDataReader();
                    dtzipcodes.Load(zipreader);
                    List<zipcodeEntityUnloc> objlist = new List<zipcodeEntityUnloc>();

                    for (int i = 0; i < dtzipcodes.Rows.Count; i++)
                    {
                        zipcodeEntityUnloc objUnloctions = new zipcodeEntityUnloc();
                        objUnloctions.UNLOCATIONID = Convert.ToDecimal( dtzipcodes.Rows[i]["UNLOCATIONID"]);
                        objUnloctions.CODE = dtzipcodes.Rows[i]["CODE"].ToString();
                        objUnloctions.NAME = dtzipcodes.Rows[i]["NAME"].ToString();
                        objUnloctions.SUBDIVISIONCODE = dtzipcodes.Rows[i]["SUBDIVISIONCODE"].ToString();
                        objUnloctions.COUNTRYID = Convert.ToDecimal(dtzipcodes.Rows[i]["COUNTRYID"]);
                        objUnloctions.COUNTRYCODE = dtzipcodes.Rows[i]["COUNTRYCODE"].ToString();


                        objlist.Add(objUnloctions);
                    }
                    mEntity.Items = objlist.AsEnumerable<zipcodeEntityUnloc>();
                    zipreader.Close();
                    mConnection.Close();
                }
            }
            return mEntity;
        }

        public ResultsEntity<zipcodeEntity> IsZipcodesAvailable(string CountryCode)
        {
            ResultsEntity<zipcodeEntity> mEntity = new ResultsEntity<zipcodeEntity>();

            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(FOCISConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    mCommand.BindByName = true;
                    mCommand.Transaction = mOracleTransaction;
                    mCommand.CommandType = CommandType.StoredProcedure;

                    mCommand.Parameters.Clear();
                    mCommand.Parameters.Add("PRM_COUNTRYCODE", CountryCode);
                    mCommand.Parameters.Add("PRM_COUNTRYDATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    mCommand.CommandText = "USP_FOCIS_GETZIPCODEDATA";
                    mCommand.CommandType = CommandType.StoredProcedure;
                    mCommand.ExecuteNonQuery();

                    DataTable dtzipcodes = new DataTable();

                    OracleDataReader zipreader = ((OracleRefCursor)mCommand.Parameters["PRM_COUNTRYDATA"].Value).GetDataReader();
                    dtzipcodes.Load(zipreader);
                    List<zipcodeEntity> objlist = new List<zipcodeEntity>();

                    for (int i = 0; i < dtzipcodes.Rows.Count; i++)
                    {
                        zipcodeEntity objUnloctions = new zipcodeEntity();
                        objUnloctions.Zipcode = dtzipcodes.Rows[i]["ZIPCODE"].ToString();
                        objlist.Add(objUnloctions);
                    }
                    mEntity.Items = objlist.AsEnumerable<zipcodeEntity>();
                    zipreader.Close();
                    mConnection.Close();
                }
            }
            return mEntity;
        }

        public string AddTeammembers(string Emails, string Type, string user, string partyname, long partyid)
        {
            string message = string.Empty;
            OracleConnection mConnection;
            OracleTransaction mOracleTransaction = null;
            using (mConnection = new OracleConnection(SSPConnectionString))
            {
                if (mConnection.State == ConnectionState.Closed) mConnection.Open();
                mOracleTransaction = mConnection.BeginTransaction();
                using (OracleCommand mCommand = mConnection.CreateCommand())
                {
                    try
                    {
                        mCommand.BindByName = true;
                        mCommand.Transaction = mOracleTransaction;
                        mCommand.CommandType = CommandType.StoredProcedure;

                        mCommand.Parameters.Add("IP_EMAILS", Oracle.DataAccess.Client.OracleDbType.Varchar2).Value = Emails;
                        mCommand.Parameters.Add("IP_USERID", Oracle.DataAccess.Client.OracleDbType.Varchar2).Value = user;
                        mCommand.Parameters.Add("IP_TYPE", Oracle.DataAccess.Client.OracleDbType.Varchar2).Value = Type;
                        mCommand.Parameters.Add("IP_PARTYNAME", Oracle.DataAccess.Client.OracleDbType.Varchar2).Value = partyname;
                        mCommand.Parameters.Add("IP_PARTYID", Oracle.DataAccess.Client.OracleDbType.Long).Value = partyid;
                        mCommand.Parameters.Add("OP_MESSAGE", Oracle.DataAccess.Client.OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        mCommand.CommandText = "USP_SSP_ADDALTERNATENAMES";
                        mCommand.ExecuteNonQuery();

                        mOracleTransaction.Commit();
                        DataTable dt = new DataTable();
                        OracleDataReader Message = ((OracleRefCursor)mCommand.Parameters["OP_MESSAGE"].Value).GetDataReader();
                        dt.Load(Message);
                        message = dt.Rows[0][0].ToString();
                    }
                    catch (Exception ex)
                    {
                        mOracleTransaction.Rollback();
                        throw new ShipaDbException(ex.ToString());
                    }
                }
            }
            return message;
        }       
    }
}
