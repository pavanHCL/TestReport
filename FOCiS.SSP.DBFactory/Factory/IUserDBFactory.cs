﻿using FOCiS.SSP.DBFactory.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOCiS.SSP.DBFactory.Core;

namespace FOCiS.SSP.DBFactory.Factory
{
    public interface IUserDBFactory
    {
        UserEntity GetUserDetails(string UserId);
        UserEntity GetUserFirstAndLastName(string UserId);
        QuotationEntity GetQuoteDetails(int Id);
        UserProfileEntity GetUserProfileDetails(string UserId, int tab);
        AdditionalCreditEntity GetAddCreditLimit(string UserId, int addCreditId);
        long Save(UserEntity entity);
        void Update(UserEntity entity);
        void ActivateUserAccountStatus(string UserId);
        void UpdateUserProfile(UserProfileEntity entity, string hdnvalue, string notifymsg, string ChangeState);
        void UpdateCredit(UserProfileEntity entity);
        long? UpdateAdditionalCredit(UserProfileEntity entity);
        void ResetPassword(string UserId, string Password);
        List<NotificationEntity> GetNotificationDetails(string UserId);
        DataSet GetNotificationDetailsScript(string UserId);
        DataTable ShowNotificationCount(string UserId);
        string DeleteNotification(int Id);
        string NotificationsMark(int Id);
        int getJobBookingCount(string userid);
        DataSet GetCountryTaxDetails(int countryid);
        IList<SSPDocumentsEntity> GetCreditDocument(string userid, string filename);
        IList<SSPDocumentsEntity> GetAddCreditDocument(string userid, string filename);
        void DeleteCreditDocument(string userid, int docid);
        void DeleteAddCreditDocument(string userid, int docid);
        ProfileInfo GetProfileInfo(string UserId);
        DataSet IsProfileUpdated(string UserId);
        string UnlocationMapping(int CountryId);
    }
}
