﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ActiveCampaign.Helpers;
using ActiveCampaign.Responses;
using FOCIS.SSP.ActiveCampaign;

namespace ActiveCampaign.Models
{
    public class Contact : Base
    {
        public Contact(Api api)
        {
            Api = api;
        }

        public SubscriberInsertResponse Add(Struct.Contact contact)
        {
            var request = Api.Acurl + "contact_add";

            string postData =
                "email=" + HttpUtility.UrlEncode(contact.email)
                + "&first_name=" + contact.firstName
                + "&last_name=" + contact.lastName
                + "&phone=" + contact.phone
                + "&orgname=" + contact.orgName
                + "&tags=" + GetTags(contact)
                + "&ip4=" + contact.ip4
                + "&status[" + contact.list + "]=" + contact.status
                + "&field[%SubscriptionNotification%,0]=" + contact.notificationSubscription
                + "&field[%Quotation_Number%,0]=" + contact.quotationNumber
                + "&field[%Quotation_Number%,0]=" + contact.quotationNumber
                + "&field[%Quote_Request_Date%,0]=" + contact.quoteRequestDate
                + "&field[%GSSC_Quote_Received_Date%,0]=" + contact.quoteGivenByGSSCDate
                + "&field[%Booking_ID%,0]=" + contact.operationalJobNumber
                + "&field[%Origin_Country%,0]=" + contact.oCountryName
                + "&field[%Country%,0]=" + contact.dCountryName
                + "&field[%Mode_of_Transport%,0]=" + contact.productName
                + "&field[%Movement_Type%,0]=" + contact.movementTypeName
                + "&field[%Customer_Status%,0]=" + contact.customerStatus
                + "&field[%Contact_Action%,0]=" + contact.stateId
                + "&field[%Currency%,0]=" + contact.currency
                + "&field[%Quote_Grand_Total%,0]=" + contact.quoteGrandTotal
                + "&field[%Booking_Grand_Total%,0]=" + contact.jobGrandTotal
                + "&p[" + contact.list + "]=" + contact.list;

            if (contact.form > 0)
            {
                postData += "&form=" + contact.form;
            }

            postData += HttpHelper.FormatValues("field", contact.fieldDictionary, true);
            postData += HttpHelper.FormatValues("p", contact.listDictionary);
            postData += HttpHelper.FormatValues("status", contact.statusDictionary);
            postData += HttpHelper.FormatValues("noresponders", contact.noRespondersDictionary);
            postData += HttpHelper.FormatValues("sdate", contact.sDateDictionary);
            postData += HttpHelper.FormatValues("instantresponders", contact.instantRespondersDictionary);
            postData += HttpHelper.FormatValues("lastmessage", contact.lastMessageDictionary);

            return SendRequest<SubscriberInsertResponse>(request, postData);
        }

        private static string GetTags(Struct.Contact contact)
        {
            return HttpUtility.UrlEncode(String.Join(",", contact.tags.ToArray()));
        }

        public string AutomationList(int Id, string email, int offset = 0, int limit = 20)
        {
            throw new NotImplementedException();
        }

        public string Delete(int Id = 0)
        {
            var request = Api.Acurl + "contact_delete&id=" + Id;
            var response = ReadStream(request);
            return response;
        }

        public string DeleteList(List<int> idsList)
        {
            string joinedIds = string.Join(",", idsList.Select(x => x.ToString()).ToArray());

            var request = Api.Acurl + "contact_delete_list&ids=" + joinedIds;
            var response = ReadStream(request);
            return response;
        }

        public SubscriberUpdatePost Edit(Struct.Contact contact)
        {
            var request = Api.Acurl + "contact_edit";

            string postData =
                "id=" + contact.id
                + "email=" + HttpUtility.UrlEncode(contact.email)
                + "&first_name=" + contact.firstName
                + "&last_name=" + contact.lastName
                + "&phone=" + contact.phone
                + "&orgname=" + contact.orgName
                + "&tags=" + GetTags(contact)
                + "&ip4=" + contact.ip4
                + "&status[" + contact.list + "]=" + contact.status
                + "&field[%SubscriptionNotification%,0]=" + contact.notificationSubscription
                + "&field[%Quotation_Number%,0]=" + contact.quotationNumber
                + "&field[%Quote_Request_Date%,0]=" + contact.quoteRequestDate
                + "&field[%GSSC_Quote_Received_Date%,0]=" + contact.quoteGivenByGSSCDate
                + "&field[%Booking_ID%,0]=" + contact.operationalJobNumber
                + "&field[%Origin_Country%,0]=" + contact.oCountryName
                + "&field[%Country%,0]=" + contact.dCountryName
                + "&field[%Mode_of_Transport%,0]=" + contact.productName
                + "&field[%Movement_Type%,0]=" + contact.movementTypeName
                + "&field[%Customer_Status%,0]=" + contact.customerStatus
                + "&field[%Contact_Action%,0]=" + contact.stateId
                + "&field[%Currency%,0]=" + contact.currency
                + "&field[%Quote_Grand_Total%,0]=" + contact.quoteGrandTotal
                + "&field[%Booking_Grand_Total%,0]=" + contact.jobGrandTotal
                + "&p[" + contact.list + "]=" + contact.list;

            if (contact.form > 0)
            {
                postData += "&form=" + contact.form;
            }

            postData += HttpHelper.FormatValues("field", contact.fieldDictionary, true);
            postData += HttpHelper.FormatValues("p", contact.listDictionary);
            postData += HttpHelper.FormatValues("status", contact.statusDictionary);
            postData += HttpHelper.FormatValues("noresponders", contact.noRespondersDictionary);
            postData += HttpHelper.FormatValues("sdate", contact.sDateDictionary);
            postData += HttpHelper.FormatValues("instantresponders", contact.instantRespondersDictionary);
            postData += HttpHelper.FormatValues("lastmessage", contact.lastMessageDictionary);

            return SendRequest<SubscriberUpdatePost>(request, postData);
        }

        public string List(List<int> contactList, string filters = null, int full = 0, string sort = "id",
            string sortDirection = "DESC", int page = 20)
        {
            var request = Api.Acurl + "contact_list";

            if (contactList.Count > 0)
            {
                string joinedIds = string.Join(",", contactList.Select(x => x.ToString()).ToArray());
                request += "&ids=" + joinedIds;
            }

            if (filters != null)
            {
                request += "&filters=" + filters;
            }

            request += "&full=" + full;
            request += "&sort=" + sort;
            request += "&sort_direction=" + sortDirection;
            request += "&page=" + page;

            var response = ReadStream(request);
            return response;
        }

        public string NoteAdd(int contactId, int listId, string note, Struct.Contact contact)
        {
            var request = Api.Acurl + "contact_note_add";
            string postData =
                "id=" + contactId
                + "&listid=" + listId
                + "&note=" + HttpUtility.UrlEncode(note);

            if (contact.email != null)
            {
                postData += "contact[email]=" + HttpUtility.UrlEncode(contact.email);
            }

            var response = WriteStream(request, postData);
            return response;
        }

        public string NoteEdit(int contactId, int noteId, int listId, string note)
        {
            var request = Api.Acurl + "contact_note_edit";
            var postData =
                "&subscriberid=" + contactId
                + "&listid=" + listId
                + "&noteid=" + noteId
                + "&note=" + HttpUtility.UrlEncode(note);

            var response = WriteStream(request, postData);
            return response;
        }

        public string NoteDelete(int noteId = 0)
        {
            var request = Api.Acurl + "contact_note_delete&noteid=" + noteId;
            var response = ReadStream(request);
            return response;
        }

        public string Paginator(string sort = null, int offset = 20, int limit = 20, int filter = 0, int publicBool = 0)
        {
            var request = Api.Acurl + "contact_paginator";
            request += "&sort=" + sort
                       + "&offset=" + offset
                       + "&limit=" + limit
                       + "&filter=" + filter
                       + "&public=" + publicBool;

            var response = ReadStream(request);
            return response;
        }

        public SubscriberSyncResponse Sync(Struct.Contact contact)
        {
            var request = Api.Acurl + "contact_sync";

            string postData =
                "email=" + HttpUtility.UrlEncode(contact.email)
                + "&first_name=" + contact.firstName
                + "&last_name=" + contact.lastName
                + "&phone=" + contact.phone
                + "&orgname=" + contact.orgName
                + "&tags=" + GetTags(contact)
                + "&ip4=" + contact.ip4;

            if (contact.form > 0)
            {
                postData += "&form=" + contact.form;
            }

            postData += HttpHelper.FormatValues("field", contact.fieldDictionary, true);
            postData += HttpHelper.FormatValues("p", contact.listDictionary);
            postData += HttpHelper.FormatValues("status", contact.statusDictionary);
            postData += HttpHelper.FormatValues("noresponders", contact.noRespondersDictionary);
            postData += HttpHelper.FormatValues("sdate", contact.sDateDictionary);
            postData += HttpHelper.FormatValues("instantresponders", contact.instantRespondersDictionary);
            postData += HttpHelper.FormatValues("lastmessage", contact.lastMessageDictionary);

            return SendRequest<SubscriberSyncResponse>(request, postData);
        }

        public string TagAdd(Struct.Contact contact)
        {
            var request = Api.Acurl + "contact_tag_add";

            string postData = null;

            if (contact.email != null)
            {
                postData += "email=" + HttpUtility.UrlEncode(contact.email) + "&";
            }

            if (contact.id > 0)
            {
                postData += "id=" + contact.id + "&";
            }

            if (contact.tags.Count > 0)
            {
                postData += "tags=" + GetTags(contact);
            }

            var response = WriteStream(request, postData);
            return response;
        }

        public string TagRemove(Struct.Contact contact)
        {
            var request = Api.Acurl + "contact_tag_remove";

            string postData = null;

            if (contact.email != null)
            {
                postData += "&email=" + HttpUtility.UrlEncode(contact.email);
            }

            if (contact.id > 0)
            {
                postData += "&id=" + contact.id;
            }

            if (contact.tags.Count > 0)
            {
                postData += "&tags=" + GetTags(contact);
            }

            var response = WriteStream(request, postData);
            return response;
        }

        public string View(int id = 0)
        {
            var request = Api.Acurl + "contact_view&id=" + id;
            var response = ReadStream(request);
            return response;
        }

        public string ViewEmail(string email = null)
        {
            var request = Api.Acurl + "contact_view_email&email=" + HttpUtility.UrlEncode(email);
            var response = ReadStream(request);
            return response;
        }

        public string ViewHash(string hash = null)
        {
            var request = Api.Acurl + "contact_view_hash&hash=" + HttpUtility.UrlEncode(hash);
            var response = ReadStream(request);
            return response;
        }
    }
}