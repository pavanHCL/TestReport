﻿using System;
using System.IO;
using System.Net;
using System.Text;
using ActiveCampaign.Helpers;
using ActiveCampaign.Models;
using ActiveCampaign.Struct;
using FOCIS.SSP.ActiveCampaign;

namespace ActiveCampaign
{
    public class Base
    {
        private Api PApi;

        public Api Api
        {
            get { return PApi; }
            set { PApi = value; }
        }


        public string ReadStream(string url)
        {
            string data = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var apiStream = response.GetResponseStream();
                if (apiStream != null)
                {
                    var apiReader = new StreamReader(apiStream);
                    data = apiReader.ReadToEnd();
                    apiStream.Close();
                }
            }
            response.Close();

            if (PApi.debug)
            {
                Console.WriteLine(url);
                Console.WriteLine(data);
            }

            return data;
        }

        public string WriteStream(string url, string postStruct)
        {
            string data = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Testing";
            request.Method = System.Net.WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";

            byte[] postArray = Encoding.UTF8.GetBytes(postStruct);
            request.ContentLength = postArray.Length;

            Stream apiStream = request.GetRequestStream();
            apiStream.Write(postArray, 0, postArray.Length);

            apiStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            apiStream = response.GetResponseStream();
            StreamReader apiReader = new StreamReader(apiStream);

            data = apiReader.ReadToEnd();

            apiReader.Close();
            apiStream.Close();
            response.Close();

            if (PApi.debug)
            {
                Console.WriteLine(url);
                Console.WriteLine(postStruct);
                Console.WriteLine(data);
            }

            return data;
        }

        public T SendRequest<T>(string url, string postStruct = null)
        {
            string response;

            if (string.IsNullOrEmpty(postStruct))
            {
                response = ReadStream(url);
            }
            else
            {
                response = WriteStream(url, postStruct);
            }

            return XmlHelper.Deserialize<T>(response);
        }
    }
}