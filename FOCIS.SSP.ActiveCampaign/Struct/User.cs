﻿using System;
using System.Collections.Generic;

namespace ActiveCampaign.Struct
{
    public class User : CoreStruct
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = 0; }
        }

        private string username;
        public string Username
        {
            get { return username; }
            set { username = null; }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { password = null; }
        }

        private string passwordR;
        public string PasswordR
        {
            get { return passwordR; }
            set { passwordR = null; }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = null; }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = null; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = null; }
        }

        private string phone;
        public string Phone
        {
            get { return phone; }
            set { phone = null; }
        }

        private Dictionary<int, int> groupDictionary;
        public Dictionary<int, int> GroupDictionary
        {
            get { return groupDictionary; }
            set { groupDictionary = null; }
        }

        private string language;
        public string Language
        {
            get { return language; }
            set { language = null; }
        }

        private string timezone;
        public string Timezone
        {
            get { return timezone; }
            set { timezone = null; }
        }
    }
}