﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveCampaign.Struct
{
    public class Contact : CoreStruct
    {
        private int Id = 0;

        public int id
        {
            get { return Id; }
            set { Id = value; }
        }

        private string Email = null;

        public string email
        {
            get { return Email; }
            set { Email = value; }
        }

        private string FirstName = null;

        public string firstName
        {
            get { return FirstName; }
            set { FirstName = value; }
        }
        private string LastName = null;

        public string lastName
        {
            get { return LastName; }
            set { LastName = value; }
        }

        private string Phone = null;

        public string phone
        {
            get { return Phone; }
            set { Phone = value; }
        }

        private string OrgName = null;

        public string orgName
        {
            get { return OrgName; }
            set { OrgName = value; }
        }

        private List<string> Tags = new List<string>();

        public List<string> tags
        {
            get { return Tags; }
            set { Tags = value; }
        }

        private string Ip4 = null;

        public string ip4
        {
            get { return Ip4; }
            set { Ip4 = value; }
        }

        private Dictionary<string, string> FieldDictionary = new Dictionary<string, string>();

        public Dictionary<string, string> fieldDictionary
        {
            get { return FieldDictionary; }
            set { FieldDictionary = value; }
        }

        private Dictionary<int, int> ListDictionary = new Dictionary<int, int>();

        public Dictionary<int, int> listDictionary
        {
            get { return ListDictionary; }
            set { ListDictionary = value; }
        }

        private Dictionary<int, int> StatusDictionary = new Dictionary<int, int>();

        public Dictionary<int, int> statusDictionary
        {
            get { return StatusDictionary; }
            set { StatusDictionary = value; }
        }

        private int Form = 0;

        public int form
        {
            get { return Form; }
            set { form = value; }
        }
        private Dictionary<int, int> NoRespondersDictionary = new Dictionary<int, int>();

        public Dictionary<int, int> noRespondersDictionary
        {
            get { return NoRespondersDictionary; }
            set { NoRespondersDictionary = value; }
        }

        private Dictionary<int, string> SDateDictionary = new Dictionary<int, string>();

        public Dictionary<int, string> sDateDictionary
        {
            get { return SDateDictionary; }
            set { SDateDictionary = value; }
        }

        private Dictionary<int, int> InstantRespondersDictionary = new Dictionary<int, int>();

        public Dictionary<int, int> instantRespondersDictionary
        {
            get { return InstantRespondersDictionary; }
            set { InstantRespondersDictionary = value; }
        }

        private Dictionary<int, int> LastMessageDictionary = new Dictionary<int, int>();

        public Dictionary<int, int> lastMessageDictionary
        {
            get { return LastMessageDictionary; }
            set { LastMessageDictionary = value; }
        }

        private string List = null;

        public string list
        {
            get { return List; }
            set { List = value; }
        }

        private string QuotationNumber = null;

        public string quotationNumber
        {
            get { return QuotationNumber; }
            set { QuotationNumber = value; }
        }

        private string QuoteRequestDate = null;

        public string quoteRequestDate
        {
            get { return QuoteRequestDate; }
            set { QuoteRequestDate = value; }
        }

        private string QuoteGivenByGSSCDate = null;

        public string quoteGivenByGSSCDate
        {
            get { return QuoteGivenByGSSCDate; }
            set { QuoteGivenByGSSCDate = value; }
        }

        private string OperationalJobNumber = null;

        public string operationalJobNumber
        {
            get { return OperationalJobNumber; }
            set { OperationalJobNumber = value; }
        }

        private string OCountryName = null;

        public string oCountryName
        {
            get { return OCountryName; }
            set { OCountryName = value; }
        }

        private string DCountryName = null;

        public string dCountryName
        {
            get { return DCountryName; }
            set { DCountryName = value; }
        }


        private string ProductName = null;

        public string productName
        {
            get { return ProductName; }
            set { ProductName = value; }
        }

        private string MovementTypeName = null;

        public string movementTypeName
        {
            get { return MovementTypeName; }
            set { MovementTypeName = value; }
        }

        private string OriginPortName = null;

        public string originPortName
        {
            get { return OriginPortName; }
            set { OriginPortName = value; }
        }

        private string DestinationPortName = null;

        public string destinationPortName
        {
            get { return DestinationPortName; }
            set { DestinationPortName = value; }
        }

        private string CreatedBy = null;

        public string createdBy
        {
            get { return CreatedBy; }
            set { CreatedBy = value; }
        }

        private string CustomerStatus = null;


        public string customerStatus
        {
            get { return CustomerStatus; }
            set { CustomerStatus = value; }
        }

        private string StateId = null;

        public string stateId
        {
            get { return StateId; }
            set { StateId = value; }
        }

        private string Currency = null;

        public string currency
        {
            get { return Currency; }
            set { Currency = value; }
        }

        private string QuoteGrandTotal = null;

        public string quoteGrandTotal
        {
            get { return QuoteGrandTotal; }
            set { QuoteGrandTotal = value; }
        }

        private string JobGrandTotal = null;

        public string jobGrandTotal
        {
            get { return JobGrandTotal; }
            set { JobGrandTotal = value; }
        }

        private string NotificationSubscription = null;

        public string notificationSubscription
        {
            get { return NotificationSubscription; }
            set { NotificationSubscription = value; }
        }

        private int Status;

        public int status
        {
            get { return Status; }
            set { Status = value; }
        }
    }
}