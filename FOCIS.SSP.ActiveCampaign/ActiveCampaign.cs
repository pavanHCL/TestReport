﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCIS.SSP.ActiveCampaign
{
    public class ActiveCampaign
    {
        private Api PApi;

        public Api Api
        {
            get { return PApi; }
            set { PApi = value; }
        }

        public ActiveCampaign(Api api)
        {
            PApi = api;
        }
    }
}
