﻿using ActiveCampaign.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOCIS.SSP.ActiveCampaign
{
    public class Api
    {
        // Defined URL
        private string AcUrl;
        private string ApiKey;
        private string ApiUrl;
        private bool Debug;
        private string Output = "xml";

        // Load Models
        private Account Account;
        private Automation Automation;
        private Contact Contact;
        private User User;
        private Lists List;

        public string Acurl
        {
            get { return AcUrl; }
            set { AcUrl = value; }
        }

        public string Apikey
        {
            get { return ApiKey; }
            set { ApiKey = value; }
        }

        public string Apiurl
        {
            get { return ApiUrl; }
            set { ApiUrl = value; }
        }

        public bool debug
        {
            get { return Debug; }
            set { Debug = value; }
        }

        public Contact contact
        {
            get { return Contact; }
            set { Contact = value; }
        }

        public Lists list
        {
            get { return List; }
            set { List = value; }
        }
        public Api(string apiUrl, string apiKey, bool debug, string output)
        {
            ApiUrl = apiUrl;
            ApiKey = apiKey;
            Debug = debug;

            // Build the URL
            AcUrl = ApiUrl + "/admin/api.php?api_key=" + ApiKey + "&api_output=" + output + "&api_action=";

            User = new User(this);
            Account = new Account(this);
            Contact = new Contact(this);
            List = new Lists(this);
        }

        public Api(string apiUrl, string apiKey, bool debug)
            : this(apiUrl, apiKey, debug, "xml")
        {
        }
    }
}
