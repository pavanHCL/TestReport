﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ActiveCampaign.Responses
{
    [XmlRoot(ElementName = "WebHook_UnSubScribeResult")]
    public class WebHookUnSubScribeResult
    {
        [XmlElement(ElementName = "type")]
        public int type { get; set; }

        [XmlElement(ElementName = "initiated_from")]
        public string initiatedfrom { get; set; }

        [XmlElement(ElementName = "contact[last_name]")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "initiated_by")]
        public int initiatedby { get; set; }

        [XmlElement(ElementName = "orgname")]
        public string orgname { get; set; }

        [XmlElement(ElementName = "campaign[id]")]
        public string campaignid { get; set; }

        [XmlElement(ElementName = "contact[first_name]")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "contact[id]")]
        public int contactId { get; set; }

        [XmlElement(ElementName = "campaign[status]")]
        public string campaignstatus { get; set; }

        [XmlElement(ElementName = "list[0][name]")]
        public string ListName { get; set; }

        [XmlElement(ElementName = "contact[orgname]")]
        public string contactOrgName { get; set; }

        [XmlElement(ElementName = "contact[phone]")]
        public string contactPhone { get; set; }

        [XmlElement(ElementName = "unsubscribe[reason]")]
        public string unsubscribereason { get; set; }

        [XmlElement(ElementName = "campaign[name]")]
        public string campaignname { get; set; }

        [XmlElement(ElementName = "contact[email]")]
        public string contactemail { get; set; }

        [XmlElement(ElementName = "list[0][id]")]
        public string listId { get; set; }
    }
}
